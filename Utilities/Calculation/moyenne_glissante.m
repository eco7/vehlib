% function [ERR,pmoy]=moyenne_glissante(ERR,VD,param,pinst,ind)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Moyennation du vecteur pinst (echantillonnage regulier) 
% sur une fenetre glissante de duree param.t_moy sec.
% ind : indice du calcul courant au temps VD.CYCL.temps(ind)
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1
function [ERR,pmoy]=moyenne_glissante(ERR,VD,param,pinst,ind)

if VD.CYCL.temps(ind)>param.t_moy
    ind_deb=find(VD.CYCL.temps==(VD.CYCL.temps(ind)-param.t_moy));
    pmoy=mean(pinst(ind_deb:ind));
elseif ind>1
    nb_repmat=round(param.t_moy-VD.CYCL.temps(ind)./param.pas_temps);
    pmoy=mean([pinst(1:ind) repmat(param.pini,1,nb_repmat)]);
else
    pmoy=param.pini;
end
