% Function: lissage_acc
% Fonction developpee pour lisser les donnees des cinematiques a simuler.
%
%  � Copyright IFSTTAR LTE 1999-2011 
%
%
%
% Objet:
% Fonction developpee pour lisser les donnees des cinematiques a simuler.
%
% Le but est de corriger les variations de vitesse dues aux incertitudes du capteur.
%
% Argument d'appel:
% - VD.CYCL_temps = vecteur temps de la cinematique a lisser
% - VD.CYCL_vitesse = vecteur vitesse de la cinematique a lisser en m/s
% - res = resolution du capteur de vitesse en m/s
% - freq_in = frequence d'echantillonnage du signal d'entree, en Hz.
%           Si NaN, pas variable
% - freq_out = frequence d'echantillonnaige du signale de sortie, en Hz.
%           Si NaN, pas variable
%
% Argument de retour:
% - TPS = vecteur temps de la cinematique lissee
% - VIT = vecteur vitesse de la cinematique lissee
% - VD.ACC = vecteur acceleration de la cinematique lissee
% ------------------------------------------------------
%
function [TPS,VIT,VD.ACC]=lissage_acc(VD.CYCL_temps,VD.CYCL_vitesse,res,freq_in,freq_out);

disp('Appel de lissage_acc.m : Lissage de la vitesse et de l''acceleration');
disp(strcat('Resolution du capteur de vitesse : ',num2str(res),' m/s'));

if ~isnan(freq_in)
    pas=1/freq_in;	% pas de mesure en s
    disp(strcat('Pas de mesure : ',num2str(pas),' s'));
else
    pas=0.1;    % Valeur arbitraire pour les pentes.
    disp('Pas de mesure variable');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Changement de nom de variable pour conserver les valeurs d'origine
VIT=VD.CYCL_vitesse;
TPS=VD.CYCL_temps;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PARTIE 1 : lissage a freq_in
% premier lissage pour enlever les oscillations a vitesse constante
n=3;
VIT(1)=VD.CYCL_vitesse(1);
while n<length(TPS);
    % On enleve les perturbations a l'arret du vehicule
    if VIT(n-1)<2.5*res
        if VIT(n)<2.5*res
            VIT(n-1)=0;
        end;
    % On lisse les palliers
    elseif abs(VIT(n-2) - VIT(n-1))<1.5*res;
        if abs(VIT(n-1)-VIT(n))<1.5*res;
            if abs(VIT(n-2) - VIT(n))<0.5*res;
                VIT(n-1)=(VIT(n-2)+VIT(n))/2;
            end;
        end;
    end;
    n=n+1;
end;

% Deuxi�me lissage pour sellectionner des points particuliers
% des palliers de vitesse en vue d'une meilleure interpollation
i=0;
n=0;
while n<(length(TPS)-2);
   n=n+1;
   i=i+1;
   if VIT(n)<0.5*res;
      vitesse(i)=VIT(n);
      temps(i)=TPS(n);
   elseif abs(VIT(n)-VIT(n+1))<0.5*res;
      j=n;
      n=n+1;
      while abs(VIT(n)-VIT(n+1))<0.5*res;
         n=n+1;
         if n>length(TPS)-0.5;
             break;
         end;
      end;
      % imposistion de palliers pour la spline
      vitesse(i)=(VIT(j)+VIT(n))/2;
      temps(i)=((TPS(j)+TPS(n))/2)-0.01*pas;
      vitesse(i+1)=(VIT(j)+VIT(n))/2;
      temps(i+1)=(TPS(j)+TPS(n))/2;
      vitesse(i+2)=(VIT(j)+VIT(n))/2;
      temps(i+2)=((TPS(j)+TPS(n))/2)+0.01*pas;
      i=i+2;
   else
      temps(i)=TPS(n);
      vitesse(i)=VIT(n);
   end;
end;
clear VIT TPS;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PARTIE 2 : lissage a 1Hz
% Reechantillonnage a 1Hz
temps(1)=VD.CYCL_temps(1);
temps(length(temps))=VD.CYCL_temps(length(VD.CYCL_temps));

pas=1;
YY=...
   interp1(temps,vitesse,VD.CYCL_temps(1):pas:VD.CYCL_temps(length(VD.CYCL_temps)));
XX=VD.CYCL_temps(1):pas:VD.CYCL_temps(length(VD.CYCL_temps));

indice=find(isnan(YY))
for i=1:length(indice)
   YY(indice(i))=0;
end

% premier lissage pour enlever les oscillations a YYesse constante
n=3;
while n<length(XX);
    % On enleve les perturbations a l'arret du vehicule
    if YY(n-1)<2.5*res
        if YY(n)<2.5*res
            YY(n-1)=0;
        end
    % On lisse les palliers
    elseif abs(YY(n-2) - YY(n-1))<1.5*res;
        if abs(YY(n-1)-YY(n))<1.5*res;
            if abs(YY(n-2) - YY(n))<0.5*res;
                YY(n-1)=(YY(n-2)+YY(n))/2;
            end
        end
    end
    n=n+1;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interpolation cubique a freq_out
j=VD.CYCL_temps(length(VD.CYCL_temps));
if ~isnan(freq_out)
    if freq_out>1/pas
        TPS=VD.CYCL_temps(1):(1/freq_out):j;
        VIT=spline(XX,YY,TPS);
    else
        TPS=XX;
        VIT=interp1(XX,YY,TPS);
    end
else
    TPS=VD.CYCL_temps;
    VIT=spline(XX,YY,VD.CYCL_temps);
end;
clear XX YY;
% Enlever les oscillations de la vitesse a l'arret du vehicule (pb du a spline)
k=1;
while k<length(VD.CYCL_temps);
   if VD.CYCL_vitesse(k)==0;
      indice1=find(TPS==VD.CYCL_temps(k));	% Recherche de l'indice de TPS correspondant au temps ou la vitesse s'annule
      VIT(indice1)=0;
      if VD.CYCL_vitesse(k+1)==0;
         indice2=find(TPS==VD.CYCL_temps(k+1));
         for l=(indice1+1):indice2;
            VIT(l)=0;
         end;
      end;
   end;
   k=k+1;
end
% Enlever les vitesses negatives pouvant rester dans les donnees lissees par spline
k=1;
while k<length(TPS);
   if VIT(k)<0;
      VIT(k)=0;
   end;
   k=k+1;
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calcul des accelerations lissees
pas=1;  % Calcul des accelerations a 1Hz pour eviter les perturbations
% Passage a 1 Hz de la vitesse pour calculer la derivee.
YY=interp1(TPS,VIT,VD.CYCL_temps(1):pas:VD.CYCL_temps(length(VD.CYCL_temps)));
for i=1:length(YY)
   if i<2.5
      VD.ACC(i)=(2*YY(i+3)-9*YY(i+2)+18*YY(i+1)-11*YY(i))/(6*pas);
   elseif i<length(YY)-1.5
      VD.ACC(i)=(-YY(i+2)+8*YY(i+1)-8*YY(i-1)+YY(i-2))/(12*pas);
   else
      VD.ACC(i)=(-2*YY(i-3)+9*YY(i-2)-18*YY(i-1)+11*YY(i))/(6*pas);
   end
end

% Calcul de la vitesse en integrant l'acceleration
VIT_INT(1)=0;
for i=2:length(VD.ACC)
   VIT_INT(i)=VIT_INT(i-1)+VD.ACC(i-1)*pas;
end

if isnan(freq_out)
   % Reechantillonnage des accelerations a l'echantillonage initial
   VD.ACC=spline(VD.CYCL_temps(1):pas:VD.CYCL_temps(length(VD.CYCL_temps)),VD.ACC,TPS);
   VIT_INT=interp1(VD.CYCL_temps(1):pas:VD.CYCL_temps(length(VD.CYCL_temps)),VIT_INT,TPS);
else
   % Reechantillonnage des accelerations a freq_out
   if freq_out>1/pas
      VD.ACC=spline(VD.CYCL_temps(1):pas:VD.CYCL_temps(length(VD.CYCL_temps)),VD.ACC,TPS);
   else
      VD.ACC=interp1(VD.CYCL_temps(1):pas:VD.CYCL_temps(length(VD.CYCL_temps)),VD.ACC,TPS);
   end
   TPS=VD.CYCL_temps(1):1/freq_out:VD.CYCL_temps(length(VD.CYCL_temps));
   VIT_INT=interp1(VD.CYCL_temps(1):pas:VD.CYCL_temps(length(VD.CYCL_temps)),VIT_INT,TPS);
end


% mise en forme
if sum(size(VIT)==size(VD.CYCL_temps))==0 & freq_in==freq_out
   % on transpose
   VIT=VIT';
   VD.ACC=VD.ACC';
   TPS=TPS';
end

if sum(size(VIT)==size(VD.CYCL_temps))==0 & freq_in==freq_out
   errordlg('pb lissage_acc','lissage_acc');
end
