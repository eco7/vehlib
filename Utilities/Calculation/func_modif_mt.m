function [MOTH]=func_modif_mt(k_moth,MOTH,coeff_abs)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Objet: Cette fonction modifie la structure MOTH
%          contenant les champs necessaires pour renseigner 
%          le composant moteur thermique de la librairie VEHLIB.
%
% Interface entrée
% k_moth     :coeff de multiplication
% MOTH  : structure a modifiée
% coeff_abs : si non existant ou égal 0 on modifie ACM1 tel quelle est
% dans l'interface entré
%             si 1, on recharge d'abord les données contenu dans vehlib.acm1
% Important quand on fait plusieurs appel successif à cette fonction
% Sylvain Perez
% 20/07/2010

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin==3 & coeff_abs==1
    evalin('base','eval(''vehlib.motherm;'')') % NB EV 26/04/16 est ce que ca fonctionne encore avec VD ?
end


% Masse du moteur thermique
MOTH.Masse_mth    = k_moth*MOTH.Masse_mth;

% Inertie du moteur thermique
MOTH.J_mt        = k_moth^(5/3)*MOTH.J_mt;	


% % Courbes enveloppe (estimation pleine charge et coupure d'injection(Nm) en fct regime MT(rad/s)
MOTH.cmt_max=k_moth*MOTH.cmt_max;
MOTH.cmt_min=k_moth*MOTH.cmt_min;%
if isfield(MOTH,'cmt_opti')
    MOTH.cmt_opti=k_moth*MOTH.cmt_opti;%
end
MOTH.pmt_max = MOTH.cmt_max.*MOTH.wmt_max;


MOTH.Cpl_2dconso=k_moth*MOTH.Cpl_2dconso; % % Valeurs de debit de carburant en g/s
MOTH.Conso_2d=k_moth*MOTH.Conso_2d;

display('Fin d''initialisation des parametres moteur thermique');
