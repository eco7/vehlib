% Function: charge_sustain
% Calcul a bilan batterie nulle pour les modeles de la bibliotheque VEHLIB
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
%
% Calcul a bilan batterie nulle pour les modeles de la bibliotheque VEHLIB
% Fonctionne pour les hybrides thermique-electrique sur batterie
%
%
% Arguments d'appel (mais peut-etre appele sans arguments):
%      method: Methode de calcul ?? bilan batterie nul
%
%       Methode 1:       interpolation lineaire
%
%       Methode 2:       dichotomie avec critere d'arret sur amplitude de capacite batterie
%
%       Methode 3:       dichotomie avec critere d'arret sur le ratio
%                        (Energie batterie)/(Energie moteur thermique)
%
%       vehlib_f:        nom du fichier modele
%
%       architecture:    architecture du vehicule
%
%       cinem:           cinematique
%
%       int_dod:         intervalle pour dod (mini et maxi)
%
%       lect_mod:        Booleen permettant de relire le modele(1) ou pas(0) cf <vehsim_auto2>
%
%       precision:       Critere d'arret
%
%       verbose:         1 pour mode verbeux
%
%
% Arguments de retour:
%     erreur:       Code d'erreur:
%
%       4: mauvais nbre d arguments
%
%       1: precision sur variation de dod non atteintes pour le nombre d iteration maxi
%
%       2: on n encadre pas la solution
%
%       3: sortie sur faible variation de dod initiale entre deux calculs.
%
%     consoZero:   Consommation a bilan batterie nulle (en litre)
%
%                   Attention: Suivant la methode, il s'agit de la valeur interpolee (methode 1)
%                   ou la valeur de sortie de boucle de dichotomoie si erreur=0 (methode =2 ou 3).
%
%    conso100:      Consommation du vehicule en l/100km
%
%    deltaDodZero:  dod(end)-dod(1) lors d'un calcul avec methode 2 ou 3 en %
%
%    dodIni:        dod(1) profondeur de decharge initiale en %
%
% (start code)
% Exemple de lancement:
% method=2;
% vehlib_f='PRIUS_II';
% architecture='HPDP_EPI';
% cinem='CIN_NEDC_BV';
% charge_sustain(method,vehlib_f,architecture,cinem,[20 60],1,0.05,1);
% (end code)
% ---------------------------------------------------------
function [erreur,consoZero,conso100,deltaDodZero,dodIni]=charge_sustain(method,vehlib_f,architecture,cinem,int_dod,lect_mod,precision,verbose)

if nargin~=0 & nargin~=8
    fprintf(1,'mauvais nombre d''argument\n');
    erreur=4;
    return
end

% initialisation
erreur=0;
% nombre d iterations maxi
itermax=20;
% iteration en cours
iter=0;
% turn off echo
%echo off
% suppression des messages ecran
warning off;
% Precision sur ecart de capacite initiale: critere de sortie si convergence impossible
precision2=0.01;

if nargin==0
    % Choix methode eventuellement
    if ~exist('method','var')
        %choix de la methode de calcul
        fprintf(1,'Choix de la methode de calcul\n');
        fprintf(1,'\t1	: interpolation lineaire\n');
        fprintf(1,'\t2	: dichotomie avec critere d''arret sur amplitude de capacite batterie\n');
        fprintf(1,'\t3	: dichotomie avec critere d''arret sur le ratio(Energie batterie)/(Energie moteur thermique)\n');
        method=0;
        while  isempty(method) | ( method<1 | method>3)
            method=input('Choix de la methode de calcul :');
        end
    end
    verbose=1;
    int_dod=[20 80];
    if method~=1
        % Choix de la precision de convergence
        if ~exist('precision','var')
            fprintf(1,'%s\n','Choix de la precision de convergence (Val typique 0.05 %)');
            precision=0;
            while  isempty(precision) | ( precision<=0)
                precision=input('Votre choix :');
            end
        end
    else
        precision=0.05;
    end
end

% choix du modele simulink
if nargin==0
    fprintf(1,'Choix du modele simulink :\n');
    [vehlib, VD]=vehsim_gui;
else
    % lecture donnees
    [vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,int_dod(1),lect_mod);
end

% Stockage du nom
sim_bd=vehlib.simulink;

if verbose
    % Preparation des figures
    if ~isempty(findobj('tag','charge_sustaining'))
        close(findobj('tag','charge_sustaining'));
    end
    figure('tag','charge_sustaining');
    subplot(2,1,1);
    xlabel('DoD relative');
    ylabel('DoD initiale');
    hold on;
    plot([precision precision],[0 70 ],'--');
    plot([-precision -precision],[0 70 ],'--');
    subplot(2,1,2);
    xlabel('DoD relative');
    ylabel('Consommation en l/100 km');
    hold on;
    plot([precision precision],[0 10 ],'--');
    plot([-precision -precision],[0 10 ],'--');

    if method==1
        % interpolation lineaire
    elseif method==2
        % dichotomie, critere 1
    elseif method==3
        % dichotomie, critere 2
        subplot(2,1,1);
        xlabel('Ratio d''energie - batterie/moteur thermique');
        subplot(2,1,2);
        xlabel('Ratio d''energie - batterie/moteur thermique');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Premier calcul avec une decharge faible
dod_ini1=int_dod(1);
evalin('base',strcat('VD.INIT.Dod0=',num2str(dod_ini1),';'));

% on lance le calcul
evalin('base','clear erreur;');

assignin('base','VD',VD);
chaine=['try, sim(''' sim_bd '''),catch, erreur=lasterr, end'];
evalin('base',chaine);
if evalin('base','exist(''erreur'',''var'')')
    % une erreur dans les donnees ou le modele
    erreur=evalin('base','erreur');
    warndlg(erreur,'Charge_sustaining - Probleme de calcul');
    return;
else
    iter=iter+1;
end

% Recuperation des vecteurs dans le workspace.
dod=evalin('base','dod');
conso=evalin('base','conso');

% on recupere la PDD fin de calcul
dod_fin1=dod(end);
dod_rel1=dod(end)-dod(1);
% on stocke la conso en l
conso1=conso(end);

% critere de convergence
if method==1 |method==2
    critere=(dod(length(dod))-dod(1));
elseif method==3
    chaine='(sum(trapz(tsim,ibat))).*mean(ubat)/sum(trapz(tsim,wmt.*cmt));';
    critere=evalin('base',chaine);
end

if verbose
    subplot(2,1,1)
    plot(critere,dod(1),'+');
    subplot(2,1,2)
    chaine=strcat('plot(',num2str(critere),',100*conso(end)/(max(distance)/1000),''+'')');
    evalin('base',chaine);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Deuxieme calcul avec une decharge forte
dod_ini2=int_dod(2);
VD.INIT.Dod0=dod_ini2;

% on lance le calcul
assignin('base','VD',VD);
chaine=['try, sim(''' sim_bd '''),catch, erreur=lasterr, end'];
evalin('base',chaine);
if evalin('base','exist(''erreur'',''var'')')
    % une erreur dans les donnees ou le modele
    erreur=evalin('base','erreur');
    warndlg(erreur,'Charge_sustain - Probleme de calcul');
    return;
else
    iter=iter+1;
end

% Recuperation des vecteurs dans le workspace.
dod=evalin('base','dod');
conso=evalin('base','conso');

% on recupere la PDD fin de calcul
dod_fin2=dod(end);
dod_rel2=dod(end)-dod(1);
% on stocke la conso en l
conso2=conso(end);

% critere de convergence
if method==1 |method==2
    critere=(dod(length(dod))-dod(1));
elseif method==3
    chaine='(sum(trapz(tsim,ibat))).*mean(ubat)/sum(trapz(tsim,wmt.*cmt));';
    critere=evalin('base',chaine);
end

if verbose
    subplot(2,1,1)
    plot(critere,dod(1),'+');
    subplot(2,1,2)
    chaine=strcat('plot(',num2str(critere),',100*conso(end)/(max(distance)/1000),''+'')');
    evalin('base',chaine);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% on cherche a interpoler a delta nul
x=[dod_fin1-dod_ini1 dod_fin2-dod_ini2];
y=[dod_ini1 dod_ini2];
z=[conso1 conso2];

if x(1)*x(2)>0
    fprintf(1,'%s\n','Impossible d obtenir un bilan nul ');
    fprintf(1,'%s%f%s\n','Decharge initiale : ',dod_ini1,' %')
    fprintf(1,'%s%f%s\n','Variation relative capacite batterie: ',dod_fin1-dod_ini1,' %')
    fprintf(1,'%s%f%s\n','Consommation : ',conso1,' l')
    fprintf(1,'%s%f%s\n','Decharge initiale : ',dod_ini2,' %')
    fprintf(1,'%s%f%s\n','Variation relative capacite batterie: ',dod_fin2-dod_ini2,' %')
    fprintf(1,'%s%f%s\n','Consommation : ',conso2,' l')
    erreur=2;
    consoZero=NaN;
    dodIni=dod(1);
    conso100=NaN;
    deltaDodZero=NaN;
    return;
else
    consoZero=interp1(x,z,0);
    fprintf(1,'%s%f%s\n','Decharge initiale : ',dod_ini1,' %')
    fprintf(1,'%s%f%s\n','Variation relative capacite batterie: ',dod_fin1-dod_ini1,' %')
    fprintf(1,'%s%f%s\n\n','Consommation : ',conso1,' l')
    fprintf(1,'%s%f%s\n','Decharge initiale : ',dod_ini2,' %')
    fprintf(1,'%s%f%s\n','Variation relative capacite batterie: ',dod_fin2-dod_ini2,' %')
    fprintf(1,'%s%f%s\n\n','Consommation : ',conso2,' l')
    fprintf(1,'\n%s%d%s\n','Methode de calcul: ',1,' (Interpolation lineaire)')
    fprintf(1,'%s%f%s\n','Consommation equivallente a bilan batterie nul: ',consoZero,' l')
    fprintf(1,'%s%f%s\n\n','Consommation equivallente a bilan batterie nul: ',100*consoZero/evalin('base','(max(distance)/1000)'),' l/100 km')
end

if method==1
    deltaDodZero=0;
    dodIni=dod(1);
    conso100=100*consoZero/evalin('base','(max(distance)/1000)');
end

%%%%%%%%%%%%%%%%%%%%%%%%
if method==2 | method==3
    
    % on affine l'intervalle de dod avec une troisieme calcul
    dod_ini3=dod_ini1-dod_rel1*(dod_ini2-dod_ini1)/(dod_rel2-dod_rel1);
    dod0=dod_ini3;
    VD.INIT.Dod0=dod0;
    % on lance le calcul
    assignin('base','VD',VD);
    chaine=['try, sim(''' sim_bd '''),catch, erreur=lasterr, end'];
    evalin('base',chaine);
    if evalin('base','exist(''erreur'',''var'');')
        % une erreur dans les donnees ou le modele
        erreur=evalin('base','erreur');
        warndlg(erreur,'Charge_sustain - Probleme de calcul');
        return;;
    else
        iter=iter+1;
    end

    % Recuperation des vecteurs dans le workspace.
    dod=evalin('base','dod');
    conso=evalin('base','conso');
    
    % critere de convergence
    if method==1 |method==2
        critere=(dod(length(dod))-dod(1));
    elseif method==3
        chaine='(sum(trapz(tsim,ibat))).*mean(ubat)/sum(trapz(tsim,wmt.*cmt));';
        critere=evalin('base',chaine);
    end

    if verbose
        subplot(2,1,1)
        plot(critere,dod(1),'+');
        subplot(2,1,2)
        chaine=strcat('plot(',num2str(critere),',100*conso(end)/(max(distance)/1000),''+'')');
        evalin('base',chaine);
    end

    dod_rel3=dod(end)-dod(1);
    conso3=conso(end);
    
    % on affine l'intervalle de dod avec une quatrieme calcul
    dod_ini4=dod_ini3+2*dod_rel3;
    dod0=dod_ini4;
    VD.INIT.Dod0=dod0;
    % on lance le calcul
    assignin('base','VD',VD);
    chaine=['try, sim(''' sim_bd '''),catch, erreur=lasterr, end'];
    evalin('base',chaine);
    if evalin('base','exist(''erreur'',''var'');')
        % une erreur dans les donnees ou le modele
        erreur=evalin('base','erreur');
        warndlg(erreur,'Charge_sustain - Probleme de calcul');
        return;;
    else
        iter=iter+1;
    end

    % Recuperation des vecteurs dans le workspace.
    dod=evalin('base','dod');
    conso=evalin('base','conso');
    
    % critere de convergence
    if method==1 |method==2
        critere=(dod(length(dod))-dod(1));
    elseif method==3
        chaine='(sum(trapz(tsim,ibat))).*mean(ubat)/sum(trapz(tsim,wmt.*cmt));';
        critere=evalin('base',chaine);
    end

    if verbose
        subplot(2,1,1)
        plot(critere,dod(1),'+');
        subplot(2,1,2)
        chaine=strcat('plot(',num2str(critere),',100*conso(end)/(max(distance)/1000),''+'')');
        evalin('base',chaine);
    end

    dod_rel4=dod(length(dod))-dod(1);
    conso4=conso(end);
    
    % on demarre la dichotomie sur l'intervalle [dod_ini3 dod_ini4]
    % Le vecteur x est suppose decroissant pour le choix des intervalles
    % pendant la dichotomie
    x=[dod_rel3 dod_rel4];
    y=[dod_ini3 dod_ini4];
    z=[conso3 conso4];
    if x(1)*x(2)>0
       % un probleme dans le choix de l intervalle !
       if x(1)>0
        fprintf(1,'%s\n','On n a pas pu reduire l intervalle de dod; cas 1');
        x=[dod_rel3 dod_rel2];
        y=[dod_ini3 dod_ini2];
        z=[conso3 conso2];
       else
        fprintf(1,'%s\n','On n a pas pu reduire l intervalle de dod; cas 2');
        x=[ dod_rel1 dod_rel4];
        y=[dod_ini1 dod_ini4];
        z=[conso1 conso4];
       end          
    end
        
    if x(1)<x(2)
        fprintf(1,'%s\n','attention; intervalle de dod initiales decroissantes');
        fprintf(1,'%s\n','On permute les vecteurs x ey y');
        x=[x(2) x(1)];
        y=[y(2) y(1)];
    end

    if verbose
        fprintf(1,'%s\n','Debut dichotomie');
        fprintf(1,'%s%.3f%s%.3f\n','vecteur des dod relatives ',x(1),' et ',x(2));
        fprintf(1,'%s%.3f%s%.3f\n','vecteur des dod initiales ',y(1),' et ',y(2));
        fprintf(1,'%s%.3f%s%.3f\n','vecteur des consommations ',z(1),' et ',z(2));
        fprintf(1,'%s%.2f%s%.2f%s\n','Consommations : ',100*z(1)/(evalin('base','max(distance)')/1000),' et ',100*z(2)/(evalin('base','max(distance)')/1000),' l/100 km');      
    end

    %%%%%%%%%%%%
    % dichotomie
   
    % critere de convergence
    if method==2
        critere=(dod(length(dod))-dod(1));
    elseif method==3
        chaine='(sum(trapz(tsim,ibat))).*mean(ubat)/sum(trapz(tsim,wmt.*cmt))';
        critere=evalin('base',chaine);
    end

    % Boucle de dichotomie
    while abs(critere)>precision & iter<=itermax & abs(y(1)-y(2))>precision2

        dod0=(y(1)+y(2))/2;
        VD.INIT.Dod0=dod0;

        % on lance le calcul
        assignin('base','VD',VD);
        chaine=['try, sim(''' sim_bd '''),catch, erreur=lasterr, end;'];
        evalin('base',chaine);
        if evalin('base','exist(''erreur'',''var'')')
            % une erreur dans les donnees ou le modele
            erreur=evalin('base','erreur');
            warndlg(erreur,'Charge_sustain - Probleme de calcul');
            return;
        else
            iter=iter+1;
        end

        % Recuperation des vecteurs dans le workspace.
        dod=evalin('base','dod');
        conso=evalin('base','conso');

        if method==2
            critere=(dod(length(dod))-dod(1));
        elseif method==3
            chaine='(sum(trapz(tsim,ibat))).*mean(ubat)/sum(trapz(tsim,wmt.*cmt))';
            critere=evalin('base',chaine);
        end

        if verbose
            subplot(2,1,1)
            plot(critere,dod(1),'+');
            subplot(2,1,2)
            chaine=strcat('plot(',num2str(critere),',100*conso(end)/(max(distance)/1000),''+'')');
            evalin('base',chaine);
        end
        if dod(length(dod))-dod(1)>0
            % decharge batterie pendant le calcul
            x(1)=dod(end)-dod(1);
            y(1)=dod0;
            z(1)=conso(end);
        else
            x(2)=dod(end)-dod(1);
            y(2)=dod0;
            z(2)=conso(end);
        end

    end % sortie du while (dichotomie)


    % Test de sortie
    if iter>itermax
        fprintf(1,'\n%s\n','Attention: la precision demandee n''a pas ete atteinte');
        erreur=1;
        consoZero=NaN;
        deltaDodZero=NaN;
        dodIni=dod(1);
        conso100=NaN;
    else
        % sortie sur un des deux criteres
        if abs(critere)<precision
            % tout c est bien passe
            erreur=0;
            fprintf(1,'\n%s%d\n','Methode de calcul: ',method)
            fprintf(1,'%s%d%s\n','Convergence atteinte apres ',iter,' iterations')
            fprintf(1,'%s%.2f%s\n','Capacite initiale batterie: ',dod(1),' %')
            fprintf(1,'%s%.4f%s\n','Variation relative capacite batterie: ',dod(end)-dod(1),' %')
            fprintf(1,'%s%.4f%s\n','Consommation : ',conso(end),' l')
            fprintf(1,'%s%.3f%s\n','Consommation : ',100*conso(end)/(evalin('base','max(distance)')/1000),' l/100 km');
            consoZero=conso(end);
            deltaDodZero=dod(end)-dod(1);
            dodIni=dod(1);
            conso100=100*consoZero/evalin('base','(max(distance)/1000)');
        elseif  abs(y(1)-y(2))<precision2 & length(unique(x))==length(x)
            % sortie sur faible variation de dod initiale entre deux calculs
            erreur=3;
            % on interpole quand meme;
            consoZero=interp1(x,z,0);
            deltaDodZero=NaN;
            dodIni=dod(1);
            conso100=100*consoZero/evalin('base','(max(distance)/1000)');
            fprintf(1,'\n%s%d\n','Methode de calcul: ',method)
            fprintf(1,'%s%d\n','Sortie sur erreur: ',erreur)
            fprintf(1,'%s%.4f%s%.4f\n','Vecteur de delta DOD: ',x(1),' et',x(2));
            fprintf(1,'%s%.4f%s%.4f\n','Vecteur de DOD initiales: ',y(1),' et',y(2));
            fprintf(1,'%s%.4f%s%.4f\n','Vecteur consommations cumulees: ',z(1),' et',z(2));
            fprintf(1,'%s%.4f\n','Consommation interpolee: ',consoZero);
            fprintf(1,'%s%.3f%s\n','Consommation : ',100*consoZero/(evalin('base','max(distance)')/1000),' l/100 km');
        elseif  abs(y(1)-y(2))<precision2 & length(unique(x))~=length(x)
            % deux valeurs de delta dod identiques !
            erreur=5;
            fprintf(1,'\n%s%d\n','Methode de calcul: ',method)
            fprintf(1,'%s%d\n','Sortie sur erreur: ',erreur)
            fprintf(1,'%s%.4f%s%.4f\n','Vecteur de delta DOD: ',x(1),' et ',x(2));
            fprintf(1,'%s%.4f%s%.4f\n','Vecteur de DOD initiales: ',y(1),' et ',y(2));
            fprintf(1,'%s%.4f%s%.4f\n','Vecteur consommations cumulees: ',z(1),' et ',z(2));
            fprintf(1,'%s%.3f%s\n','Consommation pour z(1): ',100*z(1)/(evalin('base','max(distance)')/1000),' l/100 km');
            fprintf(1,'%s%.3f%s\n','Consommation pour z(2): ',100*z(2)/(evalin('base','max(distance)')/1000),' l/100 km');
            consoZero=NaN;
            deltaDodZero=NaN;
            dodIni=dod(1);
            conso100=NaN;
        end
    end

end % fin methode ==2 ou 3



% validation des messages ecran
warning on;
%echo on;

bdclose;
