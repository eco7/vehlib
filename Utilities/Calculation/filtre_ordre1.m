% function [ERR,pfiltre]=filtre_ordre1(ERR,VD,param,pinst,pfiltre,ind
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Filtrage du vecteur pinst avec un filtre du premier ordre 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

function [ERR,pfiltre]=filtre_ordre1(ERR,VD,param,pinst,pfiltre,ind)

wc=2*pi*param.fc;
DT=param.pas_temps;
al=DT/(DT+1/wc);
if ind>1
    pfiltre(ind)=al*pinst(ind)+(1-al)*pfiltre(ind-1);
else
    pfiltre(ind)=0;
end

