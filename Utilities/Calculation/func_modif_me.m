function [ACM]= func_modif_me(b,ACM1,coeff_abs)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Objet: Cette fonction modifie la structure ACM1
%          contenant les champs necessaires pour renseigner 
%          le composant moteur electrique de traction de la librairie VEHLIB.
%
% Interface entrée
% b     :coeff de multiplication
% ACM1  : structure a modifiée
% coeff_abs : si non existant ou égal 0 on modifie ACM1 tel quelle est
% dans l'interface entré
%             si 1, on recharge d'abord les données contenu dans vehlib.acm1
% Important quand on fait plusieurs appel successif à cette fonction
% Sylvain Perez
% 20/07/2010

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin==3 & coeff_abs==1
    evalin('base','eval(''vehlib.acm1;'')')
end

ACM=ACM1;

ACM.Cmot_pert=b*ACM1.Cmot_pert;

ACM.Pert_mot=b*ACM1.Pert_mot;

ACM.Cpl_frot=b*ACM1.Cpl_frot;

ACM.J_mg=   b^(5/3)* ACM1.J_mg ;   %  a revoir unite bizarre sur la plaquette

ACM.Masse_mg= b*ACM1.Masse_mg;    %moteur+controle

ACM.Cmax_mot=b*ACM1.Cmax_mot;
ACM.Cmin_mot=b*ACM.Cmin_mot;

% Courbe de performances maxi sur l arbre du moteur de traction pour le calculateur
if isfield(ACM,'cpl_trac_max')
    ACM.cpl_trac_max=b*ACM.cpl_trac_max;
else
    ACM.cpl_trac_max=ACM.Cmax_mot(:,length(ACM.Tension_cont))';
end
if isfield(ACM,'cpl_trac_min')
    ACM.cpl_trac_min=b*ACM.cpl_trac_min;
else
    ACM.cpl_trac_min=ACM.Cmin_mot(:,length(ACM.Tension_cont))';
end

display('Fin d''initialisation des parametres moteur electrique');


