
% function [a,b] = calc_fft(fh,T,t,N,verbose)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Décomposition en serie de fourier

% Arguments appel :
% fh : handle de la fonction d'appel
% T : Periode du signal
% t : echantillonage, si pas défini on le fera par defaut
% N : Nombre d'harmonique souhaite (1000 si pas defini)
% verbose : trace des fonction d'entrée et de la fonction recomposé si on
% le souhaite 

% Arguments de sortie :
% a,b : coeff de la transformee de fourier
% m : module des harmoniques
%
% 21-11-11(EV): Creation 

function [a,b,m] = calc_fft(fh,T,t,N,verbose)

if nargin < 3 || sum(t)==0
    t=-T/2:T/10000:T/2;
end

if nargin < 3 || N==0
    N=1000;
end

a0=1/T*trapz(t,fh(t));

for ii=1:N
    a(ii)=2/T*trapz(t,fh(t).*cos(ii*t*2*pi/T));
    b(ii)=2/T*trapz(t,fh(t).*sin(ii*t*2*pi/T));
end

m=sqrt(a.^2+b.^2);

fr=a0;

for ii=1:N
    fr=fr+a(ii)*cos(ii*t*2*pi/T)+b(ii)*sin(ii*t*2*pi/T);
end

if nargin >4 && verbose>=1
    figure(100)
    clf
    plot(t,fh(t),t,fr)
    grid
    legend('fonction','somme des harmoniques')
    
    figure(101)
    clf
    bar(m,0.3)
    ylabel('Amplitude des harmoniques')
end


end

