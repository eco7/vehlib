function [CYCL] = concatCinemat(listOfCin)
% concatenate a list of vehlib cycles
% ToDo : cycles with gearboxes are not allowed
% listOfCin : cell array of strings (cycles)

tsim = [];
vit = [];
threshold = 0;
for i=1:length(listOfCin)
    % run the cycle
    run(listOfCin{i});
    % Concat
    tsim=[tsim CYCL.temps+threshold];
    vit = [vit CYCL.vitesse];
    threshold = max(tsim+1);
end

VD = [];
%Create the CYCL structure
[CYCL] = createCinemat(VD, tsim, vit);

end