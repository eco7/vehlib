% function [varargout] = mat_ind(ind,D,varargin)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Recuperation des valeurs dans un tableau N-Dim dimensions
% Correspondant aux mins pris dans un autre tableau N-Dim selon la dimension D
%
% Arguments appel :
% ind : matrice N-1-Dim des indices selon la dimensions des mins
% D : dimensions selon laquelle on as pris les mins
% varargin : tableaux N-dim d'entree
% Arguments de sortie :
% varargout : tableau N-1 dim avec les vars de sorties
%
% Exemple utilisation : 
% [min_dcarb,ind_min]=min(dcarb_tot_hp_4D,[],4);
% [cacm1_hp_3D,qacm1_hp_3D]=mat_ind(ind_min,4,cacm1_hp_4D,qacm1_hp_4D);
%
% EV 04-04-16

function [varargout] = mat_ind(ind,D,varargin)

if nargout~=nargin-2
    'mat_ind:go to hell'
    'return'
end
 
ndim=ndims(varargin{1});

if ~isscalar(ind)
    for ii=1:nargout
        if ndim==2
            if D==3 || D==4 ||D==5
                varargout{ii}=varargin{ii};
            end
            [d1,d2]=size(varargin{1});
            if D==2
                T_i=1:1:d2;
                Ilin=(ind)+(T_i-1)*d1;
                varargout{ii}=varargin{ii}(Ilin);
            end
            if D==1
                T_j=1:1:d1;
                Ilin=(ind-1)'*d1+T_j;
                varargout{ii}=varargin{ii}(Ilin);
            end
            
        elseif ndim==3
            if D==4 || D==5
                varargout{ii}=varargin{ii};
            end
            [d1,d2,d3]=size(varargin{1});
            if D==1
                T_j=repmat((1:1:d2),[1 1 d3]);
                T_k=repmat(reshape((1:1:d3),[1 1 d3]),[1 d2 1]);
                Ilin=(T_k-1)*d1*d2+(T_j-1)*d1+ind;
                varargout{ii}=varargin{ii}(Ilin);
            end
            if D==3
                T_i=repmat((1:1:d1)',[1 d2]);
                T_j=repmat((1:1:d2),[d1 1]);
                Ilin=(ind-1)*d1*d2+(T_j-1)*d1+T_i;
                varargout{ii}=varargin{ii}(Ilin);
            end
            
        elseif ndim==4
            if D==5
                varargout{ii}=varargin{ii};
            end
            [d1,d2,d3,d4]=size(varargin{1});
            if D==4
                T_i=repmat((1:1:d1)',[1 d2 d3]);
                T_j=repmat((1:1:d2),[d1 1 d3]);
                T_k=repmat(reshape((1:1:d3),[1 1 d3]),[d1 d2 1]);
                Ilin=(ind-1)*d1*d2*d3+(T_k-1)*d1*d2+(T_j-1)*d1+T_i;
                varargout{ii}=varargin{ii}(Ilin);
            end
            if D==3
                T_i=repmat((1:1:d1)',[1 d2 1 d4]);
                T_j=repmat((1:1:d2),[d1 1 1 d4]);
                T_l=repmat(reshape((1:1:d4),[1 1 1 d4]),[d1 d2 1 1]);
                Ilin=(T_l-1)*d1*d2*d3+(ind-1)*d1*d2+(T_j-1)*d1+T_i;
                varargout{ii}=varargin{ii}(Ilin);
            end
            
        elseif ndim==5
            [d1,d2,d3,d4,d5]=size(varargin{1});
            if D==3
                T_i=repmat((1:1:d1)',[1 d2 1 d4  d5]);
                T_j=repmat((1:1:d2),[d1 1 1 d4 d5]);
                T_l=repmat(reshape((1:1:d4),[1 1 1 d4]),[d1 d2 1 1 d5]);
                T_m=repmat(reshape((1:1:d5),[1 1 1 1 d5]),[d1 d2 1 d4 1]);
                Ilin=(T_m-1)*d1*d2*d3*d4+(T_l-1)*d1*d2*d3+(ind-1)*d1*d2+(T_j-1)*d1+T_i;
                varargout{ii}=varargin{ii}(Ilin);
            end
            if D==5
                T_i=repmat((1:1:d1)',[1 d2 d3 d4]);
                T_j=repmat((1:1:d2),[d1 1 d3 d4]);
                T_k=repmat(reshape((1:1:d3),[1 1 d3]),[d1 d2 1 d4]);
                T_l=repmat(reshape((1:1:d4),[1 1 1 d4]),[d1 d2 d3 1]);
                Ilin=(ind-1)*d1*d2*d3*d4+(T_l-1)*d1*d2*d3+(T_k-1)*d1*d2+(T_j-1)*d1+T_i;
                varargout{ii}=varargin{ii}(Ilin);
            end
            
        elseif ndim==6
            
            
        end
    end
else
    for ii=1:nargout
        if ndim==2 % Pas sure que ndim soit utile dans le cas ind = scalaire
            if D==2
                varargout{ii}=varargin{ii}(ind);
            end
            if D==3
                varargout{ii}=varargin{ii}(:,:,ind);
            end
        elseif ndim==3
            if D==3
                varargout{ii}=varargin{ii}(:,:,ind);
            end
            if D==4
                varargout{ii}=varargin{ii}(:,:,:,ind);
            end
            if D==5
                varargout{ii}=varargin{ii}(:,:,:,:,ind);
            end
        elseif ndim==4
            if D==4
                varargout{ii}=varargin{ii}(:,:,:,ind);
            end
            if D==3
                varargout{ii}=varargin{ii}(:,:,ind,:);
            end
        elseif ndim==5
            if D==5
                varargout{ii}=varargin{ii}(:,:,:,:,ind);
            end
        elseif ndim==6
            
            
        end
    end
end
