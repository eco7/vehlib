% function : fonction donnant pour chaque puissance le point de
% fonctionnement optimum dans une cartographie (valeur la plus faible
% d'apr�s la cartographie), le regime le plus faible est le regime de
% ralenti
% -------------------------------------------------------------------------
%
% pmt_opti : vecteur des puissances testees
% wmt_opti : regime optimum
% cmt_opti : couple optimum
% couple_carto : vecteur des couples de la carto
% regime_carto : vecteur des regimes de la carto
% P_carto : cartographie des puissances
% carto : cartographie, 
% nb_iso : nb de courbe d'isopuissance pour le balayage en puissance 
% nb_pt_iso : nomdre de points testees pour chaque puissance
% C_max : vecteur courbe enveloppe
% V_max : vecteur vitesse enveloppe
% RegRal : regime de ralenti
%
% function [pmt_opti wmt_opti cmt_opti]=puissance_opti_carto(couple_carto,regime_carto,P_carto,carto,nb_iso,nb_pt_iso,C_max,V_max,RegRal)


function [pmt_opti wmt_opti cmt_opti]=puissance_opti_carto(couple_carto,regime_carto,P_carto,carto,nb_iso,nb_pt_iso,C_max,V_max,RegRal)


% recherche de la puissance max du moteur, on suppose la puissance max sur
% la courbe enveloppe

puissancemax=max(interp2(couple_carto,regime_carto,P_carto,C_max,V_max));
%puissancemax=max(C_max.*V_max); % Qui a ecrit ca (?)
pas_iso=puissancemax/nb_iso;
if isnan(puissancemax)
    warning('puissance_opti_carto:NoPmax', ...
            'Aucune Pmax trouvee, probleme probable avec la taille d''une des machines.')
    pmt_opti=[];
    wmt_opti=[];
    cmt_opti=[];
    return
end

wmt_opti=zeros(1,nb_iso+1);
cmt_opti=zeros(1,nb_iso+1);
CSPopt=zeros(1,nb_iso+1);

pmt_opti=[0:pas_iso:puissancemax puissancemax];
pmt_opti=unique(pmt_opti);
% pour chaque puissance balayage de vitesse par pas de V_max/nb_pt_iso
regime_test=RegRal:(max(regime_carto)-RegRal)/nb_pt_iso:max(regime_carto);

% enveloppe de couple max reechantillone sur regime test
cpl_max_test=interp1(V_max,C_max,regime_test,'linear','extrap');

% figure
% hold
indi=1;
for p=pmt_opti(1:end)

    % recherche des isopuissances
    cpl_test=zeros(size(regime_test));
    i=1;
    for reg=regime_test
        ptest=interp2(couple_carto,regime_carto,P_carto,couple_carto,reg);
        cpl_test(i)=interp1(ptest,couple_carto,p,'linear','extrap');
        i=i+1;
    end    
 
    % utilisation de interp2 pour avoir un grand nombre de point par
    % isopuissance

    csp=interp2(couple_carto,regime_carto,carto,cpl_test,regime_test);
    
    % suppression de points en dehors de l'enveloppe
    csp(cpl_test>cpl_max_test)=NaN;

    % stockage du meilleur point
    if isnan(min(csp))
        % pour des raisons de pas et d'interpolation, on ne trouve parfois
        % pas le point de puissance max absolu dans la zone delimitee par
        % le Cmax
        CSPopt(indi)=[];
        wmt_opti(indi)=[];
        cmt_opti(indi)=[];
        pmt_opti(indi)=[];
 %       warning('puissance_opti_carto:PmaxNotReachable', ...
   %         'Aucun couple (C,N) ne correspond a la puissance %d kW.', p/1000)
    else
        CSPopt(indi)=min(csp);
        wmt_opti(indi)=regime_test(csp==min(csp));
        cmt_opti(indi)=cpl_test(csp==min(csp));
        indi=indi+1;
    end
    
% plot(regime_test*30/pi,cpl_test,'.')
% pause

end