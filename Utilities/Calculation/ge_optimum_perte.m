% fonction generant la courbe de fonctionnement optimal d'un groupe
% electrogene
% VD.MOTH : moteur thermique
% VD.ACM2 : machine electrique
% nb_iso : nombre de puissances testees
% nb_pt_iso : nombre de points par puissance
% trace : 1 trace de la courbe optimum, 0 pas de trace

function [VD]=ge_optimum_perte(VD,nb_iso,nb_pt_iso,trace)


%% Cartogaphie des pertes du groupe ?lectrog?ne

% homogeneisation des cartographies de la machine electrique et du moteur
% thermique (VD.ACM2 reechantillonne sur VD.MOTH)

% perte electrique reechantillone sur regime et couple moteur thermique

VD.GE.Cpl_carto=VD.MOTH.Cpl_2dconso(1):(min(VD.MOTH.Cpl_2dconso(end),max(VD.ACM2.Cmot_pert))-VD.MOTH.Cpl_2dconso(1))/40:min(VD.MOTH.Cpl_2dconso(end),max(VD.ACM2.Cmot_pert));
VD.GE.Reg_carto=VD.MOTH.Reg_2dconso(1):(VD.MOTH.Reg_2dconso(end)-VD.MOTH.Reg_2dconso(1))/40:VD.MOTH.Reg_2dconso(end);

% le couple carto est positif (=couple mth) il doit etre pris en negatif pour le calcul des pertes VD.ACM2
VD.GE.qacm2=interp2(VD.ACM2.Cmot_pert,VD.ACM2.Regmot_pert',VD.ACM2.Pert_mot,-VD.GE.Cpl_carto,VD.GE.Reg_carto');

% matrice des puissances mecanique sur les points de la cartographie
[Cpl,Reg]=meshgrid(VD.GE.Cpl_carto,VD.GE.Reg_carto);
VD.GE.P_meca=Cpl.*Reg;

% expression de la carto moteur en Watt
VD.GE.Conso_2d=interp2(VD.MOTH.Cpl_2dconso,VD.MOTH.Reg_2dconso',VD.MOTH.Conso_2d,VD.GE.Cpl_carto,VD.GE.Reg_carto');
VD.GE.Conso_2dWatt=VD.GE.Conso_2d.*VD.MOTH.pci;

% carto des pertes moteur thermique
VD.GE.Pert_therm=VD.GE.Conso_2dWatt-VD.GE.P_meca;

% carto des perte du groupe
VD.GE.Pert_group=VD.GE.Pert_therm+VD.GE.qacm2;

% courbe enveloppe

% couple min electrique reechantillonne sur regime max moteur thermique
Cpl_pert_min=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,size(VD.ACM2.Tension_cont,2)),VD.MOTH.wmt_max);

%  courbe enveloppe du groupe generateur
VD.GE.cpl_max=min(-Cpl_pert_min,VD.MOTH.cmt_max);
VD.GE.reg_max=VD.MOTH.wmt_max;
% carto puissance electrique du groupe
VD.GE.Pelec_group=VD.GE.P_meca-VD.GE.qacm2;

%% recherche courbe optimum perte

[pmt_opti wmt_opti cmt_opti]=puissance_opti_carto(VD.GE.Cpl_carto,VD.GE.Reg_carto',VD.GE.Pelec_group,VD.GE.Pert_group,nb_iso,nb_pt_iso,VD.GE.cpl_max,VD.GE.reg_max,VD.MOTH.ral);

VD.GE.pge_opti=pmt_opti;
VD.GE.wmt_opti=wmt_opti;%(index);
VD.GE.cmt_opti=cmt_opti;%(index);
VD.GE.dcarb_opti=interp2(VD.MOTH.Cpl_2dconso,VD.MOTH.Reg_2dconso,VD.MOTH.Conso_2d,VD.GE.cmt_opti,VD.GE.wmt_opti);
VD.GE.qacm2_opti=interp2(VD.GE.Cpl_carto,VD.GE.Reg_carto,VD.GE.qacm2,VD.GE.cmt_opti,VD.GE.wmt_opti);

VD.GE.rendement=VD.GE.Pelec_group./VD.GE.Conso_2dWatt;

if isfield(VD.MOTH,'nom') && isfield(VD.ACM2,'nom')
    VD.GE.nom=['groupe electrogene : ',num2str(round(max(VD.GE.pge_opti./1000))),' kW : ',regexprep(VD.MOTH.nom,'_',' '),' et ',regexprep(VD.ACM2.nom,'_',' ')];
elseif isfield(VD.MOTH,'nom')
    VD.GE.nom=['groupe electrogene : ',num2str(round(max(VD.GE.pge_opti./1000))),' kW : ',regexprep(VD.MOTH.nom,'_',' ')];
elseif isfield(VD.ACM2,'nom')
    VD.GE.nom=['groupe electrogene : ',num2str(round(max(VD.GE.pge_opti./1000))),' kW : ',regexprep(VD.ACM2.nom,'_',' ')];
else
    VD.GE.nom=['groupe electrogene : ',num2str(round(max(VD.GE.pge_opti./1000))),' kW  '];
end
    

%% trace de la courbe
if trace==1
    figure
    plot(VD.MOTH.wmt_max*30/pi,-Cpl_pert_min,...
        VD.MOTH.wmt_max*30/pi,VD.MOTH.cmt_max,...
        VD.MOTH.wmt_max*30/pi,VD.GE.cpl_max,...
        VD.GE.wmt_opti*30/pi,VD.GE.cmt_opti,':+')
    hold on
    val=VD.GE.rendement;
    for ii=1:length(VD.GE.Reg_carto)
        ligneval=val(ii,:);
        ligneval(VD.GE.Cpl_carto>interp1(VD.GE.reg_max,VD.GE.cpl_max,VD.GE.Reg_carto(ii)))=NaN;
        val(ii,:)=ligneval;
    end
    [c,h]=contour(VD.GE.Reg_carto*30/pi,VD.GE.Cpl_carto,val'.*100,20:2.5:50);
    clabel(c,h);
    grid
    hold off
    
    title(['rendement ',VD.GE.nom])
    xlabel('regime tr/min')
    ylabel('couple moteur thermique Nm')
    legend('enveloppe motelec','enveloppe mtherm','enveloppe groupe','courbe optimum','location','NorthWest')
end