function [ERR,grandeurFinale,ResXml] = call_fct_forward(fct_forward, lambda, vehlib, VD)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
ERR=struct([]);

% Affectation du parametre de convergence
eval(['VD.ECU.lambda1=',num2str(lambda,16),';']);

assignin('base','VD',VD);
assignin('base','vehlib',vehlib);

% Calcul
evalin('base',fct_forward);
% Afectation du critere de retour
%grandeurFinale=soc;
ResXml=miseEnForme4VEHLIB(vehlib,'base');
grandeurFinale=XmlValue(ResXml,'soc');
grandeurFinale=grandeurFinale(end);
end

