function [ERR,grandeurFinale,ResXml] = call_fct_backward(fct_backward,param,lambda)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% Affectation du parametre de convergence
% Par exemple param.paramCvgce='param.lambda1';
eval([param.paramCvgce,'=',num2str(lambda,16),';']);
% Calcul
% Trois argument de retour des fonctions calc_XXX

[ERR,VD,ResXml]=fct_backward(param);
if ~isempty(ERR)
    grandeurFinale=NaN;
    ResXml=struct([]);
    return
end
% Afectation du critere de retour
% exemple :  param.fonctionCvgce='soc=XmlValue(ResXml,'soc');'
eval(param.fonctionCvgce);
grandeurFinale=eval(param.critereCvgce);
end

