% Function: fitcarto
%
% fitcarto est un utilitaire permettant de "fitter" des cartographies
%
% selon un modele du type :
%
% Z = c1*C^2.N^2 + c2*C.N^2 + c3*N^2 + c4*C^2.N + c5*C.N + c6*N + c7*C^2 + c8*C + c9
%
%   Copyright IFSTTAR LTE 1999-2012
%
%
% Objet:
%
% L'utilitaire fitcarto permet de "fitter" une cartographie afin
%
% d'approcher au mieux un jeu de points de mesure par une surface dont
%
% l'equation est connue et de la forme ci-dessus. Cet utilitaire fonctionne
%
% avec des donnees d'entree sous forme de fichier texte, ou bien de fichier
%
% composant VEHLIB type VD.ACM1, VD.ACM2 ou VD.MOTH.
%
% Arguments d'appel:
%
% input = string ou structure VEHLIB, donnees sources. Peut etre un fichier
%
% texte avec une ou deux lignes d'entetes, ou un fichier composant VEHLIB
%
% type VD.ACM1, VD.ACM2 ou VD.MOTH. Si non renseigne, une fenetre d'ouverture de fichier
%
% apparaitra.
%
% verbose = scalaire, gestion de l'affichage
%
%               - 0 (defaut) pour ne rien afficher
%
%               - 1 pour afficher les points de mesure et la surface fittee
%
%               - 2 pour afficher les ecarts absolus et en pourcent des points
%
%                 de mesure par rapport a la surface.
%
% fitmod = scalaire, choix de l'equation utilisee pour fitter
%
%               - 0 pour utiliser un masque (interactif si non-specifie)
%
%               - 1 (defaut) pour utiliser l'equation sans terme constant
%
% ecr = scalaire, gestion de l'ecriture
%
%               - 0 (defaut) ne rien ecrire
%
%               - 1 pour ecrire la carto dans un fichier composant type VEHLIB.
%
% masque = vecteur (1,n), permet de modifier l'equation utilisee pour fitter
%
%           la carto. Par exemple, masque = [1 1 1 1 1 1 1 1 0] supprimera
%
%           le terme constant de l'equation de fittage (eq. a fitmod = 1).
%
%           N'est utilise que si fitmod = 0.
%
% Resultats:
%
% output = structure type VEHLIB, a laquelle s'ajoute l'equation et les coeffs
%
% ec_moy = scalaire, ecart moyen (VD.ECUUL A VERIFIER)
%
% err_moy = scalaire, erreur moyenne (VD.ECUUL A VERIFIER)
%
% Exemples d'appel:
% (start code)
% VD.ACM1 = fitcarto('isos_LS.txt')
%
% VD.ACM1 = fitcarto(VD.ACM1, 0, 0, 0, [0 1 1 1 1 1 1 1 1])
%
% [VD.ACM1, ec_moy, err_moy] = fitcarto(VD.ACM1, 2, 0)
%
% fitcarto
% (end)
%
% -------------------------------------------------------------------------

function [output, ec_moy, err_moy] = fitcarto(input, verbose, fitmod, ecr, masque)

% Cas pas assez d'arguments specifies
if nargin == 0
    input = uigetfile('*.*');
    fitmod = 1;
    verbose = 1;
    ecr = 0;
elseif nargin == 1
    fitmod = 1;
    verbose = 1;
    ecr = 0;
elseif nargin == 2
    fitmod = 1;
    ecr = 0;
elseif nargin == 3
    ecr = 0;
end

% Cas nom fichier laisse vide
if isempty(input)
    input = uigetfile('*.*');
end

%% PARAM
n_pts_Cmax = 2;
n_pts_Cneg = 10;

%% Importation des donnees
% 'input' peut etre une structure ou un nom de fichier. Quelque soit son
% type, il faut recuperer une structure de type VD.MOTH, VD.ACM1 ou VD.ACM2, ou un
% fichier texte de donnees. VD.ACM1 ou VD.ACM2 seront consideres comme etant ACM.

if isstruct(input)
    % Cas input est une structure
    if isfield(input,'Pert_mot')
        % ACM1 et ACM2 sont supposes avoir un champ 'Pert_mot'
        ACM = input;
    elseif isfield(input,'Conso_2d')
        % MOTH est suppose avoir un champ 'Conso_2d'
        MOTH = input;
    else
        % Gestion de l'erreur : aucune structure utilisable reconnue
        ERR = MException('BackwardModel:fitCarto',['La structure ''input'' n''est pas reconnue.' ...
            ' Seules certains composants VEHLIB de type ACM ou MOTH sont acceptes.']);
        throw(ERR);
    end
else
    % Cas input est un nom de fichier
    
    % Separation nom/extension
    ext = regexp(input,'\.','split');
    
    % Gestion de l'import selon l'extension (*.txt, *.m, *)
    if strcmp(ext{end},'txt')
        % Fichiers textes respectant la mise en forme adequate
        
        % ATTENTION : s'il y a plusieurs fichiers du meme nom que le
        % fichier choisi, Matlab pourrait importer les donnees d'un autre
        % fichier a votre insu (ex : un isos.txt par dossier moteur). De
        % plus, une erreur sera genere si le fichier choisi n'est pas dans
        % le path Matlab

        % 1 ou 2 lignes d'entete ?
        data = importdata(input,'\t',1);
        if iscell(data)
            % S'il n'y avait eu qu'une ligne d'entete, 'data' serait une structure
            data = importdata(input,'\t',2);
        end
        for i = 1:length(data.colheaders)
            eval([data.colheaders{i} '= data.data(:,i);']);
        end
        
        button = questdlg('Quelles pertes voulez-vous etudier ?','Choix des pertes etudiees',...
            'Moteur + Onduleur','Moteur','Onduleur','Moteur + Onduleur');
        
        if strcmp(button,'Moteur + Onduleur')
            % Pertes du groupe moteur + controle
            % (P_6 negative et en W correspond a P DC dans donnees CEA)
            Pertes_W = (-P_6) - 1000*Puissance_Moteur_Brute;                % [W]
        elseif strcmp(button,'Moteur')
            % Pertes du moteur
            % (P_SigmaB negative et en W correspond a P Active AC dans donnees
            % CEA)
            Pertes_W = (-P_SigmaB) - 1000*Puissance_Moteur_Brute;           % [W]
        elseif strcmp(button,'Onduleur')
            % Pertes de la partie controle
            % (P_6 negative et en W correspond a P DC dans donnees CEA)
            % (P_SigmaB negative et en W correspond a P Active AC dans donnees
            % CEA)
            Pertes_W = (-P_6) - (-P_SigmaB);                               	% [W]
        end
        
        % Recherche de la courbe de couple max
        %   Arrondi du regime moteur a la centaine pres
        Vitesse_moteur_lisse = 100 * round(Vitesse_moteur/100);             % [tr/min]
        %   Classes de regime
        Regmot_cmax = [];                                                   % [tr/min]
        for i = 0:100:max(Vitesse_moteur_lisse)
            if length(find(Vitesse_moteur_lisse == i)) >= n_pts_Cmax
                Regmot_cmax = [Regmot_cmax i];                              % [tr/min]
            end
        end
        %   Couple max et min sur chaque classe de regime
        Cmax_mot = [];                                                      % [N.m]
        for i = 1:length(Regmot_cmax)
            cond = (Vitesse_moteur_lisse == Regmot_cmax(i)) & (Couple_mesure_MP60 > 0);
            if isempty(Couple_mesure_MP60(cond))
                Cmax_mot = [Cmax_mot NaN];                                  % [N.m]
            else
                Cmax_mot = [Cmax_mot max(ceil(Couple_mesure_MP60(cond)))]; % [N.m]
            end
        end
        % Filtrage des eventuels NaN par interpolation lineaire
        Cmax_mot = interp1(Regmot_cmax(~isnan(Cmax_mot)),Cmax_mot(~isnan(Cmax_mot)),Regmot_cmax,'linear','extrap');
        %   Y a t'il des enregistrement a couple negatif ? (au moins dix mesures)
        if length(Couple_mesure_MP60(Couple_mesure_MP60 < 0)) > n_pts_Cneg
            Cmin_mot = [];                                                  % [N.m]
            for i = 1:length(Regmot_cmax)
                cond = (Vitesse_moteur_lisse == Regmot_cmax(i)) & (Couple_mesure_MP60 < 0);
                if isempty(Couple_mesure_MP60(cond))
                    Cmin_mot = [Cmin_mot NaN];                              % [N.m]
                else
                    Cmin_mot = [Cmin_mot min(floor(Couple_mesure_MP60(cond)))]; % [N.m]
                end
            end
            % Filtrage des eventuels NaN par interpolation lineaire
            Cmin_mot = interp1(Regmot_cmax(~isnan(Cmin_mot)),Cmin_mot(~isnan(Cmin_mot)),Regmot_cmax,'linear','extrap');
        else
            Cmin_mot = -Cmax_mot;                                           % [N.m]
        end
        Regmot_cmax = Regmot_cmax*pi/30;                                    % [rad/s]
        
        % Standardisation des donnees
        Cple_Nm = Couple_mesure_MP60;                                       % [N.m]
        Reg_rads = Vitesse_moteur * pi /30;                                 % [rad/s]
        
        % Exclusion des points de fonctionnement superieur au couple max
        Cmax = interp1(Regmot_cmax,Cmax_mot,Reg_rads);                      % [N.m]
        Reg_rads = Reg_rads(Cple_Nm <= Cmax);                               % [rad/s]
        Pertes_W = Pertes_W(Cple_Nm <= Cmax);                               % [W]
        Cple_Nm = Cple_Nm(Cple_Nm <= Cmax);                                 % [N.m]
        
        Cmin = interp1(Regmot_cmax,Cmin_mot,Reg_rads);                      % [N.m]
        Reg_rads = Reg_rads(Cple_Nm >= Cmin);                               % [rad/s]
        Pertes_W = Pertes_W(Cple_Nm >= Cmin);                               % [W]
        Cple_Nm = Cple_Nm(Cple_Nm >= Cmin);                                 % [N.m]
        
        Z = Pertes_W;                                                       % [W]
        
    elseif strcmp(ext{end},'m') || strcmp(ext{end},ext{1})
        % Fichiers Matlab, ainsi que fichiers sans extension specifies
        % consideres comme etant des fichiers Matlab
        
        try
            eval(ext{1});
        catch ME
            % Identification du message d'erreur
            idSegLast = regexp(ME.identifier, '(?<=:)\w+$', 'match');
            % Gestion du cas ou le fichier sans extension n'etait pas un *.m
            if strcmp(idSegLast,'UndefinedFunction') && strcmp(ext{end},ext{1})
                ERR = MException('BackwardModel:fitCarto:FileNotFound',['Sans extension specifie, le fichier est considere' ...
                    ' comme etant un *.m, or le fichier ''%s.m'' n''existe pas.'],ext{1});
            end
            throw(addCause(ERR,ME));
        end
%         
%         % Standardisation des structures
%         if exist('VD.ACM1','var')
%             ACM = VD.ACM1;
%         elseif exist('VD.ACM2','var')
%             ACM = VD.ACM2;
%         elseif exist('VD.MOTH','var')
%             % OK
%         else
%             % Gestion de l'erreur : aucune structure utilisable reconnue
%             ERR = MException('BackwardModel:fitCarto',['Aucune structure VD.ACM1, VD.ACM2 ou VD.MOTH n''a' ...
%                 ' ete trouve dans le fichier ''%s.m''.'],ext{1});
%             throw(ERR);
%         end
    else
        % Gestion de l'erreur : aucun fichier utilisable reconnu
        ERR = MException('BackwardModel:fitCarto',['Ce type de fichier (*.%s)' ...
            ' n''est pas pris en charge.'],ext{end});
        throw(ERR);
    end
end

% Si 'input' ne correspondait pas a un fichier de donnees sous format
% texte, on devrait avoir une structure ACM ou MOTH dans le workspace. On
% calcule alors les donnees necessaires au fittage.
if exist('ACM','var')
    % Import des donnees d'un moteur electrique, et standardisation
    Cple_Nm = repmat(ACM.Cmot_pert', size(ACM.Regmot_pert));                % [N.m]
    Reg_rads = repmat(ACM.Regmot_pert, size(ACM.Cmot_pert'));               % [rad/s]
    Pertes_W = interp2(ACM.Cmot_pert,ACM.Regmot_pert,ACM.Pert_mot,Cple_Nm,Reg_rads); % [W]
    
    Cple_Nm = Cple_Nm(:);                                                   % [N.m]
    Reg_rads = Reg_rads(:);                                                 % [rad/s]
    Pertes_W = Pertes_W(:);                                                 % [W]
    
    % Exclusion des points de fonctionnement superieur au couple max
    Cmax = interp1(ACM.Regmot_cmax,ACM.Cmax_mot(:,end),Reg_rads);           % [N.m]
    Reg_rads = Reg_rads(Cple_Nm <= Cmax);                                   % [rad/s]
    Pertes_W = Pertes_W(Cple_Nm <= Cmax);                                   % [W]
    Cple_Nm = Cple_Nm(Cple_Nm <= Cmax);                                     % [N.m]
    
    Cmin = interp1(ACM.Regmot_cmax,ACM.Cmin_mot(:,end),Reg_rads);           % [N.m]
    Reg_rads = Reg_rads(Cple_Nm >= Cmin);                                   % [rad/s]
    Pertes_W = Pertes_W(Cple_Nm >= Cmin);                                   % [W]
    Cple_Nm = Cple_Nm(Cple_Nm >= Cmin);                                     % [N.m]
    
    Z = Pertes_W;                                                           % [W]
    
elseif exist('MOTH','var')
    % Import des donnees d'un moteur thermique, et standardisation
    Cple_Nm = repmat(MOTH.Cpl_2dconso',size(MOTH.Reg_2dconso(MOTH.Reg_2dconso >= MOTH.ral_min)));   % [N.m]
    Reg_rads = repmat(MOTH.Reg_2dconso(MOTH.Reg_2dconso >= MOTH.ral_min),size(MOTH.Cpl_2dconso')); 	% [rad/s]
    Conso_gs = interp2(MOTH.Cpl_2dconso,MOTH.Reg_2dconso,MOTH.Conso_2d,Cple_Nm,Reg_rads);         	% [g/s]
    
    Cple_Nm = Cple_Nm(:);                                                   % [N.m]
    Reg_rads = Reg_rads(:);                                                 % [rad/s]
    Conso_gs = Conso_gs(:);                                                 % [g/s]
    
    % Exclusion des points de fonctionnement superieur au couple max
    Cmax = interp1(MOTH.wmt_max,MOTH.cmt_max,Reg_rads);                     % [N.m]
    Reg_rads = Reg_rads(Cple_Nm <= Cmax);                                   % [rad/s]
    Conso_gs = Conso_gs(Cple_Nm <= Cmax);                                   % [g/s]
    Cple_Nm = Cple_Nm(Cple_Nm <= Cmax);                                     % [N.m]
    
    Cmin = interp1(MOTH.wmt_max,MOTH.cmt_min,Reg_rads);                     % [N.m]
    Reg_rads = Reg_rads(Cple_Nm >= Cmin);                                   % [rad/s]
    Conso_gs = Conso_gs(Cple_Nm >= Cmin);                                   % [g/s]
    Cple_Nm = Cple_Nm(Cple_Nm >= Cmin);                                     % [N.m]
    
    Z = Conso_gs;                                                           % [g/s]
end

%% Fittage de la cartographie
% On cherche a approximer les pertes ou la consommation par un polynome de
% degre 4 fonction du couple et du regime moteur :
% Z = (c1*C^2 + c2*C + c3)*N^2 + (c4*C^2 + c5*C + c6)*N + (c7*C^2 + c8*C + c9)
% Pour trouver les coefficients, on resout le calcul matriciel suivant :
%
% [ Z1 ] = [C^2.N^2 C.N^2 N^2 C^2.N C.N N C^2 C 1 ] * [ c1 ]
% | .  |   |  .       .    .    .    .  .  .  . . |   | c2 |
% | .  |   |  .       .    .    .    .  .  .  . . |   | c3 |
% | .  |   |  .       .    .    .    .  .  .  . . |   | c4 |
% | .  |   |  .       .    .    .    .  .  .  . . |   | c5 |
% | .  |   |  .       .    .    .    .  .  .  . . |   | c6 |
% | .  |   |  .       .    .    .    .  .  .  . . |   | c7 |
% | .  |   |  .       .    .    .    .  .  .  . . |   | c8 |
% [ Zn ]   [C^2.N^2 C.N^2 N^2 C^2.N C.N N C^2 C 1 ]   [ c9 ]
%
%   Z    =     system     *    coeffs

% Gestion choix du mode de fittage
switch fitmod
    case 0
        % Forme complete de l'equation
        system = [  Cple_Nm.^2.*Reg_rads.^2, 	Cple_Nm.*Reg_rads.^2, ...
            Reg_rads.^2,            	Cple_Nm.^2.*Reg_rads, ...
            Cple_Nm.*Reg_rads,       	Reg_rads, ...
            Cple_Nm.^2,                 Cple_Nm,	ones(size(Cple_Nm))];
        % Choix interactif du masque, s'il n'a pas ete specifie en entree
        if ~exist('masque','var')
            masque = repmat(GUI_fitcarto(), size(system,1), 1);
        else
            masque = repmat(masque, size(system,1), 1);
        end
        % Application du masque
        system = system.*masque;
    case 1
        % Modele sans le terme constant
        system = [  Cple_Nm.^2.*Reg_rads.^2, 	Cple_Nm.*Reg_rads.^2, ...
            Reg_rads.^2,            	Cple_Nm.^2.*Reg_rads, ...
            Cple_Nm.*Reg_rads,       	Reg_rads, ...
            Cple_Nm.^2,                 Cple_Nm];
    otherwise
        % Gestion de l'erreur : choix 'fitmod' invalide
        ERR = MException('BackwardModel:fitCarto','Choix ''fitmod'' invalide.');
        throw(ERR);
end


% La fonction '\' de Matlab cherche la solution la plus proche selon la
% methode des moindres carres dans le cas ou elle ne trouve pas de solution
% exacte
Inan=find(isnan(Z));
Z(Inan)=[];
system(Inan,:)=[];
Cple_Nm(Inan,:)=[];
Reg_rads(Inan,:)=[];
coeffs = system\Z;

% Maillage de la zone de fonctionnement
Nx = 0:25:50*ceil(max(Reg_rads)/50);                                        % [rad/s]
if exist('button','var')
    % 'button' n'existe que dans le cas d'un fichier texte en entree
    % On ne cadrille que sur la zone de mesure
    Cy = min(0, 5*floor(min(Cple_Nm)/5)):2.5:5*ceil(max(Cple_Nm)/5);        % [N.m]
else
    Cy = 5*floor(min(Cmin)/5):2.5:5*ceil(max(Cmax)/5);                      % [N.m]
end
Nxmat = repmat(Nx,size(Cy'));                                               % [rad/s]
Cymat = repmat(Cy',size(Nx));                                               % [N.m]
% Exclusion des points de fonctionnement superieur au couple max et
% inferieur au couple min, sauf si l'on veut ecrire la carto, car dans ce
% cas on veut eviter d'avoir des NaN
if ecr == 0
    if exist('button','var')
        % 'button' n'existe que dans le cas d'un fichier texte en entree
        Cmaxmat = interp1(Regmot_cmax,Cmax_mot,Nxmat);                          % [N.m]
        Cymat(Cymat > Cmaxmat) = NaN;
        Cminmat = interp1(Regmot_cmax,Cmin_mot,Nxmat);                          % [N.m]
        Cymat(Cymat < Cminmat) = NaN;
    elseif exist('ACM','var')
        Cmaxmat = interp1(ACM.Regmot_cmax,ACM.Cmax_mot(:,end),Nxmat,'linear','extrap');           % [N.m]
        Cymat(Cymat > Cmaxmat) = NaN;
        Cminmat = interp1(ACM.Regmot_cmax,ACM.Cmin_mot(:,end),Nxmat,'linear','extrap');           % [N.m]
        Cymat(Cymat < Cminmat) = NaN;
    elseif exist('MOTH','var')
        Cmaxmat = interp1(MOTH.wmt_max,MOTH.cmt_max,Nxmat);                     % [N.m]
        Cymat(Cymat > Cmaxmat) = NaN;
    end
end

% Calcul des pertes ou de la conso sur les points du maillage
switch fitmod
    case 0
        Zmat = coeffs(1)*Cymat.^2.*Nxmat.^2 + coeffs(2)*Cymat.*Nxmat.^2 + coeffs(3)*Nxmat.^2 + ...
            coeffs(4)*Cymat.^2.*Nxmat + coeffs(5)*Cymat.*Nxmat + coeffs(6)*Nxmat + ...
            coeffs(7)*Cymat.^2 + coeffs(8)*Cymat + coeffs(9);               % [W] ou [g/s]
    case 1
        Zmat = coeffs(1)*Cymat.^2.*Nxmat.^2 + coeffs(2)*Cymat.*Nxmat.^2 + coeffs(3)*Nxmat.^2 + ...
            coeffs(4)*Cymat.^2.*Nxmat + coeffs(5)*Cymat.*Nxmat + coeffs(6)*Nxmat + ...
            coeffs(7)*Cymat.^2 + coeffs(8)*Cymat;                           % [W] ou [g/s]
    otherwise
        % Gestion de l'erreur : choix 'fitmod' invalide
        ERR = MException('BackwardModel:fitCarto','Choix ''fitmod'' invalide.');
        throw(ERR);
end

% Calcul d'erreur
Z_app = system*coeffs;                                                      % [W] ou [g/s]
ec_moy = sqrt(sum((Z_app - Z).^2)/length(Z));
err_moy = 100*mean(abs(Z_app(Z~=0) - Z(Z~=0))./Z(Z~=0));
ec_pct = 100 * abs(Z - Z_app)./ Z;

%% Affichage dans le workspace de l'equation
termes = {'C^2.N^2' 'C.N^2' 'N^2' 'C^2.N' 'C.N' 'N' 'C^2' 'C' '1'};
nom_coeffs = {'c1' 'c2' 'c3' 'c4' 'c5' 'c6' 'c7' 'c8' 'c9'};
format_coeffs = '%4.2g';

equation = '';
template_eq = '';

% Creation de l'equation et du template
for j = 1:length(coeffs)
    if coeffs(j)
        if isempty(equation)
            equation = ['Z = ' nom_coeffs{j} '*' termes{j}];
            template_eq = ['Z = ' format_coeffs '*' termes{j}];
        else
            equation = [equation ' + ' nom_coeffs{j} '*' termes{j}];
            template_eq = [template_eq ' + ' format_coeffs '*' termes{j}];
        end
    end
end

% Affichage
if verbose >= 1
    fprintf([template_eq '\n'], coeffs(coeffs~=0));
end

%% Representation graphique optionnelle
if verbose >= 1
    figure(1)
    clf
    hold on
    surf(Nxmat*30/pi, Cymat, Zmat)
    scatter3(Reg_rads*30/pi, Cple_Nm, Z, 'r')
    xlabel('Regime [tr/min]')
    ylabel('Couple [N.m]')
    if exist('ACM','var') || exist('button','var')
        zlabel('Pertes [W]')
    elseif exist('MOTH','var')
        zlabel('Conso [g/s]')
    end
    title(sprintf(template_eq,coeffs(coeffs~=0)))
    grid on
    hold off
end

if verbose >= 2
    figure(2)
    clf
    scatter3(Reg_rads*30/pi, Cple_Nm, ec_pct, 'r')
    xlabel('Regime [tr/min]')
    ylabel('Couple [N.m]')
    zlabel('Ecart [%]')
    title('Ecart par rapport a la mesure en %')
    grid on
    
    figure(3)
    clf
    scatter3(Reg_rads*30/pi, Cple_Nm, abs(Z - Z_app), 'r')
    xlabel('Regime [tr/min]')
    ylabel('Couple [N.m]')
    if exist('ACM','var') || exist('button','var')
        zlabel('Ecart [W]')
    elseif exist('MOTH','var')
        zlabel('Ecart [g/s]')
    end
    title('Ecart absolu par rapport a la mesure')
    grid on
end

%% Stockage resultats

if exist('button','var')
    % 'button' n'existe que dans le cas d'un fichier texte en entree
    
    if strcmp(button,'Moteur + Onduleur') || strcmp(button,'Moteur')
        % Si le cadrant generateur n'existe pas, on le cree
        if min(Cy) >= 0
            % Carto regeneration obtenue par symetrisation du rendement
            Regmot_pert = Nx;                                               % [rad/s]
            Cmot_pert = [-fliplr(Cy(2:end)) 0 Cy(2:end)];                   % [N.m]
            Pmeca = Cy' * Nx;                                               % [W]
            Pelec = Pmeca + Zmat;                                           % [W]
            Rend  = Pmeca ./ Pelec;
            Pertes_neg = Pmeca - (Pmeca .* Rend);                           % [W]
            Pert_mot   = [flipud(Pertes_neg(2:end,:)); Zmat];               % [W]
        else
            Regmot_pert = Nx;                                               % [rad/s]
            Cmot_pert = Cy;                                                 % [N.m]
            Pert_mot = Zmat;                                                % [W]
        end
        
        % Creation structure composant VD.ACM1
        VD.ACM1.ntypmg      = 10;
        VD.ACM1.Regmot_pert = Regmot_pert;
        VD.ACM1.Cmot_pert   = Cmot_pert;
        VD.ACM1.Pert_mot    = Pert_mot';
        
        VD.ACM1.Cpl_frot = zeros(size(VD.ACM1.Regmot_pert));                      % HYP [N.m]
        
        VD.ACM1.J_mg     = 1e-2;                                               % HYP [kg.m^2]
        VD.ACM1.Masse_mg = 25;                                                 % HYP [kg]
        
        VD.ACM1.Tension_cont = [24 48 72];                                     % HYP [V]
        
        VD.ACM1.Regmot_cmax = Regmot_cmax;                                     % [rad/s]
        VD.ACM1.Cmax_mot    = ones(3,1) * Cmax_mot;                            % [N.m]
        VD.ACM1.Cmin_mot    = ones(3,1) * Cmin_mot;                            % [N.m]
        
        VD.ACM1.grad_cmg_mont  = 3000;
        VD.ACM1.grad_cmg_desc  = -3000;
        
    elseif strcmp(button,'Onduleur')
        % Si le cadrant generateur n'existe pas, on le cree
        if min(Cy) >= 0
            % Carto regeneration obtenue par symetrisation des pertes
            Regmot_pert = Nx;                                               % [rad/s]
            Cmot_pert = [-fliplr(Cy(2:end)) 0 Cy(2:end)];                   % [N.m]
            Pert_ond  = [flipud(Zmat(2:end,:)); Zmat];                      % [W]
        else
            Regmot_pert = Nx;                                               % [rad/s]
            Cmot_pert = Cy;                                                 % [N.m]
            Pert_ond = Zmat;                                                % [W]
        end
        
        VD.ACM1.Regmot_pert = Regmot_pert;                                     % [rad/s]
        VD.ACM1.Cmot_pert   = Cmot_pert;                                       % [N.m]
        
        VD.ACM1.Pert_ond    = Pert_ond';                                       % [W]
    end
    
    % Stockage de l'equation utilisee pour le fittage et des coeffs
    VD.ACM1.equation = equation;
    for c = 1:length(coeffs)
        if coeffs(c)
            VD.ACM1.(nom_coeffs{c}) = coeffs(c);
        end
    end
    
    % Creation structure resultat
    output = VD.ACM1;
    
elseif exist('ACM','var')    
    % Stockage de l'equation utilisee pour le fittage et des coeffs
    ACM.equation = equation;
    for c = 1:length(coeffs)
        if coeffs(c)
            ACM.(nom_coeffs{c}) = coeffs(c);
        end
    end
    
    % Creation structure resultat
    output = ACM;
    
elseif exist('MOTH','var')
    % Stockage de l'equation utilisee pour le fittage et des coeffs
    MOTH.equation = equation;
    for c = 1:length(coeffs)
        if coeffs(c)
            MOTH.(nom_coeffs{c}) = coeffs(c);
        end
    end
    
    % Creation structure resultat
    output = MOTH;
end

%% Ecriture du fichier composant VEHLIB

if ecr == 1
    % Nom du fichier composant qui sera cree
    [filename,path] = uiputfile('*.m','Save VEHLIB Component file as');
    
    % ATTENTION : les donnees issues d'un fichier texte ont ete stockees
    % dans une structure 'VD.ACM1'
    if exist('button','var')
        % 'button' n'existe que dans le cas d'un fichier texte en entree
        if strcmp(button,'Moteur + Onduleur') || strcmp(button,'Moteur')
            % Appel du script d'ecriture de fichier
            ecrit_carto_motelec([path filename],VD.ACM1);
        elseif strcmp(button,'Onduleur')
            % Appel du script d'ecriture de fichier
            ecrit_carto_onduleur([path filename],VD.ACM1);
        end
    elseif exist('ACM','var')
        % Appel du script d'ecriture de fichier
        ecrit_carto_motelec([path filename],ACM);
        
    elseif exist('MOTH','var')
        % Appel du script d'ecriture de fichier
        ecrit_carto_mth([path filename],MOTH);
    end
end

end
