% function [ERR,ACM]=inverse_carto_melec(ACM,verbose)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet
% Inversion de carto pour passer d'une carto de pertes(vitesse, couple)
% a une carto pertes(vitesse, puissance_elec).
%
% Arguments appel :
% Arguments de sortie :
%
% 15_10_10 (EV): creation


function [ERR,ACM]=inverse_carto_melec(ACM,verbose)

ERR=[ ];

if nargin == 1
    verbose = 0;
end

P_elec=ACM.Pert_mot+ACM.Regmot_pert'*ACM.Cmot_pert;

Pertes_p_elec_max=interp2(ACM.Cmot_pert,ACM.Regmot_pert,ACM.Pert_mot,ACM.Cmax_mot,repmat(ACM.Regmot_cmax',1,length(ACM.Cmax_mot(1,:))));
Pertes_p_elec_max(isnan(Pertes_p_elec_max))=0;

Pertes_p_elec_min=interp2(ACM.Cmot_pert,ACM.Regmot_pert,ACM.Pert_mot,ACM.Cmin_mot,repmat(ACM.Regmot_cmax',1,length(ACM.Cmax_mot(1,:))));
Pertes_p_elec_min(isnan(Pertes_p_elec_min))=0;


P_elec_max=max(max(ACM.Cmax_mot.*repmat(ACM.Regmot_cmax',1,length(ACM.Cmax_mot(1,:)))+Pertes_p_elec_max));
P_elec_min=min(min(ACM.Cmin_mot.*repmat(ACM.Regmot_cmax',1,length(ACM.Cmax_mot(1,:)))+Pertes_p_elec_min));


ACM.Pelec_max=ACM.Cmax_mot.*repmat(ACM.Regmot_cmax',1,length(ACM.Cmax_mot(1,:)))+Pertes_p_elec_max;
ACM.Pelec_min=ACM.Cmin_mot.*repmat(ACM.Regmot_cmax',1,length(ACM.Cmax_mot(1,:)))+Pertes_p_elec_min; 

ACM.Pelec_min(ACM.Pelec_min>0)=0;

ACM.Pelec_min(Pertes_p_elec_min==0)=0;
ACM.Pelec_max(Pertes_p_elec_max==0)=0;


ech=ceil(length(ACM.Regmot_cmax)*(-P_elec_min/P_elec_max)); % on essaye d'achantilloner ACM.Pelec_pert de façon a peu pres reguliere
                                                             % et avec autant équivaleente à ACM.Regmot_cmax
if P_elec_min~=0
    ACM.Pelec_pert=P_elec_min:-P_elec_min/ech:0;
else
    ACM.Pelec_pert=0;
end
ACM.Pelec_pert=[ACM.Pelec_pert P_elec_max/length(ACM.Regmot_cmax):P_elec_max/length(ACM.Regmot_cmax):P_elec_max];


%interpollation dans chaque colonne
for jj=1:length(ACM.Regmot_pert)  
    if jj==1 % la premiere ligne a vitesse nulle doit etre traitee a part a cause des possibles valeurs identiques dans la table ACM.Pert_mot
        P_elec1=P_elec(jj,:);
        Pert_mot1=ACM.Pert_mot(jj,:);
        for ii=1:1:length(P_elec(jj,:))
            if ii<=length(P_elec1)
                ind=find(P_elec1==P_elec1(ii));
                if length(ind)>1
                    P_elec1(ind(2:end))=[];
                    Pert_mot1(ind(2:end))=[];
                end
            end
        end
        Pertes_grid(1:length(ACM.Pelec_pert),jj)=interp1(P_elec1,Pert_mot1,ACM.Pelec_pert);
        ind=find(isnan(Pertes_grid));
        Pertes_grid(ind)=max(Pertes_grid(1:length(ACM.Pelec_pert),jj));
    else
        Pertes_grid(1:length(ACM.Pelec_pert),jj)=interp1(P_elec(jj,:),ACM.Pert_mot(jj,:),ACM.Pelec_pert);
        ind=find(isnan(Pertes_grid));
        Pertes_grid(ind)=max(Pertes_grid(1:length(ACM.Pelec_pert),jj));
    end
end

ACM.Pertes_pelec=Pertes_grid;

ACM.Regmot_pelec_max=ACM.Regmot_cmax;

if verbose==1
    gr_melec_pelec(ACM,0,0)
end
