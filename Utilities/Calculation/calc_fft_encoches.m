T=2*pi;
% fonction de "remplissage"
X=-T/2:T/12:T/2+T/12;
X=X-T/24;

Y=[-1 -1 0 0 0 0 1 1 0 0 0 0 -1 -1 ];

% forme des courant
X=-T/2:T/12:T/2+T/12;
X=X-T/24;
Y=[-1 -1 -1/2 -1/2 1/2 1/2 1 1 1/2 1/2 -1/2 -1/2 -1 -1];


t=-T/2:T/10000:T/2;
fh=@(t) interp1(X,Y,t);
N=35;

[a,b,m] = calc_fft(fh,T,t,N,1);
