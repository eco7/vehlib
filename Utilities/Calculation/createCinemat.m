function [CYCL] = createCinemat(VD,tsim, vit, distance, altitude, wmt)

CYCL = [];
% Cinematique vitesse-temps.
CYCL.ntypcin =  1 ;
CYCL.recharge = 0;	% Recharge
CYCL.temps = 0:1:max(tsim);
CYCL.tmax = max(CYCL.temps);
CYCL.vitesse = interp1(tsim,vit,CYCL.temps);

if nargin == 6
    % Cinematique vitesse-temps avec rapport de boite.
    CYCL.ntypcin =  3 ;

    % Rapport de boite
    rappvit =  nan(size(wmt));
    kbv = wmt./(vit./VD.VEHI.Rpneu)./VD.RED.kred;
    k= VD.BV.k;
    for j=length(k)-1:-1:1
        rappvit(isnan(rappvit) & kbv<((k(j)+k(j+1))/2)) =  j+1;
    end
    CYCL.rappvit = round(interp1(tsim,rappvit,CYCL.temps));
    CYCL.rappvit(isnan(CYCL.rappvit)) = 1;

    d = 0:200:max(distance);
    alt = interp1(distance,altitude,d);
    d = d(~isnan(alt));
    alt = alt(~isnan(alt));

    pente = 100*diff(alt)./diff(d);
    pente = max(min(pente,8),-8);

    % Pente
    CYCL.ntyppente = 1; % Pente variable avec le point kilometrique du vehicule
    CYCL.pente_ligne=[ diff(d)/1000; pente];
    CYCL.pente_ligne=CYCL.pente_ligne';
else
    % Pente
    CYCL.pente = 0;
end

end