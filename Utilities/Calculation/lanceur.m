
%clear all
[vehlib, VD]=vehsim_auto2('Peugeot_308SW_1L6VTI16V_HP_BV_2EMB_TWC','HP_BV_2EMB','CIN_NEDC_BV',40,1,0);
eval('param_backward_lagrange_2EMB_TWC_cvgce');
ERR=struct([]);
%fonctionEval='@(param) calc_HP_BV_2EMB_TWC(ERR,vehlib,param,VD)';
fct_bckw=@(param) calc_HP_BV_2EMB_TWC(ERR,vehlib,param,VD);

ii=0;

K=[0 0.025 0.05 0.1 0.15 0.2 0.3 0.5 0.7 1 2 3 5 7 10 20 40];
%kNOx=[1 1.5 2 3 5];
%K=0.1;
%lambda2_vec=[-100 -50 -10 -1 -0.1 0.1 1 10 50 100];
lambda2_vec=-200:10:100;
for jj=1:1:length(K)
    eval(['param.K=',num2str(K(jj)),';']);
   % for lambda2p=500:-50:-1500
    for kk=1:1:length(lambda2_vec);
        
        %try
            %eval(['param.lambda2=',num2str(lambda2p),';']);
            param.lambda2=lambda2_vec(kk);
            ii=ii+1;
            fprintf(1,'Calcul: %d - lambda2: %f k: %f\n',ii,param.lambda2,param.K);
            [ERR,X,Y,X1,X2,ResXml]=dicho(@(lambda) call_fct_backward(fct_bckw,param,lambda),60,0.1,[0 20],30,1,0);
            if isempty(ERR)
                conso100=XmlValue(ResXml,'conso100');
                Conso100_tab(ii)=conso100;
                soc=XmlValue(ResXml,'soc');
                Dsoc_tab(ii)=soc(end)-soc(1);
                tsim=XmlValue(ResXml,'tsim');
                NOx=XmlValue(ResXml,'NOx');
                NOx_tab(ii)=trapz(tsim,NOx);
                CO=XmlValue(ResXml,'CO');
                CO_tab(ii)=trapz(tsim,CO);
                HC=XmlValue(ResXml,'HC');
                HC_tab(ii)=trapz(tsim,HC);
            else
                Conso100_tab(ii)=NaN;
                Dsoc_tab(ii)=NaN;
                NOx_tab(ii)=NaN;
                CO_tab(ii)=NaN;
                HC_tab(ii)=NaN;
            end
            K_tab(ii)=param.K;
            lambda1_tab(ii)=X;
            lambda2_tab(ii)=param.lambda2;
            fprintf(1,'Calcul: %d - lambda1: %f lambda2: %f conso100: %f\n',ii,X,param.lambda2,Conso100_tab(ii));
        %catch
          %  fprintf(1,'%s %f\n','erreur calcul pour lambda2 = ',lambda2p);
        %end
    end
end
save res_NEDC_3polluants_l2nc_TcatChaud
 
