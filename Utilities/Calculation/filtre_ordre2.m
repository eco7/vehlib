%  function [ERR,pfiltre]=filtre_ordre2(ERR,VD,param,pinst,pfiltre,ind)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Filtrage du vecteur pinst avec un filtre du premier ordre 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

function [ERR,pfiltre]=filtre_ordre2(ERR,VD,param,pinst,pfiltre,ind)

wc2=2*pi*param.fc2;
Qc2=1/sqrt(2);
DT=param.pas_temps;
al1=DT*wc2;

if ind>2
    pfiltre(ind)=1/(1+al1^2+al1/Qc2)*( pinst(ind)*al1^2  +   pfiltre(ind-1)*(2+al1/Qc2) -   pfiltre(ind-2)  );
else
    pfiltre(ind)=0;
end

