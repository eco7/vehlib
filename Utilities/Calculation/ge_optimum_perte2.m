% fonction generant la courbe de fonctionnement optimal d'un groupe
% electrogene
% MOTH : moteur thermique
% ACM : machine electrique
% nb_iso : nombre de puissances testees
% nb_pt_iso : nombre de points par puissance
% trace : 1 trace de la courbe optimum, 0 pas de trace

function [GE]=ge_optimum_perte2(MOTH,ACM,nb_iso,nb_pt_iso,trace,kred,rend)

if nargin==5
    kred=1;
    rend=1;
end

%% Cartogaphie des pertes du groupe electrogene

% homogeneisation des cartographies de la machine electrique et du moteur
% thermique (ACM reechantillonne sur MOTH)

% perte electrique reechantillone sur regime et couple moteur thermique

GE.Cpl_carto=max(0,MOTH.Cpl_2dconso(1)) : (min(MOTH.Cpl_2dconso(end),max(-kred/rend*ACM.Cmot_pert))-max(0,MOTH.Cpl_2dconso(1)))/40 : min(MOTH.Cpl_2dconso(end),max(-kred/rend*ACM.Cmot_pert));
GE.Reg_carto=MOTH.Reg_2dconso(1):(min(MOTH.Reg_2dconso(end),ACM.Regmot_pert(end)/kred)-MOTH.Reg_2dconso(1))/30:min(MOTH.Reg_2dconso(end),ACM.Regmot_pert(end)/kred);

% le couple carto est positif (=couple mth) il doit etre pris en negatif pour le calcul des pertes ACM

 % Pour de s pb de precision nuerique il peut arriver que la veleur max de reg_interp depasse
% d'un pouilleme la valeur max de ACM.Regmot_pert, du a division puis
% multiplication par kred.
% On la remet a sa valeur
% exacte. On rencontre le meme pb sur les couples. 
% On remet aussi la derniere valeur de c_interp a sa valeur exacte
reg_interp=kred*GE.Reg_carto; 
reg_interp(end)=min(MOTH.Reg_2dconso(end)*kred,ACM.Regmot_pert(end));

c_interp = -rend*GE.Cpl_carto/kred;
c_interp(end) = max(-rend/kred*MOTH.Cpl_2dconso(end),min(ACM.Cmot_pert));

GE.qacm=interp2(ACM.Cmot_pert,ACM.Regmot_pert',ACM.Pert_mot,c_interp,reg_interp');



% matrice des puissances mecanique niveau moteur thermique sur les points de la cartographie
[Cpl,Reg]=meshgrid(GE.Cpl_carto,GE.Reg_carto);
GE.P_meca=Cpl.*Reg;

% expression de la carto moteur en Watt
GE.Conso_2d=interp2(MOTH.Cpl_2dconso,MOTH.Reg_2dconso',MOTH.Conso_2d,GE.Cpl_carto,GE.Reg_carto');
GE.Conso_2dWatt=GE.Conso_2d.*MOTH.pci;

% carto des pertes moteur thermique
GE.Pert_therm=GE.Conso_2dWatt-GE.P_meca;

% carto des pertes red
GE.Pert_red=GE.P_meca*(1-rend);

% carto des perte du groupe
GE.Pert_group=GE.Pert_therm+GE.Pert_red+GE.qacm;

% courbe enveloppe

% couple min electrique reechantillonne sur regime max moteur thermique
Cpl_pert_min=-kred/rend*interp1(ACM.Regmot_cmax,ACM.Cmin_mot(:,size(ACM.Tension_cont,2)),kred*MOTH.wmt_max,'linear',0);

%  courbe enveloppe du groupe generateur
GE.cpl_max=min(Cpl_pert_min,MOTH.cmt_max);
GE.reg_max=MOTH.wmt_max;
% carto puissance electrique du groupe
GE.Pelec_group=GE.P_meca*rend-GE.qacm;

%% recherche courbe optimum perte

[pmt_opti wmt_opti cmt_opti]=puissance_opti_carto(GE.Cpl_carto,GE.Reg_carto',GE.Pelec_group,GE.Pert_group,nb_iso,nb_pt_iso,GE.cpl_max,GE.reg_max,MOTH.ral);

GE.pge_opti=pmt_opti; % Puissance mecanique opti
GE.wmt_opti=wmt_opti;%(index);
GE.wacm_opti=wmt_opti*kred;%(index); % important a inclure si pour le cas ou l'on a un reducteur entre les deux
GE.cmt_opti=cmt_opti;%(index);
GE.cacm_opti=-cmt_opti*rend/kred;%(index);     % important a inclure si pour le cas ou l'on a un reducteur entre les deux
GE.dcarb_opti=interp2(MOTH.Cpl_2dconso,MOTH.Reg_2dconso,MOTH.Conso_2d,GE.cmt_opti,GE.wmt_opti);
GE.qacm_opti=interp2(GE.Cpl_carto,GE.Reg_carto,GE.qacm,GE.cmt_opti,GE.wmt_opti);

GE.rendement=GE.Pelec_group./GE.Conso_2dWatt;

if isfield(MOTH,'nom') && isfield(ACM,'nom')
    GE.nom=['groupe electrogene : ',num2str(round(max(GE.pge_opti./1000))),' kW : ',regexprep(MOTH.nom,'_',' '),' et ',regexprep(ACM.nom,'_',' ')];
elseif isfield(MOTH,'nom')
    GE.nom=['groupe electrogene : ',num2str(round(max(GE.pge_opti./1000))),' kW : ',regexprep(MOTH.nom,'_',' ')];
elseif isfield(ACM,'nom')
    GE.nom=['groupe electrogene : ',num2str(round(max(GE.pge_opti./1000))),' kW : ',regexprep(ACM.nom,'_',' ')];
else
    GE.nom=['groupe electrogene : ',num2str(round(max(GE.pge_opti./1000))),' kW  '];
end
    

%% trace de la courbe
if trace>=1
    figure
    plot(MOTH.wmt_max*30/pi,Cpl_pert_min,...
        MOTH.wmt_max*30/pi,MOTH.cmt_max,...
        MOTH.wmt_max*30/pi,GE.cpl_max,...
        GE.wmt_opti*30/pi,GE.cmt_opti,':+')
    hold on
    val=GE.rendement;
    for ii=1:length(GE.Reg_carto)
        ligneval=val(ii,:);
        ligneval(GE.Cpl_carto>interp1(GE.reg_max,GE.cpl_max,GE.Reg_carto(ii)))=NaN;
        val(ii,:)=ligneval;
    end
    [c,h]=contour(GE.Reg_carto*30/pi,GE.Cpl_carto,val'.*100,20:2.5:50);
    clabel(c,h);
    grid
    hold off
    
    title(['rendement ',GE.nom])
    xlabel('regime tr/min')
    ylabel('couple moteur thermique Nm')
    legend('enveloppe motelec ramenee au moth','enveloppe mtherm','enveloppe groupe','courbe optimum','location','NorthWest')
end