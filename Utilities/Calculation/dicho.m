% function function [ERR,X,Y,X1,X2]=dicho(fonctionEval,Objectif,Cconv,intervalle,Nbmax)
% 
% interface entree :
% fonctionEval : handle de la fonction a evaluer Y=f(X)
% Objectif : Valeur recherche pour Y
% Cconv : Critere de convergence sur la valeur recherchee de Y
% intervalle : intervalle de depart pour X
% Nbmax : nombe max d'iterations
% Res : format des donn?es : 0 rien, 1 format Xml, 2 structure "simple" 
% verbose : affichage
% Interface sortie
% ERR : 
% X : Valeur de X apres convergence
% Y : Valeur de Y apres convergence
% X1,X2 : Derniere valeur de l'intervalle de recherche utile notamment
% si non convergence
% 
% Exemple :
% [vehlib, VD]=vehsim_auto2('Peugeot_308_HYBS_PVAR','HSER_GE','CIN_NEDC',40, 1,0);
% eval('param_backward_lagrange_cvgce_HSER_GE');
%
% ERR=struct([]);
% fct_bckw=@(param) calc_HSER_GE(ERR,vehlib,param,VD);
%
% [ERR,X,Y,ResXml]=dicho(@(lambda) call_fct_backward(fct_bckw,param,lambda),60,0.2,[2 4],30,1,0);
%
%
%function [ERR,X,Y,X1,X2,RX]=dicho(fonctionEval,Objectif,Cconv,intervalle,Nbmax,Rxml,verbose)
function [ERR,X,Y,RX,X1,X2,RX1,RX2,Y1,Y2]=dicho(fonctionEval,Objectif,Cconv,intervalle,Nbmax,Res,verbose)

ERR=struct([]);
if nargin ~=7
    ERR=MException('BackwardModel:dicho','Nombre d arguments incompatibles');
    return;
end

nboucle=1;

% initialisation
X1=intervalle(1);
if Res==0;
    [ERR,Y1]=fonctionEval(X1);
elseif Res==1;
    [ERR,Y1,RX1]=fonctionEval(X1);
elseif Res==2;
    [ERR,Y1,~,RX1]=fonctionEval(X1);  
end
if ~isempty(ERR)
    [X,Y,X1,X2]=backward_erreur(1);
    RX=[]; RX1=[]; RX2=[];
    return
end
critere1=Y1-Objectif;
if verbose~=0
    fprintf(1,'Iteration: %d - Critere = %f - X = %f - Y = %f \n',nboucle,critere1,X1,Y1);
end

if  (abs(critere1)<Cconv)
    X=X1;
    Y=Y1;
    if Res==1
        RX=RX1;
        RX2=RX;
        X2=X1;
    end
    return
end

nboucle=nboucle+1;
X2=intervalle(2);
if Res==0;
    [ERR,Y2]=fonctionEval(X2);
elseif Res==1;
    [ERR,Y2,RX2]=fonctionEval(X2);
elseif Res==2;
    [ERR,Y2,~,RX2]=fonctionEval(X2);
end
if ~isempty(ERR)
    [X,Y,X1,X2]=backward_erreur(1);
    RX=[]; RX1=[]; RX2=[];
    return
end
critere2=Y2-Objectif;
if  (abs(critere2)<Cconv)
    X=X2;
    Y=Y2;
    if Res==1
        RX=RX2;
    end
    return
end
if verbose~=0 
    fprintf(1,'Iteration: %d - Critere = %f - X = %f - Y = %f \n',nboucle,critere2,X2,Y2);
end

if critere1*critere2>0
    ERR=MException('BackwardModel:dicho','Probleme de valeurs initiales');
    critere1
    Y1
    critere2
    Y2
    [X,Y]=backward_erreur(1);
    RX=[]; RX1=[]; RX2=[];
    return;
end

nboucle=nboucle+1;
X=(X1+X2)/2;
if Res==0;
    [ERR,Y]=fonctionEval(X);
elseif Res==1;
    [ERR,Y,RX]=fonctionEval(X);
elseif Res==2;    
    [ERR,Y,~,RX]=fonctionEval(X);
end
if ~isempty(ERR)
    [X,Y,X1,X2]=backward_erreur(1);
    RX=[]; RX1=[]; RX2=[];
    return
end
critere=Y-Objectif;
if  (abs(critere)<Cconv)
    return
end
if verbose~=0
    fprintf(1,'Iteration: %d - Critere = %f - X = %f - Y = %f \n',nboucle,critere,X,Y);
end

% Si la courbe est decroissante il faut inverser les valeurs
if critere1>0
    X_temp=X1;
    X1=X2;
    X2=X_temp;
end

nboucle=nboucle+1;

while nboucle<Nbmax && (abs(critere)>Cconv)
    if critere<0
        X1=X;
        RX1=RX;
    else
        X2=X;
        RX2=RX;
    end
    X=(X1+X2)/2;
    if Res==0;
        [ERR,Y]=fonctionEval(X);
    elseif Res==1;
        [ERR,Y,RX]=fonctionEval(X);
    elseif Res==2;
        [ERR,Y,~,RX]=fonctionEval(X);   
    end
    if ~isempty(ERR)
        [X,Y,X1,X2]=backward_erreur(1);
        RX=[]; RX1=[]; RX2=[];
        return
    end
    critere=Y-Objectif;
    if verbose
        fprintf(1,'Iteration: %d - Critere = %f - X = %f - Y = %f \n',nboucle,critere,X,Y);
    end
    nboucle= nboucle+1;
end % fin boucle while

return






