

% -------------------------------------------------------------------------
% function [X_liss]=lissage_mg(X,a,res1);
%
% Fonction lissage par moyenne glissante plus suppression des pics
% interface : X=vecteur a lisse
%             a=vecteur de ponderation
%             res1=seuil de suppression des pics
%
% Cet utilitaire supprime dans un premier temps les singularit�s (si il y as une trop grand diff�rence entre deux points cons�cutifs, 
% valeur a fixer dans l'interface de la fonction), ces points sont remplac�s par la moyenne du point pr�c�dent et du point suivant. 
% On itere ensuite sur les donn�es jusqu'� ce qu'il n'y ai plus de singularit�.
% Ensuite un filtrage par moyenne glissante est effectu�.
% On peut choisir les nombres de point de moyennage, leur position par rapport au point sur lequel 
% on effectue le calcul et pond�rer les points utilis�s pour le calcul de cette moyenne.
% Il suffit de renseigner le vecteur a, qui n'as pour seul contrainte que de contenir un nombre impairs d'�l�ments.
% Les poids appliquer aux points de moyennage sont alors tel que l'element central de a applique son poids aux point courant, 
% les pr�c�dents aux point pr�c�dents, les suivants aux points suivant. La fonction lissage_mg envoie une alerte si la somme des poids est diff�rente de 1.
%
% Exemple d'appel
% a=1/5[1 1 1 1 1];
% res1=0.1
% [alt_liss]=lissage_mg(alt_rel,,res1); X_moy=1/5*X(i-2)+1/5*X(i-1)+1/5*X(i)+1/5*X(i+1)+1/5*X(i+2)
% et suppression des points dont le difference est superieur � 0.1
%
% a=[1]; uniquement suppressione des singularit�s
%
% a=[1/6 1/6 1/3 0 0]; moyenne sur trois points pr�c�dent avec pond�ration
% res1=NaN;
% X_moy=1/6*X(i-2)+1/6*X(i-1)+1/3*X(i)
% moyenne sur trois points pr�c�dent avec pond�ration et a priori pas de
% suppression des singularit�
%
%
% Avril 2007 : EV,BJ
% -------------------------------------------------------------------------

function [X_liss]=lissage_mg(X,a,res1);

N=length(a);

%%% test parit� de N
if fix(N/2) == ceil(N/2)
    par_N=1;
else 
    par_N=0;
    Nm=fix(N/2);
end
if par_N==1
    'Attention nombre pair de valeur dans a'
end


%%% Verification taille a
if ~sum(a)==1
    '*** Attention somme des valeurs de a diff�rentes de 1 ***'
end

X_liss=X;

%% Suppression des pics
if nargin==3 & ~isnan(res1)
    n_pic=1;
    while ~n_pic == 0
        n_pic=0;
        for n=2:length(X)-1
            if(abs(X_liss(n)-X_liss(n-1))>res1 | abs(X_liss(n+1)-X_liss(n))>res1) & (abs(X_liss(n)-X_liss(n-1))>abs(X_liss(n+1)-X_liss(n-1)) |abs(X_liss(n+1)-X_liss(n))>abs(X_liss(n+1)-X_liss(n-1)))
                X_liss(n)=(X_liss(n+1)+X_liss(n-1))/2;
                n_pic=n_pic+1;
            end
        end
    end
end


%% lissage par moyenne glissante
for i=Nm+1:length(X)-Nm
    X_filt(i)=0;
    for j=1:N
       X_filt(i)=X_filt(i)+a(j)*X_liss(i+(j-1)-Nm);
    end
end

X_liss(length(X)-Nm:length(X))=X_liss(length(X_liss));
X_liss((Nm+1):length(X)-(Nm+1))=X_filt((Nm+1):length(X)-(Nm+1));

