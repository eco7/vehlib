function [var, indice, notVar, notIndice, ERR]= compare_threshold(varIn, threshold, exact)
% Detect signal higher than a threshold
% varIn is a vector, not tested for matrix
% var = compare_threshold(varIn, threshold), for a input vector varIn, returns a vector var
% satisfying varIn > threshold
% if threshold is omitted, default value is 0
% if exact is true, the test become varIn == threshold (default is false)
%
% [var, indice, notVar, notIndice, ERR]= compare_threshold(varIn, threshold)
%
% Example:
% varIn=1:10;
% threshold=2;
% [var, indice, notVar, notIndice]= compare_threshold(varIn, threshold)
%var =
%     3     4     5     6     7     8     9    10
%indice =
%     3     4     5     6     7     8     9    10
%notVar =
%     1     2
%notIndice =
%     1     2
%
% [var, indice, notVar, notIndice]= compare_threshold(varIn, threshold,true)
%var =
%     2
%indice =
%     2
%notVar =
%     1     3     4     5     6     7     8     9    10
%notIndice =
%     1     3     4     5     6     7     8     9    10
     
ERR=struct([]);

error(nargchk(1,3,nargin));

if nargin == 1
    threshold = 0;
    exact = false;
end
if nargin == 2
    % !
    exact = false;
end

if ~isvector(varIn) || ~isscalar(threshold)
    if ~isvector(varIn)
        chaine='First argument must be a vector';
    else
        chaine='Second argument must be a scalar';
    end
    ERR=MException('myDiff:Arg',chaine);
    throw(ERR)
end

if ~iscolumn(varIn)
    indices=[1:length(varIn)];
else
    indices=[1:length(varIn)]'; 
end
if exact
    indice=find(varIn == threshold);
else
    indice=find(varIn > threshold);
end

var=varIn(indice);
notIndice=setdiff(indices,indice);
notVar=varIn(notIndice);
