% Function: difference
% Fonction qui fait la difference entre le premier et le dernier element
% des variables vars contenues dans la structure xml
%
%  C Copyright IFSTTAR LTE 1999-2011
%
% Objet:

function [retour,ERR]=difference(S,vars,conditions)
ERR=struct([]);

if nargin ==3 && length(vars)~=length(conditions)
    ERR=1;
    return
end

for j=1:length(vars)
    vect=XmlValue(S,vars{j});
    if nargin == 3
    vect=vect(conditions{j});
    end
    retour(j)=vect(end)-vect(1);
end