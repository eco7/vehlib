% Function: integration
% Fonction qui fait l'integrale
% par la methode des trapezes
% des variables vars en fonction de x
%
%  C Copyright IFSTTAR LTE 1999-2011
%
% Objet:

function [retour]=integration(S,x,vars)

retour = zeros(size(vars));
if ~isempty(x)
    X=XmlValue(S,x{1});
end
for j=1:length(vars)
    vect=XmlValue(S,vars{j});
    vect(isnan(vect)) = 0;
    if ~isempty(x)
        retour(j)=trapz(X,vect);
    else
        retour(j)=trapz(vect);
    end
end