function [var, indice, notVar, notIndice, ERR]= diff_threshold(varIn, threshold)
% Detect signal variation higher than a threshold
% varIn is a vector, not tested for matrix
% var = diff_threshold(varIn, threshold), for a input vector varIn, returns a vector var
% satisfying diff(varIn) > threshold
% if threshold is omitted, default value is 0
%
% [var, indice, notVar, notIndice, ERR]= diff_threshold(varIn, threshold)
%
% Example:
% varIn=ones(1,10);
% varIn(3)=5;
% varIn(6)=5;
% threshold=2;
% [var, indice, notVar, notIndice]= diff_threshold(varIn, threshold)
% var = 
%     5     5
% indice =
%     3     6
% notVar =
%     1     1     1     1     1     1     1     1
% notIndice =
%     1     2     4     5     7     8     9    10

ERR=struct([]);

error(nargchk(1,2,nargin));

if nargin == 1
    threshold = 0;
end

if ~isvector(varIn) || ~isscalar(threshold)
    if ~isvector(varIn)
        chaine='First argument must be a vector';
    else
        chaine='Second argument must be a scalar';
    end
    ERR=MException('diff_threshold:Arg',chaine);
    throw(ERR)
end

if ~iscolumn(varIn)
    dvarIn=[0 abs(diff(varIn))];
    indices=[1:length(varIn)];
else
    dvarIn=[0; abs(diff(varIn))];
    indices=[1:length(varIn)]'; 
end

indice=find(dvarIn > threshold);

var=varIn(indice);
notIndice=setdiff(indices,indice);
notVar=varIn(notIndice);
