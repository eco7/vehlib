/*==========================================================
 * csortrows.c - implementation en langage C de sortrows
 * Top-Down implementation of a merge-sort algorithm
 * 
 * sort a matrix
 *
 * The calling syntax is:
 *
 *	[id_in_order(uint32), order_cost(uint32)]	= csortrows(id(uint32), cost(double))
 *  
 *  Sort over two columns : with ascending id, and the minimum cost in first position
 *  Put the result in first returning arg.
 *  The second argument order_cost shows the indices of the original id.
 *
 * BJ Mars 2021
 *========================================================*/
         
#include "mex.h"

#define NDIMS 1

/* The computational routine */
void CopyArrayDouble(double *A, uint32_T iBegin, uint32_T iEnd, double *B)
{
    uint32_T k;
    for(k = iBegin; k < iEnd; k++)
        B[k] = A[k];
}
void CopyArrayUint32(uint32_T *A, uint32_T iBegin, uint32_T iEnd, uint32_T *B)
{
    uint32_T k;
    for(k = iBegin; k < iEnd; k++)
        B[k] = A[k];
}
void CreateArrayUint32(uint32_T iBegin, uint32_T iEnd, uint32_T *B)
{
    uint32_T k;
    for(k = iBegin; k < iEnd; k++)
        B[k] = k+1;
}

//  Left source half is A[ iBegin:iMiddle-1].
// Right source half is A[iMiddle:iEnd-1   ].
// Result is            B[ iBegin:iEnd-1   ].
void TopDownMerge(double *A, uint32_T *inId, uint32_T *inInd, uint32_T iBegin, uint32_T iMiddle, uint32_T iEnd, double *B, uint32_T *outId, uint32_T *outInd, uint32_T maxRows)
{
    uint32_T i, j, k;
    
    i = iBegin, j = iMiddle;
 
    // While there are elements in the left or right runs...
    for (k = iBegin; k < iEnd; k++) {
        // If left run head exists and is <= existing right run head.
        if (i < iMiddle && (j >= iEnd || inId[i] < inId[j])) {
            B[k] = A[i];
            outId[k] = inId[i];
            outInd[k] = inInd[i];
            i = i + 1;
        } 
        else if (i < iMiddle && (j >= iEnd || inId[i] == inId[j])) {
            if (A[i] <= A[j]) {
               B[k] = A[i];
               outId[k] = inId[i];
               outInd[k] = inInd[i];
                i = i + 1;
            }
            else {
                B[k] = A[j];
                outId[k] = inId[j];
                outInd[k] = inInd[j];
                j = j + 1;
            }           
        }
        else {
            B[k] = A[j];
            outId[k] = inId[j];
            outInd[k] = inInd[j];
            j = j + 1;
        }
    }
}

// Sort the given run of array A[] using array B[] as a source.
// iBegin is inclusive; iEnd is exclusive (A[iEnd] is not in the set).
void TopDownSplitMerge(double *B, uint32_T *outId, uint32_T *outInd, uint32_T iBegin, uint32_T iEnd, double *A, uint32_T *inId, uint32_T *inInd, uint32_T maxRows)
{
    uint32_T iMiddle;
    if(iEnd - iBegin <= 1)                       // if run size == 1
        return;                                 //   consider it sorted
    // split the run longer than 1 item into halves
    iMiddle = (iEnd + iBegin) / 2;              // iMiddle = mid point
    // recursively sort both runs from array A[] into B[]
    TopDownSplitMerge(A, inId, inInd, iBegin,  iMiddle, B, outId, outInd, maxRows);  // sort the left  run
    TopDownSplitMerge(A, inId, inInd, iMiddle,    iEnd, B, outId, outInd, maxRows);  // sort the right run
    // merge the resulting runs from array B[] into A[]
    TopDownMerge(A, inId, inInd, iBegin, iMiddle, iEnd, B, outId, outInd, maxRows);
}


void TopDownMergeSort(double *A, uint32_T *inId, uint32_T *inInd, double *B, uint32_T *outId, uint32_T *outInd, int n, int maxRows)
{
     CopyArrayDouble(A, 0, maxRows, B);                // one time copy of A[] to B[]
     CopyArrayUint32(inId, 0, maxRows, outId);         // one time copy of A[] to B[]
     CopyArrayUint32(inInd, 0, maxRows, outInd);       // one time copy of A[] to B[]
     TopDownSplitMerge(B, outId, outInd,  0, n, A, inId, inInd, maxRows);     // sort data from B[] into A[]
}


/* The gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *inMatrix, * inMatrixCopy;               /* 1xN input matrix */
    mxUint32 *inId, *inIdCopy, *inInd;
    uint32_T ncols, nrows, maxRows, i;   /* size of matrix */
    double *workingMatrix;              /* output matrix */
    mxUint32 *outId, *outInd;
    
    /* check for proper number of arguments */
    if(nrhs!=2) {
        mexErrMsgIdAndTxt("MyToolbox:csortrows:nrhs","Two inputs required.");
    }
    if(nlhs!=2) {
        mexErrMsgIdAndTxt("MyToolbox:csortrows:nlhs","Two outputs required.");
    }
    
    /* make sure the second input argument is type double */
    if (!mxIsDouble(prhs[1])) {
        mexErrMsgIdAndTxt("MyToolbox:csortrows:notDouble","Input matrix must be type double.");
    }
    
    /* check that number of columns in first input argument is 1 */
    if(mxGetN(prhs[0])!=1) {
       mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notRowVector","Input must be a one columns matrix.");
    }
    
     /* create a pointer to the real data in the input matrix  */
    inId = (uint32_T *)mxGetPr(prhs[0]);

    /* create a pointer to the real data in the input matrix  */
    inMatrix = mxGetPr(prhs[1]);
    
    /* get dimensions of the input matrix */
    ncols = 1;
    nrows = mxGetM(prhs[0]);
    maxRows = nrows;
    
     /* create the output id */
    plhs[0] = mxCreateNumericMatrix((uint32_T)nrows,(uint32_T)ncols, mxUINT32_CLASS, mxREAL);
    /* get a pointer to the real data in the output matrix */
    outId = (uint32_T *)mxGetPr(plhs[0]);
 
     /* create the output ind */
    plhs[1] = mxCreateNumericMatrix((uint32_T)nrows,(uint32_T)ncols, mxUINT32_CLASS, mxREAL);
    /* get a pointer to the real data in the output matrix */
    outInd = (uint32_T *)mxGetPr(plhs[1]);
    
    /* create the working matrix B */
    workingMatrix = (double *)mxGetPr(mxCreateNumericMatrix((uint32_T)nrows,(uint32_T)ncols, mxDOUBLE_CLASS, mxREAL));

    /* create a copy of input matrix A */
    inMatrixCopy = (double *)mxGetPr(mxCreateNumericMatrix((uint32_T)nrows,(uint32_T)ncols, mxDOUBLE_CLASS, mxREAL));
    /* create a copy of inId */
    inIdCopy = (uint32_T *)mxGetPr(mxCreateNumericMatrix((uint32_T)nrows,(uint32_T)ncols, mxUINT32_CLASS, mxREAL));
    /* create a matrix of Indices */
    inInd = (uint32_T *)mxGetPr(mxCreateNumericMatrix((uint32_T)nrows,(uint32_T)ncols, mxUINT32_CLASS, mxREAL));
    
    /* create a pointer to the copy */
    //inMatrixCopy = mxGetPr(copy);
    

//      for (i=0; i<maxRows; i++) {
//         mexPrintf("%f \n",inId[i]);
//     }
       
    CopyArrayDouble(inMatrix, 0, maxRows, inMatrixCopy);
    CopyArrayUint32(inId, 0, maxRows, inIdCopy);
    CreateArrayUint32(0, maxRows, inInd);
    
    /* call the computational routine */   
    TopDownMergeSort(inMatrixCopy, inIdCopy, inInd, workingMatrix, outId, outInd, nrows, maxRows);
//     for (i=0; i<maxRows; i++) {
//         mexPrintf("%f \n",inId[i]);
//     }
    

}
