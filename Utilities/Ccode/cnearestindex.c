/*==========================================================
 * cnearestindex.c - implementation en langage C d'une fonction qui renvoie
 * un vecteur d'indice (en uint32) qui minimise abs(X(i)-Y)
 * X et Y sont des vecteurs de differentes tailles
 * 
 *
 * The calling syntax is:
 *
 *	[id(uint32)]	= cnearestindex(X(double), Y(double))
 *  
 *
 * BJ Juin 2021
 *========================================================*/
         
#include "mex.h"
#include <math.h>

#define NDIMS 1



/* The gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *X, *Y;               /* 1xN input matrix */
    uint32_T ncols, nXrows, nYrows, i, j;   /* size of matrix */
    mxUint32 *outId;
    uint32_T nl, nh, n;
    
    /* check for proper number of arguments */
    if(nrhs!=2) {
        mexErrMsgIdAndTxt("MyToolbox:csortrows:nrhs","Two inputs required.");
    }
    if(nlhs!=1) {
        mexErrMsgIdAndTxt("MyToolbox:csortrows:nlhs","One output required.");
    }
    
    /* make sure the first input argument is type double */
    if (!mxIsDouble(prhs[0])) {
        mexErrMsgIdAndTxt("MyToolbox:cmin:notDouble","Input matrix must be type double.");
    }
    
    /* make sure the second input argument is type double */
    if (!mxIsDouble(prhs[1])) {
        mexErrMsgIdAndTxt("MyToolbox:cmin:notDouble","Input matrix must be type double.");
    }
    
    /* check that number of columns in first input argument is 1 */
    if(mxGetN(prhs[0])!=1) {
       mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notRowVector","Input must be a one columns matrix.");
    }
    
    /* check that number of columns in second input argument is 1 */
    if(mxGetN(prhs[1])!=1) {
       mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notRowVector","Input must be a one columns matrix.");
    }
    
     /* create a pointer to the real data in the X matrix  */
    X = mxGetPr(prhs[0]);

    /* create a pointer to the real data in the Y matrix  */
    Y = mxGetPr(prhs[1]);
    
    /* get dimensions of the input matrix */
    ncols = 1;
    nXrows = mxGetM(prhs[0]);
    nYrows = mxGetM(prhs[1]);
    
    /* create the output id */
    plhs[0] = mxCreateNumericMatrix((uint32_T)nXrows,(uint32_T)ncols, mxUINT32_CLASS, mxREAL);
    /* get a pointer to the real data in the output matrix */
    outId = (uint32_T *)mxGetPr(plhs[0]);
    
    for (i=0; i<nXrows; i++) {
        nl = 0;
        nh = nYrows-1;
        while (1) {
            n = floor((nl+nh)/2);
            if (nh == nl) {
                break;
            }
            else if (fabs(nh-nl) <= 1.) {
                if (fabs(X[i]-Y[nl]) < fabs(X[i]-Y[nh])) {
                    n = nl;
                }
                else {
                    n = nh;
                }
                break;
            }
            if (X[i]<Y[n]) {
                nh = n;
            }
            else {
                nl = n;
            }
        }
        outId[i] = n+1;       
    }
    
        
        
        
        //outId[i] = 1;
//     for (i=0; i<nXrows; i++) {
//         mexPrintf("%d %f \n",outId[i], X[i]);
//     }
       
    

}
