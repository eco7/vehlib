function [d_opt, v_opt, d_palier, v_palier] = adas(v0, d, v, Tdes)
% Implementation algorithme SRTO : Simple and Rapid Trajectory
% Optimisation - Travaux de S. Javanmardi
% Calcul de la trajectoire optimale d'un vehicule a partir de limitation de
% vitesse fonction de point kilometrique (d,v) et d'un temps desire de
% parcours.
% v0 est la vitesse initiale
% V2 : On commence par ajouter les accel et les decel
% V3 :  Ajout de la vitesse initiale v0 -
% Gestion de crise si une deceleration apparait avant d'atteindre la vitesse de palier ...


% Pas en distance constant
pas = 1;

% Initialisation
verbose = 0;
% Valeur de deceleration
decel = -0.5;
% Acceleration moyenne en fonction de la vitesse
aa = [1.5 1.5 1.0 1.0 0.5 0.5];
va = [0 50 51 90 91 150]/3.6;

if verbose 
    if ~isempty(findobj('tag','adas2'))
        hold off
    else
        figure('tag','adas2');
    end
    plot(d,v); grid on;
    hold on;
end

if v0 ==0
    % Il faut une vitesse initiale non nulle
    v0= 1;
end
v_opt(1) = v0;
v(1) = v0;
v_opt(2) = v0;
v(2) = v0;

temps = calcul_temps(d,v);

if temps > Tdes
    if verbose, disp('Pas de solution'); end
    d_opt = d;
    v_opt = v;
    d_palier = d;
    v_palier = v;
    return
end

v2 = v;
d2=d;

% Ajout des accelerations
ac = diff(v);
in_acc = find(ac > 0);
in_dcc = find(ac < 0);
% Tous les changements de vitesse
in_acc_or_dcc = sort([in_acc in_dcc]);

pas_t2 = [0 diff(d2)]./v2;
pas_t2(isinf(pas_t2))=0;
pas_t2(isnan(pas_t2))=0;

pas_d=[0 diff(d)];

% Rajout des accelerations
for j = 1 :length(in_acc)
    % Indice debut acceleration
    ia = in_acc(j);
    % Vitesse haute en fin d'acceleration
    vh = v2(ia+1);
    indice=ia;
    % calcul de l'acceleration moyenne
    accel = interp1(va,aa,vh,'linear','extrap');
    % Prochain changement de vitesse (apres le courant)
    next = in_acc_or_dcc(find(in_acc_or_dcc == ia)+1);
    while (1)
        % on accelere
        pas_t2(indice+1) = (pas_d(indice)./v2(indice));
        v2(indice+1)=v2(indice)+accel*(pas_d(indice)./v2(indice));
        %v2(indice+1)=v2(indice)+accel*pas_t2(indice);
        if v2(indice+1) >= vh
            % Cas nominal
            v2(indice+1) = vh;
            break
        end
        if trapz(cumsum(pas_t2(1:indice)),v2(1:indice))>d2(next)
            % on a atteint le prochain changement de vitesse avant d'avoir
            % atteint la vitesse du palier - A suivre ...
            break
        end
        indice = indice + 1;
    end
end

if verbose, plot(d2,v2); pause; end

% Rajout des decelerations
for j = 1 :length(in_dcc)
    id = in_dcc(j);
    % Vitesse haute en debut de deceleration
    vh = v2(id);
    indice=id+1;
    while (1)
        % on decelere
        if v2(indice-1)<v2(indice)
            % Gestion de crise !
            break
        end
        if v2(indice)~=0
            v2(indice-1)=v2(indice)-decel*(pas_d(indice)./v2(indice));
        else
            v2(indice-1)=v2(indice)-decel*pas_t2(indice-1);
            if pas_t2(indice-1)==0
                'AIE'
            end
        end
        if v2(indice-1)>=vh
            % Cas nominal
            v2(indice-1)=vh;
            break;
        end
        indice = indice - 1;
    end
end

if verbose, plot(d2,v2); pause; end

temps = calcul_temps(d2,v2);

if temps > Tdes
    if verbose, disp('Pas de solution apres ajout dynamique vehicule'); end
    d_opt = d;
    v_opt = v;
    d_palier = d;
    v_palier = v;
    return
end

% Travail sur les paliers de vitesse
t2=temps;
vtemp = v;
while t2<Tdes
    % Extraction du palier a vitesse la plus elevee
    vprev = v2;
    vmax=max(vtemp);
    in = (vtemp==vmax);
    vmax2=max(vtemp(~in));
    in2 = (v2 >vmax2);
    v2(in2) = vmax2;
    vtemp(in) = vmax2;
    t2 = calcul_temps(d2,v2);
    if verbose, plot(d2,v2); pause; end
end

% Calcul du temps hors palier a vmax
 v2(in2)=0;
 t2 = calcul_temps(d2,v2);

% Calcul de la bonne vitesse
%delta_d = max(d2(in2))- min(d2(in2));
% Le calcul precedent ne fonctionne pas si plusieurs paliers espaces sur le
% parcours
delta_d = sum(in2)*pas;
delta_t = Tdes - t2;
v_seuil = delta_d / delta_t;

% Affectation du palier a la bonne vitesse (vprev permet de conserver la
% dynamique)
v2(in2) = min(vprev(in2),v_seuil);

d_opt = d2;
v_opt = v2;

if verbose, plot(d_opt,v_opt); end

% Reconstitution des paliers hors dynamique (pour le mdl simulink) !
v_palier = v2;
d_palier = d2;
ac = diff(v_palier);
in_acc = find(ac > 0); % | ac < 0
in2 = find(diff(in_acc)~=1);
if ~isempty(in2)
    ind = sort([1 in_acc(in2) in_acc(in2+1) in_acc(end)]);
else
    if ~isempty(in_acc)
        ind = [in_acc(1) in_acc(end)];
    else
        ind = [];
    end
end
if v2(2)~=v2(1)
    ind = [1 ind];
end
for j=1:2:length(ind)-1  
    v_palier(ind(j):ind(j+1)) = v_palier(ind(j+1)+1);  
end

[~] = calcul_temps(d_opt,v_opt);

end


function t = calcul_temps(d,v)
% Fonction de calcul du temps de parcours
%
d=diff(d);
delta_d=[0 d];

delta_d=delta_d(v~=0);
v=v(v~=0);

t = sum(delta_d./v);
fprintf(1,'Temps : %f\n',t);

end

