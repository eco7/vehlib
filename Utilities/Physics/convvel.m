function v2 = convvel(v1,unit1,unit2)
% CONVVEL convert between velocity units
%   v2 = CONVVEL(v1,unit1,unit2) 
%   Valid values of unit1 and unit2 are: lunit/tunit, with:
%   lunit: 'm','mi','in','ft' (meter, mile, inch, feet)
%   tunit: 's','m','h','d','y' (seconds,minutes,hours,days,years)
%   scale prefix are accepted: 'n','u','m','k','M','G' (nano, micro, milli,
%   kilo, mega, giga)
%
%   Examples:
%     v2 = convvel(v1,'km/h','m/s');
%     v2 = convlength(v1,'mi/h','km/h');
%
%   See also convlenght, convtime, convtemp
%   Inspired from: http://fr.mathworks.com/help/aerotbx/unit-conversions-1.html
%
%   IFSTTAR/LTE  - E. REDONDO
%   $Revision: 0.1 $  $Created: 2015/08/12, Modified: 2015/08/12$


lunit1 = regexp(unit1,'^.*/','match','once');
lunit2 = regexp(unit2,'^.*/','match','once');
tunit1 = regexp(unit1,'/.*$','match','once');
tunit2 = regexp(unit2,'/.*$','match','once');
lunit1 = lunit1(1:end-1);
lunit2 = lunit2(1:end-1);
tunit1 = tunit1(2:end);
tunit2 = tunit2(2:end);

lfactor = convlength(1,lunit1,lunit2);
tfactor = convtime(1,tunit1,tunit2);
v2 = v1 * lfactor/tfactor;

end

