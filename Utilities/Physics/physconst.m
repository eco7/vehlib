function [Const, Unit] = physconst(Name)
% PHYSCONST Physical constants
%   Const = physconst(Name) returns the constant corresponding to the
%   string Name in SI units.
%   Valid values of Name are:
%   'LightSpeed', 'Boltzmann', 'EarthRadius',... TODO (complete this)
%
%   See also mathconst
%
%   Inspired from: http://fr.mathworks.com/help/phased/ref/physconst.html
%   Data from:
%   [1] http://physics.nist.gov/cuu/Constants/allascii_2010.txt
%   [2] https://dx.doi.org/10.1007%2Fs001900050278
%
%   IFSTTAR/LTE  - E. REDONDO
%   $Revision: 0.1 $  $Created: 2015/08/12, Modified: 2015/08/12$
Names = {'Gravity', 'LightSpeed', 'Boltzmann', 'EarthRadius'};
Consts = 	[9.80665, 299792458, 1.3806488e-23,6371009];
Units = {'m s^-2','m s^-1','J K^-1',};
Const = Consts(strcmpi(Names,Name));
Unit = Units(strcmpi(Names,Name));
end