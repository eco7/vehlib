function T2 = convtemp(T1,unit1,unit2)
% CONVTEMP convert between temperature scales
%   T2 = CONVTEMP(T1,unit1,unit2) 
%   Valid values of unit1 and unit2 are:
%   'K','C','F' (kelvin, celsius, farenheit)
%
%   Examples:
%     T2 = convtemp(T1,'C','K')%from celsius to kelvin
%     T2 = convtemp(T1,'F','C')%from farenheit to celsius
%
%   See also convtime, convvel, convtemp
%   Inspired from: http://fr.mathworks.com/help/aerotbx/unit-conversions-1.html
%
%   IFSTTAR/LTE  - E. REDONDO
%   $Revision: 0.1 $  $Created: 2015/08/12, Modified: 2015/08/12$

units = {'K','C','F'};
slopes = [1 1 5/9];%chaque unite ramenee aux kelvin
biases = [0 273.15 459.67];%chaque unite ramenee aux kelvin

slop1 = slopes(strcmpi(units,unit1));
bias1 = biases(strcmpi(units,unit1));
slop2 = slopes(strcmpi(units,unit2));
bias2 = biases(strcmpi(units,unit2));

% T1k = T1*slop1+bias1;
% T2 = (T1k-bias2)/slop2;
T1k = (T1+bias1)*slop1;
T2 = T1k/slop2-bias2;
end