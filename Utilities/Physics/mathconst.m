function [Const] = mathconst(Name)
% MATHCONST Mathematical constants
%   Const = physconst(Name) returns the constant corresponding to the
%   string Name in SI units.
%   Valid values of Name are:
%   'pi', 'e', 'phi',... TODO (complete this)
%
%   See also physconst
%
%   Inspired from: http://fr.mathworks.com/help/phased/ref/physconst.html
%   Data from:
%   [1] https://en.wikipedia.org/wiki/Golden_ratio
%
%   IFSTTAR/LTE  - E. REDONDO
%   $Revision: 0.1 $  $Created: 2015/08/12, Modified: 2015/08/12$
Names = {'pi', 'e', 'phi'};
Consts = 	[pi, exp(1),1.6180339887498948482];
Const = Consts(strcmpi(Names,Name));

end