function t2 = convtime(t1,unit1,unit2)
% CONVTIME convert between time units
%   t2 = CONVTIME(t1,unit1,unit2) 
%   Valid values of unit1 and unit2 are:
%   's','m','h','d','y' (seconds,minutes,hours,days,years)
%   scale prefix are accepted: 'n','u','m','k','M','G' (nano, micro, milli,
%   kilo, mega, giga)
%
%   Examples:
%     t2 = convtime(t1,'s','h');%convert from seconds to hours
%     t2 = convtime(t1,'y','m');%convert from years to minutes
%
%   See also convlenght, convvel, convtemp
%   Inspired from: http://fr.mathworks.com/help/aerotbx/unit-conversions-1.html
%
%   IFSTTAR/LTE  - E. REDONDO
%   $Revision: 0.1 $  $Created: 2015/08/12, Modified: 2015/08/12$

units = {'s','m','h','d','y'};
unValues = [1 60 3600 86400 31536000];%chaque unite ramenee aux secondes
prefixes = {'n','u','m','','k','M','G'};
preValues = [1e-9, 1e-6,1e-3,1,1e3,1e6,1e9];

re = cell2mat(cellfun(@(x) ['|' x],units,'UniformOutput' , false));
re(1)='(';
re(end+1)=')';
re(end+1)='$';

un1 = regexp(unit1,re,'match','once');
pr1 = regexprep(unit1,re,'');

unV1 = unValues(strcmpi(units,un1));
prV1 = preValues(strcmp(prefixes,pr1));

un2 = regexp(unit2,re,'match','once');
pr2 = regexprep(unit2,re,'');

unV2 = unValues(strcmpi(units,un2));
prV2 = preValues(strcmp(prefixes,pr2));


t2 = t1*unV1*prV1/(unV2*prV2);
end