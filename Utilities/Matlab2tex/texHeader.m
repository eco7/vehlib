function [ ERR ] = texHeader(fid)
ERR=struct([]);

fprintf(fid,'%s\n','\documentclass[10pt, a4paper]{article}');
%\documentclass[twocolumn,10pt]{article}
fprintf(fid,'%s\n\n','\usepackage[dvips]{graphicx}');
fprintf(fid,'%s\n\n','\usepackage{amsmath}');
fprintf(fid,'%s\n','\usepackage{fullpage}');
fprintf(fid,'%s\n','\usepackage{placeins}');

% % *** inclusion des packages
% \usepackage[french]{babel}
% \usepackage{aeguill}
% \usepackage{isolatin1}
% \usepackage{float}
% \usepackage{graphicx}
%fprintf(fid,'%s\n','\usepackage{layout}');
% \usepackage{epsfig}  %pour mettre 2 fig cote a cote
% \usepackage{times}  %pour utiliser la police times et visualiser correctement dans acrobat
% \usepackage{verbatim}  %pour inclusion de fichiers
% \usepackage{multirow}
% \usepackage{lscape}
% \usepackage{color}		% Need the color package
% %\usepackage[centertags,intlimits]{amsmath}
% \usepackage{amsmath}
% \usepackage{multicol}

fprintf(fid,'%s\n','\setlength{\textwidth}{17cm}');
fprintf(fid,'%s\n','\setlength{\oddsidemargin}{-0.5cm}');
fprintf(fid,'%s\n','\setlength{\evensidemargin}{0.5cm}');
fprintf(fid,'%s\n','\setlength{\textheight}{230mm}');
fprintf(fid,'%s\n','\setlength{\headheight}{0mm}');
fprintf(fid,'%s\n','\setlength{\headsep}{15mm}');
fprintf(fid,'%s\n','\setlength{\topmargin}{-5mm}');
fprintf(fid,'%s\n','\setlength{\footskip}{8mm}');



fprintf(fid,'%s\n','\begin{document}');
%fprintf(fid,'%s\n','\layout');

end

