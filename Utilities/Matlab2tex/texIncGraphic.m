function [ ERR ] = texGraphic( fid, fileName, pathName, param)
ERR=struct([]);

file=fullfile(pathName,fileName);

fprintf(fid,'%s\n','\begin{figure}[!ht]');

fprintf(fid,'%s\n','\begin{center}');

%fprintf(fid,'%s%s%s\n','\includegraphics[height=1in,width=1in,angle=-90]{',file,'}');
fprintf(fid,'%s%s%s\n','\includegraphics{',file,'}');

if ~isempty(param.caption)
    fprintf(fid,'%s%s%s\n','\caption{',param.caption,'}');
end
fprintf(fid,'%s\n','\end{center}');
fprintf(fid,'%s\n','\end{figure}');

end

