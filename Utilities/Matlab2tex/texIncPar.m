function [ ERR ] = texIncPar(fid, text)
ERR=struct([]);

fprintf(fid,'%s\n\n','\par');
for ind=1:length(text)
fprintf(fid,'%s\n',text{ind});
end

end

