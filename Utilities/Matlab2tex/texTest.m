fid=fopen('test.tex','w');

ERR=texHeader(fid);

x = -pi:pi/10:pi;
y = tan(sin(x)) - sin(tan(x));
plot(x,y,'--rs','LineWidth',2,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor','g',...
    'MarkerSize',10);

set(gcf,'PaperPositionMode','auto')
set(gcf,'position',[282 527 700 300]);
print -depsc2 figure.eps
close(gcf);
param.caption='Test figure.';

[ ERR ] = texIncGraphic( fid, 'figure.eps', './', param);

[ ERR ] = texIncPar(fid, {'cette figure ...';'';'djfldhsdlmf'});
[ ERR ] = texIncPar(fid, {'$\operatorname{erfc}(x) =\frac{2}{\sqrt{\pi}} \int_x^{\infty} e^{-t^2}\,dt =\frac{e^{-x^2}}{x\sqrt{\pi}}\sum_{n=0}^\infty (-1)^n \frac{(2n)!}{n!(2x)^{2n}}$'});

[ ERR ] = texFinish( fid );

unix('latex test.tex ');
unix('dvipdf test.dvi');

fclose(fid);