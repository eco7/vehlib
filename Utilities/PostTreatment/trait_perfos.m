% File: trait_perfos
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Script de post traitement 
% pour temps de passage acceleration maxi
%
% Version 0 juillet 2006
% ------------------------------------------------------

vimin=0.01;

%lecture et increment du numero de calcul
fnum=strcat(VD.INIT.InitialFolder,VD.INIT.sep,'numcal.mat');
load(fnum);

fprintf(1,'\n\t%s%s%s%s%s%s\n',['POST TRAITEMENT PERFOS pour modele ',vehlib.nom,' - Architecture: ',vehlib.architecture,' - Version: ',VD.INIT.version]);
fprintf(1,'\n\t%s\n','------------------------------------------------------------------------------------');


% ecriture du fichier de resultats synthetiques
fid=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF_PERFOS'),'w');
fprintf(fid,'\n\t%s%s%s%s%s%s\n',['POST TRAITEMENT PERFOS pour modele ',vehlib.nom,' - Architecture: ',vehlib.architecture,' - Version: ',VD.INIT.version]);
fprintf(fid,'\n\t%s\n','------------------------------------------------------------------------------------');
fprintf(fid,'\n%s %s','Date de traitement du fichier          :  ',date);
fprintf(fid,'\n%s %4d\n\n','Numero de calcul                       :  ',numcal);
f=fieldnames(vehlib);

fprintf(fid,'\n%s\n','Nom des fichiers de simulation:');
for i=1:length(f)
    fprintf(fid,'%s%s%s%s\n','Nom du fichier: ',eval(strcat('vehlib.',f{i})),' - Nom du champ: ',strcat('vehlib.',f{i}));
end
fprintf(fid,'\n\n');

%%% VALEURS SYNTHETIQUES %%%%%
% Masse
fprintf(fid,'\n%s%.1f\n\n',['Masse du vehicule en kg : '],masse(1));


% Vmax
fprintf(fid,'%s%.2f\n\n','Vitesse  maximum en km/h     : ',max(vit)*3.6);

%temps de passage
v=[20 40 60 80 90 110 130 150]/3.6;
t=zeros(length(v),1);
for i=1:length(v)
    indice=min(find(vit>v(i)));
    t(i)=min(tsim(find(vit>v(i))));
    fprintf(fid,'%s%.2f%s\n',['Temps de passage 0 - ',num2str(v(i)*3.6),' km/h : '],t(i),' sec.');
    fprintf(fid,'%s%.2f%s\n\n',['Conso 0 - ',num2str(v(i)*3.6),' km/h : '],100*conso(indice)/(distance(indice)/1000),' l/100 km');
end
% fermeture
fclose(fid);

if isunix
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF_PERFOS')]);
end

