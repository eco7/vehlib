% Function: dep_accm3
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = dep_accm3(VD,temps, pas, vit, waccm, caccm, paccm_alt, paccm_servo, paccm_air, paccm_ref, vimin, fid)
%
% Resultat accessoires mecaniques (application bus)
% waccm, caccm: Vitesse et couple total consommes par les aux
% paccm_alt:    puis meca consomme par l alternateur
% paccm_servo:  puis meca consomme par la servo-direction
% paccm_air:    puis meca consomme par le compresseur d'air (portes, agenouillement).
% paccm_ref:    puis meca consomme par le ventilateur de refroidissement moteur (ex:pompe hydrostatique).
%
% VD.ACC.ntypacc=3
%
% version 1: juin 2005
% ------------------------------------------------------
function [] = dep_accm3(VD,temps, pas, vit, waccm, caccm, paccm_alt, paccm_servo, paccm_air, paccm_ref, vimin, fid)

fprintf(fid,'\n%s\n\n','          *** ACCESSOIRES MECANIQUES ***');

fprintf(fid,'%s\n',...
   '  Alternateur :');
fprintf(fid,'%s%6.2f%s\n',...
   '  Consommation mecanique moyenne alternateur  : ',mean(paccm_alt)/1000,' kW   ');
fprintf(fid,'%s%6.2f%s\n',...
   '  Rendement moyen alternateur                 : ',100*VD.ACC.rdmoy_acm2,' %   ');
fprintf(fid,'%s%6.2f%s\n',...
   '  Consommation electrique moyenne alternateur : ',mean(paccm_alt)/1000.*VD.ACC.rdmoy_acm2,' kW   ');

fprintf(fid,'%s\n',...
   '  Servo moteur de direction :');
fprintf(fid,'%s%6.2f%s\n',...
   '  Consommation mecanique moyenne servo moteur : ',mean(paccm_servo)/1000,' kW   ');

fprintf(fid,'%s\n',...
   '  Compresseur d air :');
fprintf(fid,'%s%6.2f%s\n',...
   '  Consommation mecanique moyenne compresseur  : ',mean(paccm_air)/1000,' kW   ');

fprintf(fid,'%s\n',...
   '  Ventilateur de refroidissement moteur :');
fprintf(fid,'%s%6.2f%s\n',...
   '  Consommation mecanique moyenne ventilateur  : ',mean(paccm_ref)/1000,' kW   ');

fprintf(fid,'\n%s%6.2f%s\n',...
   '  Consommation mecanique moyenne des accessoires : ',-1*mean(waccm.*caccm)/1000,' kW   ');
