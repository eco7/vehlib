% Function: dep_cin
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [pas] = dep_cin(VD,Temps, distance, Vitesse, Vit_dem, acc, elhyb, vimin, fid)
%
% Resultat cinematique.
% Arg de retour:
% pas: echantillonnage des sorties 'To Workspace' de la simulation (sec)
% ------------------------------------------------------
function [pas] = dep_cin(VD,Temps, distance, Vitesse, Vit_dem, acc, elhyb, vimin, fid)

nb_arret=1;
pas(1)=0;
for i=2:length(Temps)
    pas(i)=Temps(i)-Temps(i-1);
    if Vitesse(i)<=vimin & Vitesse(i-1)>vimin
        nb_arret=nb_arret+1;
    end
end
if size(pas) ~=size(Vitesse)
    pas=pas';
end

accmax=max(acc);
accmin=min(acc);

dist=sum(trapz(Temps,Vitesse));

if VD.CYCL.ntypcin~=4
    % 4 pour simulation de conduite
    dist_dem=sum(trapz(Temps,Vit_dem));
    vmoy_dem=sum(Vit_dem.*pas)/Temps(length(Temps));

    condition='Vit_dem>=vimin';
    tdep_dem=sum(pas(eval(condition)));
    vmoy_dep_dem=sum(Vit_dem(eval(condition)).*pas(eval(condition)))/tdep_dem;

    condition='Vit_dem<vimin';
    tarret_dem=sum(pas(eval(condition)));
end

vmoy=sum(Vitesse.*pas)/Temps(length(Temps));

condition='Vitesse>=vimin';
tdep=sum(pas(eval(condition)));
vmoy_dep=sum(Vitesse(eval(condition)).*pas(eval(condition)))/tdep;

condition='Vitesse<vimin';
tarret=sum(pas(eval(condition)));

dist=sum(trapz(Temps,Vitesse));

condition='elhyb==0';
distE=sum(Vitesse(eval(condition)).*pas(eval(condition)));

condition='elhyb~=0';
distH=sum(Vitesse(eval(condition)).*pas(eval(condition)));


fprintf(fid,'%s\n\n','            *** CARACTERISTIQUES DU VD.CYCLE ***');
fprintf(fid,'%s%6.1f%s\n',...
'  Duree totale du cycle : ',Temps(length(Temps)),' sec.');
if VD.CYCL.ntypcin~=4
    fprintf(fid,'%s\n',...
        '                             Cycle theorique    Cycle realise');
    fprintf(fid,'%s%6.3f%s%6.3f%s%4.2f%s\n',...
        '  Distance                     :   ',dist_dem/1000,' km     ',dist/1000,' km soit: ',100*distance(length(distance))/dist_dem,' %');
    fprintf(fid,'%s%6.1f%s%6.1f%s\n',...
        '  Duree deplacement            : ',tdep_dem,' s      ',tdep,' s');
    fprintf(fid,'%s%6.1f%s%6.1f%s\n',...
        '  Duree arret                  : ',tarret_dem,' s      ',tarret,' s');
    fprintf(fid,'%s%6.1f%s%6.1f%s\n',...
        '  Vitesse moyenne              : ',vmoy_dem*3.6,' km/h   ',vmoy*3.6,' km/h');
    fprintf(fid,'%s%6.1f%s%6.1f%s\n',...
        '  Vitesse moyenne deplacement  : ',vmoy_dep_dem*3.6,' km/h   ',vmoy_dep*3.6,' km/h');
    fprintf(fid,'%s%6.2f%s\n',...
        '  Acceleration moyenne         :     /          ',mean(acc(find(acc>0))),' m/s2');
    fprintf(fid,'%s%6.2f%s\n',...
        '  Acceleration maxi            :     /          ',accmax,' m/s2 ');
    fprintf(fid,'%s%6.2f%s\n',...
        '  Acceleration mini            :     /          ',accmin,' m/s2');
else
    fprintf(fid,'%s%6.3f%s\n',...
        '  Distance                     : ',dist/1000,' km');
    fprintf(fid,'%s%6.1f%s\n',...
        '  Duree deplacement            : ',tdep,' s');
    fprintf(fid,'%s%6.1f%s\n',...
        '  Duree arret                  : ',tarret,' s');
    fprintf(fid,'%s%6.1f%s\n',...
        '  Vitesse moyenne              : ',vmoy*3.6,' km/h');
    fprintf(fid,'%s%6.1f%s\n',...
        '  Vitesse moyenne deplacement  : ',vmoy_dep*3.6,' km/h');
    fprintf(fid,'%s%6.2f%s\n',...
        '  Acceleration moyenne         : ',mean(acc(find(acc>0))),' m/s2');
    fprintf(fid,'%s%6.2f%s\n',...
        '  Acceleration maxi            : ',accmax,' m/s2 ');
    fprintf(fid,'%s%6.2f%s\n',...
        '  Acceleration mini            : ',accmin,' m/s2');
end
if(max(elhyb)~=min(elhyb))
    fprintf(fid,'%s%6.3f%s\n',         '  Distance parcourue en mode electrique :  ',distE/1000,' km');
    fprintf(fid,'%s%6.3f%s\n',         '  Distance parcourue en mode hybride    :  ',distH/1000,' km');
end


