 %load res_NEDC_24_03_14 % avec trois obj
% load  res_NEDC_3polluants.mat
%load res_NEDC_4polluants
% load res_NEDC_3polluants_07_04_14
load res_NEDC_3polluants_l2nc_09_04_2014
%load res_NEDC_3polluants_10_04_2014.mat
PE=(NOx_tab/80+HC_tab/100+CO_tab/1000)*1000/(trapz(CYCL.temps,CYCL.vitesse)/1000);
%I=find(abs(Dsoc_tab)<0.1 & lambda2_tab<=0);
vec=1:1:length(Conso100_tab);
tab_res=[ vec' K_tab' lambda1_tab' (lambda2_tab')/10 Dsoc_tab' NOx_tab' HC_tab'  CO_tab' Conso100_tab' PE'];
I=find(abs(Dsoc_tab)<0.1 );

tab_res_I=[ vec(I)' K_tab(I)' lambda1_tab(I)' (lambda2_tab(I)')/10 Dsoc_tab(I)' NOx_tab(I)' HC_tab(I)'  CO_tab(I)' Conso100_tab(I)' PE(I)'];
%domin=non_domination_sort_mod_EV([ vec(I)' KNOx_tab(I)' lambda1_tab(I)' (lambda2_tab(I)')/10 Dsoc_tab(I)'    Conso100_tab(I)' NOx_tab(I)' ],2,5);
%domin=non_domination_sort_mod_EV([ vec(I)' K_tab(I)' lambda1_tab(I)' (lambda2_tab(I)')/10 Dsoc_tab(I)' NOx_tab(I)' HC_tab(I)'  CO_tab(I)' ...
%    Conso100_tab(I)' (NOx_tab(I)'/80+HC_tab(I)'/100+CO_tab(I)'/1000)],2,8);
domin=non_domination_sort_mod_EV(tab_res_I,2,8);
Imax= find(domin(:,11)==1,1,'last');
pareto=domin(1:Imax,:);

{'I' 'K' 'lambda1' 'lambda2/10' 'Dsoc' 'NOx' 'hc' 'CO'  'Conso' 'Obj' 'dominN' '?'}
pareto


PE_conv=10.285; % taux de polluant emis par le vehicule conventionnel ((NOx_tab/80+HC_tab/100+CO_tab/1000)*1000/(trapz(CYCL.temps,CYCL.vitesse)/1000));
h=figure
hold on
scatter(domin(:,9),domin(:,10)/PE_conv)
scatter(pareto(1:Imax,9),pareto(1:Imax,10)/PE_conv,'r')
% vehicule conventionnel
scatter(6.967,1,'sr','SizeData',80,'LineWidth',2)

I_K=[1 3 5 15 16];
scatter(pareto(I_K,9),pareto(I_K,10)/PE_conv,'sk','SizeData',80,'LineWidth',1)

% I1=find(domin(:,2)==0)
% scatter(domin(I1,9),domin(I1,10),'g')
% vehicule conventionnel

grid
xh=xlabel('fuel consumption in l/100 km','Fontsize',12,'FontName','Times')
yh=ylabel('Pollutant normalized emission','Fontsize',12,'FontName','Times')
lh=legend('cloud of points','Pareto optimal points','conventionnal vehicle','K=0.1 to 5',...
    'Fontsize',12,'FontName','Times')
set(lh,'Fontsize',12);
set(lh,'FontName','Times');
set(lh,'FontAngle','italic')
%set(h,'PaperPositionMode','auto');
%set(h,'position',[119 428 700 420]);
set(gca,'Fontsize',12,'FontName','Times')
axis([3.5 7.5 0 3])
% 
% I1=find(pareto(:,2)==0.1)
% scatter(pareto(I1,6),pareto(I1,7),'g')
% 
% 
% I1=find(pareto(:,2)==0.15)
% scatter(pareto(I1,6),pareto(I1,7),'m')
% 
% 
% I1=find(pareto(:,2)==0.2)
% scatter(pareto(I1,6),pareto(I1,7),'r')
% 
% I1=find(pareto(:,2)==0.3)
% scatter(pareto(I1,6),pareto(I1,7),'c')



