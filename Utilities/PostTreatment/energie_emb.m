% Function: energie_emb
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_emb(VD,temps, pas, vitdem, dist, psec, pprim, pprim_inertie, pveh, elehyb, vimin, numemb, fid, fid3)
%
% Depouillement des energies consommees par les pertes de l embrayage
% ------------------------------------------------------
function [] = energie_emb(VD,temps, pas, vitdem, dist, psec, pprim, pprim_inertie, pveh, elehyb, vimin, numemb, fid, fid3)


% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>=0';
QEMB_M=sum((pprim(eval(condition))-psec(eval(condition))).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb==0 & vitdem>vimin & pveh<0';
QEMB_R=sum((pprim(eval(condition))-psec(eval(condition))).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb==0 & vitdem<vimin';
QEMB_A=sum((pprim(eval(condition))-psec(eval(condition))).*pas(eval(condition))/3600);
QIN_A=sum(pprim_inertie(eval(condition)).*pas(eval(condition))/3600);

condition='elehyb==0 & vitdem<vimin & pprim_inertie>=0';
QIN_M=sum(pprim_inertie(eval(condition)).*pas(eval(condition))/3600);
condition='elehyb==0 & vitdem<vimin & pprim_inertie<0';
QIN_R=sum(pprim_inertie(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>=0';
QEMB_MH=sum((pprim(eval(condition))-psec(eval(condition))).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb~=0 & vitdem>vimin & pveh<0';
QEMB_RH=sum((pprim(eval(condition))-psec(eval(condition))).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb~=0 & vitdem<vimin';
QEMB_AH=sum((pprim(eval(condition))-psec(eval(condition))).*pas(eval(condition))/3600);
QIN_AH=sum(pprim_inertie(eval(condition)).*pas(eval(condition))/3600);

condition='elehyb~=0 & vitdem>vimin & pprim_inertie>=0';
QIN_MH=sum(pprim_inertie(eval(condition)).*pas(eval(condition))/3600);
condition='elehyb~=0 & vitdem>vimin & pprim_inertie<0';
QIN_RH=sum(pprim_inertie(eval(condition)).*pas(eval(condition))/3600);

% EN Wh
if numemb==1
    if(max(elehyb)==0)
        ecr_energie('sum',fid, 'Patinage+Inertie prim.  : ', [QEMB_M QEMB_R]);
        ecr_energie('separateur',fid, '', zeros(3,1));
        ecr_energie('nothing',fid, 'Dont Inertie arbre prim : ', [QIN_M QIN_R]);
        ecr_energie('separateur',fid, '', zeros(3,1));
    else
        ecr_energie('sum',fid, 'Patinage+Inertie prim.  : ', [QEMB_M QEMB_MH QEMB_R QEMB_RH]);
        ecr_energie('separateur',fid, '', zeros(6,1));
        ecr_energie('nothing',fid, 'Dont Inertie arbre prim : ', [QIN_M QIN_MH QIN_R QIN_RH]);
        ecr_energie('separateur',fid, '', zeros(6,1));
    end
else
    if(max(elehyb)==0)
        ecr_energie('sum',fid, 'Patinage+Inertie prim.2 : ', [QEMB_M QEMB_R]);
        ecr_energie('separateur',fid, '', zeros(3,1));
        ecr_energie('nothing',fid, 'Dont Inertie arbre prim2: ', [QIN_M QIN_R]);
        ecr_energie('separateur',fid, '', zeros(3,1));
    else
        ecr_energie('sum',fid, 'Patinage+Inertie prim.2 : ', [QEMB_M QEMB_MH QEMB_R QEMB_RH]);
        ecr_energie('separateur',fid, '', zeros(6,1));
        ecr_energie('nothing',fid, 'Dont Inertie arbre prim2: ', [QIN_M QIN_MH QIN_R QIN_RH]);
        ecr_energie('separateur',fid, '', zeros(6,1));
    end
end

% EN Wh/km
if numemb==1
    if(max(elehyb)==0)
        ecr_energie('sum',fid3, 'Patinage+Inertie prim.  : ', [QEMB_M QEMB_R]/(dist/1000));
        ecr_energie('separateur',fid3, '', zeros(3,1));
        ecr_energie('nothing',fid3,'Dont Inertie arbre prim : ', [QIN_M QIN_R]/(dist/1000));
        ecr_energie('separateur',fid3, '', zeros(3,1));
    else
        ecr_energie('sum',fid3, 'Patinage+Inertie prim.  : ', [QEMB_M QEMB_MH QEMB_R QEMB_RH]/(dist/1000));
        ecr_energie('separateur',fid3, '', zeros(6,1));
        ecr_energie('nothing',fid3, 'Dont Inertie arbre prim : ', [QIN_M QIN_MH QIN_R QIN_RH]/(dist/1000));
        ecr_energie('separateur',fid3, '', zeros(6,1));
    end
else
    if(max(elehyb)==0)
        ecr_energie('sum',fid3, 'Patinage+Inertie prim.2 : ', [QEMB_M QEMB_R]/(dist/1000));
        ecr_energie('separateur',fid3, '', zeros(3,1));
        ecr_energie('nothing',fid3,'Dont Inertie arbre prim2: ', [QIN_M QIN_R]/(dist/1000));
        ecr_energie('separateur',fid3, '', zeros(3,1));
    else
        ecr_energie('sum',fid3, 'Patinage+Inertie prim.2 : ', [QEMB_M QEMB_MH QEMB_R QEMB_RH]/(dist/1000));
        ecr_energie('separateur',fid3, '', zeros(6,1));
        ecr_energie('nothing',fid3, 'Dont Inertie arbre prim2: ', [QIN_M QIN_MH QIN_R QIN_RH]/(dist/1000));
        ecr_energie('separateur',fid3, '', zeros(6,1));
    end
end
