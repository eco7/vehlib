% Function: dep_masse
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = dep_masse(VD, masse, fid);
%
% Realisation du bilan de masse statique du vehicule
% version 1: juin 2007
% ------------------------------------------------------
function [] = dep_masse(VD, masse, fid)

fprintf(fid,'\n%s\n\n','          *** BILAN DE MASSE STATIQUE DU VEHICULE ***');

% Ecriture du bilan de masse
mveh=VD.VEHI.Mveh;
cmveh=num2str(round(mveh*10)/10);
cmveh=[cmveh blanks(7-length(cmveh))];

mcharge=VD.VEHI.Charge;
cmcharge=num2str(round(mcharge*10)/10);
cmcharge=[cmcharge blanks(7-length(cmcharge))];

if isfield(VD,'BATT')
    mbat=VD.BATT.Nblocser*VD.BATT.Nbranchepar*VD.BATT.Masse_bloc;
    cmbat=num2str(round(mbat*10)/10);
    cmbat=[cmbat blanks(7-length(cmbat))];
else
    mbat=0;
    cmbat='rien';
end

if isfield(VD,'BATT_BT')
    mbat_bt=VD.BATT_BT.Nblocser*VD.BATT_BT.Nbranchepar*VD.BATT_BT.Masse_bloc;
    cmbat_bt=num2str(round(mbat_bt*10)/10);
    cmbat_bt=[cmbat_bt blanks(7-length(cmbat_bt))];
else
    mbat_bt=0;
    cmbat_bt='rien';
end

if isfield(VD,'SC')
    msc=VD.SC.Nblocser*VD.SC.Nbranchepar*VD.SC.Masse_cell;
    cmsc=num2str(round(msc*10)/10);
    cmsc=[cmsc blanks(7-length(cmsc))];
else
    msc=0;
    cmsc='rien';
end
if isfield(VD,'ACM1')
    macm1=VD.VEHI.nbacm1*VD.ACM1.Masse_mg;
    cmacm1=num2str(round(macm1*10)/10);
    cmacm1=[cmacm1 blanks(7-length(cmacm1))];
else
    macm1=0;
    cmacm1='rien';
end

if isfield(VD,'MOTH')
    mmth=VD.MOTH.Masse_mth;
    cmmth=num2str(round(mmth*10)/10);
    cmmth=[cmmth blanks(7-length(cmmth))];
else
    mmth=0;
    cmmth='rien';
end

if isfield(VD,'ACM2')
    macm2=VD.ACM2.Masse_mg;
    cmacm2=num2str(round(macm2*10)/10);
    cmacm2=[cmacm2 blanks(7-length(cmacm2))];
else
    macm2=0;
    cmacm2='rien';
end

if isfield(VD,'MH')
    mmh=VD.MH.Masse_mh*VD.VEHI.nbmh;
    cmmh=num2str(round(mmh*10)/10);
    cmmh=[cmmh blanks(7-length(cmmh))];
else
    mmh=0;
    cmmh='rien';
end

if isfield(VD,'RH1')
    mrh1=VD.RH1.Masse*VD.VEHI.nbaccus_hp;
    cmrh1=num2str(round(mrh1*10)/10);
    cmrh1=[cmrh1 blanks(7-length(cmrh1))];
else
    mrh1=0;
    cmrh1='rien';
end

if isfield(VD,'RH2')
    mrh2=VD.RH2.Masse*VD.VEHI.nbaccus_bp;
    cmrh2=num2str(round(mrh2*10)/10);
    cmrh2=[cmrh2 blanks(7-length(cmrh2))];
else
    mrh2=0;
    cmrh2='rien';
end

mtot=mveh+mcharge+mbat+mbat_bt+macm1+mmth+macm2+msc+mmh+mrh1+mrh2;
cmtot=num2str(round(mtot*10)/10);
cmtot=[cmtot blanks(7-length(cmtot))];

banniere='        Totale | Vehi.  | Charge |';
smasse=['        ',cmtot,'| ',cmveh,'| ',cmcharge];

if ~strcmp(cmbat,'rien')
    banniere=[banniere,' Batt.  |'];
    smasse=[smasse,'| ',cmbat];
end

if ~strcmp(cmbat_bt,'rien')
    banniere=[banniere,' Batt.BT|'];
    smasse=[smasse,'| ',cmbat_bt];
end

if ~strcmp(cmsc,'rien')
    banniere=[banniere,' SC.    |'];
    smasse=[smasse,'| ',cmsc];
end

if ~strcmp(cmacm1,'rien')
    banniere=[banniere,' acm1.  |'];
    smasse=[smasse,'| ',cmacm1];
end

if ~strcmp(cmmth,'rien')
    banniere=[banniere,' moth.  |'];
    smasse=[smasse,'| ',cmmth];
end

if ~strcmp(cmacm2,'rien')
    banniere=[banniere,' acm2.  |'];
    smasse=[smasse,'| ',cmacm2];
end

if ~strcmp(cmmh,'rien')
    banniere=[banniere,'  mh.   |'];
    smasse=[smasse,'| ',cmmh];
end

if ~strcmp(cmrh1,'rien')
    banniere=[banniere,'  rh1.  |'];
    smasse=[smasse,'| ',cmrh1];
end

if ~strcmp(cmrh2,'rien')
    banniere=[banniere,'  rh2.  |'];
    smasse=[smasse,'| ',cmrh2];
end


if min(masse)~=max(masse)
    fprintf(fid,'La charge du vehicule a change pendant le calcul');
end
if min(masse)==max(masse) && masse(1)~=mtot
    fprintf(fid,'\n\nATTENTION: Probleme dans le calcul de la masse du vehicule\n\n');
    fprintf(fid,'%s%f\n','Masse_bil vaut: ',masse(1));
end

fprintf(fid,'%s\n',banniere(1:length(banniere)-1));
fprintf(fid,'%s\n\n',smasse);


