% File: classe_tri

%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% ------------------------------------------------------
% Mfile: classe_tri
%
% Semble faire un tri et constituer des classes
%
% ------------------------------------------------------

var=ibat.*ubat/1000;
varclasse=[-15:0.5:-0.5 0.5:0.5:10]
%varclasse=[-0.5 0.5];
duree=[0.05:0.1:10];

freq=10;




effectif=zeros(length(duree),length(varclasse));




for k=1:length(varclasse)
k
    ind1=[];
    if varclasse(k)>0
        ind1=find(var>=varclasse(k));
        %length(ind1)
    else
        ind1=find(var<=varclasse(k));
       varclasse_k= varclasse(k)
       len_ind1=length(ind1)
    end
   if  len_ind1==1     
    varduree=0;
    varduree=varduree+1/freq;
                for j=length(duree):-1:1
                    if varduree>duree(j)
                        effectif(j,k)=effectif(j,k)+1;
                        break
                    end
                end
    end 
    
    varduree=0.1;
    for i=2:length(ind1)-1

        if (ind1(i)-ind1(i-1))==1
            varduree=varduree+1/freq;
        else
            while (varduree>0)
                for j=length(duree):-1:1
                    if varduree>duree(j)
                        effectif(j,k)=effectif(j,k)+1;
                        break
                    end
                end
                varduree=varduree-max(duree(j),min(duree));
            end
            varduree=0.1;
        end

    end
end

surf(varclasse,duree,effectif)
 shading interp
 xlabel('Puissance batterie kW')
 ylabel('Temps successifs s')
 zlabel('Occurence /')