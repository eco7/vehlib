% Function: dep_acc
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = dep_acc(VD,temps, pas, vit, iacc, uacc, fid)
%
% Resultat accessoires electriques.
%   Puissance electrique consommee
%
% VD.ACC.ntypacc=1
% version 1: juin 2003
% ------------------------------------------------------
function [] = dep_acc(VD,temps, pas, vit, iacc, uacc, fid)

fprintf(fid,'\n%s\n\n','          *** ACCESSOIRES ELECTRIQUES ***');

fprintf(fid,'%s%6.2f%s\n',...
   '  Courant moyen des accessoires                   : ',mean(iacc),' A   ');
fprintf(fid,'%s%6.2f%s\n',...
   '  Tension moyenne des accessoires                 : ',mean(uacc),' V   ');
fprintf(fid,'%s%6.2f%s\n',...
   '  Consommation electrique moyenne des accessoires : ',mean(iacc.*uacc)/1000,' kW   ');
