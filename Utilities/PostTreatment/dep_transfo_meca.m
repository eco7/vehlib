% Function: dep_transfo_meca
% ---------------------------------------------------------------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = dep_transfo_meca(VD,temps, pas, vit, wsec_red, csec_red, wprim_red, cprim_red, chaine, vimin, fid)
%
%                                    VEHLIB
%                 Utilitaire de depouillement des resultats instantanes.
%         Depouillement des pertes pour un transformateur d'energie mecanique
%                            Independant du type de modele
% --------------------------------------------------------------------------------------------------------------
function [] = dep_transfo_meca(VD,temps, pas, vit, wsec_red, csec_red, wprim_red, cprim_red, chaine, vimin, fid)

pmec_sec=wsec_red.*csec_red;
pmec_prim=wprim_red.*cprim_red;

% Phases motrices.
condition='vit>vimin & pmec_sec>0 & pmec_prim>0';
tmot=sum(pas(eval(condition)));
if(tmot~=0)
   psec_moymot=sum(pmec_sec(eval(condition)).*pas(eval(condition)))/tmot;
   pprim_moymot=sum(pmec_prim(eval(condition)).*pas(eval(condition)))/tmot;
else
   psec_moymot=0;
   pprim_moymot=0;
end

% Phases recup.
condition='vit>vimin & pmec_sec<0 & pmec_prim<0';
trec=sum(pas(eval(condition)));
if(tmot~=0)
   psec_moyrec=sum(pmec_sec(eval(condition)).*pas(eval(condition)))/trec;
   pprim_moyrec=sum(pmec_prim(eval(condition)).*pas(eval(condition)))/trec;
else
   psec_moyrec=0;
   pprim_moyrec=0;
end

if(pprim_moymot~=0)
   rdmoymot=psec_moymot/pprim_moymot;
else
   rdmoymot=0;
end
if(psec_moyrec~=0)
   rdmoyrec=pprim_moyrec/psec_moyrec;
else
   rdmoyrec=0;
end

fprintf(fid,'\n%s\n\n',chaine);

fprintf(fid,'%s%s\n\n','           Phase motrice','       Phase recuperatrice');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Temps de fonct                : ',tmot,' s      ',trec,' s');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Puissance secondaire moyenne  : ',psec_moymot./1000,' kW     ',psec_moyrec./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Puissance primaire moyenne    : ',pprim_moymot./1000,' kW     ',pprim_moyrec./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Rendement moyen               : ',100.*rdmoymot,' %      ',100.*rdmoyrec,' %');
