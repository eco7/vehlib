% Function: energie_transfo_meca
% -----------------------------------------------------------------------------------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_transfo_meca(VD,temps, pas, vitdem, dist, csec_trans, wsec_trans, cprim_trans, wprim_trans, pveh, elehyb, chaine, vimin, fid, fid3)
%
%                                    VEHLIB
%          Utilitaire de post traitement des resultats instantanees
%
%  Depouillement des pertes d energies disspees dans un transformateur mecanique
% -----------------------------------------------------------------------------------------------------------------------------------
function [] = energie_transfo_meca(VD,temps, pas, vitdem, dist, csec_trans, wsec_trans, cprim_trans, wprim_trans, pveh, elehyb, chaine, vimin, fid, fid3)

% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>=0';
QTRANSFO_M=sum((cprim_trans(eval(condition)).*wprim_trans(eval(condition))- ...
csec_trans(eval(condition )).*wsec_trans(eval(condition ))).*pas(eval(condition))/3600);
FTRANSFOP_M=sum((cprim_trans(eval(condition)).*wprim_trans(eval(condition))).*pas(eval(condition))/3600);
FTRANSFOS_M=sum((csec_trans(eval(condition )).*wsec_trans(eval(condition ))).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb==0 & vitdem>vimin & pveh<0';
QTRANSFO_R=sum((cprim_trans(eval(condition)).*wprim_trans(eval(condition))- ...
csec_trans(eval(condition )).*wsec_trans(eval(condition ))).*pas(eval(condition))/3600);
FTRANSFOP_R=sum((cprim_trans(eval(condition)).*wprim_trans(eval(condition))).*pas(eval(condition))/3600);
FTRANSFOS_R=sum((csec_trans(eval(condition )).*wsec_trans(eval(condition ))).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb==0 & vitdem<vimin';
QTRANSFO_A=sum((cprim_trans(eval(condition)).*wprim_trans(eval(condition))- ...
csec_trans(eval(condition )).*wsec_trans(eval(condition ))).*pas(eval(condition))/3600);
FTRANSFOP_A=sum((cprim_trans(eval(condition)).*wprim_trans(eval(condition))).*pas(eval(condition))/3600);
FTRANSFOS_A=sum((csec_trans(eval(condition )).*wsec_trans(eval(condition ))).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>=0';
QTRANSFO_MH=sum((cprim_trans(eval(condition)).*wprim_trans(eval(condition))- ...
csec_trans(eval(condition )).*wsec_trans(eval(condition ))).*pas(eval(condition))/3600);
FTRANSFOP_MH=sum((cprim_trans(eval(condition)).*wprim_trans(eval(condition))).*pas(eval(condition))/3600);
FTRANSFOS_MH=sum((csec_trans(eval(condition )).*wsec_trans(eval(condition ))).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb~=0 & vitdem>vimin & pveh<0';
QTRANSFO_RH=sum((cprim_trans(eval(condition)).*wprim_trans(eval(condition))- ...
csec_trans(eval(condition )).*wsec_trans(eval(condition ))).*pas(eval(condition))/3600);
FTRANSFOP_RH=sum((cprim_trans(eval(condition)).*wprim_trans(eval(condition))).*pas(eval(condition))/3600);
FTRANSFOS_RH=sum((csec_trans(eval(condition )).*wsec_trans(eval(condition ))).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb~=0 & vitdem<vimin';
QTRANSFO_AH=sum((cprim_trans(eval(condition)).*wprim_trans(eval(condition))- ...
csec_trans(eval(condition )).*wsec_trans(eval(condition ))).*pas(eval(condition))/3600);
FTRANSFOP_AH=sum((cprim_trans(eval(condition)).*wprim_trans(eval(condition))).*pas(eval(condition))/3600);
FTRANSFOS_AH=sum((csec_trans(eval(condition )).*wsec_trans(eval(condition ))).*pas(eval(condition))/3600);

% La chaine de caractere fait au maximum 26 caracteres, soit 24+2
if length(chaine)>26
    'la chaine est tronque dans energie_transfo_meca'
    chaine=chaine(1:26);
else
    chaine=[chaine blanks(24-length(chaine)) ': '];
    chaine=chaine(1:26);
end

% EN Wh
if(max(elehyb)==0)
 ecr_energie('sum',fid,chaine, [QTRANSFO_M QTRANSFO_R]);
%  fprintf(fid,'%s%s\n',...
%   '                           -----------','            -----------');
else
 ecr_energie('sum',fid,chaine, [QTRANSFO_M QTRANSFO_MH QTRANSFO_R QTRANSFO_RH]);
end

% EN Wh% EN Wh/km
if(max(elehyb)==0)
 ecr_energie('sum',fid3,chaine, [QTRANSFO_M QTRANSFO_R]/(dist/1000));
%  fprintf(fid3,'%s%s\n',...
%   '                           -----------','            -----------');
else
 ecr_energie('sum',fid3,chaine, [QTRANSFO_M QTRANSFO_MH QTRANSFO_R QTRANSFO_RH]/(dist/1000));
end

% Ecriture de la structure de flux si existante
if evalin('base','isstruct(flux)')
    flux=evalin('base','flux');
    j=length(flux);
    
    j=j+1;
    flux(j).nom='FTRANSFO_P_M';
    flux(j).type='';
    flux(j).valeur=FTRANSFOP_M+FTRANSFOP_MH;
    flux(j).position=[0.53 0.34];
    flux(j).couleur='black';
    j=j+1;
    flux(j).nom='FTRANSFO_P_R';
    flux(j).type='';
    flux(j).valeur=FTRANSFOP_R+FTRANSFOP_RH;
    flux(j).position=[0.53 0.4];
    flux(j).couleur='green';
    j=j+1;
    flux(j).nom='FTRANSFO_S_M';
    flux(j).type='';
    flux(j).valeur=FTRANSFOS_M+FTRANSFOS_MH;
    flux(j).position=[0.68 0.34];
    flux(j).couleur='black';
    j=j+1;
    flux(j).nom='FTRANSFO_S_R';
    flux(j).type='';
    flux(j).valeur=FTRANSFOS_R+FTRANSFOS_RH;
    flux(j).position=[0.68 0.4];
    flux(j).couleur='green';
    

   assignin('base','flux',flux);
end


