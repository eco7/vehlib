% Function: dep_cplhyd
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [qcplhyd] = dep_cplhyd(VD,temps, pas, vit, wconv, cs_conv, wmt, ce_conv, pont_conv, rappvit, vimin, fid)
%
% Arg de retour:
% qcplhyd:    Pertes (W) du coupleur
% Resultat coupleur hydraulique.
% ------------------------------------------------------
function [qcplhyd] = dep_cplhyd(VD,temps, pas, vit, wconv, cs_conv, wmt, ce_conv, pont_conv, rappvit, vimin, fid)

pturb=wconv.*cs_conv;
ppompe=wmt.*ce_conv;

%Pertes du coupleur hydraulique
qcplhyd=ppompe-pturb;

% puissances moyennes et rendement
condition='vit>vimin & pont_conv==0 & rappvit==1 & pturb>0 & ppompe>0';
t=sum(pas(eval(condition)));
if(t~=0)
   ppomp_moy=sum(ppompe(eval(condition)).*pas(eval(condition)))/t;
   pturb_moy=sum(pturb(eval(condition)).*pas(eval(condition)))/t;
else
   ppomp_moy=0;
   pturb_moy=0;
end

if(ppomp_moy~=0)
   rdmoy=pturb_moy/ppomp_moy;
else
   rdmoy=0;
end

fprintf(fid,'\n%s\n\n','          *** CARACTERISTIQUES VD.CONVERTISSEUR (en 1ere) ***');

fprintf(fid,'%s%6.2f%s\n',...
'  Temps de fonct                : ',t,' s      ');
fprintf(fid,'%s%6.2f%s\n',...
'  Puissance sec. conv moy(VD.BV)   : ',pturb_moy./1000,' kW     ');
fprintf(fid,'%s%6.2f%s\n',...
'  Puissance prim. conv moy(MT)  : ',ppomp_moy./1000,' kW     ');
fprintf(fid,'%s%6.2f%s\n',...
'  Rendement moyen               : ',100.*rdmoy,' %      ');
