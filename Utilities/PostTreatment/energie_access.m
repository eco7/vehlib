% Function: energie_access
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_access(VD,temps, pas, vitdem, dist, pveh, iacc, uacc, elehyb, vimin, fid, fid3)
%
% Depouillement des energies consommees par les accessoires electriques
% ------------------------------------------------------
function [] = energie_access(VD,temps, pas, vitdem, dist, pveh, iacc, uacc, elehyb, vimin, fid, fid3)

pacc=uacc.*iacc;

% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>= 0';
FACC_M=sum(pacc(eval(condition)).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb==0 & vitdem>vimin & pveh< 0';
FACC_R=sum(pacc(eval(condition)).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb==0 & vitdem<vimin';
FACC_A=sum(pacc(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>= 0';
FACC_MH=sum(pacc(eval(condition)).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb~=0 & vitdem>vimin & pveh< 0';
FACC_RH=sum(pacc(eval(condition)).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb~=0 & vitdem<vimin';
FACC_AH=sum(pacc(eval(condition)).*pas(eval(condition))/3600);

% en Wh.
if(max(elehyb)==0)
 ecr_energie('sum',fid, 'Energie accessoires     : ', [FACC_M FACC_R FACC_A]);
else
 ecr_energie('sum',fid, 'Energie accessoires     : ', [FACC_M FACC_MH FACC_R FACC_RH FACC_A FACC_AH]);
end   

% en Wh/km.
if(max(elehyb)==0)
 ecr_energie('sum',fid3, 'Energie accessoires     : ', [FACC_M FACC_R FACC_A]/(dist/1000));
else
 ecr_energie('sum',fid3, 'Energie accessoires     : ', [FACC_M FACC_MH FACC_R FACC_RH FACC_A FACC_AH]/(dist/1000));
end

% Ecriture de la structure de flux
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='FACCE_MA';
flux(j).type='';
flux(j).valeur=FACC_M+FACC_MH+FACC_A+FACC_AH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='FACCE_R';
flux(j).type='';
flux(j).valeur=FACC_R+FACC_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

assignin('base','flux',flux);
