% ------------------------------------------------------
% function [psc, Ah] = dep_sc(VD,Temps, pas, vit, Isc, Usc, pertes, vimin, fid)
%
% depouillement de la sc
% Version vectorisee juin 2003
% ------------------------------------------------------
function [psc, Ah] = dep_sc(VD,Temps, pas, vit, Isc, Usc, pertes, vimin, fid)

psc=Usc.*Isc;
Ah=cumtrapz(Temps,Isc)/3600;
psc_min=min(psc);
psc_max=max(psc);

if (Isc==0) 
    Uscmoy_T=max(Usc);
    Iscmoy_T=0;
    psc_moy_T=0;
    pertes_sc_moy_T=0;
    Ubmax_T=max(Usc);
    tUbmax_T=max(Temps);
    Ubmin_T=max(Usc);
    tUbmin_T=0;
    Ibmax_T=max(Isc);
    tIbmax_T=max(Temps);
    Ibmin_T=max(Isc);
    tIbmin_T=0;
   
    Uscmoy_R=max(Usc);
    Iscmoy_R=0;
    psc_moy_R=0;
    pertes_sc_moy_R=0;
    Ubmax_R=max(Usc);
    tUbmax_R=0;
    Ubmin_R=max(Usc);
    tUbmin_R=0;
    Ibmax_R=max(Isc);
    tIbmax_R=0;
    Ibmin_R=max(Isc);
    tIbmin_R=0;

    Uscmoy_A=max(Usc);
    psc_moy_A=0;
    pertes_sc_moy_A=0;
    Iscmoy_A=0;
    Ubmax_A=max(Usc);
    tUbmax_A=0;
    Ubmin_A=max(Usc);
    tUbmin_A=0;
    Ibmax_A=max(Isc);
    tIbmax_A=0;
    Ibmin_A=max(Isc);
    tIbmin_A=0;
    
    tm=max(Temps);
    tr=0;
    ta=0;
else
    Ah=cumtrapz(Temps,Isc)/3600;
    psc_min=min(psc);
    psc_max=max(psc);

    % Traction
    %%%%%%%%%%%%%
    Ubmin_T=min(Usc(Isc>0 & vit>vimin));
    tUbmin_T=Temps(find(Usc==Ubmin_T));

    Ibmin_T=min(Isc(Isc>0 & vit>vimin));
    tIbmin_T=Temps(find(Isc==Ibmin_T));

    Ubmax_T=max(Usc(Isc>0 & vit>vimin));
    tUbmax_T=Temps(find(Usc==Ubmax_T));

    Ibmax_T=max(Isc(Isc>0 & vit>vimin));
    tIbmax_T=Temps(find(Isc==Ibmax_T));

    tm=sum(pas(Isc>0 & vit>vimin));
    if(tm~=0)
       Uscmoy_T=sum(Usc(Isc>0 & vit>vimin).*pas(Isc>0 & vit>vimin))./tm;
       Iscmoy_T=sum(Isc(Isc>0 & vit>vimin).*pas(Isc>0 & vit>vimin))./tm;
       psc_moy_T=sum(psc(psc>0 & vit>vimin).*pas(psc>0 & vit>vimin))./tm;
	pertes_sc_moy_T=sum(pertes(psc>=0 & vit>=vimin).*pas(psc>=0 & vit>=vimin))./tm;
    else
       Uscmoy_T=0;
       Iscmoy_T=0;
       psc_moy_T=0;
        pertes_sc_moy_T=0;
    end

    % Recup
    %%%%%%%%%
    Ubmin_R=min(Usc(Isc<0 & vit>vimin));
    tUbmin_R=Temps(find(Usc==Ubmin_R));

    Ibmin_R=min(Isc(Isc<0 & vit>vimin));
    tIbmin_R=Temps(find(Isc==Ibmin_R));

    Ubmax_R=max(Usc(Isc<0 & vit>vimin));
    tUbmax_R=Temps(find(Usc==Ubmax_R));

    Ibmax_R=max(Isc(Isc<0 & vit>vimin));
    tIbmax_R=Temps(find(Isc==Ibmax_R));

    tr=sum(pas(Isc<0 & vit>vimin));
    if(tr~=0)
       Uscmoy_R=sum(Usc(Isc<0 & vit>vimin).*pas(Isc<0 & vit>vimin))./tr;
       Iscmoy_R=sum(Isc(Isc<0 & vit>vimin).*pas(Isc<0 & vit>vimin))./tr;
       psc_moy_R=sum(psc(psc<0 & vit>vimin).*pas(psc<0 & vit>vimin))./tr;
	  pertes_sc_moy_R=sum(pertes(psc<0 & vit>=vimin).*pas(psc<0 & vit>=vimin))./tr;
    else
       Uscmoy_R=0;
       psc_moy_R=0;
       Iscmoy_R=0;
      pertes_sc_moy_R=0;
    end

    % Arret
    %%%%%%%%%%
    Ubmin_A=min(Usc(vit<vimin));
    tUbmin_A=Temps(find(Usc==Ubmin_A));

    Ibmin_A=min(Isc(vit<vimin));
    tIbmin_A=Temps(find(Isc==Ibmin_A));
    if length(tIbmin_A)>1
       tIbmin_A=tIbmin_A(1);
    end

    Ubmax_A=max(Usc(vit<vimin));
    tUbmax_A=Temps(find(Usc==Ubmax_A));

    Ibmax_A=max(Isc(vit<vimin));
    tIbmax_A=Temps(find(Isc==Ibmax_A));
    if length(tIbmax_A)>1
       tIbmax_A=tIbmax_A(1);
    end

    ta=sum(pas(vit<vimin));
    if(ta~=0)
       Uscmoy_A=sum(Usc(vit<vimin).*pas(vit<vimin))./ta;
       Iscmoy_A=sum(Isc(vit<vimin).*pas(vit<vimin))./ta;
       psc_moy_A=sum(psc(vit<vimin).*pas(vit<vimin))./ta;
	    pertes_sc_moy_A=sum(pertes(vit<vimin).*pas(vit<vimin))./ta;
    else
       Uscmoy_A=0;
       psc_moy_A=0;
       Iscmoy_A=0;
        pertes_sc_moy_A=0;
    end

    %Corrrection si on retrouve la valeur de depart
    if(isempty(Ubmin_T))
       Ubmax_T=0;
       tUbmax_T=0;
       Ubmin_T=0;
       tUbmin_T=0;
       Ibmax_T=0;
       tIbmax_T=0;
       Ibmin_T=0;
       tIbmin_T=0;
    end
    if(isempty(Ubmin_R))
       Ubmax_R=0;
       tUbmax_R=0;
       Ubmin_R=0;
       tUbmin_R=0;
       Ibmax_R=0;
       tIbmax_R=0;
       Ibmin_R=0;
       tIbmin_R=0;
    end
    if(isempty(Ubmin_A))
       Ubmax_A=0;
       tUbmax_A=0;
       Ubmin_A=0;
       tUbmin_A=0;
       Ibmax_A=0;
       tIbmax_A=0;
       Ibmin_A=0;
       tIbmin_A=0;
    end
end

% Rendement


fprintf(fid,'\n%s\n\n', ...
             '          *** CARACTERISTIQUES SUPER CONDENSATEUR ***');
fprintf(fid,'%s%s%s\n\n',...
   '                    Phase motrice','          Phase r?cup?ratrice'  ,...
   '    Phase d''arret');
fprintf(fid,'%s%5.1f%s%5.1f%s%5.1f%s\n', ...
             '  Temps de fonct  :   ',tm,             ' s                 ',tr,' s               ',ta,' s');
fprintf(fid,'%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s\n', ...
   '  Tension maximum :   ',Ubmax_T,  ' V ',tUbmax_T(1), ' s        ',Ubmax_R,  ' V ',tUbmax_R(1),' s      ', ...
   Ubmax_A,  ' V ',tUbmax_A(1),' s');
fprintf(fid,'%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s\n', ...
   '  Tension minimum :   ',Ubmin_T,  ' V ',tUbmin_T(1), ' s        ',Ubmin_R,  ' V ',tUbmin_R(1),' s      ', ...
   Ubmin_A,  ' V ',tUbmin_A(1),' s ');
fprintf(fid,'%s%6.2f%s%6.2f%s%6.2f%s\n', ...
             '  Tension moyenne :   ',Uscmoy_T,' V                ',Uscmoy_R,' V              ',Uscmoy_A,' V ');
fprintf(fid,'%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s\n', ...
   '  Courant maximum :   ',Ibmax_T,  ' A ',tIbmax_T(1), ' s        ',Ibmax_R,  ' A ',tIbmax_R(1),' s      ', ...
   Ibmax_A,  ' A ',tIbmax_A,' s');
fprintf(fid,'%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s\n', ...
   '  Courant minimum :   ',Ibmin_T,  ' A ',tIbmin_T(1), ' s        ',Ibmin_R,  ' A ',tIbmin_R(1),' s      ', ...
   Ibmin_A,  ' A ',tIbmin_A(1),' s');
fprintf(fid,'%s%6.2f%s%6.2f%s%6.2f%s\n', ...
             '  Courant moyen   :   ',Iscmoy_T,' A                ',Iscmoy_R,' A              ',Iscmoy_A,' A ');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
   '  Puissance maxi  :   ',psc_max/1000,' kW               ',psc_min/1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s%6.2f%s\n', ...
   '  Puissance moy   :   ',psc_moy_T/1000,' kW               ',psc_moy_R/1000,' kW             ',psc_moy_A/1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s%6.2f%s\n', ...
   '  Pertes moy      :   ',pertes_sc_moy_T/1000,' kW               ',pertes_sc_moy_R/1000,' kW             ',pertes_sc_moy_A/1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s%6.2f%s\n', ...
   '  Rendement moy   :   ',100*psc_moy_T/(psc_moy_T+pertes_sc_moy_T),' %                ', ...
   100*(psc_moy_R+pertes_sc_moy_R)/psc_moy_R,' %              ',100*psc_moy_A/(psc_moy_A+pertes_sc_moy_A),' %');
fprintf(fid,'\n%s%6.2f%s\n', ...
             '  Consommation super condensateur  :   ',Ah(length(Temps)),' Ah');
