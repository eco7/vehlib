% Function: analyse_carburant
%--------------------------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%function [] = analyse_carburant(VD,temps, pas, vit, puis, dcarb, chaine, tolerance, vimin, fid)
%
% Script d'analyse de la consommation
% Repartition par phase suivant le signe de puis dans la tolerance 
%
% Arguments: en unite SI
%  dcarb en g/s
%  chaine est le titre du paragraphe
%  tolerance: scalaire pour distinction des phases
%
% Version 1 Dec 2005
%--------------------------------------------------------------------------
function [] = analyse_carburant(VD,temps, pas, vit, puis, dcarb, chaine, tolerance, vimin, fid)

tt=max(temps);
ccum=trapz(temps,dcarb);
ct=sum(ccum);

condition='puis>tolerance & vit>vimin';
c1=sum(dcarb(find(eval(condition))).*pas(find(eval(condition))));
t1=sum(pas(find(eval(condition))));

condition='puis<=(-1)*tolerance & vit>vimin';
c2=sum(dcarb(find(eval(condition))).*pas(find(eval(condition))));;
t2=sum(pas(find(eval(condition))));

%condition='puis<tolerance & puis>(-1)*tolerance';
condition='vit<vimin';
c3=sum(dcarb(find(eval(condition))).*pas(find(eval(condition))));;
t3=sum(pas(find(eval(condition))));

fprintf(fid,'\n\n        %s %s %s\n\n','*** ',chaine,'***');
fprintf(fid,'%s %5.1f\n','  Tolerance :',tolerance);

fprintf(fid,'%s\n','                        Ph mot  Ph recup   Ph arret    Total');

chaine1='Temps (s)           :';
fprintf(fid,'  %s  %5.1f   %5.1f      %5.1f      %5.1f\n',chaine1,t1,t2,t3,t1+t2+t3);

chaine1='Temps (%)           :';
fprintf(fid,'  %s  %5.1f   %5.1f      %5.1f      %5.1f\n',chaine1,100*t1/tt,100*t2/tt,100*t3/tt,100*(t1+t2+t3)/tt);

chaine1='Consommation (g)    :';
fprintf(fid,'  %s  %5.1f   %5.1f      %5.1f      %5.1f\n',chaine1,c1,c2,c3,c1+c2+c3);

chaine1='Consommation (%)    :';
fprintf(fid,'  %s  %5.1f   %5.1f      %5.1f      %5.1f\n',chaine1,100*c1/ct,100*c2/ct,100*c3/ct,100*(c1+c2+c3)/ct);

