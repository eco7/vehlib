% Function: energie_accm3
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_accm3(VD,temps, pas, vitdem, dist, paccm_alt, paccm_servo, paccm_air, paccm_ref, pveh, elehyb, vimin, fid, fid3)

% Version 1: Septembre 2004
% waccm, caccm: Vitesse et couple total consommes par les aux
% paccm_alt:    puis meca consomme par l alternateur
% paccm_servo:  puis meca consomme par la servo-direction
% paccm_air:    puis meca consomme par le compresseur d'air (portes, agenouillement).
% paccm_ref:    puis meca consomme par le ventilateur de refroidissement moteur (ex:pompe hydrostatique).
%
% VD.ACC.ntypacc=3
%
% Depouillement des energies consommees par les accessoires mecaniques
% ------------------------------------------------------
function [] = energie_accm3(VD,temps, pas, vitdem, dist, paccm_alt, paccm_servo, paccm_air, paccm_ref, pveh, elehyb, vimin, fid, fid3)


% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>= 0';
QALTM_M=sum(paccm_alt(eval(condition)).*pas(eval(condition))/3600);
QSERVO_M=sum(paccm_servo(eval(condition)).*pas(eval(condition))/3600);
QAIR_M=sum(paccm_air(eval(condition)).*pas(eval(condition))/3600);
QREF_M=sum(paccm_ref(eval(condition)).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb==0 & vitdem>vimin & pveh< 0';
QALTM_R=sum(paccm_alt(eval(condition)).*pas(eval(condition))/3600);
QSERVO_R=sum(paccm_servo(eval(condition)).*pas(eval(condition))/3600);
QAIR_R=sum(paccm_air(eval(condition)).*pas(eval(condition))/3600);
QREF_R=sum(paccm_ref(eval(condition)).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb==0 & vitdem<vimin';
QALTM_A=sum(paccm_alt(eval(condition)).*pas(eval(condition))/3600);
QSERVO_A=sum(paccm_servo(eval(condition)).*pas(eval(condition))/3600);
QAIR_A=sum(paccm_air(eval(condition)).*pas(eval(condition))/3600);
QREF_A=sum(paccm_ref(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>= 0';
QALTM_MH=sum(paccm_alt(eval(condition)).*pas(eval(condition))/3600);
QSERVO_MH=sum(paccm_servo(eval(condition)).*pas(eval(condition))/3600);
QAIR_MH=sum(paccm_air(eval(condition)).*pas(eval(condition))/3600);
QREF_MH=sum(paccm_ref(eval(condition)).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb~=0 & vitdem>vimin & pveh< 0';
QALTM_RH=sum(paccm_alt(eval(condition)).*pas(eval(condition))/3600);
QSERVO_RH=sum(paccm_servo(eval(condition)).*pas(eval(condition))/3600);
QAIR_RH=sum(paccm_air(eval(condition)).*pas(eval(condition))/3600);
QREF_RH=sum(paccm_ref(eval(condition)).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb~=0 & vitdem<vimin';
QALTM_AH=sum(paccm_alt(eval(condition)).*pas(eval(condition))/3600);
QSERVO_AH=sum(paccm_servo(eval(condition)).*pas(eval(condition))/3600);
QAIR_AH=sum(paccm_air(eval(condition)).*pas(eval(condition))/3600);
QREF_AH=sum(paccm_ref(eval(condition)).*pas(eval(condition))/3600);


% En Wh
if(max(elehyb)~=0)
 ecr_energie('sum',fid, 'Pertes alternateur meca : ', [QALTM_M QALTM_MH QALTM_R QALTM_RH QALTM_A QALTM_AH]);
 ecr_energie('sum',fid, 'Pertes servo direction  : ', [QSERVO_M QSERVO_MH QSERVO_R QSERVO_RH QSERVO_A QSERVO_AH]);
 ecr_energie('sum',fid, 'Pertes compresseur air  : ', [QAIR_M QAIR_MH QAIR_R QAIR_RH QAIR_A QAIR_AH]);
 ecr_energie('sum',fid, 'Pertes refroidissement  : ', [QREF_M QREF_MH QREF_R QREF_RH QREF_A QREF_AH]);
end

% En Wh/km
if(max(elehyb)~=0)
ecr_energie('sum',fid3, 'Pertes alternateur meca : ', [QALTM_M QALTM_MH QALTM_R QALTM_RH QALTM_A QALTM_AH]/(dist/1000));
 ecr_energie('sum',fid3, 'Pertes servo direction  : ', [QSERVO_M QSERVO_MH QSERVO_R QSERVO_RH QSERVO_A QSERVO_AH]/(dist/1000));
 ecr_energie('sum',fid3, 'Pertes compresseur air  : ', [QAIR_M QAIR_MH QAIR_R QAIR_RH QAIR_A QAIR_AH]/(dist/1000));
 ecr_energie('sum',fid3, 'Pertes refroidissement  : ', [QREF_M QREF_MH QREF_R QREF_RH QREF_A QREF_AH]/(dist/1000));
end
  
% Ecriture de la structure de flux si existante
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='FVD.ACCM_R';
flux(j).type='';
flux(j).valeur=QALTM_M+QALTM_MH+QSERVO_M+QSERVO_MH+QAIR_M+QAIR_MH+QREF_M+QREF_MH+ ...
QALTM_A+QALTM_AH+QSERVO_A+QSERVO_AH+QAIR_A+QAIR_AH+QREF_A+QREF_AH;
flux(j).position=[0.33 0.41];
flux(j).couleur='black';
j=j+1;
flux(j).nom='FVD.ACCM_R';
flux(j).type='';
flux(j).valeur=QALTM_R+QALTM_RH+QSERVO_R+QSERVO_RH+QAIR_R+QAIR_RH+QREF_R+QREF_RH;
flux(j).position=[0.33 0.48];
flux(j).couleur='green';

assignin('base','flux',flux);

