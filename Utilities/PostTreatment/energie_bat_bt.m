% Function: energie_bat_bt
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_bat_bt(VD,temps, pas, vitdem, dist, pbat, pveh, elehyb, vimin, fid, fid3)
%
% Depouillement des energies consommees par la batterie basse tension
% convention source
% ------------------------------------------------------
function [] = energie_bat_bt(VD,temps, pas, vitdem, dist, pbat, pveh, elehyb, vimin, fid, fid3)

% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>= 0';
FBAT_M=sum(pbat(eval(condition)).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb==0 & vitdem>vimin & pveh< 0';
FBAT_R=sum(pbat(eval(condition)).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb==0 & vitdem<vimin';
FBAT_A=sum(pbat(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>= 0';
FBAT_MH=sum(pbat(eval(condition)).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb~=0 & vitdem>vimin & pveh< 0';
FBAT_RH=sum(pbat(eval(condition)).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb~=0 & vitdem<vimin';
FBAT_AH=sum(pbat(eval(condition)).*pas(eval(condition))/3600);


% en Wh.
if(max(elehyb)==0)
 ecr_energie('substract',fid, 'Energie elec. bat BT    : ', [FBAT_M FBAT_R FBAT_A]);
else
 ecr_energie('substract',fid, 'Energie elec. bat BT    : ', [FBAT_M FBAT_MH FBAT_R FBAT_RH FBAT_A FBAT_AH]);
end

% en Wh/km.
if(max(elehyb)==0)
 ecr_energie('substract',fid3, 'Energie elec. bat BT    : ', [FBAT_M FBAT_R FBAT_A]/(dist/1000));
else
 ecr_energie('substract',fid3, 'Energie elec. bat BT    : ', [FBAT_M FBAT_MH FBAT_R FBAT_RH FBAT_A FBAT_AH]/(dist/1000));
end

% Ecriture de la structure de flux
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='FBAT_BT_MA';
flux(j).type='';
flux(j).valeur=FBAT_M+FBAT_MH+FBAT_A+FBAT_AH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='FBAT_BT_R';
flux(j).type='';
flux(j).valeur=FBAT_R+FBAT_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='green';

assignin('base','flux',flux);
