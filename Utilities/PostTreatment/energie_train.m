% Function: energie_train
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_train(VD,temps, pas, vitdem, dist, psec_trans, pprim_trans, pveh, elehyb, vimin, fid, fid3)
%
% Depouillement des pertes d energies disspees dans le train epicycloidal
% ------------------------------------------------------
function [] = energie_train(VD,temps, pas, vitdem, dist, psec_trans, pprim_trans, pveh, elehyb, vimin, fid, fid3)

% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>=0';
QTRAIN_M=sum((pprim_trans(eval(condition))-psec_trans(eval(condition))).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb==0 & vitdem>vimin & pveh<0';
QTRAIN_R=sum((pprim_trans(eval(condition))-psec_trans(eval(condition))).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb==0 & vitdem<vimin';
QTRAIN_A=sum((pprim_trans(eval(condition))-psec_trans(eval(condition))).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>=0';
QTRAIN_MH=sum((pprim_trans(eval(condition))-psec_trans(eval(condition))).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb~=0 & vitdem>vimin & pveh<0';
QTRAIN_RH=sum((pprim_trans(eval(condition))-psec_trans(eval(condition))).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb~=0 & vitdem<vimin';
QTRAIN_AH=sum((pprim_trans(eval(condition))-psec_trans(eval(condition))).*pas(eval(condition))/3600);

% EN Wh
if(max(elehyb)==0)
 ecr_energie('sum',fid, 'Pertes train epi        : ', VD.VEHI.nbacm1*[QTRAIN_M QTRAIN_R]);
 fprintf(fid,'%s%s\n',...
  '                           -----------','            -----------');
else
 ecr_energie('sum',fid, 'Pertes train epi        : ', VD.VEHI.nbacm1*[QTRAIN_M QTRAIN_MH QTRAIN_R QTRAIN_RH]);
end

% EN Wh% EN Wh/km
if(max(elehyb)==0)
 ecr_energie('sum',fid3, 'Pertes train epi        : ', VD.VEHI.nbacm1*[QTRAIN_M QTRAIN_R]/(dist/1000));
 fprintf(fid3,'%s%s\n',...
  '                           -----------','            -----------');
else
 ecr_energie('sum',fid3, 'Pertes train epi        : ', VD.VEHI.nbacm1*[QTRAIN_M QTRAIN_MH QTRAIN_R QTRAIN_RH]/(dist/1000));
end

