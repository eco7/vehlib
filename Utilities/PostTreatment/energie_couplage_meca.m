% Function: energie_couplage_meca
% -----------------------------------------------------------------------------------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_couplage_meca(VD,temps, pas, vitdem, dist, csec_cpl, wsec_cpl, cprim1_cpl, wprim1_cpl, cprim2_cpl, wprim2_cpl, pveh, elehyb, chaine, vimin, fid, fid3)
%
%                                    VEHLIB
%          Utilitaire de post traitement des resultats instantanees
%
%  Depouillement des pertes d energies disspees dans un systeme de couplage mecanique
%
% -----------------------------------------------------------------------------------------------------------------------------------
function [] = energie_couplage_meca(VD,temps, pas, vitdem, dist, csec_cpl, wsec_cpl, cprim1_cpl, wprim1_cpl, cprim2_cpl, wprim2_cpl, pveh, elehyb, chaine, vimin, fid, fid3)

% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>=0';
QCPL_M=sum((cprim1_cpl(eval(condition)).*wprim1_cpl(eval(condition))+cprim2_cpl(eval(condition)).*wprim2_cpl(eval(condition))- ...
csec_cpl(eval(condition )).*wsec_cpl(eval(condition ))).*pas(eval(condition))/3600);

% Phases recup
condition='elehyb==0 & vitdem>vimin & pveh<0';
QCPL_R=sum((cprim1_cpl(eval(condition)).*wprim1_cpl(eval(condition))+cprim2_cpl(eval(condition)).*wprim2_cpl(eval(condition))- ...
csec_cpl(eval(condition )).*wsec_cpl(eval(condition ))).*pas(eval(condition))/3600);

% Phases d arret
condition='elehyb==0 & vitdem<vimin';
QCPL_A=sum((cprim1_cpl(eval(condition)).*wprim1_cpl(eval(condition))+cprim2_cpl(eval(condition)).*wprim2_cpl(eval(condition))- ...
csec_cpl(eval(condition )).*wsec_cpl(eval(condition ))).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>=0';
QCPL_MH=sum((cprim1_cpl(eval(condition)).*wprim1_cpl(eval(condition))+cprim2_cpl(eval(condition)).*wprim2_cpl(eval(condition))- ...
csec_cpl(eval(condition )).*wsec_cpl(eval(condition ))).*pas(eval(condition))/3600);

% Phases recup
condition='elehyb~=0 & vitdem>vimin & pveh<0';
QCPL_RH=sum((cprim1_cpl(eval(condition)).*wprim1_cpl(eval(condition))+cprim2_cpl(eval(condition)).*wprim2_cpl(eval(condition))- ...
csec_cpl(eval(condition )).*wsec_cpl(eval(condition ))).*pas(eval(condition))/3600);

% Phases d arret
condition='elehyb~=0 & vitdem<vimin';
QCPL_AH=sum((cprim1_cpl(eval(condition)).*wprim1_cpl(eval(condition))+cprim2_cpl(eval(condition)).*wprim2_cpl(eval(condition))- ...
csec_cpl(eval(condition )).*wsec_cpl(eval(condition ))).*pas(eval(condition))/3600);

% La chaine de caractere fait au maximum 26 caracteres, soit 24+2
if length(chaine)>26
    'la chaine est tronque dans energie_couplage_meca'
    chaine=chaine(1:26);
else
    chaine=[chaine blanks(24-length(chaine)) ': '];
    chaine=chaine(1:26);
end

% EN Wh
if(max(elehyb)==0)
 ecr_energie('sum',fid,chaine, [QCPL_M QCPL_R]);
 fprintf(fid,'%s%s\n',...
  '                           -----------','            -----------');
else
 ecr_energie('sum',fid,chaine, [QCPL_M QCPL_MH QCPL_R QCPL_RH]);
end

% EN Wh% EN Wh/km
if(max(elehyb)==0)
 ecr_energie('sum',fid3,chaine, [QCPL_M QCPL_R]/(dist/1000));
 fprintf(fid3,'%s%s\n',...
  '                           -----------','            -----------');
else
 ecr_energie('sum',fid3,chaine, [QCPL_M QCPL_MH QCPL_R QCPL_RH]/(dist/1000));
end


