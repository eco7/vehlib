% Function: energie_convelec
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [pconv] = energie_convelec(VD,temps, pas, vitdem, dist, pveh, iconv, uconv, pbat, elehyb, vimin, fid, fid3)
%
% Depouillement des energies du convertisseur de tension
% ------------------------------------------------------
function [pconv] = energie_convelec(VD,temps, pas, vitdem, dist, pveh, iconv, uconv, pbat, elehyb, vimin, fid, fid3)

pconv=uconv.*iconv;

% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>= 0';
QVD.CONV_M=sum((pbat(eval(condition))-pconv(eval(condition))).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb==0 & vitdem>vimin & pveh< 0';
QVD.CONV_R=sum((pbat(eval(condition))-pconv(eval(condition))).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb==0 & vitdem<vimin';
QVD.CONV_A=sum((pbat(eval(condition))-pconv(eval(condition))).*pas(eval(condition))/3600);

% En mode hybride% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>= 0';
QVD.CONV_MH=sum((pbat(eval(condition))-pconv(eval(condition))).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb~=0 & vitdem>vimin & pveh< 0';
QVD.CONV_RH=sum((pbat(eval(condition))-pconv(eval(condition))).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb~=0 & vitdem<vimin';
QVD.CONV_AH=sum((pbat(eval(condition))-pconv(eval(condition))).*pas(eval(condition))/3600);

% EN Wh
if(max(elehyb)==0)
 ecr_energie('sum',fid, 'Pertes convertisseur    : ', [QVD.CONV_M QVD.CONV_R QVD.CONV_A]);
else
 ecr_energie('sum',fid, 'Pertes convertisseur    : ', [QVD.CONV_M QVD.CONV_MH QVD.CONV_R QVD.CONV_RH QVD.CONV_A QVD.CONV_AH]);
end

% EN Wh% EN Wh/km
if(max(elehyb)==0)
 ecr_energie('sum',fid3, 'Pertes convertisseur    : ', [QVD.CONV_M QVD.CONV_R QVD.CONV_A]/(dist/1000));
else
 ecr_energie('sum',fid3, 'Pertes convertisseur    : ', [QVD.CONV_M QVD.CONV_MH QVD.CONV_R QVD.CONV_RH QVD.CONV_A QVD.CONV_AH]/(dist/1000));
end

% Ecriture de la structure de flux
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='QVD.CONV_MA';
flux(j).type='';
flux(j).valeur=QVD.CONV_M+QVD.CONV_MH+QVD.CONV_A+QVD.CONV_AH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='QVD.CONV_R';
flux(j).type='';
flux(j).valeur=QVD.CONV_R+QVD.CONV_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

assignin('base','flux',flux);

