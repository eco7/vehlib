% Function: dep_accm2
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = dep_accm2(VD,temps, pas, vit, waccm1, caccm1, waccm2, caccm2, vimin, fid)
%
% Resultat accessoires mecaniques entraine par courroie.
%   Alternateur
%   Compresseur de climatisation
%
% VD.ACC.ntypacc=2
%
% version 1: juin 2003
% ------------------------------------------------------
function [] = dep_accm2(VD,temps, pas, vit, waccm1, caccm1, waccm2, caccm2, vimin, fid)

fprintf(fid,'\n%s\n\n','          *** ACCESSOIRES MECANIQUES ***');

fprintf(fid,'%s%6.2f%s\n',...
   '  Consommation mecanique moyenne des accessoires poulie alternateur: ',-1*mean(waccm1.*caccm1)/1000,' kW   ');
if VD.ACC.ntypacc==2
    fprintf(fid,'%s%6.2f%s\n',...
        '  Rendement moyen alternateur : ',100*VD.ACC.rdmoy_acm2,' %   ');
    fprintf(fid,'%s%6.2f%s\n',...
        '  Consommation electrique moyenne des accessoires : ',-1*mean(waccm1.*caccm1)/1000.*VD.ACC.rdmoy_acm2,' kW   ');
elseif VD.ACC.ntypacc==4
    rdmoy_acm2 = VD.ACC.Pacc_elec/mean(waccm1.*(-1).*caccm1);
    fprintf(fid,'%s%6.2f%s\n',...
        '  Rendement moyen alternateur : ',100*rdmoy_acm2,' %   ');
    fprintf(fid,'%s%6.2f%s\n',...
        '  Consommation electrique moyenne des accessoires : ',-1*mean(waccm1.*caccm1)/1000.*rdmoy_acm2,' kW   ');
end

fprintf(fid,'%s%6.2f%s\n',...
   '  Consommation mecanique moyenne des accessoires poulie compresseur: ',-1*mean(waccm2.*caccm2)/1000,' kW   ');
fprintf(fid,'%s%6.2f%s\n',...
   '  Consommation totale moyenne des accessoires: ',-1*mean(waccm1.*caccm1+waccm2.*caccm2)/1000,' kW   ');
