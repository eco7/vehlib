% ------------------------------------------------------
% function [] = energie_sc(VD,temps, pas, vitdem, dist, usc, psc, pveh, elehyb, vimin, fid, fid3)
%
% Depouillement des energies consommees par la batterie de super condensateur
% ------------------------------------------------------
function [] = energie_sc(VD,temps, pas, vitdem, dist, usc, psc, pveh, elehyb, vimin, fid, fid3)

% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>= 0';
FSC_M=sum(psc(eval(condition)).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb==0 & vitdem>vimin & pveh< 0';
FSC_R=sum(psc(eval(condition)).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb==0 & vitdem<vimin';
FSC_A=sum(psc(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>= 0';
FSC_MH=sum(psc(eval(condition)).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb~=0 & vitdem>vimin & pveh< 0';
FSC_RH=sum(psc(eval(condition)).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb~=0 & vitdem<vimin';
FSC_AH=sum(psc(eval(condition)).*pas(eval(condition))/3600);

% en Wh.
if(max(elehyb)==0)
fprintf(fid,'%s%s%s\n',...
  '                           -----------','            -----------','          -----------');
 ecr_energie('substract',fid, 'Energie elec. bsc       : ', [FSC_M FSC_R FSC_A]);
else
 fprintf(fid,'%s\n',...
 '                          -----------------------       ---------------------          ---------------------');
 ecr_energie('substract',fid, 'Energie elec. bsc       : ', [FSC_M FSC_MH FSC_R FSC_RH FSC_A FSC_AH]);
end

% en Wh/km.
if(max(elehyb)==0)
fprintf(fid3,'%s%s%s\n',...
  '                           -----------','            -----------','          -----------');
 ecr_energie('substract',fid3, 'Energie elec. bsc       : ', [FSC_M FSC_R FSC_A]/(dist/1000));
else
 fprintf(fid,'%s\n',...
 '                          -----------------------       ---------------------          ---------------------');
 ecr_energie('substract',fid3, 'Energie elec. bsc       : ', [FSC_M FSC_MH FSC_R FSC_RH FSC_A FSC_AH]/(dist/1000));
end

% Ecriture de la structure de flux
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='FSC_MA';
flux(j).type='';
flux(j).valeur=FSC_M+FSC_MH+FSC_R+FSC_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='FSC_R';
flux(j).type='';
flux(j).valeur=FSC_R+FSC_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='green';

assignin('base','flux',flux);
