% Function: dep_veh2
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [pveh] = dep_veh2(VD,Temps, pas, Vitesse, acc, force1, force2, force3, fpente, cinertie, vimin, fid)
%
% Pour modele vehicule type a+b*v+c*v*v.
% ------------------------------------------------------
function [pveh] = dep_veh2(VD,Temps, pas, Vitesse, acc, force1, force2, force3, fpente, cinertie, vimin, fid)

if nargin == 11
    fprintf(1,'%s\n','Gestion obsolescence-01/2016-Nombre d''argument incompatible, rajouter cinertie !');
end

% Calcul de la puiisance vehicule
finertie=cinertie/VD.VEHI.Rpneu;

%pveh=(froul+faero+fpente+cfrein_meca/VD.VEHI.Rpneu).*Vitesse;
pveh=(force1+force2+force3+fpente+finertie).*Vitesse;
ppente=fpente.*Vitesse;
pinertie=finertie.*Vitesse;

% Phases motrices.
condition='Vitesse>vimin & pveh>0 ';
tmot=sum(pas(eval(condition)));
if(tmot~=0)
   pveh_moymot=sum(pveh(eval(condition)).*pas(eval(condition)))/tmot;
   ppente_moymot=sum(ppente(eval(condition)).*pas(eval(condition)))/tmot;
   pinertie_moymot=sum(pinertie(eval(condition)).*pas(eval(condition)))/tmot;
else
   pveh_moymot=0;
   ppente_moymot=0;
   pinertie_moymot=0;
end

% Phases recup.
condition='Vitesse>vimin & pveh<0 ';
trec=sum(pas(eval(condition)));
if(trec~=0)
   pveh_moyrec=sum(pveh(eval(condition)).*pas(eval(condition)))/trec;
   ppente_moyrec=sum(ppente(eval(condition)).*pas(eval(condition)))/trec;
   pinertie_moyrec=sum(pinertie(eval(condition)).*pas(eval(condition)))/trec;
else
   pveh_moyrec=0;
   ppente_moyrec=0;
   pinertie_moyrec=0;
end


fprintf(fid,'\n%s\n\n','          *** CARACTERISTIQUES VEHICULE ***');

fprintf(fid,'%s%s\n\n',...
'                 Phase motrice','  Phase recuperatrice');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Temps de fonct  : ',tmot,' s      ',trec,' s');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Puis inertie moy: ',pinertie_moymot./1000,' kW     ',pinertie_moyrec./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Puiss. pente moy: ',ppente_moymot./1000,' kW     ',ppente_moyrec./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Puiss. veh moy  : ',pveh_moymot./1000,' kW     ',pveh_moyrec./1000,' kW');
