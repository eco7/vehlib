% File: trait_hp_bv_1emb
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Script de post traitement d'un modele de vehicule 
% Hybide parallele un embrayage et boite de vitesse dans VEHLIB
%
% ------------------------------------------------------

vimin=0.01;

%lecture et increment du numero de calcul
fnum=strcat(VD.INIT.InitialFolder,VD.INIT.sep,'numcal.mat');
load(fnum);


% ecriture du fichier de resultats synthetiques
fid=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF1'),'w');
fid2=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF2'),'w');
fid3=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF3'),'w');

fprintf(fid,'\n\t%s\n',['Post-traitement Veh hyb. parallele 1 emb. pour modele VEHLIB- VERSION ',VD.INIT.version]);
fprintf(fid,'\n\t%s\n','------------------------------------------------------------------------------');
fprintf(fid,'\n%s %s','Date de traitement du fichier          :  ',date);
fprintf(fid,'\n%s %4d\n\n','Numero de calcul                       :  ',numcal);
fprintf(fid2,'\n\t%s\n',['Post-traitement Veh hyb. parallele 1 emb. pour modele VEHLIB- VERSION ',VD.INIT.version]);
fprintf(fid2,'\n\t%s\n','-----------------------------------------------------------------------------');
fprintf(fid2,'\n%s %s','Date de traitement du fichier         :  ',date);
fprintf(fid2,'\n%s %4d\n\n','Numero de calcul                      :  ',numcal);
fprintf(fid3,'\n\t%s\n',['Post-traitement veh hyb. parallele 1 emb. pour modele VEHLIB- VERSION ',VD.INIT.version]);
fprintf(fid3,'\n\t%s\n','-----------------------------------------------------------------------------');
fprintf(fid3,'\n%s %s','Date de traitement du fichier         :  ',date);
fprintf(fid3,'\n%s %4d\n\n','Numero de calcul                      :  ',numcal);

fprintf(fid,'%s%s\n','Nom du fichier modele                  :   ',vehlib.nom);
if isfield(vehlib,'simulink')
fprintf(fid,'%s%s\n','Nom du modele simulink                 :   ',vehlib.simulink);
end
fprintf(fid,'%s%s\n','Nom du fichier cinematique             :   ',vehlib.CYCL);
fprintf(fid,'%s%s\n','Nom du fichier vehicule                :   ',vehlib.VEHI);
fprintf(fid,'%s%s\n','Nom du fichier reducteur               :   ',vehlib.RED);
fprintf(fid,'%s%s\n','Nom du fichier boite de vitesse        :   ',vehlib.BV);
fprintf(fid,'%s%s\n','Nom du fichier calculateur             :   ',vehlib.ECU);
fprintf(fid,'%s%s\n','Nom du fichier embrayage               :   ',vehlib.EMBR1);
fprintf(fid,'%s%s\n','Nom du fichier moteur electrique       :   ',vehlib.ACM1);
fprintf(fid,'%s%s\n','Nom du fichier pack batterie           :   ',vehlib.BATT);
fprintf(fid,'%s%s\n','Nom du fichier monobloc batterie       :   ',VD.BATT.Nom_bloc);
fprintf(fid,'%s%s\n','Nom du fichier moteur thermique        :   ',vehlib.MOTH);
fprintf(fid,'%s%s\n\n','Nom du fichier couplage mecanique    :   ',vehlib.ADCPL);
fprintf(fid,'%s%s\n\n','Nom du fichier accessoires             :   ',vehlib.ACC);

fprintf(1,'\n\t%s\n',['Post-traitement veh hyb. parallele 1 emb. pour modele VEHLIB- VERSION ',VD.INIT.version]);
fprintf(1,'\n\t%s\n','-----------------------------------------------------------------------------');

%%% VALEURS SYNTHETIQUES %%%%%
% Ecriture du bilan de masse
dep_masse(VD, masse, fid);

[pas]=dep_cin(VD,tsim, distance, vit, vit_dem, acc, elhyb, vimin, fid);

if(VD.VEHI.ntypveh==1)
   [pveh]=dep_veh1(VD,tsim, pas, vit, acc, froul, faero, fpente, cfrein_meca, cinertie, vimin, fid);
elseif(VD.VEHI.ntypveh==2)
   [pveh]=dep_veh2(VD,tsim, pas, vit, acc, force1, force2, force3, fpente, cinertie, vimin, fid);
end

chaine='          ***   CARACTERISTIQUES REDUCTEUR FINAL   ***';
dep_transfo_meca(VD,tsim, pas, vit, wsec_red, csec_red, wprim_red, cprim_red, chaine, vimin, fid);

chaine='          ***   CARACTERISTIQUES BOITE DE VITESSE  ***';
dep_transfo_meca(VD,tsim, pas, vit, wsec_bv, csec_bv, wprim_bv, cprim_bv, chaine, vimin, fid);

dep_emb(VD,tsim, pas, vit, wsec_emb1, csec_emb1, wprim_emb1, cprim_emb1_sans_inertie, pinertie_emb1, rappvit_bv, glisst_emb1, vimin, fid);

chaine='          ***   CARACTERISTIQUES ASSOC. CONVERTISSEUR MACHINE ELECTRIQUE  ***';
if(VD.ACM1.ntypmg==10)
  [pmec_mg1, pcacm1]=dep_mct(VD,tsim, vit, pas, wacm1, cacm1_sans_inertie, iacm1, uacm1, chaine, vimin, fid);
elseif(VD.ACM1.ntypmg==1)

end
if isfield(VD,'TWC')
dep_mth(VD,tsim, pas, vit, wmt, cmt, distance(length(distance)), conso, cumcarb, elhyb, co, hc, nox, co2, fid);
else
dep_mth(VD,tsim, pas, vit, wmt, cmt, distance(length(distance)), conso, cumcarb, elhyb, zeros(size(tsim)), zeros(size(tsim)), zeros(size(tsim)), co2, fid);   
end
[pbat]=dep_bat(VD,tsim, pas, vit, ibat, ubat, dod, pertes_bat, vimin, fid, cahbat);

dep_acc(VD,tsim,pas,vit,iacc,uacc,fid);

%%% ENERGIES  %%%%

% script d initialisation des bilans sources/consommateurs
if max(elhyb~=0)
   ecr_energie('initwh',fid2,'',[1 2 3 4 5 6]);
   ecr_energie('initwhkm',fid3,'',[1 2 3 4 5 6]);
else
   ecr_energie('initwh',fid2,'',[1 2 3]);
   ecr_energie('initwhkm',fid3,'',[1 2 3]);
end
   
% initialisation de la structure pour renseigner les flux d energie
flux=struct([]);
assignin('base','flux',flux);

if(VD.VEHI.ntypveh==1)
   energie_veh1(VD,tsim, pas, vit, vit_dem, distance(length(distance)), fpente, faero, froul, cfrein_meca, cinertie, wroue, pveh, elhyb, vimin, fid2, fid3);
elseif(VD.VEHI.ntypveh==2)
   energie_veh2(VD,tsim, pas, vit, vit_dem, distance(length(distance)), fpente, force1, force2, force3, cfrein_meca, cinertie, wroue, pveh, elhyb, vimin, fid2, fid3);
end

chaine='Pertes reducteur';
energie_transfo_meca(VD,tsim, pas, vit_dem, distance(length(distance)), csec_red, wsec_red, cprim_red, wprim_red, pveh, elhyb, chaine, vimin, fid2, fid3)

chaine='Pertes boite de vitesse';
energie_transfo_meca(VD,tsim, pas, vit_dem, distance(length(distance)), csec_bv, wsec_bv, cprim_bv, wprim_bv, pveh, elhyb, chaine, vimin, fid2, fid3)

energie_emb(VD,tsim, pas, vit_dem, distance(length(distance)), wsec_emb1.*csec_emb1, wprim_emb1.*cprim_emb1_sans_inertie, pinertie_emb1, pveh, elhyb, vimin, 1, fid2, fid3);

chaine='Pertes syst. couplage';
energie_couplage_meca(VD,tsim, pas, vit_dem, distance(length(distance)), csec_cpl, wsec_cpl, cprim1_cpl, wprim1_cpl, cprim2_cpl, wprim2_cpl, pveh, elhyb, chaine, vimin, fid2, fid3)

if(VD.ACM1.ntypmg==10)
   energie_MCT(VD,tsim, pas, vit_dem, distance(length(distance)), pmec_mg1, pcacm1, pveh, elhyb, vimin, 1, fid2, fid3);
elseif(VD.ACM1.ntypmg==1)
   energie_MCC(VD,tsim, pas, vit,vit_dem, distance(length(distance)), acc, pmec_roue, pmec_mg1, pind, pcind, pex, ...
   PtMeca, PtJind, PtJexcit, PtFer, PtSupp, ubal, img1, ...
   fpente, faero, froul, cfrein_meca, cinertie, wroue, ubat, iacc, pbat,pveh, fid2, fid3);
end

energie_access(VD,tsim, pas, vit_dem, distance(length(distance)), pveh, iacc, uacc, elhyb, vimin, fid2, fid3);

energie_bat(VD,tsim, pas, vit_dem, distance(length(distance)), pbat, pveh, elhyb, vimin, fid2, fid3);

energie_mth(VD,tsim, pas, vit_dem, distance(length(distance)), wmt, cmt, pveh, elhyb, vimin, fid2, fid3);

% script de fin des bilans sources/consommateurs
if max(elhyb)~=0
   ecr_energie('end',fid2,'',[1 2 3 4 5 6]);
   ecr_energie('end',fid3,'',[1 2 3 4 5 6]);
else
   ecr_energie('end',fid2,'',[1 2 3]);
   ecr_energie('end',fid3,'',[1 2 3]);
end

% fermeture
fclose(fid);
fclose(fid2);
fclose(fid3);

if isunix
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF1')]);
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF2')]);
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF3')]);
end

