% ------------------------------------------------------
%
% Depouillement des pertes du convertisseur DCDC
% ------------------------------------------------------
function [] = energie_dcdc(VD,temps, pas, vitdem, dist, pveh, pamont, paval, elehyb, vimin, fid, fid3)


% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>= 0';
QDCDC_M=sum((pamont(eval(condition))-paval(eval(condition))).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb==0 & vitdem>vimin & pveh< 0';
QDCDC_R=sum((pamont(eval(condition))-paval(eval(condition))).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb==0 & vitdem<vimin';
QDCDC_A=sum((pamont(eval(condition))-paval(eval(condition))).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>= 0';
QDCDC_MH=sum((pamont(eval(condition))-paval(eval(condition))).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb~=0 & vitdem>vimin & pveh< 0';
QDCDC_RH=sum((pamont(eval(condition))-paval(eval(condition))).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb~=0 & vitdem<vimin';
QDCDC_AH=sum((pamont(eval(condition))-paval(eval(condition))).*pas(eval(condition))/3600);

% en Wh.
if(max(elehyb)==0)
 ecr_energie('sum',fid, 'Pertes conv. DC DC      : ', [QDCDC_M QDCDC_R QDCDC_A]);
else
 ecr_energie('sum',fid, 'Pertes conv. DC DC      : ', [QDCDC_M QDCDC_MH QDCDC_R QDCDC_RH QDCDC_A QDCDC_AH]);
end   

% en Wh/km.
if(max(elehyb)==0)
 ecr_energie('sum',fid3, 'Pertes conv. DC DC      : ', [QDCDC_M QDCDC_R QDCDC_A]/(dist/1000));
else
 ecr_energie('sum',fid3, 'Pertes conv. DC DC      : ', [QDCDC_M QDCDC_MH QDCDC_R QDCDC_RH QDCDC_A QDCDC_AH]/(dist/1000));
end

% Ecriture de la structure de flux
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='_MA';
flux(j).type='QDCDC_MA';
flux(j).valeur=QDCDC_M+QDCDC_MH+QDCDC_A+QDCDC_AH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='QDCDC_R';
flux(j).type='';
flux(j).valeur=QDCDC_R+QDCDC_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='green';

assignin('base','flux',flux);
