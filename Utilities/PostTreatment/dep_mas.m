% Function: dep_mas
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%function [pmec, pstat, pcstat,Istat, Vstat, cosfi,Gliss] = dep_mas(VD,temps, vit, pas,flag_alim, wmg, cmg, id, vd, iq, vq, ...
%                      ubat,ibat, iond, PtJstat,PtFer,PtMeca, wrmg, wsmg, fid)
%
% Depouillement des simulation Machine asynchrone et convertisseur
% ------------------------------------------------------
function [pmec, pstat, pcstat,Istat, Vstat, cosfi,Gliss] = dep_mas(VD,temps, vit, pas,flag_alim, wmg, cmg, id, vd, iq, vq, ...
                      ubat,ibat, iond, PtJstat,PtFer,PtMeca, wrmg, wsmg, fid)
                   
pmec=wmg.*cmg;
pmec=pmec.*flag_alim;              
pstat=vd.*id+vq.*iq;
pcstat=ubat.*iond;


Vstat=sqrt((vd.^2+vq.^2)/3);
Istat=sqrt((id.^2+iq.^2)/3);

cosfi=pstat./(3*Vstat.*Istat);

for i=1:length(temps)
if wrmg(i)>1

   Gliss(i)=wrmg(i)/wsmg(i)*100;
else
   Gliss(i)=0;
end
end

Gliss=Gliss';

% Valeurs moyennes
wmg_moy_T=0;
wmg_moy_R=0;
cmg_moy_T=0;
cmg_moy_R=0;
pmec_moy_T=0;
pmec_moy_R=0;
pstat_moy_T=0;
pstat_moy_R=0;
pcstat_moy_T=0;
pcstat_moy_R=0;
Istat_moy_T=0;
Istat_moy_R=0;
Vstat_moy_T=0;
Vstat_moy_R=0;

t_T=0;
t_R=0;

tcstat_T=0;
tcstat_R=0;

% Mini - Maxi
cmg_min=min(cmg);

pmec_min=min(pmec);
pstat_min=min(pstat);
pcstat_min=min(pcstat);
Istat_min=min(Istat);

cmg_max=max(cmg);
pmec_max=max(pmec);
pstat_max=max(pstat);
pcstat_max=max(pcstat);
Istat_max=max(Istat);

for i=1:length(temps)
   rendmas_stat(i)=0;
   rendmas(i)=0;
   rendcmg(i)=0;
   
 if(vit(i)>=0.01)
   if (pmec(i)> 0 & pstat(i)> 0)
      wmg_moy_T=wmg_moy_T+wmg(i)*pas(i);
      cmg_moy_T=cmg_moy_T+cmg(i)*pas(i);
      pmec_moy_T=pmec_moy_T+pmec(i).*pas(i);
      pstat_moy_T=pstat_moy_T+pstat(i).*pas(i);
      rendmsy_stat(i)=pmec(i)./pstat(i);
      rendmsy(i)=pmec(i)./(pstat(i));
      Istat_moy_T=Istat_moy_T+Istat(i).*pas(i);
      Vstat_moy_T=Vstat_moy_T+Vstat(i).*pas(i);
      t_T=t_T+pas(i);
   end
   if (pmec(i)< 0 & pstat(i)< 0)
      wmg_moy_R=wmg_moy_R+wmg(i)*pas(i);
      cmg_moy_R=cmg_moy_R+cmg(i)*pas(i);
      pmec_moy_R=pmec_moy_R+pmec(i).*pas(i);
      pstat_moy_R=pstat_moy_R+pstat(i).*pas(i);
      rendmsy_stat(i)=pmec(i)./pstat(i);
      rendmsy(i)=(pstat(i))./pmec(i);  
      Istat_moy_R=Istat_moy_R+Istat(i).*pas(i);
      Vstat_moy_R=Vstat_moy_R+Vstat(i).*pas(i);
      t_R=t_R+pas(i);
   end
   if (pmec(i)> 0 & pstat(i)> 0)

      tcstat_T=tcstat_T+pas(i);
      pcstat_moy_T=pcstat_moy_T+pcstat(i).*pas(i);      
      rendcmg(i)=(pstat(i))./pcstat(i);         
   elseif (pmec(i)< 0 & pstat(i)< 0)
      tcstat_R=tcstat_R+pas(i);
      pcstat_moy_R=pcstat_moy_R+pcstat(i).*pas(i);      
      rendcmg(i)=pcstat(i)./(pstat(i));    
   end
else

end
end

if(t_T~=0)
   wmg_moy_T=wmg_moy_T/t_T;
   cmg_moy_T=cmg_moy_T/t_T;
   pmec_moy_T=pmec_moy_T/t_T;
   pstat_moy_T=pstat_moy_T/t_T;
   Istat_moy_T=Istat_moy_T/t_T;
   Vstat_moy_T=Vstat_moy_T/t_T;
else
   wmg_moy_T=0;
   cmg_moy_T=0;
   pmec_moy_T=0;
   pstat_moy_T=0;
   Istat_moy_T=0;
   Vstat_moy_T=0;
end

if(tcstat_T~=0)
   pcstat_moy_T=pcstat_moy_T/tcstat_T;
else
   pcstat_moy_T=0;
end

if(t_R~=0)
   wmg_moy_R=wmg_moy_R/t_R;
   cmg_moy_R=cmg_moy_R/t_R;
   pmec_moy_R=pmec_moy_R/t_R;
   pstat_moy_R=pstat_moy_R/t_R;
   Istat_moy_R=Istat_moy_R/t_R;
   Vstat_moy_R=Vstat_moy_R/t_R;
else
   wmg_moy_R=0;;
   cmg_moy_R=0;
   pmec_moy_R=0;
   pstat_moy_R=0;
   Istat_moy_R=0;
   Vstat_moy_R=0;
end

if(tcstat_R~=0)
   pcstat_moy_R=pcstat_moy_R/tcstat_R;
else
   pcstat_moy_R=0;
end

if(pstat_moy_T~=0)
   rdstat_moy_T=pmec_moy_T/pstat_moy_T;
else
   rdstat_moy_T=0;
end

if((pstat_moy_T)~=0)
   rdmas_moy_T=pmec_moy_T/(pstat_moy_T);
else
   rdmas_moy_T=0;
end

if(pcstat_moy_T~=0)
   rdcstat_moy_T=pstat_moy_T/pcstat_moy_T;
else
   rdcstat_moy_T=0;
end

if(pmec_moy_R~=0)
   rdstat_moy_R=pstat_moy_R/pmec_moy_R;
   rdmas_moy_R=(pstat_moy_R)/pmec_moy_R;
else
   rdstat_moy_R=0;
   rdmsy_moy_R=0;
end

if(pstat_moy_R~=0)
   rdcstat_moy_R=pcstat_moy_R/pstat_moy_R;
else
   rdcstat_moy_R=0;
end

fprintf(fid,'\n\t\t\t%s\n\n','*** CARACTERISTIQUES MOTEUR mas ***');

fprintf(fid,'\t\t\t\t%s\t\t%s\n\n','Phase motrice','Phase recuperatrice');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Temps (pm et pe >< 0): ',t_T,'s',t_R,'s');
fprintf(fid,'%s\t\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Regime moyen : ', ...
   wmg_moy_T*30/pi,'tr/mn',wmg_moy_R*30/pi,'tr/mn');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Couple maximum : ', ...
   cmg_max,'Nm',cmg_min,'Nm');
fprintf(fid,'%s\t\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Couple moyen : ', ...
   cmg_moy_T,'Nm',cmg_moy_R,'Nm');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance mecanique maximum : ', ...
   pmec_max./1000,'kW',pmec_min./1000,'kW');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance mecanique moyenne :', ...
   pmec_moy_T./1000,'kW',pmec_moy_R./1000,'kW');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance stator maximum :', ...
   pstat_max./1000,'kW',pstat_min./1000,'kW');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance stator moyenne :', ...
   pstat_moy_T./1000,'kW',pstat_moy_R./1000,'kW');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Courant stator maximum :', ...
   Istat_max,'A',Istat_min,'A');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Courant stator moyen :', ...
   Istat_moy_T,'A',Istat_moy_R,'A');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Tension stator moyenne :', ...
   Vstat_moy_T,'V',Vstat_moy_R,'V');

fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Rendement moyen  :', ...
   100.*rdstat_moy_T,'%',100.*rdstat_moy_R,'%');

fprintf(fid,'\n\t\t\t%s\n\n','*** CARACTERISTIQUES CONTROLE mas ***');

fprintf(fid,'\t\t\t\t%s\t\t%s\n\n','Phase motrice','Phase recuperatrice');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance maximum :', ...
   pcstat_max./1000,'kW',pcstat_min./1000,'kW');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance moyenne :', ...
   pcstat_moy_T./1000,'kW',pcstat_moy_R./1000,'kW');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Rendement moyen :', ...
   100.*rdcstat_moy_T,'%',100.*rdcstat_moy_R,'%');
