function [res] = resultats(VD,param,ResXml)

res=[];

[tsim,vit]=XmlValue(ResXml,{'tsim','vit'});

if param.verbose>=1
    fprintf(1,'%s %.1f %s\n','La distance parcourue est de : ',VD.CYCL.distance(end),'m');
end

if isfield(VD,'MOTH')
    [dcarb]=XmlValue(ResXml,'dcarb');
    [a,b]=size(dcarb);
    if a==1
        res.cumcarb=cumsum(VD.CYCL.pas_temps.*dcarb);
        res.conso=res.cumcarb/VD.MOTH.dens_carb;
        res.co2_eq=44*res.cumcarb(end)./(12+VD.MOTH.CO2carb);
        res.CO2_gkm=res.co2_eq/(VD.CYCL.distance(end)/1000);
        res.CO2=44*dcarb./(12+VD.MOTH.CO2carb);
        res.conso100=100.*res.cumcarb(end)./VD.MOTH.dens_carb./(VD.CYCL.distance(end)/1000);
    else
        for ii=1:a
            res.cumcarb(ii,:)=cumsum(VD.CYCL.pas_temps.*dcarb(ii,:));
            res.conso(ii,:)=res.cumcarb(ii,:)/VD.MOTH.dens_carb;
            res.co2_eq(ii)=44*res.cumcarb(ii,end)./(12+VD.MOTH.CO2carb);
            res.CO2_gkm(ii)=res.co2_eq(ii)/(VD.CYCL.distance(end)/1000);
            res.CO2(ii,:)=44*dcarb(ii,:)./(12+VD.MOTH.CO2carb);
            res.conso100(ii)=100.*res.cumcarb(ii,end)./VD.MOTH.dens_carb./(VD.CYCL.distance(end)/1000);
        end
    end
    %co2_inst=44*dcarb./(12+VD.MOTH.CO2carb);
    
    if isfield(VD,'TWC')
        [co,hc,nox]=XmlValue(ResXml,{'co','hc','nox'});
        if sum(isnan(co))==0 && sum(isnan(hc))==0 && sum(isnan(nox))==0
            % Resultats d'emission
            res.co_gkm=trapz(tsim,co)/(VD.CYCL.distance(end)/1000);
            res.hc_gkm=trapz(tsim,hc)/(VD.CYCL.distance(end)/1000);
            res.nox_gkm=trapz(tsim,nox)/(VD.CYCL.distance(end)/1000);
            if param.verbose>=1              
                fprintf(1,'%s %f %s\n','Emission de CO :',res.co_gkm,'g/km');
                fprintf(1,'%s %f %s\n','Emission de HC :',res.hc_gkm,'g/km');
                fprintf(1,'%s %f %s\n','Emission de NOx :',res.nox_gkm,'g/km');
                PE4=1/3*(res.co_gkm/VD.TWC.CO_EURO4+res.hc_gkm/VD.TWC.THC_EURO4+res.nox_gkm/VD.TWC.NOx_EURO4);
                PE5=1/3*(res.co_gkm/VD.TWC.CO_EURO5+res.hc_gkm/VD.TWC.THC_EURO5+res.nox_gkm/VD.TWC.NOx_EURO5);
                fprintf(1,'%s %f\n','Soit une ponderation par rapport a la norme EURO IV : :',PE4);
                fprintf(1,'%s %f\n','Soit une ponderation par rapport a la norme EURO V : :',PE5);
            end
        end
    end
    
    if param.verbose>=1
        fprintf(1,'%s %.3f %s\n','La consommation du vehicule est de : ',res.conso100,'l/100 km');
        fprintf(1,'%s %.3f %s\n','Les emissions de CO2 du vehicule sont de : ',res.CO2_gkm,'g/km');
    end
    
    %     if isfield(param,'calc_2chemin') && param.calc_2chemin==1;
    %         res.cumcarb_2=cumsum(VD.CYCL.temps,r.dcarb(2,:));
    %         res.co2_eq_2=44*cumcarb_2(length(cumcarb_2))./(12+VD.MOTH.CO2carb);
    %         res.CO2_gkm_2=co2_eq(2)/(dist/1000);
    %         res.conso100_2=100.*sum(r.dcarb(2,:).*VD.CYCL.pas_temps)./VD.MOTH.dens_carb./trapz(VD.CYCL.temps,VD.CYCL.vitesse./1000);
    %         %co2_inst=44*dcarb(1,:)./(12+VD.MOTH.CO2carb);
    %     end
end

if isfield(VD,'BATT')
    [ibat,ubat,soc]=XmlValue(ResXml,{'ibat','ubat','soc'});
    res.ibat_eff=sqrt( sum(ibat.*ibat.*VD.CYCL.pas_temps)./(tsim(end)-tsim(1)) );
    res.ibat_moy=sum(VD.CYCL.pas_temps.*ibat)./(tsim(end)-tsim(1));
    if param.verbose>=1
        fprintf(1,'%s %.3f %s\n','La variation de SOC sur le cycle est de : ',soc(end)-soc(1),'%');
        fprintf(1,'%s %.1f %s\n','Le courant efficace de la batterie : ',res.ibat_eff,'A');
        fprintf(1,'%s %.1f %s\n','Le courant moyen de la batterie : ',res.ibat_moy,'A'); %
    end
    
end

end

