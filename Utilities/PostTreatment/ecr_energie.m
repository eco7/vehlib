% Function: ecr_energie
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function []= ecr_energie(action, fid, chaine, valeurs)
%
% action sert a faire des cumuls des flux par colonne dans RESUF2 et 3
% action: initwh, initwhkm, separateur, sum, substract,end ou nothing
% nothing: pour pas de cumul par colonne, mais ecriture de la ligne.
% initwh   : initialisation des valeurs en Wh et stockage du fid de resuf2 ds fidwh
% initwhkm: initialisation des valeurs en Wh/km et stockage du fid de resuf2 ds fidwhkm
% fid: file id
% chaine: string
% valeurs: array of values.
%
% Attention: utilisation de variables persistent dans la fonction
% ------------------------------------------------------
function []= ecr_energie(action, fid, chaine, valeurs)

persistent FEN_M FEN_MH FEN_R FEN_RH FEN_A FEN_AH fidwh
persistent FEN_WHKM_M FEN_WHKM_MH FEN_WHKM_R FEN_WHKM_RH FEN_WHKM_A FEN_WHKM_AH fidwhkm

if ~strcmp(action,'end') & ~strcmp(action,'initwh') & ~strcmp(action,'initwhkm') & ~strcmp(action,'separateur')
    if(length(valeurs)==2)
        % implicitement phases motrices et recuperatrices suivant un mode de fct
        fprintf(fid,'%s   %7.1f               %7.1f \n',...
            chaine,valeurs(1),valeurs(2));
    end
    if(length(valeurs)==3)
        % implicitement phases motrices, recuperatrices et arret suivant un mode de fct
        fprintf(fid,'%s   %7.1f               %7.1f           %7.1f \n',...
            chaine,valeurs(1),valeurs(2),valeurs(3));
    end
    if(length(valeurs)==4)
        % implicitement phases motrices et recuperatrices suivant mode electrique et hybride
        fprintf(fid,'%s%7.1f %7.1f %7.1f     %7.1f %7.1f %7.1f\n',...
            chaine,valeurs(1),valeurs(2),valeurs(1)+valeurs(2),valeurs(3),valeurs(4),valeurs(3)+valeurs(4));
    end
    if(length(valeurs)==6)
        % implicitement phases motrices, recuperatrices et arret suivant mode electrique et hybride
        fprintf(fid,'%s%7.1f %7.1f %7.1f     %7.1f %7.1f %7.1f        %7.1f %7.1f %7.1f\n',...
            chaine,valeurs(1),valeurs(2),valeurs(1)+valeurs(2),valeurs(3),valeurs(4),valeurs(3)+valeurs(4),valeurs(5),valeurs(6),valeurs(5)+valeurs(6));
    end
end

% debut des sommations des flux pour bilan sources/consommateurs
if strcmp(action,'sum')| strcmp(action,'substract')
    if(length(valeurs)==2)
        f_m=valeurs(1);
        f_mh=0;
        f_r=valeurs(2);
        f_rh=0;
        f_a=0;
        f_ah=0;
    elseif(length(valeurs)==3)
        f_m=valeurs(1);
        f_mh=0;
        f_r=valeurs(2);
        f_rh=0;
        f_a=valeurs(3);
        f_ah=0;
    elseif(length(valeurs)==4)
        f_m=valeurs(1);
        f_mh=valeurs(2);
        f_r=valeurs(3);
        f_rh=valeurs(4);
        f_a=0;
        f_ah=0;
    elseif(length(valeurs)==6)
        f_m=valeurs(1);
        f_mh=valeurs(2);
        f_r=valeurs(3);
        f_rh=valeurs(4);
        f_a=valeurs(5);
        f_ah=valeurs(6);
    end
end

if strcmp(action,'initwh')
    % Initialisation pour les wh
    fidwh=fid;
    FEN_M=0;
    FEN_MH=0;
    FEN_R=0;
    FEN_RH=0;
    FEN_A=0;
    FEN_AH=0;
    fprintf(fid,'\n%s\n\n',...
        '                                    *** FLUX D''ENERGIES DANS LA TRANSMISSION en Wh ***');
    fprintf(fid,'%s\n\n',...
        '                                Phase motrice          Phase recuperatrice             Phase d''arret');
    if length(valeurs)==6
        fprintf(fid,'%s\n\n',...
            'Mode de fonctionnement  :  Elect. Hybride   Total     Elect. Hybride    Total        Elect. Hybride    Total');
    end

elseif strcmp(action,'initwhkm')
    % Initialisation pour les Wh/km
    fidwhkm=fid;
    FEN_WHKM_M=0;
    FEN_WHKM_MH=0;
    FEN_WHKM_R=0;
    FEN_WHKM_RH=0;
    FEN_WHKM_A=0;
    FEN_WHKM_AH=0;
    fprintf(fid,'\n%s\n\n',...
        '                                    *** FLUX D''ENERGIES DANS LA TRANSMISSION en Wh/km ***');
    fprintf(fid,'%s\n\n',...
        '                                Phase motrice          Phase recuperatrice             Phase d''arret');
    if length(valeurs)==6
        fprintf(fid,'%s\n\n',...
            'Mode de fonctionnement  :  Elect. Hybride   Total     Elect. Hybride    Total        Elect. Hybride    Total');
    end
elseif strcmp(action,'separateur')
    % Ecriture d un separateur
    if length(valeurs)==6
        fprintf(fid,'%s\n',...
            '                          -----------------------       --------------------- ');
    elseif length(valeurs)==9
        fprintf(fid,'%s\n',...
            '                          -----------------------       ---------------------          ---------------------');
    elseif length(valeurs)==2
        fprintf(fid,'%s%s%s\n',...
            '                           -----------','            -----------');        
    elseif length(valeurs)==3
        fprintf(fid,'%s%s%s\n',...
            '                           -----------','            -----------','          -----------');        
    end
elseif strcmp(action,'sum')
    if fid==fidwh
        FEN_M=FEN_M+f_m;
        FEN_MH=FEN_MH+f_mh;
        FEN_R=FEN_R+f_r;
        FEN_RH=FEN_RH+f_rh;
        FEN_A=FEN_A+f_a;
        FEN_AH=FEN_AH+f_ah;
    else
        FEN_WHKM_M=FEN_WHKM_M+f_m;
        FEN_WHKM_MH=FEN_WHKM_MH+f_mh;
        FEN_WHKM_R=FEN_WHKM_R+f_r;
        FEN_WHKM_RH=FEN_WHKM_RH+f_rh;
        FEN_WHKM_A=FEN_WHKM_A+f_a;
        FEN_WHKM_AH=FEN_WHKM_AH+f_ah;
    end
elseif strcmp(action,'substract')
    if fid==fidwh
        FEN_M=FEN_M-f_m;
        FEN_MH=FEN_MH-f_mh;
        FEN_R=FEN_R-f_r;
        FEN_RH=FEN_RH-f_rh;
        FEN_A=FEN_A-f_a;
        FEN_AH=FEN_AH-f_ah;
    else
        FEN_WHKM_M=FEN_WHKM_M-f_m;
        FEN_WHKM_MH=FEN_WHKM_MH-f_mh;
        FEN_WHKM_R=FEN_WHKM_R-f_r;
        FEN_WHKM_RH=FEN_WHKM_RH-f_rh;
        FEN_WHKM_A=FEN_WHKM_A-f_a;
        FEN_WHKM_AH=FEN_WHKM_AH-f_ah;
    end
elseif strcmp(action,'end')
    fprintf(fid,'\n\n');
    if length(valeurs)==6
        fprintf(fid,'%s\n',...
            '                          -----------------------       ---------------------          ---------------------');
        if fid==fidwh
            ecr_energie('nothing',fid, 'Bilan Consomm/Sources   : ', [FEN_M FEN_MH FEN_R FEN_RH FEN_A FEN_AH]);
        else
            ecr_energie('nothing',fid, 'Bilan Consomm/Sources   : ', [FEN_WHKM_M FEN_WHKM_MH FEN_WHKM_R FEN_WHKM_RH FEN_WHKM_A FEN_WHKM_AH]);
        end
    else
        fprintf(fid,'%s%s%s\n',...
            '                           -----------','            -----------','          -----------');
        if fid==fidwh
            ecr_energie('nothing',fid, 'Bilan Consomm/Sources   : ', [FEN_M FEN_R FEN_A]);
        else
            ecr_energie('nothing',fid, 'Bilan Consomm/Sources   : ', [FEN_WHKM_M FEN_WHKM_R FEN_WHKM_A]);
        end
    end
end
