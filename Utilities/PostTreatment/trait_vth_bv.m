% File: trait_vth_bv
% ------------------------------------------------------
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%
% Script de post traitement d'un modele de vehicule conventionnel dans VEHLIB
%
% ------------------------------------------------------

vimin=0.01;

%lecture et increment du numero de calcul
fnum=strcat(VD.INIT.InitialFolder,VD.INIT.sep,'numcal.mat');
load(fnum);

% ecriture du fichier de resultats synthetiques
fid=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF1'),'w');
fid2=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF2'),'w');
fid3=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF3'),'w');

fprintf(fid,'\n\t%s\n',['Post-traitement Vehicule thermique pour modele VEHLIB - VERSION ',VD.INIT.version]);
fprintf(fid,'\n\t%s\n', '-------------------------------------------------------------------');
fprintf(fid,'\n%s %s','Date de traitement du fichier          :  ',date);
fprintf(fid,'\n%s %4d\n\n','Numero de calcul                       :  ',numcal);
fprintf(fid2,'\n\t%s\n',['Post-traitement Vehicule thermique pour modele VEHLIB - VERSION ',VD.INIT.version]);
fprintf(fid2,'\n\t%s\n','--------------------------------------------------------------------');
fprintf(fid2,'\n%s %s','Date de traitement du fichier         :  ',date);
fprintf(fid2,'\n%s %4d\n\n','Numero de calcul                      :  ',numcal);
fprintf(fid3,'\n\t%s\n',['Post-traitement Vehicule thermique pour modele VEHLIB - VERSION ',VD.INIT.version]);
fprintf(fid3,'\n\t%s\n','--------------------------------------------------------------------');
fprintf(fid3,'\n%s %s','Date de traitement du fichier         :  ',date);
fprintf(fid3,'\n%s %4d\n\n','Numero de calcul                      :  ',numcal);

fprintf(fid,'%s%s\n','Nom du fichier modele                  :   ',vehlib.nom);
if isfield(vehlib,'simulink')
    fprintf(fid,'%s%s\n','Nom du modele simulink                 :   ',vehlib.simulink);
end
fprintf(fid,'%s%s\n','Nom du fichier cinematique             :   ',vehlib.CYCL);
fprintf(fid,'%s%s\n','Nom du fichier vehicule                :   ',vehlib.VEHI);
fprintf(fid,'%s%s\n','Nom du fichier boite de vitesse        :   ',vehlib.BV);
fprintf(fid,'%s%s\n','Nom du fichier reducteur               :   ',vehlib.RED);
fprintf(fid,'%s%s\n','Nom du fichier embrayage               :   ',vehlib.EMBR1);
fprintf(fid,'%s%s\n','Nom du fichier calculateur             :   ',vehlib.ECU);
fprintf(fid,'%s%s\n','Nom du fichier moteur thermique        :   ',vehlib.MOTH);
fprintf(fid,'%s%s\n','Nom du fichier courroie 1              :   ',vehlib.BELT1);
fprintf(fid,'%s%s\n\n','Nom du fichier accessoires             :   ',vehlib.ACC);

fprintf(1,'\n\t%s\n',['Post-traitement Vehicule thermique pour modele VEHLIB - VERSION ',VD.INIT.version]);
fprintf(1,'\n\t%s\n','--------------------------------------------------------------------');

%%% VALEURS SYNTHETIQUES %%%%%

% Ecriture du bilan de masse
dep_masse(VD, masse, fid);

[pas]=dep_cin(VD, tsim, distance, vit, vit_dem, acc, ones(length(vit),1), vimin, fid);

if(VD.VEHI.ntypveh==1 || VD.VEHI.ntypveh == 3 || VD.VEHI.ntypveh == 4)
   [pveh]=dep_veh1(VD, tsim, pas, vit, acc, froul, faero, fpente, cfrein_meca, cinertie, vimin, fid);
elseif(VD.VEHI.ntypveh==2)
   [pveh]=dep_veh2(VD, tsim, pas, vit, acc, force1, force2, force3, fpente, cinertie, vimin, fid);
end

chaine='          ***   CARACTERISTIQUES REDUCTEUR FINAL   *****';
dep_transfo_meca(VD, tsim, pas, vit, wsec_red, csec_red, wprim_red, cprim_red, chaine, vimin, fid);

chaine='          ***   CARACTERISTIQUES BOITE DE VITESSE  *****';
dep_transfo_meca(VD, tsim, pas, vit, wsec_bv, csec_bv, wprim_bv, cprim_bv, chaine, vimin, fid);

dep_emb(VD, tsim, pas, vit, wsec_emb1, csec_emb1, wprim_emb1, cprim_emb1_sans_inertie, pinertie_emb1, rappvit_bv, glisst_emb1, vimin, fid);

dep_mth(VD, tsim, pas, vit, wmt, cmt, distance(length(distance)), conso, cumcarb, ones(size(vit)), co, hc, nox, co2, fid);

analyse_carburant(VD, tsim, pas, vit, wprim_emb1.*cprim_emb1_sans_inertie, dcarb, 'Analyse sur l''arbre primaire de l''embrayage', 0, vimin, fid)
analyse_carburant(VD, tsim, pas, vit, wmt.*cmt, dcarb, 'Analyse sur l''arbre du moteur thermique', 0, vimin, fid)

if VD.ACC.ntypacc==1
    %dep_accm2(VD, tsim, pas, vit, wacm2, cacm2, zeros(size(vit)), zeros(size(vit)), vimin, fid)
elseif VD.ACC.ntypacc==2 || VD.ACC.ntypacc==4
   dep_accm2(VD, tsim, pas, vit, waccm1, caccm1, waccm2, caccm2,vimin, fid)
elseif VD.ACC.ntypacc==3
   dep_accm3(VD, tsim, pas, vit, waccm1, caccm1, paccm_alt, paccm_servo, paccm_air, paccm_ref, vimin, fid)
end

%%% ENERGIES  %%%%

% script d initialisation des bilans sources/consommateurs
ecr_energie('initwh',fid2,'',[1 2 3 4 5 6]);
ecr_energie('initwhkm',fid3,'',[1 2 3 4 5 6]);

  
% initialisation de la structure pour renseigner les flux d energie
flux=struct([]);
assignin('base','flux',flux);

if(VD.VEHI.ntypveh==1 || VD.VEHI.ntypveh == 3 || VD.VEHI.ntypveh == 4)
   energie_veh1(VD, tsim, pas, vit, vit_dem, distance(length(distance)), fpente, faero, froul, cfrein_meca, cinertie, wroue, pveh, ones(size(vit)), vimin, fid2, fid3);
elseif(VD.VEHI.ntypveh==2)
   energie_veh2(VD, tsim, pas, vit, vit_dem, distance(length(distance)), fpente, force1, force2, force3, cfrein_meca, cinertie, wroue, pveh, ones(size(vit)), vimin, fid2, fid3);
end

chaine='Pertes reducteur';
energie_transfo_meca(VD, tsim, pas, vit_dem, distance(length(distance)), csec_red, wsec_red, cprim_red, wprim_red, pveh, ones(size(vit)), chaine, vimin, fid2, fid3)

chaine='Pertes boite de vitesse';
energie_transfo_meca(VD, tsim, pas, vit_dem, distance(length(distance)), csec_bv, wsec_bv, cprim_bv, wprim_bv, pveh, ones(size(vit)), chaine, vimin, fid2, fid3)

energie_emb(VD, tsim, pas, vit_dem, distance(length(distance)), wsec_emb1.*csec_emb1, wprim_emb1.*cprim_emb1_sans_inertie, pinertie_emb1, pveh, ones(size(vit)), vimin, 1, fid2, fid3);

if VD.ACC.ntypacc==2 || VD.ACC.ntypacc==4
   energie_accm2(VD, tsim, pas, vit_dem, distance(length(distance)), waccm1.*caccm1, waccm2.*caccm2, pveh, ones(size(vit)), vimin, fid2, fid3);
elseif VD.ACC.ntypacc==3
   energie_accm3(VD, tsim, pas, vit_dem, distance(length(distance)), paccm_alt, paccm_servo, paccm_air, paccm_ref, pveh, ones(size(vit)), vimin, fid2, fid3);
end

energie_mth(VD, tsim, pas, vit_dem, distance(length(distance)), wmt, cmt, pveh, ones(size(vit)), vimin, fid2, fid3);

% script de fin des bilans sources/consommateurs
ecr_energie('end',fid2,'',[1 2 3 4 5 6]);
ecr_energie('end',fid3,'',[1 2 3 4 5 6]);

% fermeture
fclose(fid);
fclose(fid2);
fclose(fid3);
if isunix
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF1')]);
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF2')]);
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF3')]);
end
