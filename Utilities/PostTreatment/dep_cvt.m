% Function: dep_cvt
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = dep_cvt(VD,temps, pas, vit, wprim_cvt,wsec_cvt,cprim_cvt,csec_cvt, vimin, fid)
%
% Depouillement du composant CVT a rendement constant
% ------------------------------------------------------
function [] = dep_cvt(VD,temps, pas, vit, wprim_cvt,wsec_cvt,cprim_cvt,csec_cvt, vimin, fid)

pprim_cvt=wprim_cvt.*cprim_cvt;
psec_cvt=wsec_cvt.*csec_cvt;

%traction
condition='vit>vimin & pprim_cvt>0 & psec_cvt>0';
tmot=sum(pas(eval(condition)));
if(tmot~=0)
   pprim_moymot=sum(pprim_cvt(eval(condition)).*pas(eval(condition)))/tmot;
   psec_moymot=sum(psec_cvt(eval(condition)).*pas(eval(condition)))/tmot;
else
   pprim_moymot=0;
   psec_moymot=0;
end

% recup
condition='vit>vimin & pprim_cvt<0 & psec_cvt<0';
trec=sum(pas(eval(condition)));
if(trec~=0)
   pprim_moyrec=sum(pprim_cvt(eval(condition)).*pas(eval(condition)))/trec;
   psec_moyrec=sum(psec_cvt(eval(condition)).*pas(eval(condition)))/trec;
else
   pprim_moyrec=0;
   psec_moyrec=0;
end
if(pprim_moymot~=0)
   rdmoymot=psec_moymot/pprim_moymot;
else
   rdmoymot=0;
end
if(psec_moyrec~=0)
   rdmoyrec=pprim_moyrec/psec_moyrec;
else
   rdmoyrec=0;
end

fprintf(fid,'\n%s\n\n','          *** CARACTERISTIQUES CVT       ***');

fprintf(fid,'%s%s\n\n','           Phase motrice','       Phase recuperatrice');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Temps de fonct                : ',tmot,' s      ',trec,' s');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Puissance entree cvt          : ',pprim_moymot./1000,' kW     ',pprim_moyrec./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Puissance sortie cvt          : ',psec_moymot./1000,' kW     ',psec_moyrec./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Rendement moyen               : ',100.*rdmoymot,' %      ',100.*rdmoyrec,' %');
