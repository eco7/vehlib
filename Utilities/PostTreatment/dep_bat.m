% Function: dep_bat
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [pbat] = dep_bat(VD,Temps, pas, vit, Ibat, Ubat, dod, pertes, vimin, fid)
%
% depouillement de la batterie
% Version vectorisee juin 2003
% ------------------------------------------------------
function [pbat] = dep_bat(VD,Temps, pas, vit, Ibat, Ubat, dod, pertes, vimin, fid, cahbat)

if nargin == 10
    fprintf(1,'%s\n','Gestion obsolescence-01/2016-Rajouter la variable cahbat dans l''appel a dep_bat !')
    cahbat=evalin('base','cahbat');
end

pbat=Ubat.*Ibat;

pbat_min=min(pbat);
pbat_max=max(pbat);

% Traction
%%%%%%%%%%%%%
if ~isempty(Temps(Ibat>=0 & vit>=vimin))
    Ubmin_T=min(Ubat(Ibat>=0 & vit>=vimin));
    tUbmin_T=Temps(find(Ubat==Ubmin_T));

    Ibmin_T=min(Ibat(Ibat>=0 & vit>=vimin));
    tIbmin_T=Temps(find(Ibat==Ibmin_T));

    Ubmax_T=max(Ubat(Ibat>=0 & vit>=vimin));
    tUbmax_T=Temps(find(Ubat==Ubmax_T));

    Ibmax_T=max(Ibat(Ibat>=0 & vit>=vimin));
    tIbmax_T=Temps(find(Ibat==Ibmax_T));

    tm=sum(pas(Ibat>=0 & vit>=vimin));
    if(tm~=0)
	Ubatmoy_T=sum(Ubat(Ibat>=0 & vit>=vimin).*pas(Ibat>=0 & vit>=vimin))./tm;
	Ibatmoy_T=sum(Ibat(Ibat>=0 & vit>=vimin).*pas(Ibat>=0 & vit>=vimin))./tm;
	pbat_moy_T=sum(pbat(pbat>=0 & vit>=vimin).*pas(pbat>=0 & vit>=vimin))./tm;
	pertes_bat_moy_T=sum(pertes(pbat>=0 & vit>=vimin).*pas(pbat>=0 & vit>=vimin))./tm;
    else
        Ubatmoy_T=0;
        Ibatmoy_T=0;
        pbat_moy_T=0;
        pertes_bat_moy_T=0;
    end
else
    Ubmin_T=0;
    tUbmin_T=0;
    Ibmin_T=0;
    tIbmin_T=0;
    Ubmax_T=0;
    tUbmax_T=0;
    Ibmax_T=0;
    tIbmax_T=0;
    tm=0;
    Ubatmoy_T=0;
    pbat_moy_T=0;
    Ibatmoy_T=0;
   pertes_bat_moy_T=0;
end

% Recup
%%%%%%%
if ~isempty(Temps(Ibat<0 & vit>=vimin))

   Ubmin_R=min(Ubat(Ibat<0 & vit>=vimin));
   tUbmin_R=Temps(find(Ubat==Ubmin_R));

   Ibmin_R=min(Ibat(Ibat<0 & vit>=vimin));
   tIbmin_R=Temps(find(Ibat==Ibmin_R));

   Ubmax_R=max(Ubat(Ibat<0 & vit>=vimin));
   tUbmax_R=Temps(find(Ubat==Ubmax_R));

   Ibmax_R=max(Ibat(Ibat<0 & vit>=vimin));
   tIbmax_R=Temps(find(Ibat==Ibmax_R));

   tr=sum(pas(Ibat<0 & vit>=vimin));
   if(tr~=0)
      Ubatmoy_R=sum(Ubat(Ibat<0 & vit>=vimin).*pas(Ibat<0 & vit>=vimin))./tr;
      Ibatmoy_R=sum(Ibat(Ibat<0 & vit>=vimin).*pas(Ibat<0 & vit>=vimin))./tr;
      pbat_moy_R=sum(pbat(pbat<0 & vit>=vimin).*pas(pbat<0 & vit>=vimin))./tr;
	  pertes_bat_moy_R=sum(pertes(pbat<0 & vit>=vimin).*pas(pbat<0 & vit>=vimin))./tr;
   else
      Ubatmoy_R=0;
      pbat_moy_R=0;
      Ibatmoy_R=0;
      pertes_bat_moy_R=0;
   end
else
    Ubmin_R=0;
    tUbmin_R=0;
    Ibmin_R=0;
    tIbmin_R=0;
    Ubmax_R=0;
    tUbmax_R=0;
    Ibmax_R=0;
    tIbmax_R=0;
    tr=0;
    Ubatmoy_R=0;
    pbat_moy_R=0;
    Ibatmoy_R=0;
   pertes_bat_moy_R=0;
end
   
% Arret
%%%%%%%%%%

if ~isempty(Temps(vit<vimin))
    Ubmin_A=min(Ubat(vit<vimin));
    tUbmin_A=Temps(find(Ubat==Ubmin_A));

    Ibmin_A=min(Ibat(vit<vimin));
    tIbmin_A=Temps(find(Ibat==Ibmin_A));
    if length(tIbmin_A)>1
        tIbmin_A=tIbmin_A(1);
    end
    Ubmax_A=max(Ubat(vit<vimin));
    tUbmax_A=Temps(find(Ubat==Ubmax_A));

    Ibmax_A=max(Ibat(vit<vimin));
    tIbmax_A=Temps(find(Ibat==Ibmax_A));
    if length(tIbmax_A)>1
        tIbmax_A=tIbmax_A(1);
    end

    ta=sum(pas(vit<vimin));
    if(ta~=0)
        Ubatmoy_A=sum(Ubat(vit<vimin).*pas(vit<vimin))./ta;
        Ibatmoy_A=sum(Ibat(vit<vimin).*pas(vit<vimin))./ta;
        pbat_moy_A=sum(pbat(vit<vimin).*pas(vit<vimin))./ta;
	    pertes_bat_moy_A=sum(pertes(vit<vimin).*pas(vit<vimin))./ta;
    else
        Ubatmoy_A=0;
        pbat_moy_A=0;
        Ibatmoy_A=0;
        pertes_bat_moy_A=0;
    end
else
    Ubmin_A=0;
    tUbmin_A=0;
    Ibmin_A=0;
    tIbmin_A=0;
    tIbmin_A=0;
    Ubmax_A=0;
    tUbmax_A=0;

    Ibmax_A=0;
    tIbmax_A=0;
    ta=0;
    Ubatmoy_A=0;
    Ibatmoy_A=0;
    pbat_moy_A=0;
   pertes_bat_moy_A=0;
end


%Corrrection si on retrouve la valeur de depart
if(isempty(Ubmin_T))
   Ubmax_T=0;
   tUbmax_T=0;
   Ubmin_T=0;
   tUbmin_T=0;
   Ibmax_T=0;
   tIbmax_T=0;
   Ibmin_T=0;
   tIbmin_T=0;
   pertes_bat_moy_T=0;
end
if(isempty(Ubmin_R))
   Ubmax_R=0;
   tUbmax_R=0;
   Ubmin_R=0;
   tUbmin_R=0;
   Ibmax_R=0;
   tIbmax_R=0;
   Ibmin_R=0;
   tIbmin_R=0;
   pertes_bat_moy_R=0;
end
if(isempty(Ubmin_A))
   Ubmax_A=0;
   tUbmax_A=0;
   Ubmin_A=0;
   tUbmin_A=0;
   Ibmax_A=0;
   tIbmax_A=0;
   Ibmin_A=0;
   tIbmin_A=0;
   pertes_bat_moy_A=0;
end



fprintf(fid,'\n%s\n\n', ...
   '          *** CARACTERISTIQUES BATTERIE ***');
%fprintf(fid,'%s%6.2f%s\n', ...
%   ' Etat initial batterie           :   ',evalin('base','cahbat(1)*VD.BATT.Nbranchepar'),' Ah');
fprintf(fid,'%s%6.2f%s\n', ...
   '  Profondeur de decharge initiale :   ',dod(1),' %');
fprintf(fid,'%s%6.2f%s\n', ...
   '  Profondeur de decharge finale   :   ',dod(end),' %');
   

fprintf(fid,'%s%s%s\n\n',...
   '                    Phase motrice','          Phase recuperatrice'  ,...
   '    Phase d''arret');
fprintf(fid,'%s%5.1f%s%5.1f%s%5.1f%s\n', ...
             '  Temps de fonct  :   ',tm,             ' s                 ',tr,' s               ',ta,' s');
fprintf(fid,'%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s\n', ...
   '  Tension maximum :   ',Ubmax_T,  ' V ',tUbmax_T(1), ' s        ',Ubmax_R,  ' V ',tUbmax_R(1),' s      ', ...
   Ubmax_A,  ' V ',tUbmax_A(1),' s');
fprintf(fid,'%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s\n', ...
   '  Tension minimum :   ',Ubmin_T,  ' V ',tUbmin_T(1), ' s        ',Ubmin_R,  ' V ',tUbmin_R(1),' s      ', ...
   Ubmin_A,  ' V ',tUbmin_A(1),' s ');
fprintf(fid,'%s%6.2f%s%6.2f%s%6.2f%s\n', ...
             '  Tension moyenne :   ',Ubatmoy_T,' V                ',Ubatmoy_R,' V              ',Ubatmoy_A,' V ');
fprintf(fid,'%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s\n', ...
   '  Courant maximum :   ',Ibmax_T,  ' A ',tIbmax_T(1), ' s        ',Ibmax_R,  ' A ',tIbmax_R(1),' s      ', ...
   Ibmax_A,  ' A ',tIbmax_A,' s');
fprintf(fid,'%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s%6.2f%s%5.1f%s\n', ...
   '  Courant minimum :   ',Ibmin_T,  ' A ',tIbmin_T(1), ' s        ',Ibmin_R,  ' A ',tIbmin_R(1),' s      ', ...
   Ibmin_A,  ' A ',tIbmin_A(1),' s');
fprintf(fid,'%s%6.2f%s%6.2f%s%6.2f%s\n', ...
             '  Courant moyen   :   ',Ibatmoy_T,' A                ',Ibatmoy_R,' A              ',Ibatmoy_A,' A ');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
   '  Puissance maxi  :   ',pbat_max/1000,' kW               ',pbat_min/1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s%6.2f%s\n', ...
   '  Puissance moy   :   ',pbat_moy_T/1000,' kW               ',pbat_moy_R/1000,' kW             ',pbat_moy_A/1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s%6.2f%s\n', ...
   '  Pertes moy      :   ',pertes_bat_moy_T/1000,' kW               ',pertes_bat_moy_R/1000,' kW             ',pertes_bat_moy_A/1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s%6.2f%s\n', ...
   '  Rendement moy   :   ',100*pbat_moy_T/(pbat_moy_T+pertes_bat_moy_T),' %                ', ...
   100*(pbat_moy_R+pertes_bat_moy_R)/pbat_moy_R,' %              ',100*pbat_moy_A/(pbat_moy_A+pertes_bat_moy_A),' %');
   
fprintf(fid,'\n%s%6.2f%s\n', ...
'  Consommation batterie     :   ',(cahbat(end)-cahbat(1))*VD.BATT.Nbranchepar,' Ah');
fprintf(fid,'\n%s%6.2f%s\n', ...
'  Consommation batterie     :   ',dod(end)-dod(1),' %');
% Attention, rendt faradique non pris en compte !
%fprintf(fid,'\n%s%6.2f%s\n', ...
%             '  Consommation batterie     :   ',sum(trapz(Temps,Ibat))./3600,' Ah');
fprintf(fid,'%s%6.2f%s\n', ...
             '  Tension moyenne batterie  :   ',sum(trapz(Temps,Ubat))/max(Temps),' V');
fprintf(fid,'%s%6.2f%s\n', ...
             '  Courant moyen batterie    :   ',sum(trapz(Temps,Ibat))/max(Temps),' A');
