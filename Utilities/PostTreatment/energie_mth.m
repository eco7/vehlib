% Function: energie_mth
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_mth(VD,temps, pas, vitdem, dist, wmt, cmt, pveh, elehyb, vimin, fid, fid3)
%
% Depouillement des energies consommees par le moteur thermique
% ------------------------------------------------------
function [] = energie_mth(VD,temps, pas, vitdem, dist, wmt, cmt, pveh, elehyb, vimin, fid, fid3)

pmt=wmt.*cmt;

% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>=0';
FMTH_M=sum(pmt(eval(condition)).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb==0 & vitdem>vimin & pveh<0';
FMTH_R=sum(pmt(eval(condition)).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb==0 & vitdem<vimin';
FMTH_A=sum(pmt(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>=0';
FMTH_MH=sum(pmt(eval(condition)).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb~=0 & vitdem>vimin & pveh<0';
FMTH_RH=sum(pmt(eval(condition)).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb~=0 & vitdem<vimin';
FMTH_AH=sum(pmt(eval(condition)).*pas(eval(condition))/3600);

% En Wh
if(max(elehyb)~=0)
 ecr_energie('substract',fid, 'Energie moteur thermique: ', [FMTH_M FMTH_MH FMTH_R FMTH_RH FMTH_A FMTH_AH]);
end

% En Wh/km
if(max(elehyb)~=0)
 ecr_energie('substract',fid3, 'Energie moteur thermique: ', [FMTH_M FMTH_MH FMTH_R FMTH_RH FMTH_A FMTH_AH]/(dist/1000));
end


% Ecriture de la structure de flux
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='FMTH_MA';
flux(j).type='';
flux(j).valeur=FMTH_M+FMTH_MH+FMTH_A+FMTH_AH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='FMTH_R';
flux(j).type='';
flux(j).valeur=FMTH_R+FMTH_RH;
flux(j).position=[0.25 0.37];
flux(j).couleur='green';

% % on met la conso totale
% j=j+1;
% flux(j).nom='Consommation';
% flux(j).type=['Consommation : ' num2str(evalin('base','round(100*100*conso(length(conso))/(max(distance/1000)))/100')) ' l/100 km'];
% flux(j).valeur=NaN;
% flux(j).position=[0.1 0.27];
% flux(j).couleur='blue';

assignin('base','flux',flux);


