% Function: energie_MSY
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_MSY(VD,temps, pas, vit,vitdem, dist, acc, pveh, elhyb, pmec, pstat, pcstat, pex, pcex, ...
%                      PtMeca, PtJstat, PtJexcit, PtFer, vimin, nummg, fid, fid3)
% ------------------------------------------------------
function [] = energie_MSY(VD,temps, pas, vit,vitdem, dist, acc, pveh, elhyb, pmec, pstat, pcstat, pex, pcex, ...
                      PtMeca, PtJstat, PtJexcit, PtFer, vimin, nummg, fid, fid3)
                   
%PtJstat=pjstat;                   
% Initialisation
FMGMECA_M=0;
QMGMECA_M=0;
QMGFER_M=0;
QMGJOULE_M=0;
QMGINELE_M=0;
FMGstat_M=0;
FMGCstat_M=0;
QMGCEX_M=0;

FMGMECA_R=0;
QMGMECA_R=0;
QMGFER_R=0;
QMGJOULE_R=0;
QMGINELE_R=0;
FMGstat_R=0;
FMGCstat_R=0;
QMGCEX_R=0;

FMGMECA_A=0;
QMGMECA_A=0;
QMGFER_A=0;
QMGJOULE_A=0;
QMGINELE_A=0;
FMGstat_A=0;
FMGCstat_A=0;
QMGCEX_A=0;

FMGMECA_MH=0;
QMGMECA_MH=0;
QMGFER_MH=0;
QMGJOULE_MH=0;
QMGINELE_MH=0;
FMGstat_MH=0;
FMGCstat_MH=0;
QMGCEX_MH=0;

FMGMECA_RH=0;
QMGMECA_RH=0;
QMGFER_RH=0;
QMGJOULE_RH=0;
QMGINELE_RH=0;
FMGstat_RH=0;
FMGCstat_RH=0;
QMGCEX_RH=0;

FMGMECA_AH=0;
QMGMECA_AH=0;
QMGFER_AH=0;
QMGJOULE_AH=0;
QMGINELE_AH=0;
FMGstat_AH=0;
FMGCstat_AH=0;
QMGCEX_AH=0;

for i=2:length(temps)
   if(elhyb(i)==0)
      %Phase tout elec

      if(vitdem(i)>vimin)
         if(pveh(i)>= 0)

      FMGMECA_M=FMGMECA_M+pmec(i)*pas(i)/3600;
      QMGMECA_M=QMGMECA_M+PtMeca(i)*pas(i)/3600;
      QMGFER_M=QMGFER_M+PtFer(i)*pas(i)/3600;
      QMGJOULE_M=QMGJOULE_M+PtJstat(i)*pas(i)/3600;
      %QMGINELE_M=QMGINELE_M+pin_ele(i)*pas(i)/3600
      FMGstat_M=FMGstat_M+pstat(i)*pas(i)/3600;
      FMGCstat_M=FMGCstat_M+pcstat(i)*pas(i)/3600;
      QMGCEX_M=QMGCEX_M+pcex(i)*pas(i)/3600;
   else
      FMGMECA_R=FMGMECA_R+pmec(i)*pas(i)/3600;
      QMGMECA_R=QMGMECA_R+PtMeca(i)*pas(i)/3600;
      QMGFER_R=QMGFER_R+PtFer(i)*pas(i)/3600;
      QMGJOULE_R=QMGJOULE_R+PtJstat(i)*pas(i)/3600;
      %QMGINELE_R=QMGINELE_R+pin_ele(i)*pas(i)/3600;
      FMGstat_R=FMGstat_R+pstat(i)*pas(i)/3600;
      FMGCstat_R=FMGCstat_R+pcstat(i)*pas(i)/3600;
      QMGCEX_R=QMGCEX_R+pcex(i)*pas(i)/3600;
   end
 else
      FMGMECA_A=FMGMECA_A+pmec(i)*pas(i)/3600;
      QMGMECA_A=QMGMECA_A+PtMeca(i)*pas(i)/3600;
      QMGFER_A=QMGFER_A+PtFer(i)*pas(i)/3600;
      QMGJOULE_A=QMGJOULE_A+PtJstat(i)*pas(i)/3600;
      %QMGINELE_A=QMGINELE_A+pin_ele(i)*pas(i)/3600;
      FMGstat_A=FMGstat_A+pstat(i)*pas(i)/3600;
      FMGCstat_A=FMGCstat_A+pcstat(i)*pas(i)/3600;
      QMGCEX_A=QMGCEX_A+pcex(i)*pas(i)/3600;
 end
else
   %phase hybride
      if(vitdem(i)>vimin)
         if(pveh(i)>= 0)

      FMGMECA_MH=FMGMECA_MH+pmec(i)*pas(i)/3600;
      QMGMECA_MH=QMGMECA_MH+PtMeca(i)*pas(i)/3600;
      QMGFER_MH=QMGFER_MH+PtFer(i)*pas(i)/3600;
      QMGJOULE_MH=QMGJOULE_MH+PtJstat(i)*pas(i)/3600;
      %QMGINELE_MH=QMGINELE_MH+pin_ele(i)*pas(i)/3600;
      FMGstat_MH=FMGstat_MH+pstat(i)*pas(i)/3600;
      FMGCstat_MH=FMGCstat_MH+pcstat(i)*pas(i)/3600;
      QMGCEX_MH=QMGCEX_MH+pcex(i)*pas(i)/3600;
   else
      FMGMECA_RH=FMGMECA_RH+pmec(i)*pas(i)/3600;
      QMGMECA_RH=QMGMECA_RH+PtMeca(i)*pas(i)/3600;
      QMGFER_RH=QMGFER_RH+PtFer(i)*pas(i)/3600;
      QMGJOULE_RH=QMGJOULE_RH+PtJstat(i)*pas(i)/3600;
      %QMGINELE_RH=QMGINELE_RH+pin_ele(i)*pas(i)/3600;
      FMGstat_RH=FMGstat_RH+pstat(i)*pas(i)/3600;
      FMGCstat_RH=FMGCstat_RH+pcstat(i)*pas(i)/3600;
      QMGCEX_RH=QMGCEX_RH+pcex(i)*pas(i)/3600;
   end
 else
      FMGMECA_AH=FMGMECA_AH+pmec(i)*pas(i)/3600;
      QMGMECA_AH=QMGMECA_AH+PtMeca(i)*pas(i)/3600;
      QMGFER_AH=QMGFER_AH+PtFer(i)*pas(i)/3600;
      QMGJOULE_AH=QMGJOULE_AH+PtJstat(i)*pas(i)/3600;
      %QMGINELE_AH=QMGINELE_AH+pin_ele(i)*pas(i)/3600;
      FMGstat_AH=FMGstat_AH+pstat(i)*pas(i)/3600;
      FMGCstat_AH=FMGCstat_AH+pcstat(i)*pas(i)/3600;
      QMGCEX_AH=QMGCEX_AH+pcex(i)*pas(i)/3600;
 end
   
   
end
end
% en Wh.
if(max(elhyb)==0)
 ecr_energie('sum',fid, 'Energie meca Mot. elec. : ', [FMGMECA_M FMGMECA_R]);
 ecr_energie('sum',fid, 'Pertes meca Mot. elec.  : ', [QMGMECA_M QMGMECA_R]);
 ecr_energie('sum',fid, 'Pertes fer Mot. elec.   : ', [QMGFER_M QMGFER_R]);
 ecr_energie('sum',fid, 'Pertes joule stator     : ', [QMGJOULE_M QMGJOULE_R]);
 %ecr_energie('sum',fid, 'Pertes inertie elec     : ', [QMGINELE_M QMGINELE_R QMGINELE_A]);
 %ecr_energie('sum',fid, 'Pertes au stator        : ', [FMGstat_M-FMGMECA_M FMGstat_R-FMGMECA_R]);

fprintf(fid,'%s%s\n',...
  '                           -----------','            -----------');
 ecr_energie('sum',fid, 'Energie sur le stator   : ', [FMGstat_M FMGstat_R]);
 ecr_energie('sum',fid, 'Pertes onduleur         : ', [FMGCstat_M-FMGstat_M FMGCstat_R-FMGstat_R]);
 ecr_energie('sum',fid, 'Pertes excitation       : ', [QMGCEX_M QMGCEX_R QMGCEX_A]);
fprintf(fid,'%s%s\n',...
  '                           -----------','            -----------');
 ecr_energie('sum',fid, 'Energie controle        : ', [FMGCstat_M+QMGCEX_M FMGCstat_R+QMGCEX_R FMGCstat_A+QMGCEX_A]);
 
else
   
 %ecr_energie('sum',fid, 'Pertes meca Mot. elec.  : ', [QMGMECA_M QMGMECA_MH QMGMECA_R QMGMECA_RH]);
 %ecr_energie('sum',fid, 'Pertes fer Mot. elec.   : ', [QMGFER_M QMGFER_MH QMGFER_R QMGFER_RH]);
 %ecr_energie('sum',fid, 'Pertes joule stator     : ', [QMGJOULE_M QMGJOULE_MH QMGJOULE_R QMGJOULE_RH]);
 %ecr_energie('sum',fid, 'Pertes inertie elec     : ', [QMGINELE_M QMGINELE_MH QMGINELE_R QMGINELE_RH QMGINELE_A QMGINELE_AH]);
 %ecr_energie('sum',fid, 'Pertes au stator        : ', [FMGstat_M-FMGMECA_M FMGstat_MH-FMGMECA_MH FMGstat_R-FMGMECA_R FMGstat_RH-FMGMECA_RH]);
 %ecr_energie('sum',fid, 'Pertes onduleur         : ', [FMGCstat_M-FMGstat_M FMGCstat_MH-FMGstat_MH FMGCstat_R-FMGstat_R FMGCstat_RH-FMGstat_RH]);
 %ecr_energie('sum',fid, 'Pertes excitation       : ', [QMGCEX_M QMGCEX_MH QMGCEX_R QMGCEX_RH QMGCEX_A QMGCEX_AH]);
 %fprintf(fid,'%s%s\n',...
 %  '                           -----------','            -----------');
if(nummg==1)
 ecr_energie('sum',fid, 'Pertes au stator VD.ACM1   : ', [FMGstat_M-FMGMECA_M FMGstat_MH-FMGMECA_MH FMGstat_R-FMGMECA_R FMGstat_RH-FMGMECA_RH]);
 ecr_energie('sum',fid, 'Pertes onduleur VD.ACM1    : ', [FMGCstat_M-FMGstat_M FMGCstat_MH-FMGstat_MH FMGCstat_R-FMGstat_R FMGCstat_RH-FMGstat_RH]);
 ecr_energie('sum',fid, 'Pertes excitation VD.ACM1  : ', [QMGCEX_M QMGCEX_MH QMGCEX_R QMGCEX_RH QMGCEX_A QMGCEX_AH]);
%ecr_energie('sum',fid, 'Pertes Mot1+cont1        : ', [FMGCstat_M-FMGMECA_M+QMGCEX_M FMGCstat_MH-FMGMECA_MH+QMGCEX_MH FMGCstat_R-FMGMECA_R+QMGCEX_R FMGCstat_RH-FMGMECA_RH+QMGCEX_RH FMGCstat_A-FMGMECA_A+QMGCEX_A FMGCstat_AH-FMGMECA_AH+QMGCEX_AH]);
elseif(nummg==2)      
 ecr_energie('sum',fid, 'Pertes au stator VD.ACM2   : ', [FMGstat_M-FMGMECA_M FMGstat_MH-FMGMECA_MH FMGstat_R-FMGMECA_R FMGstat_RH-FMGMECA_RH]);
 ecr_energie('sum',fid, 'Pertes onduleur VD.ACM2    : ', [FMGCstat_M-FMGstat_M FMGCstat_MH-FMGstat_MH FMGCstat_R-FMGstat_R FMGCstat_RH-FMGstat_RH]);
 ecr_energie('sum',fid, 'Pertes excitation VD.ACM2  : ', [QMGCEX_M QMGCEX_MH QMGCEX_R QMGCEX_RH QMGCEX_A QMGCEX_AH]);
%ecr_energie('sum',fid, 'Pertes Mot2+cont2        : ', [FMGCstat_M-FMGMECA_M+QMGCEX_M FMGCstat_MH-FMGMECA_MH+QMGCEX_MH FMGCstat_R-FMGMECA_R+QMGCEX_R FMGCstat_RH-FMGMECA_RH+QMGCEX_RH FMGCstat_A-FMGMECA_A+QMGCEX_A FMGCstat_AH-FMGMECA_AH+QMGCEX_AH]);
end
end

% en Wh/km
if(max(elhyb)==0)
 ecr_energie('sum',fid3, 'Energie meca Mot. elec. : ', [FMGMECA_M FMGMECA_R]/(dist/1000));
 %ecr_energie('sum',fid3, 'Pertes meca Mot. elec.  : ', [QMGMECA_M QMGMECA_R]/(dist/1000));
 %ecr_energie('sum',fid3, 'Pertes fer Mot. elec.   : ', [QMGFER_M QMGFER_R]/(dist/1000));
 %ecr_energie('sum',fid3, 'Pertes joule stator     : ', [QMGJOULE_M QMGJOULE_R]/(dist/1000));
 %ecr_energie('sum',fid3, 'Pertes inertie elec     : ', [QMGINELE_M QMGINELE_R QMGINELE_A]/(dist/1000));
 ecr_energie('sum',fid3, 'Pertes au stator        : ', [FMGstat_M-FMGMECA_M FMGstat_R-FMGMECA_R]/(dist/1000));

fprintf(fid3,'%s%s\n',...
  '                           -----------','            -----------');
 ecr_energie('sum',fid3, 'Energie sur le stator   : ', [FMGstat_M FMGstat_R]/(dist/1000));
 ecr_energie('sum',fid3, 'Pertes onduleur         : ', [FMGCstat_M-FMGstat_M FMGCstat_R-FMGstat_R]/(dist/1000));
 ecr_energie('sum',fid3, 'Pertes excitation       : ', [QMGCEX_M QMGCEX_R QMGCEX_A]/(dist/1000));
fprintf(fid3,'%s%s\n',...
  '                           -----------','            -----------');
 ecr_energie('sum',fid3, 'Energie controle        : ', [FMGCstat_M+QMGCEX_M FMGCstat_R+QMGCEX_R FMGCstat_A+QMGCEX_A]/(dist/1000));

else
   
 %ecr_energie('sum',fid3, 'Pertes meca Mot. elec.  : ', [QMGMECA_M QMGMECA_MH QMGMECA_R QMGMECA_RH]/(dist/1000));
 %ecr_energie('sum',fid3, 'Pertes fer Mot. elec.   : ', [QMGFER_M QMGFER_MH QMGFER_R QMGFER_RH]/(dist/1000));
 %ecr_energie('sum',fid3, 'Pertes joule stator     : ', [QMGJOULE_M QMGJOULE_MH QMGJOULE_R QMGJOULE_RH]/(dist/1000));
 %ecr_energie('sum',fid3, 'Pertes inertie elec     : ', [QMGINELE_M QMGINELE_MH QMGINELE_R QMGINELE_RH QMGINELE_A QMGINELE_AH]/(dist/1000));
 %ecr_energie('sum',fid3, 'Pertes au stator        : ', [FMGstat_M-FMGMECA_M FMGstat_MH-FMGMECA_MH FMGstat_R-FMGMECA_R FMGstat_RH-FMGMECA_RH]/(dist/1000));
 %ecr_energie('sum',fid3, 'Pertes onduleur         : ', [FMGCstat_M-FMGstat_M FMGCstat_MH-FMGstat_MH FMGCstat_R-FMGstat_R FMGCstat_RH-FMGstat_RH]/(dist/1000));
 %ecr_energie('sum',fid3, 'Pertes excitation       : ', [QMGCEX_M QMGCEX_MH QMGCEX_R QMGCEX_RH QMGCEX_A QMGCEX_AH]/(dist/1000));
fprintf(fid,'%s%s\n',...
  '                           -----------','            -----------');
if(nummg==1)
 ecr_energie('sum',fid3, 'Pertes au stator VD.ACM1   : ', [FMGstat_M-FMGMECA_M FMGstat_MH-FMGMECA_MH FMGstat_R-FMGMECA_R FMGstat_RH-FMGMECA_RH]/(dist/1000));
 ecr_energie('sum',fid3, 'Pertes onduleur VD.ACM1    : ', [FMGCstat_M-FMGstat_M FMGCstat_MH-FMGstat_MH FMGCstat_R-FMGstat_R FMGCstat_RH-FMGstat_RH]/(dist/1000));
 ecr_energie('sum',fid3, 'Pertes excitation VD.ACM1  : ', [QMGCEX_M QMGCEX_MH QMGCEX_R QMGCEX_RH QMGCEX_A QMGCEX_AH]/(dist/1000));
%ecr_energie('sum',fid3, 'Pertes Mot1+cont1       : ', [FMGCstat_M-FMGMECA_M+QMGCEX_M FMGCstat_MH-FMGMECA_MH+QMGCEX_MH FMGCstat_R-FMGMECA_R+QMGCEX_R FMGCstat_RH-FMGMECA_RH+QMGCEX_RH FMGCstat_A-FMGMECA_A+QMGCEX_A FMGCstat_AH-FMGMECA_AH+QMGCEX_AH]/(dist/1000));
elseif(nummg==2)      
 ecr_energie('sum',fid3, 'Pertes au stator VD.ACM2   : ', [FMGstat_M-FMGMECA_M FMGstat_MH-FMGMECA_MH FMGstat_R-FMGMECA_R FMGstat_RH-FMGMECA_RH]/(dist/1000));
 ecr_energie('sum',fid3, 'Pertes onduleur VD.ACM2    : ', [FMGCstat_M-FMGstat_M FMGCstat_MH-FMGstat_MH FMGCstat_R-FMGstat_R FMGCstat_RH-FMGstat_RH]/(dist/1000));
 ecr_energie('sum',fid3, 'Pertes excitation VD.ACM2  : ', [QMGCEX_M QMGCEX_MH QMGCEX_R QMGCEX_RH QMGCEX_A QMGCEX_AH]/(dist/1000));
%ecr_energie('sum',fid3, 'Pertes Mot2+cont2       : ', [FMGCstat_M-FMGMECA_M+QMGCEX_M FMGCstat_MH-FMGMECA_MH+QMGCEX_MH FMGCstat_R-FMGMECA_R+QMGCEX_R FMGCstat_RH-FMGMECA_RH+QMGCEX_RH FMGCstat_A-FMGMECA_A+QMGCEX_A FMGCstat_AH-FMGMECA_AH+QMGCEX_AH]/(dist/1000));
end
end
end

