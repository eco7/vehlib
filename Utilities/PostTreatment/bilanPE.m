function [bp] = bilanPE(VD,param,ResXml,num_fig)

if nargin==3
    num_fig=100;
end

warning off;
bp=[];

% Puissance au niveau des roues
[croue,wroue,masse]=XmlValue(ResXml,{'croue','wroue','masse'});

bp.proue=croue.*wroue;
bp.prouet=wroue.*croue; % Puissance en traction
bp.prouet(bp.prouet<0)=0;
bp.prouer=wroue.*croue; % Puissance en recup
bp.prouer(bp.prouer>0)=0;
bp.Eroue=sum(VD.CYCL.pas_temps.*bp.proue)/3600;
bp.Erouet=sum(VD.CYCL.pas_temps.*bp.prouet)/3600;
bp.Erouer=sum(VD.CYCL.pas_temps.*bp.prouer)/3600;

[cfrein_meca]=XmlValue(ResXml,{'cfrein_meca'});
if ~isnan(cfrein_meca)
    bp.qfrein_meca=cfrein_meca.*wroue;
    bp.Efrein=sum(VD.CYCL.pas_temps.*bp.qfrein_meca)/3600;
    if param.verbose>=1
        fprintf(1,'%s %.1f %s\n','L''energie dissipee dans les freins :',bp.Efrein,'Wh');
    end
else
    bp.qfrein_meca=0*ones(1,length(VD.CYCL.temps));
    bp.Efrein=0;
end

if isfield(VD.CYCL,'P_driv')
    bp.pdriv=VD.CYCL.P_driv;
    bp.Edriv=sum(VD.CYCL.pas_temps.*bp.pdriv)/3600;
else
    bp.pdriv=zeros(1,length(VD.CYCL.temps));
    bp.Edriv=0;
end

if VD.VEHI.ntypveh==1 || VD.VEHI.ntypveh==3;
    [ERR,~,~,~,penteFpk,Fres,Faero,Froul,Fpente]=calc_efforts_vehicule([],VD,masse,0);
    vit=VD.CYCL.vitesse;
    bp.proue_roul=Froul.*vit;
    bp.proue_aero=Faero.*vit;
    bp.proue_pente=Fpente.*vit;
    bp.Eroue_roul=sum(VD.CYCL.pas_temps.*bp.proue_roul)/3600;
    bp.Eroue_aero=sum(VD.CYCL.pas_temps.*bp.proue_aero)/3600;
    bp.Eroue_pente=sum(VD.CYCL.pas_temps.*bp.proue_pente)/3600;
    bp.proue_penteP=bp.proue_pente;
    bp.proue_penteP(bp.proue_penteP<0)=0;
    bp.proue_penteN=bp.proue_pente;
    bp.proue_penteN( bp.proue_penteN>0)=0;
    bp.Eroue_penteP=sum(VD.CYCL.pas_temps.*bp.proue_penteP)/3600;
    bp.Eroue_penteN=sum(VD.CYCL.pas_temps.*bp.proue_penteN)/3600;
end

if param.verbose>=1
    fprintf(1,'%s %.1f %s %s %.1f %s %s %.1f %s\n','L''energie fournie aux roues : ',bp.Eroue,'Wh',...
        'en traction',bp.Erouet,'Wh','en recup',bp.Erouer,'Wh');
    if isfield(bp,'Eroue_roul')
         fprintf(1,'%s %.6f %s\n','Les pertes de resistance au roulement : ',bp.Eroue_roul,'Wh');
    end
    if VD.VEHI.ntypveh==1 || VD.VEHI.ntypveh==3;
        fprintf(1,'%s %.1f %s %s %.1f %s %s %.1f %s\n','L''energie global de "pentes" : ',bp.Eroue_pente,'Wh',...
            'positive',bp.Eroue_penteP,'Wh','negative',bp.Eroue_penteN,'Wh');
    end
end

if isfield(VD,'BATT')
    [ibat,ubat,Rbat,u0bat,RdF]=XmlValue(ResXml,{'ibat','ubat','Rbat','u0bat','RdF'});
    bp.pbat=ibat.*ubat;
    bp.pjbat=Rbat.*ibat.*ibat;
    bp.p0bat=u0bat.*ibat;
    bp.Ejbat=sum(VD.CYCL.pas_temps.*bp.pjbat)/3600; % Energie perdue par pertes joules dans la batterie
    %bp.E0bat=trapz(VD.CYCL.temps,p0bat)/3600,'Wh'); % energie fournie par la batterie (source parfaite)  % ne tiens pas compte du RdF donc ne sera pas nul a Dsoc nul
    bp.E0bat=sum(VD.CYCL.pas_temps.*bp.p0bat.*RdF)/3600; % energie fournie par la batterie (source parfaite)
    bp.Erdfbat=-sum(VD.CYCL.pas_temps.*bp.p0bat.*(1-RdF))/3600; % energie perdue par rendemment faradique dans la batterie
    bp.Epbat=bp.Ejbat+bp.Erdfbat; % energie totale dissipee dans la batterie
    bp.Ebat=sum(VD.CYCL.pas_temps.*ibat.*ubat)/3600; % energie fournie par la batterie
    if param.verbose>=1
        fprintf(1,'%s %.6f %s\n','Les pertes joules dans les batteries : ',bp.Ejbat,'Wh');
        fprintf(1,'%s %.1f %s\n','L''energie fournie par la batterie (source parfaite pluds RdF) : ',bp.E0bat,'Wh'); % ne tiens pas compte du RdF donc ne sera pas nul a Dsoc nul
        fprintf(1,'%s %.1f %s\n','L''energie fournie par la batterie : ',bp.Ebat,'Wh');
    end
else
    bp.pbat=0*ones(1,length(VD.CYCL.temps));
    bp.pjbat=0*ones(1,length(VD.CYCL.temps));
    bp.p0bat=0*ones(1,length(VD.CYCL.temps));
    bp.Ejbat=0;
    bp.E0bat=0;
    bp.Epbat=0;
    bp.Erdfbat=0;
    bp.Ebat=0;
end

if isfield(VD,'ACM1')
    [bp.qacm1,wacm1,dwacm1,cacm1]=XmlValue(ResXml,{'qacm1','wacm1','dwacm1','cacm1'});
    bp.iacm1=VD.ACM1.J_mg.*dwacm1.*wacm1;
    bp.Eacm1=VD.VEHI.nbacm1*sum(VD.CYCL.pas_temps.*bp.qacm1)/3600; % Les pertes dans les machines electrique
    if param.verbose>=1
        fprintf(1,'%s %.1f %s\n','Les pertes dans les machines electrique acm1 : ',bp.Eacm1,'Wh');% Les pertes dans les machines electrique
    end
else
    bp.qacm1=0*ones(1,length(VD.CYCL.temps));
    bp.Eacm1=0;
    bp.iacm1=0*ones(1,length(VD.CYCL.temps));
end

if isfield(VD,'ACM2')
    [bp.qacm2,wacm2,dwacm2,cacm2]=XmlValue(ResXml,{'qacm2','wacm2','dwacm2','cacm2'});
    bp.iacm2=VD.ACM2.J_mg.*dwacm2.*wacm2;
    bp.Eacm2=VD.VEHI.nbacm1*sum(VD.CYCL.pas_temps.*bp.qacm2)/3600; % Les pertes dans les machines electrique

    if param.verbose>=1
        fprintf(1,'%s %.1f %s\n','Les pertes dans les machines electrique acm2 : ',bp.Eacm2,'Wh');% Les pertes dans les machines electrique
    end
else
    bp.qacm2=zeros(1,length(VD.CYCL.temps));
    bp.iacm2=zeros(1,length(VD.CYCL.temps));
    bp.Eacm2=0;
end

if isfield(VD,'RED')
    [cprim_red,wprim_red,csec_red,wsec_red]=XmlValue(ResXml,{'cprim_red','wprim_red','csec_red','wsec_red'});
    bp.qred=abs(cprim_red.*wprim_red-csec_red.*wsec_red); % Les pertes dans le reducteur
    bp.Ered=sum(VD.CYCL.pas_temps.*bp.qred)/3600;
    if param.verbose>=1
        fprintf(1,'%s %.1f %s\n','Les pertes dans le reducteur : ',bp.Ered,'Wh');
    end
else
    bp.qred=0*ones(1,length(VD.CYCL.temps));
    bp.Ered=0;
end

if isfield(VD,'RED2')
    [cprim_red2,wprim_red2,csec_red2,wsec_red2]=XmlValue(ResXml,{'cprim_red2','wprim_red2','csec_red2','wsec_red2'});
    bp.qred2=abs(cprim_red2.*wprim_red2-csec_red2.*wsec_red2); % Les pertes dans le reducteur
    bp.Ered2=sum(VD.CYCL.pas_temps.*bp.qred2)/3600;
    if param.verbose>=1
        fprintf(1,'%s %.1f %s\n','Les pertes dans le reducteur 2: ',bp.Ered2,'Wh');
    end
else
    bp.qred2=0*ones(1,length(VD.CYCL.temps));
    bp.Ered2=0;
end


if isfield(VD,'BV')
    [cprim_bv,wprim_bv,csec_bv,wsec_bv]=XmlValue(ResXml,{'cprim_bv','wprim_bv','csec_bv','wsec_bv'});
    bp.qbv=abs(cprim_bv.*wprim_bv-csec_bv.*wsec_bv); % Les pertes dans la boite vitesse
    bp.Ebv=sum(VD.CYCL.pas_temps.*bp.qbv)/3600;
    if param.verbose>=1
        fprintf(1,'%s %.1f %s\n','Les pertes dans la boite de vitesse: ',bp.Ebv,'Wh');
    end
else
    bp.qbv=0*ones(1,length(VD.CYCL.temps));
    bp.Ebv=0;
end

if isfield(VD,'EMBR1')
    [cprim_emb1,wprim_emb1,csec_emb1,wsec_emb1]=XmlValue(ResXml,{'cprim_emb1','wprim_emb1','csec_emb1','wsec_emb1'});
    if isfield(VD.EMBR1,'mode') && strcmp(VD.EMBR1.mode,'serie_par')
        [elhyb]=XmlValue(ResXml,'elhyb');
        bp.qemb1=zeros(1,length(VD.CYCL.temps));
        I_e2=find(elhyb==2);
        bp.qemb1(I_e2)=abs(cprim_emb1(I_e2).*wprim_emb1(I_e2)-csec_emb1(I_e2).*wsec_emb1(I_e2)); 
    else
         bp.qemb1=abs(cprim_emb1.*wprim_emb1-csec_emb1.*wsec_emb1); 
    end
    
    bp.Eemb1=sum(VD.CYCL.pas_temps.*bp.qemb1)/3600;
    if param.verbose>=1
        fprintf(1,'%s %.1f %s\n','Les pertes dans l''embrayage emb1 : ',bp.Eemb1,'Wh');
    end
else
    bp.qemb1=zeros(1,length(VD.CYCL.temps));
    bp.Eemb1=0;
end

if isfield(VD,'ADCPL')
    [cprim1_cpl,wprim1_cpl,cprim2_cpl,wprim2_cpl,csec_cpl,wsec_cpl]=XmlValue(ResXml,{'cprim1_cpl','wprim1_cpl','cprim2_cpl','wprim2_cpl','csec_cpl','wsec_cpl'});
    bp.qadcpl=abs(cprim1_cpl.*wprim1_cpl+cprim2_cpl.*wprim2_cpl-csec_cpl.*wsec_cpl); % Les pertes dans le coupleur
    bp.Eadcpl=sum(VD.CYCL.pas_temps.*bp.qadcpl)/3600;
    if param.verbose>=1
        fprintf(1,'%s %.1f %s\n','Les pertes dans le coupleur : ',bp.Eadcpl,'Wh');
    end
else
    bp.qadcpl=0*ones(1,length(VD.CYCL.temps));
    bp.Eadcpl=0;
end

if isfield(VD,'ADCPL1')
    [cprim1_cpl1,wprim1_cpl1,cprim2_cpl1,wprim2_cpl1,csec_cpl1,wsec_cpl1]=XmlValue(ResXml,{'cprim1_cpl1','wprim1_cpl1','cprim2_cpl1','wprim2_cpl1','csec_cpl1','wsec_cpl1'});
    bp.qadcpl1=abs(cprim1_cpl1.*wprim1_cpl1+cprim2_cpl1.*wprim2_cpl1-csec_cpl1.*wsec_cpl1); % Les pertes dans le coupleur1
    bp.Eadcpl1=sum(VD.CYCL.pas_temps.*bp.qadcpl1)/3600;
    if param.verbose>=1
        fprintf(1,'%s %.1f %s\n','Les pertes dans le coupleur 1 : ',bp.Eadcpl1,'Wh');
    end
else
    bp.qadcpl1=0*ones(1,length(VD.CYCL.temps));
    bp.Eadcpl1=0;
end

if isfield(VD,'ADCPL2')
    [cprim1_cpl2,wprim1_cpl2,cprim2_cpl2,wprim2_cpl2,csec_cpl2,wsec_cpl2]=XmlValue(ResXml,{'cprim1_cpl2','wprim1_cpl2','cprim2_cpl2','wprim2_cpl2','csec_cpl2','wsec_cpl2'});
    bp.qadcpl2=abs(cprim1_cpl2.*wprim1_cpl2+cprim2_cpl2.*wprim2_cpl2-csec_cpl2.*wsec_cpl2); % Les pertes dans le coupleur1
    bp.Eadcpl2=sum(VD.CYCL.pas_temps.*bp.qadcpl2)/3600;
    if param.verbose>=1
        fprintf(1,'%s %.1f %s\n','Les pertes dans le coupleur 1 : ',bp.Eadcpl2,'Wh');
    end
else
    bp.qadcpl2=0*ones(1,length(VD.CYCL.temps));
    bp.Eadcpl2=0;
end

if isfield(VD,'EPI')
    [cps,wps,csoleil,wsoleil,ccour,wcour]=XmlValue(ResXml,{'cps_train','wps_train','csoleil_train','wsoleil_train','ccour_train','wcour_train'});
    if isfield(VD.EPI,'sens') && strcmp(VD.EPI.sens,'hsdp')
        bp.qepi=abs(ccour.*wcour+csoleil.*wsoleil-cps.*wps);
    else
        bp.qepi=abs(cps.*wps+csoleil.*wsoleil-ccour.*wcour);
    end
    bp.Eepi=sum(VD.CYCL.pas_temps.*bp.qepi)/3600;
    
    if param.verbose>=1
        fprintf(1,'%s %.1f %s\n','L''energie dissipee dans le train epicycloidal :',bp.Eepi,'Wh');
    end
else
    bp.qepi=0*ones(1,length(VD.CYCL.temps));
    bp.Eepi=0;
end

if isfield(VD,'ACC')
    if VD.ACC.ntypacc==1
        bp.pacc=VD.ACC.Pacc_elec.*ones(size(VD.CYCL.temps));
        if isfield(VD.ACC,'Pacc_elec_PAC') && isfield(VD.ACC,'Pmin_comp_PAC')
            bp.pacc=bp.pacc+(VD.ACC.Pacc_elec_PAC+VD.ACC.Pmin_comp_PAC).*ones(size(VD.CYCL.temps));
        end
        bp.Eacc=sum(VD.CYCL.pas_temps.*bp.pacc)/3600;
    elseif VD.ACC.ntypacc==2 || VD.ACC.ntypacc==4
        [pacc]=XmlValue(ResXml,{'Paccm1'});
        bp.pacc=-pacc;
        bp.Eacc=sum(VD.CYCL.pas_temps.*bp.pacc)/3600;
    end
    if param.verbose>=1
        fprintf(1,'%s %.1f %s\n','L''energie consommee par les accessoires : ',bp.Eacc,'Wh');
    end
else
    bp.pacc=0*ones(1,length(VD.CYCL.temps));
    bp.Eacc=0;
end


if isfield(VD,'MOTH')
    [cmt,wmt,dwmt,cfrein_meca]=XmlValue(ResXml,{'cmt','wmt','dwmt','cfrein_meca'});
    bp.pmt=cmt.*wmt;
    bp.imt=VD.MOTH.J_mt.*dwmt.*wmt;
    bp.Emt=sum(VD.CYCL.pas_temps.*bp.pmt)/3600;
    bp.Earbremt=sum(VD.CYCL.pas_temps.*(bp.pmt-bp.imt))/3600;
    bp.pmtP=bp.pmt;
    bp.pmtP(bp.pmtP<0)=0;
    bp.pmtN=bp.pmt;
    bp.pmtN( bp.pmtN>0)=0;
    bp.EmtP=sum(VD.CYCL.pas_temps.*bp.pmtP)/3600;
    bp.EmtN=sum(VD.CYCL.pas_temps.*bp.pmtN)/3600;
else
    bp.pmt=zeros(1,length(VD.CYCL.temps));
    bp.imt=zeros(1,length(VD.CYCL.temps));
end

if isfield(VD,'PAC')
    [ipac,upac]=XmlValue(ResXml,{'iPAC','uPAC'});
    bp.ppac=ipac.*upac;   
    bp.Epac=sum(VD.CYCL.pas_temps.*ipac.*upac)/3600; % energie fournie par la pile
else
    bp.ppac=zeros(1,length(VD.CYCL.temps));
    bp.pac=zeros(1,length(VD.CYCL.temps));
    bp.Epac=0;
end

% Pertes motelec en mode tout thermique
bp.Pf_motelec=0*ones(1,length(VD.CYCL.temps));
if isfield(param,'tout_thermique') && param.tout_thermique==1
    [cacm1,wacm1,elhyb]=XmlValue(ResXml,{'cacm1','wacm1','elhyb'});
    bp.Pf_motelec(elhyb==2)=-cacm1(elhyb==2).*wacm1(elhyb==2);
end


% HP_BV_2emb
% bP1=cmt.*wmt+bp.p0bat-bp.pjbat-VD.VEHI.nbacm1*bp.qacm1-bp.Pf_motelec-bp.pacc-bp.qred-bp.qadcpl-bp.qemb1-bp.proue-bp.qbv-VD.ACM1.J_mg.*dwacm1.*wacm1-VD.MOTH.J_mt.*dwmt.*wmt-cfrein_meca.*wroue;

% HPDP_EPI
% bp.bilanP=-croue.*wroue-Pertes_red+r.cmt.*r.wmt+p0bat-Pjbat-r.pacc-r.qacm2-Pertes_train-r.qacm1-VD.ACM1.J_mg.*r.dwacm1.*r.wacm1-r.cfrein_meca.*wroue;

bp.bilanP= bp.pmt  -bp.imt + bp.p0bat -bp.pjbat + bp.ppac + bp.pdriv -bp.proue -bp.qfrein_meca  -bp.pacc...
          -VD.VEHI.nbacm1*bp.qacm1-VD.VEHI.nbacm1*bp.iacm1 ...
          -bp.qacm2 -bp.iacm2 ...
          -bp.qred  -bp.qred2 -bp.qepi  -bp.qadcpl -bp.qadcpl1 -bp.qadcpl2 -bp.qemb1  -bp.qbv...
          -bp.Pf_motelec;
          
if param.verbose>=2
    figure(10)
    clf
    if isfield(param,'optim') && ~strcmp(lower(param.optim),'prog_dyn')
        plot(VD.CYCL.temps,bp.bilanP)
    else %pb du point origine
        plot(VD.CYCL.temps(2:end),bp.bilanP(2:end))
    end
    grid
    legend('bilan de puissance')
end

bp.bilanE=[];
bp.nombilanE=[];
ind=[];

dist=trapz(VD.CYCL.temps,VD.CYCL.vitesse);
comp={'Eroue','Eacc','Epac','Edriv','Efrein','Eacm1','Eacm2','Epbat','Eepi','Ered','Ered2','Eadcpl','Eadcpl1','Eadcpl2','Ebv','Eemb1'};
legend_name={'wheel energy','auxiliary energy','PAC energy','driver energy','brake losses','em1 losses','em2 losses','battery losses',...
    'planetary gear lossses','final gear losses','engine gear losses','coupling device losses','coupling device 1 losses','coupling device 2 losses','gear box losses','clutch losses'};
for ii=1:length(comp)
    if bp.(comp{ii})~=0
        bp.bilanE=[bp.bilanE bp.(comp{ii})*1000/dist];
        bp.nombilanE=[bp.nombilanE comp{ii}];
        ind=[ind ii];
    end
end
bp.comp=comp;
bp.legend_name=legend_name;
bp.ind=ind;
    

if param.verbose>=3    
    figure(num_fig)
    clf
    axes('fontsize',12)
    hold on   
    bar([1 2],[bp.bilanE' 0*bp.bilanE']',0.3,'stacked')
    if sum(bp.bilanE)>0
        axis([0.5 1.5 0 1.1*sum(bp.bilanE)])
    else
        axis([0.5 1.5 1.1*min(bp.bilanE) 1.1*sum(bp.bilanE(bp.bilanE>0))])
    end
    legend(legend_name(ind))
    ylabel('Wh/km','Fontsize',12)
    title('energy balance','Fontsize',12)
    
%     figure(num_fig+1)
%     clf 
%     pie3(bp.bilanE,[0 ones(1,length(bp.bilanE)-1)])
%     legend(legend_name(ind),'location',[0.15 0.7 0.1 0.1])
% %     view(90,30)
%     colormap hsv  
end

if (VD.VEHI.ntypveh==1 || VD.VEHI.ntypveh==3)  % param vehicule en SCx plus frottement on s�pare energie aux roue aero, roulement
    bp.bilanE=[];
    bp.nombilanE=[];
    ind=[];
    dist=trapz(VD.CYCL.temps,VD.CYCL.vitesse);
    comp={'Eroue_roul','Eroue_aero','Eroue_pente','Eacc','Epac','Edriv','Efrein','Eacm1','Eacm2','Epbat','Eepi','Ered','Ered2','Eadcpl','Eadcpl1','Eadcpl2','Ebv','Eemb1'};
    legend_name={'wheel energy rolling','wheel energy aero','wheel energy slope','auxiliary energy','PAC energy','driver energy','brake losses','em1 losses','em2 losses','battery losses',...
        'planetary gear lossses','final gear losses','engine gear losses','coupling device losses','coupling device 1 losses','coupling device 2 losses','gear box losses','clutch losses'};
    for ii=1:length(comp)
        if bp.(comp{ii})~=0
            bp.bilanE=[bp.bilanE bp.(comp{ii})*1000/dist];
            bp.nombilanE=[bp.nombilanE comp{ii}];
            ind=[ind ii];
        end
    end
    bp.comp=comp;
    bp.legend_name=legend_name;
    bp.ind=ind;
end

if param.verbose>=3     
    figure(num_fig+2)
    clf
    axes('fontsize',12)
    hold on   
    bar([1 2],[bp.bilanE' 0*bp.bilanE']',0.3,'stacked')
    if sum(bp.bilanE)>0
        axis([0.5 1.5 0 1.1*sum(bp.bilanE)])
    else
        axis([0.5 1.5 1.1*min(bp.bilanE) 1.1*sum(bp.bilanE(bp.bilanE>0))])
    end
    legend(legend_name(ind))
    ylabel('Wh/km','Fontsize',12)
    title('energy balance','Fontsize',12)
    
    figure(num_fig+3)
    clf 
    pie3(bp.bilanE,[0 ones(1,length(bp.bilanE)-1)])
    legend(legend_name(ind),'location',[0.15 0.7 0.1 0.1])
    view(90,30)
    colormap hsv  
end
warning on;
end

