% Function: dep_mcc
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [pmec, pind, pcind, pex, pcex] = dep_mcc(VD,temps, vit, pas, wmg, cmg, img, uind, uex, iex, ...
%                      ubat,ibat, ihind, ihex, fid)
%                   
% Depouillement des simulation Machinecourant continu et convertisseur
% ------------------------------------------------------
 function [pmec, pind, pcind, pex, pcex] = dep_mcc(VD,temps, vit, pas, wmg, cmg, img, uind, uex, iex, ...
                      ubat,ibat, ihind, ihex, fid)
                  
pmec=wmg.*cmg;
pind=img.*uind;
pcind=ihind.*ubat;
pex=iex.*uex;
pcex=ihex.*ubat;

% Valeurs moyennes
wmg_moy_T=0;
wmg_moy_R=0;
cmg_moy_T=0;
cmg_moy_R=0;
pmec_moy_T=0;
pmec_moy_R=0;
pind_moy_T=0;
pind_moy_R=0;
pex_moy_T=0;
pex_moy_R=0;
pcind_moy_T=0;
pcind_moy_R=0;
img_moy_T=0;
img_moy_R=0;
uind_moy_T=0;
uind_moy_R=0;

t_T=0;
t_R=0;

tcind_T=0;
tcind_R=0;

% Mini - Maxi
cmg_min=min(cmg);

pmec_min=min(pmec);
pind_min=min(pind);
pcind_min=min(pcind);
img_min=min(img);

cmg_max=max(cmg);
pmec_max=max(pmec);
pind_max=max(pind);
pcind_max=max(pcind);
img_max=max(img);

for i=1:length(temps)
   rendmcc_ind(i)=0;
   rendmcc(i)=0;
   rendcmg(i)=0;
   
 if(vit(i)>=0.01)
   if (pmec(i)> 0 & pind(i)> 0)
      wmg_moy_T=wmg_moy_T+wmg(i)*pas(i);
      cmg_moy_T=cmg_moy_T+cmg(i)*pas(i);
      pmec_moy_T=pmec_moy_T+pmec(i).*pas(i);
      pind_moy_T=pind_moy_T+pind(i).*pas(i);
      rendmcc_ind(i)=pmec(i)./pind(i);
      pex_moy_T=pex_moy_T+pex(i).*pas(i);
      rendmcc(i)=pmec(i)./(pind(i)+pex(i));
      img_moy_T=img_moy_T+img(i).*pas(i);
      uind_moy_T=uind_moy_T+uind(i).*pas(i);
      t_T=t_T+pas(i);
   end
   if (pmec(i)< 0 & pind(i)< 0)
      wmg_moy_R=wmg_moy_R+wmg(i)*pas(i);
      cmg_moy_R=cmg_moy_R+cmg(i)*pas(i);
      pmec_moy_R=pmec_moy_R+pmec(i).*pas(i);
      pind_moy_R=pind_moy_R+pind(i).*pas(i);
      rendmcc_ind(i)=pmec(i)./pind(i);
      pex_moy_R=pex_moy_R+pex(i).*pas(i);
      rendmcc(i)=(pind(i)+pex(i))./pmec(i);  
      img_moy_R=img_moy_R+img(i).*pas(i);
      uind_moy_R=uind_moy_R+uind(i).*pas(i);
      t_R=t_R+pas(i);
   end
%   if(pcind(i)>0 & (pind(i)+pex(i))>0)
   if (pmec(i)> 0 & pind(i)> 0)

      tcind_T=tcind_T+pas(i);
      pcind_moy_T=pcind_moy_T+pcind(i).*pas(i);      
      rendcmg(i)=(pind(i)+pex(i))./pcind(i);         
%   elseif(pcind(i)<0 & (pind(i)+pex(i))<0)
   elseif (pmec(i)< 0 & pind(i)< 0)
      tcind_R=tcind_R+pas(i);
      pcind_moy_R=pcind_moy_R+pcind(i).*pas(i);      
      rendcmg(i)=pcind(i)./(pind(i)+pex(i));    
   end
else
 %  pex(i)=0;
 %  iex(i)=0;
 %  ibat(i)=ibat(i)-ihex(i);
 %  ihex(i)=0;
end
end

if(t_T~=0)
   wmg_moy_T=wmg_moy_T/t_T;
   cmg_moy_T=cmg_moy_T/t_T;
   pmec_moy_T=pmec_moy_T/t_T;
   pind_moy_T=pind_moy_T/t_T;
   pex_moy_T=pex_moy_T/t_T;
   img_moy_T=img_moy_T/t_T;
   uind_moy_T=uind_moy_T/t_T;
else
   wmg_moy_T=0;
   cmg_moy_T=0;
   pmec_moy_T=0;
   pind_moy_T=0;
   pex_moy_T=0;
   img_moy_T=0;
   uind_moy_T=0;
end

if(tcind_T~=0)
   pcind_moy_T=pcind_moy_T/tcind_T;
else
   pcind_moy_T=0;
end

if(t_R~=0)
   wmg_moy_R=wmg_moy_R/t_R;
   cmg_moy_R=cmg_moy_R/t_R;
   pmec_moy_R=pmec_moy_R/t_R;
   pind_moy_R=pind_moy_R/t_R;
   pex_moy_R=pex_moy_R/t_R;
   img_moy_R=img_moy_R/t_R;
   uind_moy_R=uind_moy_R/t_R;
else
   wmg_moy_R=0;;
   cmg_moy_R=0;
   pmec_moy_R=0;
   pind_moy_R=0;
   pex_moy_R=0;
   img_moy_R=0;
   uind_moy_R=0;
end

if(tcind_R~=0)
   pcind_moy_R=pcind_moy_R/tcind_R;
else
   pcind_moy_R=0;
end

if(pind_moy_T~=0)
   rdind_moy_T=pmec_moy_T/pind_moy_T;
else
   rdind_moy_T=0;
end

if((pind_moy_T+pex_moy_T)~=0)
   rdmcc_moy_T=pmec_moy_T/(pind_moy_T+pex_moy_T);
else
   rdmcc_moy_T=0;
end

if(pcind_moy_T~=0)
   rdcind_moy_T=pind_moy_T/pcind_moy_T;
else
   rdcind_moy_T=0;
end

if(pmec_moy_R~=0)
   rdind_moy_R=pind_moy_R/pmec_moy_R;
   rdmcc_moy_R=(pind_moy_R+pex_moy_R)/pmec_moy_R;
else
   rdind_moy_R=0;
   rdmcc_moy_R=0;
end

if(pind_moy_R~=0)
   rdcind_moy_R=pcind_moy_R/pind_moy_R;
else
   rdcind_moy_R=0;
end

fprintf(fid,'\n\t\t\t%s\n\n','*** CARACTERISTIQUES MOTEUR MCC ***');

fprintf(fid,'\t\t\t\t%s\t\t%s\n\n','Phase motrice','Phase recuperatrice');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Temps (pm et pe >< 0): ',t_T,'s',t_R,'s');
fprintf(fid,'%s\t\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Regime moyen : ', ...
   wmg_moy_T*30/pi,'tr/mn',wmg_moy_R*30/pi,'tr/mn');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Couple maximum : ', ...
   cmg_max,'Nm',cmg_min,'Nm');
fprintf(fid,'%s\t\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Couple moyen : ', ...
   cmg_moy_T,'Nm',cmg_moy_R,'Nm');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance mecanique maximum : ', ...
   pmec_max./1000,'kW',pmec_min./1000,'kW');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance mecanique moyenne :', ...
   pmec_moy_T./1000,'kW',pmec_moy_R./1000,'kW');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance induit maximum :', ...
   pind_max./1000,'kW',pind_min./1000,'kW');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance induit moyenne :', ...
   pind_moy_T./1000,'kW',pind_moy_R./1000,'kW');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Courant induit maximum :', ...
   img_max,'A',img_min,'A');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Courant induit moyen :', ...
   img_moy_T,'A',img_moy_R,'A');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Tension induit moyenne :', ...
   uind_moy_T,'V',uind_moy_R,'V');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance d''excit moy :', ...
   pex_moy_T./1000,'kW',pex_moy_R./1000,'kW');

fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Rendement moyen sans excit :', ...
   100.*rdind_moy_T,'%',100.*rdind_moy_R,'%');
fprintf(fid,'%s\t%6.2f\t%s\t\t%6.2f\t%s\n','Rendement moyen avec excit :', ...
   100.*rdmcc_moy_T,'%',100.*rdmcc_moy_R,'%');

fprintf(fid,'\n\t\t\t%s\n\n','*** CARACTERISTIQUES CONTROLE MCC ***');

fprintf(fid,'\t\t\t\t%s\t\t%s\n\n','Phase motrice','Phase recuperatrice');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance maximum :', ...
   pcind_max./1000,'kW',pcind_min./1000,'kW');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Puissance moyenne :', ...
   pcind_moy_T./1000,'kW',pcind_moy_R./1000,'kW');
fprintf(fid,'%s\t\t%6.2f\t%s\t\t%6.2f\t%s\n','Rendement moyen :', ...
   100.*rdcind_moy_T,'%',100.*rdcind_moy_R,'%');
