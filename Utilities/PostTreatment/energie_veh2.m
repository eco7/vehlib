% Function: energie_veh2
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_veh2(VD,temps, pas, vit, vitdem, dist, fpente, force1, force2, force3, cfrein_meca, cinertie, wroue, pveh, elehyb, vimin, fid, fid3)
%
% Depouillement des energies du vehicule
% Pour un modele vehicule de type 2 (VD.VEHI.ntypveh=2)
% cad F=a+b*v+c*v2.
% ------------------------------------------------------
function [] = energie_veh2(VD,temps, pas, vit, vitdem, dist, fpente, force1, force2, force3, cfrein_meca, cinertie, wroue, pveh, elehyb, vimin, fid, fid3)

% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>=0';
FPENTE_M=sum(fpente(eval(condition)).*vit(eval(condition)).*pas(eval(condition))/3600);
FRES_M=sum((force1(eval(condition))+force2(eval(condition))+force3(eval(condition))).*vit(eval(condition)).*pas(eval(condition))/3600);
FFREIN_M=sum(cfrein_meca(eval(condition)).*wroue(eval(condition)).*pas(eval(condition))/3600);
FINERTIE_M=sum(cinertie(eval(condition)).*wroue(eval(condition)).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb==0 & vitdem>vimin & pveh<0';
FPENTE_R=sum(fpente(eval(condition)).*vit(eval(condition)).*pas(eval(condition))/3600);
FRES_R=sum((force1(eval(condition))+force2(eval(condition))+force3(eval(condition))).*vit(eval(condition)).*pas(eval(condition))/3600);
FFREIN_R=sum(cfrein_meca(eval(condition)).*wroue(eval(condition)).*pas(eval(condition))/3600);
FINERTIE_R=sum(cinertie(eval(condition)).*wroue(eval(condition)).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb==0 & vitdem<vimin';
FPENTE_A=sum(fpente(eval(condition)).*vit(eval(condition)).*pas(eval(condition))/3600);
FRES_A=sum((force1(eval(condition))+force2(eval(condition))+force3(eval(condition))).*vit(eval(condition)).*pas(eval(condition))/3600);
FFREIN_A=sum(cfrein_meca(eval(condition)).*wroue(eval(condition)).*pas(eval(condition))/3600);
FINERTIE_A=sum(cinertie(eval(condition)).*wroue(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>=0';
FPENTE_MH=sum(fpente(eval(condition)).*vit(eval(condition)).*pas(eval(condition))/3600);
FRES_MH=sum((force1(eval(condition))+force2(eval(condition))+force3(eval(condition))).*vit(eval(condition)).*pas(eval(condition))/3600);
FFREIN_MH=sum(cfrein_meca(eval(condition)).*wroue(eval(condition)).*pas(eval(condition))/3600);
FINERTIE_MH=sum(cinertie(eval(condition)).*wroue(eval(condition)).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb~=0 & vitdem>vimin & pveh<0';
FPENTE_RH=sum(fpente(eval(condition)).*vit(eval(condition)).*pas(eval(condition))/3600);
FRES_RH=sum((force1(eval(condition))+force2(eval(condition))+force3(eval(condition))).*vit(eval(condition)).*pas(eval(condition))/3600);
FFREIN_RH=sum(cfrein_meca(eval(condition)).*wroue(eval(condition)).*pas(eval(condition))/3600);
FINERTIE_RH=sum(cinertie(eval(condition)).*wroue(eval(condition)).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb~=0 & vitdem<vimin';
FPENTE_AH=sum(fpente(eval(condition)).*vit(eval(condition)).*pas(eval(condition))/3600);
FRES_AH=sum((force1(eval(condition))+force2(eval(condition))+force3(eval(condition))).*vit(eval(condition)).*pas(eval(condition))/3600);
FFREIN_AH=sum(cfrein_meca(eval(condition)).*wroue(eval(condition)).*pas(eval(condition))/3600);
FINERTIE_AH=sum(cinertie(eval(condition)).*wroue(eval(condition)).*pas(eval(condition))/3600);

% En Wh
if(max(elehyb)==0)
 ecr_energie('sum',fid, 'Forces de resistance    : ', [FRES_M FRES_R]);
 ecr_energie('sum',fid, 'Forces de pente         : ', [FPENTE_M FPENTE_R]);
 ecr_energie('sum',fid, 'Forces d''inertie        : ', [FINERTIE_M FINERTIE_R]);
 ecr_energie('separateur',fid, '', zeros(3,1));
 ecr_energie('nothing',fid, 'Energie vehicule        : ',([FRES_M FRES_R]+ ...
                                                          [FPENTE_M FPENTE_R]+ ...
                                                          [FINERTIE_M FINERTIE_R]));
 ecr_energie('sum',fid, 'Forces de freinage      : ', [FFREIN_M FFREIN_R]);
else
 ecr_energie('sum',fid, 'Forces de resistance    : ', [FRES_M FRES_MH FRES_R FRES_RH]);
 ecr_energie('sum',fid, 'Forces de pente         : ', [FPENTE_M FPENTE_MH FPENTE_R FPENTE_RH]);
 ecr_energie('sum',fid, 'Forces d''inertie        : ', [FINERTIE_M FINERTIE_MH FINERTIE_R FINERTIE_RH]);
 ecr_energie('separateur',fid, '', zeros(6,1));
 ecr_energie('nothing',fid, 'Energie vehicule        : ',([FRES_M FRES_MH FRES_R FRES_RH]+ ...
                                                          [FPENTE_M FPENTE_MH FPENTE_R FPENTE_RH]+ ...
                                                          [FINERTIE_M FINERTIE_MH FINERTIE_R FINERTIE_RH]));
 ecr_energie('sum',fid, 'Forces de freinage      : ', [FFREIN_M FFREIN_MH FFREIN_R FFREIN_RH]);
end

% En Wh/km
if(max(elehyb)==0)
 ecr_energie('sum',fid3, 'Forces de resistance    : ', [FRES_M FRES_R]/(dist/1000));
 ecr_energie('sum',fid3, 'Forces de pente         : ', [FPENTE_M FPENTE_R]/(dist/1000));
 ecr_energie('sum',fid3, 'Forces d''inertie        : ', [FINERTIE_M FINERTIE_R]/(dist/1000));
 ecr_energie('separateur',fid3, '', zeros(3,1));
 ecr_energie('nothing',fid3, 'Energie vehicule        : ',([FRES_M FRES_R]+ ...
                                                           [FPENTE_M FPENTE_R]+ ...
                                                           [FINERTIE_M FINERTIE_R])/(dist/1000));
 ecr_energie('sum',fid3, 'Forces de freinage      : ', [FFREIN_M FFREIN_R]/(dist/1000));
else
 ecr_energie('sum',fid3, 'Forces de resistance    : ', [FRES_M FRES_MH FRES_R FRES_RH]/(dist/1000));
 ecr_energie('sum',fid3, 'Forces de pente         : ', [FPENTE_M FPENTE_MH FPENTE_R FPENTE_RH]/(dist/1000));
 ecr_energie('sum',fid3, 'Forces d''inertie        : ', [FINERTIE_M FINERTIE_MH FINERTIE_R FINERTIE_RH]/(dist/1000));
 ecr_energie('separateur',fid3, '', zeros(6,1));
  ecr_energie('nothing',fid3, 'Energie vehicule        : ',([FRES_M FRES_MH FRES_R FRES_RH]+ ...
                                                            [FPENTE_M FPENTE_MH FPENTE_R FPENTE_RH]+ ...
                                                            [FINERTIE_M FINERTIE_MH FINERTIE_R FINERTIE_RH])/(dist/1000));
  ecr_energie('sum',fid3, 'Forces de freinage      : ', [FFREIN_M FFREIN_MH FFREIN_R FFREIN_RH]/(dist/1000));
end

% Ecriture de la structure de flux si existante
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='';
flux(j).type='Traction';
flux(j).valeur=NaN;
flux(j).position=[0.85 0.14];
flux(j).couleur='black';
j=j+1;
flux(j).nom='FRES_M';
flux(j).type='Resist avanct';
flux(j).valeur=FRES_M+FRES_MH;
flux(j).position=[0.85 0.17];
flux(j).couleur='black';
j=j+1;
flux(j).nom='FPENTE_M';
flux(j).type='Pente';
flux(j).valeur=FPENTE_M+FPENTE_MH;
flux(j).position=[0.85 0.23];
flux(j).couleur='black';
j=j+1;
flux(j).nom='FINERTIE_M';
flux(j).type='Inertie';
flux(j).valeur=FINERTIE_M+FINERTIE_MH;
flux(j).position=[0.85 0.26];
flux(j).couleur='black';

j=j+1;
flux(j).nom='FVEH_M';
flux(j).type='';
flux(j).valeur=FRES_M+FRES_MH+FPENTE_M+FPENTE_MH+FINERTIE_M+FINERTIE_MH;
flux(j).position=[0.75 0.34];
flux(j).couleur='black';

j=j+1;
flux(j).nom='';
flux(j).type='Freinage';
flux(j).valeur=NaN;
flux(j).position=[0.85 0.43];
flux(j).couleur='black';
j=j+1;
flux(j).nom='FRES_R';
flux(j).type='Roult';
flux(j).valeur=FRES_R+FRES_RH;
flux(j).position=[0.85 0.46];
flux(j).couleur='black';
j=j+1;
flux(j).nom='FPENTE_R';
flux(j).type='Pente';
flux(j).valeur=FPENTE_R+FPENTE_RH;
flux(j).position=[0.85 0.52];
flux(j).couleur='black';
j=j+1;
flux(j).nom='FINERTIE_R';
flux(j).type='Inertie';
flux(j).valeur=FINERTIE_R+FINERTIE_RH;
flux(j).position=[0.85 0.55];
flux(j).couleur='green';
j=j+1;
flux(j).nom='FFREIN_R';
flux(j).type='Frein';
flux(j).valeur=FFREIN_R+FFREIN_RH;
flux(j).position=[0.85 0.58];
flux(j).couleur='red';

j=j+1;
flux(j).nom='FVEH_R';
flux(j).type='';
flux(j).valeur=FRES_R+FRES_RH+FPENTE_R+FPENTE_RH+FINERTIE_R+FINERTIE_RH+FFREIN_R+FFREIN_RH;
flux(j).position=[0.75 0.4];
flux(j).couleur='green';

assignin('base','flux',flux);

