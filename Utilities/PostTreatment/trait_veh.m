% File: trait_veh
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Script de post traitement de la caisse du vehicule seule dans VEHLIB
%
% ------------------------------------------------------

vimin=0.01;

%lecture et increment du numero de calcul
fnum=strcat(VD.INIT.InitialFolder,VD.INIT.sep,'numcal.mat');
load(fnum);

% ecriture du fichier de resultats synthetiques
fid=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF1'),'w');
fid2=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF2'),'w');
fid3=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF3'),'w');

fprintf(fid,'\n\t%s\n',['POST TRAITEMENT pour modele VEHLIB- VERSION ',num2str(VD.INIT.version)]);
fprintf(fid,'\n\t%s\n','-----------------------------------------------------');
fprintf(fid,'\n%s %s','Date de traitement du fichier          :  ',date);
fprintf(fid,'\n%s %4d\n\n','Numero de calcul                       :  ',numcal);
fprintf(fid2,'\n\t%s\n',['POST TRAITEMENT pour modele VEHLIB- VERSION ',num2str(VD.INIT.version)]);
fprintf(fid2,'\n\t%s\n','-----------------------------------------------------');
fprintf(fid2,'\n%s %s','Date de traitement du fichier         :  ',date);
fprintf(fid2,'\n%s %4d\n\n','Numero de calcul                      :  ',numcal);
fprintf(fid3,'\n\t%s\n',['POST TRAITEMENT pour modele VEHLIB- VERSION ',num2str(VD.INIT.version)]);
fprintf(fid3,'\n\t%s\n','-----------------------------------------------------');
fprintf(fid3,'\n%s %s','Date de traitement du fichier         :  ',date);
fprintf(fid3,'\n%s %4d\n\n','Numero de calcul                      :  ',numcal);

fprintf(fid,'%s%s\n','Nom du fichier modele                  :   ',vehlib.nom);
fprintf(fid,'%s%s\n','Nom du modele simulink                 :   ',vehlib.simulink);
fprintf(fid,'%s%s\n','Nom du fichier cinematique             :   ',vehlib.CYCL);
fprintf(fid,'%s%s\n','Nom du fichier vehicule                :   ',vehlib.VEHI);


fprintf(1,'\n\t%s\n',['POST TRAITEMENT pour modele VEHLIB- VERSION ',num2str(VD.INIT.version)]);
fprintf(1,'\n\t%s\n','---------------------------------------------------');

%%% VALEURS SYNTHETIQUES %%%%%
% Ecriture du bilan de masse
% Ecriture du bilan de masse
dep_masse(VD, masse, fid);
% mcharge=evalin('base',['VD.VEHI.Charge']);
% mveh=evalin('base',['VD.VEHI.Mveh']);
% mtot=mveh+mcharge;
% mass ='        Totale   Charge   Vehi.';
% mass1=['        ',num2str(mtot),'       ',num2str(mcharge),'      ',num2str(mveh)];
% fprintf(fid,'\n%s\n','        Bilan de masse statique vehicule:');
% fprintf(fid,'%s\n',mass);
% fprintf(fid,'%s\n\n',mass1);
% 
[pas]=dep_cin(VD,tsim, distance, vit, vit_dem, acc, ones(length(vit),1), vimin, fid);

if(VD.VEHI.ntypveh==1)
   [pveh]=dep_veh1(VD,tsim, pas, vit, acc, froul, faero, fpente, cfrein_meca, cinertie, vimin, fid);
elseif(VD.VEHI.ntypveh==2)
   [pveh]=dep_veh2(VD,tsim, pas, vit, acc, force1, force2, force3, fpente, cinertie, vimin, fid);
end

% Repartition des puissances vehicules par classes 
fprintf(fid,'\n\n');
titre={'        *** Classes suivant puissance vehicule en kW ***';'';'Phases de traction';'UP: Classes exprimees en energie cumulee en Wh'};
%classe_energie(VD,tsim/3.6,pveh/1000,3,0,50,20,1,1,fid,titre);
classe_energie(VD,tsim/3.6,pveh/1000,3,0,100,40,1,1,fid,titre);

fprintf(fid,'\n\n');
titre={'Phases de freinage';'UP: Classes exprimees en energie cumulee en Wh'};
%classe_energie(VD,tsim/3.6,pveh/1000,3,0,50,20,-1,1,fid,titre);
classe_energie(VD,tsim/3.6,pveh/1000,3,0,100,40,-1,1,fid,titre);
fprintf(fid,'\n');

%%% ENERGIES  %%%%

% script d initialisation des bilans sources/consommateurs
ecr_energie('initwh',fid2,'',[1 2 3 4 5 6]);
ecr_energie('initwhkm',fid3,'',[1 2 3 4 5 6]);

  
% initialisation de la structure pour renseigner les flux d energie
flux=struct([]);

if(VD.VEHI.ntypveh==1)
   energie_veh1(VD,tsim, pas, vit, vit_dem, distance(length(distance)), fpente, faero, froul, cfrein_meca, cinertie, wroue, pveh, ones(length(vit),1), vimin, fid2, fid3);
elseif(VD.VEHI.ntypveh==2)
   energie_veh2(VD,tsim, pas, vit, vit_dem, distance(length(distance)), fpente, force1, force2, force3, cfrein_meca, cinertie, wroue, pveh, ones(length(vit),1), vimin, fid2, fid3);
end

% script de fin des bilans sources/consommateurs
ecr_energie('end',fid2,'',[1 2 3 4 5 6]);
ecr_energie('end',fid3,'',[1 2 3 4 5 6]);

% fermeture
fclose(fid);
fclose(fid2);
fclose(fid3);
if isunix
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF1')]);
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF2')]);
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF3')]);
end
