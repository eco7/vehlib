% Function: energie_MCT
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_MCT(VD,temps, pas, vitdem, dist, pmec, pcmg, pveh, elehyb, vimin, nummg, fid, fid3)
%
% Depouillement des energies consommees les moteurs et controles cartographies
% ------------------------------------------------------
function [] = energie_MCT(VD,temps, pas, vitdem, dist, pmec, pcmg, pveh, elehyb, vimin, nummg, fid, fid3)


% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>=0';
FMGMECA_M=sum(pmec(eval(condition)).*pas(eval(condition))/3600);
QMGT_M=sum((pcmg(eval(condition))-pmec(eval(condition))).*pas(eval(condition))/3600);
FCMG_M=sum(pcmg(eval(condition)).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb==0 & vitdem>vimin & pveh<0';
FMGMECA_R=sum(pmec(eval(condition)).*pas(eval(condition))/3600);
QMGT_R=sum((pcmg(eval(condition))-pmec(eval(condition))).*pas(eval(condition))/3600);
FCMG_R=sum(pcmg(eval(condition)).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb==0 & vitdem<vimin';
FMGMECA_A=sum(pmec(eval(condition)).*pas(eval(condition))/3600);
QMGT_A=sum((pcmg(eval(condition))-pmec(eval(condition))).*pas(eval(condition))/3600);
FCMG_A=sum(pcmg(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>=0';
FMGMECA_MH=sum(pmec(eval(condition)).*pas(eval(condition))/3600);
QMGT_MH=sum((pcmg(eval(condition))-pmec(eval(condition))).*pas(eval(condition))/3600);
FCMG_MH=sum(pcmg(eval(condition)).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb~=0 & vitdem>vimin & pveh<0';
FMGMECA_RH=sum(pmec(eval(condition)).*pas(eval(condition))/3600);
QMGT_RH=sum((pcmg(eval(condition))-pmec(eval(condition))).*pas(eval(condition))/3600);
FCMG_RH=sum(pcmg(eval(condition)).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb~=0 & vitdem<vimin';
FMGMECA_AH=sum(pmec(eval(condition)).*pas(eval(condition))/3600);
QMGT_AH=sum((pcmg(eval(condition))-pmec(eval(condition))).*pas(eval(condition))/3600);
FCMG_AH=sum(pcmg(eval(condition)).*pas(eval(condition))/3600);

% en Wh.
if(max(elehyb)==0)
 ecr_energie('substract',fid, 'Energie meca Mot. elec. : ', VD.VEHI.nbacm1*[FMGMECA_M FMGMECA_R]);
 ecr_energie('nothing',fid, 'Pertes Mot + Cont       : ', VD.VEHI.nbacm1*[QMGT_M QMGT_R]);
fprintf(fid,'%s%s\n',...
  '                           -----------','            -----------');
 ecr_energie('sum',fid, 'Energie controle        : ', VD.VEHI.nbacm1*[FCMG_M FCMG_R FCMG_A]);
else
 if(nummg==1)
  ecr_energie('sum',fid, 'Pertes Mot1 + Cont1     : ', VD.VEHI.nbacm1*[QMGT_M QMGT_MH QMGT_R QMGT_RH]);
 elseif(nummg==2)
  ecr_energie('sum',fid, 'Pertes Mot2 + Cont2     : ', [QMGT_M QMGT_MH QMGT_R QMGT_RH]);
 end
end

% en Wh/km.
if(max(elehyb)==0)
 ecr_energie('substract',fid3, 'Energie meca Mot. elec. : ', VD.VEHI.nbacm1*[FMGMECA_M FMGMECA_R]/(dist/1000));
 ecr_energie('nothing',fid3, 'Pertes Mot + Cont       : ', VD.VEHI.nbacm1*[QMGT_M QMGT_R]/(dist/1000));
fprintf(fid3,'%s%s\n',...
  '                           -----------','            -----------');
 ecr_energie('sum',fid3, 'Energie controle        : ', VD.VEHI.nbacm1*[FCMG_M FCMG_R FCMG_A]/(dist/1000));
else
 if(nummg==1)
  ecr_energie('sum',fid3, 'Pertes Mot1 + Cont1     : ', VD.VEHI.nbacm1*[QMGT_M QMGT_MH QMGT_R QMGT_RH]/(dist/1000));
 elseif(nummg==2)
  ecr_energie('sum',fid3, 'Pertes Mot2 + Cont2     : ', [QMGT_M QMGT_MH QMGT_R QMGT_RH]/(dist/1000));
 end
end

% Ecriture de la structure de flux
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='FMG1_MA';
flux(j).type='';
flux(j).valeur=FMGMECA_M+FMGMECA_MH+FMGMECA_A+FMGMECA_AH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='FMG1_R';
flux(j).type='';
flux(j).valeur=FMGMECA_R+FMGMECA_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='green';

j=j+1;
flux(j).nom='FCMG1_MA';
flux(j).type='';
flux(j).valeur=FCMG_M+FCMG_MH+FCMG_A+FCMG_AH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='FCMG1_R';
flux(j).type='';
flux(j).valeur=FCMG_R+FCMG_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='green';

assignin('base','flux',flux);
