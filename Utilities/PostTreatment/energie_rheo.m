% Function: energie_rheo
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_rheo(VD,temps, pas, vitdem, dist, pveh, prheo, elehyb, vimin, fid, fid3)
%
% Depouillement des energies disspees par le RHEOSTAT
% ------------------------------------------------------
function [] = energie_rheo(VD,temps, pas, vitdem, dist, pveh, prheo, elehyb, vimin, fid, fid3)


% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>=0';
FRHEO_M=sum(prheo(eval(condition)).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb==0 & vitdem>vimin & pveh<0';
FRHEO_R=sum(prheo(eval(condition)).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb==0 & vitdem<vimin';
FRHEO_A=sum(prheo(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>=0';
FRHEO_MH=sum(prheo(eval(condition)).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb~=0 & vitdem>vimin & pveh<0';
FRHEO_RH=sum(prheo(eval(condition)).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb~=0 & vitdem<vimin';
FRHEO_AH=sum(prheo(eval(condition)).*pas(eval(condition))/3600);

% en Wh.
if(max(elehyb)==0)
 ecr_energie('sum',fid, 'Energie rheostat        : ', [FRHEO_M FRHEO_R FRHEO_A]);
else
 ecr_energie('sum',fid, 'Energie rheostat        : ', [FRHEO_M FRHEO_MH FRHEO_R FRHEO_RH FRHEO_A FRHEO_AH]);
end   

% en Wh/km.
if(max(elehyb)==0)
 ecr_energie('sum',fid3, 'Energie rheostat        : ', [FRHEO_M FRHEO_R FRHEO_A]/(dist/1000));
else
 ecr_energie('sum',fid3, 'Energie rheostat        : ', [FRHEO_M FRHEO_MH FRHEO_R FRHEO_RH FRHEO_A FRHEO_AH]/(dist/1000));
end

% Ecriture de la structure de flux
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='FRHEO_MA';
flux(j).type='';
flux(j).valeur=FRHEO_M+FRHEO_MH+FRHEO_A+FRHEO_AH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='FRHEO_R';
flux(j).type='';
flux(j).valeur=FRHEO_R+FRHEO_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='green';

assignin('base','flux',flux);
