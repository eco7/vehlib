% Function: energie_surv
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_surv(VD,temps, pas, vitdem, dist, pveh, qsurv, elehyb, vimin, fid, fid3)
%
% Depouillement des energies du survolteur
% ------------------------------------------------------
function [] = energie_surv(VD,temps, pas, vitdem, dist, pveh, qsurv, elehyb, vimin, fid, fid3)


% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>= 0';
QVD.SURV_M=sum(qsurv(eval(condition)).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb==0 & vitdem>vimin & pveh< 0';
QVD.SURV_R=sum(qsurv(eval(condition)).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb==0 & vitdem<vimin';
QVD.SURV_A=sum(qsurv(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>= 0';
QVD.SURV_MH=sum(qsurv(eval(condition)).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb~=0 & vitdem>vimin & pveh< 0';
QVD.SURV_RH=sum(qsurv(eval(condition)).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb~=0 & vitdem<vimin';
QVD.SURV_AH=sum(qsurv(eval(condition)).*pas(eval(condition))/3600);

% EN Wh
if(max(elehyb)==0)
 ecr_energie('sum',fid, 'Pertes survolteur       : ', [QVD.SURV_M QVD.SURV_R QVD.SURV_A]);
else
 ecr_energie('sum',fid, 'Pertes survolteur       : ', [QVD.SURV_M QVD.SURV_MH QVD.SURV_R QVD.SURV_RH QVD.SURV_A QVD.SURV_AH]);
end

% EN Wh% EN Wh/km
if(max(elehyb)==0)
 ecr_energie('sum',fid3, 'Pertes survolteur       : ', [QVD.SURV_M QVD.SURV_R QVD.SURV_A]/(dist/1000));
else
 ecr_energie('sum',fid3, 'Pertes survolteur       : ', [QVD.SURV_M QVD.SURV_MH QVD.SURV_R QVD.SURV_RH QVD.SURV_A QVD.SURV_AH]/(dist/1000));
end

% Ecriture de la structure de flux
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='QVD.SURV_MA';
flux(j).type='';
flux(j).valeur=QVD.SURV_M+QVD.SURV_MH+QVD.SURV_A+QVD.SURV_AH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='QVD.SURV_R';
flux(j).type='';
flux(j).valeur=QVD.SURV_R+QVD.SURV_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

assignin('base','flux',flux);

