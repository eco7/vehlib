% Function: dep_mct
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% function [pmec, pcmg] = dep_mct(VD,temps, vit, pas, wmg, cmg, icmg, uacm, chaine, vimin, fid)
%
% Depouillement machines elctriques cartographiees
% ------------------------------------------------------
function [pmec, pcmg] = dep_mct(VD,temps, vit, pas, wmg, cmg, icmg, uacm, chaine, vimin, fid)

pmec=wmg.*cmg;
pcmg=icmg.*uacm;


% Mini - Maxi
cmg_min=min(cmg);
cmg_max=max(cmg);
pm_min=min(pmec);
pe_min=min(pcmg);
pm_max=max(pmec);
pe_max=max(pcmg);


% Phases de traction
condition='vit>vimin & pmec>0 & pcmg>0';
tmot=sum(pas(eval(condition)));
if(tmot~=0)
   wmg_moymot=sum(wmg(eval(condition)).*pas(eval(condition)))/tmot;
   cmg_moymot=sum(cmg(eval(condition)).*pas(eval(condition)))/tmot;
   pm_moymot=sum(pmec(eval(condition)).*pas(eval(condition)))/tmot;
   pe_moymot=sum(pcmg(eval(condition)).*pas(eval(condition)))/tmot;
else
   wmg_moymot=0;
   cmg_moymot=0;
   pm_moymot=0;
   pe_moymot=0;
end

if(pe_moymot~=0)
   rdmoymot=pm_moymot/pe_moymot;
else
   rdmoymot=0;
end

% Phases de recup
condition='vit>vimin & pmec<0 & pcmg<0';
trec=sum(pas(eval(condition)));
if(trec~=0)
   wmg_moyrec=sum(wmg(eval(condition)).*pas(eval(condition)))/trec;
   cmg_moyrec=sum(cmg(eval(condition)).*pas(eval(condition)))/trec;
   pm_moyrec=sum(pmec(eval(condition)).*pas(eval(condition)))/trec;
   pe_moyrec=sum(pcmg(eval(condition)).*pas(eval(condition)))/trec;
else
   wmg_moyrec=0;
   cmg_moyrec=0;
   pm_moyrec=0;
   pe_moyrec=0;
end

if(pm_moyrec~=0)
   rdmoyrec=pe_moyrec/pm_moyrec;
else
   rdmoyrec=0;
end

fprintf(fid,'\n%s\n\n',chaine);

fprintf(fid,'%s%s\n\n','           Phase motrice','       Phase recuperatrice');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Temps de fonct                : ',tmot,' s         ',trec,' s');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Regime moyen                  : ',wmg_moymot*30/pi,' tr/mn   ',wmg_moyrec*30/pi,' tr/mn');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Couple maximum                : ',cmg_max,' Nm       ',cmg_min,' Nm');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Couple moyen                  : ',cmg_moymot,' Nm       ',cmg_moyrec,' Nm');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Puissance mecanique maximum   : ',pm_max./1000,' kW       ',pm_min./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Puissance mecanique moyenne   : ',pm_moymot./1000,' kW       ',pm_moyrec./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Puissance electrique maximum  : ',pe_max./1000,' kW       ',pe_min./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Puissance electrique moyenne  : ',pe_moymot./1000,' kW       ',pe_moyrec./1000,' kW');

fprintf(fid,'%s%6.2f%s%6.2f%s\n', ...
'  Rendement moyen ACM           : ',100.*rdmoymot,' %        ',100.*rdmoyrec,' %');

