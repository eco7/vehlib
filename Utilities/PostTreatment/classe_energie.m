% Function: classe_energie
% Calcul de classe d'energie et traces eventuels
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Generation de classe et traces eventuels
%
% Arguments d'appels:
% - t: occurence pour cumul (se peut etre le temps)
%
% - b: variable que l'on veut classer
%
% - Min: valeur mini pour classe
%
% - Max: valeur maxi pour classe
%
% - nbinter: nbre intervalles de classe
%
% - option=1 classe en fonction de t
%
% - option=3 classe en fonction de t*b
%
% - cadrant=0  tout
%
% - cadrant=1  analyse des valeurs positives seulement
%
% - cadrant=-1 analyse des valeurs negatives seulement
%
% - cumul=0 pas de cumul des classes
%
% - cumul=1 cumul des classes
%
% Exemple d'utilisation:
% (start code)
%
%        Inter=-125:25:350;
%        classe_energie(VD,tsim,ibat,3,min(ibat),max(ibat),10,0,0,1,{'titre'},1,Inter)
%
% (end)
% -------------------------------------------------------------

function [h, x, y, Xinter,Inter, B2bar]= classe_energie(t,b,option,Min,Max,nbinter,cadrant,cumul,fid,titre,trace,Inter)


% Remise en forme des vecteurs
%t=t';
%b=b';

% Reconstruction du pas de temps.
t2=t(2:length(t));
pas=t2-t(1:length(t)-1);
pas=[0 pas];

% Selection du cadrant eventuel
if cadrant==1
    b=b(b>0);
    pas=pas(b>0);
elseif cadrant==-1
    b=b(b<0);
    pas=pas(b<0);
    b=-b;
end

% Calcul des classes
%r=fix(Max-Min);
if isempty(Inter)
    Vinter= fix(Max-Min)./nbinter;
    Inter=Min:Vinter:Max;
else
    Vinter=(Inter(end)-Inter(1))/(length(Inter)-1);
end

% Constitution des classes
B2bar=zeros(size(Inter));
for j=1:length(Inter)-1
    if option==1
        % Classes exprimees en t (temps par ex.)
        B2bar(j)=sum(pas(find(b>=Inter(j) & b<Inter(j+1))));
    elseif option==3
        % Classes exprimees en t*b (energie par ex.)
        B2bar(j)=sum(pas(find(b>=Inter(j) & b<Inter(j+1))).*b(find(b>=Inter(j) & b<Inter(j+1))));
    end
    if cumul==1 & j>1
        % On cumule
        B2bar(j)= B2bar(j)+B2bar(j-1);
    end
end

if option==1
    B2bar(length(Inter))=sum(pas(find(b>=Inter(length(Inter)))));
elseif option==3
    B2bar(length(Inter))=sum(pas(find(b>=Inter(length(Inter)))).*b(find(b>=Inter(length(Inter)))));
end

if cumul==1
    % Cumul
    B2bar(length(Inter))= B2bar(length(Inter))+B2bar(length(Inter)-1);
end

if cumul==1
    B2bar_pc=100*B2bar./B2bar(length(Inter));
else
    B2bar_pc=100*B2bar./sum(B2bar);
end


h = [];x = []; y= [];Xinter = Inter+Vinter/2;
if trace==1
%set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));
% f = figure;
% clf;
Xinter = Inter+Vinter/2;
h = bar(Xinter,B2bar/3600,'BarWidth',0.6);
%bar(Inter,B2bar/3600,'BarWidth',0.6);
x = xlabel('courant en A');
y = ylabel('Ah dans chaque classe')
%set(gca,'XTick',Inter+Vinter/2)
S=[ ];
for i=1:length(Inter)
    %S=[S sprintf('%s%0.1f',' ',Inter(i)+Vinter/2)];
    S=[S sprintf('%s%0.1f',' ',Inter(i))];
end
set(gca,'XTick',str2num(S))
%set(gca,'XTickLabel',{'-pi','-pi/2','0','pi/2','pi'})
grid
end

% ECRITURES
% Titre
if fid~=0
    for i=1:length(titre)
        fprintf(fid,'%s\n',titre{i});
    end
    fprintf(fid,'%s%.2f\n','Intervalles de classe: ',Vinter);

    % Ecriture des valeurs, 10 par 10
    nbval_lig=5;
    for k=0:fix(nbinter/nbval_lig)
        for i=k*nbval_lig+1:min((k+1)*nbval_lig,length(Inter))
            if i==k*nbval_lig+1
                fprintf(fid,'%s','Classes     : ');
            end
            fprintf(fid,' %7.2f |',Inter(i)+Vinter/2);
        end
        fprintf(fid,'\n');

        for i=k*nbval_lig+1:min((k+1)*nbval_lig,length(Inter))
            if i==k*nbval_lig+1
                fprintf(fid,'%s','Valeurs (UP): ');
            end
            fprintf(fid,' %7.2f |',B2bar(i));
        end
        fprintf(fid,'\n');
        for i=k*nbval_lig+1:min((k+1)*nbval_lig,length(Inter))
            if i==k*nbval_lig+1
                fprintf(fid,'%s','Valeurs (%) : ');
            end
            fprintf(fid,' %7.2f |',B2bar_pc(i));
        end
        fprintf(fid,'\n\n');
        k=k+1;
    end
end
