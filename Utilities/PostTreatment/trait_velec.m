% File: trait_velec
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Script de post traitement d'un modele de vehicule electrique dans VEHLIB
%
% ------------------------------------------------------

vimin=0.01;

%lecture et increment du numero de calcul
fnum=strcat(VD.INIT.InitialFolder,VD.INIT.sep,'numcal.mat');
load(fnum);

% ecriture du fichier de resultats synthetiques
fid=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF1'),'w');
fid2=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF2'),'w');
fid3=fopen(strcat(VD.INIT.ResultsFolder,VD.INIT.sep,'RESUF3'),'w');

fprintf(fid,'\n\t%s\n', ['POST TRAITEMENT Veh. Electrique pour modele VEHLIB- VERSION ',VD.INIT.version]);
fprintf(fid,'\n\t%s\n','-----------------------------------------------------');
fprintf(fid,'\n%s %s','Date de traitement du fichier          :  ',date);
fprintf(fid,'\n%s %4d\n\n','Numero de calcul                       :  ',numcal);
fprintf(fid2,'\n\t%s\n',['POST TRAITEMENT Veh. Electrique pour modele VEHLIB- VERSION ',VD.INIT.version]);
fprintf(fid2,'\n\t%s\n','-----------------------------------------------------');
fprintf(fid2,'\n%s %s','Date de traitement du fichier         :  ',date);
fprintf(fid2,'\n%s %4d\n\n','Numero de calcul                      :  ',numcal);
fprintf(fid3,'\n\t%s\n',['POST TRAITEMENT Veh. Electrique pour modele VEHLIB- VERSION ',VD.INIT.version]);
fprintf(fid3,'\n\t%s\n','-----------------------------------------------------');
fprintf(fid3,'\n%s %s','Date de traitement du fichier         :  ',date);
fprintf(fid3,'\n%s %4d\n\n','Numero de calcul                      :  ',numcal);

fprintf(fid,'%s%s\n','Nom du fichier modele                  :   ',vehlib.nom);
if isfield(vehlib,'simulink')
    fprintf(fid,'%s%s\n','Nom du modele simulink                 :   ',vehlib.simulink);
end
fprintf(fid,'%s%s\n','Nom du fichier cinematique             :   ',vehlib.CYCL);
fprintf(fid,'%s%s\n','Nom du fichier vehicule                :   ',vehlib.VEHI);
fprintf(fid,'%s%s\n','Nom du fichier reducteur               :   ',vehlib.RED);
if(VD.ACM1.ntypmg==10)
   fprintf(fid,'%s%s\n','Nom du fichier ACM cartographie        :   ',vehlib.ACM1);
elseif(VD.ACM1.ntypmg==1)
   fprintf(fid,'%s%s\n','Nom du fichier moteur courant continu  :   ',VD.ACM1.nom_mot);
   fprintf(fid,'%s%s\n','Nom du fichier commande CC             :   ',VD.ACM1.nom_comm);
   fprintf(fid,'%s%s\n','Nom du fichier hacheur induit          :   ',VD.ACM1.nom_hind);
   fprintf(fid,'%s%s\n','Nom du fichier hacheur excitation      :   ',VD.ACM1.nom_hex);
elseif(VD.ACM1.ntypmg==3)
   fprintf(fid,'%s%s\n','Nom du fichier moteur synchrone bobine :   ',VD.ACM1.nom_mot);
   fprintf(fid,'%s%s\n','Nom du fichier commande MSB            :   ',VD.ACM1.nom_comm);
   fprintf(fid,'%s%s\n','Nom du fichier onduleur                :   ',VD.ACM1.nom_ond);

elseif(VD.ACM1.ntypmg==2)
   fprintf(fid,'%s%s\n','Nom du fichier moteur asynchrone       :   ',VD.ACM1.nom_mot);
   fprintf(fid,'%s%s\n','Nom du fichier commande MAS            :   ',VD.ACM1.nom_comm);
   fprintf(fid,'%s%s\n','Nom du fichier onduleur                :   ',VD.ACM1.nom_ond);

end
fprintf(fid,'%s%s\n','Nom du fichier batterie                :   ',vehlib.BATT);
fprintf(fid,'%s%s\n','Nom du fichier monobloc batterie       :   ',VD.BATT.Nom_bloc);
fprintf(fid,'%s%s\n\n','Nom du fichier auxiliaire              :   ',vehlib.ACC);

if VD.ACM1.ntypmg==1 
   if VD.ACM1.ntypcomcc==1
      fprintf(fid,'%s\n\n','Strategie de commande du moteur : Flux maximum');
   end
end

if VD.ACM1.ntypmg==3 
   if VD.ACM1.ntypcommsb==1
      fprintf(fid,'%s\n\n','Strategie de commande du moteur : Couplee  maximum');
   elseif VD.ACM1.ntypcommsb==2
      fprintf(fid,'%s\n\n','Strategie de commande du moteur : CRendementt maximum');      
   end
end

if VD.ACM1.ntypmg==2 
   if VD.ACM1.ntypcommas==1
      fprintf(fid,'%s\n\n','Strategie de commande du moteur : Flux maximum');
   elseif VD.ACM1.ntypcommas==2
      fprintf(fid,'%s\n\n','Strategie de commande du moteur : Courant stator minimum');      
   end
end

fprintf(1,'\n\t%s\n',['POST TRAITEMENT Veh. Electrique pour modele VEHLIB- VERSION ',VD.INIT.version]);
fprintf(1,'\n\t%s\n','-------------------------------------------------------------');


% Ecriture du bilan de masse
dep_masse(VD, masse, fid);

% depouillement
[pas]=dep_cin(VD,tsim, distance, vit, vit_dem, acc, zeros(length(tsim),1), vimin, fid);

if(VD.VEHI.ntypveh==1)
   [pveh]=dep_veh1(VD,tsim, pas, vit, acc, froul, faero, fpente, cfrein_meca, cinertie, vimin, fid);
elseif(VD.VEHI.ntypveh==2)
   [pveh]=dep_veh2(VD,tsim, pas, vit, acc, force1, force2, force3, fpente, cinertie, vimin, fid);
end

chaine='          ***   CARACTERISTIQUES REDUCTEUR FINAL   *****';
dep_transfo_meca(VD,tsim, pas, vit, wsec_red, csec_red, wprim_red, cprim_red, chaine, vimin, fid);

if(VD.ACM1.ntypmg==10)
    chaine='          ***   CARACTERISTIQUES ASSOC. CONVERTISSEUR MACHINE ELECTRIQUE  ***';
    [pmec_mg1, pcacm1]=dep_mct(VD,tsim, vit, pas, wacm1, cacm1_sans_inertie, iacm1, uacm1, chaine, vimin, fid);
elseif(VD.ACM1.ntypmg==1)
   [pmec_mg1, pind1, pcind1, pex1, pcex1]=dep_mcc(VD, tsim, vit, pas, ...
   wacm1, cacm1, img1, uind1, uex1, iex1, ...
   ubat,ibat, iacm1, ihex1, fid);
elseif(VD.ACM1.ntypmg==3)
   [pmec_mg1, pstat1, pcstat1, pex1, pcex1,istat1, vstat1, cosfi1]=dep_msb(VD, tsim, vit, pas, ...
   1,wacm1, cacm1, id1, vd1, iq1, vq1, vex1, iex1, ...
   ubat,ibat, iacm1, ihex1,ptjstat1,ptjexcit1,ptfer1,ptmeca1, fid);

elseif(VD.ACM1.ntypmg==2)
   [pmec_mg1, pstat1, pcstat1,istat1, vstat1, cosfi1, Gliss1]=dep_mas(VD, tsim, vit, pas, ...
   1,wacm1, cacm1, id1, vd1, iq1, vq1, ...
   ubat,ibat, iacm1, ptjstat1, ptfer1, ptmeca1,wrmg1, wsmg1, fid);

end

dep_acc(VD,tsim, pas, vit, iacc, uacc, fid)

[pbat]=dep_bat(VD, tsim, pas, vit, ibat, ubat, dod, pertes_bat, vimin, fid, cahbat);


%%% ENERGIES  %%%%

% script d initialisation des bilans sources/consommateurs
ecr_energie('initwh',fid2,'',[1 2 3]);
ecr_energie('initwhkm',fid3,'',[1 2 3]);

% initialisation de la structure pour renseigner les flux d energie
flux=struct([]);
assignin('base','flux',flux);

if(VD.VEHI.ntypveh==1)
   energie_veh1(VD, tsim, pas, vit, vit_dem, distance(length(distance)), fpente, faero, froul, cfrein_meca, cinertie, wroue, pveh, zeros(size(vit)), vimin, fid2, fid3);
elseif(VD.VEHI.ntypveh==2)
   energie_veh2(VD, tsim, pas, vit, vit_dem, distance(length(distance)), fpente, force1, force2, force3, cfrein_meca, cinertie, wroue, pveh, zeros(size(tsim)), vimin, fid2, fid3);
end

chaine='Pertes reducteur';
energie_transfo_meca(VD, tsim, pas, vit_dem, distance(length(distance)), csec_red, wsec_red, cprim_red, wprim_red, pveh, zeros(size(tsim)), chaine, vimin, fid2, fid3)

if(VD.ACM1.ntypmg==10)
   energie_MCT(VD, tsim, pas, vit_dem, distance(length(distance)), pmec_mg1, pcacm1, pveh, zeros(size(tsim)), vimin, 1, fid2, fid3)
elseif(VD.ACM1.ntypmg==1)
   energie_MCC(VD,tsim, pas, vit,vit_dem, distance(length(distance)), acc, pveh, zeros(size(tsim)), pmec_mg1, pind1, pcind1, pex1, pcex1, ...
   ptmeca1, ptjind1, ptjexcit1, ptfer1, ptsupp1, ubal1, img1, vimin, 1, fid2, fid3);
elseif(VD.ACM1.ntypmg==2)
   energie_MAS(VD, tsim, pas, vit,vit_dem, distance(length(distance)), acc, pveh, zeros(size(tsim)), pmec_mg1, pstat1, pcstat1, ...
   ptmeca1, ptjstat1, ptfer1, vimin, 1, fid2, fid3);
elseif(VD.ACM1.ntypmg==3)
   energie_MSB(VD, tsim, pas, vit,vit_dem, distance(length(distance)), acc, pveh, zeros(size(tsim)), pmec_mg1, pstat1, pcstat1, pex1, pcex1, ...
   ptmeca1, ptjstat1, ptjexcit1, ptfer1, vimin, 1, fid2, fid3);
end

energie_access(VD, tsim, pas, vit_dem, distance(length(distance)), pveh, iacc, uacc, zeros(size(tsim)), vimin, fid2, fid3);

energie_bat(VD, tsim, pas, vit_dem, distance(length(distance)), pbat, pveh, zeros(size(tsim)), vimin, fid2, fid3);

% script de fin des bilans sources/consommateurs
ecr_energie('end',fid2,'',[1 2 3]);
ecr_energie('end',fid3,'',[1 2 3]);

% fermeture
fclose(fid);
fclose(fid2);
fclose(fid3);

if isunix
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF1')]);
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF2')]);
   eval(['!chmod 660 ',fullfile(VD.INIT.ResultsFolder,'RESUF3')]);
end
