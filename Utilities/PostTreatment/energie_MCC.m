% Function: energie_MCC
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_MCC(VD,temps, pas, vit,vitdem, dist, acc, pveh, elhyb, pmec, pind, pcind, pex, pcex, ...
%                      PtMeca, PtJind, PtJexcit, PtFer, PtSupp, ubal, img, vimin, nummg, fid, fid3)
% ------------------------------------------------------
 function [] = energie_MCC(VD,temps, pas, vit,vitdem, dist, acc, pveh, elhyb, pmec, pind, pcind, pex, pcex, ...
                      PtMeca, PtJind, PtJexcit, PtFer, PtSupp, ubal, img, vimin, nummg, fid, fid3)
        
%icmg=(pcind+pex)./ubat;

% Initialisation
FMGMECA_M=0;
QMGMECA_M=0;
QMGFER_M=0;
QMGJOULE_M=0;
QMGSUPP_M=0;
QMGBAL_M=0;
FMGIND_M=0;
FMGCIND_M=0;
QMGEX_M=0;

FMGMECA_R=0;
QMGMECA_R=0;
QMGFER_R=0;
QMGJOULE_R=0;
QMGSUPP_R=0;
QMGBAL_R=0;
FMGIND_R=0;
FMGCIND_R=0;
QMGEX_R=0;

FMGMECA_A=0;
QMGMECA_A=0;
QMGFER_A=0;
QMGJOULE_A=0;
QMGSUPP_A=0;
QMGBAL_A=0;
FMGIND_A=0;
FMGCIND_A=0;
QMGEX_A=0;


for i=2:length(temps)
   if(elhyb(i)==0)
      if(vitdem(i)>vimin)
         if(pveh(i)>= 0)

      FMGMECA_M=FMGMECA_M+pmec(i)*pas(i)/3600;
      QMGMECA_M=QMGMECA_M+PtMeca(i)*pas(i)/3600;
      QMGFER_M=QMGFER_M+PtFer(i)*pas(i)/3600;
      QMGJOULE_M=QMGJOULE_M+PtJind(i)*pas(i)/3600;
      QMGSUPP_M=QMGSUPP_M+PtSupp(i)*pas(i)/3600;
      QMGBAL_M=QMGBAL_M+ubal(i)*img(i)*pas(i)/3600;
      FMGIND_M=FMGIND_M+pind(i)*pas(i)/3600;
      FMGCIND_M=FMGCIND_M+pcind(i)*pas(i)/3600;
      QMGEX_M=QMGEX_M+pex(i)*pas(i)/3600;
   else
      FMGMECA_R=FMGMECA_R+pmec(i)*pas(i)/3600;
      QMGMECA_R=QMGMECA_R+PtMeca(i)*pas(i)/3600;
      QMGFER_R=QMGFER_R+PtFer(i)*pas(i)/3600;
      QMGJOULE_R=QMGJOULE_R+PtJind(i)*pas(i)/3600;
      QMGSUPP_R=QMGSUPP_R+PtSupp(i)*pas(i)/3600;
      QMGBAL_R=QMGBAL_R+ubal(i)*img(i)*pas(i)/3600;
      FMGIND_R=FMGIND_R+pind(i)*pas(i)/3600;
      FMGCIND_R=FMGCIND_R+pcind(i)*pas(i)/3600;
      QMGEX_R=QMGEX_R+pex(i)*pas(i)/3600;
   end
 else
      FMGMECA_A=FMGMECA_A+pmec(i)*pas(i)/3600;
      QMGMECA_A=QMGMECA_A+PtMeca(i)*pas(i)/3600;
      QMGFER_A=QMGFER_A+PtFer(i)*pas(i)/3600;
      QMGJOULE_A=QMGJOULE_A+PtJind(i)*pas(i)/3600;
      QMGSUPP_A=QMGSUPP_A+PtSupp(i)*pas(i)/3600;
      QMGBAL_A=QMGBAL_A+ubal(i)*img(i)*pas(i)/3600;
      FMGIND_A=FMGIND_A+pind(i)*pas(i)/3600;
      FMGCIND_A=FMGCIND_A+pcind(i)*pas(i)/3600;
      QMGEX_A=QMGEX_A+pex(i)*pas(i)/3600;
 end
end
end


% en Wh.
if(max(elhyb)==0)
ecr_energie('sum',fid, 'Energie meca Mot. elec. : ', [FMGMECA_M FMGMECA_R]);
fprintf(fid,'%s%s\n',...
   '                           -----------','            -----------');
ecr_energie('sum',fid, 'Pertes meca Mot. elec.  : ',[QMGMECA_M QMGMECA_R]);
ecr_energie('sum',fid, 'Pertes fer Mot. elec.   : ',[QMGFER_M QMGFER_R]);
ecr_energie('sum',fid, 'Pertes joule Mot. elec. : ',[QMGJOULE_M QMGJOULE_R]);
ecr_energie('sum',fid, 'Pertes supp Mot. elec.  : ',[QMGSUPP_M QMGSUPP_R]);
ecr_energie('sum',fid, 'Pertes dans les balais  : ',[QMGBAL_M QMGBAL_R]);
fprintf(fid,'%s%s\n',...
   '                           -----------','            -----------');
ecr_energie('sum',fid, 'Energie sur l''induit    : ',[FMGIND_M FMGIND_R]);
ecr_energie('sum',fid, 'Pertes controle         : ',[FMGCIND_M-FMGIND_M FMGCIND_R-FMGIND_R]);
ecr_energie('sum',fid, 'Pertes excitation       : ',[QMGEX_M QMGEX_R QMGEX_A]);
fprintf(fid,'%s%s%s\n',...
   '                           -----------','            -----------','      -----------');
ecr_energie('sum',fid, 'Energie controle        : ',[FMGCIND_M+QMGEX_M FMGCIND_R+QMGEX_R FMGCIND_A+QMGEX_A]);
else
   if nummg==1
      ecr_energie('sum',fid, 'Pertes induit  VD.ACM1     : ', ...
      [QMGMECA_M+QMGJOULE_M+QMGFER_M+QMGSUPP_M+QMGBAL_M QMGMECA_R+QMGJOULE_R+QMGFER_R+QMGSUPP_R+QMGBAL_R]);
      ecr_energie('sum',fid, 'Pertes excitation VD.ACM1  : ',[QMGEX_M QMGEX_R QMGEX_A]);
      ecr_energie('sum',fid, 'Pertes controle  VD.ACM1   : ',[FMGCIND_M-FMGIND_M FMGCIND_R-FMGIND_R]);
   elseif nummg==2
      ecr_energie('sum',fid, 'Pertes induit  VD.ACM2     : ', ...
      [QMGMECA_M+QMGJOULE_M+QMGFER_M+QMGSUPP_M+QMGBAL_M QMGMECA_R+QMGJOULE_R+QMGFER_R+QMGSUPP_R+QMGBAL_R]);
      ecr_energie('sum',fid, 'Pertes excitation VD.ACM2  : ',[QMGEX_M QMGEX_R QMGEX_A]);
      ecr_energie('sum',fid, 'Pertes controle  VD.ACM2   : ',[FMGCIND_M-FMGIND_M FMGCIND_R-FMGIND_R]);
   end
end


% en Wh/km.
if(max(elhyb)==0)
ecr_energie('sum',fid3, 'Energie meca Mot. elec. : ', [FMGMECA_M FMGMECA_R]/(dist/1000));
fprintf(fid,'%s%s\n',...
   '                           -----------','            -----------');
ecr_energie('sum',fid3, 'Pertes meca Mot. elec.  : ',[QMGMECA_M QMGMECA_R]/(dist/1000));
ecr_energie('sum',fid3, 'Pertes fer Mot. elec.   : ',[QMGFER_M QMGFER_R]/(dist/1000));
ecr_energie('sum',fid3, 'Pertes joule Mot. elec. : ',[QMGJOULE_M QMGJOULE_R]/(dist/1000));
ecr_energie('sum',fid3, 'Pertes supp Mot. elec.  : ',[QMGSUPP_M QMGSUPP_R]/(dist/1000));
ecr_energie('sum',fid3, 'Pertes dans les balais  : ',[QMGBAL_M QMGBAL_R]/(dist/1000));
fprintf(fid3,'%s%s\n',...
   '                           -----------','            -----------');
ecr_energie('sum',fid3, 'Energie sur l''induit    : ',[FMGIND_M FMGIND_R]/(dist/1000));
ecr_energie('sum',fid3, 'Pertes controle         : ',[FMGCIND_M-FMGIND_M FMGCIND_R-FMGIND_R]/(dist/1000));
ecr_energie('sum',fid3, 'Pertes excitation       : ',[QMGEX_M QMGEX_R QMGEX_A]/(dist/1000));
fprintf(fid3,'%s%s%s\n',...
   '                           -----------','            -----------','      -----------');
ecr_energie('sum',fid3, 'Energie controle        : ',[FMGCIND_M+QMGEX_M FMGCIND_R+QMGEX_R FMGCIND_A+QMGEX_A]/(dist/1000));
else
   if nummg==1
      ecr_energie('sum',fid3, 'Pertes induit  VD.ACM1     : ', ...
      [QMGMECA_M+QMGJOULE_M+QMGFER_M+QMGSUPP_M+QMGBAL_M QMGMECA_R+QMGJOULE_R+QMGFER_R+QMGSUPP_R+QMGBAL_R]/(dist/1000));
      ecr_energie('sum',fid3, 'Pertes excitation VD.ACM1  : ',[QMGEX_M QMGEX_R QMGEX_A]/(dist/1000));
      ecr_energie('sum',fid3, 'Pertes controle  VD.ACM1   : ',[FMGCIND_M-FMGIND_M FMGCIND_R-FMGIND_R]/(dist/1000));
   elseif nummg==2
      ecr_energie('sum',fid3, 'Pertes induit  VD.ACM2     : ', ...
      [QMGMECA_M+QMGJOULE_M+QMGFER_M+QMGSUPP_M+QMGBAL_M QMGMECA_R+QMGJOULE_R+QMGFER_R+QMGSUPP_R+QMGBAL_R]/(dist/1000));
      ecr_energie('sum',fid3, 'Pertes excitation VD.ACM2  : ',[QMGEX_M QMGEX_R QMGEX_A]/(dist/1000));
      ecr_energie('sum',fid3, 'Pertes controle  VD.ACM2   : ',[FMGCIND_M-FMGIND_M FMGCIND_R-FMGIND_R]/(dist/1000));
   end
end

