% Function: energie_access
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
% Depouillement des energies consommees par les inertie de la liaison rigide
% ------------------------------------------------------
function [] = energie_liaison_rigide(VD,tsim, pas, vitdem, dist, pveh, carbre1_rigide, carbre2_rigide, warbre_rigide, elehyb, vimin, fid, fid3);

pinertie=(carbre1_rigide+carbre2_rigide).*warbre_rigide;

% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>= 0';
FINERTIE_M=sum(pinertie(eval(condition)).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb==0 & vitdem>vimin & pveh< 0';
FINERTIE_R=sum(pinertie(eval(condition)).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb==0 & vitdem<vimin';
FINERTIE_A=sum(pinertie(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>= 0';
FINERTIE_MH=sum(pinertie(eval(condition)).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb~=0 & vitdem>vimin & pveh< 0';
FINERTIE_RH=sum(pinertie(eval(condition)).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb~=0 & vitdem<vimin';
FINERTIE_AH=sum(pinertie(eval(condition)).*pas(eval(condition))/3600);

% en Wh.
if(max(elehyb)==0)
 ecr_energie('sum',fid, 'Energie inertie GE      : ', [FINERTIE_M FINERTIE_R FINERTIE_A]);
else
 ecr_energie('sum',fid, 'Energie inertie GE      : ', [FINERTIE_M FINERTIE_MH FINERTIE_R FINERTIE_RH FINERTIE_A FINERTIE_AH]);
end   

% en Wh/km.
if(max(elehyb)==0)
 ecr_energie('sum',fid3, 'Energie inertie GE      : ', [FINERTIE_M FINERTIE_R FINERTIE_A]/(dist/1000));
else
 ecr_energie('sum',fid3, 'Energie inertie GE      : ', [FINERTIE_M FINERTIE_MH FINERTIE_R FINERTIE_RH FINERTIE_A FINERTIE_AH]/(dist/1000));
end

% Ecriture de la structure de flux
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='FINERTIEE_MA';
flux(j).type='';
flux(j).valeur=FINERTIE_M+FINERTIE_MH+FINERTIE_A+FINERTIE_AH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='FINERTIEE_R';
flux(j).type='';
flux(j).valeur=FINERTIE_R+FINERTIE_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

assignin('base','flux',flux);
