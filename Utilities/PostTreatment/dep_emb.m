% Function: dep_emb
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = dep_emb(VD,temps, pas, vit, wsec, csec, wprim, cprim, pprim_in, rappvit, glisst, vimin, fid)
%
% resultat partie embrayage.
% secondaire= cote bv
% primaire= cote mth
% version 1: avril 2003
% ------------------------------------------------------
function [] = dep_emb(VD,temps, pas, vit, wsec, csec, wprim, cprim, pprim_in, rappvit, glisst, vimin, fid)

psec=wsec.*csec;
pprim=wprim.*cprim;

condition='glisst==0 & rappvit==1';

tglisst=sum(pas(eval(condition)));
if tglisst~=0
  pprim_gl=sum((pprim(eval(condition))-pprim_in(eval(condition))).*pas(eval(condition)))./tglisst;
  psec_gl=sum(psec(eval(condition)).*pas(eval(condition)))./tglisst;
  wmoy_glisst=sum(wprim(eval(condition)).*pas(eval(condition)))./tglisst;
else
  pprim_gl=0;
  psec_gl=0;
  wmoy_glisst=0;
end

fprintf(fid,'\n%s\n\n','          *** CARACTERISTIQUES EMBRAYAGE ***');

fprintf(fid,'%s\n','           Phase de glissement en premiere:');
fprintf(fid,'%s%6.2f%s\n',...
   '  Duree de glissement emb. : ',tglisst,' sec.   ');
fprintf(fid,'%s%6.2f%s\n',...
   '  Regime moyen arbre prim. : ',wmoy_glisst*30/pi,' tr/mn   ');
fprintf(fid,'%s%6.2f%s\n',...
   '  Puissance prim glisst    : ',pprim_gl/1000,' kW   ');
fprintf(fid,'%s%6.2f%s\n',...
   '  Puissance sec glisst     : ',psec_gl/1000,' kW   ');
fprintf(fid,'%s%6.2f%s\n',...
   '  Rendement phase glisst   : ',100*psec_gl/pprim_gl,' %  ');
