% Function: energie_accm2
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_accm2(VD,temps, pas, vitdem, dist, paccm1, paccm2, pveh, elehyb, vimin, fid, fid3)
%
% Version 1: Avril 2003
%   Alternateur
%   Compresseur de climatisation
%
% VD.ACC.ntypacc=2
%
% Depouillement des energies consommees par les accessoires mecaniques
% ------------------------------------------------------
function [] = energie_accm2(VD,temps, pas, vitdem, dist, paccm1, paccm2, pveh, elehyb, vimin, fid, fid3)

% signe pour coherence "pertes accessoires" dans resuf
paccm1=-paccm1;
paccm2=-paccm2;

% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>= 0';
FVD.ACC1_M=sum(paccm1(eval(condition)).*pas(eval(condition))/3600);
FVD.ACC2_M=sum(paccm2(eval(condition)).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb==0 & vitdem>vimin & pveh< 0';
FVD.ACC1_R=sum(paccm1(eval(condition)).*pas(eval(condition))/3600);
FVD.ACC2_R=sum(paccm2(eval(condition)).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb==0 & vitdem<vimin';
FVD.ACC1_A=sum(paccm1(eval(condition)).*pas(eval(condition))/3600);
FVD.ACC2_A=sum(paccm2(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>= 0';
FVD.ACC1_MH=sum(paccm1(eval(condition)).*pas(eval(condition))/3600);
FVD.ACC2_MH=sum(paccm2(eval(condition)).*pas(eval(condition))/3600);
% Phases de recup
condition='elehyb~=0 & vitdem>vimin & pveh< 0';
FVD.ACC1_RH=sum(paccm1(eval(condition)).*pas(eval(condition))/3600);
FVD.ACC2_RH=sum(paccm2(eval(condition)).*pas(eval(condition))/3600);
% Phases d'arret
condition='elehyb~=0 & vitdem<vimin';
FVD.ACC1_AH=sum(paccm1(eval(condition)).*pas(eval(condition))/3600);
FVD.ACC2_AH=sum(paccm2(eval(condition)).*pas(eval(condition))/3600);


% En Wh
if(max(elehyb)~=0)
 ecr_energie('sum',fid, 'Energie meca alternateur: ', [FVD.ACC1_M FVD.ACC1_MH FVD.ACC1_R FVD.ACC1_RH FVD.ACC1_A FVD.ACC1_AH]);
 ecr_energie('sum',fid, 'Energie meca compresseur: ', [FVD.ACC2_M FVD.ACC2_MH FVD.ACC2_R FVD.ACC2_RH FVD.ACC2_A FVD.ACC2_AH]);
end

% En Wh/km
if(max(elehyb)~=0)
ecr_energie('sum',fid3, 'Energie meca alternateur: ', [FVD.ACC1_M FVD.ACC1_MH FVD.ACC1_R FVD.ACC1_RH FVD.ACC1_A FVD.ACC1_AH]/(dist/1000));
ecr_energie('sum',fid3, 'Energie meca compresseur: ', [FVD.ACC2_M FVD.ACC2_MH FVD.ACC2_R FVD.ACC2_RH FVD.ACC2_A FVD.ACC2_AH]/(dist/1000));
end
  
% Ecriture de la structure de flux
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='FVD.ACC_MA';
flux(j).type='';
flux(j).valeur=FVD.ACC1_M+FVD.ACC1_MH+FVD.ACC1_A+FVD.ACC1_AH+FVD.ACC2_M+FVD.ACC2_MH+FVD.ACC2_A+FVD.ACC2_AH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='FVD.ACC_R';
flux(j).type='';
flux(j).valeur=FVD.ACC1_R+FVD.ACC1_RH+FVD.ACC2_R+FVD.ACC2_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

assignin('base','flux',flux);
