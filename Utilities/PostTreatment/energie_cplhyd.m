% Function: energie_cplhyd
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = energie_cplhyd(VD,temps, pas, vitdem, dist, qconv, pveh, elehyb, vimin, fid, fid3)
%
% Depouillement des pertes energetiques du coupleur hydraulique
% ------------------------------------------------------
function [] = energie_cplhyd(VD,temps, pas, vitdem, dist, qconv, pveh, elehyb, vimin, fid, fid3)

% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>= 0';
QVD.CONV_M=sum(qconv(eval(condition)).*pas(eval(condition))/3600);
% Phases recuperatrices
condition='elehyb==0 & vitdem>vimin & pveh< 0';
QVD.CONV_R=sum(qconv(eval(condition)).*pas(eval(condition))/3600);
% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>= 0';
QVD.CONV_MH=sum(qconv(eval(condition)).*pas(eval(condition))/3600);
% Phases recuperatrices
condition='elehyb~=0 & vitdem>vimin & pveh< 0';
QVD.CONV_RH=sum(qconv(eval(condition)).*pas(eval(condition))/3600);

% En Wh
if(max(elehyb)~=0)
 ecr_energie('sum',fid, 'Pertes coupleur hyd.    : ', [QVD.CONV_M QVD.CONV_MH QVD.CONV_R QVD.CONV_RH 0 0]);
end

% En Wh/km
if(max(elehyb)~=0)
 ecr_energie('sum',fid3, 'Pertes coupleur hyd.    : ', [QVD.CONV_M QVD.CONV_MH QVD.CONV_R QVD.CONV_RH 0 0]/(dist/1000));
end
  
