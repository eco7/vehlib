% Function: dep_veh1
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [pveh] = dep_veh1(VD,Temps, pas, Vitesse, acc, froul, faero, fpente, cfrein_meca, cinertie, vimin, fid)
%
% Pour modele vehicule type roulement aero.
% ------------------------------------------------------
function [pveh] = dep_veh1(VD,Temps, pas, Vitesse, acc, froul, faero, fpente, cfrein_meca, cinertie, vimin, fid)

if nargin == 11
    fprintf(1,'%s\n','Gestion obsolescence-01/2016-Nombre d''argument incompatible, rajouter cinertie !');
end

%9 novembre 2001
% La force de freinage meca est supprimee de l'expression de pveh
% pour pb de pveh= +/-eps lorsqu'on supprime la recup sur les machines electriques
% distinction des phases motrices et recup dans RESUF2 et 3.
% en toute rigueur, il  s agit d'une puissance a la roue
% Attention, les inerties des moteurs en amont de l embrayage ne sont pas comptabilises

% Resistance a l avancement 
%finertie=Mveh.*acc;
finertie=cinertie/VD.VEHI.Rpneu;

%pveh=(froul+faero+fpente+cfrein_meca/VD.VEHI.Rpneu).*Vitesse;
pveh=(froul+faero+fpente+finertie).*Vitesse;
proul=froul.*Vitesse;
paero=faero.*Vitesse;
ppente=fpente.*Vitesse;
pinertie=finertie.*Vitesse;

% Phases motrices.
condition='Vitesse>vimin & pveh>0 ';
tmot=sum(pas(eval(condition)));
if(tmot~=0)
   pveh_moymot=sum(pveh(eval(condition)).*pas(eval(condition)))/tmot;
   proul_moymot=sum(proul(eval(condition)).*pas(eval(condition)))/tmot;
   paero_moymot=sum(paero(eval(condition)).*pas(eval(condition)))/tmot;
   ppente_moymot=sum(ppente(eval(condition)).*pas(eval(condition)))/tmot;
   pinertie_moymot=sum(pinertie(eval(condition)).*pas(eval(condition)))/tmot;
else
   pveh_moymot=0;
   proul_moymot=0;
   paero_moymot=0;
   ppente_moymot=0;
   pinertie_moymot=0;
end

% Phases recup.
condition='Vitesse>vimin & pveh<0 ';
trec=sum(pas(eval(condition)));
if(trec~=0)
   pveh_moyrec=sum(pveh(eval(condition)).*pas(eval(condition)))/trec;
   proul_moyrec=sum(proul(eval(condition)).*pas(eval(condition)))/trec;
   paero_moyrec=sum(paero(eval(condition)).*pas(eval(condition)))/trec;
   ppente_moyrec=sum(ppente(eval(condition)).*pas(eval(condition)))/trec;
   pinertie_moyrec=sum(pinertie(eval(condition)).*pas(eval(condition)))/trec;
else
   pveh_moyrec=0;
   proul_moyrec=0;
   paero_moyrec=0;
   ppente_moyrec=0;
   pinertie_moyrec=0;
end


fprintf(fid,'\n%s\n\n','          *** CARACTERISTIQUES VEHICULE ***');

fprintf(fid,'%s%s\n\n',...
'                 Phase motrice','  Phase recuperatrice');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Temps de fonct  : ',tmot,' s      ',trec,' s');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Puiss. roul moy : ',proul_moymot./1000,' kW     ',proul_moyrec./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Puiss. aero moy : ',paero_moymot./1000,' kW     ',paero_moyrec./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Puis inertie moy: ',pinertie_moymot./1000,' kW     ',pinertie_moyrec./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Puiss. pente moy: ',ppente_moymot./1000,' kW     ',ppente_moyrec./1000,' kW');
fprintf(fid,'%s%6.2f%s%6.2f%s\n',...
'  Puiss. veh moy  : ',pveh_moymot./1000,' kW     ',pveh_moyrec./1000,' kW');
