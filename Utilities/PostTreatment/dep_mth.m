% Function: dep_mth
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function [] = dep_mth(VD,temps, pas, vit, wmt, cmt, dist, conso, cumcarb, elhy, CO, HC, NOx, CO2, fid);
%
% Depouillement moteur thermiques
% ------------------------------------------------------
function [] = dep_mth(VD,temps, pas, vit, wmt, cmt, dist, conso, cumcarb, elhy, CO, HC, NOx, CO2, fid);


pmt=wmt.*cmt;
tmth=0;
pmth_moy=0;
wmt_moy=0;
cmt_moy=0;
co_cum=0;
co2_cum=0;
hc_cum=0;
nox_cum=0;
nbdem=0;

tmt=sum(pas(elhy==1));

for i=1:length(temps)
   if(elhy(i)==1)
      if(pmt(i)>-1e-3)
         % modif bj + rt pour comptabiliser la conso de ralenti
         tmth=tmth+pas(i);
         pmth_moy=pmth_moy+pmt(i).*pas(i);
         wmt_moy=wmt_moy+wmt(i)*pas(i);
         cmt_moy=cmt_moy+cmt(i)*pas(i);
         co_cum=co_cum+CO(i)*pas(i);
         co2_cum=co2_cum+CO2(i)*pas(i);
         hc_cum=hc_cum+HC(i)*pas(i);
         nox_cum=nox_cum+NOx(i)*pas(i);
      end
   end
   if(i>1&(elhy(i-1)==0|elhy(i-1)==10)&elhy(i)==1)
       nbdem=nbdem+1;
   end
end

if(tmth~=0)
   pmth_moy=pmth_moy/tmth;
   wmt_moy=wmt_moy/tmth;
   cmt_moy=cmt_moy/tmth;
else
   pmth_moy=0;
   wmt_moy=0;
   cmt_moy=0;
end


% Calcul de consommation
fprintf(fid,'\n%s\n\n','         *** CARACTERISTIQUES MOTEUR THERMIQUE ***');
fprintf(fid,'%s%6.2f%s\n', ...
'  Temps de fonct           : ',tmt,' s');
fprintf(fid,'%s%6i%s\n', ...
'  Nombre de demarrage      : ',nbdem,' /');
fprintf(fid,'%s%6.2f%s\n',...
'  Regime moyen             : ',wmt_moy*30/pi,' tr/mn');
fprintf(fid,'%s%6.2f%s\n',...
'  Couple moyen             : ',cmt_moy,' Nm');
fprintf(fid,'%s%6.2f%s\n',...
'  Puissance moyenne        : ',pmth_moy/1000,' kW');

if ~isfield(VD.MOTH,'modele_therm') || VD.MOTH.modele_therm~=2
    fprintf(fid,'%s%5.3f%s\n',...
    '  Consommation du cycle    : ',conso(length(conso)),' l');
    fprintf(fid,'%s%5.3f%s\n',...
    '  Consommation du cycle    : ',cumcarb(length(cumcarb)),' g');
    fprintf(fid,'%s%5.3f%s\n',...
    '  Consommation du cycle    : ',cumcarb(length(cumcarb))/(dist/1000),' g/km');
    fprintf(fid,'%s%5.2f%s\n\n',...
    '  Consommation en l/100 km : ',100*conso(length(conso))/(dist/1000),' l/100 km');
    co2_eq=44*cumcarb(length(cumcarb))./(12+VD.MOTH.CO2carb);
    fprintf(fid,'%s%5.2f%s\n',...
    '  Consommation de CO2 equivallente  : ',co2_eq/(dist/1000),' g/km');
    fprintf(fid,'%s%4.1f%s\n',...
    '  Rendement moyen moteur   : ',100*pmth_moy*tmth/(VD.MOTH.pci*cumcarb(length(cumcarb))),' %');
    fprintf(fid,'%s%4.1f%s\n',...
    '  Csp moyen moteur   : ',cumcarb(length(cumcarb))/(pmth_moy*tmth/(1000*3600)),' g/kWh');
end

if isfield(VD.MOTH,'modele_therm') && VD.MOTH.modele_therm==2
    % Prise en compte de la surconsommation liee au depart a froid moteur
    cumcarb_froid=evalin('base','cumcarb_froid');
    conso_froid=evalin('base','conso_froid');

    fprintf(fid,'\n%s\n','  * Conditions thermiques *');
    T_amb=VD.INIT.Tamb;
    fprintf(fid,'%s%5.1f%s\n',...
    '  Température ambiante           : ',T_amb,' °C');
    Tm_ini=VD.MOTH.Tinit;
    fprintf(fid,'%s%5.1f%s\n',...
    '  Température initiale moteur    : ',Tm_ini,' °C');
    Th_inf=VD.MOTH.Thuile_infini;
    fprintf(fid,'%s%5.1f%s\n',...
    '  Température huile équilibre    : ',Th_inf,' °C');

    fprintf(fid,'\n%s\n','  * Consommations moteur chaud *');
    fprintf(fid,'%s%5.3f%s\n',...
    '  Consommation du cycle    : ',conso(length(conso)),' l');
    fprintf(fid,'%s%5.3f%s\n',...
    '  Consommation du cycle    : ',cumcarb(length(cumcarb)),' g');
    fprintf(fid,'%s%5.3f%s\n',...
    '  Consommation du cycle    : ',cumcarb(length(cumcarb))/(dist/1000),' g/km');
    fprintf(fid,'%s%5.2f%s\n\n',...
    '  Consommation en l/100 km : ',100*conso(length(conso))/(dist/1000),' l/100 km');
    co2_eq=44*cumcarb(length(cumcarb))./(12+VD.MOTH.CO2carb);
    fprintf(fid,'%s%5.2f%s\n',...
    '  Consommation de CO2 equivallente  : ',co2_eq/(dist/1000),' g/km');
    fprintf(fid,'%s%4.1f%s\n',...
    '  Rendement moyen moteur   : ',100*pmth_moy*tmth/(VD.MOTH.pci*cumcarb(length(cumcarb))),' %');
    fprintf(fid,'%s%4.1f%s\n',...
    '  Csp moyen moteur   : ',cumcarb(length(cumcarb))/(pmth_moy*tmth/(1000*3600)),' g/kWh');

    fprintf(fid,'\n%s\n','  * Consommations avec départ froid *');
    fprintf(fid,'%s%5.3f%s\n',...
    '  Consommation du cycle    : ',conso_froid(length(conso_froid)),' l');
    fprintf(fid,'%s%5.3f%s\n',...
    '  Consommation du cycle    : ',cumcarb_froid(length(cumcarb_froid)),' g');
    fprintf(fid,'%s%5.3f%s\n',...
    '  Consommation du cycle    : ',cumcarb_froid(length(cumcarb_froid))/(dist/1000),' g/km');
    fprintf(fid,'%s%5.2f%s\n\n',...
    '  Consommation en l/100 km : ',100*conso_froid(length(conso_froid))/(dist/1000),' l/100 km');
    co2_eq=44*cumcarb_froid(length(cumcarb_froid))./(12+VD.MOTH.CO2carb);
    fprintf(fid,'%s%5.2f%s\n',...
    '  Consommation de CO2 equivalent en l/100 km : ',co2_eq/(dist/1000),' g/km');
    % Rendement et Csp
    fprintf(fid,'%s%4.1f%s\n',...
    '  Rendement moyen moteur   : ',100*pmth_moy*tmth/(VD.MOTH.pci*cumcarb_froid(length(cumcarb_froid))),' %');
    fprintf(fid,'%s%4.1f%s\n',...
    '  Csp moyen moteur   : ',cumcarb_froid(length(cumcarb_froid))/(pmth_moy*tmth/(1000*3600)),' g/kWh');
end

if co_cum~=0 | hc_cum~=0 |nox_cum~=0 | co2_cum~=0
    fprintf(fid,'\n%s\n','         *** EMISSIONS DU MOTEUR THERMIQUE ***');
    if co_cum~=0
        fprintf(fid,'%s%5.2f%s\n',...
        '  Emission de CO           : ',co_cum/(dist/1000),' g/km');
    end
    if hc_cum~=0
        fprintf(fid,'%s%5.2f%s\n',...
        '  Emission de HC           : ',hc_cum/(dist/1000),' g/km');
    end
    if nox_cum~=0
        fprintf(fid,'%s%5.2f%s\n',...
        '  Emission de NOx          : ',nox_cum/(dist/1000),' g/km');
    end
    if co2_cum~=0
        fprintf(fid,'%s%5.2f%s\n',...
        '  Emission de CO2          : ',co2_cum/(dist/1000),' g/km');
        if isfield(VD.MOTH,'CO2carb')
            CO2_conso=(12+VD.MOTH.CO2carb')*co2_cum/44;
            fprintf(fid,'%s%5.2f%s\n',...
            '  Consommation             : ',CO2_conso/(dist/1000),' g/km');
        end
    end
end
