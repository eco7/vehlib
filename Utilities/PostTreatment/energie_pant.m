% Function: energie_pant
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%function [resu_panto] = energie_pant(VD,temps, pas, vitdem, dist, upanto, ipanto, pveh, elehyb, vimin, fid, fid3)
%
% Depouillement des energies consommees par le pantographe
% ------------------------------------------------------
function [resu_panto] = energie_pant(VD,temps, pas, vitdem, dist, upanto, ipanto, pveh, elehyb, vimin, fid, fid3)


% En mode electrique
% Phases motrices
condition='elehyb==0 & vitdem>vimin & pveh>=0';
FPANT_M=sum(ipanto(eval(condition)).*upanto(eval(condition)).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb==0 & vitdem>vimin & pveh<0';
FPANT_R=sum(ipanto(eval(condition)).*upanto(eval(condition)).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb==0 & vitdem<vimin';
FPANT_A=sum(ipanto(eval(condition)).*upanto(eval(condition)).*pas(eval(condition))/3600);

% En mode hybride
% Phases motrices
condition='elehyb~=0 & vitdem>vimin & pveh>=0';
FPANT_MH=sum(ipanto(eval(condition)).*upanto(eval(condition)).*pas(eval(condition))/3600);
% Phases recup
condition='elehyb~=0 & vitdem>vimin & pveh<0';
FPANT_RH=sum(ipanto(eval(condition)).*upanto(eval(condition)).*pas(eval(condition))/3600);
% Phases d arret
condition='elehyb~=0 & vitdem<vimin';
FPANT_AH=sum(ipanto(eval(condition)).*upanto(eval(condition)).*pas(eval(condition))/3600);


resu_panto=FPANT_M + FPANT_MH + FPANT_R + FPANT_RH + FPANT_A + FPANT_AH;

% en Wh.
if(max(elehyb)==0)
 ecr_energie('substract',fid, 'Energie pantographe     : ', [FPANT_M FPANT_R FPANT_A]);
else
 ecr_energie('substract',fid, 'Energie pantographe     : ', [FPANT_M FPANT_MH FPANT_R FPANT_RH FPANT_A FPANT_AH]);
end

% en Wh/km.
if(max(elehyb)==0)
 ecr_energie('substract',fid3, 'Energie pantographe     : ', [FPANT_M FPANT_R FPANT_A]/(dist/1000));
else
 ecr_energie('substract',fid3, 'Energie pantographe     : ', [FPANT_M FPANT_MH FPANT_R FPANT_RH FPANT_A FPANT_AH]/(dist/1000));
end

% Ecriture de la structure de flux
flux=evalin('base','flux');
j=length(flux);

j=j+1;
flux(j).nom='FPANT_MA';
flux(j).type='';
flux(j).valeur=FPANT_M+FPANT_MH+FPANT_A+FPANT_AH;
flux(j).position=[0.25 0.33];
flux(j).couleur='black';

j=j+1;
flux(j).nom='FPANT_R';
flux(j).type='';
flux(j).valeur=FPANT_R+FPANT_RH;
flux(j).position=[0.25 0.33];
flux(j).couleur='green';

assignin('base','flux',flux);
