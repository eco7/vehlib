function resultat = lsDirsWithFiles(srcdir, extension)
% function dirList = lsDirsWithFiles(srcdir, extension)
%cherche les fichiers avec lsFiles
%
%   See also lsFiles, lsEmptyFolders.
fileList = lsFiles(srcdir, extension);
[mesDossiers mesFichiers mesExt] =  cellfun(@(x) fileparts(x),fileList,'UniformOutput',false);
dirList = unique(mesDossiers);

resultat = struct([]);
taille = cell(size(dirList));
nbFiles = cell(size(dirList));
for ind = 1:length(dirList)
D = dir(dirList{ind});
D = D(~[D.isdir]);
taille{ind} = sum([D.bytes]);
nbFiles{ind} = length(D);
resultat(ind).dirList = dirList{ind};
resultat(ind).taille = sprintf('%d',taille{ind});
resultat(ind).nbFiles = sprintf('%d',nbFiles{ind});
end
% resultat.dirList = dirList;
% resultat.taille = taille;
% resultat.nbFiles = nbFiles;
end