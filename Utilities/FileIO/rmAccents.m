function rmAccents(srcdir)
% function rmAccents(srcdir)
% Recusively change file extension.
% TODO: accents in files compatibility
% parametres:
% - srcdir: [string] nom du repertoire a traiter (absolu ou relatif)
%   See also lsFiles, cpFiles, cpFilesNOW.

D = dir(srcdir);
%filtrer fichiers caches (commencant par point)
fileList = {D.name}';
indices = cellfun(@(x) x(1)~='.',fileList);
fileList = fileList(indices);
D = D(indices);
for ind = 1:length(D)
    filename = fileList{ind};
    pathname = fullfile(srcdir,filename);
    [Folder File Ext] = fileparts(pathname);
        newFile = cleanStr(File);
        if ~isequal(newFile,File)
            newname = fullfile(Folder,sprintf('%s%s',newFile,Ext));
            movefile(pathname,newname);
            fprintf('%s>%s\n',pathname,newname);
        end
    if D(ind).isdir
        rmAccents(pathname);
    end
end