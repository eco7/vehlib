% Function: create_path
% Fonction d'initialisation du chemin vers un dossier de donnees
%
%  (C) Copyright IFSTTAR LTE 1999-2011
%
% -------------------------------------------------------------------------
%
% Objet:
% Utilise par des scripts de traitement de donnees, il permet au premier
% lancement de choisir le dossier contenant les donnees. Un fichier
% initdatadir sera cree, permettant de memoriser le dossier pour les
% prochains appels.
%
% Auteur      : IFSTTAR/LTE
%
% Date de creation :
% Oct 2013
%
% -------------------------------------------------------------------------
function [] = create_path()

if exist('initdatadir.m','file')
    initdatadir
else
    folder_name = uigetdir(pwd,'Selectionnez le dossier contenant les donnees a traiter.');
    fid = fopen('initdatadir.m','w');
    Contenu = {'% Function: initdatadir';
        '% Contient un chemin vers un dossier de donnees';
        '%';
        '%  (C) Copyright IFSTTAR LTE 1999-2013 ';
        '%';
        '% -------------------------------------------------------------------------';
        '%';
        '% Objet:';
        '% Cree par createpath. Contient l''emplacement du dossier contenant';
        '% les donnees a traiter';
        '%';
        '% auteur      : IFSTTAR/LTE';
        '% ';
        '% -------------------------------------------------------------------------';
        'function []=initdatadir()';
        '';};
    fprintf(fid,'%s\n',Contenu{:});
    % Si l'on travail sur le reseau, le chemin vers le dossier de donnees
    % est relatif, sinon il est absolu
    if strfind(pwd,'LTE_VEH')
        new_folder = relative_path(pwd,folder_name);
    else
        new_folder = folder_name;
    end
    fprintf(fid,'evalin(''base'',''DATADIR=''''%s'''';'');\n',new_folder);
    
    fclose(fid);
    
    run('initdatadir')
end
