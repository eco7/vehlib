function printTree(dirname,prompt,level,fid)
% function printTree(dirname,prompt,level,fid)
%
%   See also lsFiles.
if ~exist('fid','var')
    fid = 0;
end
prompt(end+1) = prompt(end);
[D F E] = fileparts(dirname);
fprintf(fid,'%s%s\n',prompt,F);
if level>0
    %children list
    D = dir(dirname);
    %only folders
    D = D([D.isdir]);
    %only names
    listeDirs = {D.name}';
    %not hidden or special folders (like '.' or '..')
    listeDirs = listeDirs(~strncmp('.',listeDirs,1));
    for ind = 1:length(listeDirs)
        thisDir = fullfile(dirname,listeDirs{ind});
        printTree(thisDir,prompt,level-1,fid);
    end
end
end