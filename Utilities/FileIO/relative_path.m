% Function: relative_path
% Construction d'un chemin relatif entre un dossier et un autre.
%
%  (C) Copyright IFSTTAR LTE 1999-2011
%
% -------------------------------------------------------------------------
%
% Objet:
% Construction d'un chemin relatif entre un dossier source et un dossier
% destination.
% 
%   Cas 1 : S = /-/-/-/X/X/X/X
%           D : /-/-/-/O/O/O
%   Cas 2 : S = /-/-/-/X/X/X/X
%           D : /-/-/-/
%   Cas 3 : S = /-/-/-/
%           D : /-/-/-/O/O/O
%   Cas 1 : S = /-/-/-/
%           D : /-/-/-/
%
% Auteur      : IFSTTAR/LTE
%
% Date de creation : Oct 2013
% -------------------------------------------------------------------------
function chemin = relative_path(source_dir,dest_dir)

split_dest_dir = regexp(dest_dir,filesep,'split');
split_source_dir = regexp(source_dir,filesep,'split');

minlen = min(length(split_dest_dir),length(split_source_dir));
comp = strcmp(split_dest_dir(1:minlen),split_source_dir(1:minlen));
first_ptpt = find(comp~=1,1);

if (isempty(first_ptpt) && length(split_dest_dir)>=length(split_source_dir))
    % Cas 3 & Cas 4
    chemin = strcat('.',filesep);
    if length(split_dest_dir)>length(split_source_dir)
        % Cas 3
        chemin = strcat(chemin,fullfile(split_dest_dir{length(split_source_dir)+1:end}));
    end
else
    % Cas 1 & Cas 2
    nbr_ptpt = max(length(split_dest_dir),length(split_source_dir)) - minlen;
    if ~isempty(first_ptpt)
        % Cas 1
        nbr_ptpt = length(comp(first_ptpt:end)) + nbr_ptpt;
        chemin = fullfile(split_dest_dir{first_ptpt:end});
    else
        % Cas 2
        chemin = '';
    end
    % Cas 1 & Cas 2
    for i=1:nbr_ptpt
        chemin = strcat('..',filesep,chemin);
    end
end
