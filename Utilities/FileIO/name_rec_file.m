function name = name_rec_file(cinem,VD,param)

stack = dbstack;
caller = stack(2).name;
if isequal(caller,'graphe_HP_BV_2EMB_3U_TWC')
    call = '_2EMB_3U_TWC';
elseif isequal(caller,'graphe_VTH_BV_2U_TWC')
    call = '_2UVTH';
else
    call = '_';
end

if isfield(param,'Pacm1')
    p_machine = ['_acm' num2str(uint32(param.Pacm1))];
else
    p_machine = '';
end

if isfield(param,'lg_calcul')
    partiel = num2str(param.lg_calcul);
else
    partiel = '';
end

if isfield(param,'k_cata')
    k_cata = ['_kcat' num2str(param.k_cata)];
else
    k_cata = '';
end

if isfield(param,'emission_cst') && param.emission_cst
    emission_cst = '_Ecst';
else
    emission_cst = '';
end

if isfield(param,'pas_phi')
    phi = ['_dphi' erase(num2str(param.pas_phi),'.')];
else
    phi = '';
end

if isfield(param,'pas_delta_AA')
    AA = ['_dAA' erase(num2str(param.pas_delta_AA),'.')];
else
    AA = '';
end

if isfield(param,'pas_Padm')
    Padm = ['_dP' erase(num2str(param.pas_Padm),'.')];
else
    Padm = '';
end

if isfield(param,'pas_temps')
    tps = ['_dt' erase(num2str(param.pas_temps),'.')];
else
    tps = '_dt1';
end

% if VD.TWC.T0 == 350
%     light_off = '_T280';
% elseif VD.TWC.T0 == 47.941343132437310+273.15
%     light_off = '_T250';
% end
light_off='';

if isfield(param,'Pbat')
    Pbat = ['_Pbat' num2str(param.Pbat/1000)];
else
    Pbat = '';
end

if isfield(param,'pas_soc')
    pas_soc = ['_ds' erase(num2str(param.pas_soc),'.')];
else
    pas_soc = '';
end
if isfield(param,'res_batt')
    res_b = ['_' char(param.res_batt)];
else
    res_b = '';
end
if isfield(param,'pas_T')
    pas_T = ['_dT' erase(num2str(param.pas_T),'.')];
else
    pas_T = '';
end
if isfield(param,'alpha')
    p_alpha = ['_a' erase(num2str(param.alpha),'.')];
else
    p_alpha = '';
end

if isfield(VD.BATT,'std_ql_ini')
    p_ql_ini = ['ql' erase(num2str(VD.BATT.std_ql_ini),'.')];
else
    p_ql_ini = '';
end
if isfield(VD.BATT,'std_r_d')
    p_rd = ['_rd' erase(num2str(VD.BATT.std_r_d),'.')];
else
    p_rd = '';
end
if isfield(VD.BATT,'std_r_c')
    p_rc = ['_rc' erase(num2str(VD.BATT.std_r_c),'.')];
else
    p_rc = '';
end
if isfield(VD.BATT,'std_ah')
    p_ah = ['_ah' erase(num2str(VD.BATT.std_ah),'.')];
else
    p_ah = '';
end
if isfield(VD.BATT,'std_ql_var')
    p_ql_var = ['_qlVar' erase(num2str(VD.BATT.std_ql_var),'.')];
else
    p_ql_var = '';
end
if isfield(VD.BATT,'std_hs')
    p_hs = ['_hs' erase(num2str(VD.BATT.std_hs),'.')];
else
    p_hs = '';
end

if isfield(VD.INIT,'Tamb_var') && VD.INIT.Tamb_var == 1
    p_tamb = '_tamb_var';
elseif isfield(VD.INIT,'Tamb')
    p_tamb = ['_tamb' erase(num2str(VD.INIT.Tamb),'.')];
else
    p_tamb = '';
end


    
p_strat = ['_CBStrat_HL' erase(num2str(param.CBStrat_HL),'.')];
r_shunt = ['_rshunt' erase(num2str(VD.BATT.Rshunt),'.')];
r_MsocD = ['_MaxSocDisp' erase(num2str(param.MaxSocDisp),'.')];

% Strore the date
ds = datestr(datetime('now'),'yyyy_mm_dd_HH_MM_SS');
if ~strcmp(call,'_')
name = ['rec' call ...
        '_' erase(cinem,["CIN_","_BV","_lisse","_Classe3"])...
        partiel ...
        p_machine ...
        p_alpha ...
        pas_soc ...
        pas_T ...
        tps phi AA Padm ...
        res_b...
        k_cata...
        emission_cst...
        light_off...
        Pbat ...
        p_ql_ini ...
        p_ah ...
        p_ql_var ...
        p_hs ...
        p_rd ...
        p_rc ...
        p_tamb ...
        p_strat ...
        r_shunt ...
        r_MsocD ...
        '_date',ds];
else
name = [p_ql_ini ...
        p_ah ...
        p_ql_var ...
        p_hs ...
        p_rd ...
        p_rc ...
        p_tamb ...
        p_strat ...
        r_shunt ...
        r_MsocD ...
        '_date',ds];
end

end
