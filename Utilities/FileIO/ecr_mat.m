% -------------------------------------------------------------------------
% function []= ecr_mat(mat,Nom_mat,fid,form,commentaire)
%
% Fonction ecriture d'une valeur dans un matrice
% interface : mat=matrice des valeurs a �crire
%             Nom_mat=Nom du matrice dans le fichier d'�criture
%             fid=numero du fichier d'�criture
%             form=format d'�criture pour l'instant (6.3f ou E)
%
% Exemple d'appel
% ecr_mat(E_moth_i,strcat('E_moth_i',type),t_nom,fid10,'6.3f','% Energie fournie par le moteur thermique issue mesure PMI en Joules')
%
% avril 2006
% -------------------------------------------------------------------------
function [erreur]= ecr_mat(mat,Nom_mat,fid,form,commentaire,entete,entier_1)
if nargin < 7
    entier_1=0;
end

erreur=0;
if ~strcmp(entete,' ')
    n_ligne_entete=size(entete);
    Tmax=20; % taille max des chaine de la ligne d'entête
    for i_ligne_entete=1:n_ligne_entete(1)
        ligne_entete=entete(i_ligne_entete,:);
        %    if ischar('ligne_entete(1)') & ~strcmp(ligne_entete(1),' ')
        if ischar('ligne_entete(1)')
            for i=1:length(ligne_entete)
                if length(ligne_entete{i})<Tmax
                    n=Tmax-length(ligne_entete{i});
                    format=['%' num2str(n) 'c%s  '];
                    fprintf(fid,format,' ',ligne_entete{i});
                elseif length(ligne_entete{i}) == Tmax
                    fprintf(fid,'%s  ',ligne_entete{i});
                elseif  length(ligne_entete{i}) > Tmax
                    fprintf(fid,'%s%s  ',ligne_entete{i}(1:Tmax),'..');
                end
            end
        elseif ~strcmp(ligne_entete(1),' ')
            fprintf(1,'la ligne d''entete n''est pas constituer de caracteres');
        end
        fprintf(fid,'\r\n','   ');
    end
    fprintf(fid,'\r\n','   ');
end

if ~strcmp(Nom_mat,' ')
    fprintf(fid,'%s',Nom_mat);
    fprintf(fid,'%s',' = [');
    fprintf(fid,'\r\n','   ');
end

[Nb_ligne,Nb_colonne]=size(mat);
for i_ligne=1:Nb_ligne
    vec=mat(i_ligne,:);

 %% format 6 chiffres caracteristique si ligne de chiffre
    if isnumeric(vec(1))
        if strcmp(form,'6.3f')
            fprintf(fid,'%s','  ');
            for i=1:length(vec)
                if isnan(vec(i)) ~= 1
                    if vec(i)>0 & vec(i)<0.01
                        fprintf(fid,' %2.1E %s',vec(i),'   ');
                    elseif vec(i)>0 & vec(i)<10
                        fprintf(fid,' %6.3f %s',vec(i),'   ');
                    elseif  vec(i)>0 & vec(i)<100
                        fprintf(fid,' %6.3f %s',vec(i),'   ');
                    elseif  vec(i)>0 & vec(i)<1000
                        fprintf(fid,' %6.3f %s',vec(i),'   ');
                    elseif  vec(i)>0 & vec(i)<10000
                        fprintf(fid,' %6.2f %s',vec(i),'   ');
                    elseif  vec(i)>0 & vec(i)<100000
                        fprintf(fid,' %6.1f %s',vec(i),'   ');
                    elseif  vec(i)>0 & vec(i)<1000000
                        fprintf(fid,' %6.0f %s',vec(i),'   ');
                    elseif  vec(i)>1000000
                        %fprintf(fid,' %2.1E %s',vec(i),'   ');
                        fprintf(fid,' %10.3f %s',vec(i),'   ');

                    elseif vec(i)<0 & vec(i)>-0.01
                        fprintf(fid,'%2.1E %s',vec(i),'   ');
                    elseif vec(i)<0 & vec(i)>-10
                        fprintf(fid,'%6.3f %s',vec(i),'   ');
                    elseif  vec(i)<0 & vec(i)>-100
                        fprintf(fid,'%6.3f %s',vec(i),'   ');
                    elseif  vec(i)<0 & vec(i)>-1000
                        fprintf(fid,'%6.3f %s',vec(i),'   ');
                    elseif  vec(i)<0 & vec(i)>-10000
                        fprintf(fid,'%6.2f %s',vec(i),'   ');
                    elseif  vec(i)<0 & vec(i)>-100000
                        fprintf(fid,'%6.1f %s',vec(i),'   ');
                    elseif  vec(i)<0 & vec(i)>-1000000
                        fprintf(fid,'%6.0f %s',vec(i),'   ');
                    elseif  vec(i)<-1000000
                        fprintf(fid,'%2.1E %s',vec(i),'   ');

                    elseif  vec(i) == 0
                        fprintf(fid,'%6.1f %s',vec(i),'   ');
                    end
                else
                    fprintf(fid,'     %s %s','NaN','   ');
                end
            end

            %% format scientifique 2 chiffres apres virgule
        elseif strcmp(form,'E')
            fprintf(fid,'%s',' ');
            for i=1:length(vec)
                if vec(i)>=0
                    fprintf(fid,' %2.2E %s',vec(i),'  ');
                elseif vec(i)<0
                    fprintf(fid,'%2.2E %s',vec(i),'  ');
                end
            end
            
        elseif strcmp(form,'tab_tex')
             for i=1:length(vec)
                if isnan(vec(i)) ~= 1
                    if vec(i)>0 & vec(i)<0.01
                        fprintf(fid,' %2.1E %s',vec(i),'&');
                    elseif vec(i)>0 & vec(i)<10
                        fprintf(fid,' %6.3f %s',vec(i),'&');
                    elseif  vec(i)>0 & vec(i)<100
                        fprintf(fid,' %6.3f %s',vec(i),'&');
                    elseif  vec(i)>0 & vec(i)<1000
                        fprintf(fid,' %6.3f %s',vec(i),'&');
                    elseif  vec(i)>0 & vec(i)<10000
                        fprintf(fid,' %6.2f %s',vec(i),'&');
                    elseif  vec(i)>0 & vec(i)<100000
                        fprintf(fid,' %6.1f %s',vec(i),'&');
                    elseif  vec(i)>0 & vec(i)<1000000
                        fprintf(fid,' %6.0f %s',vec(i),'&');
                    elseif  vec(i)>1000000
                        %fprintf(fid,' %2.1E %s',vec(i),'   ');
                        fprintf(fid,' %10.3f %s',vec(i),'&');

                    elseif vec(i)<0 & vec(i)>-0.01
                        fprintf(fid,'%2.1E %s',vec(i),'&');
                    elseif vec(i)<0 & vec(i)>-10
                        fprintf(fid,'%6.3f %s',vec(i),'&');
                    elseif  vec(i)<0 & vec(i)>-100
                        fprintf(fid,'%6.3f %s',vec(i),'&');
                    elseif  vec(i)<0 & vec(i)>-1000
                        fprintf(fid,'%6.3f %s',vec(i),'&');
                    elseif  vec(i)<0 & vec(i)>-10000
                        fprintf(fid,'%6.2f %s',vec(i),'&');
                    elseif  vec(i)<0 & vec(i)>-100000
                        fprintf(fid,'%6.1f %s',vec(i),'&');
                    elseif  vec(i)<0 & vec(i)>-1000000
                        fprintf(fid,'%6.0f %s',vec(i),'&');
                    elseif  vec(i)<-1000000
                        fprintf(fid,'%2.1E %s',vec(i),'&');

                    elseif  vec(i) == 0
                        fprintf(fid,'%6.1f %s',vec(i),'&');
                    end
                else
                    fprintf(fid,'     %s %s','NaN','&');
                end
             end
             fprintf(fid,'%s','\\');
        elseif strcmp(form,'tab_tex_2')
            for i=1:length(vec)
                if isnan(vec(i)) ~= 1
                    if i==1 && entier_1==1
                        fprintf(fid,' %3.0f %s',vec(i),'&');
                    elseif (vec(i)==0 || vec(i)==1) &&  entier_1==1
                        fprintf(fid,' %3.0f %s',vec(i),'&');
                    elseif  abs(vec(i))<0.01
                        fprintf(fid,' %2.1E %s',vec(i),'&');
                    elseif abs(vec(i))<10
                        fprintf(fid,' %1.2f %s',vec(i),'&');
                    elseif abs(vec(i))<100
                        fprintf(fid,' %2.1f %s',vec(i),'&');
                    elseif  abs(vec(i))<1000
                        fprintf(fid,' %3.0f %s',vec(i),'&');
                    elseif  abs(vec(i))<10000
                        fprintf(fid,' %3.0f %s',vec(i),'&');
                    elseif  abs(vec(i))<100000
                        fprintf(fid,' %3.0f %s',vec(i),'&');
                    elseif abs(vec(i))<1000000
                        fprintf(fid,' %3.0f %s',vec(i),'&');
                    elseif  abs(vec(i))>1000000
                        fprintf(fid,' %1.2E %s',vec(i),'   ');
                        %fprintf(fid,' %10.3f %s',vec(i),'&');

%                     elseif vec(i)<0 & vec(i)>-0.01
%                         fprintf(fid,'%2.1E %s',vec(i),'&');
%                     elseif vec(i)<0 & vec(i)>-10
%                         fprintf(fid,'%6.3f %s',vec(i),'&');
%                     elseif  vec(i)<0 & vec(i)>-100
%                         fprintf(fid,'%6.3f %s',vec(i),'&');
%                     elseif  vec(i)<0 & vec(i)>-1000
%                         fprintf(fid,'%6.3f %s',vec(i),'&');
%                     elseif  vec(i)<0 & vec(i)>-10000
%                         fprintf(fid,'%6.2f %s',vec(i),'&');
%                     elseif  vec(i)<0 & vec(i)>-100000
%                         fprintf(fid,'%6.1f %s',vec(i),'&');
%                     elseif  vec(i)<0 & vec(i)>-1000000
%                         fprintf(fid,'%6.0f %s',vec(i),'&');
%                     elseif  vec(i)<-1000000
%                         fprintf(fid,'%2.1E %s',vec(i),'&');

                    elseif  vec(i) == 0
                        fprintf(fid,'%6.1f %s',vec(i),'&');
                    end
                else
                    fprintf(fid,'     %s %s','NaN','&');
                end
             end
             fprintf(fid,'%s','\\');
        else
            fprintf(1,'Probleme de format a l''appel de ecr_vec ');
            erreur=1;
            return
        end


        %% si ligne de Caracteres
    elseif ischar('vec(1)')
        for i=1:length(vec)
            if length(vec{i})<10
                n=10-length(vec{i});
                format=['%' num2str(n) 'c%s  '];
                fprintf(fid,format,' ',vec{i});
            elseif length(vec{i}) == 10
                fprintf(fid,'%s  ',vec{i});
            elseif  length(vec{i}) > 10
                fprintf(fid,'%s%s  ',vec{i}(1:8),'..');
            end
        end  
    end
    if i_ligne~=Nb_ligne 
        fprintf(fid,'\r\n','   ');
    end
end

if ~strcmp(Nom_mat,' ')
    fprintf(fid,'%s','];');
else
    fprintf(fid,'\r\n','   ');
end

if ~strcmp(commentaire,' ')
fprintf(fid,'  %s\n',commentaire);
end
