function cpFiles(srcdir, dstdir, extension)
% function cpFiles(srcdir, dstdir, extension)
% Recusively copy of directories (preserving tree structure).
% If extension is not given, only tree structure (directories) will be copied.
% If extension is "*", everything will be copied.
% If extension is ".jpg", only .jpg files will be copied.
% TODO: accents in files compatibility
% 
% parametres:
% - srcdir: [string] nom du repertoire a copier (absolu ou relatif)
% - dstdir: [string] nom du repertoire destination (absolu ou relatif)
% - extension: [optionnel, string] extension des fichiers a copier
% examples:
% (1) cpFiles('c:\Folder1', 'c:\Folder2', '.txt') : copie tous les fichiers txt
% de Folder1 a Folder2 y compris toute l'arborescence de fichiers
% (2) cpFiles('c:\Folder1', 'c:\Folder2', '*') : copie tous les fichiers
% de Folder1 a Folder2 y compris toute l'arborescence de fichiers
% (3) cpFiles('c:\Folder1', 'c:\Folder2', '') : copie toute l'arborescence 
% de Folder1 a Folder2 (seulement repertoires)
% (4) cpFiles('c:\Folder1', 'c:\Folder2') : Idem de (3)
% 
%   See also lsFiles, cpFilesNOW.

if ~exist('extension','var')
    extension = '';
end

% #pas compatible avec des noms avec accents
if ~exist(dstdir,'dir')
    mkdir(dstdir);
end
D = dir(srcdir);
%filtrer fichiers caches (commencant par point)
fileList = {D.name}';
indices = cellfun(@(x) x(1)~='.',fileList);
% fileList = fileList(indices);
D = D(indices);

for ind = 1:length(D)
    srcname = fullfile(srcdir,D(ind).name);
    if ~D(ind).isdir
        [Folder File Ext] =fileparts(srcname);
        if ~isempty(extension)
            if isequal(Ext,extension) || isequal(extension,'*')
                copyfile(srcname,dstdir);
            end
        end
    else
        dstname = fullfile(dstdir,D(ind).name);
        cpFiles(srcname,dstname,extension);
    end
end
