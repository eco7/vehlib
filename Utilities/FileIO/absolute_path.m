function abs_path = absolute_path(path_name)
% function absolute_path(path_name)
% Cleanup path_name and makes it absolute if it is a relative path

%0 clean double filesep '//' in unix, '\\' in windows
while contains(path_name,[filesep filesep])
    path_name = strrep(path_name,[filesep filesep],filesep);
end

%1 check if it is a relative or an absolute path
if isunix
    if path_name(1)=='~'
        [~,B] = dos('echo $HOME');
        path_name = regexprep(path_name,'^~',strtrim(B));
    end
    % FIXME error if isempty path_name
    isrelative = path_name(1) ~= filesep;
else
    % TODO check in Windows
    % FIXME error if length path_name<2
    isrelative = path_name(2)~=':';
end

%2 if is relative, add current path
if isrelative% path_name is a relative path
%     path_name = [pwd filesep path_name];
    path_name = fullfile(pwd,path_name);
    
end

%3 split into parts
D = regexp(path_name,filesep,'split');

%4 cleanup empty folders:
% D{1} in unix absolute paths always start with filesep
% D{end} if path is finished with filesep
Is = cellfun(@(x) ~isempty(x),D);
D = D(Is);

%5 cleanup parent folders: '..'
while ismember('..',D)
    [~, ~, Is, ~] = regexpFiltre(D, '^\.\.$');
    ind = find(Is,1);%find index for element '..'
    D = [D(1:ind-2) D(ind+1:end)];%remove element '..' and its predecesor
end
%cleanup current folders: '.'
[~,D] = regexpFiltre(D,'^\.$');
%6 concatenate:
abs_path = [];
for ind = 1:length(D)
    abs_path = [abs_path filesep D{ind}];
end
if ispc%remove first filesep in windows
    abs_path = abs_path(2:end);
end

if ~exist(abs_path)
    error('absolute_path: folder does not exist')
end
end
