function rmEmptyFolders(dirbase)
% function rmEmptyFolders(dirbase)
% nettoye les repertoires vides
% si on fait mkSIMCALTree(dirbase), on reconstruit l'arborescence complete
%
%   See also lsFiles, lsEmptyFolders, mkSIMCALTree.

emptyFolders = lsEmptyFolders(dirbase);
while(~isempty(emptyFolders))
    fprintf('Found %d empty folders: ',length(emptyFolders))
    for ind = 1:length(emptyFolders)
        rmdir(emptyFolders{ind});
    end
    fprintf('ERASED\n')
    emptyFolders = lsEmptyFolders(dirbase);
end
fprintf('FINI\n')
end