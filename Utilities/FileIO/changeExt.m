function changeExt(srcdir, oldext, newext)
% changeExt Recusively change file extension.
% changeExt(srcdir, oldext, newext)
% parametres:
% - srcdir: [string] nom du repertoire a copier (absolu ou relatif)
% - oldext: [string] extension des fichiers a copier
% - newext: [string] nouvelle extension
% Example (1):
% changeExt('images', '.jpg','.bmp') : renomme tous les fichiers jpg
% de images en bmp
% 
%   See also lsFiles, cpFiles, cpFilesNOW.

oldList = lsFiles(srcdir,oldext);
newList = regexprep(oldList,[oldext '$'],newext);
for ind = 1:length(oldList)
    movefile(oldList{ind},newList{ind})
end

end