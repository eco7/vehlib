function emptyList = lsEmptyFolders(srcdir)
% function emptyList = lsEmptyFolders(srcdir)
% Recusively list empty folders in a tree.
%
%   See also lsFiles, lsDirsWithFiles, rmEmptyFolders.

D = dir(srcdir);
%filtrer fichiers caches (commencant par point)
fileList = {D.name}';
indices = cellfun(@(x) x(1)~='.',fileList);
fileList = fileList(indices);
D = D(indices);

if isempty(D)
%     fprintf('%s\n',srcdir);
    emptyList = {srcdir};
else
    emptyList = cell(0);
end
for ind = 1:length(D)
    filename = fileList{ind};
    pathname = fullfile(srcdir,filename);
    if D(ind).isdir
        oldList = lsEmptyFolders(pathname);
        emptyList = [emptyList oldList];
    end
end

end
    