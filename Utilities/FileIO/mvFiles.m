function mvFiles(srcdir, dstdir, extension)
% function mvFiles(srcdir, dstdir, extension)
% Recusively move of directories (preserving tree structure).
% If extension is not given, only tree structure (directories) will be moved.
% If extension is "*", everything will be moved.
% If extension is ".jpg", only .jpg files will be moved.
% TODO: accents in files compatibility
% 
% parametres:
% - srcdir: [string] nom du repertoire a deplacer (absolu ou relatif)
% - dstdir: [string] nom du repertoire destination (absolu ou relatif)
% - extension: [optionnel, string] extension des fichiers a deplacer
% examples:
% (1) mvFiles('c:\Folder1', 'c:\Folder2', '.txt') : deplace tous les fichiers txt
% de Folder1 a Folder2 y compris toute l'arborescence de fichiers
% (2) mvFiles('c:\Folder1', 'c:\Folder2', '*') : deplace tous les fichiers
% de Folder1 a Folder2 y compris toute l'arborescence de fichiers
% (3) mvFiles('c:\Folder1', 'c:\Folder2', '') : copie toute l'arborescence 
% de Folder1 a Folder2 (seulement repertoires)
% (4) mvFiles('c:\Folder1', 'c:\Folder2') : Idem de (3)
% 
%   See also lsFiles, cpFilesNOW, cpFiles.

if ~exist('extension','var')
    extension = '';
end

% #pas compatible avec des noms avec accents
if ~exist(dstdir,'dir')
    mkdir(dstdir);
end
D = dir(srcdir);
%filtrer fichiers caches (commencant par point)
fileList = {D.name}';
indices = cellfun(@(x) x(1)~='.',fileList);
% fileList = fileList(indices);
D = D(indices);

for ind = 1:length(D)
    srcname = fullfile(srcdir,D(ind).name);
    if ~D(ind).isdir
        [Folder File Ext] =fileparts(srcname);
        if ~isempty(extension)
            if isequal(Ext,extension) || isequal(extension,'*')
                movefile(srcname,dstdir);
            end
        end
    else
        dstname = fullfile(dstdir,D(ind).name);
        mvFiles(srcname,dstname,extension);
    end
end
