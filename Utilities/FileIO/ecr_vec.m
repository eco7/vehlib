% -------------------------------------------------------------------------
% function []= ecr_vec(vec,Nom_vec,taille_nom_vec,fid,form,commentaire)
%
% Fonction ecriture d'une valeur dans un vecteur
% interface : vec=vecteur des valeurs a �crire
%             Nom_vec=Nom du vecteur dans la fichier d'�criture
%             taille_nom_vec= taille max du nom du vecteur
%             fid=numero du fichier d'�criture
%             form=format d'�criture pour l'instant (6.3f ou E)
%
% Exemple d'appel
% ecr_vec(E_moth_i,strcat('E_moth_i',type),t_nom,fid10,'6.3f','% Energie fournie par le moteur thermique issue mesure PMI en Joules')
%
% avril 2006
% -------------------------------------------------------------------------
function [erreur]= ecr_vec(vec,Nom_vec,taille_nom_vec,fid,form,commentaire)

erreur=0;

if length(Nom_vec)<taille_nom_vec
    n=taille_nom_vec-length(Nom_vec);
    format=['%s%' num2str(n) 'c'];
    fprintf(fid,format,Nom_vec,' ');
elseif length(Nom_vec)==taille_nom_vec
    fprintf(fid,'%s',Nom_vec);
else
    fprintf(fid,'%s',Nom_vec);
end

fprintf(fid,'%s',' = [');

%% format 6 chiffres caract�ristique
if isnumeric(vec(1))
    if strcmp(form,'6.3f')
        fprintf(fid,'%s','  ');
        for i=1:length(vec)
            if isnan(vec(i)) ~= 1
                if vec(i)>0 & vec(i)<0.01
                    fprintf(fid,' %2.1E %s',vec(i),'   ');
                elseif vec(i)>0 & vec(i)<10
                    fprintf(fid,'  %6.3f %s',vec(i),'   ');
                elseif  vec(i)>0 & vec(i)<100
                    fprintf(fid,'  %6.3f %s',vec(i),'   ');
                elseif  vec(i)>0 & vec(i)<1000
                    fprintf(fid,' %6.3f %s',vec(i),'   ');
                elseif  vec(i)>0 & vec(i)<10000
                    fprintf(fid,' %6.2f %s',vec(i),'   ');
                elseif  vec(i)>0 & vec(i)<100000
                    fprintf(fid,' %6.1f %s',vec(i),'   ');
                elseif  vec(i)>0 & vec(i)<1000000
                    fprintf(fid,'  %6.0f %s',vec(i),'   ');
                elseif  vec(i)>1000000
                    fprintf(fid,' %2.1E %s',vec(i),'   ');

                elseif vec(i)<0 & vec(i)>-0.01
                    fprintf(fid,'%2.1E %s',vec(i),'   ');
                elseif vec(i)<0 & vec(i)>-10
                    fprintf(fid,'  %6.3f %s',vec(i),'   ');
                elseif  vec(i)<0 & vec(i)>-100
                    fprintf(fid,' %6.3f %s',vec(i),'   ');
                elseif  vec(i)<0 & vec(i)>-1000
                    fprintf(fid,'%6.3f %s',vec(i),'   ');
                elseif  vec(i)<0 & vec(i)>-10000
                    fprintf(fid,'%6.2f %s',vec(i),'   ');
                elseif  vec(i)<0 & vec(i)>-100000
                    fprintf(fid,'%6.1f %s',vec(i),'   ');
                elseif  vec(i)<0 & vec(i)>-1000000
                    fprintf(fid,' %6.0f %s',vec(i),'   ');
                elseif  vec(i)<-1000000
                    fprintf(fid,'%2.1E %s',vec(i),'   ');

                elseif  vec(i) == 0
                    fprintf(fid,'  %6.1f %s',vec(i),'   ');
                end
            else
                fprintf(fid,'     %s %s','NaN','   ');
            end
        end
        fprintf(fid,'%s','];');

        %% format scientifique 2 chiffres apr�s virgule
    elseif strcmp(form,'E')
        fprintf(fid,'%s',' ');
        for i=1:length(vec)
            if vec(i)>=0
                fprintf(fid,' %2.2E %s',vec(i),'  ');
            elseif vec(i)<0
                fprintf(fid,'%2.2E %s',vec(i),'  ');
            end
        end
        fprintf(fid,'%s',' ];');

    else
        fprintf(1,'Probleme de format a l''appel de ecr_vec ');
        erreur=1;
        return
    end


    %% si vecteur Caractere
elseif ischar('vec(1)')
    for i=1:length(vec)
        if length(vec{i})<10
            n=10-length(vec{i});
            format=['%' num2str(n) 'c%s  '];
            fprintf(fid,format,' ',vec{i});
        elseif length(vec{i}) == 10
            fprintf(fid,'%s  ',vec{i});
        elseif  length(vec{i}) > 10
            fprintf(fid,'%s%s  ',vec{i}(1:8),'..');
        end
    end
    fprintf(fid,'%s','  ];');
end

fprintf(fid,'  %s\n',commentaire);
