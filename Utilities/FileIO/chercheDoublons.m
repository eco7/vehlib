function resultat = chercheDoublons(srcdir,extension, inPath)
% function out = chercheDoublons(srcdir,extension)
% cherche des fichier en double (meme nom)
% si extension non renseigne, par defaut: '.m'
% inPath: si true (par defaut) et extension == '.m' filtre les resultats
% aux repertoires qui sont dans le path
%
%   See also lsFiles.

%1.-verification des entrees
if ~exist('srcdir','var')
    fprintf('ERROR: you must provide srcdir parameter\n');
    return;
end
if ~exist('extension','var')
    extension = '.m';
end
if ~exist('inPath','var')
    if strcmp(extension,'.m')
        inPath = true;
    else
        inPath = false;
    end
end

if ~ischar(srcdir)
    fprintf('ERROR: srcdir must be a string\n');
    return;
end

if ~exist(srcdir,'dir')
    fprintf('ERROR: could not find srcdir:%s\n',srcdir);
    return;
end

%2.- lister les fichiers
fileList = lsFiles(srcdir,extension);
[D F E] = cellfun(@fileparts,fileList,'UniformOutput',false);
%trier par nom de fichier
[F,indices] = sort(F);
fileList = fileList(indices);
D = D(indices);
E = E(indices);

if inPath
    %2.-bis: filtrer ceux qui ne sont pas dans le path
    P = path;
    if isunix
        P = regexp(P,':','split');%TODO compatibilite avec Windows
    else
        fprintf('Tu es sur Windows, parle à Eduardo de ton problème\n')
    end
    indices = ismember(D,P);
    D = D(indices);
    F = F(indices);
    E = E(indices);
end
%3.- chercher les doublons
Fu = unique(F);
if length(Fu)<length(F)
    %methode1
    doublons = cell(size(Fu));
    for ind = 1:length(Fu)
        doublons{ind} = find(strcmp(Fu{ind},F));
    end
    %on ne prend que s'il y a plusieurs
    indices = cellfun(@(x) length(x)>1,doublons);
    doublons = doublons(indices);
    
    %methode2: plus complexe et finalement ca va pas plus vite
%     doublons= cell(0);
%     newDoublon = true;
%     for ind =2:length(F)
%         if strcmp(F{ind},F{ind-1})%il y a un doublon
%             if newDoublon%il y en avait pas
%                 doublons{end+1} = [ind-1 ind];
%                 newDoublon = false;
%             else
%                 doublons{end}(end+1) = ind;
%             end
%         else
%             newDoublon = true;
%         end
%     end
    
    resultat = struct;
    
    for ind = 1:length(doublons)
        resultat(ind).fileName = F{doublons{ind}(1)};
        resultat(ind).pathName = fileList(doublons{ind});
    end
else
    resultat = [];
end

%ecrit rapport
fid = fopen('listeDoublons.txt','w+');
for ind = 1:length(resultat)
fprintf(fid,'%s\t',resultat(ind).fileName);
for indD = 1:length(resultat(ind).pathName)
fprintf(fid,'%s\t',resultat(ind).pathName{indD});
end
fprintf(fid,'\n');
end
fclose(fid);
end

