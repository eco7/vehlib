function cpFilesNOW(srcdir, dstdir, extension)
% function cpFilesNOW(srcdir, dstdir, extension)
% Same as cpFiles but without overwritting existing files
% 
%   See also cpFiles, lsFiles.

if ~exist('extension','var')
    extension = '';
end

% #pas compatible avec des noms avec accents
if ~exist(dstdir,'dir')
    mkdir(dstdir);
end
D = dir(srcdir);
%filtrer fichiers caches (commencant par point)
fileList = {D.name}';
indices = cellfun(@(x) x(1)~='.',fileList);
% fileList = fileList(indices);
D = D(indices);

for ind = 1:length(D)
    srcname = fullfile(srcdir,D(ind).name);
    if ~D(ind).isdir
        [Folder File Ext] =fileparts(srcname);
        if ~isempty(extension)
            if isequal(Ext,extension) || isequal(extension,'*')
				dstname = fullfile(dstdir,sprintf('%s%s',File,Ext))
				if ~exist(dstname,'file')
					copyfile(srcname,dstdir);
				end 
            end
        end
    else
        dstname = fullfile(dstdir,D(ind).name);
        cpFiles(srcname,dstname,extension);
    end
end
