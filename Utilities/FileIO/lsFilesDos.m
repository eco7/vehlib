function fileList = lsFilesDos(srcdir, extension, toponly)
% function fileList = lsFilesDos(srcdir, extension, toponly)
% Same as lsFiles but using faster dos command (Windows)
%
%   See also lsFiles.
if ~exist('extension','var')
    extension = '';
end
if ~exist('toponly','var')
    toponly = false;
end
fileFilter = fullfile(srcdir,sprintf('*%s',extension));
if toponly
    myCommand = sprintf('dir /b %s',fileFilter);
else
    myCommand = sprintf('dir /s /b %s',fileFilter);
end
[status,result] = dos(myCommand);
fileList = regexp(result,'\n','split')';
indices = cellfun(@(x) ~isempty(x),fileList);
fileList = fileList(indices);
end