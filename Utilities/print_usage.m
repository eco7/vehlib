function print_usage()
% print_usage
%   print_usage displays help for caller function
%   typically, put this in starting part of your functions:
%
%   function myFun(arg1,arg2,...)
%   % Help for function myFun
%   % Usage of myFun
%   % Etc.
%   if nargin==0)
%       print_usage
%   end
%   ...
%   Your code here
%   ...
%
%   See also dbstack
%       http://fr.mathworks.com/help/matlab/matlab_prog/comments.html
%       http://fr.mathworks.com/help/matlab/matlab_prog/add-help-for-your-program.html
%
%   IFSTTAR/LTE  - E. REDONDO
%   $Revision: 0.1 $  $Created: 2015/08/12, Modified: 2015/08/12$
[ST,I] = dbstack;
if length(ST)>1
    help(ST(2).name);
else
    print_usage;
end

end