%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Script pour lancer les calculs backward en boucle
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR]=auto_backward_VELEC_BATT_SC

% Lecture du fichier parametres et renseignement de la structure

fichier_param='param_backward_ligne';
cinem='CIN_ECE15';
architecture='VELEC_BAT_SC';
vehicule='AX_ELE_BAT_SC_DCDCVD.SC';
VEHLIB_backward(cinem,vehicule,architecture,fichier_param,1,1);

% Initialisation d'un code d'exception pour gerer les erreurs
ERR=[];

%nbloc=25:25:300;
%nbloc=[0 nbloc];
%nbloc=[ 0 1 2 5 10 15 20 25 30 35 40 45 50];
nbloc=[0 25];
%nbloc=5;

for nbcal=1:length(nbloc)
    
    evalin('base','VD.SC.Nbranchepar=1;');
    evalin('base',['VD.SC.Nblocser=',num2str(nbloc(nbcal)),';']);
    
   if nbloc(nbcal)==0 
       evalin('base','param_backward_ligne;');
       evalin('base','param.strat=0;');
       nbloc(nbcal)=1;
       evalin('base',['VD.SC.Nblocser=',num2str(nbloc(nbcal)),';']);
   else
       evalin('base',fichier_param);
   end
  
    % Script de lancement des calculs
    VEHLIB_backward(cinem,vehicule,architecture,fichier_param,0);

    % Resultats
    Ieff_bat(nbcal)=evalin('base','sqrt( trapz(VD.CYCL.temps,ibat.*ibat)/(VD.CYCL.temps(end)-VD.CYCL.temps(1)) );');
    NRJ_sc(nbcal)=evalin('base','0.5*VD.SC.C*VD.SC.Nbranchepar/VD.SC.Nblocser*(u0sc(1)^2-u0sc(end)^2)/3600;');
    NRJ_bat(nbcal)=evalin('base','trapz(VD.CYCL.temps,ibat.*u0bat)/3600;');
    NRJ_Pjsc(nbcal)=evalin('base','trapz(VD.CYCL.temps,Pjsc)/3600;');   
    NRJ_Pjbat(nbcal)=evalin('base','trapz(VD.CYCL.temps,Pjbat)/3600;');   
    
   fprintf(1,'%s %d\n','Nombre de super capacité dans le calcul:  ',evalin('base','VD.SC.Nbranchepar*VD.SC.Nblocser;'));
   fprintf(1,'%s %.1f %s\n','Le courant efficace de la batterie : ',Ieff_bat(nbcal),'A');
   fprintf(1,'%s %.1f %s\n','L''energie fournie par les ucap : ',NRJ_sc(nbcal),'Wh');
   fprintf(1,'%s %.1f %s\n','L''energie fournie par la batterie : ',NRJ_bat(nbcal),'Wh');
end
subplot(2,1,1)
nbloc(1)=0;
plot(nbloc,NRJ_bat)
title(strrep(['Cycle : ',cinem,' Critere : min(Ieff_bat)'],'_','\_'));
grid on
ylabel('Energie batterie en Wh')
subplot(2,1,2)
plot(nbloc,Ieff_bat)
grid on
xlabel('Nombre de super capacites')
ylabel('Courant efficace batterie')
