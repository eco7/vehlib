% function [ERR, vehlib, VD, ResXml]=calc_backward(ERR,vehlib,param,VD)
%
% 
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Lancement des simulations backward
%
%
function [ERR, vehlib, VD, ResXml,r]=calc_backward(ERR,vehlib,param,VD)
ERR=[];
r=[];

switch vehlib.architecture
    case 'VTH_BV'
        if isfield(param,'optim') && strcmpi(param.optim, '2U_TWC')
            [ERR, VD, ResXml]=calc_VTH_BV_2U_TWC(ERR,vehlib,param,VD);
        else
            [ERR, VD, ResXml]=calc_VTH_BV(ERR,vehlib,param,VD);            
        end
    case 'VTH_CPLHYD'
        [ERR, VD, ResXml]=calc_VTH_CPLHYD(ERR,vehlib,param,VD);
    case 'VELEC'
        [ERR, VD, ResXml]=calc_VELEC_BAT(ERR,vehlib,param,VD);
    case 'VELEC_BAT_SC'
        [ERR, VD, ResXml]=calc_VELEC_BAT_SC(ERR,vehlib,param,VD);
    case 'HP_BV_2EMB'
        if isfield(param,'optim') && strcmpi(param.optim, 'lagrange_TWC')
            [ERR, VD, ResXml]=calc_HP_BV_2EMB_TWC(ERR,vehlib,param,VD);
        elseif isfield(param,'optim') && strcmpi(param.optim, 'analytique')
            [ERR, VD, ResXml]=calc_HP_BV_2EMB_analytique(ERR,vehlib,param,VD);
        elseif isfield(param,'optim') && strcmpi(param.optim, '3U_TWC')
            [ERR, VD, ResXml]=calc_HP_BV_2EMB_3U_TWC(ERR,vehlib,param,VD);            
        else
            [ERR, VD, ResXml,r]=calc_HP_BV_2EMB(ERR,vehlib,param,VD);
        end
    case 'HP_BV_1EMB'
            [ERR, VD, ResXml]=calc_HP_BV_1EMB(ERR,vehlib,param,VD);
    case 'HSER_GE'
        [ERR, VD, ResXml]=calc_HSER_GE(ERR,vehlib,param,VD);
    case 'HSER_GE_Pres' % Si la "mission" est directement en puissance réseau
        [ERR, VD, ResXml]=calc_HSER_GE_Pres(ERR,vehlib,param,VD);
    case 'HP_BV_2EMB_SC'
        [ERR, VD, ResXml]=calc_HP_BV_2EMB_SC(ERR,vehlib,param,VD);
    case 'HSER_PAC'
        if isfield(VD,'PAC_PILE') && VD.PAC_PILE.ntyppac==20
            % PAC CEA - Deplace dans le projet utilisateurs/CEA
            ERR=MException('BackwardModel:VEHLIB_backward','Architecture obsolete !');
            ResXml=struct([]);
            %evalin('base','[ERR,CYCL,dcarb_h2,ah,soc]=calc_HSER_PAC_CEA(ERR,vehlib,param,INIT,CYCL,ECU,VEHI,RED,ACC,ACM1,BATT,DCDC_PAC,PAC_PILE);');
        else
            %ERR=MException('BackwardModel:VEHLIB_backward','Architecture a coder !');
            ResXml=struct([]);
            [ERR, VD, ResXml]=calc_HSER_PAC(ERR,vehlib,param,VD);
        end
    case 'HP_VELO_PAC'
        [ERR, VD, ResXml,r]=calc_HP_VELO_PAC(ERR,vehlib,param,VD);
    case 'HPDP_EPI_SURV'
        [ERR, VD, ResXml,r]=calc_HPDP_EPI_SURV(ERR,vehlib,param,VD);
    case 'HPDP_EVT_SURV'
        [ERR, VD, ResXml]=calc_HPDP_EVT_SURV(ERR,vehlib,param,VD);
    case 'HPDP_EVT_SURV_RED'
        [ERR, VD, ResXml]=calc_HPDP_EVT_SURV_RED(ERR,vehlib,param,VD);
    case 'HSP_2EMB'
        [ERR, VD, ResXml]=calc_HSP_2EMB(ERR,vehlib,param,VD);
    case 'HSP_BV_2EMB'
        [ERR, VD, ResXml]=calc_HSP_BV_2EMB(ERR,vehlib,param,VD);
    case 'HSP_BV_2EMB_CPL'
        [ERR, VD, ResXml,r]=calc_HSP_BV_2EMB_CPL(ERR,vehlib,param,VD);
    case 'HSP_PLR_2EMB_CPL'
        [ERR, VD, ResXml]=calc_HSP_PLR_2EMB_CPL(ERR,vehlib,param,VD);  
    case 'HSP_PLR_BV'
        [ERR, VD, ResXml]=calc_HSP_PLR_BV(ERR,vehlib,param,VD);  
    case 'HSP_1EMB'
        [ERR, VD, ResXml]=calc_HSP_1EMB(ERR,vehlib,param,VD);
    % case 'HSP_1EMB_RED'
        % [ERR, VD, ResXml]=calc_HSP_1EMB_RED(ERR,vehlib,param,VD);
    case 'HSDP_EPI'
        [ERR, VD, ResXml]=calc_HSDP_EPI(ERR,vehlib,param,VD);
    case 'GENERAL'
        [ERR, VD, ResXml]=calc_GENERAL(ERR,vehlib,param,VD);   
    otherwise
        chaine=evalin('base','strcat(''Architecture :'',vehlib.architecture,'' non reconnue'');');
        ERR=MException('BackwardModel:backward',chaine);
        ResXml=struct([]);
end

end
