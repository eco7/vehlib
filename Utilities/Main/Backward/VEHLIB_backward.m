%function [ERR, vehlib, VD, ResXml]=VEHLIB_backward(cinem,vehlib_f,architecture,fichier_param,lect_mod_param,lect_mod_veh,DoD0,DoDSC0)
%
% 
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Lecture des donnees et lancement des simulations backward
%
% Interface entre
% cinem : cinematique 
% vehlib_f : nom vehicule
% architecture : architecture vehicule
% fichier_param : fichier des parametre pour la gestion d'energie
% lect_mod_param : permet de charger ou nom les param vehicule dans le worspace
%            0 : Pas de chargement parametre
%            1 : chargement parametre vehicule et gestion
% DoD0 : profondeur de decharge initiale, si pas d'argument en entree mise a 40 par defaut
% lect_mod_veh = (voir vehsim_auto2) entier permettant de 
%               - relire le modele sans la cinematique et sans nettoyage du Workspace matlab (3) 
%               - relire le modele sans nettoyage du Workspace matlab (2) 
%               - relire le modele avec nettoyage du Workspace matlab (1)
%               - relire la cinematique uniquement (0)
%               - ne pas appeler vehsim_auto2 (-1)
%
%
% TODO : 
% - lect_mod_param et lect_mod_veh semblent obsoletes ... Serait a
%   supprimer egalement dans vehsim_auto2. A VOIR DANS UNE VERSION ULTERIEURE
%
% - La structure param devrait etre renvoyee par VEHLIB_backward et ses
%   sous-programmes (calcul_backward et tous les calc_ARCH) Puisqu'ils ne
%   sont plus présents dans le workspace. Pourrait aussi etre dans la
%   structure VD et le nom du script parametre dans vehlib ?
%
%
%
% Examples : 
% --------
% Dual battery - Lagrange
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ECE15','AX_ELE_BAT_SC_DCDCSC','VELEC_BAT_SC','param_backward_lagrange',1,1,40);
% Run conventional vehicle
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ECE15','CLIO_1L5DCI','VTH_BV','param_backward',1,1);
% Run series hybrid - Dynamic programming
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC','Peugeot_308_HYBS_PVAR','HSER_GE','param_backward_prog_dyn_HSER_GE',1,1,40);
%
% 26-08-2011 (BJ-EV) : version 1

function [ERR, vehlib, VD, ResXml,r]=VEHLIB_backward(cinem,vehlib_f,architecture,fichier_param,lect_mod_param,lect_mod_veh,DoD0,DoDSC0)
ERR=[];
evalin('base','ERR=[ ];');

% Increment du numero de calcul
numcal_increment;

if lect_mod_veh ~= -1
    if nargin==6
        [vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,40,lect_mod_veh,0);
    elseif nargin==7
        [vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,DoD0,lect_mod_veh,0);
    elseif nargin==8
        [vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,DoD0,lect_mod_veh,0);
        assignin('base','DoDSC0',DoDSC0);
        %evalin('base','SC.Vc0=(100-DoDSC0)/100*SC.Tension;');
        %if evalin('base','SC.Vc0 < SC.minTensionAdmissible==1')
        %    'attention tension ini UC < min tension admissible'
        %end
    end
end

% lecture fichier parametres
if lect_mod_param==1
    eval(fichier_param);
end

evalin('base','ERR=[ ];');

% Lancement des calculs en mode backward
[ERR, vehlib, VD, ResXml,r]=calc_backward(ERR,vehlib,param,VD);

end

