% Function: vehlib_liste
% Creation de la liste des vehicules disponibles dans vehlib pour une architecture donnee
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
%
%  Fonction appelee par l'interface homme-machine <vehsim>, dont l'objet
%  est:
%
%  la creation de la liste des architectures disponibles,
%  
%  la creation de la liste des vehicule repertories pour l'architecture 'architecture',
%
%  la creation d'une zone de texte decrivant l'architecture.
%
% Argument d'appel:
%
% architecture: architecture du vehicule.
%
% correspond au champ vehlib.architecture
%
% Argument de retour:
%
% liste_arch : cell_array de la liste des architectures de vehlib
%
% liste_veh : celle_array de la liste des vehicules disponibles pour
% l'architecture 'architecture'.
%
% texte: chaine de caractere decrivant l'architecture 'architecture'.
%
% ---------------------------------------------------------
function [liste_arch, liste_veh, texte]=vehlib_liste(architecture)

% creation de la liste des architectures disponibles dans vehlib.
n=1;
liste_arch(n)={'VTH_BV';};
n=n+1;
liste_arch(n)={'VELEC';};
n=n+1;
liste_arch(n)={'HPDP_EPI';};
n=n+1;
liste_arch(n)={'HP_BV_1EMB';};
n=n+1;
liste_arch(n)={'HP_BV_2EMB';};
n=n+1;
liste_arch(n)={'VTH_CPLHYD';};
n=n+1;
liste_arch(n)={'HSER_GE';};
n=n+1;
liste_arch(n)={'UTILITAIRES';};
n=n+1;
liste_arch(n)={'VELEC_BAT_SC';};
n=n+1;
liste_arch(n)={'UTILITAIRES_BATTERIE';};

% Initialisation
liste_veh={''};
texte='';

if strcmp(architecture,'VELEC')
   texte='Vehicule tout electrique sur batterie';
   n=1;
   liste_veh(n)={'AX_ELE';};
   n=n+1;
   liste_veh(n)={'AIXAM_E_CITY';};
   n=n+1;
   liste_veh(n)={'Citroen_C0';};
   n=n+1;
   liste_veh(n)={'Citroen_C0_DRIVER';};
elseif strcmp(architecture,'HPDP_EPI')
   texte='Vehicule hybride parallele a derivation de puissance avec train epicycloidal et batterie';
   n=1;
   liste_veh(n)={'PRIUS_JAPON';};
    n=n+1;
    liste_veh(n)={'PRIUS_II';};
elseif strcmp(architecture,'VELEC_BAT_SC')
    texte='Vehicule electrique batterie et supercondensateur';
    n=1;
    liste_veh(n)={'AX_ELE_BAT_SC_DCDCSC';};
elseif strcmp(architecture,'VTH_BV')
   texte='Vehicule thermique avec embrayage et boite de vitesse';
   n=1;
   liste_veh(n)={'D306';};
   n=n+1;
   liste_veh(n)={'D306_hdi';};
   n=n+1;
   liste_veh(n)={'CLIO_1L5DCI';};
   n=n+1;
   liste_veh(n)={'CLIO_1L5DCI_perfos';};
   n=n+1;
   liste_veh(n)={'ALFA_ROMEO_159';};
   n=n+1;
   liste_veh(n)={'Peugeot_308SW_1L6VTI16V';};
   n=n+1;
   liste_veh(n)={'Peugeot_308SW_1L6VTI16V_DRIVER';};
   n=n+1;
   liste_veh(n)={'Peugeot_308SW_1L6VTI16V_TWC';};
   n=n+1;
   liste_veh(n)={'Peugeot_308SW_1L6VTI16V_Pacejka';};
   n=n+1;
   liste_veh(n)={'XANTIA';};
   n=n+1;
   liste_veh(n)={'CLIO_III_1L2Turbo';};
   n=n+1;
   liste_veh(n)={'Renault_Kadjar_TCE130';};  
   n=n+1;
   liste_veh(n)={'Renault_Kadjar_TCE130_CONTROL';};  
elseif strcmp(architecture,'HP_BV_1EMB')
   texte='Vehicule hybride parallele avec un embrayage et batterie';
   n=1;
   liste_veh(n)={'CLIO_1L5DCI_HP_BV_1EMB_UQM';};
   n=n+1;
   liste_veh(n)={'Peugeot_308SW_1L6VTI16V_HP_BV_1EMB';};
   n=n+1;
   liste_veh(n)={'Peugeot_308SW_1L6VTI16V_HP_BV_1EMB_TWC';};
elseif strcmp(architecture,'HP_BV_2EMB')
   texte='Vehicule hybride parallele avec deux embrayages et batterie';
   n=1;
   liste_veh(n)={'CLIO_1L5DCI_HP_BV_2EMB_UQM';};
   n=n+1;
   liste_veh(n)={'Renault_Kadjar_TCE130_HP_BV_2EMB_CONTROL';};
   n=n+1;
   liste_veh(n)={'Renault_Kadjar_TCE130_HP_BV_2EMB_CONTROL_NN';};
elseif strcmp(architecture,'HP_BV_2EMB_SC')
   texte='Vehicule hybride parallele avec deux embrayages et supercondensateur';
   n=1;
   liste_veh(n)={'CLIO_1L5DCI_HP_BV_2EMB_NES_SC';};
elseif strcmp(architecture,'VTH_CPLHYD')
   texte='Vehicule thermique avec coupleur hydraulique et boite de vitesse';
   n=1;
   liste_veh(n)={'BUS_CPLHYD';};
elseif strcmp(architecture,'HSER_GE')
   texte='Vehicule hybride serie avec groupe electrogene et batterie';
   n=1;
%    liste_veh(n)={'D306_HYBS_PVAR';};
%    n=n+1;
%    liste_veh(n)={'D306_HYBS_PCST';};
%    n=n+1;
%    liste_veh(n)={'D306_HYBS_STOPSTART';};
%    n=n+1;
%    liste_veh(n)={'D306_HYBS_PTHER';};
%    n=n+1;
%    liste_veh(n)={'D306_HYBS_DoDCIBLE';};
%    n=n+1;
   liste_veh(n)={'Peugeot_308_HYBS_PVAR';};
   n=n+1;
   liste_veh(n)={'Peugeot_308_HYBS_PCST';};
   n=n+1;
   liste_veh(n)={'Peugeot_308_HYBS_PTHER';};
   n=n+1;
   liste_veh(n)={'Peugeot_308_HYBS_STOPSTART';};
   n=n+1;
   liste_veh(n)={'Peugeot_308_HYBS_DoDCIBLE';};
   n=n+1;
   liste_veh(n)={'Peugeot_308_HYBS_Ligne';};
elseif strcmp(architecture,'HP_BV_1EMB_DA')
   texte='Vehicule hybride parallele un embrayage double arbre (i.e. les arbres peuvent etre decouples)';
    n=1;
    liste_veh(n)={'BERLINGO_HP_BV_1EMB_DA_TU3_GEN';};
    
elseif strcmp(architecture,'UTILITAIRES')
   texte='Utilitaires de VEHLIB';
   n=1;
   liste_veh(n)={'VEH_CLIO';};
elseif strcmp(architecture,'UTILITAIRES_BATTERIE')
   texte='Utilitaires Batterie de VEHLIB';
   n=1;
   liste_veh(n)={'CHARGEUR_AX';};
end

% Appel des scripts specifiques aux repertoires des utilisateurs
% sous utilisateurs/XXX/utilitaires/vehlib_liste_XXX.m)
eval('initdata;');

rep=lsFiles(INIT.UsersFolder,'',true, true);
P=path;
for ind=1:length(rep)
    [~,dossier]=fileparts(rep{ind});
    if ~strcmp(dossier,'templates')
        % le repertoire templates est un template !
        file=strcat('vehlib_liste_',dossier,'.m');
        try
            chaine=['[liste_arch, liste_veh,texte]=',file(1:length(file)-2),'(architecture,liste_arch,liste_veh,texte);'];
            eval(chaine);
        catch ME
%             reponse= questdlg(['Probleme a l execution de : ',file,' Continuer tout de meme ?'], ...
%                 'VEHLIB - Script vehlib_liste.m','Oui', 'Non','Oui');
%             if strcmp(reponse,'Non')
%                 break;
%             end
            fprintf(1,'%s%s%s%s\n','Probleme a l execution de : ',file,' du projet :',dossier); 
        end
    end
end

% Classement par ordre alphabetique avant sortie
liste_arch=sort(liste_arch);
liste_veh=sort(liste_veh);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
