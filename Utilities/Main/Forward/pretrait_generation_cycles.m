%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Script de pre traitement pour mise en forme des tableaux de definition
% d une ligne de bus dans le logiciel VEHLIB.
% Ce script est appele depuis ini_calcul
% dans le cas ou la variable VD.CYCL.ntypcin vaut 5
% Version 1.0 fevrier 2011
% INRETS LTE 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [erreur, VD]=pretrait_generation_cycles(VD)

erreur=0;

% Mise en forme pour VEHLIB : decalage des indices de distance - balise de vitesse
temp=[VD.CYCL.balise_vitesse(2:end,1) VD.CYCL.balise_vitesse(1:end-1,2)];
% On rajoute la derniere vitesse balise avec la distance de fin (c'est une balise d'arret)
temp=[temp; VD.CYCL.balise_arret(end,1) VD.CYCL.balise_vitesse(end,2) ];
VD.CYCL.balise_vitesse=temp;
clear temp;

% Mise en forme pour vehlib
% baliseType (1 or 2) distance speed stopTime
VD.CYCL.balise_vitesse=[ones(length(VD.CYCL.balise_vitesse(:,1)),1) VD.CYCL.balise_vitesse zeros(length(VD.CYCL.balise_vitesse(:,1)),1)];
VD.CYCL.balise_arret=[2*ones(length(VD.CYCL.balise_arret(:,1)),1) VD.CYCL.balise_arret(:,1) zeros(length(VD.CYCL.balise_arret(:,1)),1) VD.CYCL.balise_arret(:,2)];

VD.CYCL.balise=VD.CYCL.balise_vitesse;
VD.CYCL.balise=[VD.CYCL.balise_arret(1,:); VD.CYCL.balise];
VD.CYCL.balise=[VD.CYCL.balise; VD.CYCL.balise_arret(end,:)];

% Mise en forme pour VEHLIB: rajout des balises d'arret
for aind=2:length(VD.CYCL.balise_arret)-1
    for vind=1:length(VD.CYCL.balise(:,1))
        if VD.CYCL.balise_arret(aind,2)==VD.CYCL.balise(vind,2)
            VD.CYCL.balise=[VD.CYCL.balise(1:vind,:); VD.CYCL.balise_arret(aind,:); VD.CYCL.balise(vind+1:end,:)];
            break;
        end
    end
end

% Mise en forme pour VEHLIB : decalage des indices de distance - balise de pente
% ini_pente est ecrit pour des distances en km
% les distances sont relatives
temp=[VD.CYCL.pente_ligne(2:end,1)];
temp=[temp; VD.CYCL.pente_ligne(end,1)/1000];
temp=temp-VD.CYCL.pente_ligne(1:end,1);
VD.CYCL.pente_ligne=[temp/1000 VD.CYCL.pente_ligne(1:end,2)];
clear temp;


