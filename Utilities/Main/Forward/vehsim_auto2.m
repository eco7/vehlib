function [vehlib, VD]=vehsim_auto2(vehicule,architecture,cinem,Dod0,lect_mod,slk)
%VEHSIM_AUTO2 Fonction remplacant vehsim en automatisant tout.
%
%  (C) Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Fonction remplacant <vehsim> en automatisant tout. Plus besoin d'intervention de l'utilisateur.
% Cette fonction permet de renseigner :
% - la structure vehlib 
% - la macro structure de donnees composants VD
% - initialise egalement toutes les valeurs necessaires au lancement d'un calcul dans VEHLIB.
%
% Arguments d'appel:
% - vehlib_f = string contenant le nom de fichier qui renseigne la structure vehlib.
% - architecture = string contenant le nom du modele.
% - cinem = string contenant le nom du fichier cinematique
%
% Arguments optionnels :
% - Dod0 = DOD initiale ou bien vecteur (DODini,DODmax)
% - lect_mod : entier permettant de 
% relire le modele et la cinematique avec nettoyage du Workspace matlab (1, valeur par defaut) 
% relire le modele sans la cinematique et sans nettoyage du Workspace matlab (3) 
% relire le modele avec la cinematique et sans nettoyage du Workspace matlab (2) 
% relire la cinematique uniquement sans nettoyage du ws (0)
% - slk  : entier permettant d'ouvrir le modele simulink (1, valeur par defaut) 
%
% TODO : 
% - lect_mod semble obsolete ...
% A VOIR DANS UNE VERSION ULTERIEURE
%
%  Exemple de lancement:
%  [vehlib, VD] = vehsim_auto2('CLIO_1L5DCI','VTH_BV','CIN_NEDC_BV');
%
% see also VEHSIM, LECTUREMODEL, LECTUREDATA, INI_CALCUL
% ---------------------------------------------------------

if nargin ~=2 && nargin ~=3 && nargin ~=4 && nargin~=5 && nargin~=6
    errordlg('Wrong number of arguments','vehsim_auto2')
end

if nargin==2
    cinem = '';
    Dod0=NaN;
    lect_mod=1;
    slk=1;
end

if nargin==3
    Dod0=NaN;
    lect_mod=1;
    slk=1;
end

if nargin==4
    lect_mod=1;
    slk=1;
end

if nargin==5
    slk=1;
end

if lect_mod==1
    % Nettoyage du WS
    evalin('base','clear');
end

if lect_mod==1 || lect_mod==2  || lect_mod==3
    
    % Lecture des fichiers de donnees composants
    [vehlib, err]=lectureModel(vehicule,architecture);
    if err
        disp('Erreur a la lecture du modele');
        [~, VD]=vehsim_auto2_erreur;
        return
    end
    if nargin >=3 && ~isfield(vehlib,'CYCL') && ~isempty(cinem)
        % Il faut initialiser le champ CYCL
        vehlib.CYCL=cinem;
    end 
    if ~isempty(cinem) && isfield(vehlib,'CYCL') && ~strcmp(cinem,vehlib.CYCL)
        % Il faut rafraichir le champ vehlib.CYCL
        vehlib.CYCL=cinem;
    end
    [VD, err]=lectureData(vehlib);
    if err
        disp('Erreur a la lecture des donnees');
        return
    end
    
end

% if lect_mod ==3 
%     disp({'Cette option est obsolete. Utiliser dorenavant le code suivant :'; ...
%         'vehlib.CYCL=''New Cycle'';[VD, err]=lectureData(vehlib, 3, {''CYCL''}, VD);[err, vehlib, VD]=ini_calcul(vehlib, VD)'});
%         [vehlib, VD]=vehsim_auto2_erreur;
%         return;
% end

if lect_mod == 0
    disp({'Cette option est obsolete. Utiliser dorenavant le code suivant :'; ...
        'vehlib.CYCL=''New Cycle'';[VD, err]=lectureData(vehlib, 2, {''CYCL''}, VD);[err, vehlib, VD]=ini_calcul(vehlib, VD)'});
        [vehlib, VD]=vehsim_auto2_erreur;
        return;  
end

if ~isnan(Dod0)
    % Lecture de la valeur de decharge batterie initiale
    VD.INIT.Dod0=Dod0(1);
end

if slk==1
    open_system(vehlib.simulink);
elseif isfield(vehlib,'simulink')
    vehlib=rmfield(vehlib,'simulink');
end

% Initialisation des parametres propres a la simulation
[erreur, vehlib, VD]=ini_calcul(vehlib, VD);
if erreur
    disp('Erreur a l execution de ini_calcul');
    return
end

end

%------------------------------------------
%
function [vehlib, VD]=vehsim_auto2_erreur

vehlib=struct();
VD=struct();

end