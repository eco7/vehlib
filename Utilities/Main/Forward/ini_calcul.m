% Function: ini_calcul
% Fonction appelee par vehsim (et vehsim_auto2) qui realise des pre-calculs pour les modeles de simulation de VEHLIB
%
%  ?? Copyright IFSTTAR LTE 1999-2011
%
% -----------------------------------------------------------
%
% Objet:
%
% Fonction appele par vehsim (et vehsim_auto) dont le but est :
%
% - Initialiser des parametres non utilisees mais necessaire aux modele simulink
%   (ex: type de cinematiques, type de vehicule, type de calculateur)
% - Configurer les blocs configurables
% - Realiser des pre calculs (ex: dod batterie)
%
% Attention lorsqu'il peut y avoir une ambiguite (par exemple le bloc
% configurable batterie est configure si la structure BATT existe dans VD
% et si le champ BATT existe dans vehlib), les 2 tests sont effectues.
% Par contre, pour l'initialisation du vehicule hybride serie, seul le
% champ architecture de vehlib est teste.
% Argument de retour:
%
% - erreur permet de renvoyer un code d'erreur
% - vehlib : normalement inchangee
% - VD : structure contenant les donnees du vehicule, "enrichie" pour les
% calculs avec vehlib:
% ** Blocs configurable (batterie, cycles et vehicule) sont initialises si un modele simulink est declare
% ** Quelques calucls preliminaires batterie
% ** Type de calcul des efforts de resistance a l'avancement vehicule
% ** Parametres des Cycles, trajets, pente, courbures
% ** Parametres de certains calculateur de VEHLIB.mdl
%
% -----------------------------------------------------------
function [erreur, vehlib, VD]=ini_calcul(vehlib, VD)

% initialisation
erreur=0;

% ------------------------
% Initialisation du bloc batterie de la bibliotheque VEHLIB

% Pour le systeme configurable de la batterie
if isfield(vehlib,'BATT') && isfield(VD,'BATT') && isfield(vehlib,'simulink')
    try
        name_blck=find_system(vehlib.simulink,'name','Batterie_Configurable');
        name_blck=char(name_blck);
        if VD.BATT.ntypbat==1
            set_param(name_blck, 'BlockChoice','Batterie');
        elseif VD.BATT.ntypbat==2
            set_param(name_blck, 'BlockChoice','Batterie S fonc');
        elseif VD.BATT.ntypbat==3
            set_param(name_blck, 'BlockChoice','Batterie Modele 3');
        end
    catch
        %uiwait(errordlg('ini_calcul: you should use the Battery_Configurable bloc within your model',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
        disp('ini_calcul: you should use the Battery_Configurable bloc within your model');
    end
end

% Conditions initiales batterie
if isfield(VD,'BATT')
    if VD.BATT.ntypbat==1 || VD.BATT.ntypbat==2
        if ~isfield(VD.INIT,'Dod0') || isnan(VD.INIT.Dod0)
            if length(unique(VD.BATT.ocv(1,:)))~=length((VD.BATT.ocv(1,:)))
                errordlg('Probleme avec interp1: The values of  VD.BATT.ocv(1,:) should be distinct.');
                erreur=1;
                return
            end
            C=interp1(VD.BATT.ocv(1,:),VD.BATT.dod_ocv,VD.INIT.Ubat0);
            if isnan(C)
                errordlg('La Tension initiale est en dehors des limites');
                erreur=1;
                return
            end
            VD.INIT.Dod0=C;
        end
    elseif VD.BATT.ntypbat==3
    elseif VD.BATT.ntypbat==10
        C=VD.ECU.BMS_Ubat_ini_param1+VD.ECU.BMS_Ubat_ini_param2*(100-VD.INIT.Dod0);
        VD.INIT.Ubat0=C;
    else
        errordlg('type de batterie non reconnue');
    end
end

% ------------------------
% Initialisation du bloc vehicule de la bibliotheque VEHLIB

% Pour le systeme configurable du vehicule
if isfield(vehlib,'VEHI') && isfield(VD,'VEHI') && isfield(vehlib,'simulink')
    try
        name_blck=find_system(vehlib.simulink,'name','Vehicule_Configurable');
        name_blck=char(name_blck);
        if isfield(VD.VEHI,'ntyppneu') && VD.VEHI.ntyppneu==2
            set_param(name_blck, 'BlockChoice','Vehicule Pacejka');
        else
            set_param(name_blck, 'BlockChoice','Vehicule');
        end
    catch
        %uiwait(errordlg('ini_calcul: you should use the Vehicule_Configurable bloc within your model',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
        disp('ini_calcul: you should use the Vehicule_Configurable bloc within your model');
    end
end

% Pour le systeme configurable de la loi de route du vehicule par defaut
if isfield(vehlib,'VEHI') && isfield(VD,'VEHI') && isfield(vehlib,'simulink')
    % Voir pour les blocs Pacejka. La variation avec le signe de la
    % vitesse est code avec tanh
    try
        if ~isfield(VD.VEHI,'ntyppneu') || VD.VEHI.ntyppneu~=2
            name_blck=find_system(vehlib.simulink,'FollowLinks','on','name','Loi_Route_Configurable');
            name_blck=char(name_blck);
            if VD.VEHI.ntypveh==1
                % Loi de route F =
                % (VD.VEHI.a+VD.VEHI.b*v*v)*Masse*VD.VEHI.Gpes+VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx/2*v*v
                set_param(name_blck,'BlockChoice', 'Loi de route ntypveh 1');
            elseif VD.VEHI.ntypveh==2
                % Loi de route F = VD.VEHI.fcst+VD.VEHI.fvit1*v+VD.VEHI.fvit2*v*v
                set_param(name_blck,'BlockChoice', 'Loi de route ntypveh 2');
            elseif VD.VEHI.ntypveh==3
                % Loi de route F = VD.VEHI.a0+VD.VEHI.a1*v+VD.VEHI.a2*v*v + VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx/2*v*v
                set_param(name_blck,'BlockChoice', 'Loi de route ntypveh 3');
            elseif VD.VEHI.ntypveh==4
                set_param(name_blck,'BlockChoice', 'Loi de route ntypveh 4');
                % Loi de route F = crr(1)*Masse*Essieu_Dir_pc/100 + crr(2)*Masse*Essieu_Drv_pc/100 + crr(3)*Masse*Essieu_Rem_pc/100 + VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx/2*v*v
            end
        end
    catch
        disp('ini_calcul: you should use the Road Load configurable bloc within your model');
    end
end

% initialisation de variables inutiles au bloc vehicule du modele de Pacejka
if isfield(vehlib,'VEHI') && isfield(VD,'VEHI') && isfield(vehlib,'simulink')
    if isfield(VD.VEHI,'ntyppneu') && VD.VEHI.ntyppneu==2 % Pacejka
        if VD.VEHI.ntypveh==1
            % Loi de route F =
            % (VD.VEHI.a+VD.VEHI.b*v*v)*Masse*VD.VEHI.Gpes+VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx/2*v*v
            VD.VEHI.fcst=0;
            VD.VEHI.fvit1=0;
            VD.VEHI.fvit2=0;
            VD.VEHI.a0=0;
            VD.VEHI.a1=0;
            VD.VEHI.a2=0;
        elseif VD.VEHI.ntypveh==2
            % Loi de route F = VD.VEHI.fcst+VD.VEHI.fvit1*v+VD.VEHI.fvit2*v*v
            VD.VEHI.a=0;
            VD.VEHI.b=0;
            VD.VEHI.Sveh=0;
            VD.VEHI.Cx=0;
            VD.VEHI.Roatm=0;
            VD.VEHI.a0=0;
            VD.VEHI.a1=0;
            VD.VEHI.a2=0;
        elseif VD.VEHI.ntypveh==3
            % Loi de route F = VD.VEHI.a0+VD.VEHI.a1*v+VD.VEHI.a2*v*v + VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx/2*v*v
            VD.VEHI.a=0;
            VD.VEHI.b=0;
            VD.VEHI.fcst=0;
            VD.VEHI.fvit1=0;
            VD.VEHI.fvit2=0;
        end
    end
end

% ------------------------
% Initialisation des blocs cinematique de la bibliotheque VEHLIB

% Pour le systeme configurable de la cinematique
if isfield(vehlib,'CYCL') && isfield(VD,'CYCL') && isfield(vehlib,'simulink')
    try
        name_blck=find_system(vehlib.simulink,'name','Cinematique_Configurable');
        name_blck=char(name_blck);
        if (isfield(VD.CYCL,'ntypcin') && VD.CYCL.ntypcin == 1) || ...
           (isfield(VD.CYCL,'ntyptrajet') && VD.CYCL.ntyptrajet == 1)
            set_param(name_blck, 'BlockChoice','Cinematique Modele type 1 et 3');
        elseif (isfield(VD.CYCL,'ntypcin') && VD.CYCL.ntypcin == 2) || ...
               (isfield(VD.CYCL,'ntyptrajet') && VD.CYCL.ntyptrajet == 2)
            set_param(name_blck, 'BlockChoice','Cinematique Modele type 2');
        elseif (isfield(VD.CYCL,'ntypcin') && VD.CYCL.ntypcin == 3) || ...
                (isfield(VD.CYCL,'ntyptrajet') && VD.CYCL.ntyptrajet == 3)
            set_param(name_blck, 'BlockChoice','Cinematique Modele type 1 et 3');
        elseif (isfield(VD.CYCL,'ntypcin') && VD.CYCL.ntypcin == 4) || ...
                (isfield(VD.CYCL,'ntyptrajet') && VD.CYCL.ntyptrajet == 4)
            set_param(name_blck, 'BlockChoice','Cinematique Modele type 4');
        elseif VD.CYCL.ntypcin == 5
            set_param(name_blck, 'BlockChoice','Cinematique Modele type 5');
        end
    catch
        %uiwait(errordlg('ini_calcul: you should use the Cinematique_Configurable bloc within your model',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
        if strcmp(vehlib.architecture,'VTH_BV') || strcmp(vehlib.architecture,'VELEC')
            disp('ini_calcul: you should use the Cinematique_Configurable bloc within your model');
        end
    end
end

if isfield(VD,'CYCL')
    if ~isfield(VD.CYCL,'ntyptrajet') || VD.CYCL.ntyptrajet==0
        % initialisation de variables inutiles au bloc cinematique
        if VD.CYCL.ntypcin==1
            % cycle vitesse/temps
            VD.CYCL.rappvit=1:length(VD.CYCL.temps);
        elseif VD.CYCL.ntypcin==2
            % cycle vitesse/distance
            % Script de mise en forme des lignes de bus
            [erreur, VD]=pretrait_ligne(VD);
            if erreur
                uiwait(errordlg('ini_calcul: Erreur lors de l execution de la fonction pretrait_ligne',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
            end
            
        elseif VD.CYCL.ntypcin==3
            % cycle vitesse/temps/rapport BV
        elseif VD.CYCL.ntypcin==4
            % Simulation de conduite
        elseif VD.CYCL.ntypcin==5 %simulation de conduite vellecta
            VD.CYCL.vitesse=[0 3];
            VD.CYCL.temps=[0 3];
            VD.CYCL.rappvit=[0 3];
            % on lance un script de mise en forme des donnees de la ligne
            [erreur, VD]=pretrait_generation_cycles(VD);
            if erreur
                uiwait(errordlg('ini_calcul: Erreur lors de l execution de la fonction pretrait_generation_cycles',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
                return
            end
        end
    elseif VD.CYCL.ntyptrajet==1
        [erreur, VD]=pretrait_trajet(VD);
        if erreur
            uiwait(errordlg('ini_calcul: Erreur lors de l execution de la fonction pretrait_trajet',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
            return
        end
        VD.CYCL.rappvit=1:length(VD.CYCL.temps);
    elseif VD.CYCL.ntyptrajet==2
        [erreur, VD]=pretrait_trajet(VD);
        if erreur
            uiwait(errordlg('ini_calcul: Erreur lors de l execution de la fonction pretrait_trajet',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
            return
        end
        [erreur, VD]=pretrait_ligne(VD);
        if erreur
            uiwait(errordlg('ini_calcul: Erreur lors de l execution de la fonction pretrait_trajet',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
            return
        end
    elseif VD.CYCL.ntyptrajet==3
        [erreur, VD]=pretrait_trajet(VD);
        if erreur
            uiwait(errordlg('ini_calcul: Erreur lors de l execution de la fonction pretrait_trajet',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
            return
        end
    else
        error('Pb de reconnaissance du type de cycle');
    end
    
        
    % Gestion des pentes
    [erreur, VD]=ini_pente(VD);
    if erreur               
        uiwait(errordlg('ini_calcul: Erreur lors de l execution de la fonction ini_pente',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
        return
    end
    % Gestion de la courbure
    [erreur, VD]=ini_courbure(VD);
    if erreur               
        uiwait(errordlg('ini_calcul: Erreur lors de l execution de la fonction ini_courbure',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
        return
    end

    % Choix du temps final pour le calcul
    if VD.CYCL.ntypcin==1 || VD.CYCL.ntypcin==3
        VD.CYCL.tmax=max(VD.CYCL.temps);
    elseif VD.CYCL.ntypcin==4
        VD.CYCL.tmax=Inf;
    else
        VD.CYCL.tmax=Inf;
    end
    
end

% ------------------------
% Initialisation des blocs conducteur de la bibliotheque VEHLIB

% Pour le systeme configurable du conducteur
% Le modele conducteur de type 2 utilise une machine d'etat sous Stateflow.
% Mais toutes les licences n'ont pas Stateflow, ce qui pose des problemes
% (Warning a l'ouverture du modele et impossibilite de modifier la librairie):
% Le modele de conducteur de type 2 est sorti de la librairie VEHLIB.mdl et
% les modeles VTH_BV et VELEC sont dupliques.
if isfield(vehlib,'DRIV') && isfield(VD,'DRIV') && isfield(vehlib,'simulink')
    try
        name_blck=find_system(vehlib.simulink,'name','Conducteur_Configurable');
        name_blck=char(name_blck);
        if (isfield(VD.DRIV,'type') && VD.DRIV.type == 1) 
            set_param(name_blck, 'BlockChoice','Conducteur');
%         elseif (isfield(VD.DRIV,'type') && VD.DRIV.type == 2)
%             set_param(name_blck, 'BlockChoice','Conducteur Modele 2');
        end
    catch
        %uiwait(errordlg('ini_calcul: you should use the Cinematique_Configurable bloc within your model',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
        if strcmp(vehlib.architecture,'VTH_BV') || strcmp(vehlib.architecture,'VELEC')
            disp('ini_calcul: you should use the Conducteur_Configurable bloc within your model');
        end
    end
end


% ------------------------
% Initialisation des blocs calculateur de la bibliotheque VEHLIB

% Pour les systemes configurables des calculateurs de vehicule conventionnel et
% du vehicule electrique :
% Les donnees sont les memes, (pas de type VD.ECU.type pour les distinguer),
% mais le calculateur avec modele PID est attaque en couple moteur (cas general de VEHLIB)
% alors que le modele de conducteur est attaque en Force vehicule
if strcmp(vehlib.architecture,'VTH_BV')
    if isfield(vehlib,'ECU') && isfield(VD,'ECU') && isfield(vehlib,'simulink')
        try
            name_blck=find_system(vehlib.simulink,'name','ECU_VTH_BV_Configurable');
            name_blck=char(name_blck);
            if (isfield(VD.DRIV,'type') && VD.DRIV.type == 1)
                set_param(name_blck, 'BlockChoice','ECU_VTH_BV Modele 1');
            elseif (isfield(VD.DRIV,'type') && VD.DRIV.type == 2)
                set_param(name_blck, 'BlockChoice','ECU_VTH_BV Modele 2');
            end
        catch
            %uiwait(errordlg('ini_calcul: you should use the Cinematique_Configurable bloc within your model',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
            disp('ini_calcul: you should use the ECU_VTH_BV_Configurable bloc within your model');
        end
    end
end
if strcmp(vehlib.architecture,'VELEC')
    if isfield(vehlib,'ECU') && isfield(VD,'ECU') && isfield(vehlib,'simulink')
        try
            name_blck=find_system(vehlib.simulink,'name','ECU_VELEC_Configurable');
            name_blck=char(name_blck);
            if (isfield(VD.DRIV,'type') && VD.DRIV.type == 1)
                set_param(name_blck, 'BlockChoice','ECU_VELEC Modele 1');
            elseif (isfield(VD.DRIV,'type') && VD.DRIV.type == 2)
                set_param(name_blck, 'BlockChoice','ECU_VELEC Modele 2');
            end
        catch
            %uiwait(errordlg('ini_calcul: you should use the Cinematique_Configurable bloc within your model',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
            disp('ini_calcul: you should use the ECU_VELEC_Configurable bloc within your model');
        end
    end
end

% initialisation de variables inutiles au bloc calculateur du vehicule hybride serie.
if strcmp(vehlib.architecture,'HSER_GE') || strcmp(vehlib.architecture,'HSER_GE_SC')
    switch VD.ECU.ntypstrat
        case 1
            VD.ECU.Vit_dec_hyb=0;
            VD.ECU.Vit_arret_hyb=0;
            VD.ECU.DoD_dec_hyb=0;
            VD.ECU.DoD_arret_hyb=0;
            VD.ECU.Pcst=0;
            VD.ECU.cpcbat_mar=0;
            VD.ECU.cpcbat_arret=0;
        case 2
            VD.ECU.cpcbat_mar=0;
            VD.ECU.cpcbat_arret=0;
            VD.ECU.Vit_dec_hyb=0;
            VD.ECU.Vit_arret_hyb=0;
            VD.ECU.DoD_dec_hyb=0;
            VD.ECU.DoD_arret_hyb=0;
        case 3
            VD.ECU.Vit_dec_hyb=0;
            VD.ECU.Vit_arret_hyb=0;
            VD.ECU.P_dec_hyb=0;
            VD.ECU.P_arret_hyb=0;
            VD.ECU.DoD_dec_hyb=0;
            VD.ECU.DoD_arret_hyb=0;
        case 4
            VD.ECU.Pcst=0;
            VD.ECU.cpcbat_mar=0;
            VD.ECU.cpcbat_arret=0;
            VD.ECU.P_dec_hyb=0;
            VD.ECU.P_arret_hyb=0;
            VD.ECU.DoD_dec_hyb=0;
            VD.ECU.DoD_arret_hyb=0;
        case 5
            VD.ECU.Pcst=0;
            VD.ECU.Vit_dec_hyb=0;
            VD.ECU.Vit_arret_hyb=0;
            VD.ECU.cpcbat_mar=0;
            VD.ECU.cpcbat_arret=0;
    end
end

% initialisation de variables inutiles au bloc calculateur du vehicule avec coupleur hydraulique.
if strcmp(vehlib.architecture,'VTH_CPLHYD') || strcmp(vehlib.architecture,'HP_HYD') || strcmp(vehlib.architecture,'HP_BV_CPLHYD_BAT_SC')
    if VD.ECU.bv_auto==1 || VD.ECU.bv_auto==3
        VD.ECU.alpha_rapport=[0 1];
        VD.ECU.rapport=[0 1];
        VD.ECU.accel_maxup=[0 1;0 1];
        VD.ECU.accel_minup=[0 1;0 1];
        VD.ECU.accel_maxdown=[0 1;0 1];
        VD.ECU.accel_mindown=[0 1;0 1];
        VD.ECU.Nmin_shiftup=[0 1;0 1];
        VD.ECU.Nmax_shiftup=[0 1;0 1];
        VD.ECU.Nmin_shiftdown=[0 1;0 1];
        VD.ECU.Nmax_shiftdown=[0 1;0 1];
    end
    if VD.ECU.bv_auto==2 || VD.ECU.bv_auto==3
        VD.ECU.wmt_chrap_M=[0 1];
        VD.ECU.ouvert_chrap_M=[0 1];
        VD.ECU.wmt_chrap_M1=[0 1];
        VD.ECU.ouvert_chrap_M1=[0 1];
        VD.ECU.wmt_chrap_D=[0 1];
        VD.ECU.ouvert_chrap_D=[0 1];
    end
    if VD.ECU.bv_auto==1 || VD.ECU.bv_auto==2
        %VD.ECU.ouvert_chrap_M1=[0 1];
        VD.ECU.wred_chrap_M1=[0 1];
        VD.ECU.ouvert_chrap_M2=[0 1];
        VD.ECU.wred_chrap_M2=[0 1];
        %VD.ECU.ouvert_chrap_M=[0 1];
        VD.ECU.wred_chrap_M=[0 1];
        VD.ECU.ouvert_chrap_D1=[0 1];
        VD.ECU.wred_chrap_D1=[0 1];
        VD.ECU.ouvert_chrap_D2=[0 1];
        VD.ECU.wred_chrap_D2=[0 1];
        %VD.ECU.ouvert_chrap_D=[0 1];
        VD.ECU.wred_chrap_D=[0 1];
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: ini_pente
%  Initialisation des grandeurs liees a la pente longitudinale de la chausse
%
% Objet:
%
%  Initialisation des grandeurs liees a la pente longitudinale de la chausse
%  Gestion des erreurs eventuelles.
%
%  Initialisation de variables inutiles au calcul, mais present dans les modeles de composants
%  CYCL.ntyppente 
%   0 : pente constante définie en % dans le champ VD.CYCL.pente
%   1 : pente variable en fonction de la distance longitudinale pour les
%   cycles vitesse - distance (CYCL.ntypcin = 2)
%   2 : pente variable pour un fichier ferroviaire
%   3 : pente variable en fonction de la distance longitudianle pour les
%   cycles vitesse - temps (CYCL.ntypcin = 1 ou 3)
%
% Arguments de retour:
%
%  erreur: Gestion des erreurs eventuelles.
%
%  Version 1: Bogdan Vulturescu Mai 05
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [erreur, VD]=ini_pente(VD)

% initialisation
erreur=0;

if ~isfield(VD.CYCL,'ntyppente') || VD.CYCL.ntyppente==0
    VD.CYCL.ntyppente=0; % compatibilite avec anterieur
    VD.CYCL.distpente_bal=[0 1];
    VD.CYCL.pente_bal=[0 0];
    VD.CYCL.PKpente=[0 1];
    VD.CYCL.penteFpk=[0 0];
    
elseif isfield(VD.CYCL,'ntyppente') && VD.CYCL.ntyppente == 1
%     if VD.CYCL.ntypcin ~= 2
%         erreur=1;
%         return
%     end
    % Definition pente
    for i=1:1:length(VD.CYCL.pente_ligne)
        if i>1
            VD.CYCL.distpente_bal(i)=VD.CYCL.pente_ligne(i,1)'*1000+VD.CYCL.distpente_bal(i-1);
        else
            VD.CYCL.distpente_bal(i)=VD.CYCL.pente_ligne(i,1)'*1000;
        end
    end
    VD.CYCL.pente_bal=VD.CYCL.pente_ligne(:,2)';
    VD.CYCL.pente=0;
    VD.CYCL.PKpente=[0 1];
    VD.CYCL.penteFpk=[0 0];
    
elseif isfield(VD.CYCL,'ntyppente') && VD.CYCL.ntyppente == 2
    % Le champ est defini - on utilize apriori un fichier ferro
    % Initialisation des champs necessaires ? la description des pentes de l'inter station:
    [VD.CYCL.PKpente, VD.CYCL.penteFpk]=data2ANGLE(VD.CYCL.data_Oxz);
    % VD.CYCL.PKcourbure  =   Les PK des changements de pente
    % VD.CYCL.courbureFpk =   Description de la pente tout au long de
    % l'interstation
    VD.CYCL.distpente_bal=[0 1];
    VD.CYCL.pente_bal=[0 0];
elseif isfield(VD.CYCL,'ntyppente') && VD.CYCL.ntyppente==3
    % Cycle vitesse - temps (avec ou sans rapport de boite)
    % et pente en fonction du pK lu dans le fichier cinematique.
    % Les variables sont donc VD.CYCL.PKpente (m), VD.CYCL.penteFpk (%)
    VD.CYCL.distpente_bal=[0 1];
    VD.CYCL.pente_bal=[0 0];
    VD.CYCL.pente=0;
else
    erreur=1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: ini_courbure
%  Initialisation des grandeurs liees a la courbure de la chausse
%
% Objet:
%
%  Initialisation des grandeurs liees a la courbure de la chausse
%
%  Gestion des erreurs eventuelles.
%  Initialisation de variables inutiles au calcul, mais present dans les modeles de composants
%
% Arguments de retour:
%
%  erreur: Gestion des erreurs eventuelles.
%
%  Version 1: Bogdan Vulturescu Mai 05
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [erreur, VD]=ini_courbure(VD)

% initialisation
erreur=0;

if isfield(VD.CYCL,'ntypcourb') && VD.CYCL.ntypcourb==1
    % Le champ est defini - on utilize apriori un fichier ferro
    % Initialisation des champs necessaires ? la description des courbes de l'inter station:
    [VD.CYCL.PKcourbure, VD.CYCL.courbureFpk]=data3CURVE(VD.CYCL.data_Oxy);
    % VD.CYCL.PKcourbure  =   Les PK des changements de courbure
    % VD.CYCL.courbureFpk =   Description de la courbure tout au long de l'interstation
else
    % Le champ n'est pas defini
    VD.CYCL.ntypcourb=0;
    VD.CYCL.PKcourbure=[0 1];
    VD.CYCL.courbureFpk=[0 0];
    VD.CYCL.debutInterStation=0;
    VD.CYCL.finInterStation=0;
end

