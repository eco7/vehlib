% Function: lec_mod_ws
% Lecture des fichiers : fichier modele puis fichiers composants.
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
%
%  Lecture des fichiers : fichier modele puis fichiers composants.
%
% Argument d'appel:
%
% nom_modele = string contenant le fichier modele qui renseigne la structure vehlib.
%
% Attention: L'appel a cette fonction nettoye la tructure de donnees des
% composants VD
% ---------------------------------------------------------
function [fields]=lec_mod_ws(nom_modele,lect_mod)

disp('Cette fonction est obsolete. Elle sera supprimee des prochaines release de vehlib. Utilisez dorenavant: lectureData et lectureModel !');

if nargin == 1
    lect_mod=1;
end

if lect_mod == 1
    % nettoyage des structures de donnees composants
    evalin('base','clear VD');
end

% on evalue le script selectionne
evalin('base',nom_modele);
% on stocke le nom du fichier dans la structure vehlib
evalin('base',strcat('vehlib=setfield(vehlib,''nom'',','''',nom_modele,''');'));
% Chargement des donnees dans le workspace

fields=evalin('base','fieldnames(vehlib)');
for i=1:evalin('base','length(fieldnames(vehlib))');
   % pour ne pas ouvrir un modele quand la structure vehlib existe ds WS
   if ~or(strcmp(fields{i},'nom'),or(strcmp(fields{i},'architecture'),strcmp(fields{i},'VD.CYCL'))) 
       if ~strcmp(fields{i},'simulink') & ~strcmp(fields{i},'documents')
          evalin('base',strcat('eval(vehlib.',fields{i},');'))
      end
   end
end

if lect_mod~=3
    evalin('base','eval(vehlib.CYCL);');
end

