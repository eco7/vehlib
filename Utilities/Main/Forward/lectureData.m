% Function: lectureData
% function [ VD, err ] = lectureData( vehlib, lect_mod, vehlib_comp, VD)
% Lecture des fichiers composants et initialisation de la structure de donnees VD.
%
%  (C) Copyright IFSTTAR LTE 1999-2014
%
% Objet:
%
%  Lecture des fichiers composants.
%
% Argument d'appel:
%
% vehlib = structure comprenant le nom des fichiers composants.
% lect_mod  = argument optionnel
%   1 : lecture de tous le contenu des champs de la structure vehlib
%   2 : lecture d'une liste de fichier composant
%   3 : lecture de tous les champs sauf la liste (A CODER)
% vehlibComp = argument optionnel si lect_mod vaut 2 ou 3
% CellArray comprenant le nom des structures à rafraichir (lect_mod == 2)
% ou au contraire à ne pas rafraichir (lect_mod == 3)
% VD sructure a rafraichir si lect_mod == 2 ou 3
%
function [ VD, err ] = lectureData( vehlib, lect_mod, vehlibComp, VD )

err=0;

if nargin == 1
    lect_mod=1;
end

if (lect_mod == 2 ||lect_mod == 3) && nargin ~=4
    err=1;
    disp('Wrong number of arguments')
    VD=struct();
    return
end

if lect_mod == 1
    fields=fieldnames(vehlib);
    expr=strcat('VD=struct(');
    for iji=1:length(fields)
        %if ~strcmp(fields{iji},'nom') && ~strcmp(fields{iji},'architecture') && ~strcmp(fields{iji},'CYCL') ...
        if ~strcmp(fields{iji},'nom') && ~strcmp(fields{iji},'architecture') ...
                && ~strcmp(fields{iji},'simulink') && ~strcmp(fields{iji},'documents')
            file=eval(strcat('vehlib.',fields{iji}));
            try
                % Lecture du fichier de donnees du composant
                eval(file);
                if ~exist(fields{iji},'var')
                    % Ce test n'est pas suffisant mais devrait permettre de trouver pas mal d'erreur
                    disp(['Le champ : ',fields{iji},' n''existe pas dans le fichier : ',file]);
                    err=6;
                    VD=struct();
                    return
                end
            catch ME
                err=2;
                displayME(ME, err);
                VD=struct();
                return
            end
            if iji>1
                expr=strcat(expr,',');
            end
            expr=strcat(expr,'''',fields{iji},''',',fields{iji});
        end
    end
    expr=strcat(expr,');');
    try
        eval(expr);
    catch ME
        err=3;
        displayME(ME, err);
        VD=struct();
        return
    end
end

if lect_mod == 2
    for iji=1:length(vehlibComp)
        try
            fichier=vehlib.(vehlibComp{iji});
            eval(fichier);
            expr=strcat('VD=setfield(VD,''',vehlibComp{iji},''',',vehlibComp{iji},');');
            eval(expr)
        catch ME
            err=4;
            displayME(ME, err);
            %VD=struct();
            return
        end
    end
end

if lect_mod == 3
    fields=fieldnames(vehlib);
    for iji=1:length(fields)
        if sum(strcmp(fields{iji},vehlibComp)) == 0 && ...
                sum(strcmp(fields{iji},{'nom', 'architecture', 'simulink', 'documents'})) == 0
            try
                fichier=vehlib.(fields{iji});
                eval(fichier);
                expr=strcat('VD=setfield(VD,''',fields{iji},''',',fields{iji},');');
                eval(expr)
            catch ME
                err=5;
                displayME(ME, err);
                %VD=struct();
                return
            end
        end
    end
end
end

%-------------------------------------%
%
function displayME(ME, err)

if exist('ME','var')
    disp(['lectureData: erreur ',num2str(err)]);
    disp(['lectureData: Erreur dans la fonction : ',ME.stack(1).name, ' - ligne : ',num2str(ME.stack(1).line)])
    disp(ME.message)
end

end