%VEHSIM Interface homme-machine pour la selection d'un vehicule dans VEHLIB
%
%  (C) Copyright IFSTTAR LTE 1999-2011
%
% Objet:
%
% Interface homme-machine pour la selection d'un vehicule dans VEHLIB
% choix d'une architecture de vehicule,
% des fichiers composants
% de la cinematique
% et qques parametres initiaux (dod, charge du vehicule, ...).
%
% Ouvre le modele simulink selectionne et 
% initialise les parametres du vehicule
%
% Example:
% vehsim
%
% see also VEHSIM_AUTO2
%-------------------------------------------------------------------------
clear % Necessaire sinon explgr peut trouver des variables initialisées lors d'un calcul precedent
disp('Vehsim : Nettoyage du workspace')
[vehlib, VD]=vehsim_gui;