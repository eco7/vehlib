function [vehlib, VD] = create_vehicle(vehicle, architecture, em_file, batt_file)
% This function creates a vehlib's compatible structure (VD) with a reduced number of parameters
% datas are based on Prius electric motor (MCT_MSA_N1_PRIUS_II)
% and Lms_yuasa_50Ah battery cell
% TO DO : Pack is created by adjustong cells in series and parallel so that pack
% voltage is around 400V. See that in details
%
% Example :
% [vehlib, VD] = create_vehicle;
% param_backward % Create param structure
% CIN_NEDC % Run Drive cycle
% VD.CYCL = CYCL; % Create VD.CYCL structure
% ERR = [];
% [erreur, vehlib, VD]=ini_calcul(vehlib, VD); % init
% [ERR, vehlib, VD, ResXml,r]=calc_backward(ERR,vehlib,param,VD); % Finally, run

if nargin == 0
    %Model = 'Tesla_Model3';
    vehicle = 'Renault_Zoe_135ps';
    architecture = 'VELEC';
    em_file = 'MCT_MSA_N1_PRIUS_II';
    batt_file = 'Lms_yuasa_50Ah';
elseif nargin == 2
    em_file = 'MCT_MSA_N1_PRIUS_II';
    batt_file = 'Lms_yuasa_50Ah';
end
if  isempty(em_file)
    em_file = 'MCT_MSA_N1_PRIUS_II';
end
if isempty(batt_file)
    batt_file = 'Lms_yuasa_50Ah';
end


run(vehicle);

% Motor
run(em_file);
% Same performances regarding voltage
ACM1.Cmax_mot=repmat(ACM1.Cmax_mot(:,end),1,3);
ACM1.Cmin_mot=repmat(ACM1.Cmin_mot(:,end),1,3);

% Prius Map Values
ACM1.Wme_max=680;
ACM1.Cme_max=400;

kspeed = Wme_max/ACM1.Wme_max;
% Speed
kspeed = Wme_max/ACM1.Wme_max;
ACM1.Regmot_pert = ACM1.Regmot_pert*kspeed;
ACM1.Pert_mot=ACM1.Pert_mot*kspeed; % At 0 rpm, losses are multiplied by kspeed !
ACM1.Regmot_cmax=ACM1.Regmot_cmax*kspeed;
% Torque
ktorque = Cme_max/ACM1.Cme_max;
ACM1.Cmot_pert=ACM1.Cmot_pert*ktorque;
ACM1.Pert_mot=ACM1.Pert_mot*ktorque; % At 0 Nm, losses are multiplied by ktorque
ACM1.Cmax_mot=ACM1.Cmax_mot*ktorque;
ACM1.Cmin_mot=ACM1.Cmin_mot*ktorque;

% finally, power !
Pme_max_init = ACM1.Regmot_cmax.*ACM1.Cmax_mot(:,1)';
Pme_max_init = max(Pme_max_init);
kelec = Pme_max/Pme_max_init;
[ACM1]= func_modif_me(kelec,ACM1);


% Reduction grear
RED.kred=kred;	% rapport de reduction
RED.rend=0.96;      % rendement du rapport de reduction (hyp)
RED.Csec_pert=0;

% Accessories
ACC.ntypacc=1;
ACC.Pacc_elec=Pacc_elec;	% [W] Sans clim ni chauffage

% Battery
% initialisation des caracteristiques de l element 
run(batt_file);
BATT.Nom_bloc=batt_file;
soc = 100-linspace(0,100,3600);
Rd=interp2(BATT.dod,BATT.tbat,BATT.Rd,(100-soc),BATT.tbat(1),'linear',max(max(BATT.Rd)));
ocv=interp2(BATT.dod_ocv,BATT.tbat_ocv,BATT.ocv,(100-soc),BATT.tbat_ocv(1));
I=repmat(BATT.Cahbat_nom,1,3600);
t=linspace(0,3600,3600);
nrj=trapz(t,I.*(ocv-Rd.*I))/3600; % NRJ of one element

ocv_mean=interp2(BATT.dod_ocv,BATT.tbat_ocv,BATT.ocv,50,BATT.tbat_ocv(1));
Upack = 400;
BATT.Nblocser = round(Upack/ocv_mean);
BATT.Nbranchepar   =   round(Batt_nrj/nrj/BATT.Nblocser);    % nombre de branches en parallele
BATT.Nblocser = round(Batt_nrj/nrj/BATT.Nbranchepar );

% Vehicle
VEHI.nbacm1 = nbacm1;
VEHI.Inroue = 0.7;                              % kg/m2
VEHI.Nbroue = 4;
VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur
VEHI.Patm     = 101325 ; 			% Pression atmospherique
VEHI.Tatm     = 298.15 ; 			% Temperature atmospherique
VEHI.Ratm     = 287 ; 			% constante de l'air
VEHI.Roatm    = VEHI.Patm/VEHI.Ratm/VEHI.Tatm ; 		% masse volumique de l'air atmopherique
VEHI.Rpneu  = largeur*hauteur + d_jante/2; 		% rayon des pneus m
VEHI.Charge = 0;
VEHI.ntypveh=1;
VEHI.Cx       = Cx ; 			%
VEHI.Sveh     = Sveh; 			% 
VEHI.adherance = 0.6;
% coef de resistance au roulement (kg/tonne) (origine ???)
VEHI.a	= 0.008;    		% terme constant (kg/tonne)
VEHI.b	= 0;                % terme en v*v
VEHI.Mveh = Veh_Mass-ACM1.Masse_mg-BATT.Masse_bloc*BATT.Nblocser*BATT.Nbranchepar;

% ECU
ECU.Vit_Cmin=ACM1.Regmot_cmax;
ECU.Cpl_min=[0 ACM1.Cmin_mot(2:end,end)'];
ECU.Frein_entre=[0 1];
ECU.Frein_avant=[1 1];

% DRIVER
driver_file='DRIVER_TYPE1_Val4';
run(driver_file);

% INIT
initdata;

% Create VD
VD.ACM1 = ACM1;
VD.VEHI = VEHI;
VD.ACC = ACC;
VD.RED = RED;
VD.BATT = BATT;
VD.ECU = ECU;
VD.INIT = INIT;
VD.DRIV = DRIV;

% Create vehlib
vehlib.architecture=architecture;
vehlib.nom = vehicle;
vehlib.DRIV = '';% mandatory for ResXml structure
vehlib.ECU=''; 
vehlib.BATT='';
vehlib.ACM1='';
vehlib.RED='';
vehlib.VEHI='';
vehlib.ACC='';
vehlib.CYCL='';

end
