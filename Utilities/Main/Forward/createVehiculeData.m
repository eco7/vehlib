% Function : createVehiculeData
%function [ERR]=createVehiculeData(dataVehiculeListe)
%
% 
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% creation d'une structure de donnees agregees pour le vehicule
%
function [ERR,VD]=createVehiculeData(dataVehiculeListe,lect_mod)

disp('Cette fonction est obsolete. Elle sera supprimee des prochaines release de vehlib. Utilisez dorenavant: lectureData et lectureModel !');

ERR=struct([]);

if lect_mod ~=0
    evalin('base',['clear VD']);
end

for ind=1:length(dataVehiculeListe)
    evalin('base',['if exist(''',dataVehiculeListe{ind},'''',',''var'');VD.',dataVehiculeListe{ind},'=',dataVehiculeListe{ind},';end']);
end

VD=evalin('base','VD');
% evalin('caller','VD=struct([]);');
% VD=struct([]);
% VD(1).T='r';
% Struct=evalin('base',[dataVehiculeListe{ind},';']);
% %evalin('caller',['VD(1).',dataVehiculeListe{ind},'=',Struct])
% setfield(VD,dataVehiculeListe{ind},Struct)
% assignin('base','VD',VD);