% Function: pretrait_trajet
% Script de pre traitement pour mise en forme des tableaux de definition de trajet dans le logiciel VEHLIB.
%
%  � Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
%
% Script de pre traitement pour mise en forme des tableaux de definition de trajet dans le logiciel VEHLIB.
%
% Ce script gere tous les types de trajets
%
%------------------------------ BV 13 avril2005 ---------------------------
%
% "pretrait_ligne"  a besoin de VD.CYCL.accces_ligne, VD.CYCL.def_ligne,
% VD.CYCL.pente_ligne pour la mise en forme des variables suivantes :
%
% VD.CYCL.acces_bal, VD.CYCL.distacces_bal, VD.CYCL.pente_bal, VD.CYCL.distpente_bal,
% VD.CYCL.vit_bal, VD.CYCL.dist_bal, VD.CYCL.autohyb_bal, ...
%
% Dans les applications ferroviaires la pente n'est pas utilisee, car elle
% doit etre definie en fonction des Pk (de meme la courbure). La variable
% VD.CYCL.pente_ligne est calculee mais pas utilisee par la suite.
%
% Pour pouvoir concatener plusieurs descriptions de pente/courbure pour
% plusieurs inter stations:
%
%  on fait les hypotheses suivantes: les donnees
% (VD.CYCL.data_Oxz, VD.CYCL.data_Oxy, VD.CYCL.ntyppente, VD.CYCL.ntypcourb) existent
% pour chaque interstation et les inter stations sont dans l'ordre
% correcte.
%
% 1. VD.CYCL.ntyppente et VD.CYCL.ntypcourb ont les memes valeurs pour toutes les
% inter stations (1 pour commencer).
%
% 2. Le sens des donnes est le sens de l'aller (ne prend pas en compte la
% variable definie dans le "TRAJET_ ...").
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [erreur, VD]=pretrait_trajet(VD)

erreur=0;

% Quelques tests de coherence
eval(VD.CYCL.ligne_trajet{1});
ntypcin=CYCL.ntypcin;
if isfield(CYCL,'ntyppente')
    ntyppente=CYCL.ntyppente;
else
    ntyppente=0;
end
if isfield(CYCL,'pente')
    pente=CYCL.pente;
else
    pente=0;
end

sens=VD.CYCL.sens_profils_pente{1};

for i=2:length(VD.CYCL.ligne_trajet)    
    eval(VD.CYCL.ligne_trajet{i});
    if ntypcin ~= CYCL.ntypcin
        % Combinaison de cycles differents non compatible
        erreur=1;
        return
    end
    if ((~isfield(CYCL,'ntyppente') || CYCL.ntyppente==0) && ntyppente ~=0) || ...
    ((isfield(VD.CYCL,'ntyppente') && VD.CYCL.ntyppente == 1) && ntyppente ~= 1) || ...
    ((isfield(VD.CYCL,'ntyppente') && VD.CYCL.ntyppente == 2) && ntyppente ~= 2) || ...
    ((isfield(VD.CYCL,'ntyppente') && VD.CYCL.ntyppente == 3) && ntyppente ~= 3)
        % Les pentes ne sont pas toutes definies de la meme maniere dans les cycles
        erreur=1;
        return       
    end
    if ntyppente == 0
        if CYCL.pente ~=pente
            % Pente constante pour un cycle mais variable d'un cycle a l'autre
            erreur=1;
            return
        end
    end
    if ntyppente == 0 && pente ~= 0
        if sens ~= VD.CYCL.sens_profils_pente{i}
            % Pente constante et non nulle, mais sens de parcours differents
            erreur=1;
            return
        end
    end
end

VD.CYCL.ntypcin=ntypcin;
VD.CYCL.ntyppente=ntyppente;
if ntyppente == 0
    VD.CYCL.pente=pente;
end

if VD.CYCL.ntyppente == 2
    % A traiter
    disp('Type de pente a mettre a jour au besoin');
    erreur=1;
    return;
end

switch VD.CYCL.ntyptrajet
    case 1
        [erreur, VD]=pretrait_trajet_ntypcin1(VD);
        return;
    case 2
        [erreur, VD]=pretrait_trajet_ntypcin2(VD);
        return;
    case 3
        [erreur, VD]=pretrait_trajet_ntypcin3(VD);
        return;
    otherwise
        uiwait(errordlg('pretrait_trajet: Type de trajet inconnu',strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));


end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: pretrait_trajet_ntypcin1
% Pre traitement pour mise en forme des tableaux de definition de trajet dans le logiciel VEHLIB.
%
%
% Objet:
% Pre traitement pour mise en forme des tableaux de definition de trajet de bus dans le logiciel VEHLIB.
%
% Cas ou la variable VD.CYCL.ntyptrajet vaut 1:il s'agit d'une
% cinematique type 1 (vitesse en fonction du temps).
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [erreur, VD]=pretrait_trajet_ntypcin1(VD)

% TRAITEMENT COMUN de DONNEES
%--------------------------------------------------------------------------

erreur=0;
flag_courb=1;

for i=1:length(VD.CYCL.ligne_trajet)
    
    eval(VD.CYCL.ligne_trajet{i});

    %__________________________________________
    % Generation du profil de vitesse en fonction du temps.
    if (i==1)
        VD.CYCL.temps=CYCL.temps;
        if (VD.CYCL.sens_profils_vitesse{1}==1)
            VD.CYCL.vitesse=CYCL.vitesse;
        else
            VD.CYCL.vitesse=fliplr(CYCL.vitesse);
        end
    else
        if CYCL.temps(1)==0
            VD.CYCL.temps=[VD.CYCL.temps CYCL.temps+(CYCL.temps(2)-CYCL.temps(1))+VD.CYCL.temps(end)];
        else
            VD.CYCL.temps=[VD.CYCL.temps CYCL.temps+VD.CYCL.temps(end)];
        end
        if (VD.CYCL.sens_profils_vitesse{i}==1)
            VD.CYCL.vitesse=[VD.CYCL.vitesse CYCL.vitesse];
        else
            VD.CYCL.vitesse=[VD.CYCL.vitesse fliplr(CYCL.vitesse)];
        end
    end
    
    %__________________________________________
    % Traitement des courbures du trajet.
    
    if ~isfield(VD.CYCL, 'ntypcourb') | (flag_courb==0)
        % Compatibilit� avec anterieur.
        % Si le champ n'existe pas dans une des interstations, la courbure
        %   du trajet sera constante et egale � 0 et le flag_courb=0.
        flag_courb=0;
        disp('pretrait_trajet_ntypcin1 : une valeur "ntypcourb" non definie. courbure nulle du trajet entier')
    end
    if (flag_courb==1)
        % Initialisation trajet selon la premiere inter station
        if (i==1)
            switch VD.CYCL.ntypcourb
                case 0
                    if (VD.CYCL.sens_profils_pente{1}==1)
                        a.courb_TOT=VD.CYCL.courbure;
                    else
                        a.courb_TOT=-VD.CYCL.courbure;
                    end
                case 1
                    data_Oxy=VD.CYCL.data_Oxy;
                    CfirstPK =VD.CYCL.debutInterStation;
                    ClastPK  =VD.CYCL.finInterStation;
            end
        else
            % Traitement du reste des interstations
            switch VD.CYCL.ntypcourb
                case 0

                case 1
                    if (CfirstPK<VD.CYCL.finInterStation)
                        % Rajout des donn�es "2" apr�s les donn�es "1". Les
                        % donn�es "2" sont les nouveaux donn�es � rajouter.
                        ClastPK=VD.CYCL.finInterStation;
                        data1_Oxy=data_Oxy;
                        data2_Oxy=VD.CYCL.data_Oxy;
                    else
                        % Rajout des donn�es "2" apr�s les donn�es "1". Les
                        % donn�es "2" sont les nouveaux donn�es � rajouter.
                        CfirstPK=VD.CYCL.debutInterStation;
                        data1_Oxy=VD.CYCL.data_Oxy;
                        data2_Oxy=data_Oxy;
                    end                    
                    [l_Oxy,c_Oxy]=size(data1_Oxy);
                    penultimatePK_Oxy=data1_Oxy(l_Oxy-1,1);
                    tmp_Oxy=data1_Oxy(1:l_Oxy-1,:);
                    [minim_Oxy,index_Oxy]=min(abs(data2_Oxy(:,1)-penultimatePK_Oxy));
                    [l_Oxy,c_Oxy]=size(data2_Oxy);
                    if (index_Oxy+1<l_Oxy)
                        data_Oxy=[tmp_Oxy;data2_Oxy(index_Oxy+1:l_Oxy,:)];
                    end
            end
        end
    end
    if isfield(VD.CYCL, 'ntypcourb')
        saveNTYPCOURB=VD.CYCL.ntypcourb;
        VD.CYCL=rmfield(VD.CYCL,'ntypcourb');
    end

    %__________________________________________
    % Traitement des pentes du trajet.
    % Initialisation trajet selon la premiere inter station
    if (i==1)
        switch VD.CYCL.ntyppente
            case 0
                if (VD.CYCL.sens_profils_pente{1}==1)
                    VD.CYCL.pente=CYCL.pente;
                else
                    VD.CYCL.pente=-CYCL.pente;
                end
            case 1
                if (VD.CYCL.sens_profils_pente{i}==1)
                    a.pente_ligne=CYCL.pente_ligne;
                else
                    a.pente_ligne(:,2)=-1*CYCL.pente_ligne(:,2);
                    a.pente_ligne(:,1)=CYCL.pente_ligne(:,1);
                    tmp=a.pente_ligne;
                    for j=1:length(a.pente_ligne)
                        a.pente_ligne(j,:)=tmp(length(a.pente_ligne)-j+1,:);
                    end
                end
            case 2
                data_Oxz=VD.CYCL.data_Oxz;
                PfirstPK =VD.CYCL.debutInterStation;
                PlastPK  =VD.CYCL.finInterStation;
            case 3
                if (VD.CYCL.sens_profils_pente{1}==1)
                    VD.CYCL.PKpente_TOT=CYCL.PKpente;
                    VD.CYCL.penteFpk_TOT=CYCL.penteFpk;
                else
                    VD.CYCL.PKpente_TOT=-(fliplr(CYCL.PKpente-CYCL.PKpente(end)));
                    VD.CYCL.penteFpk_TOT=-fliplr(CYCL.penteFpk);
                end
        end
    else
        % Traitement du reste des interstations
        switch VD.CYCL.ntyppente
            case 0
                % Fait une fois pour toute a la premier lecture
            case 1
                if (VD.CYCL.sens_profils_pente{i}==1)
                    a.pente_ligne=[a.pente_ligne; CYCL.pente_ligne];
                else
                    b.pente_ligne(:,2)=-1*CYCL.pente_ligne(:,2);
                    b.pente_ligne(:,1)=CYCL.pente_ligne(:,1);
                    tmp=b.pente_ligne;
                    for j=1:length(b.pente_ligne)
                        b.pente_ligne(j,:)=tmp(length(b.pente_ligne)-j+1,:);
                    end
                    a.pente_ligne=[b.pente_ligne; a.pente_ligne];
                end
            case 2
                if (PfirstPK<VD.CYCL.finInterStation)
                    % Rajout des donn�es "2" apr�s les donn�es "1". Les
                    % donn�es "2" sont les nouveaux donn�es � rajouter.
                    PlastPK=CYCL.finInterStation;
                    data1_Oxz=data_Oxz;
                    data2_Oxz=CYCL.data_Oxz;
                else
                    % Rajout des donn�es "2" apr�s les donn�es "1". Les
                    % donn�es "2" sont les nouveaux donn�es � rajouter.
                    PfirstPK=CYCL.debutInterStation;
                    data1_Oxz=CYCL.data_Oxz;
                    data2_Oxz=data_Oxz;
                end
                [l_Oxz,c_Oxz]=size(data1_Oxz);
                penultimatePK_Oxz=data1_Oxz(l_Oxz-1,1);
                tmp_Oxz=data1_Oxz(1:l_Oxz-1,:);
                [minim_Oxz,index_Oxz]=min(abs(data2_Oxz(:,1)-penultimatePK_Oxz));
                [l_Oxz,c_Oxz]=size(data2_Oxz);
                if (index_Oxz+1<l_Oxz)
                    data_Oxz=[tmp_Oxz;data2_Oxz(index_Oxz+1:l_Oxz,:)];
                end
            case 3
                if (VD.CYCL.sens_profils_pente{1}==1)
                    VD.CYCL.PKpente_TOT=[VD.CYCL.PKpente_TOT CYCL.PKpente+VD.CYCL.PKpente_TOT(end)+(CYCL.PKpente(2)-CYCL.PKpente(1))];
                    VD.CYCL.penteFpk_TOT=[VD.CYCL.penteFpk_TOT CYCL.penteFpk];
                else
                    VD.CYCL.PKpente_TOT=[VD.CYCL.PKpente_TOT -(fliplr(CYCL.PKpente-CYCL.PKpente(end)))+(CYCL.PKpente(2)-CYCL.PKpente(1))];
                    VD.CYCL.penteFpk_TOT=[VD.CYCL.penteFpk_TOT -fliplr(CYCL.penteFpk)];
                end
        end
    end
%     if isfield(VD.CYCL, 'ntyppente')
%         saveNTYPPENTE=VD.CYCL.ntyppente;
%         VD.CYCL=rmfield(VD.CYCL,'ntyppente');
%     end

    %__________________________________________
    % Traitement du nom du trajet.
    if (i==1)
        if isfield(VD.CYCL,'info')
            infoTXT=VD.CYCL.info;
            VD.CYCL=rmfield(VD.CYCL,'info');
        else
            infoTXT='Nom interstation : NON DISPONIBLE';
        end
    else
        if isfield(VD.CYCL,'info')
            infoTXT=strvcat(infoTXT,VD.CYCL.info);
            VD.CYCL=rmfield(VD.CYCL,'info');
        else
            infoTXT=strvcat(infoTXT,'Nom interstation : NON DISPONIBLE');
        end
    end    
end
VD.CYCL.info=infoTXT;
clear infoTXT;

%--------------------------------------------------------------------------



% TRAITEMENT INDIVIDUEL de DONNEES
%--------------------------------------------------------------------------


if (flag_courb==1)
    VD.CYCL.ntypcourb=saveNTYPCOURB;
    clear saveNTYPCOURB;
    switch VD.CYCL.ntypcourb
        case 0
            VD.CYCL.courbure=a.courb_TOT;
        case 1
            clear l_Oxy c_Oxy penultimatePK_Oxy index_Oxy minim_Oxy tmp_Oxy;
            clear data1_Oxy data2_Oxy;
            VD.CYCL.data_Oxy=data_Oxy;
    end
    %   Si flag_courb=0 le champ "ntypcourb" n'existe pas et la procedure
    %   ini_courbure va l'introduire et mettre VD.CYCL.courbure � 0 =>
    %     VD.CYCL.ntypcourb=0;
    %     VD.CYCL.courbure=0;
end

switch VD.CYCL.ntyppente
    case 1
        VD.CYCL.pente_ligne=a.pente_ligne;
        clear b;
    case 2
        clear l_Oxz c_Oxz penultimatePK_Oxz index_Oxz minim_Oxz tmp_Oxz;
        clear data1_Oxz data2_Oxz;
        VD.CYCL.data_Oxz=data_Oxz;
    case 3
        VD.CYCL.PKpente=VD.CYCL.PKpente_TOT;
        VD.CYCL.penteFpk=VD.CYCL.penteFpk_TOT;
end

if isfield(VD.CYCL,'debutInterStation')
    if exist('PfirstPK','var')
        VD.CYCL.debutInterStation=PfirstPK;
        VD.CYCL.finInterStation=PlastPK;
        VD.CYCL.AllerRetour=VD.CYCL.sens_profils_pente{1};
        clear i PfirstPK PlastPK;
    end
    if exist('CfirstPK','var')
        VD.CYCL.debutInterStation=CfirstPK;
        VD.CYCL.finInterStation=ClastPK;
        VD.CYCL.AllerRetour=VD.CYCL.sens_profils_pente{1};
        clear CfirstPK ClastPK;
    end
end

%[VD]=trait_trajet_coupuresCRT(VD);

end
%--------------------------------------------------------------------------
% Function: pretrait_trajet_ntypcin2
% Script de pre traitement pour mise en forme des tableaux de definition de trajet dans le logiciel VEHLIB.
%
% Objet:
% Script de pre traitement pour mise en forme des tableaux de definition de trajet dans le logiciel VEHLIB.
%
% Cas ou la variable VD.CYCL.ntyptrajet vaut 2: il s'agit d'une
% cinematique type 2 (vitesse en fonction de la distance).
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [erreur, VD]=pretrait_trajet_ntypcin2(VD)

erreur=0;

% TRAITEMENT COMUN de DONNEES
%--------------------------------------------------------------------------
flag_pente=1;flag_courb=1;

for i=1:length(VD.CYCL.ligne_trajet)
    eval(VD.CYCL.ligne_trajet{i})
    if i==1
        VD.CYCL.djerk=CYCL.djerk;
        VD.CYCL.acclig=CYCL.acclig;
        VD.CYCL.dcclig=CYCL.dcclig;
    end

    %__________________________________________
    % Generation du profil de vitesse en fonction de la distance :
    if (i==1)
        if (VD.CYCL.sens_profils_vitesse{1}==1)
            VD.CYCL.def_ligne=CYCL.def_ligne;
        else
            VD.CYCL.def_ligne=flipud(CYCL.def_ligne);
        end
    else
        if (VD.CYCL.sens_profils_vitesse{i}==1)
            VD.CYCL.def_ligne=[VD.CYCL.def_ligne; CYCL.def_ligne];
        else
            VD.CYCL.def_ligne=[VD.CYCL.def_ligne; flipud(CYCL.def_ligne)];
        end
    end

        %__________________________________________
    % Generation du profil de consommation des accessoires en fonction de la distance :
    if (i==1)
            VD.CYCL.acces_ligne=CYCL.acces_ligne;
    else
            VD.CYCL.acces_ligne=[VD.CYCL.acces_ligne; CYCL.acces_ligne];
    end

    %__________________________________________
    % Traitement des courbures du trajet.
    if ~isfield(VD.CYCL, 'ntypcourb') || (flag_courb==0)
        % Compatibilit� avec anterieur.
        % Si le champ n'existe pas dans une des interstations, la courbure
        %   du trajet sera constante et egale � 0 et le flag_courb=0.
        flag_courb=0;
        disp('pretrait_trajet_ntypcin2 : une valeur "ntypcourb" non definie. courbure nulle du trajet entier')
    end
    if (flag_courb==1)
        % Initialisation trajet selon la premiere inter station
        if (i==1)
            switch VD.CYCL.ntypcourb
                case 0
                    if (VD.CYCL.sens_profils_pente{1}==1)
                        a.courb_TOT=VD.CYCL.courbure;
                    else
                        a.courb_TOT=-VD.CYCL.courbure;
                    end
                case 1
                    data_Oxy=VD.CYCL.data_Oxy;
                    CfirstPK =VD.CYCL.debutInterStation;
                    ClastPK  =VD.CYCL.finInterStation;
            end
        else
            % Traitement du reste des interstations
            switch VD.CYCL.ntypcourb
                case 0

                case 1
                    if (CfirstPK<VD.CYCL.finInterStation)
                        % Rajout des donn�es "2" apr�s les donn�es "1". Les
                        % donn�es "2" sont les nouveaux donn�es � rajouter.
                        ClastPK=VD.CYCL.finInterStation;
                        data1_Oxy=data_Oxy;
                        data2_Oxy=VD.CYCL.data_Oxy;
                    else
                        % Rajout des donn�es "2" apr�s les donn�es "1". Les
                        % donn�es "2" sont les nouveaux donn�es � rajouter.
                        CfirstPK=VD.CYCL.debutInterStation;
                        data1_Oxy=VD.CYCL.data_Oxy;
                        data2_Oxy=data_Oxy;
                    end                    
                    [l_Oxy,c_Oxy]=size(data1_Oxy);
                    penultimatePK_Oxy=data1_Oxy(l_Oxy-1,1);
                    tmp_Oxy=data1_Oxy(1:l_Oxy-1,:);
                    [minim_Oxy,index_Oxy]=min(abs(data2_Oxy(:,1)-penultimatePK_Oxy));
                    [l_Oxy,c_Oxy]=size(data2_Oxy);
                    if (index_Oxy+1<l_Oxy)
                        data_Oxy=[tmp_Oxy;data2_Oxy(index_Oxy+1:l_Oxy,:)];
                    end
            end
        end
    end
    if isfield(VD.CYCL, 'ntypcourb')
        saveNTYPCOURB=VD.CYCL.ntypcourb;
        VD.CYCL=rmfield(VD.CYCL,'ntypcourb');
    end

    %__________________________________________
    % Traitement des pentes du trajet.
%     if (i==1)
%         if ~isfield(VD.CYCL,'pente')
%             disp('pretrait_trajet_ntypcin2 : une valeur "VD.CYCL.pente" non definie. Valeur prise egale a 0')
%             VD.CYCL.pente=0;
%         end
%         premierePente=VD.CYCL.pente;
% 
%     end
%     if ~isfield(VD.CYCL, 'ntyppente') | (flag_pente==0)
%         % Compatibilit� avec anterieur.
%         % Si le champ n'existe pas dans une des interstations, la pente du
%         %   trajet sera constante et egale � la variable VD.CYCL.pente de la
%         %   PREMIERE interstation du trajet.
%         flag_pente=0;
%         disp('pretrait_trajet_ntypcin2 : une valeur "ntyppente" non definie. pente constante')
%     end
    if (flag_pente==1)
        % Initialisation trajet selon la premiere inter station
        if (i==1)
            switch VD.CYCL.ntyppente
                case 0
                    if (VD.CYCL.sens_profils_pente{1}==1)
                        VD.CYCL.pente=CYCL.pente;
                    else
                        VD.CYCL.pente=-CYCL.pente;
                    end
                case 1
                    if (VD.CYCL.sens_profils_pente{1}==1)
                        a.pente_ligne=CYCL.pente_ligne;
                    else
                        a.pente_ligne(:,1)=CYCL.pente_ligne(:,1);
                        a.pente_ligne(:,2)=-1*CYCL.pente_ligne(:,2);
                        a.pente_ligne=flipud(a.pente_ligne);
                    end
                case 2
                    data_Oxz=VD.CYCL.data_Oxz;
                    PfirstPK =VD.CYCL.debutInterStation;
                    PlastPK  =VD.CYCL.finInterStation;
                case 3
                    if (VD.CYCL.sens_profils_pente{1}==1)
                       VD.CYCL.PKpente_TOT=VD.CYCL.PKpente;
                       VD.CYCL.penteFpk_TOT=VD.CYCL.penteFpk; 
                    else
                       VD.CYCL.PKpente_TOT=-(fliplr(VD.CYCL.PKpente-VD.CYCL.PKpente(end)));
                       VD.CYCL.penteFpk_TOT=-fliplr(VD.CYCL.penteFpk); 
                    end
            end
        else
            % Traitement du reste des interstations
            switch VD.CYCL.ntyppente
                case 1
                    if (VD.CYCL.sens_profils_pente{i}==1)
                        a.pente_ligne=[a.pente_ligne; CYCL.pente_ligne];
                    else
                        b.pente_ligne(:,1)=CYCL.pente_ligne(:,1);
                        b.pente_ligne(:,2)=-1*CYCL.pente_ligne(:,2);
                        b.pente_ligne=flipud(b.pente_ligne);
                        a.pente_ligne=[a.pente_ligne; b.pente_ligne];
                    end
                case 2
                    if (PfirstPK<VD.CYCL.finInterStation)
                        % Rajout des donn�es "2" apr�s les donn�es "1". Les
                        % donn�es "2" sont les nouveaux donn�es � rajouter.
                        PlastPK=VD.CYCL.finInterStation;
                        data1_Oxz=data_Oxz;
                        data2_Oxz=VD.CYCL.data_Oxz;
                    else
                        % Rajout des donn�es "2" apr�s les donn�es "1". Les
                        % donn�es "2" sont les nouveaux donn�es � rajouter.
                        PfirstPK=VD.CYCL.debutInterStation;
                        data1_Oxz=VD.CYCL.data_Oxz;
                        data2_Oxz=data_Oxz;
                    end
                    [l_Oxz,c_Oxz]=size(data1_Oxz);
                    penultimatePK_Oxz=data1_Oxz(l_Oxz-1,1);
                    tmp_Oxz=data1_Oxz(1:l_Oxz-1,:);
                    [minim_Oxz,index_Oxz]=min(abs(data2_Oxz(:,1)-penultimatePK_Oxz));
                    [l_Oxz,c_Oxz]=size(data2_Oxz);
                    if (index_Oxz+1<l_Oxz)
                        data_Oxz=[tmp_Oxz;data2_Oxz(index_Oxz+1:l_Oxz,:)];
                    end
                case 3
                    if (VD.CYCL.sens_profils_pente{1}==1)
                       VD.CYCL.PKpente_TOT=[VD.CYCL.PKpente_TOT VD.CYCL.PKpente+VD.CYCL.PKpente_TOT(end)+(VD.CYCL.PKpente(2)-VD.CYCL.PKpente(1))];
                       VD.CYCL.penteFpk_TOT=[VD.CYCL.penteFpk_TOT VD.CYCL.penteFpk]; 
                    else
                       VD.CYCL.PKpente_TOT=[VD.CYCL.PKpente_TOT -(fliplr(VD.CYCL.PKpente-VD.CYCL.PKpente(end)))+(VD.CYCL.PKpente(2)-VD.CYCL.PKpente(1))];
                       VD.CYCL.penteFpk_TOT=[VD.CYCL.penteFpk_TOT -fliplr(VD.CYCL.penteFpk)]; 
                    end 
            end
        end
    end
%     if isfield(VD.CYCL, 'ntyppente')
%         saveNTYPPENTE=VD.CYCL.ntyppente;
%         VD.CYCL=rmfield(VD.CYCL,'ntyppente');
%     end

    %__________________________________________
    % Traitement du nom du trajet.
    if (i==1)
        if isfield(VD.CYCL,'info')
            infoTXT=VD.CYCL.info;
            VD.CYCL=rmfield(VD.CYCL,'info');
        else
            infoTXT='Nom interstation : NON DISPONIBLE';
        end
    else
        if isfield(VD.CYCL,'info')
            infoTXT=strvcat(infoTXT,VD.CYCL.info);
            VD.CYCL=rmfield(VD.CYCL,'info');
        else
            infoTXT=strvcat(infoTXT,'Nom interstation : NON DISPONIBLE');
        end
    end   
end
%--------------------------------------------------------------------------


% TRAITEMENT INDIVIDUEL de DONNEES
%--------------------------------------------------------------------------

VD.CYCL.info=infoTXT;
clear infoTXT;

if (flag_courb==1)
    VD.CYCL.ntypcourb=saveNTYPCOURB;
    clear saveNTYPCOURB;
    switch VD.CYCL.ntypcourb
        case 0
            VD.CYCL.courbure=a.courb_TOT;
        case 1
            clear l_Oxy c_Oxy penultimatePK_Oxy index_Oxy minim_Oxy tmp_Oxy;
            clear data1_Oxy data2_Oxy;
            VD.CYCL.data_Oxy=data_Oxy;
    end
    %   Si flag_courb=0 le champ "ntypcourb" n'existe pas et la procedure
    %   ini_courbure va l'introduire et mettre VD.CYCL.courbure � 0 =>
    %     VD.CYCL.ntypcourb=0;
    %     VD.CYCL.courbure=0;
end

if (flag_pente==1)
%     VD.CYCL.ntyppente=saveNTYPPENTE;
%     clear saveNTYPPENTE;
    switch VD.CYCL.ntyppente
        case 1
            VD.CYCL.pente_ligne=a.pente_ligne;
        case 2
            clear l_Oxz c_Oxz penultimatePK_Oxz index_Oxz minim_Oxz tmp_Oxz;
            clear data1_Oxz data2_Oxz;
            VD.CYCL.data_Oxz=data_Oxz;
        case 3
            VD.CYCL.PKpente=VD.CYCL.PKpente_TOT;
            VD.CYCL.penteFpk=VD.CYCL.penteFpk_TOT; 
    end
else
%     if (VD.CYCL.sens_profils_pente{1}==1)
%         VD.CYCL.pente=premierePente;
%     else
%         VD.CYCL.pente=-premierePente;
%     end
end
clear premierePente;

if isfield(VD.CYCL,'debutInterStation')
    if exist('PfirstPK','var')
        VD.CYCL.debutInterStation=PfirstPK;
        VD.CYCL.finInterStation=PlastPK;
        VD.CYCL.AllerRetour=VD.CYCL.sens_profils_pente{1};
        clear i PfirstPK PlastPK;
    end
    if exist('CfirstPK','var')
        VD.CYCL.debutInterStation=CfirstPK;
        VD.CYCL.finInterStation=ClastPK;
        VD.CYCL.AllerRetour=VD.CYCL.sens_profils_pente{1};
        clear CfirstPK ClastPK;
    end
end

%[VD]=trait_trajet_coupuresCRT(VD);

end

%--------------------------------------------------------------------------
% Function: pretrait_trajet_ntypcin3
% Script de pre traitement pour mise en forme des tableaux de definition de trajet de bus dans le logiciel VEHLIB.
%
% Objet:
% Script de pre traitement pour mise en forme des tableaux de definition de trajet de bus dans le logiciel VEHLIB.
% 
% Cas ou la variable VD.CYCL.ntyptrajet vaut 1 et s'il s'agit d'une
% cinematique type 3 (vitesse en fonction du temps + rapports vitesse).
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [erreur, VD]=pretrait_trajet_ntypcin3(VD)

erreur=0;

% TRAITEMENT COMUN de DONNEES
%--------------------------------------------------------------------------
flag_pente=1;flag_courb=1;

for i=1:length(VD.CYCL.ligne_trajet)
    eval(VD.CYCL.ligne_trajet{i})
    if ~isfield(CYCL,'ntyppente')
        CYCL.ntyppente=0;
    end
    
    %__________________________________________
    % Generation du profil de vitesse en fonction du temps.
    if (i==1)
        a.temps_TOT=CYCL.temps;
        a.vitesse_TOT=CYCL.vitesse;
        a.rappvit_TOT=CYCL.rappvit;
    else
        if CYCL.temps(1)==0
            a.temps_TOT=[a.temps_TOT CYCL.temps+(CYCL.temps(2)-CYCL.temps(1))+a.temps_TOT(end)];
        else
            a.temps_TOT=[a.temps_TOT CYCL.temps+a.temps_TOT(end)];
        end
        a.vitesse_TOT=[a.vitesse_TOT CYCL.vitesse];
        a.rappvit_TOT=[a.rappvit_TOT; CYCL.rappvit];
    end

    %__________________________________________
    % Traitement des courbures du trajet.
    if ~isfield(VD.CYCL, 'ntypcourb') | (flag_courb==0)
        % Compatibilit� avec anterieur.
        % Si le champ n'existe pas dans une des interstations, la courbure
        %   du trajet sera constante et egale � 0 et le flag_courb=0.
        flag_courb=0;
        disp('pretrait_trajet_ntypcin2 : une valeur "ntypcourb" non definie. courbure nulle du trajet entier')
    end
    if (flag_courb==1)
        % Initialisation trajet selon la premiere inter station
        if (i==1)
            switch VD.CYCL.ntypcourb
                case 0
                    if (VD.CYCL.sens_profils_pente{1}==1)
                        a.courb_TOT=VD.CYCL.courbure;
                    else
                        a.courb_TOT=-VD.CYCL.courbure;
                    end
                case 1
                    data_Oxy=VD.CYCL.data_Oxy;
                    CfirstPK =VD.CYCL.debutInterStation;
                    ClastPK  =VD.CYCL.finInterStation;
            end
        else
            % Traitement du reste des interstations
            switch VD.CYCL.ntypcourb
                case 0

                case 1
                    if (CfirstPK<VD.CYCL.finInterStation)
                        % Rajout des donn�es "2" apr�s les donn�es "1". Les
                        % donn�es "2" sont les nouveaux donn�es � rajouter.
                        ClastPK=VD.CYCL.finInterStation;
                        data1_Oxy=data_Oxy;
                        data2_Oxy=VD.CYCL.data_Oxy;
                    else
                        % Rajout des donn�es "2" apr�s les donn�es "1". Les
                        % donn�es "2" sont les nouveaux donn�es � rajouter.
                        CfirstPK=VD.CYCL.debutInterStation;
                        data1_Oxy=VD.CYCL.data_Oxy;
                        data2_Oxy=data_Oxy;
                    end                    
                    [l_Oxy,c_Oxy]=size(data1_Oxy);
                    penultimatePK_Oxy=data1_Oxy(l_Oxy-1,1);
                    tmp_Oxy=data1_Oxy(1:l_Oxy-1,:);
                    [minim_Oxy,index_Oxy]=min(abs(data2_Oxy(:,1)-penultimatePK_Oxy));
                    [l_Oxy,c_Oxy]=size(data2_Oxy);
                    if (index_Oxy+1<l_Oxy)
                        data_Oxy=[tmp_Oxy;data2_Oxy(index_Oxy+1:l_Oxy,:)];
                    end
            end
        end
    end
    if isfield(VD.CYCL, 'ntypcourb')
        saveNTYPCOURB=VD.CYCL.ntypcourb;
        VD.CYCL=rmfield(VD.CYCL,'ntypcourb');
    end

    %__________________________________________
    % Traitement des pentes du trajet.
    if (i==1)
        premierePente=VD.CYCL.pente;
    end
%     if ~isfield(VD.CYCL, 'ntyppente') | (flag_pente==0)
%         % Compatibilit� avec anterieur.
%         % Si le champ n'existe pas dans une des interstations, la pente du
%         %   trajet sera constante et egale � la variable VD.CYCL.pente de la
%         %   PREMIERE interstation du trajet.
%         flag_pente=0;
%         disp('pretrait_trajet_ntypcin2 : une valeur "ntyppente" non definie. pente constante')
%     end
    if (flag_pente==1)
        % Initialisation trajet selon la premiere inter station
        if (i==1)
            switch VD.CYCL.ntyppente
                case 0
                    if (VD.CYCL.sens_profils_pente{1}==1)
                        VD.CYCL.pente=CYCL.pente;
                    else
                        VD.CYCL.pente=-CYCL.pente;
                    end
                case 1
                    if (VD.CYCL.sens_profils_pente{i}==1)
                        a.pente_ligne=VD.CYCL.pente_ligne;
                    else
                        a.pente_ligne(:,1)=VD.CYCL.pente_ligne(:,1);
                        a.pente_ligne(:,2)=-1*VD.CYCL.pente_ligne(:,2);
                        a.pente_ligne=flipud(a.pente_ligne);
                    end
                case 2
                    data_Oxz=VD.CYCL.data_Oxz;
                    PfirstPK =VD.CYCL.debutInterStation;
                    PlastPK  =VD.CYCL.finInterStation;
                case 3
                    if (VD.CYCL.sens_profils_pente{1}==1)
                       VD.CYCL.PKpente_TOT=VD.CYCL.PKpente;
                       VD.CYCL.penteFpk_TOT=VD.CYCL.penteFpk; 
                    else
                       VD.CYCL.PKpente_TOT=-(fliplr(VD.CYCL.PKpente-VD.CYCL.PKpente(end)));
                       VD.CYCL.penteFpk_TOT=-fliplr(VD.CYCL.penteFpk); 
                    end
            end
        else
            % Traitement du reste des interstations
            switch CYCL.ntyppente
                case 0

                case 1
                    if (VD.CYCL.sens_profils_pente{i}==1)
                        a.pente_ligne=[a.pente_ligne; VD.CYCL.pente_ligne];
                    else
                        b.pente_ligne(:,1)=VD.CYCL.pente_ligne(:,1);
                        b.pente_ligne(:,2)=-1*VD.CYCL.pente_ligne(:,2);
                        b.pente_ligne=flipud(b.pente_ligne);
                        a.pente_ligne=[a.pente_ligne; b.pente_ligne];
                    end
                case 2
                    if (PfirstPK<VD.CYCL.finInterStation)
                        % Rajout des donn�es "2" apr�s les donn�es "1". Les
                        % donn�es "2" sont les nouveaux donn�es � rajouter.
                        PlastPK=VD.CYCL.finInterStation;
                        data1_Oxz=data_Oxz;
                        data2_Oxz=VD.CYCL.data_Oxz;
                    else
                        % Rajout des donn�es "2" apr�s les donn�es "1". Les
                        % donn�es "2" sont les nouveaux donn�es � rajouter.
                        PfirstPK=VD.CYCL.debutInterStation;
                        data1_Oxz=VD.CYCL.data_Oxz;
                        data2_Oxz=data_Oxz;
                    end
                    [l_Oxz,c_Oxz]=size(data1_Oxz);
                    penultimatePK_Oxz=data1_Oxz(l_Oxz-1,1);
                    tmp_Oxz=data1_Oxz(1:l_Oxz-1,:);
                    [minim_Oxz,index_Oxz]=min(abs(data2_Oxz(:,1)-penultimatePK_Oxz));
                    [l_Oxz,c_Oxz]=size(data2_Oxz);
                    if (index_Oxz+1<l_Oxz)
                        data_Oxz=[tmp_Oxz;data2_Oxz(index_Oxz+1:l_Oxz,:)];
                    end
                case 3
                    if (VD.CYCL.sens_profils_pente{1}==1)
                       VD.CYCL.PKpente_TOT=[VD.CYCL.PKpente_TOT VD.CYCL.PKpente+VD.CYCL.PKpente_TOT(end)+(VD.CYCL.PKpente(2)-VD.CYCL.PKpente(1))];
                       VD.CYCL.penteFpk_TOT=[VD.CYCL.penteFpk_TOT VD.CYCL.penteFpk]; 
                    else
                       VD.CYCL.PKpente_TOT=[VD.CYCL.PKpente_TOT -(fliplr(VD.CYCL.PKpente-VD.CYCL.PKpente(end)))+(VD.CYCL.PKpente(2)-VD.CYCL.PKpente(1))];
                       VD.CYCL.penteFpk_TOT=[VD.CYCL.penteFpk_TOT -fliplr(VD.CYCL.penteFpk)]; 
                    end                     
            end
        end
    end
    if isfield(VD.CYCL, 'ntyppente')
        saveNTYPPENTE=VD.CYCL.ntyppente;
        VD.CYCL=rmfield(VD.CYCL,'ntyppente');
    end

    %__________________________________________
    % Traitement du nom du trajet.
    if (i==1)
        if isfield(VD.CYCL,'info')
            infoTXT=VD.CYCL.info;
            VD.CYCL=rmfield(VD.CYCL,'info');
        else
            infoTXT='Nom interstation : NON DISPONIBLE';
        end
    else
        if isfield(VD.CYCL,'info')
            infoTXT=strvcat(infoTXT,VD.CYCL.info);
            VD.CYCL=rmfield(VD.CYCL,'info');
        else
            infoTXT=strvcat(infoTXT,'Nom interstation : NON DISPONIBLE');
        end
    end    
end
%--------------------------------------------------------------------------



% TRAITEMENT INDIVIDUEL de DONNEES
%--------------------------------------------------------------------------

VD.CYCL.temps=a.temps_TOT;
VD.CYCL.vitesse=a.vitesse_TOT;
VD.CYCL.rappvit=a.rappvit_TOT;

VD.CYCL.info=infoTXT;
clear infoTXT;

if (flag_courb==1)
    VD.CYCL.ntypcourb=saveNTYPCOURB;
    clear saveNTYPCOURB;
    switch VD.CYCL.ntypcourb
        case 0
            VD.CYCL.courbure=a.courb_TOT;
        case 1
            clear l_Oxy c_Oxy penultimatePK_Oxy index_Oxy minim_Oxy tmp_Oxy;
            clear data1_Oxy data2_Oxy;
            VD.CYCL.data_Oxy=data_Oxy;
    end
    %   Si flag_courb=0 le champ "ntypcourb" n'existe pas et la procedure
    %   ini_courbure va l'introduire et mettre VD.CYCL.courbure � 0 =>
    %     VD.CYCL.ntypcourb=0;
    %     VD.CYCL.courbure=0;
end

if (flag_pente==1)
    VD.CYCL.ntyppente=saveNTYPPENTE;
    clear saveNTYPPENTE;
    switch VD.CYCL.ntyppente
        case 1
            VD.CYCL.pente_ligne=a.pente_ligne;
            clear b;
        case 2
            clear l_Oxz c_Oxz penultimatePK_Oxz index_Oxz minim_Oxz tmp_Oxz;
            clear data1_Oxz data2_Oxz;
            VD.CYCL.data_Oxz=data_Oxz;
        case 3
            VD.CYCL.PKpente=VD.CYCL.PKpente_TOT;
            VD.CYCL.penteFpk=VD.CYCL.penteFpk_TOT; 
    end
else
    if (VD.CYCL.sens_profils_pente{1}==1)
        VD.CYCL.pente=premierePente;
    else
        VD.CYCL.pente=-premierePente;
    end
end
clear premierePente;

if isfield(VD.CYCL,'debutInterStation')
    if exist('PfirstPK','var')
        VD.CYCL.debutInterStation=PfirstPK;
        VD.CYCL.finInterStation=PlastPK;
        VD.CYCL.AllerRetour=VD.CYCL.sens_profils_pente{1};
        clear i PfirstPK PlastPK;
    end
    if exist('CfirstPK','var')
        VD.CYCL.debutInterStation=CfirstPK;
        VD.CYCL.finInterStation=ClastPK;
        VD.CYCL.AllerRetour=VD.CYCL.sens_profils_pente{1};
        clear CfirstPK ClastPK;
    end
end

%[VD]=trait_trajet_coupuresCRT(VD);

end

%--------------------------------------------------------------------------
% Function: trait_trajet_coupuresCRT
% Gestion  des Courpures d'alimentation sur systeme de transport intermediaire
%
%
% Objet:
%
% Gestion  des Courpures d'alimentation sur systeme de transport intermediaire
%
%   Fonction necessaire a la concatenation des donnees "VD.CYCL.dist" et
%   "VD.CYCL.coupuresCRT" pour l'ensemble d'un trajet (quelle que soit le type
%   de cinematique 1, 2 ou 3). 
%
%   Ces variables sont definis dans les fichiers
%   cinematiques (ferroviaires) et proviennent des mesures effectuees sur
%   la ligne 2 du tramway parisien (aout 2001).
%
%   Auteur  : Bogdan Vulturescu, LTN, INRETS
%
%_____________________________________________
% Exemple ('T2_12_experimental.m') :
%
% distance (km)   logique (1 = courant coup�)
% A=[
%    0                  0.01000000000000
%    1.55               0.01000000000000
%    1.55                              0
%    3.65                              0
%    3.65               0.01000000000000
%    5.767              0.01000000000000]*100;
% VD.CYCL.dist=A(:,1)';VD.CYCL.coupuresCRT=A(:,2)';
% clear A
%_____________________________________________


function [VD]=trait_trajet_coupuresCRT(VD)

flag=0;
for i=1:length(VD.CYCL.ligne_trajet),
    fileName=VD.CYCL.ligne_trajet{i};
    eval(fileName);
    % varible VD.CYCL disponible a l'interieur de la fonction. Les
    % modifications sur VD.CYCL faites a l'interieur de cette fonction ne se
    % repercutent pas a l'exterieur.
    if isfield(VD.CYCL,'dist')
        if (i==1)
            VD.CYCL.ligne_trajet=VD.CYCL.dist;
            coupuresCRT=VD.CYCL.coupuresCRT;
        else
            VD.CYCL.dist;
            VD.CYCL.ligne_trajet=[VD.CYCL.ligne_trajet VD.CYCL.ligne_trajet(end)+VD.CYCL.dist];
            coupuresCRT=[coupuresCRT VD.CYCL.coupuresCRT];
        end
    else
        % Le champ "dist" n'existe pas dans une des inter stations
        % choisies, donc "VD.CYCL.dist" et "VD.CYCL.coupuresCRT" seront mises a
        % zero a la fin de la procedure.
        flag=1;
    end

end
VD.CYCL.coupuresCRT=coupuresCRT;

if flag
    distance=0;coupuresCRT=0;
end

end
