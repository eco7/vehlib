% Function: vehsim_gui
% Interface homme-machine pour la s??lection d'un vehicule dans VEHLIB
%
%  (C) Copyright IFSTTAR LTE 1999-2011
%
% Objet:
% Interface homme-machine pour la selection d'un vehicule dans VEHLIB
% choix d'une architecture de vehicule,
% des fichiers composants
% de la cinematique
% et qques parametres initiaux (dod, charge du vehicule, ...).
%
% Usage:
% vehsim_gui
%
% (see vehsim.png)
% -----------------------------------------------------------
function [vehlib, VD]=vehsim_gui(vehlib,VD)

if nargin == 0
    vehlib=struct('architecture','VTH_BV');
else
    % vehsim_gui est appele depuis les modeles simulink:
    % Il faut supprimer les variables de la simulation precedente du workspace de matlab !
    evalin('base','clearvars -except VD vehlib');
end

% S'il existe deja une figure avec ce tag, on la supprime.
if ~isempty(findobj('Tag','Vehlib_fig'))
    close(findobj('Tag','Vehlib_fig'));
end

% Constitution des listes des vehicules pour l'architecture 'archtecture'.
[liste_arch, liste_veh, texte]=vehlib_liste(vehlib.architecture);
% Recherche de l'architecture dans la liste
for ind_arch=1:length(liste_arch(1,:))
    if  strcmp(liste_arch(ind_arch),vehlib.architecture)
        break
    end
end

if nargin == 0
    % Lecture du premier jeu de donnees a l'initialisation
    ind_veh=1;
    [vehlib, err]=lectureModel(liste_veh{ind_veh}, vehlib.architecture);
    if err
        return;
    end
    [VD, err]=lectureData(vehlib);
    if err
        return;
    end
else
    for ind_veh=1:length(liste_veh(1,:))
        if  strcmp(liste_veh(ind_veh),vehlib.nom)
            break
        end
    end
    % BJ 17/03/2016 Not necessary. Will erase old data
    %[vehlib, err]=lectureModel(vehlib.nom, vehlib.architecture);
end
% if err
%     return;
% end
% [VD, err]=lectureData(vehlib);
% if err
%     return;
% end

% Position et facteur d'echelle pour un gui correct avec une resolution d ecran de 1400*1050
figurePos=[100 150 450 600];
[pixfactor,Taille_car,figurePos]=gui_taille(figurePos);

figurePos=figurePos.*pixfactor;
btnHt = 40;
txtArcPos = [45 500 330 60].* pixfactor;

selArchPos = [25 440 150 btnHt].* pixfactor;
popArchPos = [200 450 200 btnHt].* pixfactor;

selVehPos = [25 390 150 btnHt].* pixfactor;
popVehPos = [200 400 200 btnHt].* pixfactor;

frameCycPos= [15 285 400 120].* pixfactor;
%RdioCinPos=[20 300 btnHt btnHt].* pixfactor;
TxtCinPos  = [70 310 160 btnHt/2].* pixfactor;
PushCinPos = [250 300 150 btnHt].* pixfactor;

%RdioMesPos=[20 350 btnHt btnHt].* pixfactor;
%TxtMesPos  = [70 360 160 btnHt/2].* pixfactor;
%PushMesPos = [250 350 150 btnHt].* pixfactor;

txtCIPos=[115 260 200 20].* pixfactor;
frameCIPos=[25-1 105-1 300+btnHt+25+1 180].*pixfactor;

RdioDodPos=[25 245 btnHt 25].* pixfactor;
txtDodPos   =[25+btnHt 230 300-btnHt 25].* pixfactor;
EditDodPos   =[325 235 50 25].* pixfactor;

RdioU0Pos=[25 220 btnHt 25].* pixfactor;
txtCbatPos   =[25+btnHt 205 300-btnHt 25].* pixfactor;
EditCbatPos   =[325 210 50 25].* pixfactor;

txtCscPos   =[25+btnHt 180 300-btnHt 25].* pixfactor;
EditCscPos   =[325 185 50 25].* pixfactor;

txtTambPos   =[25 155 300 25].* pixfactor;
EditTambPos   =[325 160 50 25].* pixfactor;

txtchargePos   =[25 130 300 25].* pixfactor;
EditchargePos   =[325 135 50 25].* pixfactor;

txtpaccPos   =[25 105 300 25].* pixfactor;
EditpaccPos   =[325 110 50 25].* pixfactor;

BtnValPos = [50 25 100 btnHt].* pixfactor;
BtnAnnPos = [265 25 100 btnHt].* pixfactor;

FigH = figure('Color',[0.5 0.5 0.75], ...
    'MenuBar','none', ...
    'Name','Choix d''un vehicule', ...
    'Units','pixels', ...
    'NumberTitle','off', ...
    'Resize','off', ...
    'Position',figurePos, ...
    'Visible','off',...
    'Tag','Vehlib_fig');

setMyAppData(FigH,vehlib,VD);

uicontrol('Parent',FigH, ...
    'BackGroundColor',[0. 0.5 0.75], ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Position',txtArcPos, ...
    'Style','text', ...
    'Fontsize',Taille_car+2, ...
    'String',texte, ...
    'Tag','tagtextarch');

uicontrol('Parent',FigH, ...
    'BackGroundColor',[0.5 0.5 0.75], ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Position',selArchPos, ...
    'Style','text', ...
    'String','Choix de l''architecture :', ...
    'Fontsize',Taille_car, ...
    'Tag','tagTxtArch');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Position',popArchPos, ...
    'Value',ind_arch,...
    'String',liste_arch, ...
    'Fontsize',Taille_car, ...
    'Style','popupmenu', ...
    'Tag','tagPopArch', ...
    'CallBack',{@majPopupmenuVehicule,FigH});

uicontrol('Parent',FigH, ...
    'BackGroundColor',[0.5 0.5 0.75], ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Position',selVehPos, ...
    'Style','text', ...
    'Fontsize',Taille_car, ...
    'String','Choix du vehicule :', ...
    'Tag','tagTxtchoix');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Position',popVehPos, ...
    'Value',ind_veh,...
    'String',liste_veh, ...
    'Fontsize',Taille_car, ...
    'Style','popupmenu', ...
    'Tag','tagPopVeh', ...
    'CallBack',{@lectureDataModelGui,FigH});

% Partie cycle
uicontrol('Parent',FigH, ...
    'BackgroundColor',[0.5 0.5 0.5], ...
    'Units','pixels', ...
    'Position',frameCycPos, ...
    'Style','frame', ...
    'Tag','FrameCin');
%     uicontrol('Parent',FigH, ...
%         'Units','pixels', ...
%         'BackgroundColor',[0.5 0.5 0.5], ...
%         'Position',RdioCinPos, ...
%         'value',1,...
%         'Style','radiobutton', ...
%         'Tag','tagRdioCin', ...
%         'Callback','');
uicontrol('Parent',FigH, ...
    'BackGroundColor',[0.5 0.5 0.75], ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Userdata','cycle', ...
    'Position',TxtCinPos, ...
    'Style','text', ...
    'Fontsize',Taille_car, ...
    'String','Choix de la cinematique :', ...
    'Tag','tagTxtchoixcin');

uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Userdata','cycle', ...
    'Position',PushCinPos, ...
    'String',vehlib.CYCL, ...
    'Fontsize',Taille_car, ...
    'Style','pushbutton', ...
    'Tag','tagPushcin', ...
    'CallBack',{@lectureCinGui,FigH});

%     uicontrol('Parent',FigH, ...
%         'Units','pixels', ...
%         'BackgroundColor',[0.5 0.5 0.5], ...
%         'Callback','', ...
%         'Position',RdioMesPos, ...
%         'Value',0,...
%         'Style','radiobutton', ...
%         'Tag','tagRdioMes');
%     uicontrol('Parent',FigH, ...
%         'BackGroundColor',[0.5 0.5 0.75], ...
%         'Units','pixels', ...
%         'Userdata','mesure', ...
%         'ListboxTop',0, ...
%         'Position',TxtMesPos, ...
%         'enable','off',...
%         'Style','text', ...
%         'String','Choix d un fichier de mesure :', ...
%         'Fontsize',Taille_car, ...
%         'Tag','tagTxtchoixmes');
%     uicontrol('Parent',FigH, ...
%         'Units','pixels', ...
%         'Userdata','mesure', ...
%         'enable','off',...
%         'ListboxTop',0, ...
%         'Position',PushMesPos, ...
%         'Value',1,...
%         'String','Choix', ...
%         'Fontsize',Taille_car, ...
%         'Style','pushbutton', ...
%         'Tag','tagPushmes', ...
%         'CallBack','');

uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Enable','on', ...
    'Style','pushbutton', ...
    'Position',BtnValPos, ...
    'String','Valider', ...
    'Fontsize',Taille_car, ...
    'Tag','TagPushVal', ...
    'Callback',{@valideVehsim_gui,FigH});
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Enable','on', ...
    'Position',BtnAnnPos, ...
    'String','Annuler', ...
    'Fontsize',Taille_car, ...
    'Callback','close(''gcbf'')', ...
    'Tag','TagPushAnn');

% Affichage des conditions initiales
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',frameCIPos, ...
    'Style','frame', ...
    'Tag','FrameCI');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',txtCIPos, ...
    'String','Conditions initiales du calcul :',...
    'Fontsize',Taille_car, ...
    'Style','Text', ...
    'Tag','tagTxtCI');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',RdioDodPos, ...
    'value',1,...
    'Style','radiobutton', ...
    'Tag','tagRdioDod', ...
    'Callback',{@modDod_U0,'Dod',FigH});
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',txtDodPos, ...
    'Style','text', ...
    'Userdata','batterieDod', ...
    'String','Profondeur de decharge initiale batterie en % :', ...
    'Fontsize',Taille_car, ...
    'Tag','tagTxtDod');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',EditDodPos, ...
    'Style','edit', ...
    'Userdata','batterieDod', ...
    'String','', ...
    'Tag','TagEditDod', ...
    'Fontsize',Taille_car, ...
    'Callback',{@modParam,'VD.INIT.Dod0',FigH});
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',RdioU0Pos, ...
    'value',0,...
    'Style','radiobutton', ...
    'Tag','tagRdioU0', ...
    'Callback',{@modDod_U0,'U0',FigH});
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',txtCbatPos, ...
    'Style','text', ...
    'Userdata','batterieU0', ...
    'Fontsize',Taille_car, ...
    'Enable','off', ...
    'String','Tension batterie au repos en V :', ...
    'Tag','TagTxtCbat');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',EditCbatPos, ...
    'Style','edit', ...
    'Userdata','batterieU0', ...
    'Enable','off', ...
    'String','', ...
    'Fontsize',Taille_car, ...
    'Tag','TagEditCbat', ...
    'Callback',{@modParam,'VD.INIT.Ubat0',FigH});
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',txtCscPos, ...
    'Style','text', ...
    'Fontsize',Taille_car, ...
    'Enable','off', ...
    'String','Tension SC au repos en V :', ...
    'Tag','TagTxtCsc');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',EditCscPos, ...
    'Style','edit', ...
    'Enable','off', ...
    'String','', ...
    'Fontsize',Taille_car, ...
    'Tag','TagEditCsc', ...
    'Callback',{@modParam,'VD.INIT.Vc0',FigH});

uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',txtTambPos, ...
    'Style','text', ...
    'String','Temperature ambiante en Celsius :', ...
    'Fontsize',Taille_car, ...
    'Tag','tagTxtTamb');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',EditTambPos, ...
    'Style','edit', ...
    'Fontsize',Taille_car, ...
    'String','', ...
    'Tag','TagEditTamb', ...
    'Callback',{@modParam,'VD.INIT.Tamb',FigH});

uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',txtchargePos, ...
    'Style','text', ...
    'Fontsize',Taille_car, ...
    'String','Charge passager en kg :', ...
    'Tag','TagTxtCharge');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',EditchargePos, ...
    'Style','edit', ...
    'Fontsize',Taille_car, ...
    'String','', ...
    'Tag','TagEditCharge', ...
    'Callback',{@modParam,'VD.VEHI.Charge',FigH});

uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',txtpaccPos, ...
    'Style','text', ...
    'Fontsize',Taille_car, ...
    'String','Consommation electrique des accessoires en W :', ...
    'Tag','TagTxtPacc');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',EditpaccPos, ...
    'Style','edit', ...
    'String','', ...
    'Fontsize',Taille_car, ...
    'Tag','TagEditPacc', ...
    'Callback',{@modParam,'VD.ACC.Pacc_elec',FigH});

if isfield(vehlib,'BATT')
    % vehicule avec batterie
    set(findobj('tag','TagEditDod'),'string',num2str(VD.INIT.Dod0));
    set(findobj('tag','TagEditCbat'),'string',num2str(VD.INIT.Ubat0));
else
    % Pas de batterie
    set(findobj('tag','tagRdioDod'),'visible','off');
    set(findobj('tag','tagRdioU0'),'visible','off');
    set(findobj('Userdata','batterieDod'),'enable','off');
    set(findobj('Userdata','batterieU0'),'enable','off');
end
if isfield(vehlib,'SC')
    set(findobj('tag','TagEditCsc'),'string',num2str(VD.SC.Vc0));
    set(findobj('tag','TagTxtCsc'),'enable','on');
    set(findobj('tag','TagEditCsc'),'enable','on');
else
    set(findobj('tag','TagEditCsc'),'enable','off');
    set(findobj('tag','TagTxtCsc'),'enable','off');
end
set(findobj('tag','TagEditTamb'),'string',num2str(VD.INIT.Tamb));
if isfield(VD,'VEHI')
    set(findobj('tag','TagEditCharge'),'string',num2str(VD.VEHI.Charge));
end
if isfield(VD,'ACC')
    set(findobj('tag','TagEditPacc'),'string',num2str(VD.ACC.Pacc_elec));
end

if nargin ~=0
    % L'interface est appelee depuis les modeles simulink
    set(findobj('Tag','tagPopArch'),'enable','off');
end

set(findobj('tag','Vehlib_fig'),'visible','on');
waitfor(findobj('tag','Vehlib_fig'));

% elseif strcmp(action,'valide')
%
%     % Recuperation des donnees
%     [vehlib,VD]=getMyAppData(findobj('Tag','Vehlib_fig'));
%
%     % Ouverture du modele simulink
%     open_system(vehlib.simulink);
%
%     % Initialisation des parametres propres a la simulation
%     [erreur, vehlib, VD]=ini_calcul(vehlib, VD);
%     if erreur
%         disp('Erreur a l execution de ini_calcul');
%         close(gcbf);
%     end
%
%     % on affecte les 2 structures dans le caller !
%     % C'est pas tres esthetique, mais rien trouve d'autre !!!
%     assignin('caller','vehlib',vehlib);
%     assignin('caller','VD',VD);
%
%     % Fermeture de le fenetre vehsim_gui
%     close(gcbf);
%
% end

end

% ----------------------------------------------------------------
% Function: modDod_U0
%
% Modification du radio bouton Dod ou U0 initiale
% ----------------------------------------------------------------

function modDod_U0(~,~,choix,FigH)

% Recuperation des donnees
[vehlib,VD]=getMyAppData(FigH);

if strcmp(choix,'Dod')
    set(findobj('userdata','batterieU0'),'enable','off');
    set(findobj('userdata','batterieDod'),'enable','on');
    set(findobj('tag','tagRdioU0'),'value',0);
elseif strcmp(choix,'U0')
    set(findobj('userdata','batterieU0'),'enable','on');
    set(findobj('userdata','batterieDod'),'enable','off');
    set(findobj('tag','tagRdioDod'),'value',0);
end

if strcmp(choix,'Dod') || strcmp(choix,'CalDod')
    
    % Calcul de la dod correspond0ante
    if VD.BATT.ntypbat~=10
        if length(unique(VD.BATT.ocv(1,:)))~=length((VD.BATT.ocv(1,:)))
            errordlg('Probleme avec interp1: The values of  VD.BATT.ocv(1,:) should be distinct.');
            set(findobj('userdata','batterieU0'),'enable','on');
            set(findobj('userdata','batterieDod'),'enable','off');
            return
        end
        C=interp1(VD.BATT.ocv(1,:),VD.BATT.dod_ocv,VD.INIT.Ubat0);
        if isnan(C)
            errordlg('La Tension initiale est en dehors des limites');
            set(findobj('userdata','batterieU0'),'enable','on');
            set(findobj('userdata','batterieDod'),'enable','off');
            return
        end
        VD.INIT.Dod0=C;
        set(findobj('tag','TagEditDod'),'string',num2str(VD.INIT.Dod0));
    end
    if VD.BATT.ntypbat==10
        VD.INIT.Dod0=100-(VD.INIT.Ubat0-VD.ECU.BMS_Ubat_ini_param1)./VD.ECU.BMS_Ubat_ini_param2;
        set(findobj('tag','TagEditDod'),'string',num2str(evalin('base','VD.INIT.Dod0')));
    end
    
elseif strcmp(choix,'U0')
    
    % Calcul de la tension a vide correspondante
    if VD.BATT.ntypbat~=10
        if length(unique(VD.BATT.ocv(1,:)))~=length((VD.BATT.ocv(1,:)))
            errordlg('Probleme avec interp1: The values of  VD.BATT.ocv(1,:) should be distinct.');
            set(findobj('userdata','batterieU0'),'enable','off');
            set(findobj('userdata','batterieDod'),'enable','on');
            return
        end
        C=interp1(VD.BATT.dod_ocv,VD.BATT.ocv(1,:),VD.INIT.Dod0);
        if isnan(C)
            errordlg('La Dod initiale est en dehors des limites');
            set(findobj('userdata','batterieU0'),'enable','off');
            set(findobj('userdata','batterieDod'),'enable','on');
            return
        end
        VD.INIT.Ubat0=C;
        set(findobj('tag','TagEditCbat'),'string',num2str(VD.INIT.Ubat0));
    end
    if VD.BATT.ntypbat==10
        VD.INIT.Ubat0=VD.ECU.BMS_Ubat_ini_param1+VD.ECU.BMS_Ubat_ini_param2*(100-VD.INIT.Dod0);
        set(findobj('tag','TagEditCbat'),'string',num2str(VD.INIT.Ubat0));
    end
    
end

% Stockage des donnes
setMyAppData(FigH,vehlib,VD);


end

% ----------------------------------------------------------------
% Function: modParam
%
% Modification d'un parametre depuis l'interface
% ----------------------------------------------------------------
function modParam(src,~,nom,FigH)

% Recuperation des donnees
[vehlib,VD]=getMyAppData(FigH);

valeur=str2double(get(src,'string'));

if isempty(valeur) || ~isnumeric(valeur)
    errordlg('Valeur non compatible','vehlib','modal')
    set(src,'string',num2str(eval(nom)));
    return
end

% Mise a jour du champ de la structure VD
eval([nom,'=',num2str(valeur),';']);

if strcmp(nom,'VD.INIT.Ubat0')
    % Il faut mettre a jour la valeur de Dod0
    modDod_U0([],[],'CalDod',FigH);
end

% Stockage des donnes
setMyAppData(FigH,vehlib,VD);

end

% ----------------------------------------------------------------
% Function: valideVehsim_gui
%
% Bouton de validation de l'interface
% ----------------------------------------------------------------
function valideVehsim_gui(~,~,FigH)

% Recuperation des donnees
[vehlib,VD]=getMyAppData(FigH);

% Ouverture du modele simulink
open_system(vehlib.simulink);

% Initialisation des parametres propres a la simulation
[erreur, vehlib, VD]=ini_calcul(vehlib, VD);
if erreur
    disp('Erreur a l execution de ini_calcul');
    close(gcbf);
end

% on affecte les 2 structures dans le caller !
% C'est pas tres esthetique, mais rien trouve d'autre !!!
assignin('caller','vehlib',vehlib);
assignin('caller','VD',VD);

% Fermeture de le fenetre vehsim_gui
close(gcbf);

end
%
% ----------------------------------------------------------------
% Function: majPopupmenuVehicule
%
% Modification d'un vehicule depuis l'interface
% et lecture des donnees du vehicule
% ----------------------------------------------------------------
function majPopupmenuVehicule(src,~,FigH)

Value=get(src,'Value');
liste_arch=get(src,'String');
architecture=liste_arch{Value};

%disp(['Function: majPopupmenuVehicule ',architecture]);

[~, liste_veh, texte]=vehlib_liste(architecture);

% Mise a jour de l interface
set(findobj('tag','tagPopVeh'),'string',liste_veh,'value',1);
set(findobj('tag','tagtextarch'),'string',texte);

lectureDataModelGui([],[],FigH);

end

% ----------------------------------------------------------------
% Function: lectureDataModelGui
%
% Lecture des donnees du vehicule
% Renseignement des structures vehlib et VD
% Mise a jour des champs modifiables de l'interface
% ----------------------------------------------------------------
function lectureDataModelGui(~,~,FigH)

% Recherche du numero du modele dans la liste
Value=get(findobj('tag','tagPopVeh'),'value');
liste_veh=get(findobj('tag','tagPopVeh'),'String');
vehicule=liste_veh{Value};

Value=get(findobj('tag','tagPopArch'),'value');
liste_arch=get(findobj('tag','tagPopArch'),'String');
architecture=liste_arch{Value};

%disp(['Function: lectureDataModelGui ',vehicule]);

% Lecture des fichiers de donnees composants
[vehlib, err]=lectureModel(vehicule,architecture);
if err
    close(gcbf);
    return
end
[VD, err]=lectureData(vehlib);
if err
    disp(['Erreur lors de la lecture du fichier : ',vehlib.nom])
    close(gcbf);
    return
end

% Stockage des donnes
setMyAppData(FigH,vehlib,VD);

% Mise a jour de la cinematique
if isfield(vehlib,'CYCL')
    set(findobj('Tag','tagPushcin'),'String',vehlib.CYCL);
    set(findobj('userdata','cycle'),'Visible','on');
else
    set(findobj('userdata','cycle'),'Visible','off');
    
end

% if isfield(vehlib,'CYCL')
%     set(findobj('userdata','mesure'),'Visible','on');
%     % on efface les radio bouton cinematique et mesures
%     set(findobj('Tag','tagRdioCin'),'Visible','on');
%     set(findobj('Tag','tagRdioMes'),'Visible','on');
% else
%     set(findobj('userdata','mesure'),'Visible','off');
%     % on efface les radio bouton cinematique et mesures
%     set(findobj('Tag','tagRdioCin'),'Visible','off');
%     set(findobj('Tag','tagRdioMes'),'Visible','off');
% end

% Valeur pour la charge en passager
if isfield(vehlib,'VEHI')
    set(findobj('tag','TagTxtCharge'),'enable','on');
    set(findobj('tag','TagEditCharge'),'enable','on');
    set(findobj('tag','TagEditCharge'),'string',num2str(VD.VEHI.Charge));
else
    set(findobj('tag','TagEditCharge'),'enable','off');
    set(findobj('tag','TagTxtCharge'),'enable','off');
end

% Valeur pour la conso des auxiliaires
if isfield(vehlib,'ACC')
    set(findobj('tag','TagTxtPacc'),'enable','on');
    set(findobj('tag','TagEditPacc'),'enable','on');
    
    if isfield(vehlib,'ACC')
        set(findobj('tag','TagEditPacc'),'string',num2str(VD.ACC.Pacc_elec));
    end
else
    set(findobj('tag','TagEditPacc'),'enable','off');
    set(findobj('tag','TagTxtPacc'),'enable','off');
end

% Validation/ invalidation des CI batterie
% Mise a jour des conditions initiales
set(findobj('tag','TagEditDod'),'string',num2str(VD.INIT.Dod0));
set(findobj('tag','TagEditCbat'),'string',num2str(VD.INIT.Ubat0));
set(findobj('tag','TagEditTamb'),'string',num2str(VD.INIT.Tamb));
if isfield(vehlib,'BATT')
    if get(findobj('tag','tagRdioDod'),'value')==1
        % L'utilisateur a renseigne la Dod
        set(findobj('Userdata','batterieDod'),'enable','on');
        set(findobj('Userdata','batterieU0'),'enable','off');
    else
        % L'utilisateur a renseigne la tension au repos.
        set(findobj('Userdata','batterieDod'),'enable','off');
        set(findobj('Userdata','batterieU0'),'enable','on');
    end
    set(findobj('Tag','tagRdioDod'),'visible','on');
    set(findobj('Tag','tagRdioU0'),'visible','on');
else
    % Pas de batterie
    set(findobj('Userdata','batterieDod'),'enable','off');
    set(findobj('Userdata','batterieU0'),'enable','off');
    set(findobj('Tag','tagRdioDod'),'visible','off');
    set(findobj('Tag','tagRdioU0'),'visible','off');
end

% Validation/ invalidation des CI Super Cap
% Mise a jour des conditions initiales
set(findobj('tag','TagEditCsc'),'string',num2str(VD.INIT.Vc0));
if isfield(vehlib,'SC')
    set(findobj('tag','TagEditCsc'),'string',num2str(VD.SC.Vc0));
    set(findobj('tag','TagEditCsc'),'enable','on');
    set(findobj('tag','TagTxtCsc'),'enable','on');
else
    set(findobj('tag','TagEditCsc'),'enable','off');
    set(findobj('tag','TagTxtCsc'),'enable','off');
end

end

% ----------------------------------------------------------------
% Function: lectureCinGui
%
% Lecture des donnees cycle et mise a jour de la structure VD
% ----------------------------------------------------------------
function lectureCinGui(~,~,FigH)

% Recuperation des donnees
[vehlib,VD]=getMyAppData(FigH);

p=pwd;
cd(VD.INIT.RoadFolder);
[fichier,~]=uigetfile;
if isnumeric(fichier)
    return
end
[~,fichier,~]=fileparts(fichier);
set(findobj('tag','tagPushcin'),'string',fichier);
cd(p);

old_cycle=vehlib.CYCL;
vehlib.CYCL=fichier;
[ VD, err ] = lectureData( vehlib, 2, {'CYCL'}, VD );
if err
    errordlg(['Erreur a la lecture de la cinematique : ',fichier]);
    vehlib.CYCL=old_cycle;
    [ VD, err ] = lectureData( vehlib, 2, {'CYCL'}, VD );
    set(findobj('tag','tagPushcin'),'string',old_cycle);
end

% Stockage des donnees
setMyAppData(FigH,vehlib,VD);

end

% ----------------------------------------------------------------
% Function: setUserData
%
% Stockage des donnees de vehlib dans la userdata de la figure
% ----------------------------------------------------------------
function setMyAppData(FigH,vehlib,VD)
guiStruct=struct('vehlib',vehlib,'VD',VD);
setappdata(FigH,'guiStruct',guiStruct);

end

% ----------------------------------------------------------------
% Function: getUserData
%
% Recuperation des donnees de vehlib dans la userdata de la figure
% ----------------------------------------------------------------
function [vehlib,VD]=getMyAppData(FigH)

guiStruct=getappdata(FigH,'guiStruct');

vehlib=guiStruct.vehlib;
VD=guiStruct.VD;

end