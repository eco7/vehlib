% Function: lectureModel
% function [ vehlib, err ] = lectureModel( modelName )
% Lecture du fichier modele et initialisation de la structure vehlib
%
%  (C) Copyright IFSTTAR LTE 1999-2014 
%
% Objet:
%
%  Lecture du fichier modele .
%
% Argument d'appel:
%
% modelName = string Nom du fichier modele qui renseigne la structure vehlib.
%
function [ vehlib, err ] = lectureModel( modelName, architecture)

err=0;

try
    eval(modelName);
catch ME
    disp(ME)
    err=1;
    vehlib=struct();
    return;
end

if exist('vehlib','var') && ~isstruct(vehlib)
    err=1;
    vehlib=struct();
    return
end

% Stockage du nom du fichier maitre
vehlib.('nom')=modelName;

% Quelques tests ...
if ~exist('vehlib','var') | ~isstruct(vehlib)
    err=2;
end

% Stockage du nom de l architecture
vehlib.architecture=architecture;
