% Function: pretrait_ligne
% Script de pre traitement pour mise en forme des tableaux de definition d'une ligne de bus dans le logiciel VEHLIB.
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
% Script de pre traitement pour mise en forme des tableaux de definition d'une ligne de bus dans le logiciel VEHLIB.
%
% Ce script est appele depuis <ini_calcul> dans le cas ou la variable VD.CYCL.ntypcin vaut 2
%
% Renseigne la structure VD.CYCL presente dans le workspace de matlab et la re-assigne en fin de fonction
%
% Rajout d une balise d arret d une duree nulle si derniere balise==balise de vitesse
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [erreur, VD]=pretrait_ligne(VD)

erreur=0;

if VD.CYCL.def_ligne(length(VD.CYCL.def_ligne(:,1)),1)==1
  %on rajoute une balise d arret a duree nulle
  VD.CYCL.def_ligne=[VD.CYCL.def_ligne ; 2 0 0 1];
end

% Definition ligne
VD.CYCL.Type_bal=VD.CYCL.def_ligne(:,1)';
VD.CYCL.dist_bal=zeros(1,length(VD.CYCL.Type_bal));
VD.CYCL.vit_bal=zeros(1,length(VD.CYCL.Type_bal));
VD.CYCL.arret_bal=zeros(1,length(VD.CYCL.Type_bal));
VD.CYCL.autohyb_bal=zeros(1,length(VD.CYCL.Type_bal));
VD.CYCL.passager_bal=zeros(1,length(VD.CYCL.Type_bal));

for i=1:1:length(VD.CYCL.Type_bal)
   if(VD.CYCL.Type_bal(i)==1)
      % distance (km) vitesse (km/h)  ELE/HYB (1/0)
      if i>1
         VD.CYCL.dist_bal(i)=VD.CYCL.def_ligne(i,2)*1000+VD.CYCL.dist_bal(i-1);
      else
         VD.CYCL.dist_bal(i)=VD.CYCL.def_ligne(i,2)*1000;
      end
      VD.CYCL.vit_bal(i)=VD.CYCL.def_ligne(i,3);
      VD.CYCL.autohyb_bal(i)=VD.CYCL.def_ligne(i,4);
      if i>1
         VD.CYCL.passager_bal(i)=VD.CYCL.passager_bal(i-1);
      else
         VD.CYCL.passager_bal(1)=0;
      end
   elseif(VD.CYCL.Type_bal(i)==2 || VD.CYCL.Type_bal(i)==3)
      % Temps arret (s)   charge passagers (kg) ELE/HYB (1/0)
      VD.CYCL.arret_bal(i)=VD.CYCL.def_ligne(i,2);
      VD.CYCL.passager_bal(i)=VD.CYCL.def_ligne(i,3);
      VD.CYCL.vit_bal(i)=0;
      VD.CYCL.autohyb_bal(i)=VD.CYCL.def_ligne(i,4);
      if i>1
         VD.CYCL.dist_bal(i)=VD.CYCL.dist_bal(i-1);
      else
         VD.CYCL.dist_bal(1)=0;
      end
   else
    erreur=1;
    return
   end
end
VD.CYCL.vit_bal=VD.CYCL.vit_bal/3.6;

% Definition accessoire
for i=1:1:length(VD.CYCL.acces_ligne)
   if i>1
      VD.CYCL.distacces_bal(i)=VD.CYCL.acces_ligne(i,1)'*1000+VD.CYCL.distacces_bal(i-1);
   else
      VD.CYCL.distacces_bal(i)=VD.CYCL.acces_ligne(i,1)'*1000;
   end
end
VD.CYCL.acces_bal=VD.CYCL.acces_ligne(:,2)';

end
