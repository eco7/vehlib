function Axls = m2xdate(Amat)
% MATLAB serial date number to Excel serial date number
Axls = Amat - datenum('Jan 1, 1900') + 2;

end