function Amat = x2mdate(Axls)
% Excel serial date number to MATLAB serial date number
Amat = Axls + datenum('Jan 1, 1900')-2;

end