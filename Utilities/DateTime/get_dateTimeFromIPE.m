function [dn] = get_dateTimeFromIPE(S)
% Get GPS information to construct a datenum number

try
    UTC_day = XmlValue(S,'UTC_day');
    UTC_month = XmlValue(S,'UTC_month');
    UTC_year = XmlValue(S,'UTC_year');
    UTC_second = XmlValue(S,'UTC_second');
    UTC_minute = XmlValue(S,'UTC_minute');
    UTC_hour = XmlValue(S,'UTC_hour');
    dn = datenum(UTC_year(end)+2000,UTC_month(end),UTC_day(end),UTC_hour(end),UTC_minute(end),UTC_second(end)); % last record : gps satellite are found !
catch me
    dn=NaN;
end