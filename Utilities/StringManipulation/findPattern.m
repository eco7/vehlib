function [ result ] = findPattern( expression, pattern, place )
% Find a pattern in a cell array of strings
% result is a new cell array of strings containing the pattern

if nargin == 2
    newPattern = ['.*',pattern,'.*'];
elseif nargin == 3 && strcmpi(place,'end')
    newPattern = ['.*',pattern,'\>'];
 elseif nargin == 3 && strcmpi(place,'beg')
   newPattern = ['\<',pattern,'.*'];
else
   result={};
   return
end

cellList=regexp(expression,newPattern,'match');
EmptyCellList=~(cellfun('isempty',cellList));
indice = (EmptyCellList==1);
result=expression(indice);


%% Test
% resList={
%     '/mnt/equilibre/equilibre/donnees/DX347RQ/traites/201603/20160301_042254_DX347RQ_Descriptif.mat'
%     '/mnt/equilibre/equilibre/donnees/DX347RQ/traites/201603/20160301_042254_DX347RQ_Descriptif.mat_extrema.fig'
%     '/mnt/equilibre/equilibre/donnees/DX347RQ/traites/201603/20160301_042254_DX347RQ_Descriptif.mat_prediction.fig'
%     '/mnt/equilibre/equilibre/donnees/DX347RQ/traites/201603/20160301_042254_DX347RQ_Descriptif.mat_realite.fig'
%     '/mnt/equilibre/equilibre/donnees/DX347RQ/traites/201603/20160302_041636_DX347RQ_Descriptif.mat'
%     };
% 
% findPattern( resList, 'mat')
% findPattern( resList, 'mat','end')
% findPattern( resList, '/mnt','beg')

end
