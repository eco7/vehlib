% Function: rmchar
%
% remove a pattern in a cell array of string
% Return a cell array of string, same size as input
% ToDo : deals with special characters
%
function [ cellOut ] = rmchar( cellIn, pattern )
% Remove char in a cell array of string
%
cellOut = cell(size(cellIn));

for j=1:length(cellIn)
   cellOut{j}=regexprep(cellIn{j},pattern,'');
end
end

