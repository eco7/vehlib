function newstr = cleanStr(oldstr)
% function newstr = cleanStr(oldstr)
%fonction  pour nettoyer les accents des voyelles
%il remplace tous les autres carcteres de la partie haute d'ASCII (>127)
%par underscore.
%Cree: Aout 2013 ER
%Benchmark: 7Mo/s sur Win32 (PC Core2Duo 2GHz)
newstr = oldstr;
newstr(newstr>=224 & newstr<=229)='a';
newstr(newstr>=232 & newstr<=235)='e';
newstr(newstr>=236 & newstr<=239)='i';
newstr(newstr>=242 & newstr<=246)='o';
newstr(newstr>=249 & newstr<=252)='u';
newstr(newstr>=192 & newstr<=197)='A';
newstr(newstr>=200 & newstr<=203)='E';
newstr(newstr>=204 & newstr<=207)='I';
newstr(newstr>=210 & newstr<=214)='O';
newstr(newstr>=217 & newstr<=220)='U';
newstr(newstr==176)='_';%degres
newstr(newstr=='%')='_';
newstr(newstr==' ')='_';
newstr(newstr>=127)='_';
newstr(newstr<=31)='_';

end