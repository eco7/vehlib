#!/usr/bin/env python3

from datetime import datetime, timedelta
from pathlib import Path
import re
from sys import argv

import matplotlib
from matplotlib import pyplot
from matplotlib.ticker import EngFormatter


MONITOR_RE = re.compile('\n'.join((
    '(?P<time>.+)',
    r' *(?P<seconds>\d+) +(?P<vsz>\d+) +(?P<rss>\d+) +(?P<args>.+)',
    ' *(?P<memheader>.+)',
    'Mem: *(?P<memvalues>.+)',
    'Swap: *(?P<swapvalues>.+)',
    ''
)), flags=re.MULTILINE)


def list_snapshots(monitor_log):
    snapshots = []

    for match in MONITOR_RE.finditer(monitor_log):
        md = match.groupdict()

        memkeys = md['memheader'].split()
        memvalues = md['memvalues'].split()
        swapvalues = md['swapvalues'].split()

        snapshot = {
            'time': datetime.strptime(md['time'], '%Y-%m-%d-%H:%M:%S'),
            'uptime': int(md['seconds']),
            'vsz': int(md['vsz'])*1024,
            'rss': int(md['rss'])*1024,
            'process': md['args'],
            'mem': {memkeys[i]: int(val)*1024 for i, val in enumerate(memvalues)},
            'swap': {memkeys[i]: int(val)*1024 for i, val in enumerate(swapvalues)}
        }

        snapshots.append(snapshot)

    return snapshots


def graph_logs(f, title):
    snapshots = list_snapshots(Path(f).read_text())

    x0 = snapshots[0]['time']
    print(f'start date : {x0}')
    xend = snapshots[-1]['time']
    print(f'End date :  {xend}')
    duration = (xend-x0)
    duration = duration.total_seconds()/3600
    print(f'Duration : {duration: .2f} hours')
    #print(type(snapshots))
    #xs = []
    #for snapshot in snapshots:
        #print(snapshot['time'])
    #    var= (snapshot['time']-x0).total_seconds()/3600
    #    xs.append(var)
    #xs = tuple(xs)
    #print(f'Size of xs : {len(xs)}')
    #print(f'Last value of xs element : {xs[-1]}')
    xs = tuple((s['time']-x0).total_seconds()/3600 for s in snapshots)
    vsz = tuple(s['vsz'] for s in snapshots)
    rss = tuple(s['rss'] for s in snapshots)
    #print(f'Size of rss {len(rss)}')
    print(f'Maximum resident memory used : {max(rss)/1e9: .1f} GB')
    print(f'Maximum virtual memory used : {max(vsz)/1e9: .1f} GB')
    memavail = tuple(s['mem']['available'] for s in snapshots)
    swapused = tuple(s['swap']['used'] for s in snapshots)

    fig, axes = pyplot.subplots(figsize=(128, 9.6))
    axes.plot(xs, vsz, label='VSZ (process)')
    axes.plot(xs, rss, label='RSS (process)')
    axes.plot(xs, memavail, label='available memory (system)', linewidth=0.5)
    axes.plot(xs, swapused, label='used swap (system)')

    #axes.set_xlim(0, (snapshots[-1]['time']-x0).seconds)
    axes.set_xlim(0, xs[-1])
    axes.set_xlabel('hours')
    axes.set_ylim(0)
    axes.yaxis.set_major_formatter(EngFormatter(unit='B'))
    axes.legend()
    axes.set_title(title)

    pyplot.savefig(f.replace('.log', '.pdf'))
    # pyplot.show()


# matplotlib.use('TkAgg')

log_file = argv[1]
graph_logs(log_file, log_file)
