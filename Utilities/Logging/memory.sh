#!/bin/bash

set -eu

pid=$1

out=matlab-memory-$(date +%F-%T).log

while true
do
    date +"%F-%T" >> ${out}
    ps o cputimes=,vsz=,rss=,args= ${pid} >> ${out} || break
    free >> ${out}
    echo >> ${out}

    sleep 5
done
