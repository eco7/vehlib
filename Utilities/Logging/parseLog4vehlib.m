function [mem, elapsedTime] = parseLog4vehlib(pathname, name)

try
    fid = fopen(fullfile(pathname, name));
    
    %line=fgetl(fid);
    lines = readlines(name);
    t=regexp(lines,'.*MEM.*Memory usage = ([^,]*)%.*','tokens');
    mem=double([t{:}]);
    t=regexp(lines,'.*MEM.*Elapsed Time = ([^s]*) sec.*','tokens');
    elapsedTime =  double([t{:}]);
    
    fclose(fid);
catch
    mem = [];
    elapsedTime = [];
end

end