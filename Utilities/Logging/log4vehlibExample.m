function [] = log4vehlibExample()
% Example using log4vehlib
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global MemLog
name_memory = 'Test.log';
log4vehlib('open', name_memory);
for i=1:10
    pause(.1);
    log4vehlib('message',{'this is a message'});
    log4vehlib('memory',{['Loop : ',num2str(i)]});
    log4vehlib('cpu');
    
end

try
    fprintf('%s\n',Myvar);
catch ME
    log4vehlib('error',ME);
end

ME = MException('MyComponent:noSuchVariable', ...
    'Variable %s not found', 'Myvar');
log4vehlib('error',ME);


log4vehlib('close');
    
log4vehlib('message',{'this is a blank message'});

[mem, elapsedTime] = parseLog4vehlib('./', name_memory);

%Temps de calcul et utilisation des ressources
figure
plot(elapsedTime, mem,'linewidth',2);
ylabel('Memory usage (%)')
xlabel('CPU Time (sec)');
end