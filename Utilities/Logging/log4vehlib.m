function [] = log4vehlib(action, arg)
% Simple Log function
% action can be : open or close, message, memory, cpu or error
% if open is omitted, Log is displayed on the screen
%
% Example : see log4vehlibExample.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ù

global MemLog

if ~isfield(MemLog,'fid') &&  ~strcmpi(action,'display') && ~strcmpi(action,'open')
    % Do nothing
    return
end

if strcmpi(action,'display')
      % Display on screen
    MemLog.fid =1;
    MemLog.tstart = clock;
elseif strcmpi(action,'open')
    [path,filename,ext] = fileparts(arg);
    filename = [filename,ext];
    MemLog.fid = fopen(arg,'w');
    if MemLog.fid==-1
        MemLog.fid=1;
    end
    
    MeMLog.filename = filename;
    if isempty(path)
        MeMLog.pathname = ['.',filesep];
    else
        MeMLog.pathname = path;
    end
    MemLog.tstart = clock;
    fprintf(MemLog.fid,'%s %s\n','Log started :',datestr(now));
elseif strcmpi(action,'close')
    fclose(MemLog.fid);
    MemLog = rmfield(MemLog,'fid');
elseif strcmpi(action,'message')
    str = ['MES - Elapsed Time = ',num2str(etime(clock,MemLog.tstart)),' sec - ',arg{:}];
    fprintf(MemLog.fid,'%s\n',str);
elseif strcmpi(action,'memory')
    if isunix
        try
            pid = feature('getpid');
            [~, mem_usage]= system(['ps -p ',num2str(pid),' -o pmem=']);
            % -o rsz : resident memory size in kbytes
            % -o vsz : initial virtual memory
            [~, mem_rsz]= system(['ps -p ',num2str(pid),' -o rsz=']);
            [~, mem_vsz]= system(['ps -p ',num2str(pid),' -o vsz=']);
            if nargin == 1
            str = ['MEM - Elapsed Time = ',num2str(etime(clock,MemLog.tstart)),' sec - ','Memory usage = ',num2str(str2double(mem_usage)),' % - ',...
                'Resident memory size = ',num2str(str2double(mem_rsz)/1e6),' GiB - ',...
                'Virtual memory size = ',num2str(str2double(mem_vsz)/1e6),' GiB'];
            elseif nargin == 2
            str = ['MEM - Elapsed Time = ',num2str(etime(clock,MemLog.tstart)),' sec - ','Memory usage = ',num2str(str2double(mem_usage)),' % - ',...
                'Resident memory size = ',num2str(str2double(mem_rsz)/1e6),' GiB - ',...
                'Virtual memory size = ',num2str(str2double(mem_vsz)/1e6),' GiB - ',arg{:}];
            end
            fprintf(MemLog.fid,'%s\n',str);
            %str = [str, ', Step time = ', num2str(stepTime),' sec.'];

        catch ME % Not tested ?
            log4vehlib('error', ME);
        end
    else
        % To be implemented
        user = memory;
        str = ['Memory usage = ',num2str(user.MemUsedMATLAB)];
        if nargin == 1
            str = [str, ', Step time = ', num2str(stepTime)];
        elseif nargin == 2
            str = [str, ', Step time = ', num2str(stepTime), ' - ', message];
        end
    end
elseif strcmp(action,'cpu')
    if isunix
        pid = feature('getpid');
        [status, cpu_usage]= system(['ps -p ',num2str(pid),' -o pcpu= ']);
        if status == 0
            str = ['CPU - CPU usage = ',cpu_usage];
        end
    end
elseif strcmpi(action,'error')
    if ~isempty(arg.stack)
        str = ['ERR - ',arg.identifier,' ',arg.message,' - File : ',arg.stack(1).name,' - line : ',num2str(arg.stack(1).line)];
    else
        str = ['ERR - ',arg.identifier,' ',arg.message];
        
    end
    fprintf(MemLog.fid,'%s\n',str);
    
        
end

end