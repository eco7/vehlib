% Function: affectVehlibStruct2Workspace
% Fonction d'ecriture courte des variables dans le WS Matlab 
%
% Arguments d'appel:
%
% Object = variable de type struct en format de travail
%
% Exemple de lancement:
% [err,returnObject]=affectVehlibStruct2Workspace(Object,WS)
%
% Input Arg:
% Object : an XML structure that conform to vehlib requirements
% WS : Can be either 'caller' or 'base'
%
% Resultat:
% returnObject = variable de type struct en format court
% 
% Exemple de returnObject
% 
% Struct [1x1 struct]
% -  var1: [px1 double]
% -  var2: [px1 double]
% -  ...
%
% -----------------------------------------------------------------------------
function [ERR,returnObject]=affectVehlibStruct2Workspace(Object,WS)

ERR=[];

if nargin ~= 1 && ~strcmpi(WS,'caller') && ~strcmpi(WS,'base')
    %Construct an MException object to represent the error.
    ERR = MException('affectVehlibStruct2Workspace:ArgNotCompatible', ...
        'Second argument should be ''base'' or ''caller''');
    throw(ERR)
end

[Object, err]=verifFomatXML4Vehlib(Object);
if err<0
    %Construct an MException object to represent the error.
    ERR = MException('affectVehlibStruct2Workspace:ArgNotCompatible', ...
        'Input Argument is not compatible an valid xml format: %s',err);
    %Throw the exception to stop execution and display an error message.
    throw(ERR)
end

returnObject=struct;


if length(Object.table)>1
    % il y plusieurs tables dans la structure xml, les noms de variables sont suivis de l'id de table
    for i=1:length(Object.table)
        
        names=eval(['fieldnames(Object.table{',num2str(i),'})']);
        
        for ivar=3:length(names)
            if length(names)==length(unique(names)) % Plusieurs tables, aucune structure de donnees en doublon
                name=[eval(['Object.table{',num2str(i),'}.',char(names(ivar)),'.name'])];
                vector=eval(['Object.table{',num2str(i),'}.',char(names(ivar)),'.vector']);
                returnObject=setfield(returnObject,name,vector);
                if nargin~=1
                    assignin(lower(WS),name,vector);
                end
            else % Plusieurs tables, certaines structure de donnees sont en doublon: on rajoute le numero de la table dans le champ du nom
                name=[eval(['Object.table{',num2str(i),'}.',char(names(ivar)),'.name']),'_',num2str(i)];
                vector=eval(['Object.table{',num2str(i),'}.',char(names(ivar)),'.vector']);
                returnObject=setfield(returnObject,name,vector);
                if nargin~=1
                    assignin(lower(WS),name,vector);
                end               
            end
        end
    end
else
    % il n'y a qu'une table
    names=fieldnames(Object.table{1});
    
    for ivar=3:length(names)
        name=eval(['Object.table{1}.',char(names(ivar)),'.name']);
        vector=eval(['Object.table{1}.',char(names(ivar)),'.vector']);
        returnObject=setfield(returnObject,name,vector);
        if nargin~=1
            assignin(lower(WS),name,vector);
        end               
    end
end

end