% Function: XmlValue
% Fonction qui renvoie le champ "vector" de la structure pointee par l'argument "name" de la table xml
%
% Arguments d'appel:
%
% Object = variable de type struct en format de travail xml
% name = nom de la variable a extraire
% 
% Argument de retour:
% vector = objet pointe par le champ vector de la structure xml
%
%
% Exemple de lancement:
% (start code)
% tsim=XmlValue(Object, 'tsim')
% (end)
%
% Aujourd'hui la fonction renvoie NaN si le champ n'est pas unique. A voir ...
%
% -----------------------------------------------------------------------------
function [varargout]=XmlValue(Object, namess,verbose)

if ~exist('verbose','var')
    verbose=0;
end

[Object, err, errS]=verifFomatXML4Vehlib(Object);
if err<0
    %Construct an MException object to represent the error.
    err = MException('XmlValue:ArgNotCompatible', ...
        'Input Argument is not a compatible xml format: %s',errS);
    %Throw the exception to stop execution and display an error message.
    throw(err)
end

if iscell(namess)
    long=length(namess);
else
    long=1;
end

for k=1:long
    if iscell(namess)
        name=namess{k};
    else
        name=namess;
    end
    
    names={};
    ntables=[];
    for ind=1:length(Object.table)
        names=[names; eval(['fieldnames(Object.table{',num2str(ind),'})'])]; % Tous les noms de variables
        ntables=[ntables; ind*ones(length(eval(['fieldnames(Object.table{',num2str(ind),'})'])),1)]; % Numero de la table correspondante
    end
    
    if length(find(strcmp(names, name)))==1
        indice= strcmp(names,name)==1;
        vector=eval(['Object.table{',num2str(ntables(indice)),'}.',name,'.vector']);
    else
        if verbose
            disp(['XmlValue: Variable : ', name, ' not found']);
        end
        indice=find(strcmp(names,name)==1);
        v=[];
        for j=1:length(indice)
            ind=indice(j);
            v=[v eval(['Object.table{',num2str(ntables(ind)),'}.',name,'.vector'])];
        end
        vector=NaN;
    end
    varargout{k}=vector;
    
end