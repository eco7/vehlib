function result = getXMLTableMatrix(XMLstruct,indTable)
% function result = getXMLTableMatrix(XMLstruct,indTable)
% parametres:
% -XMLstruct [struct]: XMLStruct selon le format VEHLIB
% -indTable [1x1 double]: index de la table voulue
% valeurs de retour:
% result [struct]: structure qui contient les vecteurs des variables
% contenues dans la table numero indTable.

varList = getXMLVarList(XMLstruct);
varList = varList{indTable};
for ind = 1:length(varList)
    varName = varList{ind};
    vectorCell = getXMLVector(XMLstruct,varName);
    result.(varName) = vectorCell{indTable};
end
end