
% Function: lecture_mfile
% Fonction de lecture d'un m-file dans explgr
%
% --------------------------------------------------------------------
    function [retour,b,d,c,archit]=lecture_mfile(filename,pathname,archit)
        
        p=pwd;
        cd(pathname);
        eval(filename(1:findstr(filename,'.m')-1));
        d=who;      % Variables
        evalin('base',filename(1:findstr(filename,'.m')-1));
        cd(p);
        fprintf(1,'\n%s%s\n\n','  D?pouillement du fichier        : ',filename);
        
        clear pathname filename;
        c=num2cell(ones(size(d))); % Unites=1 => pas d'unite
        b=d;    % Nom des variables dans l'interface = nom des variables dans le WS
        retour=1;
        
    end
end

end
