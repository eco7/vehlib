% function: miseEnForme4GestionEnergie
% Rajout de variables synthetiques dans la structure xml pour les calculs inverses
%
%
%
function [VEHStruct]=miseEnForme4GestionEnergie(VEHStruct)

[VEHStruct, err]=verifFomatXML4Vehlib(VEHStruct);
if err<0
    %Construct an MException object to represent the error.
    err = MException('XmlValue:ArgNotCompatible', ...
        'Input Argument is not a compatible xml format: %s',VEHStruct);
    %Throw the exception to stop execution and display an error message.
    throw(err)
end

nn=length(VEHStruct.table)+1;

% Une table pour des resultats finaux
var={'conso100', 'Consommation du vehicule en l/100 km', 'l/100km', 'double', '%f';
'co2_gkm', 'Emission de CO2 en g/km', 'g/km',  'double', '%f';
'co_gkm', 'Emission de CO en g/km',  'g/km',  'double', '%f';
'hc_gkm', 'Emission de HC en g/km',  'g/km',  'double', '%f';
'nox_gkm', 'Emission de NOx en g/km', 'g/km',  'double', '%f'};

varName=var(:,1);
longname=var(:,2);
unit=var(:,3);
xmlType=var(:,4);
precision=var(:,5);

new_table=0;
for ii=1:length(varName)
    if evalin('caller',['exist(''',varName{ii},''',''var'')'])
        if new_table==0
            new_table=1;
            
            VEHStruct.head.date=[VEHStruct.head.date,' modification ',date];
            % Renseignement de l'id et de la metatable
            VEHStruct.table{nn}.id=num2str(nn);
            VEHStruct.table{nn}.metatable=struct;
            VEHStruct.table{nn}.metatable.name='VARIABLES GESTION ENERGIE SYNTHETIQUES';
            VEHStruct.table{nn}.metatable.date=date;
%            VEHStruct.table{nn}.metatable.sourcefile=VEHStruct.table{1}.metatable.sourcefile;
            VEHStruct.table{nn}.metatable.comments=VEHStruct.table{1}.metatable.comments;
        end
        VEHStruct.table{nn}.(varName{ii}).name=varName{ii};
        VEHStruct.table{nn}.(varName{ii}).longname=longname{ii};
        VEHStruct.table{nn}.(varName{ii}).type=xmlType{ii};
        VEHStruct.table{nn}.(varName{ii}).unit=unit{ii};
        VEHStruct.table{nn}.(varName{ii}).precision=precision{ii};
        VEHStruct.table{nn}.(varName{ii}).vector=evalin('caller',varName{ii});
    end
end



nn=length(VEHStruct.table)+1;

% Une table propre aux variables instantanees de gestion de l'energie
var={'bilanP', 'Bilan de puissance vehicule',    'W', 'double', '%f';
'bilanP_loc', 'Bilan de puissance local vehicule',    'W', 'double', '%f';
'u0sc_min',    'Limite basse du graphe SC',      'V', 'double', '%f';
'u0sc_max',    'Limite haute du graphe SC',      'V', 'double', '%f';
'rap_opt',     'Rapport de boite optimal',       '/', 'double', '%f';
%'dwmt',        'Acceleration angulaire moteur thermique', 'rd/s2', 'double', '%f'; N'a pas toujours la taille de tsim !
'Pf_motelec',  'Pf_motelec',                     '/', 'double', '%f';
'cfrein_meca_pelec_mot', 'cfrein_meca_pelec_mot', '/', 'double', '%f';
'pbat_cible',  'Puissance cible batterie',        'W', 'double', '%f';
'Pjbat', 'Pertes joule batterie', 'W', 'double', '%f';
'P0bat', 'Puissance a vide batterie', 'W', 'double', '%f';
'lambda',      'Multiplicateur de lagrange',     '/', 'double', '%f';
'poids_min',      'Poids_min',     '/', 'double', '%f';
'meilleur_chemin',      'Meilleur chemin',     '/', 'double', '%f';};
%'', '', '', 'double', '%f';

varName=var(:,1);
longname=var(:,2);
unit=var(:,3);
xmlType=var(:,4);
precision=var(:,5);

new_table=0;
for ii=1:length(varName)
    if evalin('caller',['exist(''',varName{ii},''',''var'')'])
        if new_table==0
            new_table=1;
            
            VEHStruct.head.date=[VEHStruct.head.date,' modification ',date];
            % Renseignement de l'id et de la metatable
            VEHStruct.table{nn}.id=num2str(nn);
            VEHStruct.table{nn}.metatable=struct;
            VEHStruct.table{nn}.metatable.name='VARIABLES GESTION ENERGIE INSTANTANEES';
            VEHStruct.table{nn}.metatable.date=date;
%            VEHStruct.table{nn}.metatable.sourcefile=VEHStruct.table{1}.metatable.sourcefile;
            VEHStruct.table{nn}.metatable.comments=VEHStruct.table{1}.metatable.comments;
        end
        VEHStruct.table{nn}.(varName{ii}).name=varName{ii};
        VEHStruct.table{nn}.(varName{ii}).longname=longname{ii};
        VEHStruct.table{nn}.(varName{ii}).type=xmlType{ii};
        VEHStruct.table{nn}.(varName{ii}).unit=unit{ii};
        VEHStruct.table{nn}.(varName{ii}).precision=precision{ii};
        VEHStruct.table{nn}.(varName{ii}).vector=evalin('caller',varName{ii});
    end
end
