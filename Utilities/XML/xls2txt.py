#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
xls2txt
Les verification d'existance de fichier, feuille, etc. sont faites depuis MATLAB
%ENTREES:
filename
indFeuille : MATLAB 1:n,python 0:n-1
@author: redondo
"""

import xlrd
import os
import shutil
import csv
from optparse import OptionParser

def get_xls_data(ws):
    data = [[ws.cell_value(lin, col) for col in range(ws.ncols)] for lin in range(ws.nrows)]
    return data

def data2txt(data,fileOut):
    f = open(fileOut,"wb")
    for d in data:
        for d1 in d:
            if isinstance(d1,unicode):
                f.write(d1.encode('UTF-8'))
            else:
                f.write(str(d1))
            f.write("\t")
        f.write("\n")
    f.close()

def main():
    parser = OptionParser(usage="usage: %xls2txt [options] filename",version="%xls2txt 1.0")
    
    (options, args) = parser.parse_args()
    fileIn = args[0]
    if not(os.path.exists(fileIn)):
        print "ERREUR: " + fileIn + ", fichier introuvable"
        return -1
    wb = xlrd.open_workbook(fileIn)
    ws = wb.sheets()
    if len(args)>1:
        indFeuille =  int(args[1])
        print "indFeuille: " + str(indFeuille)
        ws = ws[indFeuille-1]
        data = get_xls_data(ws)
        (F,E) = os.path.splitext(fileIn)
        fileOut = F + "_" + str(indFeuille) + ".txt"
        data2txt(data,fileOut)
    else:
        (F,E) = os.path.splitext(fileIn)
        for ws1 in ws:
            data = get_xls_data(ws1)
            fileOut = F + "_" + str(ws1.number+1) + ".txt"
            data2txt(data,fileOut)
    return 0

if __name__ == '__main__':
    main()
