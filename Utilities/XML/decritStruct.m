% Function: decritStruct
% Fonction de description du contenu d'une structure xml pour vehlib
%
% Arguments d'appel:
%
% struct : structure au format xml pour VEHLIB issue du workspace 
%
% outFile : [optionnel] fichier texte de sortie de la description, 
%           par defaut la description est affichee dans le workspace 
%
% Exemple de lancement:
% (start code)
% decritStruct(struct,outFile)
% (end)
%
% Resultat:
% 
% (start code)
% Version de la structure: 0.9 
% Date : 29-Nov-2011 
% Projet :  
% Commentaire : Created by saveVehlibSimu2XMLFormat, version : 0.1 
% Type : xml 
%  
% table numero : 1
% name : Vehicule : AIXAM_E_CITY- Cycle : TRAJET_RATP_TVM 
% date : 29-Nov-2011 
% fichier source : Simulation VEHLIB- Numero de calcul : 627 
% commentaire : VEHLIB- VERSION : 2011a 
%
% numero nom            unite    longueur type   precision nom long 
% 3      tsim           s        77001    double %f        Temps 
% 4      vit            km/h     77001    double %f        Vitesse du vehicule 
% 5      distance       m        77001    double %f        Distance parcourue 
% 6      acc            m/s2     77001    double %f        Acceleration du vehicule 
% 7      vit_dem        km/h     77001    double %f        Vitesse demandee 
% 8      pente          %        77001    double %f        Pente 
% ...
% (end)

function decritStruct(struct,outFile)

if nargin==2
    fid=fopen(outFile,'w+');
else
    fid=1;
end

% description du header

fprintf(fid,'%s %s \n','Version de la structure:', struct.head.version);
fprintf(fid,'%s %s \n','Date :', struct.head.date);
fprintf(fid,'%s %s \n','Projet :', struct.head.project);
if isfield(struct.head,'comments')
    fprintf(fid,'%s %s \n','Commentaire :', struct.head.comments);
end
fprintf(fid,'%s %s \n \n','Type :', struct.head.type);


% description des table

for i=1:size(struct.table,2)
    
    fprintf(fid,'%s %s \n \n','table numero :', num2str(eval([' struct.table{',num2str(i),'}.id'])));
    % description metatable
    fprintf(fid,'%s %s \n','name :', eval([' struct.table{',num2str(i),'}.metatable.name']));
    fprintf(fid,'%s %s \n','date :', eval([' struct.table{',num2str(i),'}.metatable.date']));
    if isfield(eval(['struct.table{',num2str(i),'}.metatable']),'sourcefile')
        fprintf(fid,'%s %s \n','fichier source :', eval([' struct.table{',num2str(i),'}.metatable.sourcefile']));
    end
    if isfield(eval(['struct.table{',num2str(i),'}.metatable']),'comments')
        fprintf(fid,'%s %s \n \n','commentaire :', eval([' struct.table{',num2str(i),'}.metatable.comments']));
    end
    % description variable
    nomvar=fieldnames(struct.table{i});
    longvar=zeros(size(nomvar));
    for k=3:length(nomvar)
        longvar(k)=length(char(nomvar(k)));
    end
    d=max(longvar);
    format1=['%s %-',num2str(d+1),'s %-8s %-8s %-6s %s %s \n'];
    format2=['%-6u %-',num2str(d+1),'s %-8s %-8u %-6s %-9s %s \n'];
    format3=['%-6u %-',num2str(d+1),'s %-8s %-8u %-6s %-9s \n'];
    format4=['%-6u %-',num2str(d+1+9),'s %-8u %-6s %s \n'];
       
    fprintf(fid,format1,'numero','nom','unite','longueur','type','precision','nom long');
    for j=3:length(nomvar)
        % nom   unite   longueur    type    precision   nomlong
        if isfield(eval(['struct.table{',num2str(i),'}.',nomvar{j}]),'longname')
            fprintf(fid,format2,j-2,...
                eval([' struct.table{',num2str(i),'}.',nomvar{j},'.name']),...
                eval([' struct.table{',num2str(i),'}.',nomvar{j},'.unit']),...
                eval(['length(struct.table{',num2str(i),'}.',nomvar{j},'.vector)']),...
                eval([' struct.table{',num2str(i),'}.',nomvar{j},'.type']),...
                eval([' struct.table{',num2str(i),'}.',nomvar{j},'.precision']),...
                eval([' struct.table{',num2str(i),'}.',nomvar{j},'.longname']));
        elseif isfield(eval(['struct.table{',num2str(i),'}.',nomvar{j}]),'unit')
            fprintf(fid,format3,j-2,...
                eval([' struct.table{',num2str(i),'}.',nomvar{j},'.name']),...
                eval([' struct.table{',num2str(i),'}.',nomvar{j},'.unit']),...
                eval(['length(struct.table{',num2str(i),'}.',nomvar{j},'.vector)']),...
                eval([' struct.table{',num2str(i),'}.',nomvar{j},'.type']),...
                eval([' struct.table{',num2str(i),'}.',nomvar{j},'.precision']));
        else
            fprintf(fid,format4,j-2,...
                eval([' struct.table{',num2str(i),'}.',nomvar{j},'.name']),...
                eval(['length(struct.table{',num2str(i),'}.',nomvar{j},'.vector)']),...
                eval([' struct.table{',num2str(i),'}.',nomvar{j},'.type']),...
                eval([' struct.table{',num2str(i),'}.',nomvar{j},'.precision']));
        end
        
    end
    fprintf(fid,'\n \n');
end
    if nargin == 2
        fclose(fid);
    end
end