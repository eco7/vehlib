% File: templateXML.m
% Modele a renseigner pour creer une structure de donnees XMLVEH.
%
% Groupe:
%
% Lecture / ecriture de donnees en format XML
% http://lx-veh2/mediawiki/index.php/Format_XML
%
% Utilisation:
%
% 1) preparation de donnees: les variables doivent etre presentes dans le
% workspace
% 2) renseigner le script: copier ce script et editer la partie 'editable'
% 3) resultat: structure 'file' dans le workspace avec le format VEH
% 4) si necessaire, renommer cette structure
% 5) cette structure est inscriptible en XML avec la fonction mat2xml
%
% Auteur: BJ / ER
%
% Date de creation: Aout 2011
% Date de modification: Octobre 2011
% ------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EDITER ICI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HEAD
DATE='yyyy/mm/dd';
PROJECT='monProjet';
COMMENTS='monCommentaire';
% FIN DE HEAD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TABLE
TABLE_NUMBER=1;
TABLE(TABLE_NUMBER).TABLE_NAME='myMatrixNr1';
TABLE(TABLE_NUMBER).TABLE_DATE='yyyy/mm/dd';
TABLE(TABLE_NUMBER).TABLE_SOURCEFILE='';
TABLE(TABLE_NUMBER).TABLE_COMMENTS='';
%VARIABLE
TABLE(TABLE_NUMBER).VAR_NAME={'variable1', 'variable2'};
TABLE(TABLE_NUMBER).VAR_UNIT={'unit1', 'unit2'};
TABLE(TABLE_NUMBER).VAR_PRECISION={'%f', '%f'};
TABLE(TABLE_NUMBER).VAR_TYPE={'double', 'double'};
TABLE(TABLE_NUMBER).VAR_LONGNAME={'variable1', 'variable2'};

%fin de variables
% FIN DE TABLE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AUTRE TABLE
TABLE_NUMBER=2;
TABLE(TABLE_NUMBER).TABLE_NAME='myMatrixNr2';
TABLE(TABLE_NUMBER).TABLE_DATE='yyyy/mm/dd';
TABLE(TABLE_NUMBER).TABLE_SOURCEFILE='';
TABLE(TABLE_NUMBER).TABLE_COMMENTS='';
%VARIABLE
TABLE(TABLE_NUMBER).VAR_NAME={'variable1', 'variable2'};
TABLE(TABLE_NUMBER).VAR_UNIT={'unit1', 'unit2'};
TABLE(TABLE_NUMBER).VAR_PRECISION={'1', '1'};
TABLE(TABLE_NUMBER).VAR_TYPE={'double', 'double'};
TABLE(TABLE_NUMBER).VAR_LONGNAME={'variable1', 'variable2'};
%fin de variables
% FIN DE TABLE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIN DU RENSEIGNEMENT DU FICHIER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NE PAS EDITER
file = struct;
file.head = struct;
file.head.version = '';
file.head.type = 'xml';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HEAD
file.head.date=DATE;
file.head.project=PROJECT;
file.head.comments=COMMENTS;
% FIN DE HEAD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TABLE
file.table = cell(1,TABLE_NUMBER);

for ind_table=1:TABLE_NUMBER
    file.table{ind_table}.id=sprintf('%i',ind_table);
    file.table{ind_table}.metatable = struct;
%     file.table{ind_table}.metatable.name='';
%     file.table{ind_table}.metatable.date='';
%     file.table{ind_table}.metatable.sourcefile='';
%     file.table{ind_table}.metatable.comments='';
    file.table{ind_table}.metatable.name=TABLE(ind_table).TABLE_NAME;
    file.table{ind_table}.metatable.date=TABLE(ind_table).TABLE_DATE;
    file.table{ind_table}.metatable.sourcefile=TABLE(ind_table).TABLE_SOURCEFILE;
    file.table{ind_table}.metatable.comments=TABLE(ind_table).TABLE_COMMENTS;
    
%     file.table.variable=struct;
%     file.table.variable.id='';
%     file.table.variable.name='';
%     file.table.variable.type='';
%     file.table.variable.unit='';
%     file.table.variable.precision='';
%     file.table.variable.vector=[];
    
    
    for ind_var=1:length(TABLE(ind_table).VAR_NAME)
        file.table{ind_table}.(TABLE(ind_table).VAR_NAME{ind_var})=struct;
        file.table{ind_table}.(TABLE(ind_table).VAR_NAME{ind_var}).name=TABLE(ind_table).VAR_NAME{ind_var};
        file.table{ind_table}.(TABLE(ind_table).VAR_NAME{ind_var}).unit=TABLE(ind_table).VAR_UNIT{ind_var};
        file.table{ind_table}.(TABLE(ind_table).VAR_NAME{ind_var}).precision=TABLE(ind_table).VAR_PRECISION{ind_var};
        file.table{ind_table}.(TABLE(ind_table).VAR_NAME{ind_var}).type=TABLE(ind_table).VAR_TYPE{ind_var};
        file.table{ind_table}.(TABLE(ind_table).VAR_NAME{ind_var}).longname=TABLE(ind_table).VAR_LONGNAME{ind_var};
        file.table{ind_table}.(TABLE(ind_table).VAR_NAME{ind_var}).vector=evalin('base',[file.table{ind_table}.(TABLE(ind_table).VAR_NAME{ind_var}).name '(:);']);
        %         file.table{ind_table}.data = [file.table{ind_table}.data...
        %             evalin('base',file.table{ind_table}.metatable.(TABLE(ind_table).VAR_NAME{ind_var}).name)];
%         file.table{ind_table}.(TABLE(ind_table).VAR_NAME{ind_var}).id = sprintf('%i',ind_var);
        
    end
end
% FIN DE TABLE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear COMMENTS DATE PROJECT TABLE TABLE_NUMBER ind_var ind_table