% Function: importViaFlightRecMercge
% Fonction d'import d'un fichier issu des enregistreur IPETRONIK
%
%  C Copyright IFSTTAR LTE 1999-2011
%
% Objet:
%
% Fonction d'import d'un fichier issu des enregistreur IPETRONIK
%
% Necessite d'avoir une version du logiciel IPECOnverter.exe install� sur le pc
% Ne fonctionne que sous windows
%
% Arguments d'appel:
%
% param.FlightRecMergeName : Nom de l executable
% param.FlightRecMergePath : Repertoire de flightRecMerge
% FileNameBin : nom du fichier a importer
% PathNameBin : repertoire de stockage du fichier a importer
%
% Arguments de retour:

% FileNameMat : nom du fichier extrait
% PathNameMat : repertoire du fichier extrait

%
% ERR
%
% -----------------------------------------------------------------------------
function [ERR, FileNameMat, PathNameMat]=importviaFlightRecMerge(param, FileNameBin, PathNameBin)

ERR=struct([]);

% Gestion  des erreurs
if nargin~=3
    %Construct an MException object to represent the error.
    ERR = MException('importviaFlightRecMerge:importviaFlightRecMerge', ...
        'Incompatible number of input argument');
end

if ~ispc
    %Construct an MException object to represent the error.
    ERR = MException('importviaFlightRecMerge:importviaFlightRecMerge', ...
        'This program run on Windows only');
    %Throw the exception to stop execution and display an error message.
    throw(ERR)
end

if  ~isfield(param,'FlightRecMergeName') || ~isfield(param,'FlightRecMergePath') || ...
        ~exist(param.FlightRecMergePath,'dir')
    %Construct an MException object to represent the error.
    ERR = MException('importviaFlightRecMerge:importviaFlightRecMerge', ...
        'Problem with FlightRecMergeName or Path');
end


PathNameMat=param.WorkingDataPathName;

if ~exist(PathNameBin,'dir')
    %Construct an MException object to represent the error.
    ERR = MException('importviaFlightRecMerge:importviaFlightRecMerge', ...
        'Problem with PathNameBin');
end

if ~exist(fullfile(PathNameBin,FileNameBin),'file')
    %Construct an MException object to represent the error.
    ERR = MException('importviaFlightRecMerge:importviaFlightRecMerge', ...
        'Problem with input fileName/dir');
end

test=fullfile(param.FlightRecMergePath, param.FlightRecMergeName);
if ~exist(test,'file')
     %Construct an MException object to represent the error.
    ERR = MException('importviaFlightRecMerge:importviaFlightRecMerge', ...
        'Problem with FlightRecMergeName or Path');
end

% Construction de la ligne de commande
cmd=param.FlightRecMergeName;
sourceFile=fullfile(PathNameBin,FileNameBin);
name=rmchar({FileNameBin},'.ppc');
[dir,name,~] =fileparts(name{1});
FileNameMat=[name,'.mat'];
destFile=fullfile(PathNameMat,FileNameMat);
arguments='/o';  % /i :provide information on the file, without writing the mat file
cmd=[cmd, ' ', sourceFile,' ', arguments, ' ', destFile];

try
    currentFolder=pwd;
    cd(param.FlightRecMergePath);
    eval(['!' cmd]);
    cd(currentFolder);
catch ME
    %Construct an MException object to represent the error.
    ERR = MException('importviaFlightRecMerge:importviaFlightRecMerge', ...
        'Problem while converting the file');
    return
end

% IPEconverter ne renvoie pas d'erreur. On test si le fichier out existe
test=fullfile(PathNameMat,FileNameMat);
if ~exist(test,'file')
    %Construct an MException object to represent the error.
    ERR = MException('importviaFlightRecMerge:importviaFlightRecMerge', ...
        'Output (mat) File not found !');
    return
end

% Pour quelle raison la structure porte le nom du dossier d'ecriture
% et non le nom du fichier .bin (comme controlDesk)
try
    load(test);
    s=PathNameMat;
    s=strrep(s,'\','_');
    s=strrep(s,':','_');
    s=strcat(s,'_');
    s=strcat(s,FileNameMat);
    s=s(1:31);
    if exist(s,'var')
        [~,name,~] =fileparts(FileNameMat);
        eval([name,'=',s]);
        p=pwd;
        cd(PathNameMat);
        save(FileNameMat,(name));
        cd(p);
    end
catch ME
    %Construct an MException object to represent the error.
    ERR = MException('importviaFlightRecMerge:importviaFlightRecMerge', ...
        'Problem while converting the file');
    return
end
