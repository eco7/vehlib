% Function: tableId
%
% Fonction de recherche de l'id de la table de VEHLIBStruct
%
function [id]=tableId(nomComp,structure,comp,tabl)


n=find(strcmp(comp,nomComp));

if ~isempty(n)
    nomComp=tabl{n};
    
    ii=1;
    while ~strcmp(structure.table{ii}.metatable.name,nomComp) && ii<length(structure.table)
        ii=ii+1;
    end
    
    if strcmp(structure.table{ii}.metatable.name,nomComp)
        id=str2double(structure.table{ii}.id);
    else
        id=nan;
    end
else
    id=nan;
end
end
