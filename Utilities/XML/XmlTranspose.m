function [XmlOut, err, errS] = XmlTranspose(XmlIn)
% Transpose all vectors of an Xml Structure
% Input arg : an xml sutructure
% Output arg : the transposed structure, and eventualy errors code


XmlOut = XmlIn;

% Error handling : xml format verification
[XmlIn, err, errS]=verifFomatXML4Vehlib(XmlIn);
if err<0
    %Construct an MException object to represent the error.
    err = MException('XmlValue:ArgNotCompatible', ...
        'Input Argument is not a compatible xml format: %s',errS);
    %Throw the exception to stop execution and display an error message.
    throw(err)
end

fprintf('Transposition de la structure ...');
% Get list of variables (cell array of cells, sorted by tables)
varList = getXMLVarList(XmlIn);
for i = 1:length(varList)
    for j= 1:length(varList{i})
        % Transpose
        XmlOut.table{i}.(varList{i}{j}).vector = XmlIn.table{i}.(varList{i}{j}).vector';
    end
end

fprintf('OK\n');
end
