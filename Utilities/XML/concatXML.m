% Function: concatXML
% Concatene une meme table de plusieurs structure XML
%
%  (C) Copyright IFSTTAR LTE 1999-2011
%
%
% Objet:
%
% Concatene un meme fichier (une ou plusieurs tables) a partir de plusieurs fichiers XML
%
%
% Arguments d'appel:
%
% - param.pathname: repertoire ou sont stockes les fichiers a lire
%
% - param.keyword: particule commune aux noms des fichiers a lire (selection)
%
% - param.table: Numero de la table XML a concatener
%
% - param.compteur (optionnel): nom d'une variable a reechantillonner suite
% a la concatenation ([1 2 3 1 2 3] => [1 2 3 4 5 6])
%
% - param.tables (optionnel): Par defaut (0 ou absent), toutes les donnees se retrouvent dans une table
% unique. cet argument permet de creer autant de table que de fichiers
% d'origines(si param.tables vaut 1).
%
% Arguments de retour:
%
% - Struct: structure xml contenant les tables concatenees en table 1
%
% Exemple d'utilisation:
% (start code)
%
% param.pathname='.';
% param.table=3;
% param.compteur='ind';
% param.keyword='PtsMoy';
% [ERR,Str]=concatXML(param)
%
% (end)
%
% ---------------------------------------------------------

function [ERR,Struct]=concatXML(param)
ERR=[];

% Find XML Files
if isunix
    tmpdircontent = ls(param.pathname);
    dircontent = regexp(tmpdircontent,'[\ \t\n]+','split');
    dircontent = dircontent';
else
    dircontent = cellstr(ls(param.pathname));
end

XMLfiles={};
isXML = strfind(dircontent,'.xml');
isLookedFor = strfind(dircontent,param.keyword);
for i = 1:length(dircontent)
    if ~isempty(isXML{i}) && ~isempty(isLookedFor{i})
        XMLfiles{end+1,1}=dircontent{i};
    end
end

if isfield(param,'tables') && param.tables==1
    outerloop=length(XMLfiles);
    innerloop=1;
else
    outerloop=1;
    innerloop=length(XMLfiles);
end

Struct=struct;
Struct.head=struct;
Struct.head.version='0.9';
Struct.head.date=date;
Struct.head.project='';
Struct.head.comments='Created by concatXML';
Struct.head.type='xml';

Struct.table=cell(1);
for outer = 1:outerloop
    Struct.table{outer}=struct;
    Struct.table{outer}.id='1';
    Struct.table{outer}.metatable=struct;
    Struct.table{outer}.metatable.name='Tables XML Concatenees';
    Struct.table{outer}.metatable.date=date;
    if isfield(param,'tables') && param.tables==1
        Struct.table{outer}.metatable.sourcefile=XMLfiles{outer};
    else
        Struct.table{outer}.metatable.sourcefile='';
    end
    Struct.table{outer}.metatable.comments='';
    
    % Init champs
    TmpStruct=lectureXMLFile4Vehlib(XMLfiles{outer},param.pathname);
    Fields=fieldnames(TmpStruct.table{param.table});
    for k = 1:length(Fields)
        if ~isfield(Struct.table{outer},Fields(k))
            Struct.table{outer}.(Fields{k})=TmpStruct.table{param.table}.(Fields{k});
            Struct.table{outer}.(Fields{k}).vector=[];
        end
    end
    
    if isfield(param,'date')
        Struct.table{outer}.date.name='date';
        Struct.table{outer}.date.longname='date';
        Struct.table{outer}.date.type='double';
        Struct.table{outer}.date.unit='-';
        Struct.table{outer}.date.precision='%d';
        Struct.table{outer}.date.vector=[];
    end
    
    Fields{end+1}='date';
    for inner = 1:innerloop
        TmpStruct=lectureXMLFile4Vehlib(XMLfiles{max(inner,outer)},param.pathname);
        for j = 1:length(Fields)
            if ~strcmp(Fields{j},'id') && ~strcmp(Fields{j},'metatable') && ~strcmp(Fields{j},'date')
                Struct.table{outer}.(Fields{j}).vector=[Struct.table{outer}.(Fields{j}).vector; TmpStruct.table{param.table}.(Fields{j}).vector];
            elseif strcmp(Fields{j},'date')
                datefile = readdate(TmpStruct.table{param.table}.metatable.sourcefile, param.date);
                % Fields{j-1} a la fin de la ligne est pourri, a changer...
                Struct.table{outer}.(Fields{j}).vector=[Struct.table{outer}.(Fields{j}).vector; datefile * ones(size(TmpStruct.table{param.table}.(Fields{j-1}).vector))];
            end
        end
    end
    
    if isfield(param, 'compteur')
        Struct.table{outer}.(param.compteur).vector=(1:length(Struct.table{outer}.(Fields{end}).vector))';
    end
end
end

function datefile = readdate(str, modele)

characteres = '0123456789abcdef';
buff_date = '';
for k = 1:length(characteres)
    if ~isempty(str(modele == characteres(k)))
        buff_date(k)=str(modele == characteres(k));
    end
end
datefile=str2double(buff_date);

end
