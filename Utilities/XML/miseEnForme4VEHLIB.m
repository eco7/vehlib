% Group: Functions
%
% Function: miseEnForme4VEHLIB
%
% Fonction de mise en forme des variables resultats de simulation VEHLIB dans une structure au format VEHLIB pour xml.
% La liste des variables se trouve dans <StructureType4VEHLIB>
%
% Arguments d'appel:
%
% vehlib_struct - structure vehlib
%
% vehlib_ws - workspace ou sont evaluees les variables
%
%           - 'base' pour une utilisation avec simulink
%           - 'caller' pour une utilisation en gestion d'energie
%
% VEHStruc - argument optionnel. A utiliser si l'on veut rajouter une
% table dans une structure existante. Dans ce cas, le programme conserve la
% partie header de la structure d'origine.
%
% Exemple de lancement:
% (start code)
% [VEHStruct err]=miseEnForme4VEHLIB(vehlib_struct,vehlib_ws,VEHStruct)
% (end)
%
% Resultat:
%
% VEHStruct = variable de type struct en format de travail, voir
% <Vehwiki/Format de fichier unifie VEH/XML at http://lx-veh2/mediawiki/index.php/Format_XML>
%
% Exemple de Struct (format de travail)
%(start code)
% Struct [1x1 struct]
%   head: [1x1 struct]
%       version: 'X.X'
%       date: 'jj-mmm-aaaa'
%       project: ''
%       comments: 'Created by miseEnForme4VEHLIB, version : X.X'
%       type: 'xml'
%   table: 1x1 cell {[1x1 struct]}
%       id: 1
%       metatable: [1x1 struct]
%           name: 'Vehicule : mon_vehicule- Cycle : CIN_XXX'
%           date: 'jj-mmm-aaaa'
%           sourcefile:  'Simulation VEHLIB- Numero de calcul : XXX'
%           comments:  'VEHLIB- VERSION : 20XXx'
%       variable_1: [1x1 struct]
%       ...
%       variable_m: [1x1 struct]
%           name: 'variable_m'
%           longname: 'variable_m en unit'
%           type: 'double'
%           unit: 'unit'
%           precision: '%f'
%           vector: [px1 double]
%(end)
%
function [VEHStruct,err]=miseEnForme4VEHLIB(vehlib_struct,vehlib_ws,varargin)

% Gestion des variables optionnelles: on s'attend soit a une structure XML
% valide, soit au nom de la structure contenant les variables resultats
if ~isempty(varargin)
    for i=1:length(varargin)
        if isstruct(varargin{i})
            [VEHStruct err errS]=verifFomatXML4Vehlib(varargin{i});
            if err~=0
                ERR=MException('VEHLIB:miseEnForme4VEHLIB', ...
                    sprintf('L''argument %d n''est pas une structure XML valide.\nCode erreur: %d\nComment: %s\nVar optionnelles possibles: une structure type XML ou un nom de structure dans laquelle chercher les variables.',2+i,err,errS));
                throw(ERR)
            end
        elseif ischar(varargin{i})
            if ~evalin(vehlib_ws,['exist(''' varargin{i} ''',''var'')']) || ~evalin(vehlib_ws,['isstruct(' varargin{i} ')'])
                ERR=MException('VEHLIB:miseEnForme4VEHLIB', ...
                    sprintf('L''argument %d n''est pas un nom de structure valide.\nVar optionnelles possibles: une structure type XML ou un nom de structure dans laquelle chercher les variables.',2+i));
                throw(ERR)
            else
                resStructName=varargin{i};
            end
        else
            ERR=MException('VEHLIB:miseEnForme4VEHLIB', ...
                sprintf('L''argument %d n''est pas du type attendu.\nVar optionnelles possibles: une structure type XML ou un nom de structure dans laquelle chercher les variables.',2+i));
            throw(ERR)
        end
    end
end

% chargement de la structure type
[VEHLIBStruct,comp,tabl]=StructureType4VEHLIB(vehlib_struct);

% creation du header

miseEnForme4VEHLIB.Version='0.2';

if ~exist('VEHStruct','var')
    % creation d'une structure, creation du header
    VEHStruct.head = struct;
    VEHStruct.head.version=miseEnForme4VEHLIB.Version;
    VEHStruct.head.date=date;
    VEHStruct.head.project='';
    VEHStruct.head.comments=['Created by miseEnForme4VEHLIB, version : ',VEHStruct.head.version];
    VEHStruct.head.type='vehlib results';
    
    nn=1;
else
    % ajout d'une structure, modification de la date du header
    VEHStruct.head.date=[VEHStruct.head.date,' modification ',date];
    
    nn=length(VEHStruct.table)+1;
end


% analyse de la structure vehlib (liste des tables a creer)
listeComp=fieldnames(vehlib_struct);

% information sur la source
% fnum=fullfile(evalin(vehlib_ws,'VD.INIT.InitialFolder'),'numcal.mat');
% num=evalin(vehlib_ws,['load(''',fnum,''');']);
% 
% if isfield(vehlib_struct,'simulink')
%     % simulation VEHLIB Simulink
%     if isfield(vehlib_struct,'CYCL')
%         source=['Simulation VEHLIB - Vehicule : ',vehlib_struct.nom , ' - Cycle : ',vehlib_struct.CYCL,' - Numero de calcul : ', num2str(num.numcal)];
%     else
%         source=['Simulation VEHLIB - Vehicule : ',vehlib_struct.nom , ' - Numero de calcul : ', num2str(num.numcal)];
%     end
% else 
%     % simulation VEHLIB gestion d'energie (backward)
%     if isfield(vehlib_struct,'CYCL')
%         source=['Simulation gestion d''energie VEHLIB - Vehicule : ',vehlib_struct.nom , ' - Cycle : ',vehlib_struct.CYCL,' - Numero de calcul : ', num2str(num.numcal)];
%     else
%         source=['Simulation gestion d''energie VEHLIB - Vehicule : ',vehlib_struct.nom , ' - Numero de calcul : ', num2str(num.numcal)];
%     end
% end

% creation des tables
for i=1:length(comp)
    ind=find(strcmp(listeComp,comp{i}));
    
    if ~isempty(ind)
        % le composant est present dans la structure type, recherche de la table correspondante 
        Id=tableId(listeComp{ind},VEHLIBStruct,comp,tabl);
    else
        % le composant est inconnu de la structure type, il ne sera pas traite
        Id=nan;
    end
    
    if ~isnan(Id)
        
        % renseignement de l'id et de la metatable
        VEHStruct.table{nn}.id=num2str(nn);
        
        VEHStruct.table{nn}.metatable=struct;
        VEHStruct.table{nn}.metatable.name=VEHLIBStruct.table{Id}.metatable.name;
        VEHStruct.table{nn}.metatable.date=eval(VEHLIBStruct.table{Id}.metatable.date);
%        VEHStruct.table{nn}.metatable.sourcefile=eval(VEHLIBStruct.table{Id}.metatable.sourcefile);
        VEHStruct.table{nn}.metatable.comments=evalin(vehlib_ws,VEHLIBStruct.table{Id}.metatable.comments);
        
        % renseignement des variables
        listvar=fieldnames(VEHLIBStruct.table{Id});
        for j=3:length(listvar)
            % les deux premiers champs recuperes dans listevar sont l'id et la metatable de la structure type VEHLIBStruct 
            nomvar=listvar{j};
            % test de la condition sur le variable
            
            % cas ou il y a plusieurs conditions
            if iscell(VEHLIBStruct.table{Id}.(nomvar).condition)
                k=1;
                % recherche de la condition
                while evalin(vehlib_ws,VEHLIBStruct.table{Id}.(nomvar).condition{k})~=1 && k<length(VEHLIBStruct.table{Id}.(nomvar).condition)
                    k=k+1;
                end
                % on remplace les conditions par la condition, les unites par l'unite et les formules du vecteur par la formule correspondant a la condition
                % si aucune condition n'est bonne on prend la derniere de la liste                
                VEHLIBStruct.table{Id}.(nomvar).condition=VEHLIBStruct.table{Id}.(nomvar).condition{k};
                VEHLIBStruct.table{Id}.(nomvar).unit=VEHLIBStruct.table{Id}.(nomvar).unit{k};
                VEHLIBStruct.table{Id}.(nomvar).vector=VEHLIBStruct.table{Id}.(nomvar).vector{k};
                    
            end
            
            % test de la condition sur le variable
            if evalin(vehlib_ws,VEHLIBStruct.table{Id}.(nomvar).condition)
                
                try
                    % test de l'existance de la variable dans le workspace
                    if exist('resStructName','var')
                        chaine = VEHLIBStruct.table{Id}.(nomvar).vector;
                        NewChaine = parseExpression(chaine,resStructName);
                        Values=evalin(vehlib_ws,NewChaine);
                        %evalin(vehlib_ws,[resStructName '.' VEHLIBStruct.table{Id}.(nomvar).vector,';']);
                    else
                        evalin(vehlib_ws,[VEHLIBStruct.table{Id}.(nomvar).vector,';']);
                    end
                    
                    % la variable est presente dans le workspace, on renseigne la structure de sortie VEHStruct
                    VEHStruct.table{nn}.(nomvar)=VEHLIBStruct.table{Id}.(nomvar);
                    if exist('resStructName','var')
                        VEHStruct.table{nn}.(nomvar).vector=Values;
                    else
                        VEHStruct.table{nn}.(nomvar).vector=evalin(vehlib_ws,VEHStruct.table{nn}.(nomvar).vector);
                    end
                    VEHStruct.table{nn}.(nomvar)=rmfield(VEHStruct.table{nn}.(nomvar),'condition');
                catch
                    % La condition est remplie mais la variable est abscente du workspace. 
                    % evolution possible : faire un fichier log
                    %warning('VEHLIB:miseEnForme4VEHLIB',[VEHStruct.table{nn}.metatable.name,' : ',nomvar,' abscent(e) du workspace'] )
                    
                end
            end
        end
        
        if length(fieldnames(VEHStruct.table{nn}))>2
            nn=nn+1;
        else
            VEHStruct.table(nn)=[];
        end
           
    end
end

[VEHStruct err errS]=verifFomatXML4Vehlib(VEHStruct);
if err < 0
    %Construct an MException object to represent the error.
    err = MException('VEHLIB:miseEnForme4VEHLIB', ...
        sprintf('%s,%d,%s','Incompatible structure, error code: ',err,errS));
    %Throw the exception to stop execution and display an error message.
    throw(err)
end


end

% Function: StructureType4VEHLIB
%
% Renvoie la structure type et les listes des composants et des noms de table leurs correspondants.
%
% La structure type:
%
% la struture type contient toutes les tables pour tous les cas, chaque variable contient un champ 'condition' pour evaluer la variable, 'true' si pas de condition particuliere
%
% - VEHLIBStruct.table{n}.nomvar.condition='true' par defaut
% - VEHLIBStruct.table{n}.nomvar.condition='VD.CYCL.ntypcin=3' par exemple
%
%(start code)
%         VEHLIBStruct.table{n}.id=num2str(n);
%
%         VEHLIBStruct.table{n}.metatable=struct;
%         VEHLIBStruct.table{n}.metatable.name='VARIABLES CINEMATIQUES';
%         VEHLIBStruct.table{n}.metatable.date=date;
%         VEHLIBStruct.table{n}.metatable.sourcefile=source;
%         VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB-VERSION : '',VD.INIT.version ]';
%
%         VEHLIBStruct.table{n}.tsim=struct;
%         VEHLIBStruct.table{n}.tsim.condition='true' ;
%         VEHLIBStruct.table{n}.tsim.name='tsim';
%         VEHLIBStruct.table{n}.tsim.longname='Temps';
%         VEHLIBStruct.table{n}.tsim.type='double';
%         VEHLIBStruct.table{n}.tsim.unit='s';
%         VEHLIBStruct.table{n}.tsim.precision='%f';
%         VEHLIBStruct.table{n}.tsim.vector='tsim';
%(end)
%
%         etc
%
%
% Association composant / nom de table / id de table:
%
% - le nom dans "comp" correspond au nom du composant dans la structure vehlib
% - le nom dans "tabl" correspond au nom dans la metatable VEHLIBStruct, les tables dans VEHLIStruct ne sont pas dans le meme ordre que dans "tabl".
%
% exemple : au composant 'acm2' correspond la table 'VARIABLES GENERATEUR', l'id de la table est recherche par la fonction <tableId>, dans cet exemple l'id = 9
%
% Group: Enrichissement de la structure type
%
% Procedure pour ajouter une variable ou une table a la structure type
%
% Topic: Ajout d'une variable dans une table existance (un composant existant):
% 
% Exemple : nous ajoutons la variable 'nouvellevariable' au composant 'COMP'.
%
% Aucune variable ne possede le meme nom dans la table:
% 
% Il faut ajouter le bloc suivant dans la table.
%
%(start code)
%     VEHLIBStruct.table{n}.nouvellevariable=struct;
%     VEHLIBStruct.table{n}.nouvellevariable.condition='condition de la nouvelle variable';
%     VEHLIBStruct.table{n}.nouvellevariable.name='nouvellevariable';
%     VEHLIBStruct.table{n}.nouvellevariable.longname='La nouvelle variable';
%     VEHLIBStruct.table{n}.nouvellevariable.type='double';
%     VEHLIBStruct.table{n}.nouvellevariable.unit='unite de la variable';
%     VEHLIBStruct.table{n}.nouvellevariable.precision='%f';
%     VEHLIBStruct.table{n}.nouvellevariable.vector='nom de la nouvelle variable dans le workspace';
% (end)
%
% Les champs "condition" et "vector" sont evalues dans le workspace pour renseigner la structure de sortie "VEHStruct" dans <miseEnForme4VEHLIB>.
%
% La condition permet d'ajouter ou non la variable lors de la mise en forme pour VEHLIB. 
%
% Si par exemple le composant existe avec 2 types, 'COMP.ntypcomp=1' et 'COMP.ntypcomp=2'.
%
% - La variable existe pour les 2 types. La condition sera 'true'.
% - La variable n'est presente que pour le type 2. La condition sera 'COMP.ntypcomp==2'
%
% Une variable porte deja le meme nom dans la table mais pas dans le workspace (et / ou n'a pas la meme unite).:
%
% Il faut utiliser le champ "condition" et modifier les champs "unit" et "vector". 
%
% Dans notre exemple on suppose que pour le composant de type 1 la variable s'ecrit 'nvar' et son unite est le 'truc' tandis que pour le composant de type 2 la variable s'ecrit 'Mvar' et son unite le 'millitruc'
%
%(start code)
%     VEHLIBStruct.table{n}.nouvellevariable.condition={'COMP.ntypcomp==1' 'COMP.ntypcomp==2'};
%     ...
%     VEHLIBStruct.table{n}.nouvellevariable.unit={'truc' 'millitruc'};
%     ...
%     VEHLIBStruct.table{n}.nouvellevariable.vector={'nvar' 'Mvar'};
% (end)
%
% Cette construction est visible pour la pente dans les VARIABLES CINEMATIQUES
% 
% 
% Topic: Ajout d'une table (d'un composant):
%
% 
% Il faut ajouter dans "comp" le nom du composant tel qu'ecrit dans la structure vehlib. (exemple : 'nouveau_composant')
%
% Il faut ajouter dans "tabl" le nom de la table tel qu'il sera dans la metatable. (exemple : 'VARIABLES NOUVEAU COMPOSANT')
% 
%
% A la suite de VEHLIBStruct (fin du progamme) il faut ajouter le bloc suivant
%
% (start code)
% %    'VARIABLES NOUVEAU COMPOSANT'
% 
% 
%     n=length(VEHLIBStruct.table)+1;    % augmentation du numero de table
% 
%     % id et metatable
%     VEHLIBStruct.table{n}.id=num2str(n);
% 
%     VEHLIBStruct.table{n}.metatable=struct;
%     VEHLIBStruct.table{n}.metatable.name='VARIABLES NOUVEAU COMPOSANT';  % nom de la table
%     VEHLIBStruct.table{n}.metatable.date='date';
%     VEHLIBStruct.table{n}.metatable.sourcefile='source';
%     VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';
% (end)
%
% Il faut ajouter les variables comme vu plus haut
%
%
% 
% Remarque:
% Les ajouts de VEHLIBStruct peuvent etre realises dans une fonction externe apppele conditionnellement (voir VARIABLES PAC)
%
function [VEHLIBStruct,comp,tabl]=StructureType4VEHLIB(vehlib_struct)


comp={'CYCL'
    'ECU'
    'VEHI'
    'RED'
    'RED2'
    'CVT'
    'BV'
    'EMBR1'
    'CONV'
    'EMBR2'
    'ACM1'
    'ACM2'
    'ADCPL'
    'ADCPL1'
    'ADCPL2'
    'EPI'
    'MOTH'
    'SURV'
    'DCDC_1'
    'DCDC_2'
    'PANTO'
    'BATT'
    'SC'
    'ACC'
    'RH'
    'CHRG'};

tabl={'VARIABLES CINEMATIQUES'
    'VARIABLES CALCULATEUR'
    'VARIABLES VEHICULE'
    'VARIABLES REDUCTEUR'
    'VARIABLES REDUCTEUR 2'
    'VARIABLES CVT'
    'VARIABLES BOITE DE VITESSE'
    'VARIABLES EMBRAYAGE'
    'VARIABLES CONVERTISSEUR DE COUPLE'
    'VARIABLES EMBRAYAGE 2'
    'VARIABLES MOTEUR ELECTRIQUE'
    'VARIABLES GENERATEUR'
    'VARIABLES COUPLEUR'
    'VARIABLES COUPLEUR 1'
    'VARIABLES COUPLEUR 2'
    'VARIABLES TRAIN EPICYCLOIDALE'
    'VARIABLES MOTEUR THERMIQUE'
    'VARIABLES SURVOLTEUR'
    'VARIABLES DCDC_1'
    'VARIABLES DCDC_2'
    'VARIABLES PANTOGRAPHE'
    'VARIABLES BATTERIE'
    'VARIABLES SUPER CONDENSATEUR'
    'VARIABLES ACCESSOIRES'
    'VARIABLES RHEOSTAT'
    'VARIABLES CHARGEUR'};


% 'VARIABLES CINEMATIQUES';

n=1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES CINEMATIQUES';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.tsim=struct;
VEHLIBStruct.table{n}.tsim.condition='true';
VEHLIBStruct.table{n}.tsim.name='tsim';
VEHLIBStruct.table{n}.tsim.longname='Temps';
VEHLIBStruct.table{n}.tsim.type='double';
VEHLIBStruct.table{n}.tsim.unit='s';
VEHLIBStruct.table{n}.tsim.precision='%f';
VEHLIBStruct.table{n}.tsim.vector='tsim';

VEHLIBStruct.table{n}.vit=struct;
VEHLIBStruct.table{n}.vit.condition='true';
VEHLIBStruct.table{n}.vit.name='vit';
VEHLIBStruct.table{n}.vit.longname='Vitesse du vehicule';
VEHLIBStruct.table{n}.vit.type='double';
VEHLIBStruct.table{n}.vit.unit='m/s';
VEHLIBStruct.table{n}.vit.precision='%f';
VEHLIBStruct.table{n}.vit.vector='vit';

VEHLIBStruct.table{n}.distance=struct;
VEHLIBStruct.table{n}.distance.condition='true';
VEHLIBStruct.table{n}.distance.name='distance';
VEHLIBStruct.table{n}.distance.longname='Distance parcourue';
VEHLIBStruct.table{n}.distance.type='double';
VEHLIBStruct.table{n}.distance.unit='m';
VEHLIBStruct.table{n}.distance.precision='%f';
VEHLIBStruct.table{n}.distance.vector='distance';

VEHLIBStruct.table{n}.distance_ant=struct;
VEHLIBStruct.table{n}.distance_ant.condition='isfield(vehlib,''CYCL'') && VD.CYCL.ntypcin==4';
VEHLIBStruct.table{n}.distance_ant.name='distance_ant';
VEHLIBStruct.table{n}.distance_ant.longname='Distance avec facteur d anticipation du conducteur';
VEHLIBStruct.table{n}.distance_ant.type='double';
VEHLIBStruct.table{n}.distance_ant.unit='m/s';
VEHLIBStruct.table{n}.distance_ant.precision='%f';
VEHLIBStruct.table{n}.distance_ant.vector='distance_ant';

VEHLIBStruct.table{n}.acc=struct;
VEHLIBStruct.table{n}.acc.condition='true';
VEHLIBStruct.table{n}.acc.name='acc';
VEHLIBStruct.table{n}.acc.longname='Acceleration du vehicule';
VEHLIBStruct.table{n}.acc.type='double';
VEHLIBStruct.table{n}.acc.unit='m/s2';
VEHLIBStruct.table{n}.acc.precision='%f';
VEHLIBStruct.table{n}.acc.vector='acc';

VEHLIBStruct.table{n}.vit_dem=struct;
VEHLIBStruct.table{n}.vit_dem.condition='true';
VEHLIBStruct.table{n}.vit_dem.name='vit_dem';
VEHLIBStruct.table{n}.vit_dem.longname='Vitesse demandee';
VEHLIBStruct.table{n}.vit_dem.type='double';
VEHLIBStruct.table{n}.vit_dem.unit='m/s';
VEHLIBStruct.table{n}.vit_dem.precision='%f';
VEHLIBStruct.table{n}.vit_dem.vector='vit_dem';

VEHLIBStruct.table{n}.vit_bal=struct;
VEHLIBStruct.table{n}.vit_bal.condition='isfield(vehlib,''CYCL'') && VD.CYCL.ntypcin==2';
VEHLIBStruct.table{n}.vit_bal.name='vit_bal';
VEHLIBStruct.table{n}.vit_bal.longname='Vitesse balise';
VEHLIBStruct.table{n}.vit_bal.type='double';
VEHLIBStruct.table{n}.vit_bal.unit='m/s';
VEHLIBStruct.table{n}.vit_bal.precision='%f';
VEHLIBStruct.table{n}.vit_bal.vector='vit_bal';

VEHLIBStruct.table{n}.vmax=struct;
VEHLIBStruct.table{n}.vmax.condition='isfield(vehlib,''CYCL'') && VD.CYCL.ntypcin==4';
VEHLIBStruct.table{n}.vmax.name='vmax';
VEHLIBStruct.table{n}.vmax.longname='Vitesse maximum du trajet';
VEHLIBStruct.table{n}.vmax.type='double';
VEHLIBStruct.table{n}.vmax.unit='m/s';
VEHLIBStruct.table{n}.vmax.precision='%f';
VEHLIBStruct.table{n}.vmax.vector='vmax';

VEHLIBStruct.table{n}.vmax_drv=struct;
VEHLIBStruct.table{n}.vmax_drv.condition='isfield(vehlib,''CYCL'') && VD.CYCL.ntypcin==4';
VEHLIBStruct.table{n}.vmax_drv.name='vmax_drv';
VEHLIBStruct.table{n}.vmax_drv.longname='Vitesse maximum du trajet avec coef. de survitesse';
VEHLIBStruct.table{n}.vmax_drv.type='double';
VEHLIBStruct.table{n}.vmax_drv.unit='m/s';
VEHLIBStruct.table{n}.vmax_drv.precision='%f';
VEHLIBStruct.table{n}.vmax_drv.vector='vmax_drv';

VEHLIBStruct.table{n}.vmax_ant=struct;
VEHLIBStruct.table{n}.vmax_ant.condition='isfield(vehlib,''CYCL'') && VD.CYCL.ntypcin==4';
VEHLIBStruct.table{n}.vmax_ant.name='vmax_ant';
VEHLIBStruct.table{n}.vmax_ant.longname='Vitesse maximum du trajet avec coef. de survitesse et anticipation';
VEHLIBStruct.table{n}.vmax_ant.type='double';
VEHLIBStruct.table{n}.vmax_ant.unit='m/s';
VEHLIBStruct.table{n}.vmax_ant.precision='%f';
VEHLIBStruct.table{n}.vmax_ant.vector='vmax_ant';

VEHLIBStruct.table{n}.PosVoitQ=struct;
VEHLIBStruct.table{n}.PosVoitQ.condition='isfield(vehlib,''VEHI'') && VD.VEHI.ntypveh==3';
VEHLIBStruct.table{n}.PosVoitQ.name='PosVoitQ';
VEHLIBStruct.table{n}.PosVoitQ.longname='Position voiture de queue';
VEHLIBStruct.table{n}.PosVoitQ.type='double';
VEHLIBStruct.table{n}.PosVoitQ.unit='';
VEHLIBStruct.table{n}.PosVoitQ.precision='%f';
VEHLIBStruct.table{n}.PosVoitQ.vector='squeeze(pozitie(1,end,:))./1000';

VEHLIBStruct.table{n}.courbure=struct;
VEHLIBStruct.table{n}.courbure.condition='isfield(vehlib,''VEHI'') && VD.VEHI.ntypveh==3';
VEHLIBStruct.table{n}.courbure.name='courbure';
VEHLIBStruct.table{n}.courbure.longname='Courbure';
VEHLIBStruct.table{n}.courbure.type='double';
VEHLIBStruct.table{n}.courbure.unit='1/m';
VEHLIBStruct.table{n}.courbure.precision='%f';
VEHLIBStruct.table{n}.courbure.vector='squeeze(curbura(1,1,:))';

% VEHLIBStruct.table{n}.pente=struct;
% VEHLIBStruct.table{n}.pente.condition={'isfield(vehlib,''vehicule'') && VD.VEHI.ntypveh~=3' 'isfield(vehlib,''vehicule'') && VD.VEHI.ntypveh==3'} ;
% VEHLIBStruct.table{n}.pente.name='pente';
% VEHLIBStruct.table{n}.pente.longname='Pente';
% VEHLIBStruct.table{n}.pente.type='double';
% VEHLIBStruct.table{n}.pente.unit={'%' 'mm/m'};
% VEHLIBStruct.table{n}.pente.precision='%f';
% VEHLIBStruct.table{n}.pente.vector={'pente' 'squeeze(panta(1,1,:))./1000'};

VEHLIBStruct.table{n}.pente=struct;
VEHLIBStruct.table{n}.pente.condition='true';
VEHLIBStruct.table{n}.pente.name='pente';
VEHLIBStruct.table{n}.pente.longname='Pente';
VEHLIBStruct.table{n}.pente.type='double';
VEHLIBStruct.table{n}.pente.unit='%';
VEHLIBStruct.table{n}.pente.precision='%f';
VEHLIBStruct.table{n}.pente.vector='pente';

% 'VARIABLES CALCULATEUR'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES CALCULATEUR';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.elhyb=struct;
VEHLIBStruct.table{n}.elhyb.condition='~strcmpi(vehlib.architecture,''VTH_BV'') || ~strcmpi(vehlib.architecture,''VTH_CPLHYD'') || ~strcmpi(vehlib.architecture,''VELEC'') || ~strcmpi(vehlib.architecture,''VELEC_BAT_SC'') || ~strcmpi(vehlib.architecture,''UTILITAIRES'') || ~strcmpi(vehlib.architecture,''UTILITAIRES_BATTERIE'')';
VEHLIBStruct.table{n}.elhyb.name='elhyb';
VEHLIBStruct.table{n}.elhyb.longname='Consigne electrique/hybrid (0/1)';
VEHLIBStruct.table{n}.elhyb.type='double';
VEHLIBStruct.table{n}.elhyb.unit='';
VEHLIBStruct.table{n}.elhyb.precision='%f';
VEHLIBStruct.table{n}.elhyb.vector='elhyb';

VEHLIBStruct.table{n}.pdem=struct;
VEHLIBStruct.table{n}.pdem.condition='true';
VEHLIBStruct.table{n}.pdem.name='pdem';
VEHLIBStruct.table{n}.pdem.longname='Puissance de traction demandee';
VEHLIBStruct.table{n}.pdem.type='double';
VEHLIBStruct.table{n}.pdem.unit='W';
VEHLIBStruct.table{n}.pdem.precision='%f';
VEHLIBStruct.table{n}.pdem.vector='pdem';

VEHLIBStruct.table{n}.cons_totale=struct;
VEHLIBStruct.table{n}.cons_totale.condition='true';
VEHLIBStruct.table{n}.cons_totale.name='cons_totale';
VEHLIBStruct.table{n}.cons_totale.longname='Consigne de couple conducteur';
VEHLIBStruct.table{n}.cons_totale.type='double';
VEHLIBStruct.table{n}.cons_totale.unit='Nm';
VEHLIBStruct.table{n}.cons_totale.precision='%f';
VEHLIBStruct.table{n}.cons_totale.vector='cons_totale';

VEHLIBStruct.table{n}.cons_trac=struct;
VEHLIBStruct.table{n}.cons_trac.condition='true';
VEHLIBStruct.table{n}.cons_trac.name='cons_trac';
VEHLIBStruct.table{n}.cons_trac.longname='Consigne de couple traction';
VEHLIBStruct.table{n}.cons_trac.type='double';
VEHLIBStruct.table{n}.cons_trac.unit='Nm';
VEHLIBStruct.table{n}.cons_trac.precision='%f';
VEHLIBStruct.table{n}.cons_trac.vector='cons_trac';

VEHLIBStruct.table{n}.pres=struct;
VEHLIBStruct.table{n}.pres.condition='true';
VEHLIBStruct.table{n}.pres.name='pres';
VEHLIBStruct.table{n}.pres.longname='Puissance reseau';
VEHLIBStruct.table{n}.pres.type='double';
VEHLIBStruct.table{n}.pres.unit='W';
VEHLIBStruct.table{n}.pres.precision='%f';
VEHLIBStruct.table{n}.pres.vector='pres';

VEHLIBStruct.table{n}.cons_acm1=struct;
VEHLIBStruct.table{n}.cons_acm1.condition='isfield(vehlib,''ACM1'')';
VEHLIBStruct.table{n}.cons_acm1.name='cons_acm1';
VEHLIBStruct.table{n}.cons_acm1.longname='Consigne couple moteur electrique';
VEHLIBStruct.table{n}.cons_acm1.type='double';
VEHLIBStruct.table{n}.cons_acm1.unit='Nm';
VEHLIBStruct.table{n}.cons_acm1.precision='%f';
VEHLIBStruct.table{n}.cons_acm1.vector='cons_acm1';

VEHLIBStruct.table{n}.alim_acm1=struct;
VEHLIBStruct.table{n}.alim_acm1.condition='isfield(vehlib,''ACM1'')';
VEHLIBStruct.table{n}.alim_acm1.name='alim_acm1';
VEHLIBStruct.table{n}.alim_acm1.longname='Consigne alimentation moteur electrique (0 active/1)';
VEHLIBStruct.table{n}.alim_acm1.type='double';
VEHLIBStruct.table{n}.alim_acm1.unit='';
VEHLIBStruct.table{n}.alim_acm1.precision='%f';
VEHLIBStruct.table{n}.alim_acm1.vector='alim_acm1';

VEHLIBStruct.table{n}.cons_acm2=struct;
VEHLIBStruct.table{n}.cons_acm2.condition='isfield(vehlib,''ACM2'')';
VEHLIBStruct.table{n}.cons_acm2.name='cons_acm2';
VEHLIBStruct.table{n}.cons_acm2.longname='Consigne de couple machine electrique 2';
VEHLIBStruct.table{n}.cons_acm2.type='double';
VEHLIBStruct.table{n}.cons_acm2.unit='Nm';
VEHLIBStruct.table{n}.cons_acm2.precision='%f';
VEHLIBStruct.table{n}.cons_acm2.vector='cons_acm2';

VEHLIBStruct.table{n}.alim_acm2=struct;
VEHLIBStruct.table{n}.alim_acm2.condition='isfield(vehlib,''ACM2'')';
VEHLIBStruct.table{n}.alim_acm2.name='alim_acm2';
VEHLIBStruct.table{n}.alim_acm2.longname='Consigne alimentation moteur electrique 2(0 active/1)';
VEHLIBStruct.table{n}.alim_acm2.type='double';
VEHLIBStruct.table{n}.alim_acm2.unit='';
VEHLIBStruct.table{n}.alim_acm2.precision='%f';
VEHLIBStruct.table{n}.alim_acm2.vector='alim_acm2';

VEHLIBStruct.table{n}.cons_mt=struct;
VEHLIBStruct.table{n}.cons_mt.condition='isfield(vehlib,''MOTH'')';
VEHLIBStruct.table{n}.cons_mt.name='cons_mt';
VEHLIBStruct.table{n}.cons_mt.longname='Consigne calculateur du moteur thermique';
VEHLIBStruct.table{n}.cons_mt.type='double';
VEHLIBStruct.table{n}.cons_mt.unit='Nm';
VEHLIBStruct.table{n}.cons_mt.precision='%f';
VEHLIBStruct.table{n}.cons_mt.vector='cons_mt';

VEHLIBStruct.table{n}.alim_mt=struct;
VEHLIBStruct.table{n}.alim_mt.condition='isfield(vehlib,''MOTH'')';
VEHLIBStruct.table{n}.alim_mt.name='alim_mt';
VEHLIBStruct.table{n}.alim_mt.longname='Alimentation calculateur du moteur thermique';
VEHLIBStruct.table{n}.alim_mt.type='double';
VEHLIBStruct.table{n}.alim_mt.unit='';
VEHLIBStruct.table{n}.alim_mt.precision='%f';
VEHLIBStruct.table{n}.alim_mt.vector='alim_mt';

VEHLIBStruct.table{n}.cacm1_max_bat=struct;
VEHLIBStruct.table{n}.cacm1_max_bat.condition='isfield(vehlib,''BATT'')';
VEHLIBStruct.table{n}.cacm1_max_bat.name='cacm1_max_bat';
VEHLIBStruct.table{n}.cacm1_max_bat.longname='Couple maxi (acm1+bat)';
VEHLIBStruct.table{n}.cacm1_max_bat.type='double';
VEHLIBStruct.table{n}.cacm1_max_bat.unit='Nm';
VEHLIBStruct.table{n}.cacm1_max_bat.precision='%f';
VEHLIBStruct.table{n}.cacm1_max_bat.vector='cacm1_max_bat';

VEHLIBStruct.table{n}.cacm1_min_bat=struct;
VEHLIBStruct.table{n}.cacm1_min_bat.condition='isfield(vehlib,''BATT'')';
VEHLIBStruct.table{n}.cacm1_min_bat.name='cacm1_min_bat';
VEHLIBStruct.table{n}.cacm1_min_bat.longname='Couple mini (acm1+bat)';
VEHLIBStruct.table{n}.cacm1_min_bat.type='double';
VEHLIBStruct.table{n}.cacm1_min_bat.unit='Nm';
VEHLIBStruct.table{n}.cacm1_min_bat.precision='%f';
VEHLIBStruct.table{n}.cacm1_min_bat.vector='cacm1_min_bat';

VEHLIBStruct.table{n}.cacm1_max_sc=struct;
VEHLIBStruct.table{n}.cacm1_max_sc.condition='isfield(vehlib,''SC'')';
VEHLIBStruct.table{n}.cacm1_max_sc.name='cacm1_max_sc';
VEHLIBStruct.table{n}.cacm1_max_sc.longname='Couple maxi (acm1+sc)';
VEHLIBStruct.table{n}.cacm1_max_sc.type='double';
VEHLIBStruct.table{n}.cacm1_max_sc.unit='Nm';
VEHLIBStruct.table{n}.cacm1_max_sc.precision='%f';
VEHLIBStruct.table{n}.cacm1_max_sc.vector='cacm1_max_sc';

VEHLIBStruct.table{n}.cacm1_min_sc=struct;
VEHLIBStruct.table{n}.cacm1_min_sc.condition='isfield(vehlib,''SC'')';
VEHLIBStruct.table{n}.cacm1_min_sc.name='cacm1_min_sc';
VEHLIBStruct.table{n}.cacm1_min_sc.longname='Couple mini (acm1+sc)';
VEHLIBStruct.table{n}.cacm1_min_sc.type='double';
VEHLIBStruct.table{n}.cacm1_min_sc.unit='Nm';
VEHLIBStruct.table{n}.cacm1_min_sc.precision='%f';
VEHLIBStruct.table{n}.cacm1_min_sc.vector='cacm1_min_sc';

VEHLIBStruct.table{n}.cons_emb1=struct;
VEHLIBStruct.table{n}.cons_emb1.condition='isfield(vehlib,''EMBR1'')';
VEHLIBStruct.table{n}.cons_emb1.name='cons_emb1';
VEHLIBStruct.table{n}.cons_emb1.longname='Consigne embrayage 1';
VEHLIBStruct.table{n}.cons_emb1.type='double';
VEHLIBStruct.table{n}.cons_emb1.unit='';
VEHLIBStruct.table{n}.cons_emb1.precision='%f';
VEHLIBStruct.table{n}.cons_emb1.vector='cons_emb1';

VEHLIBStruct.table{n}.cons_emb2=struct;
VEHLIBStruct.table{n}.cons_emb2.condition='isfield(vehlib,''EMBR2'')';
VEHLIBStruct.table{n}.cons_emb2.name='cons_emb2';
VEHLIBStruct.table{n}.cons_emb2.longname='Consigne embrayage 2';
VEHLIBStruct.table{n}.cons_emb2.type='double';
VEHLIBStruct.table{n}.cons_emb2.unit='';
VEHLIBStruct.table{n}.cons_emb2.precision='%f';
VEHLIBStruct.table{n}.cons_emb2.vector='cons_emb2';

%%%%%%%%% fait un peu doublon avec wmtdes %%%%%%%%%%%%%%%
% VEHLIBStruct.table{n}.wmtdes_ge=struct;
% VEHLIBStruct.table{n}.wmtdes_ge.condition='strcmpi(vehlib.architecture,''HSER_GE'')';
% VEHLIBStruct.table{n}.wmtdes_ge.name='wmtdes';
% VEHLIBStruct.table{n}.wmtdes_ge.longname='Regime cible du groupe electrogene MT+AL';
% VEHLIBStruct.table{n}.wmtdes_ge.type='double';
% VEHLIBStruct.table{n}.wmtdes_ge.unit='tr/mn';
% VEHLIBStruct.table{n}.wmtdes_ge.precision='%f';
% VEHLIBStruct.table{n}.wmtdes_ge.vector='wmtdes.*30/pi';

VEHLIBStruct.table{n}.Pdem_bat=struct;
VEHLIBStruct.table{n}.Pdem_bat.condition='strcmpi(vehlib.architecture,''HPDP_EPI'') ||strcmpi(vehlib.architecture,''HPDP_EPI_SURV'') || strcmpi(vehlib.architecture,''HSER_GE'')';
VEHLIBStruct.table{n}.Pdem_bat.name='Pdem_bat';
VEHLIBStruct.table{n}.Pdem_bat.longname='Puissance demandee pour recharger la batterie';
VEHLIBStruct.table{n}.Pdem_bat.type='double';
VEHLIBStruct.table{n}.Pdem_bat.unit='W';
VEHLIBStruct.table{n}.Pdem_bat.precision='%f';
VEHLIBStruct.table{n}.Pdem_bat.vector='Pdem_bat';

VEHLIBStruct.table{n}.wmtdes=struct;
VEHLIBStruct.table{n}.wmtdes.condition='strcmpi(vehlib.architecture,''HSER_GE'') || strcmpi(vehlib.architecture,''HPDP_EPI'') ||strcmpi(vehlib.architecture,''HPDP_EPI_SURV'')';
VEHLIBStruct.table{n}.wmtdes.name='wmtdes';
VEHLIBStruct.table{n}.wmtdes.longname='Regime cible du moteur thermique';
VEHLIBStruct.table{n}.wmtdes.type='double';
VEHLIBStruct.table{n}.wmtdes.unit='rd/s';
VEHLIBStruct.table{n}.wmtdes.precision='%f';
VEHLIBStruct.table{n}.wmtdes.vector='wmtdes';

VEHLIBStruct.table{n}.Pdem_mt=struct;
VEHLIBStruct.table{n}.Pdem_mt.condition='strcmpi(vehlib.architecture,''HPDP_EPI'') ||strcmpi(vehlib.architecture,''HPDP_EPI_SURV'')';
VEHLIBStruct.table{n}.Pdem_mt.name='Pdem_mt';
VEHLIBStruct.table{n}.Pdem_mt.longname='Puissance demandee au moteur thermique';
VEHLIBStruct.table{n}.Pdem_mt.type='double';
VEHLIBStruct.table{n}.Pdem_mt.unit='W';
VEHLIBStruct.table{n}.Pdem_mt.precision='%f';
VEHLIBStruct.table{n}.Pdem_mt.vector='Pdem_mt';

% 'VARIABLES REDUCTEUR'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES REDUCTEUR';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.wsec_red=struct;
VEHLIBStruct.table{n}.wsec_red.condition='true';
VEHLIBStruct.table{n}.wsec_red.name='wsec_red';
VEHLIBStruct.table{n}.wsec_red.longname='Vitesse de rotation secondaire reducteur';
VEHLIBStruct.table{n}.wsec_red.type='double';
VEHLIBStruct.table{n}.wsec_red.unit='rd/s';
VEHLIBStruct.table{n}.wsec_red.precision='%f';
VEHLIBStruct.table{n}.wsec_red.vector='wsec_red';

VEHLIBStruct.table{n}.csec_red=struct;
VEHLIBStruct.table{n}.csec_red.condition='true';
VEHLIBStruct.table{n}.csec_red.name='csec_red';
VEHLIBStruct.table{n}.csec_red.longname='Couple secondaire reducteur';
VEHLIBStruct.table{n}.csec_red.type='double';
VEHLIBStruct.table{n}.csec_red.unit='Nm';
VEHLIBStruct.table{n}.csec_red.precision='%f';
VEHLIBStruct.table{n}.csec_red.vector='csec_red';

VEHLIBStruct.table{n}.wprim_red=struct;
VEHLIBStruct.table{n}.wprim_red.condition='true';
VEHLIBStruct.table{n}.wprim_red.name='wprim_red';
VEHLIBStruct.table{n}.wprim_red.longname='Vitesse de rotation primaire reducteur';
VEHLIBStruct.table{n}.wprim_red.type='double';
VEHLIBStruct.table{n}.wprim_red.unit='rd/s';
VEHLIBStruct.table{n}.wprim_red.precision='%f';
VEHLIBStruct.table{n}.wprim_red.vector='wprim_red';

VEHLIBStruct.table{n}.cprim_red=struct;
VEHLIBStruct.table{n}.cprim_red.condition='true';
VEHLIBStruct.table{n}.cprim_red.name='cprim_red';
VEHLIBStruct.table{n}.cprim_red.longname='Couple primaire reducteur';
VEHLIBStruct.table{n}.cprim_red.type='double';
VEHLIBStruct.table{n}.cprim_red.unit='Nm';
VEHLIBStruct.table{n}.cprim_red.precision='%f';
VEHLIBStruct.table{n}.cprim_red.vector='cprim_red';

VEHLIBStruct.table{n}.Psec_red=struct;
VEHLIBStruct.table{n}.Psec_red.condition='true';
VEHLIBStruct.table{n}.Psec_red.name='Psec_red';
VEHLIBStruct.table{n}.Psec_red.longname='Puissance secondaire reducteur';
VEHLIBStruct.table{n}.Psec_red.type='double';
VEHLIBStruct.table{n}.Psec_red.unit='W';
VEHLIBStruct.table{n}.Psec_red.precision='%f';
VEHLIBStruct.table{n}.Psec_red.vector='csec_red.*wsec_red';

VEHLIBStruct.table{n}.Pprim_red=struct;
VEHLIBStruct.table{n}.Pprim_red.condition='true';
VEHLIBStruct.table{n}.Pprim_red.name='Pprim_red';
VEHLIBStruct.table{n}.Pprim_red.longname='Puissance primaire reducteur';
VEHLIBStruct.table{n}.Pprim_red.type='double';
VEHLIBStruct.table{n}.Pprim_red.unit='W';
VEHLIBStruct.table{n}.Pprim_red.precision='%f';
VEHLIBStruct.table{n}.Pprim_red.vector='cprim_red.*wprim_red';

% 'VARIABLES REDUCTEUR2'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES REDUCTEUR 2';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.wsec_red2=struct;
VEHLIBStruct.table{n}.wsec_red2.condition='true';
VEHLIBStruct.table{n}.wsec_red2.name='wsec_red2';
VEHLIBStruct.table{n}.wsec_red2.longname='Vitesse de rotation secondaire reducteur 2';
VEHLIBStruct.table{n}.wsec_red2.type='double';
VEHLIBStruct.table{n}.wsec_red2.unit='rd/s';
VEHLIBStruct.table{n}.wsec_red2.precision='%f';
VEHLIBStruct.table{n}.wsec_red2.vector='wsec_red2';

VEHLIBStruct.table{n}.csec_red2=struct;
VEHLIBStruct.table{n}.csec_red2.condition='true';
VEHLIBStruct.table{n}.csec_red2.name='csec_red2';
VEHLIBStruct.table{n}.csec_red2.longname='Couple secondaire reducteur 2';
VEHLIBStruct.table{n}.csec_red2.type='double';
VEHLIBStruct.table{n}.csec_red2.unit='Nm';
VEHLIBStruct.table{n}.csec_red2.precision='%f';
VEHLIBStruct.table{n}.csec_red2.vector='csec_red2';

VEHLIBStruct.table{n}.wprim_red2=struct;
VEHLIBStruct.table{n}.wprim_red2.condition='true';
VEHLIBStruct.table{n}.wprim_red2.name='wprim_red2';
VEHLIBStruct.table{n}.wprim_red2.longname='Vitesse de rotation primaire reducteur 2';
VEHLIBStruct.table{n}.wprim_red2.type='double';
VEHLIBStruct.table{n}.wprim_red2.unit='rd/s';
VEHLIBStruct.table{n}.wprim_red2.precision='%f';
VEHLIBStruct.table{n}.wprim_red2.vector='wprim_red2';

VEHLIBStruct.table{n}.cprim_red2=struct;
VEHLIBStruct.table{n}.cprim_red2.condition='true';
VEHLIBStruct.table{n}.cprim_red2.name='cprim_red2';
VEHLIBStruct.table{n}.cprim_red2.longname='Couple primaire reducteur 2';
VEHLIBStruct.table{n}.cprim_red2.type='double';
VEHLIBStruct.table{n}.cprim_red2.unit='Nm';
VEHLIBStruct.table{n}.cprim_red2.precision='%f';
VEHLIBStruct.table{n}.cprim_red2.vector='cprim_red2';

VEHLIBStruct.table{n}.Psec_red2=struct;
VEHLIBStruct.table{n}.Psec_red2.condition='true';
VEHLIBStruct.table{n}.Psec_red2.name='Psec_red2';
VEHLIBStruct.table{n}.Psec_red2.longname='Puissance secondaire reducteur 2';
VEHLIBStruct.table{n}.Psec_red2.type='double';
VEHLIBStruct.table{n}.Psec_red2.unit='W';
VEHLIBStruct.table{n}.Psec_red2.precision='%f';
VEHLIBStruct.table{n}.Psec_red2.vector='csec_red2.*wsec_red2';

VEHLIBStruct.table{n}.Pprim_red2=struct;
VEHLIBStruct.table{n}.Pprim_red2.condition='true';
VEHLIBStruct.table{n}.Pprim_red2.name='Pprim_red2';
VEHLIBStruct.table{n}.Pprim_red2.longname='Puissance primaire reducteur 2';
VEHLIBStruct.table{n}.Pprim_red2.type='double';
VEHLIBStruct.table{n}.Pprim_red2.unit='W';
VEHLIBStruct.table{n}.Pprim_red2.precision='%f';
VEHLIBStruct.table{n}.Pprim_red2.vector='cprim_red2.*wprim_red2';

% 'VARIABLES EMBRAYAGE'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES EMBRAYAGE';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.glisst_emb1=struct;
VEHLIBStruct.table{n}.glisst_emb1.condition='true';
VEHLIBStruct.table{n}.glisst_emb1.name='glisst_emb1';
VEHLIBStruct.table{n}.glisst_emb1.longname='Glissement de l embrayage';
VEHLIBStruct.table{n}.glisst_emb1.type='double';
VEHLIBStruct.table{n}.glisst_emb1.unit='';
VEHLIBStruct.table{n}.glisst_emb1.precision='%f';
VEHLIBStruct.table{n}.glisst_emb1.vector='glisst_emb1';

VEHLIBStruct.table{n}.cons_emb1=struct;
VEHLIBStruct.table{n}.cons_emb1.condition='true';
VEHLIBStruct.table{n}.cons_emb1.name='cons_emb1';
VEHLIBStruct.table{n}.cons_emb1.longname='Course de la pedale d embrayage';
VEHLIBStruct.table{n}.cons_emb1.type='double';
VEHLIBStruct.table{n}.cons_emb1.unit='';
VEHLIBStruct.table{n}.cons_emb1.precision='%f';
VEHLIBStruct.table{n}.cons_emb1.vector='cons_emb1';

VEHLIBStruct.table{n}.csec_emb1=struct;
VEHLIBStruct.table{n}.csec_emb1.condition='true';
VEHLIBStruct.table{n}.csec_emb1.name='csec_emb1';
VEHLIBStruct.table{n}.csec_emb1.longname='Couple transmis par l embrayage (secondaire)';
VEHLIBStruct.table{n}.csec_emb1.type='double';
VEHLIBStruct.table{n}.csec_emb1.unit='Nm';
VEHLIBStruct.table{n}.csec_emb1.precision='%f';
VEHLIBStruct.table{n}.csec_emb1.vector='csec_emb1';

VEHLIBStruct.table{n}.wsec_emb1=struct;
VEHLIBStruct.table{n}.wsec_emb1.condition='true';
VEHLIBStruct.table{n}.wsec_emb1.name='wsec_emb1';
VEHLIBStruct.table{n}.wsec_emb1.longname='Vitesse de rotation secondaire embrayage';
VEHLIBStruct.table{n}.wsec_emb1.type='double';
VEHLIBStruct.table{n}.wsec_emb1.unit='rd/s';
VEHLIBStruct.table{n}.wsec_emb1.precision='%f';
VEHLIBStruct.table{n}.wsec_emb1.vector='wsec_emb1';

VEHLIBStruct.table{n}.cprim_emb1=struct;
VEHLIBStruct.table{n}.cprim_emb1.condition='true';
VEHLIBStruct.table{n}.cprim_emb1.name='cprim_emb1';
VEHLIBStruct.table{n}.cprim_emb1.longname='Couple primaire embrayage';
VEHLIBStruct.table{n}.cprim_emb1.type='double';
VEHLIBStruct.table{n}.cprim_emb1.unit='Nm';
VEHLIBStruct.table{n}.cprim_emb1.precision='%f';
VEHLIBStruct.table{n}.cprim_emb1.vector='cprim_emb1';

VEHLIBStruct.table{n}.wprim_emb1=struct;
VEHLIBStruct.table{n}.wprim_emb1.condition='true';
VEHLIBStruct.table{n}.wprim_emb1.name='wprim_emb1';
VEHLIBStruct.table{n}.wprim_emb1.longname='Vitesse de rotation primaire embrayage';
VEHLIBStruct.table{n}.wprim_emb1.type='double';
VEHLIBStruct.table{n}.wprim_emb1.unit='rd/s';
VEHLIBStruct.table{n}.wprim_emb1.precision='%f';
VEHLIBStruct.table{n}.wprim_emb1.vector='wprim_emb1';

VEHLIBStruct.table{n}.Ptrans_emb1=struct;
VEHLIBStruct.table{n}.Ptrans_emb1.condition='true';
VEHLIBStruct.table{n}.Ptrans_emb1.name='Ptrans_emb1';
VEHLIBStruct.table{n}.Ptrans_emb1.longname='Puissance transmise par l embrayage';
VEHLIBStruct.table{n}.Ptrans_emb1.type='double';
VEHLIBStruct.table{n}.Ptrans_emb1.unit='W';
VEHLIBStruct.table{n}.Ptrans_emb1.precision='%f';
VEHLIBStruct.table{n}.Ptrans_emb1.vector='wsec_emb1.*csec_emb1';

VEHLIBStruct.table{n}.cemb1_max=struct;
VEHLIBStruct.table{n}.cemb1_max.condition='true';
VEHLIBStruct.table{n}.cemb1_max.name='cemb1_max';
VEHLIBStruct.table{n}.cemb1_max.longname='Couple maximum transmissible par l embrayage';
VEHLIBStruct.table{n}.cemb1_max.type='double';
VEHLIBStruct.table{n}.cemb1_max.unit='Nm';
VEHLIBStruct.table{n}.cemb1_max.precision='%f';
VEHLIBStruct.table{n}.cemb1_max.vector='cemb1_max';

VEHLIBStruct.table{n}.pprim_emb1=struct;
VEHLIBStruct.table{n}.pprim_emb1.condition='true';
VEHLIBStruct.table{n}.pprim_emb1.name='pprim_emb1';
VEHLIBStruct.table{n}.pprim_emb1.longname='Puissance primaire embrayage';
VEHLIBStruct.table{n}.pprim_emb1.type='double';
VEHLIBStruct.table{n}.pprim_emb1.unit='W';
VEHLIBStruct.table{n}.pprim_emb1.precision='%f';
VEHLIBStruct.table{n}.pprim_emb1.vector='cprim_emb1.*wprim_emb1';

VEHLIBStruct.table{n}.psec_emb1=struct;
VEHLIBStruct.table{n}.psec_emb1.condition='true';
VEHLIBStruct.table{n}.psec_emb1.name='psec_emb1';
VEHLIBStruct.table{n}.psec_emb1.longname='Puissance secondaire embrayage';
VEHLIBStruct.table{n}.psec_emb1.type='double';
VEHLIBStruct.table{n}.psec_emb1.unit='W';
VEHLIBStruct.table{n}.psec_emb1.precision='%f';
VEHLIBStruct.table{n}.psec_emb1.vector='csec_emb1.*wsec_emb1';

% 'VARIABLES EMBRAYAGE 2'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES EMBRAYAGE 2';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.glisst_emb2=struct;
VEHLIBStruct.table{n}.glisst_emb2.condition='true';
VEHLIBStruct.table{n}.glisst_emb2.name='glisst_emb2';
VEHLIBStruct.table{n}.glisst_emb2.longname='Glissement de l embrayage 2';
VEHLIBStruct.table{n}.glisst_emb2.type='double';
VEHLIBStruct.table{n}.glisst_emb2.unit='';
VEHLIBStruct.table{n}.glisst_emb2.precision='%f';
VEHLIBStruct.table{n}.glisst_emb2.vector='glisst_emb2';

VEHLIBStruct.table{n}.cons_emb2=struct;
VEHLIBStruct.table{n}.cons_emb2.condition='true';
VEHLIBStruct.table{n}.cons_emb2.name='cons_emb2';
VEHLIBStruct.table{n}.cons_emb2.longname='Course de la pedale d embrayage 2';
VEHLIBStruct.table{n}.cons_emb2.type='double';
VEHLIBStruct.table{n}.cons_emb2.unit='';
VEHLIBStruct.table{n}.cons_emb2.precision='%f';
VEHLIBStruct.table{n}.cons_emb2.vector='cons_emb2';

VEHLIBStruct.table{n}.csec_emb2=struct;
VEHLIBStruct.table{n}.csec_emb2.condition='true';
VEHLIBStruct.table{n}.csec_emb2.name='csec_emb2';
VEHLIBStruct.table{n}.csec_emb2.longname='Couple transmis par l embrayage 2 (secondaire)';
VEHLIBStruct.table{n}.csec_emb2.type='double';
VEHLIBStruct.table{n}.csec_emb2.unit='Nm';
VEHLIBStruct.table{n}.csec_emb2.precision='%f';
VEHLIBStruct.table{n}.csec_emb2.vector='csec_emb2';

VEHLIBStruct.table{n}.wsec_emb2=struct;
VEHLIBStruct.table{n}.wsec_emb2.condition='true';
VEHLIBStruct.table{n}.wsec_emb2.name='wsec_emb2';
VEHLIBStruct.table{n}.wsec_emb2.longname='Vitesse de rotation secondaire embrayage 2';
VEHLIBStruct.table{n}.wsec_emb2.type='double';
VEHLIBStruct.table{n}.wsec_emb2.unit='rd/sec';
VEHLIBStruct.table{n}.wsec_emb2.precision='%f';
VEHLIBStruct.table{n}.wsec_emb2.vector='wsec_emb2';

VEHLIBStruct.table{n}.cprim_emb2=struct;
VEHLIBStruct.table{n}.cprim_emb2.condition='true';
VEHLIBStruct.table{n}.cprim_emb2.name='cprim_emb2';
VEHLIBStruct.table{n}.cprim_emb2.longname='Couple primaire embrayage 2';
VEHLIBStruct.table{n}.cprim_emb2.type='double';
VEHLIBStruct.table{n}.cprim_emb2.unit='Nm';
VEHLIBStruct.table{n}.cprim_emb2.precision='%f';
VEHLIBStruct.table{n}.cprim_emb2.vector='cprim_emb2';

VEHLIBStruct.table{n}.wprim_emb2=struct;
VEHLIBStruct.table{n}.wprim_emb2.condition='true';
VEHLIBStruct.table{n}.wprim_emb2.name='wprim_emb2';
VEHLIBStruct.table{n}.wprim_emb2.longname='Vitesse de rotation primaire embrayage 2';
VEHLIBStruct.table{n}.wprim_emb2.type='double';
VEHLIBStruct.table{n}.wprim_emb2.unit='rd/sec';
VEHLIBStruct.table{n}.wprim_emb2.precision='%f';
VEHLIBStruct.table{n}.wprim_emb2.vector='wprim_emb2';

VEHLIBStruct.table{n}.Ptrans_emb2=struct;
VEHLIBStruct.table{n}.Ptrans_emb2.condition='true';
VEHLIBStruct.table{n}.Ptrans_emb2.name='Ptrans_emb2';
VEHLIBStruct.table{n}.Ptrans_emb2.longname='Puissance transmise par l embrayage 2';
VEHLIBStruct.table{n}.Ptrans_emb2.type='double';
VEHLIBStruct.table{n}.Ptrans_emb2.unit='W';
VEHLIBStruct.table{n}.Ptrans_emb2.precision='%f';
VEHLIBStruct.table{n}.Ptrans_emb2.vector='wsec_emb2.*csec_emb2';

VEHLIBStruct.table{n}.cemb2_max=struct;
VEHLIBStruct.table{n}.cemb2_max.condition='true';
VEHLIBStruct.table{n}.cemb2_max.name='cemb2_max';
VEHLIBStruct.table{n}.cemb2_max.longname='Couple maximum transmissible par l embrayage 2';
VEHLIBStruct.table{n}.cemb2_max.type='double';
VEHLIBStruct.table{n}.cemb2_max.unit='Nm';
VEHLIBStruct.table{n}.cemb2_max.precision='%f';
VEHLIBStruct.table{n}.cemb2_max.vector='cemb2_max';

% 'VARIABLES VEHICULE'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES VEHICULE';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.masse=struct;
VEHLIBStruct.table{n}.masse.condition='true';
VEHLIBStruct.table{n}.masse.name='masse';
VEHLIBStruct.table{n}.masse.longname='Masse du vehicule';
VEHLIBStruct.table{n}.masse.type='double';
VEHLIBStruct.table{n}.masse.unit='kg';
VEHLIBStruct.table{n}.masse.precision='%f';
VEHLIBStruct.table{n}.masse.vector='masse';

VEHLIBStruct.table{n}.faero=struct;
VEHLIBStruct.table{n}.faero.condition='VD.VEHI.ntypveh==1 || VD.VEHI.ntypveh==3 || VD.VEHI.ntypveh==4';
VEHLIBStruct.table{n}.faero.name='faero';
VEHLIBStruct.table{n}.faero.longname='Force aerodynamique';
VEHLIBStruct.table{n}.faero.type='double';
VEHLIBStruct.table{n}.faero.unit='N';
VEHLIBStruct.table{n}.faero.precision='%f';
VEHLIBStruct.table{n}.faero.vector='faero';

VEHLIBStruct.table{n}.froul=struct;
VEHLIBStruct.table{n}.froul.condition='VD.VEHI.ntypveh==1 || VD.VEHI.ntypveh==3 || VD.VEHI.ntypveh==4';
VEHLIBStruct.table{n}.froul.name='froul';
VEHLIBStruct.table{n}.froul.longname='Force de roulement';
VEHLIBStruct.table{n}.froul.type='double';
VEHLIBStruct.table{n}.froul.unit='N';
VEHLIBStruct.table{n}.froul.precision='%f';
VEHLIBStruct.table{n}.froul.vector='froul';

VEHLIBStruct.table{n}.fpente=struct;
VEHLIBStruct.table{n}.fpente.condition='true';
VEHLIBStruct.table{n}.fpente.name='fpente';
VEHLIBStruct.table{n}.fpente.longname='Force de pente';
VEHLIBStruct.table{n}.fpente.type='double';
VEHLIBStruct.table{n}.fpente.unit='N';
VEHLIBStruct.table{n}.fpente.precision='%f';
VEHLIBStruct.table{n}.fpente.vector='fpente';

VEHLIBStruct.table{n}.Ffrein=struct;
VEHLIBStruct.table{n}.Ffrein.condition='true';
VEHLIBStruct.table{n}.Ffrein.name='Ffrein';
VEHLIBStruct.table{n}.Ffrein.longname='Force de freinage mecanique';
VEHLIBStruct.table{n}.Ffrein.type='double';
VEHLIBStruct.table{n}.Ffrein.unit='N';
VEHLIBStruct.table{n}.Ffrein.precision='%f';
VEHLIBStruct.table{n}.Ffrein.vector='cfrein_meca/VD.VEHI.Rpneu';

VEHLIBStruct.table{n}.P_roul=struct;
VEHLIBStruct.table{n}.P_roul.condition='VD.VEHI.ntypveh==1 || VD.VEHI.ntypveh==3 || VD.VEHI.ntypveh==4';
VEHLIBStruct.table{n}.P_roul.name='P_roul';
VEHLIBStruct.table{n}.P_roul.longname='Puissance de roulement';
VEHLIBStruct.table{n}.P_roul.type='double';
VEHLIBStruct.table{n}.P_roul.unit='W';
VEHLIBStruct.table{n}.P_roul.precision='%f';
VEHLIBStruct.table{n}.P_roul.vector='froul.*vit';

VEHLIBStruct.table{n}.P_aero=struct;
VEHLIBStruct.table{n}.P_aero.condition='VD.VEHI.ntypveh==1 || VD.VEHI.ntypveh==3 || VD.VEHI.ntypveh==4';
VEHLIBStruct.table{n}.P_aero.name='P_aero';
VEHLIBStruct.table{n}.P_aero.longname='Puissance aerodynamique';
VEHLIBStruct.table{n}.P_aero.type='double';
VEHLIBStruct.table{n}.P_aero.unit='W';
VEHLIBStruct.table{n}.P_aero.precision='%f';
VEHLIBStruct.table{n}.P_aero.vector='faero.*vit';

VEHLIBStruct.table{n}.P_pente=struct;
VEHLIBStruct.table{n}.P_pente.condition='true';
VEHLIBStruct.table{n}.P_pente.name='P_pente';
VEHLIBStruct.table{n}.P_pente.longname='Puissance de pente';
VEHLIBStruct.table{n}.P_pente.type='double';
VEHLIBStruct.table{n}.P_pente.unit='W';
VEHLIBStruct.table{n}.P_pente.precision='%f';
VEHLIBStruct.table{n}.P_pente.vector='fpente.*vit';

VEHLIBStruct.table{n}.P_inertie=struct;
VEHLIBStruct.table{n}.P_inertie.condition='true';
VEHLIBStruct.table{n}.P_inertie.name='P_inertie';
VEHLIBStruct.table{n}.P_inertie.longname='Puissance d inertie';
VEHLIBStruct.table{n}.P_inertie.type='double';
VEHLIBStruct.table{n}.P_inertie.unit='W';
VEHLIBStruct.table{n}.P_inertie.precision='%f';
VEHLIBStruct.table{n}.P_inertie.vector='cinertie/VD.VEHI.Rpneu.*vit';

VEHLIBStruct.table{n}.P_frein=struct;
VEHLIBStruct.table{n}.P_frein.condition='VD.VEHI.ntypveh==3';
VEHLIBStruct.table{n}.P_frein.name='P_frein';
VEHLIBStruct.table{n}.P_frein.longname='Puissance de freinage (meca+el)';
VEHLIBStruct.table{n}.P_frein.type='double';
VEHLIBStruct.table{n}.P_frein.unit='W';
VEHLIBStruct.table{n}.P_frein.precision='%f';
VEHLIBStruct.table{n}.P_frein.vector='(cfrein/VD.VEHI.Rpneu).*vit';

VEHLIBStruct.table{n}.Fres_avt=struct;
VEHLIBStruct.table{n}.Fres_avt.condition={ 'VD.VEHI.ntypveh==2' 'VD.VEHI.ntypveh==1 || VD.VEHI.ntypveh==3 || VD.VEHI.ntypveh==4'};
VEHLIBStruct.table{n}.Fres_avt.name='Fres_avt';
VEHLIBStruct.table{n}.Fres_avt.longname='Force de resistance a l avancement (hors pente)';
VEHLIBStruct.table{n}.Fres_avt.type='double';
VEHLIBStruct.table{n}.Fres_avt.unit={'N' 'N'};
VEHLIBStruct.table{n}.Fres_avt.precision='%f';
VEHLIBStruct.table{n}.Fres_avt.vector={'force1+force2+force3' 'froul+faero'};

VEHLIBStruct.table{n}.Pres_avt=struct;
VEHLIBStruct.table{n}.Pres_avt.condition={'VD.VEHI.ntypveh==2' 'VD.VEHI.ntypveh==1 || VD.VEHI.ntypveh==3 || VD.VEHI.ntypveh==4'};
VEHLIBStruct.table{n}.Pres_avt.name='Pres_avt';
VEHLIBStruct.table{n}.Pres_avt.longname='Puissance de resistance a l avancement (hors pente)';
VEHLIBStruct.table{n}.Pres_avt.type='double';
VEHLIBStruct.table{n}.Pres_avt.unit={'W' 'W'};
VEHLIBStruct.table{n}.Pres_avt.precision='%f';
VEHLIBStruct.table{n}.Pres_avt.vector={'(force1+force2+force3).*vit' '(froul+faero).*vit'};

VEHLIBStruct.table{n}.F_vehi=struct;
VEHLIBStruct.table{n}.F_vehi.condition={'VD.VEHI.ntypveh==2' 'VD.VEHI.ntypveh==1 || VD.VEHI.ntypveh==3 || VD.VEHI.ntypveh==4'};
VEHLIBStruct.table{n}.F_vehi.name='F_vehi';
VEHLIBStruct.table{n}.F_vehi.longname='Force Vehicule';
VEHLIBStruct.table{n}.F_vehi.type='double';
VEHLIBStruct.table{n}.F_vehi.unit={'N' 'N'};
VEHLIBStruct.table{n}.F_vehi.precision='%f';
VEHLIBStruct.table{n}.F_vehi.vector={'force1+force2+force3+fpente+cinertie/VD.VEHI.Rpneu' 'froul+faero+fpente+cinertie/VD.VEHI.Rpneu'};

VEHLIBStruct.table{n}.P_vehi=struct;
VEHLIBStruct.table{n}.P_vehi.condition={'VD.VEHI.ntypveh==2' 'VD.VEHI.ntypveh==1'};
VEHLIBStruct.table{n}.P_vehi.name='P_vehi';
VEHLIBStruct.table{n}.P_vehi.longname='Puissance Vehicule';
VEHLIBStruct.table{n}.P_vehi.type='double';
VEHLIBStruct.table{n}.P_vehi.unit={'W' 'W'};
VEHLIBStruct.table{n}.P_vehi.precision='%f';
VEHLIBStruct.table{n}.P_vehi.vector={'(force1+force2+force3+fpente+cinertie/VD.VEHI.Rpneu).*vit' '(froul+faero+fpente+cinertie/VD.VEHI.Rpneu).*vit'};

VEHLIBStruct.table{n}.E_vehi=struct;
VEHLIBStruct.table{n}.E_vehi.condition='true';
VEHLIBStruct.table{n}.E_vehi.name='E_vehi';
VEHLIBStruct.table{n}.E_vehi.longname='Energie Vehicule';
VEHLIBStruct.table{n}.E_vehi.type='double';
VEHLIBStruct.table{n}.E_vehi.unit='Ws';
VEHLIBStruct.table{n}.E_vehi.precision='%f';
VEHLIBStruct.table{n}.E_vehi.vector='cumtrapz(tsim,pveh)';

VEHLIBStruct.table{n}.wroue=struct;
VEHLIBStruct.table{n}.wroue.condition='true';
VEHLIBStruct.table{n}.wroue.name='wroue';
VEHLIBStruct.table{n}.wroue.longname='Vitesse de rotation des roues';
VEHLIBStruct.table{n}.wroue.type='double';
VEHLIBStruct.table{n}.wroue.unit='rd/s';
VEHLIBStruct.table{n}.wroue.precision='%f';
VEHLIBStruct.table{n}.wroue.vector='wroue';

VEHLIBStruct.table{n}.croue=struct;
VEHLIBStruct.table{n}.croue.condition='true';
VEHLIBStruct.table{n}.croue.name='croue';
VEHLIBStruct.table{n}.croue.longname='Couple des roues';
VEHLIBStruct.table{n}.croue.type='double';
VEHLIBStruct.table{n}.croue.unit='N.m';
VEHLIBStruct.table{n}.croue.precision='%f';
VEHLIBStruct.table{n}.croue.vector='croue';

VEHLIBStruct.table{n}.P_frein_meca=struct;
VEHLIBStruct.table{n}.P_frein_meca.condition='true';
VEHLIBStruct.table{n}.P_frein_meca.name='P_frein_meca';
VEHLIBStruct.table{n}.P_frein_meca.longname='Puissance de freinage mecanique';
VEHLIBStruct.table{n}.P_frein_meca.type='double';
VEHLIBStruct.table{n}.P_frein_meca.unit='W';
VEHLIBStruct.table{n}.P_frein_meca.precision='%f';
VEHLIBStruct.table{n}.P_frein_meca.vector='cfrein_meca.*wroue';

VEHLIBStruct.table{n}.cfrein_meca=struct;
VEHLIBStruct.table{n}.cfrein_meca.condition='true';
VEHLIBStruct.table{n}.cfrein_meca.name='cfrein_meca';
VEHLIBStruct.table{n}.cfrein_meca.longname='Couple de freinage mecanique';
VEHLIBStruct.table{n}.cfrein_meca.type='double';
VEHLIBStruct.table{n}.cfrein_meca.unit='Nm';
VEHLIBStruct.table{n}.cfrein_meca.precision='%f';
VEHLIBStruct.table{n}.cfrein_meca.vector='cfrein_meca';

VEHLIBStruct.table{n}.Ffrein_av=struct;
VEHLIBStruct.table{n}.Ffrein_av.condition='isfield(vehlib,''BATT'') || isfield(vehlib,''SC'')';
VEHLIBStruct.table{n}.Ffrein_av.name='frein_av';
VEHLIBStruct.table{n}.Ffrein_av.longname='Couple de freinage avant (el+meca)';
VEHLIBStruct.table{n}.Ffrein_av.type='double';
VEHLIBStruct.table{n}.Ffrein_av.unit='Nm';
VEHLIBStruct.table{n}.Ffrein_av.precision='%f';
VEHLIBStruct.table{n}.Ffrein_av.vector='frein_av*VD.VEHI.Rpneu';

VEHLIBStruct.table{n}.Ffrein_ar=struct;
VEHLIBStruct.table{n}.Ffrein_ar.condition='isfield(vehlib,''BATT'') || isfield(vehlib,''SC'')';
VEHLIBStruct.table{n}.Ffrein_ar.name='frein_ar';
VEHLIBStruct.table{n}.Ffrein_ar.longname='Couple de freinage arriere';
VEHLIBStruct.table{n}.Ffrein_ar.type='double';
VEHLIBStruct.table{n}.Ffrein_ar.unit='Nm';
VEHLIBStruct.table{n}.Ffrein_ar.precision='%f';
VEHLIBStruct.table{n}.Ffrein_ar.vector='frein_ar*VD.VEHI.Rpneu';

% 'VARIABLES BOITE DE VITESSE'

n=length(VEHLIBStruct.table)+1;
% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES BOITE DE VITESSE';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.rappvit_bv=struct;
VEHLIBStruct.table{n}.rappvit_bv.condition='true';
VEHLIBStruct.table{n}.rappvit_bv.name='rappvit_bv';
VEHLIBStruct.table{n}.rappvit_bv.longname='Rapport de boite engage';
VEHLIBStruct.table{n}.rappvit_bv.type='double';
VEHLIBStruct.table{n}.rappvit_bv.unit='';
VEHLIBStruct.table{n}.rappvit_bv.precision='%f';
VEHLIBStruct.table{n}.rappvit_bv.vector='rappvit_bv';

VEHLIBStruct.table{n}.rapprend_bv=struct;
VEHLIBStruct.table{n}.rapprend_bv.condition='true';
VEHLIBStruct.table{n}.rapprend_bv.name='rapprend_bv';
VEHLIBStruct.table{n}.rapprend_bv.longname='Rendement de la boite engage';
VEHLIBStruct.table{n}.rapprend_bv.type='double';
VEHLIBStruct.table{n}.rapprend_bv.unit='';
VEHLIBStruct.table{n}.rapprend_bv.precision='%f';
VEHLIBStruct.table{n}.rapprend_bv.vector='rapprend_bv';

VEHLIBStruct.table{n}.rappred_bv=struct;
VEHLIBStruct.table{n}.rappred_bv.condition='true';
VEHLIBStruct.table{n}.rappred_bv.name='rappred_bv';
VEHLIBStruct.table{n}.rappred_bv.longname='Rapport de reduction de la boite engage';
VEHLIBStruct.table{n}.rappred_bv.type='double';
VEHLIBStruct.table{n}.rappred_bv.unit='';
VEHLIBStruct.table{n}.rappred_bv.precision='%f';
VEHLIBStruct.table{n}.rappred_bv.vector='rappred_bv';

VEHLIBStruct.table{n}.cprim_bv=struct;
VEHLIBStruct.table{n}.cprim_bv.condition='true';
VEHLIBStruct.table{n}.cprim_bv.name='cprim_bv';
VEHLIBStruct.table{n}.cprim_bv.longname='Couple primaire BV';
VEHLIBStruct.table{n}.cprim_bv.type='double';
VEHLIBStruct.table{n}.cprim_bv.unit='Nm';
VEHLIBStruct.table{n}.cprim_bv.precision='%f';
VEHLIBStruct.table{n}.cprim_bv.vector='cprim_bv';

VEHLIBStruct.table{n}.wprim_bv=struct;
VEHLIBStruct.table{n}.wprim_bv.condition='true';
VEHLIBStruct.table{n}.wprim_bv.name='wprim_bv';
VEHLIBStruct.table{n}.wprim_bv.longname='Vitesse primaire BV';
VEHLIBStruct.table{n}.wprim_bv.type='double';
VEHLIBStruct.table{n}.wprim_bv.unit='rd/s';
VEHLIBStruct.table{n}.wprim_bv.precision='%f';
VEHLIBStruct.table{n}.wprim_bv.vector='wprim_bv';

VEHLIBStruct.table{n}.csec_bv=struct;
VEHLIBStruct.table{n}.csec_bv.condition='true';
VEHLIBStruct.table{n}.csec_bv.name='csec_bv';
VEHLIBStruct.table{n}.csec_bv.longname='Couple secondaire BV';
VEHLIBStruct.table{n}.csec_bv.type='double';
VEHLIBStruct.table{n}.csec_bv.unit='Nm';
VEHLIBStruct.table{n}.csec_bv.precision='%f';
VEHLIBStruct.table{n}.csec_bv.vector='csec_bv';

VEHLIBStruct.table{n}.wsec_bv=struct;
VEHLIBStruct.table{n}.wsec_bv.condition='true';
VEHLIBStruct.table{n}.wsec_bv.name='wsec_bv';
VEHLIBStruct.table{n}.wsec_bv.longname='Vitesse secondaire BV';
VEHLIBStruct.table{n}.wsec_bv.type='double';
VEHLIBStruct.table{n}.wsec_bv.unit='rd/s';
VEHLIBStruct.table{n}.wsec_bv.precision='%f';
VEHLIBStruct.table{n}.wsec_bv.vector='wsec_bv';

VEHLIBStruct.table{n}.pprim_bv=struct;
VEHLIBStruct.table{n}.pprim_bv.condition='true';
VEHLIBStruct.table{n}.pprim_bv.name='pprim_bv';
VEHLIBStruct.table{n}.pprim_bv.longname='Puissance primaire BV';
VEHLIBStruct.table{n}.pprim_bv.type='double';
VEHLIBStruct.table{n}.pprim_bv.unit='W';
VEHLIBStruct.table{n}.pprim_bv.precision='%f';
VEHLIBStruct.table{n}.pprim_bv.vector='cprim_bv.*wprim_bv';

VEHLIBStruct.table{n}.psec_bv=struct;
VEHLIBStruct.table{n}.psec_bv.condition='true';
VEHLIBStruct.table{n}.psec_bv.name='psec_bv';
VEHLIBStruct.table{n}.psec_bv.longname='Puissance secondaire BV';
VEHLIBStruct.table{n}.psec_bv.type='double';
VEHLIBStruct.table{n}.psec_bv.unit='W';
VEHLIBStruct.table{n}.psec_bv.precision='%f';
VEHLIBStruct.table{n}.psec_bv.vector='csec_bv.*wsec_bv';

% 'VARIABLES MOTEUR ELECTRIQUE'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES MOTEUR ELECTRIQUE';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.cacm1=struct;
VEHLIBStruct.table{n}.cacm1.condition='true';
VEHLIBStruct.table{n}.cacm1.name='cacm1';
VEHLIBStruct.table{n}.cacm1.longname='Couple sur l arbre du moteur de traction';
VEHLIBStruct.table{n}.cacm1.type='double';
VEHLIBStruct.table{n}.cacm1.unit='Nm';
VEHLIBStruct.table{n}.cacm1.precision='%f';
VEHLIBStruct.table{n}.cacm1.vector='cacm1';

VEHLIBStruct.table{n}.P_meca_acm1=struct;
VEHLIBStruct.table{n}.P_meca_acm1.condition='true';
VEHLIBStruct.table{n}.P_meca_acm1.name='P_meca_acm1';
VEHLIBStruct.table{n}.P_meca_acm1.longname='Puissance sur l arbre du moteur de traction';
VEHLIBStruct.table{n}.P_meca_acm1.type='double';
VEHLIBStruct.table{n}.P_meca_acm1.unit='W';
VEHLIBStruct.table{n}.P_meca_acm1.precision='%f';
VEHLIBStruct.table{n}.P_meca_acm1.vector='cacm1.*wacm1';

VEHLIBStruct.table{n}.E_meca_acm1=struct;
VEHLIBStruct.table{n}.E_meca_acm1.condition='true';
VEHLIBStruct.table{n}.E_meca_acm1.name='E_meca_acm1';
VEHLIBStruct.table{n}.E_meca_acm1.longname='Energie sur l arbre du moteur de traction';
VEHLIBStruct.table{n}.E_meca_acm1.type='double';
VEHLIBStruct.table{n}.E_meca_acm1.unit='Ws';
VEHLIBStruct.table{n}.E_meca_acm1.precision='%f';
VEHLIBStruct.table{n}.E_meca_acm1.vector='cumtrapz(tsim,cacm1.*wacm1)';

VEHLIBStruct.table{n}.qacm1=struct;
VEHLIBStruct.table{n}.qacm1.condition='true';
VEHLIBStruct.table{n}.qacm1.name='qacm1';
VEHLIBStruct.table{n}.qacm1.longname='Pertes du moteur de traction';
VEHLIBStruct.table{n}.qacm1.type='double';
VEHLIBStruct.table{n}.qacm1.unit='W';
VEHLIBStruct.table{n}.qacm1.precision='%f';
VEHLIBStruct.table{n}.qacm1.vector='qacm1';

VEHLIBStruct.table{n}.wacm1=struct;
VEHLIBStruct.table{n}.wacm1.condition='true';
VEHLIBStruct.table{n}.wacm1.name='wacm1';
VEHLIBStruct.table{n}.wacm1.longname='Vitesse de rotation du moteur de traction';
VEHLIBStruct.table{n}.wacm1.type='double';
VEHLIBStruct.table{n}.wacm1.unit='rd/s';
VEHLIBStruct.table{n}.wacm1.precision='%f';
VEHLIBStruct.table{n}.wacm1.vector='wacm1';

VEHLIBStruct.table{n}.dwacm1=struct;
VEHLIBStruct.table{n}.dwacm1.condition='true';
VEHLIBStruct.table{n}.dwacm1.name='dwacm1';
VEHLIBStruct.table{n}.dwacm1.longname='derivee de la vitesse de rotation du moteur de traction';
VEHLIBStruct.table{n}.dwacm1.type='double';
VEHLIBStruct.table{n}.dwacm1.unit='rd/s*s';
VEHLIBStruct.table{n}.dwacm1.precision='%f';
VEHLIBStruct.table{n}.dwacm1.vector='dwacm1';

VEHLIBStruct.table{n}.iacm1=struct;
VEHLIBStruct.table{n}.iacm1.condition='true';
VEHLIBStruct.table{n}.iacm1.name='iacm1';
VEHLIBStruct.table{n}.iacm1.longname='Courant du moteur de traction';
VEHLIBStruct.table{n}.iacm1.type='double';
VEHLIBStruct.table{n}.iacm1.unit='A';
VEHLIBStruct.table{n}.iacm1.precision='%f';
VEHLIBStruct.table{n}.iacm1.vector='iacm1';

VEHLIBStruct.table{n}.P_elec_acm1=struct;
VEHLIBStruct.table{n}.P_elec_acm1.condition='true';
VEHLIBStruct.table{n}.P_elec_acm1.name='P_elec_acm1';
VEHLIBStruct.table{n}.P_elec_acm1.longname='Puissance electrique du moteur de traction';
VEHLIBStruct.table{n}.P_elec_acm1.type='double';
VEHLIBStruct.table{n}.P_elec_acm1.unit='W';
VEHLIBStruct.table{n}.P_elec_acm1.precision='%f';
VEHLIBStruct.table{n}.P_elec_acm1.vector='iacm1.*uacm1';

VEHLIBStruct.table{n}.E_elec_acm1=struct;
VEHLIBStruct.table{n}.E_elec_acm1.condition='true';
VEHLIBStruct.table{n}.E_elec_acm1.name='E_elec_acm1';
VEHLIBStruct.table{n}.E_elec_acm1.longname='Energie electrique du moteur de traction';
VEHLIBStruct.table{n}.E_elec_acm1.type='double';
VEHLIBStruct.table{n}.E_elec_acm1.unit='Ws';
VEHLIBStruct.table{n}.E_elec_acm1.precision='%f';
VEHLIBStruct.table{n}.E_elec_acm1.vector='cumtrapz(tsim,iacm1.*uacm1)';

VEHLIBStruct.table{n}.pind1=struct;
VEHLIBStruct.table{n}.pind1.condition='VD.ACM1.ntypmg==1';
VEHLIBStruct.table{n}.pind1.name='pind1';
VEHLIBStruct.table{n}.pind1.longname='Puissance electrique induit du moteur de traction';
VEHLIBStruct.table{n}.pind1.type='double';
VEHLIBStruct.table{n}.pind1.unit='W';
VEHLIBStruct.table{n}.pind1.precision='%f';
VEHLIBStruct.table{n}.pind1.vector='pind1';

VEHLIBStruct.table{n}.img1=struct;
VEHLIBStruct.table{n}.img1.condition='VD.ACM1.ntypmg==1';
VEHLIBStruct.table{n}.img1.name='img1';
VEHLIBStruct.table{n}.img1.longname='Courant induit du moteur de traction';
VEHLIBStruct.table{n}.img1.type='double';
VEHLIBStruct.table{n}.img1.unit='A';
VEHLIBStruct.table{n}.img1.precision='%f';
VEHLIBStruct.table{n}.img1.vector='img1';

VEHLIBStruct.table{n}.uind1=struct;
VEHLIBStruct.table{n}.uind1.condition='VD.ACM1.ntypmg==1';
VEHLIBStruct.table{n}.uind1.name='uind1';
VEHLIBStruct.table{n}.uind1.longname='Tension induit stator du moteur de traction';
VEHLIBStruct.table{n}.uind1.type='double';
VEHLIBStruct.table{n}.uind1.unit='V';
VEHLIBStruct.table{n}.uind1.precision='%f';
VEHLIBStruct.table{n}.uind1.vector='uind1';

VEHLIBStruct.table{n}.iex1=struct;
VEHLIBStruct.table{n}.iex1.condition='VD.ACM1.ntypmg==1 | VD.ACM1.ntypmg==3';
VEHLIBStruct.table{n}.iex1.name='iex1';
VEHLIBStruct.table{n}.iex1.longname='Courant d''excitation du moteur de traction';
VEHLIBStruct.table{n}.iex1.type='double';
VEHLIBStruct.table{n}.iex1.unit='A';
VEHLIBStruct.table{n}.iex1.precision='%f';
VEHLIBStruct.table{n}.iex1.vector='iex1';

VEHLIBStruct.table{n}.pstat1=struct;
VEHLIBStruct.table{n}.pstat1.condition='VD.ACM1.ntypmg==2 | VD.ACM1.ntypmg==3';
VEHLIBStruct.table{n}.pstat1.name='pstat1';
VEHLIBStruct.table{n}.pstat1.longname='Puissance electrique stator du moteur de traction';
VEHLIBStruct.table{n}.pstat1.type='double';
VEHLIBStruct.table{n}.pstat1.unit='W';
VEHLIBStruct.table{n}.pstat1.precision='%f';
VEHLIBStruct.table{n}.pstat1.vector='pstat1';

VEHLIBStruct.table{n}.istat1=struct;
VEHLIBStruct.table{n}.istat1.condition='VD.ACM1.ntypmg==2 | VD.ACM1.ntypmg==3';
VEHLIBStruct.table{n}.istat1.name='istat1';
VEHLIBStruct.table{n}.istat1.longname='Courant enroulement stator du moteur de traction';
VEHLIBStruct.table{n}.istat1.type='double';
VEHLIBStruct.table{n}.istat1.unit='A';
VEHLIBStruct.table{n}.istat1.precision='%f';
VEHLIBStruct.table{n}.istat1.vector='istat1';

VEHLIBStruct.table{n}.vstat1=struct;
VEHLIBStruct.table{n}.vstat1.condition='VD.ACM1.ntypmg==2 | VD.ACM1.ntypmg==3';
VEHLIBStruct.table{n}.vstat1.name='vstat1';
VEHLIBStruct.table{n}.vstat1.longname='Tension enroulement stator du moteur de traction';
VEHLIBStruct.table{n}.vstat1.type='double';
VEHLIBStruct.table{n}.vstat1.unit='V';
VEHLIBStruct.table{n}.vstat1.precision='%f';
VEHLIBStruct.table{n}.vstat1.vector='vstat1';

VEHLIBStruct.table{n}.cosfi1=struct;
VEHLIBStruct.table{n}.cosfi1.condition='VD.ACM1.ntypmg==2 | VD.ACM1.ntypmg==3';
VEHLIBStruct.table{n}.cosfi1.name='cosfi1';
VEHLIBStruct.table{n}.cosfi1.longname='Facteur de puissance du moteur de traction';
VEHLIBStruct.table{n}.cosfi1.type='double';
VEHLIBStruct.table{n}.cosfi1.unit='';
VEHLIBStruct.table{n}.cosfi1.precision='%f';
VEHLIBStruct.table{n}.cosfi1.vector='cosfi1';

VEHLIBStruct.table{n}.rcyc1=struct;
VEHLIBStruct.table{n}.rcyc1.condition='VD.ACM1.ntypmg==2 | VD.ACM1.ntypmg==3';
VEHLIBStruct.table{n}.rcyc1.name='rcyc1';
VEHLIBStruct.table{n}.rcyc1.longname='Rapport cyclique du moteur de traction';
VEHLIBStruct.table{n}.rcyc1.type='double';
VEHLIBStruct.table{n}.rcyc1.unit='';
VEHLIBStruct.table{n}.rcyc1.precision='%f';
VEHLIBStruct.table{n}.rcyc1.vector='rcyc1';

VEHLIBStruct.table{n}.Gliss1=struct;
VEHLIBStruct.table{n}.Gliss1.condition='VD.ACM1.ntypmg==2';
VEHLIBStruct.table{n}.Gliss1.name='Gliss1';
VEHLIBStruct.table{n}.Gliss1.longname='Glissement du moteur de traction';
VEHLIBStruct.table{n}.Gliss1.type='double';
VEHLIBStruct.table{n}.Gliss1.unit='';
VEHLIBStruct.table{n}.Gliss1.precision='%f';
VEHLIBStruct.table{n}.Gliss1.vector='Gliss1';

% 'VARIABLES GENERATEUR'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES GENERATEUR';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.cacm2=struct;
VEHLIBStruct.table{n}.cacm2.condition='true';
VEHLIBStruct.table{n}.cacm2.name='cacm2';
VEHLIBStruct.table{n}.cacm2.longname='Couple sur l arbre du generateur';
VEHLIBStruct.table{n}.cacm2.type='double';
VEHLIBStruct.table{n}.cacm2.unit='Nm';
VEHLIBStruct.table{n}.cacm2.precision='%f';
VEHLIBStruct.table{n}.cacm2.vector='cacm2';

VEHLIBStruct.table{n}.wacm2=struct;
VEHLIBStruct.table{n}.wacm2.condition='true';
VEHLIBStruct.table{n}.wacm2.name='wacm2';
VEHLIBStruct.table{n}.wacm2.longname='Vitesse de rotation du generateur';
VEHLIBStruct.table{n}.wacm2.type='double';
VEHLIBStruct.table{n}.wacm2.unit='rd/sec';
VEHLIBStruct.table{n}.wacm2.precision='%f';
VEHLIBStruct.table{n}.wacm2.vector='wacm2';

VEHLIBStruct.table{n}.dwacm2=struct;
VEHLIBStruct.table{n}.dwacm2.condition='true';
VEHLIBStruct.table{n}.dwacm2.name='dwacm2';
VEHLIBStruct.table{n}.dwacm2.longname='Derivee de la vitesse de rotation du generateur';
VEHLIBStruct.table{n}.dwacm2.type='double';
VEHLIBStruct.table{n}.dwacm2.unit='rd/sec²';
VEHLIBStruct.table{n}.dwacm2.precision='%f';
VEHLIBStruct.table{n}.dwacm2.vector='dwacm2';

VEHLIBStruct.table{n}.P_meca_acm2=struct;
VEHLIBStruct.table{n}.P_meca_acm2.condition='true';
VEHLIBStruct.table{n}.P_meca_acm2.name='P_meca_acm2';
VEHLIBStruct.table{n}.P_meca_acm2.longname='Puissance mecanique du generateur';
VEHLIBStruct.table{n}.P_meca_acm2.type='double';
VEHLIBStruct.table{n}.P_meca_acm2.unit='W';
VEHLIBStruct.table{n}.P_meca_acm2.precision='%f';
VEHLIBStruct.table{n}.P_meca_acm2.vector='cacm2.*wacm2';

VEHLIBStruct.table{n}.qacm2=struct;
VEHLIBStruct.table{n}.qacm2.condition='true';
VEHLIBStruct.table{n}.qacm2.name='qacm2';
VEHLIBStruct.table{n}.qacm2.longname='Pertes du generateur';
VEHLIBStruct.table{n}.qacm2.type='double';
VEHLIBStruct.table{n}.qacm2.unit='W';
VEHLIBStruct.table{n}.qacm2.precision='%f';
VEHLIBStruct.table{n}.qacm2.vector='qacm2';

VEHLIBStruct.table{n}.iacm2=struct;
VEHLIBStruct.table{n}.iacm2.condition='true';
VEHLIBStruct.table{n}.iacm2.name='iacm2';
VEHLIBStruct.table{n}.iacm2.longname='Courant generateur';
VEHLIBStruct.table{n}.iacm2.type='double';
VEHLIBStruct.table{n}.iacm2.unit='A';
VEHLIBStruct.table{n}.iacm2.precision='%f';
VEHLIBStruct.table{n}.iacm2.vector='iacm2';

VEHLIBStruct.table{n}.P_elec_acm2=struct;
VEHLIBStruct.table{n}.P_elec_acm2.condition='true';
VEHLIBStruct.table{n}.P_elec_acm2.name='P_elec_acm2';
VEHLIBStruct.table{n}.P_elec_acm2.longname='Puissance electrique generateur';
VEHLIBStruct.table{n}.P_elec_acm2.type='double';
VEHLIBStruct.table{n}.P_elec_acm2.unit='W';
VEHLIBStruct.table{n}.P_elec_acm2.precision='%f';
VEHLIBStruct.table{n}.P_elec_acm2.vector='iacm2.*uacm2';

% 'VARIABLES COUPLEUR'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES COUPLEUR';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables

VEHLIBStruct.table{n}.cprim1_cpl=struct;
VEHLIBStruct.table{n}.cprim1_cpl.condition='true';
VEHLIBStruct.table{n}.cprim1_cpl.name='cprim1_cpl';
VEHLIBStruct.table{n}.cprim1_cpl.longname='Couple au primaire 1 du coupleur';
VEHLIBStruct.table{n}.cprim1_cpl.type='double';
VEHLIBStruct.table{n}.cprim1_cpl.unit='Nm';
VEHLIBStruct.table{n}.cprim1_cpl.precision='%f';
VEHLIBStruct.table{n}.cprim1_cpl.vector='cprim1_cpl';

VEHLIBStruct.table{n}.wprim1_cpl=struct;
VEHLIBStruct.table{n}.wprim1_cpl.condition='true';
VEHLIBStruct.table{n}.wprim1_cpl.name='wprim1_cpl';
VEHLIBStruct.table{n}.wprim1_cpl.longname='Vitesse au primaire 1 du coupleur';
VEHLIBStruct.table{n}.wprim1_cpl.type='double';
VEHLIBStruct.table{n}.wprim1_cpl.unit='rd/s';
VEHLIBStruct.table{n}.wprim1_cpl.precision='%f';
VEHLIBStruct.table{n}.wprim1_cpl.vector='wprim1_cpl';

VEHLIBStruct.table{n}.cprim2_cpl=struct;
VEHLIBStruct.table{n}.cprim2_cpl.condition='true';
VEHLIBStruct.table{n}.cprim2_cpl.name='cprim2_cpl';
VEHLIBStruct.table{n}.cprim2_cpl.longname='Couple au primaire 2 du coupleur';
VEHLIBStruct.table{n}.cprim2_cpl.type='double';
VEHLIBStruct.table{n}.cprim2_cpl.unit='Nm';
VEHLIBStruct.table{n}.cprim2_cpl.precision='%f';
VEHLIBStruct.table{n}.cprim2_cpl.vector='cprim2_cpl';

VEHLIBStruct.table{n}.wprim2_cpl=struct;
VEHLIBStruct.table{n}.wprim2_cpl.condition='true';
VEHLIBStruct.table{n}.wprim2_cpl.name='wprim2_cpl';
VEHLIBStruct.table{n}.wprim2_cpl.longname='Vitesse au primaire 2 du coupleur';
VEHLIBStruct.table{n}.wprim2_cpl.type='double';
VEHLIBStruct.table{n}.wprim2_cpl.unit='rd/s';
VEHLIBStruct.table{n}.wprim2_cpl.precision='%f';
VEHLIBStruct.table{n}.wprim2_cpl.vector='wprim2_cpl';

VEHLIBStruct.table{n}.csec_cpl=struct;
VEHLIBStruct.table{n}.csec_cpl.condition='true';
VEHLIBStruct.table{n}.csec_cpl.name='csec_cpl';
VEHLIBStruct.table{n}.csec_cpl.longname='Couple au secondaire du coupleur';
VEHLIBStruct.table{n}.csec_cpl.type='double';
VEHLIBStruct.table{n}.csec_cpl.unit='Nm';
VEHLIBStruct.table{n}.csec_cpl.precision='%f';
VEHLIBStruct.table{n}.csec_cpl.vector='csec_cpl';

VEHLIBStruct.table{n}.wsec_cpl=struct;
VEHLIBStruct.table{n}.wsec_cpl.condition='true';
VEHLIBStruct.table{n}.wsec_cpl.name='wsec_cpl';
VEHLIBStruct.table{n}.wsec_cpl.longname='Vitesse au secondaire du coupleur';
VEHLIBStruct.table{n}.wsec_cpl.type='double';
VEHLIBStruct.table{n}.wsec_cpl.unit='rd/s';
VEHLIBStruct.table{n}.wsec_cpl.precision='%f';
VEHLIBStruct.table{n}.wsec_cpl.vector='wsec_cpl';

% 'VARIABLES COUPLEUR1'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES COUPLEUR 1';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables

VEHLIBStruct.table{n}.cprim1_cpl1=struct;
VEHLIBStruct.table{n}.cprim1_cpl1.condition='true';
VEHLIBStruct.table{n}.cprim1_cpl1.name='cprim1_cpl1';
VEHLIBStruct.table{n}.cprim1_cpl1.longname='Couple au primaire 1 du coupleur';
VEHLIBStruct.table{n}.cprim1_cpl1.type='double';
VEHLIBStruct.table{n}.cprim1_cpl1.unit='Nm';
VEHLIBStruct.table{n}.cprim1_cpl1.precision='%f';
VEHLIBStruct.table{n}.cprim1_cpl1.vector='cprim1_cpl1';

VEHLIBStruct.table{n}.wprim1_cpl1=struct;
VEHLIBStruct.table{n}.wprim1_cpl1.condition='true';
VEHLIBStruct.table{n}.wprim1_cpl1.name='wprim1_cpl1';
VEHLIBStruct.table{n}.wprim1_cpl1.longname='Vitesse au primaire 1 du coupleur';
VEHLIBStruct.table{n}.wprim1_cpl1.type='double';
VEHLIBStruct.table{n}.wprim1_cpl1.unit='rd/s';
VEHLIBStruct.table{n}.wprim1_cpl1.precision='%f';
VEHLIBStruct.table{n}.wprim1_cpl1.vector='wprim1_cpl1';

VEHLIBStruct.table{n}.cprim2_cpl1=struct;
VEHLIBStruct.table{n}.cprim2_cpl1.condition='true';
VEHLIBStruct.table{n}.cprim2_cpl1.name='cprim2_cpl1';
VEHLIBStruct.table{n}.cprim2_cpl1.longname='Couple au primaire 2 du coupleur';
VEHLIBStruct.table{n}.cprim2_cpl1.type='double';
VEHLIBStruct.table{n}.cprim2_cpl1.unit='Nm';
VEHLIBStruct.table{n}.cprim2_cpl1.precision='%f';
VEHLIBStruct.table{n}.cprim2_cpl1.vector='cprim2_cpl1';

VEHLIBStruct.table{n}.wprim2_cpl1=struct;
VEHLIBStruct.table{n}.wprim2_cpl1.condition='true';
VEHLIBStruct.table{n}.wprim2_cpl1.name='wprim2_cpl1';
VEHLIBStruct.table{n}.wprim2_cpl1.longname='Vitesse au primaire 2 du coupleur';
VEHLIBStruct.table{n}.wprim2_cpl1.type='double';
VEHLIBStruct.table{n}.wprim2_cpl1.unit='rd/s';
VEHLIBStruct.table{n}.wprim2_cpl1.precision='%f';
VEHLIBStruct.table{n}.wprim2_cpl1.vector='wprim2_cpl1';

VEHLIBStruct.table{n}.csec_cpl1=struct;
VEHLIBStruct.table{n}.csec_cpl1.condition='true';
VEHLIBStruct.table{n}.csec_cpl1.name='csec_cpl1';
VEHLIBStruct.table{n}.csec_cpl1.longname='Couple au secondaire du coupleur';
VEHLIBStruct.table{n}.csec_cpl1.type='double';
VEHLIBStruct.table{n}.csec_cpl1.unit='Nm';
VEHLIBStruct.table{n}.csec_cpl1.precision='%f';
VEHLIBStruct.table{n}.csec_cpl1.vector='csec_cpl1';

VEHLIBStruct.table{n}.wsec_cpl1=struct;
VEHLIBStruct.table{n}.wsec_cpl1.condition='true';
VEHLIBStruct.table{n}.wsec_cpl1.name='wsec_cpl1';
VEHLIBStruct.table{n}.wsec_cpl1.longname='Vitesse au secondaire du coupleur';
VEHLIBStruct.table{n}.wsec_cpl1.type='double';
VEHLIBStruct.table{n}.wsec_cpl1.unit='rd/s';
VEHLIBStruct.table{n}.wsec_cpl1.precision='%f';
VEHLIBStruct.table{n}.wsec_cpl1.vector='wsec_cpl1';

% 'VARIABLES COUPLEUR2'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES COUPLEUR 2';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables

VEHLIBStruct.table{n}.cprim1_cpl2=struct;
VEHLIBStruct.table{n}.cprim1_cpl2.condition='true';
VEHLIBStruct.table{n}.cprim1_cpl2.name='cprim1_cpl2';
VEHLIBStruct.table{n}.cprim1_cpl2.longname='Couple au primaire 1 du coupleur';
VEHLIBStruct.table{n}.cprim1_cpl2.type='double';
VEHLIBStruct.table{n}.cprim1_cpl2.unit='Nm';
VEHLIBStruct.table{n}.cprim1_cpl2.precision='%f';
VEHLIBStruct.table{n}.cprim1_cpl2.vector='cprim1_cpl2';

VEHLIBStruct.table{n}.wprim1_cpl2=struct;
VEHLIBStruct.table{n}.wprim1_cpl2.condition='true';
VEHLIBStruct.table{n}.wprim1_cpl2.name='wprim1_cpl2';
VEHLIBStruct.table{n}.wprim1_cpl2.longname='Vitesse au primaire 1 du coupleur';
VEHLIBStruct.table{n}.wprim1_cpl2.type='double';
VEHLIBStruct.table{n}.wprim1_cpl2.unit='rd/s';
VEHLIBStruct.table{n}.wprim1_cpl2.precision='%f';
VEHLIBStruct.table{n}.wprim1_cpl2.vector='wprim1_cpl2';

VEHLIBStruct.table{n}.cprim2_cpl2=struct;
VEHLIBStruct.table{n}.cprim2_cpl2.condition='true';
VEHLIBStruct.table{n}.cprim2_cpl2.name='cprim2_cpl2';
VEHLIBStruct.table{n}.cprim2_cpl2.longname='Couple au primaire 2 du coupleur';
VEHLIBStruct.table{n}.cprim2_cpl2.type='double';
VEHLIBStruct.table{n}.cprim2_cpl2.unit='Nm';
VEHLIBStruct.table{n}.cprim2_cpl2.precision='%f';
VEHLIBStruct.table{n}.cprim2_cpl2.vector='cprim2_cpl2';

VEHLIBStruct.table{n}.wprim2_cpl2=struct;
VEHLIBStruct.table{n}.wprim2_cpl2.condition='true';
VEHLIBStruct.table{n}.wprim2_cpl2.name='wprim2_cpl2';
VEHLIBStruct.table{n}.wprim2_cpl2.longname='Vitesse au primaire 2 du coupleur';
VEHLIBStruct.table{n}.wprim2_cpl2.type='double';
VEHLIBStruct.table{n}.wprim2_cpl2.unit='rd/s';
VEHLIBStruct.table{n}.wprim2_cpl2.precision='%f';
VEHLIBStruct.table{n}.wprim2_cpl2.vector='wprim2_cpl2';

VEHLIBStruct.table{n}.csec_cpl2=struct;
VEHLIBStruct.table{n}.csec_cpl2.condition='true';
VEHLIBStruct.table{n}.csec_cpl2.name='csec_cpl2';
VEHLIBStruct.table{n}.csec_cpl2.longname='Couple au secondaire du coupleur';
VEHLIBStruct.table{n}.csec_cpl2.type='double';
VEHLIBStruct.table{n}.csec_cpl2.unit='Nm';
VEHLIBStruct.table{n}.csec_cpl2.precision='%f';
VEHLIBStruct.table{n}.csec_cpl2.vector='csec_cpl2';

VEHLIBStruct.table{n}.wsec_cpl2=struct;
VEHLIBStruct.table{n}.wsec_cpl2.condition='true';
VEHLIBStruct.table{n}.wsec_cpl2.name='wsec_cpl2';
VEHLIBStruct.table{n}.wsec_cpl2.longname='Vitesse au secondaire du coupleur';
VEHLIBStruct.table{n}.wsec_cpl2.type='double';
VEHLIBStruct.table{n}.wsec_cpl2.unit='rd/s';
VEHLIBStruct.table{n}.wsec_cpl2.precision='%f';
VEHLIBStruct.table{n}.wsec_cpl2.vector='wsec_cpl2';

% 'VARIABLES TRAIN EPICYCLOIDALE'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES TRAIN EPICYCLOIDALE';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables

VEHLIBStruct.table{n}.ccour_train=struct;
VEHLIBStruct.table{n}.ccour_train.condition='true';
VEHLIBStruct.table{n}.ccour_train.name='ccour_train';
VEHLIBStruct.table{n}.ccour_train.longname='Couple sur la couronne';
VEHLIBStruct.table{n}.ccour_train.type='double';
VEHLIBStruct.table{n}.ccour_train.unit='Nm';
VEHLIBStruct.table{n}.ccour_train.precision='%f';
VEHLIBStruct.table{n}.ccour_train.vector='ccour_train';

VEHLIBStruct.table{n}.wcour_train=struct;
VEHLIBStruct.table{n}.wcour_train.condition='true';
VEHLIBStruct.table{n}.wcour_train.name='wcour_train';
VEHLIBStruct.table{n}.wcour_train.longname='vitesse de la couronne';
VEHLIBStruct.table{n}.wcour_train.type='double';
VEHLIBStruct.table{n}.wcour_train.unit='rd/s';
VEHLIBStruct.table{n}.wcour_train.precision='%f';
VEHLIBStruct.table{n}.wcour_train.vector='wcour_train';

VEHLIBStruct.table{n}.cps_train=struct;
VEHLIBStruct.table{n}.cps_train.condition='true';
VEHLIBStruct.table{n}.cps_train.name='cps_train';
VEHLIBStruct.table{n}.cps_train.longname='Couple sur le porte satellite';
VEHLIBStruct.table{n}.cps_train.type='double';
VEHLIBStruct.table{n}.cps_train.unit='Nm';
VEHLIBStruct.table{n}.cps_train.precision='%f';
VEHLIBStruct.table{n}.cps_train.vector='cps_train';

VEHLIBStruct.table{n}.wps_train=struct;
VEHLIBStruct.table{n}.wps_train.condition='true';
VEHLIBStruct.table{n}.wps_train.name='wps_train';
VEHLIBStruct.table{n}.wps_train.longname='vitesse du porte satellite';
VEHLIBStruct.table{n}.wps_train.type='double';
VEHLIBStruct.table{n}.wps_train.unit='rd/s';
VEHLIBStruct.table{n}.wps_train.precision='%f';
VEHLIBStruct.table{n}.wps_train.vector='wps_train';

VEHLIBStruct.table{n}.csoleil_train=struct;
VEHLIBStruct.table{n}.csoleil_train.condition='true';
VEHLIBStruct.table{n}.csoleil_train.name='csoleil_train';
VEHLIBStruct.table{n}.csoleil_train.longname='Couple sur le soleil';
VEHLIBStruct.table{n}.csoleil_train.type='double';
VEHLIBStruct.table{n}.csoleil_train.unit='Nm';
VEHLIBStruct.table{n}.csoleil_train.precision='%f';
VEHLIBStruct.table{n}.csoleil_train.vector='csoleil_train';

VEHLIBStruct.table{n}.wsoleil_train=struct;
VEHLIBStruct.table{n}.wsoleil_train.condition='true';
VEHLIBStruct.table{n}.wsoleil_train.name='wsoleil_train';
VEHLIBStruct.table{n}.wsoleil_train.longname='vitesse du soleil';
VEHLIBStruct.table{n}.wsoleil_train.type='double';
VEHLIBStruct.table{n}.wsoleil_train.unit='rd/s';
VEHLIBStruct.table{n}.wsoleil_train.precision='%f';
VEHLIBStruct.table{n}.wsoleil_train.vector='wsoleil_train';

% 'VARIABLES ACCESSOIRES'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES ACCESSOIRES';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.uacc=struct;
VEHLIBStruct.table{n}.uacc.condition='VD.ACC.ntypacc==1';
VEHLIBStruct.table{n}.uacc.name='uacc';
VEHLIBStruct.table{n}.uacc.longname='Tension accesoires';
VEHLIBStruct.table{n}.uacc.type='double';
VEHLIBStruct.table{n}.uacc.unit='V';
VEHLIBStruct.table{n}.uacc.precision='%f';
VEHLIBStruct.table{n}.uacc.vector='uacc';

VEHLIBStruct.table{n}.iacc=struct;
VEHLIBStruct.table{n}.iacc.condition='VD.ACC.ntypacc==1';
VEHLIBStruct.table{n}.iacc.name='iacc';
VEHLIBStruct.table{n}.iacc.longname='Courant accesoires';
VEHLIBStruct.table{n}.iacc.type='double';
VEHLIBStruct.table{n}.iacc.unit='A';
VEHLIBStruct.table{n}.iacc.precision='%f';
VEHLIBStruct.table{n}.iacc.vector='iacc';

VEHLIBStruct.table{n}.Pacc=struct;
VEHLIBStruct.table{n}.Pacc.condition='VD.ACC.ntypacc==1';
VEHLIBStruct.table{n}.Pacc.name='Pacc';
VEHLIBStruct.table{n}.Pacc.longname='Puissance d accessoires';
VEHLIBStruct.table{n}.Pacc.type='double';
VEHLIBStruct.table{n}.Pacc.unit='W';
VEHLIBStruct.table{n}.Pacc.precision='%f';
VEHLIBStruct.table{n}.Pacc.vector='uacc.*iacc';

VEHLIBStruct.table{n}.waccm1=struct;
VEHLIBStruct.table{n}.waccm1.condition='VD.ACC.ntypacc==2 || VD.ACC.ntypacc==3 || VD.ACC.ntypacc==4';
VEHLIBStruct.table{n}.waccm1.name='waccm1';
VEHLIBStruct.table{n}.waccm1.longname='regime de rotation d accessoires mecaniques';
VEHLIBStruct.table{n}.waccm1.type='double';
VEHLIBStruct.table{n}.waccm1.unit='rd/sec';
VEHLIBStruct.table{n}.waccm1.precision='%f';
VEHLIBStruct.table{n}.waccm1.vector='waccm1';

VEHLIBStruct.table{n}.caccm1=struct;
VEHLIBStruct.table{n}.caccm1.condition='VD.ACC.ntypacc==2 || VD.ACC.ntypacc==3 || VD.ACC.ntypacc==4';
VEHLIBStruct.table{n}.caccm1.name='caccm1';
VEHLIBStruct.table{n}.caccm1.longname='Couple d accessoires mecaniques';
VEHLIBStruct.table{n}.caccm1.type='double';
VEHLIBStruct.table{n}.caccm1.unit='Nm';
VEHLIBStruct.table{n}.caccm1.precision='%f';
VEHLIBStruct.table{n}.caccm1.vector='caccm1';

VEHLIBStruct.table{n}.Paccm1=struct;
VEHLIBStruct.table{n}.Paccm1.condition='VD.ACC.ntypacc==2 || VD.ACC.ntypacc==4';
VEHLIBStruct.table{n}.Paccm1.name='Paccm1';
VEHLIBStruct.table{n}.Paccm1.longname='Puissance d accessoires mecaniques alternateur';
VEHLIBStruct.table{n}.Paccm1.type='double';
VEHLIBStruct.table{n}.Paccm1.unit='W';
VEHLIBStruct.table{n}.Paccm1.precision='%f';
VEHLIBStruct.table{n}.Paccm1.vector='caccm1.*waccm1';

VEHLIBStruct.table{n}.Paccm_alt=struct;
VEHLIBStruct.table{n}.Paccm_alt.condition='VD.ACC.ntypacc==3';
VEHLIBStruct.table{n}.Paccm_alt.name='Paccm_alt';
VEHLIBStruct.table{n}.Paccm_alt.longname='Puissance d accessoires mecaniques alternateur';
VEHLIBStruct.table{n}.Paccm_alt.type='double';
VEHLIBStruct.table{n}.Paccm_alt.unit='W';
VEHLIBStruct.table{n}.Paccm_alt.precision='%f';
VEHLIBStruct.table{n}.Paccm_alt.vector='paccm_alt';

VEHLIBStruct.table{n}.Paccm_servo=struct;
VEHLIBStruct.table{n}.Paccm_servo.condition='VD.ACC.ntypacc==3';
VEHLIBStruct.table{n}.Paccm_servo.name='Paccm_servo';
VEHLIBStruct.table{n}.Paccm_servo.longname='Puissance d accessoires mecaniques servo';
VEHLIBStruct.table{n}.Paccm_servo.type='double';
VEHLIBStruct.table{n}.Paccm_servo.unit='W';
VEHLIBStruct.table{n}.Paccm_servo.precision='%f';
VEHLIBStruct.table{n}.Paccm_servo.vector='paccm_servo';

VEHLIBStruct.table{n}.Paccm_air=struct;
VEHLIBStruct.table{n}.Paccm_air.condition='VD.ACC.ntypacc==3';
VEHLIBStruct.table{n}.Paccm_air.name='Paccm_air';
VEHLIBStruct.table{n}.Paccm_air.longname='Puissance d accessoires mecaniques compresseur d''air';
VEHLIBStruct.table{n}.Paccm_air.type='double';
VEHLIBStruct.table{n}.Paccm_air.unit='W';
VEHLIBStruct.table{n}.Paccm_air.precision='%f';
VEHLIBStruct.table{n}.Paccm_air.vector='paccm_air';

VEHLIBStruct.table{n}.Paccm_ref=struct;
VEHLIBStruct.table{n}.Paccm_ref.condition='VD.ACC.ntypacc==3';
VEHLIBStruct.table{n}.Paccm_ref.name='Paccm_ref';
VEHLIBStruct.table{n}.Paccm_ref.longname='Puissance d accessoires mecaniques ventilateur refroidissement moteur';
VEHLIBStruct.table{n}.Paccm_ref.type='double';
VEHLIBStruct.table{n}.Paccm_ref.unit='W';
VEHLIBStruct.table{n}.Paccm_ref.precision='%f';
VEHLIBStruct.table{n}.Paccm_ref.vector='paccm_ref';

VEHLIBStruct.table{n}.waccm2=struct;
VEHLIBStruct.table{n}.waccm2.condition='VD.ACC.ntypacc==2 || VD.ACC.ntypacc==4';
VEHLIBStruct.table{n}.waccm2.name='waccm2';
VEHLIBStruct.table{n}.waccm2.longname='regime de rotation compresseur de climatisation';
VEHLIBStruct.table{n}.waccm2.type='double';
VEHLIBStruct.table{n}.waccm2.unit='rd/sec';
VEHLIBStruct.table{n}.waccm2.precision='%f';
VEHLIBStruct.table{n}.waccm2.vector='waccm2';

VEHLIBStruct.table{n}.caccm2=struct;
VEHLIBStruct.table{n}.caccm2.condition='VD.ACC.ntypacc==2 || VD.ACC.ntypacc==4';
VEHLIBStruct.table{n}.caccm2.name='caccm2';
VEHLIBStruct.table{n}.caccm2.longname='Couple d accessoires mecaniques compresseur de climatisation';
VEHLIBStruct.table{n}.caccm2.type='double';
VEHLIBStruct.table{n}.caccm2.unit='Nm';
VEHLIBStruct.table{n}.caccm2.precision='%f';
VEHLIBStruct.table{n}.caccm2.vector='caccm2';

VEHLIBStruct.table{n}.Paccm2=struct;
VEHLIBStruct.table{n}.Paccm2.condition='VD.ACC.ntypacc==2 || VD.ACC.ntypacc==4';
VEHLIBStruct.table{n}.Paccm2.name='Paccm2';
VEHLIBStruct.table{n}.Paccm2.longname='Puissance d accessoires mecaniques compresseur de climatisation';
VEHLIBStruct.table{n}.Paccm2.type='double';
VEHLIBStruct.table{n}.Paccm2.unit='W';
VEHLIBStruct.table{n}.Paccm2.precision='%f';
VEHLIBStruct.table{n}.Paccm2.vector='caccm2.*waccm2';

% 'VARIABLES BATTERIE'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES BATTERIE';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.ibat=struct;
VEHLIBStruct.table{n}.ibat.condition='true';
VEHLIBStruct.table{n}.ibat.name='ibat';
VEHLIBStruct.table{n}.ibat.longname='Courant batterie';
VEHLIBStruct.table{n}.ibat.type='double';
VEHLIBStruct.table{n}.ibat.unit='A';
VEHLIBStruct.table{n}.ibat.precision='%f';
VEHLIBStruct.table{n}.ibat.vector='ibat';

VEHLIBStruct.table{n}.ubat=struct;
VEHLIBStruct.table{n}.ubat.condition='true';
VEHLIBStruct.table{n}.ubat.name='ubat';
VEHLIBStruct.table{n}.ubat.longname='Tension batterie';
VEHLIBStruct.table{n}.ubat.type='double';
VEHLIBStruct.table{n}.ubat.unit='V';
VEHLIBStruct.table{n}.ubat.precision='%f';
VEHLIBStruct.table{n}.ubat.vector='ubat';

VEHLIBStruct.table{n}.u0bat=struct;
VEHLIBStruct.table{n}.u0bat.condition='true';
VEHLIBStruct.table{n}.u0bat.name='u0bat';
VEHLIBStruct.table{n}.u0bat.longname='Tension batterie a vide';
VEHLIBStruct.table{n}.u0bat.type='double';
VEHLIBStruct.table{n}.u0bat.unit='V';
VEHLIBStruct.table{n}.u0bat.precision='%f';
VEHLIBStruct.table{n}.u0bat.vector='u0bat';

VEHLIBStruct.table{n}.Pbat=struct;
VEHLIBStruct.table{n}.Pbat.condition='true';
VEHLIBStruct.table{n}.Pbat.name='Pbat';
VEHLIBStruct.table{n}.Pbat.longname='Puissance batterie';
VEHLIBStruct.table{n}.Pbat.type='double';
VEHLIBStruct.table{n}.Pbat.unit='W';
VEHLIBStruct.table{n}.Pbat.precision='%f';
VEHLIBStruct.table{n}.Pbat.vector='ubat.*ibat';

VEHLIBStruct.table{n}.Rbat=struct;
VEHLIBStruct.table{n}.Rbat.condition='true';
VEHLIBStruct.table{n}.Rbat.name='Rbat';
VEHLIBStruct.table{n}.Rbat.longname='Resistance batterie';
VEHLIBStruct.table{n}.Rbat.type='double';
VEHLIBStruct.table{n}.Rbat.unit='Ohms';
VEHLIBStruct.table{n}.Rbat.precision='%f';
VEHLIBStruct.table{n}.Rbat.vector='Rbat';

VEHLIBStruct.table{n}.RdF=struct;
VEHLIBStruct.table{n}.RdF.condition='true';
VEHLIBStruct.table{n}.RdF.name='RdF';
VEHLIBStruct.table{n}.RdF.longname='Rendement faradique';
VEHLIBStruct.table{n}.RdF.type='double';
VEHLIBStruct.table{n}.RdF.unit=' ';
VEHLIBStruct.table{n}.RdF.precision='%f';
VEHLIBStruct.table{n}.RdF.vector='RdF';

VEHLIBStruct.table{n}.E_bat=struct;
VEHLIBStruct.table{n}.E_bat.condition='true';
VEHLIBStruct.table{n}.E_bat.name='E_bat';
VEHLIBStruct.table{n}.E_bat.longname='Energie pack batterie';
VEHLIBStruct.table{n}.E_bat.type='double';
VEHLIBStruct.table{n}.E_bat.unit='Ws';
VEHLIBStruct.table{n}.E_bat.precision='%f';
VEHLIBStruct.table{n}.E_bat.vector='cumtrapz(tsim,ubat.*ibat)';

VEHLIBStruct.table{n}.dod=struct;
VEHLIBStruct.table{n}.dod.condition='true';
VEHLIBStruct.table{n}.dod.name='dod';
VEHLIBStruct.table{n}.dod.longname='Profondeur de decharge de la batterie';
VEHLIBStruct.table{n}.dod.type='double';
VEHLIBStruct.table{n}.dod.unit='%';
VEHLIBStruct.table{n}.dod.precision='%f';
VEHLIBStruct.table{n}.dod.vector='dod';

VEHLIBStruct.table{n}.soc=struct;
VEHLIBStruct.table{n}.soc.condition='true';
VEHLIBStruct.table{n}.soc.name='soc';
VEHLIBStruct.table{n}.soc.longname='Etat de charge de la batterie';
VEHLIBStruct.table{n}.soc.type='double';
VEHLIBStruct.table{n}.soc.unit='%';
VEHLIBStruct.table{n}.soc.precision='%f';
VEHLIBStruct.table{n}.soc.vector='soc';

VEHLIBStruct.table{n}.ah=struct;
VEHLIBStruct.table{n}.ah.condition='true';
VEHLIBStruct.table{n}.ah.name='ah';
VEHLIBStruct.table{n}.ah.longname='Ampere-heure batterie';
VEHLIBStruct.table{n}.ah.type='double';
VEHLIBStruct.table{n}.ah.unit='Ah';
VEHLIBStruct.table{n}.ah.precision='%f';
VEHLIBStruct.table{n}.ah.vector='ah';

VEHLIBStruct.table{n}.ibat_mono=struct;
VEHLIBStruct.table{n}.ibat_mono.condition='true';
VEHLIBStruct.table{n}.ibat_mono.name='ibat_mono';
VEHLIBStruct.table{n}.ibat_mono.longname='Courant monobloc batterie';
VEHLIBStruct.table{n}.ibat_mono.type='double';
VEHLIBStruct.table{n}.ibat_mono.unit='A';
VEHLIBStruct.table{n}.ibat_mono.precision='%f';
VEHLIBStruct.table{n}.ibat_mono.vector='ibat/VD.BATT.Nbranchepar';

VEHLIBStruct.table{n}.ibat_max=struct;
VEHLIBStruct.table{n}.ibat_max.condition='true';
VEHLIBStruct.table{n}.ibat_max.name='ibat_max';
VEHLIBStruct.table{n}.ibat_max.longname='Courant maxi (decharge) monobloc';
VEHLIBStruct.table{n}.ibat_max.type='double';
VEHLIBStruct.table{n}.ibat_max.unit='A';
VEHLIBStruct.table{n}.ibat_max.precision='%f';
VEHLIBStruct.table{n}.ibat_max.vector='ibat_max/VD.BATT.Nbranchepar';

VEHLIBStruct.table{n}.ibat_min=struct;
VEHLIBStruct.table{n}.ibat_min.condition='true';
VEHLIBStruct.table{n}.ibat_min.name='ibat_min';
VEHLIBStruct.table{n}.ibat_min.longname='Courant mini (charge) monobloc';
VEHLIBStruct.table{n}.ibat_min.type='double';
VEHLIBStruct.table{n}.ibat_min.unit='A';
VEHLIBStruct.table{n}.ibat_min.precision='%f';
VEHLIBStruct.table{n}.ibat_min.vector='ibat_min/VD.BATT.Nbranchepar';

VEHLIBStruct.table{n}.ubat_mono=struct;
VEHLIBStruct.table{n}.ubat_mono.condition='true';
VEHLIBStruct.table{n}.ubat_mono.name='ubat_mono';
VEHLIBStruct.table{n}.ubat_mono.longname='Tension monobloc batterie';
VEHLIBStruct.table{n}.ubat_mono.type='double';
VEHLIBStruct.table{n}.ubat_mono.unit='V';
VEHLIBStruct.table{n}.ubat_mono.precision='%f';
VEHLIBStruct.table{n}.ubat_mono.vector='ubat/VD.BATT.Nblocser';

VEHLIBStruct.table{n}.Pbat_mono=struct;
VEHLIBStruct.table{n}.Pbat_mono.condition='true';
VEHLIBStruct.table{n}.Pbat_mono.name='Pbat_mono';
VEHLIBStruct.table{n}.Pbat_mono.longname='Puissance monobloc batterie';
VEHLIBStruct.table{n}.Pbat_mono.type='double';
VEHLIBStruct.table{n}.Pbat_mono.unit='W';
VEHLIBStruct.table{n}.Pbat_mono.precision='%f';
VEHLIBStruct.table{n}.Pbat_mono.vector='ubat.*ibat/(VD.BATT.Nblocser*VD.BATT.Nbranchepar)';

VEHLIBStruct.table{n}.Ebat_mono=struct;
VEHLIBStruct.table{n}.Ebat_mono.condition='true';
VEHLIBStruct.table{n}.Ebat_mono.name='Ebat_mono';
VEHLIBStruct.table{n}.Ebat_mono.longname='Energie monobloc batterie';
VEHLIBStruct.table{n}.Ebat_mono.type='double';
VEHLIBStruct.table{n}.Ebat_mono.unit='Ws';
VEHLIBStruct.table{n}.Ebat_mono.precision='%f';
VEHLIBStruct.table{n}.Ebat_mono.vector='cumtrapz(tsim,ubat.*ibat/(VD.BATT.Nblocser*VD.BATT.Nbranchepar))';

VEHLIBStruct.table{n}.flag_recharge=struct;
VEHLIBStruct.table{n}.flag_recharge.condition='exist(''VD.CYCL'',''var'') && isfield(VD.CYCL,''ntypcin'') && VD.CYCL.ntypcin~=5';
VEHLIBStruct.table{n}.flag_recharge.name='flag_recharge';
VEHLIBStruct.table{n}.flag_recharge.longname='Indicateur de recharge de la batterie sur le reseau';
VEHLIBStruct.table{n}.flag_recharge.type='double';
VEHLIBStruct.table{n}.flag_recharge.unit='';
VEHLIBStruct.table{n}.flag_recharge.precision='%f';
VEHLIBStruct.table{n}.flag_recharge.vector='flag_recharge';

VEHLIBStruct.table{n}.flag_stop_bat=struct;
VEHLIBStruct.table{n}.flag_stop_bat.condition='true';
VEHLIBStruct.table{n}.flag_stop_bat.name='flag_stop_bat';
VEHLIBStruct.table{n}.flag_stop_bat.longname='Flag arret tension batterie';
VEHLIBStruct.table{n}.flag_stop_bat.type='double';
VEHLIBStruct.table{n}.flag_stop_bat.unit='';
VEHLIBStruct.table{n}.flag_stop_bat.precision='%f';
VEHLIBStruct.table{n}.flag_stop_bat.vector='flag_stop_bat';

% 'VARIABLES SUPER CONDENSATEUR'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES SUPER CONDENSATEUR';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

VEHLIBStruct.table{n}.isc=struct;
VEHLIBStruct.table{n}.isc.condition='true';
VEHLIBStruct.table{n}.isc.name='isc';
VEHLIBStruct.table{n}.isc.longname='Courant super condensateur';
VEHLIBStruct.table{n}.isc.type='double';
VEHLIBStruct.table{n}.isc.unit='A';
VEHLIBStruct.table{n}.isc.precision='%f';
VEHLIBStruct.table{n}.isc.vector='isc';

VEHLIBStruct.table{n}.usc=struct;
VEHLIBStruct.table{n}.usc.condition='true';
VEHLIBStruct.table{n}.usc.name='usc';
VEHLIBStruct.table{n}.usc.longname='Tension super condensateur';
VEHLIBStruct.table{n}.usc.type='double';
VEHLIBStruct.table{n}.usc.unit='V';
VEHLIBStruct.table{n}.usc.precision='%f';
VEHLIBStruct.table{n}.usc.vector='usc';

VEHLIBStruct.table{n}.P_sc=struct;
VEHLIBStruct.table{n}.P_sc.condition='true';
VEHLIBStruct.table{n}.P_sc.name='P_sc';
VEHLIBStruct.table{n}.P_sc.longname='Puissance super condensateur';
VEHLIBStruct.table{n}.P_sc.type='double';
VEHLIBStruct.table{n}.P_sc.unit='W';
VEHLIBStruct.table{n}.P_sc.precision='%f';
VEHLIBStruct.table{n}.P_sc.vector='usc.*isc';

VEHLIBStruct.table{n}.E_sc=struct;
VEHLIBStruct.table{n}.E_sc.condition='true';
VEHLIBStruct.table{n}.E_sc.name='E_sc';
VEHLIBStruct.table{n}.E_sc.longname='Energie super condensateur';
VEHLIBStruct.table{n}.E_sc.type='double';
VEHLIBStruct.table{n}.E_sc.unit='Ws';
VEHLIBStruct.table{n}.E_sc.precision='%f';
VEHLIBStruct.table{n}.E_sc.vector='cumtrapz(tsim,usc.*isc)';

VEHLIBStruct.table{n}.isc_elt=struct;
VEHLIBStruct.table{n}.isc_elt.condition='true';
VEHLIBStruct.table{n}.isc_elt.name='isc_elt';
VEHLIBStruct.table{n}.isc_elt.longname='Courant element du super condensateur';
VEHLIBStruct.table{n}.isc_elt.type='double';
VEHLIBStruct.table{n}.isc_elt.unit='A';
VEHLIBStruct.table{n}.isc_elt.precision='%f';
VEHLIBStruct.table{n}.isc_elt.vector='isc/SC.Nbranchepar';

VEHLIBStruct.table{n}.isc_min=struct;
VEHLIBStruct.table{n}.isc_min.condition='true';
VEHLIBStruct.table{n}.isc_min.name='isc_min';
VEHLIBStruct.table{n}.isc_min.longname='Courant mini du super condensateur';
VEHLIBStruct.table{n}.isc_min.type='double';
VEHLIBStruct.table{n}.isc_min.unit='A';
VEHLIBStruct.table{n}.isc_min.precision='%f';
VEHLIBStruct.table{n}.isc_min.vector='isc_min';

VEHLIBStruct.table{n}.usc_elt=struct;
VEHLIBStruct.table{n}.usc_elt.condition='true';
VEHLIBStruct.table{n}.usc_elt.name='usc_elt';
VEHLIBStruct.table{n}.usc_elt.longname='Tension element du super condensateur';
VEHLIBStruct.table{n}.usc_elt.type='double';
VEHLIBStruct.table{n}.usc_elt.unit='V';
VEHLIBStruct.table{n}.usc_elt.precision='%f';
VEHLIBStruct.table{n}.usc_elt.vector='usc/SC.Nblocser';

VEHLIBStruct.table{n}.P_sc_elt=struct;
VEHLIBStruct.table{n}.P_sc_elt.condition='true';
VEHLIBStruct.table{n}.P_sc_elt.name='P_sc_elt';
VEHLIBStruct.table{n}.P_sc_elt.longname='Puissance element du super condensateur';
VEHLIBStruct.table{n}.P_sc_elt.type='double';
VEHLIBStruct.table{n}.P_sc_elt.unit='W';
VEHLIBStruct.table{n}.P_sc_elt.precision='%f';
VEHLIBStruct.table{n}.P_sc_elt.vector='usc.*isc/(SC.Nblocser*SC.Nbranchepar)';

VEHLIBStruct.table{n}.u0sc=struct;
VEHLIBStruct.table{n}.u0sc.condition='true';
VEHLIBStruct.table{n}.u0sc.name='u0sc';
VEHLIBStruct.table{n}.u0sc.longname='Tension a vide du pack de super condensateur';
VEHLIBStruct.table{n}.u0sc.type='double';
VEHLIBStruct.table{n}.u0sc.unit='V';
VEHLIBStruct.table{n}.u0sc.precision='%f';
VEHLIBStruct.table{n}.u0sc.vector='u0sc';
% 'VARIABLES RHEOSTAT'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES RHEOSTAT';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.irheo=struct;
VEHLIBStruct.table{n}.irheo.condition='true';
VEHLIBStruct.table{n}.irheo.name='irheo';
VEHLIBStruct.table{n}.irheo.longname='Courant rheostat';
VEHLIBStruct.table{n}.irheo.type='double';
VEHLIBStruct.table{n}.irheo.unit='A';
VEHLIBStruct.table{n}.irheo.precision='%f';
VEHLIBStruct.table{n}.irheo.vector='irheo';

VEHLIBStruct.table{n}.prheo=struct;
VEHLIBStruct.table{n}.prheo.condition='true';
VEHLIBStruct.table{n}.prheo.name='prheo';
VEHLIBStruct.table{n}.prheo.longname='Puissance rheostat';
VEHLIBStruct.table{n}.prheo.type='double';
VEHLIBStruct.table{n}.prheo.unit='W';
VEHLIBStruct.table{n}.prheo.precision='%f';
VEHLIBStruct.table{n}.prheo.vector='prheo';

VEHLIBStruct.table{n}.E_rheo=struct;
VEHLIBStruct.table{n}.E_rheo.condition='true';
VEHLIBStruct.table{n}.E_rheo.name='E_rheo';
VEHLIBStruct.table{n}.E_rheo.longname='Energie rheostat';
VEHLIBStruct.table{n}.E_rheo.type='double';
VEHLIBStruct.table{n}.E_rheo.unit='Ws';
VEHLIBStruct.table{n}.E_rheo.precision='%f';
VEHLIBStruct.table{n}.E_rheo.vector='cumtrapz(tsim,prheo)';

% 'VARIABLES VD.SURVOLTEUR'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES SURVOLTEUR';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.isurv_ht=struct;
VEHLIBStruct.table{n}.isurv_ht.condition='true';
VEHLIBStruct.table{n}.isurv_ht.name='isurv_ht';
VEHLIBStruct.table{n}.isurv_ht.longname='Courant survolteur cote HT';
VEHLIBStruct.table{n}.isurv_ht.type='double';
VEHLIBStruct.table{n}.isurv_ht.unit='A';
VEHLIBStruct.table{n}.isurv_ht.precision='%f';
VEHLIBStruct.table{n}.isurv_ht.vector='isurv_ht';

VEHLIBStruct.table{n}.usurv_ht=struct;
VEHLIBStruct.table{n}.usurv_ht.condition='true';
VEHLIBStruct.table{n}.usurv_ht.name='usurv_ht';
VEHLIBStruct.table{n}.usurv_ht.longname='Tension survolteur cote HT';
VEHLIBStruct.table{n}.usurv_ht.type='double';
VEHLIBStruct.table{n}.usurv_ht.unit='V';
VEHLIBStruct.table{n}.usurv_ht.precision='%f';
VEHLIBStruct.table{n}.usurv_ht.vector='usurv_ht';

VEHLIBStruct.table{n}.P_surv_ht=struct;
VEHLIBStruct.table{n}.P_surv_ht.condition='true';
VEHLIBStruct.table{n}.P_surv_ht.name='P_surv_ht';
VEHLIBStruct.table{n}.P_surv_ht.longname='Puissance survolteur cote HT';
VEHLIBStruct.table{n}.P_surv_ht.type='double';
VEHLIBStruct.table{n}.P_surv_ht.unit='W';
VEHLIBStruct.table{n}.P_surv_ht.precision='%f';
VEHLIBStruct.table{n}.P_surv_ht.vector='usurv_ht.*isurv_ht';

VEHLIBStruct.table{n}.qsurv=struct;
VEHLIBStruct.table{n}.qsurv.condition='true';
VEHLIBStruct.table{n}.qsurv.name='qsurv';
VEHLIBStruct.table{n}.qsurv.longname='Pertes survolteur';
VEHLIBStruct.table{n}.qsurv.type='double';
VEHLIBStruct.table{n}.qsurv.unit='W';
VEHLIBStruct.table{n}.qsurv.precision='%f';
VEHLIBStruct.table{n}.qsurv.vector='qsurv';

% 'VARIABLES CHARGEUR'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES CHARGEUR';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

VEHLIBStruct.table{n}.flag_Icst=struct;
VEHLIBStruct.table{n}.flag_Icst.condition='true';
VEHLIBStruct.table{n}.flag_Icst.name='flag_Icst';
VEHLIBStruct.table{n}.flag_Icst.longname='Flag de charge a courant constant';
VEHLIBStruct.table{n}.flag_Icst.type='double';
VEHLIBStruct.table{n}.flag_Icst.unit='';
VEHLIBStruct.table{n}.flag_Icst.precision='%f';
VEHLIBStruct.table{n}.flag_Icst.vector='flag_Icst';

VEHLIBStruct.table{n}.flag_Ucst=struct;
VEHLIBStruct.table{n}.flag_Ucst.condition='true';
VEHLIBStruct.table{n}.flag_Ucst.name='flag_Ucst';
VEHLIBStruct.table{n}.flag_Ucst.longname='Flag de charge a tension constante';
VEHLIBStruct.table{n}.flag_Ucst.type='double';
VEHLIBStruct.table{n}.flag_Ucst.unit='';
VEHLIBStruct.table{n}.flag_Ucst.precision='%f';
VEHLIBStruct.table{n}.flag_Ucst.vector='flag_Ucst';

VEHLIBStruct.table{n}.ubat_cons=struct;
VEHLIBStruct.table{n}.ubat_cons.condition='true';
VEHLIBStruct.table{n}.ubat_cons.name='ubat_cons';
VEHLIBStruct.table{n}.ubat_cons.longname='Tension de consigne par monobloc';
VEHLIBStruct.table{n}.ubat_cons.type='double';
VEHLIBStruct.table{n}.ubat_cons.unit='V';
VEHLIBStruct.table{n}.ubat_cons.precision='%f';
VEHLIBStruct.table{n}.ubat_cons.vector='ubat_cons';

VEHLIBStruct.table{n}.flag_Iegal=struct;
VEHLIBStruct.table{n}.flag_Iegal.condition='true';
VEHLIBStruct.table{n}.flag_Iegal.name='flag_Iegal';
VEHLIBStruct.table{n}.flag_Iegal.longname='Flag de charge d egalisation';
VEHLIBStruct.table{n}.flag_Iegal.type='double';
VEHLIBStruct.table{n}.flag_Iegal.unit='';
VEHLIBStruct.table{n}.flag_Iegal.precision='%f';
VEHLIBStruct.table{n}.flag_Iegal.vector='flag_Iegal';

% 'VARIABLES CONVERTISSEUR DE COUPLE'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES CONVERTISSEUR DE COUPLE';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.gliss_conv=struct;
VEHLIBStruct.table{n}.gliss_conv.condition='true';
VEHLIBStruct.table{n}.gliss_conv.name='gliss_conv';
VEHLIBStruct.table{n}.gliss_conv.longname='Glissement du convertisseur de couple';
VEHLIBStruct.table{n}.gliss_conv.type='double';
VEHLIBStruct.table{n}.gliss_conv.unit='';
VEHLIBStruct.table{n}.gliss_conv.precision='%f';
VEHLIBStruct.table{n}.gliss_conv.vector='(wprim_conv-wsec_conv)./wprim_conv';

VEHLIBStruct.table{n}.pont_conv=struct;
VEHLIBStruct.table{n}.pont_conv.condition='true';
VEHLIBStruct.table{n}.pont_conv.name='pont_conv';
VEHLIBStruct.table{n}.pont_conv.longname='Pontage du convertisseur de couple';
VEHLIBStruct.table{n}.pont_conv.type='double';
VEHLIBStruct.table{n}.pont_conv.unit='';
VEHLIBStruct.table{n}.pont_conv.precision='%f';
VEHLIBStruct.table{n}.pont_conv.vector='pont_conv';

VEHLIBStruct.table{n}.Rap_cpl_conv=struct;
VEHLIBStruct.table{n}.Rap_cpl_conv.condition='true';
VEHLIBStruct.table{n}.Rap_cpl_conv.name='Rap_cpl_conv';
VEHLIBStruct.table{n}.Rap_cpl_conv.longname='Rapport de couple du convertisseur de couple';
VEHLIBStruct.table{n}.Rap_cpl_conv.type='double';
VEHLIBStruct.table{n}.Rap_cpl_conv.unit='';
VEHLIBStruct.table{n}.Rap_cpl_conv.precision='%f';
VEHLIBStruct.table{n}.Rap_cpl_conv.vector='csec_conv./cpompe_conv';

VEHLIBStruct.table{n}.cprim_conv=struct;
VEHLIBStruct.table{n}.cprim_conv.condition='true';
VEHLIBStruct.table{n}.cprim_conv.name='cprim_conv';
VEHLIBStruct.table{n}.cprim_conv.longname='Couple primaire (entree) du convertisseur';
VEHLIBStruct.table{n}.cprim_conv.type='double';
VEHLIBStruct.table{n}.cprim_conv.unit='Nm';
VEHLIBStruct.table{n}.cprim_conv.precision='%f';
VEHLIBStruct.table{n}.cprim_conv.vector='cprim_conv';

VEHLIBStruct.table{n}.cpompe_conv=struct;
VEHLIBStruct.table{n}.cpompe_conv.condition='true';
VEHLIBStruct.table{n}.cpompe_conv.name='cpompe_conv';
VEHLIBStruct.table{n}.cpompe_conv.longname='Couple pompe du convertisseur';
VEHLIBStruct.table{n}.cpompe_conv.type='double';
VEHLIBStruct.table{n}.cpompe_conv.unit='Nm';
VEHLIBStruct.table{n}.cpompe_conv.precision='%f';
VEHLIBStruct.table{n}.cpompe_conv.vector='cpompe_conv';

VEHLIBStruct.table{n}.csec_conv=struct;
VEHLIBStruct.table{n}.csec_conv.condition='true';
VEHLIBStruct.table{n}.csec_conv.name='csec_conv';
VEHLIBStruct.table{n}.csec_conv.longname='Couple secondaire (turbine) du convertisseur';
VEHLIBStruct.table{n}.csec_conv.type='double';
VEHLIBStruct.table{n}.csec_conv.unit='Nm';
VEHLIBStruct.table{n}.csec_conv.precision='%f';
VEHLIBStruct.table{n}.csec_conv.vector='csec_conv';

VEHLIBStruct.table{n}.wprim_conv=struct;
VEHLIBStruct.table{n}.wprim_conv.condition='true';
VEHLIBStruct.table{n}.wprim_conv.name='wprim_conv';
VEHLIBStruct.table{n}.wprim_conv.longname='Vitesse arbre primaire du convertisseur';
VEHLIBStruct.table{n}.wprim_conv.type='double';
VEHLIBStruct.table{n}.wprim_conv.unit='rd/sec';
VEHLIBStruct.table{n}.wprim_conv.precision='%f';
VEHLIBStruct.table{n}.wprim_conv.vector='wprim_conv';

VEHLIBStruct.table{n}.wsec_conv=struct;
VEHLIBStruct.table{n}.wsec_conv.condition='true';
VEHLIBStruct.table{n}.wsec_conv.name='wsec_conv';
VEHLIBStruct.table{n}.wsec_conv.longname='Vitesse arbre secondaire du convertisseur';
VEHLIBStruct.table{n}.wsec_conv.type='double';
VEHLIBStruct.table{n}.wsec_conv.unit='rd/sec';
VEHLIBStruct.table{n}.wsec_conv.precision='%f';
VEHLIBStruct.table{n}.wsec_conv.vector='wsec_conv';

VEHLIBStruct.table{n}.rappvit_conv=struct;
VEHLIBStruct.table{n}.rappvit_conv.condition='true';
VEHLIBStruct.table{n}.rappvit_conv.name='rappvit_conv';
VEHLIBStruct.table{n}.rappvit_conv.longname='Rapport de vitesse du convertisseur de couple';
VEHLIBStruct.table{n}.rappvit_conv.type='double';
VEHLIBStruct.table{n}.rappvit_conv.unit='';
VEHLIBStruct.table{n}.rappvit_conv.precision='%f';
VEHLIBStruct.table{n}.rappvit_conv.vector='wprim_conv./wsec_conv';

% 'VARIABLES CVT'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES CVT';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.rappcvt=struct;
VEHLIBStruct.table{n}.rappcvt.condition='true';
VEHLIBStruct.table{n}.rappcvt.name='rappcvt';
VEHLIBStruct.table{n}.rappcvt.longname='Rapport de CVT';
VEHLIBStruct.table{n}.rappcvt.type='double';
VEHLIBStruct.table{n}.rappcvt.unit='';
VEHLIBStruct.table{n}.rappcvt.precision='%f';
VEHLIBStruct.table{n}.rappcvt.vector='rappcvt';

% 'VARIABLES MOTEUR THERMIQUE'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES MOTEUR THERMIQUE';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.cons_mt=struct;
VEHLIBStruct.table{n}.cons_mt.condition='true';
VEHLIBStruct.table{n}.cons_mt.name='cons_mt';
VEHLIBStruct.table{n}.cons_mt.longname='Consigne moteur thermique';
VEHLIBStruct.table{n}.cons_mt.type='double';
VEHLIBStruct.table{n}.cons_mt.unit='';
VEHLIBStruct.table{n}.cons_mt.precision='%f';
VEHLIBStruct.table{n}.cons_mt.vector='cons_mt';

VEHLIBStruct.table{n}.alim_mt=struct;
VEHLIBStruct.table{n}.alim_mt.condition='true';
VEHLIBStruct.table{n}.alim_mt.name='alim_mt';
VEHLIBStruct.table{n}.alim_mt.longname='Alimentation calculateur moteur thermique';
VEHLIBStruct.table{n}.alim_mt.type='double';
VEHLIBStruct.table{n}.alim_mt.unit='';
VEHLIBStruct.table{n}.alim_mt.precision='%f';
VEHLIBStruct.table{n}.alim_mt.vector='alim_mt';

VEHLIBStruct.table{n}.wmt=struct;
VEHLIBStruct.table{n}.wmt.condition='true';
VEHLIBStruct.table{n}.wmt.name='wmt';
VEHLIBStruct.table{n}.wmt.longname='Regime de rotation du moteur thermique';
VEHLIBStruct.table{n}.wmt.type='double';
VEHLIBStruct.table{n}.wmt.unit='rd/sec';
VEHLIBStruct.table{n}.wmt.precision='%f';
VEHLIBStruct.table{n}.wmt.vector='wmt';

VEHLIBStruct.table{n}.dwmt=struct;
VEHLIBStruct.table{n}.dwmt.condition='true';
VEHLIBStruct.table{n}.dwmt.name='dwmt';
VEHLIBStruct.table{n}.dwmt.longname='Derivee du regime de rotation du moteur thermique';
VEHLIBStruct.table{n}.dwmt.type='double';
VEHLIBStruct.table{n}.dwmt.unit='rd/s*s';
VEHLIBStruct.table{n}.dwmt.precision='%f';
VEHLIBStruct.table{n}.dwmt.vector='dwmt';

VEHLIBStruct.table{n}.cmt=struct;
VEHLIBStruct.table{n}.cmt.condition='true';
VEHLIBStruct.table{n}.cmt.name='cmt';
VEHLIBStruct.table{n}.cmt.longname='Couple sur l arbre du moteur thermique';
VEHLIBStruct.table{n}.cmt.type='double';
VEHLIBStruct.table{n}.cmt.unit='Nm';
VEHLIBStruct.table{n}.cmt.precision='%f';
VEHLIBStruct.table{n}.cmt.vector='cmt';

VEHLIBStruct.table{n}.cmt_ind=struct;
VEHLIBStruct.table{n}.cmt_ind.condition='true';
VEHLIBStruct.table{n}.cmt_ind.name='cmt_ind';
VEHLIBStruct.table{n}.cmt_ind.longname='Couple indique du moteur thermique';
VEHLIBStruct.table{n}.cmt_ind.type='double';
VEHLIBStruct.table{n}.cmt_ind.unit='Nm';
VEHLIBStruct.table{n}.cmt_ind.precision='%f';
VEHLIBStruct.table{n}.cmt_ind.vector='cmt_ind';

VEHLIBStruct.table{n}.cmt_opti=struct;
VEHLIBStruct.table{n}.cmt_opti.condition='true';
VEHLIBStruct.table{n}.cmt_opti.name='cmt_opti';
VEHLIBStruct.table{n}.cmt_opti.longname='Couple optimum sur l arbre du moteur thermique';
VEHLIBStruct.table{n}.cmt_opti.type='double';
VEHLIBStruct.table{n}.cmt_opti.unit='Nm';
VEHLIBStruct.table{n}.cmt_opti.precision='%f';
VEHLIBStruct.table{n}.cmt_opti.vector='cmt_opti';

VEHLIBStruct.table{n}.cmt_max=struct;
VEHLIBStruct.table{n}.cmt_max.condition='true';
VEHLIBStruct.table{n}.cmt_max.name='cmt_max';
VEHLIBStruct.table{n}.cmt_max.longname='Couple maximum sur l arbre du moteur thermique';
VEHLIBStruct.table{n}.cmt_max.type='double';
VEHLIBStruct.table{n}.cmt_max.unit='Nm';
VEHLIBStruct.table{n}.cmt_max.precision='%f';
VEHLIBStruct.table{n}.cmt_max.vector='cmt_max';

VEHLIBStruct.table{n}.P_meca_mt=struct;
VEHLIBStruct.table{n}.P_meca_mt.condition='true';
VEHLIBStruct.table{n}.P_meca_mt.name='P_meca_mt';
VEHLIBStruct.table{n}.P_meca_mt.longname='Puissance sur l arbre du moteur thermique';
VEHLIBStruct.table{n}.P_meca_mt.type='double';
VEHLIBStruct.table{n}.P_meca_mt.unit='W';
VEHLIBStruct.table{n}.P_meca_mt.precision='%f';
VEHLIBStruct.table{n}.P_meca_mt.vector='cmt.*wmt';

VEHLIBStruct.table{n}.E_meca_mt=struct;
VEHLIBStruct.table{n}.E_meca_mt.condition='true';
VEHLIBStruct.table{n}.E_meca_mt.name='E_meca_mt';
VEHLIBStruct.table{n}.E_meca_mt.longname='Energie sur l arbre du moteur thermique';
VEHLIBStruct.table{n}.E_meca_mt.type='double';
VEHLIBStruct.table{n}.E_meca_mt.unit='Ws';
VEHLIBStruct.table{n}.E_meca_mt.precision='%f';
VEHLIBStruct.table{n}.E_meca_mt.vector='cumtrapz(tsim,cmt.*wmt)';

VEHLIBStruct.table{n}.conso=struct;
VEHLIBStruct.table{n}.conso.condition='true';
VEHLIBStruct.table{n}.conso.name='conso';
VEHLIBStruct.table{n}.conso.longname='Consommation cumulee';
VEHLIBStruct.table{n}.conso.type='double';
VEHLIBStruct.table{n}.conso.unit='L';
VEHLIBStruct.table{n}.conso.precision='%f';
VEHLIBStruct.table{n}.conso.vector='conso';

VEHLIBStruct.table{n}.dcarb=struct;
VEHLIBStruct.table{n}.dcarb.condition='true';
VEHLIBStruct.table{n}.dcarb.name='dcarb';
VEHLIBStruct.table{n}.dcarb.longname='Debit carburant';
VEHLIBStruct.table{n}.dcarb.type='double';
VEHLIBStruct.table{n}.dcarb.unit='g/s';
VEHLIBStruct.table{n}.dcarb.precision='%f';
VEHLIBStruct.table{n}.dcarb.vector='dcarb';

VEHLIBStruct.table{n}.teau_mth=struct;
VEHLIBStruct.table{n}.teau_mth.condition='true';
VEHLIBStruct.table{n}.teau_mth.name='teau_mth';
VEHLIBStruct.table{n}.teau_mth.longname='Temperature d eau moteur';
VEHLIBStruct.table{n}.teau_mth.type='double';
VEHLIBStruct.table{n}.teau_mth.unit='deg C';
VEHLIBStruct.table{n}.teau_mth.precision='%f';
VEHLIBStruct.table{n}.teau_mth.vector='teau_mth';

VEHLIBStruct.table{n}.Tcatg=struct;
VEHLIBStruct.table{n}.Tcatg.condition='true';
VEHLIBStruct.table{n}.Tcatg.name='Tcatg';
VEHLIBStruct.table{n}.Tcatg.longname='Temperature moyenne gaz catalyseur';
VEHLIBStruct.table{n}.Tcatg.type='double';
VEHLIBStruct.table{n}.Tcatg.unit='deg K';
VEHLIBStruct.table{n}.Tcatg.precision='%f';
VEHLIBStruct.table{n}.Tcatg.vector='Tcatg';

VEHLIBStruct.table{n}.Tcat=struct;
VEHLIBStruct.table{n}.Tcat.condition='true';
VEHLIBStruct.table{n}.Tcat.name='Tcat';
VEHLIBStruct.table{n}.Tcat.longname='Temperature moyenne catalyseur';
VEHLIBStruct.table{n}.Tcat.type='double';
VEHLIBStruct.table{n}.Tcat.unit='deg K';
VEHLIBStruct.table{n}.Tcat.precision='%f';
VEHLIBStruct.table{n}.Tcat.vector='Tcat';

VEHLIBStruct.table{n}.Tcoll=struct;
VEHLIBStruct.table{n}.Tcoll.condition='true';
VEHLIBStruct.table{n}.Tcoll.name='Tcoll';
VEHLIBStruct.table{n}.Tcoll.longname='Temperature moyenne collecteur';
VEHLIBStruct.table{n}.Tcoll.type='double';
VEHLIBStruct.table{n}.Tcoll.unit='deg K';
VEHLIBStruct.table{n}.Tcoll.precision='%f';
VEHLIBStruct.table{n}.Tcoll.vector='Tcoll';

VEHLIBStruct.table{n}.Texh=struct;
VEHLIBStruct.table{n}.Texh.condition='true';
VEHLIBStruct.table{n}.Texh.name='Texh';
VEHLIBStruct.table{n}.Texh.longname='Temperature des gaz d echappement';
VEHLIBStruct.table{n}.Texh.type='double';
VEHLIBStruct.table{n}.Texh.unit='deg K';
VEHLIBStruct.table{n}.Texh.precision='%f';
VEHLIBStruct.table{n}.Texh.vector='Texh';

VEHLIBStruct.table{n}.co_eff=struct;
VEHLIBStruct.table{n}.co_eff.condition='true';
VEHLIBStruct.table{n}.co_eff.name='co_eff';
VEHLIBStruct.table{n}.co_eff.longname='Efficacite de conversion des CO';
VEHLIBStruct.table{n}.co_eff.type='double';
VEHLIBStruct.table{n}.co_eff.unit='%';
VEHLIBStruct.table{n}.co_eff.precision='%f';
VEHLIBStruct.table{n}.co_eff.vector='co_eff';

VEHLIBStruct.table{n}.hc_eff=struct;
VEHLIBStruct.table{n}.hc_eff.condition='true';
VEHLIBStruct.table{n}.hc_eff.name='hc_eff';
VEHLIBStruct.table{n}.hc_eff.longname='Efficacite de conversion des HC';
VEHLIBStruct.table{n}.hc_eff.type='double';
VEHLIBStruct.table{n}.hc_eff.unit='%';
VEHLIBStruct.table{n}.hc_eff.precision='%f';
VEHLIBStruct.table{n}.hc_eff.vector='hc_eff';

VEHLIBStruct.table{n}.nox_eff=struct;
VEHLIBStruct.table{n}.nox_eff.condition='true';
VEHLIBStruct.table{n}.nox_eff.name='nox_eff';
VEHLIBStruct.table{n}.nox_eff.longname='Efficacite de conversion des NOx';
VEHLIBStruct.table{n}.nox_eff.type='double';
VEHLIBStruct.table{n}.nox_eff.unit='%';
VEHLIBStruct.table{n}.nox_eff.precision='%f';
VEHLIBStruct.table{n}.nox_eff.vector='nox_eff';

VEHLIBStruct.table{n}.co=struct;
VEHLIBStruct.table{n}.co.condition='true';
VEHLIBStruct.table{n}.co.name='co';
VEHLIBStruct.table{n}.co.longname='Emission de CO';
VEHLIBStruct.table{n}.co.type='double';
VEHLIBStruct.table{n}.co.unit='g/s';
VEHLIBStruct.table{n}.co.precision='%f';
VEHLIBStruct.table{n}.co.vector='co';

VEHLIBStruct.table{n}.hc=struct;
VEHLIBStruct.table{n}.hc.condition='true';
VEHLIBStruct.table{n}.hc.name='hc';
VEHLIBStruct.table{n}.hc.longname='Emission de HC';
VEHLIBStruct.table{n}.hc.type='double';
VEHLIBStruct.table{n}.hc.unit='g/s';
VEHLIBStruct.table{n}.hc.precision='%f';
VEHLIBStruct.table{n}.hc.vector='hc';

VEHLIBStruct.table{n}.nox=struct;
VEHLIBStruct.table{n}.nox.condition='true';
VEHLIBStruct.table{n}.nox.name='nox';
VEHLIBStruct.table{n}.nox.longname='Emission de NOx';
VEHLIBStruct.table{n}.nox.type='double';
VEHLIBStruct.table{n}.nox.unit='g/s';
VEHLIBStruct.table{n}.nox.precision='%f';
VEHLIBStruct.table{n}.nox.vector='nox';

VEHLIBStruct.table{n}.cocum=struct;
VEHLIBStruct.table{n}.cocum.condition='true';
VEHLIBStruct.table{n}.cocum.name='cocum';
VEHLIBStruct.table{n}.cocum.longname='Emission de CO cumule';
VEHLIBStruct.table{n}.cocum.type='double';
VEHLIBStruct.table{n}.cocum.unit='g';
VEHLIBStruct.table{n}.cocum.precision='%f';
VEHLIBStruct.table{n}.cocum.vector='cumtrapz(tsim,co)';

VEHLIBStruct.table{n}.hccum=struct;
VEHLIBStruct.table{n}.hccum.condition='true';
VEHLIBStruct.table{n}.hccum.name='hccum';
VEHLIBStruct.table{n}.hccum.longname='Emission de HC cumule';
VEHLIBStruct.table{n}.hccum.type='double';
VEHLIBStruct.table{n}.hccum.unit='g';
VEHLIBStruct.table{n}.hccum.precision='%f';
VEHLIBStruct.table{n}.hccum.vector='cumtrapz(tsim,hc)';

VEHLIBStruct.table{n}.noxcum=struct;
VEHLIBStruct.table{n}.noxcum.condition='true';
VEHLIBStruct.table{n}.noxcum.name='noxcum';
VEHLIBStruct.table{n}.noxcum.longname='Emission de NOx cumule';
VEHLIBStruct.table{n}.noxcum.type='double';
VEHLIBStruct.table{n}.noxcum.unit='g';
VEHLIBStruct.table{n}.noxcum.precision='%f';
VEHLIBStruct.table{n}.noxcum.vector='cumtrapz(tsim,nox)';
% 'VARIABLES PANTOGRAPHE'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES PANTOGRAPHE';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

VEHLIBStruct.table{n}.upanto=struct;
VEHLIBStruct.table{n}.upanto.condition='true';
VEHLIBStruct.table{n}.upanto.name='upanto';
VEHLIBStruct.table{n}.upanto.longname='Tension reseau pantographe';
VEHLIBStruct.table{n}.upanto.type='double';
VEHLIBStruct.table{n}.upanto.unit='V';
VEHLIBStruct.table{n}.upanto.precision='%f';
VEHLIBStruct.table{n}.upanto.vector='upanto';

VEHLIBStruct.table{n}.idanto=struct;
VEHLIBStruct.table{n}.idanto.condition='true';
VEHLIBStruct.table{n}.idanto.name='ipanto';
VEHLIBStruct.table{n}.idanto.longname='Courant reseau pantographe';
VEHLIBStruct.table{n}.idanto.type='double';
VEHLIBStruct.table{n}.idanto.unit='A';
VEHLIBStruct.table{n}.idanto.precision='%f';
VEHLIBStruct.table{n}.idanto.vector='ipanto';

VEHLIBStruct.table{n}.ppanto=struct;
VEHLIBStruct.table{n}.ppanto.condition='true';
VEHLIBStruct.table{n}.ppanto.name='ppanto';
VEHLIBStruct.table{n}.ppanto.longname='Puissance pantographe';
VEHLIBStruct.table{n}.ppanto.type='double';
VEHLIBStruct.table{n}.ppanto.unit='W';
VEHLIBStruct.table{n}.ppanto.precision='%f';
VEHLIBStruct.table{n}.ppanto.vector='ppanto';

VEHLIBStruct.table{n}.E_panto=struct;
VEHLIBStruct.table{n}.E_panto.condition='true';
VEHLIBStruct.table{n}.E_panto.name='E_panto';
VEHLIBStruct.table{n}.E_panto.longname='Energie pantographe';
VEHLIBStruct.table{n}.E_panto.type='double';
VEHLIBStruct.table{n}.E_panto.unit='Ws';
VEHLIBStruct.table{n}.E_panto.precision='%f';
VEHLIBStruct.table{n}.E_panto.vector='cumtrapz(tsim,ppanto)';

VEHLIBStruct.table{n}.uconv=struct;
VEHLIBStruct.table{n}.uconv.condition='true';
VEHLIBStruct.table{n}.uconv.name='uconv';
VEHLIBStruct.table{n}.uconv.longname='Tension sortie convertisseur';
VEHLIBStruct.table{n}.uconv.type='double';
VEHLIBStruct.table{n}.uconv.unit='V';
VEHLIBStruct.table{n}.uconv.precision='%f';
VEHLIBStruct.table{n}.uconv.vector='uconv';

VEHLIBStruct.table{n}.iconv=struct;
VEHLIBStruct.table{n}.iconv.condition='true';
VEHLIBStruct.table{n}.iconv.name='iconv';
VEHLIBStruct.table{n}.iconv.longname='Courant sortie convertisseur';
VEHLIBStruct.table{n}.iconv.type='double';
VEHLIBStruct.table{n}.iconv.unit='A';
VEHLIBStruct.table{n}.iconv.precision='%f';
VEHLIBStruct.table{n}.iconv.vector='iconv';

VEHLIBStruct.table{n}.pconv=struct;
VEHLIBStruct.table{n}.pconv.condition='true';
VEHLIBStruct.table{n}.pconv.name='pconv';
VEHLIBStruct.table{n}.pconv.longname='Puissance sortie convertisseur';
VEHLIBStruct.table{n}.pconv.type='double';
VEHLIBStruct.table{n}.pconv.unit='W';
VEHLIBStruct.table{n}.pconv.precision='%f';
VEHLIBStruct.table{n}.pconv.vector='uconv.*iconv';

% 'VARIABLES BATTERIE BASSE TENSION'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES BATTERIE BASSE TENSION';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.ibat_bt=struct;
VEHLIBStruct.table{n}.ibat_bt.condition='true';
VEHLIBStruct.table{n}.ibat_bt.name='ibat_bt';
VEHLIBStruct.table{n}.ibat_bt.longname='Courant batterie BT';
VEHLIBStruct.table{n}.ibat_bt.type='double';
VEHLIBStruct.table{n}.ibat_bt.unit='A';
VEHLIBStruct.table{n}.ibat_bt.precision='%f';
VEHLIBStruct.table{n}.ibat_bt.vector='ibat_bt';

VEHLIBStruct.table{n}.ubat_bt=struct;
VEHLIBStruct.table{n}.ubat_bt.condition='true';
VEHLIBStruct.table{n}.ubat_bt.name='ubat_bt';
VEHLIBStruct.table{n}.ubat_bt.longname='Tension batterie BT';
VEHLIBStruct.table{n}.ubat_bt.type='double';
VEHLIBStruct.table{n}.ubat_bt.unit='V';
VEHLIBStruct.table{n}.ubat_bt.precision='%f';
VEHLIBStruct.table{n}.ubat_bt.vector='ubat_bt';

VEHLIBStruct.table{n}.P_bat_bt=struct;
VEHLIBStruct.table{n}.P_bat_bt.condition='true';
VEHLIBStruct.table{n}.P_bat_bt.name='P_bat_bt';
VEHLIBStruct.table{n}.P_bat_bt.longname='Puissance batterie BT';
VEHLIBStruct.table{n}.P_bat_bt.type='double';
VEHLIBStruct.table{n}.P_bat_bt.unit='W';
VEHLIBStruct.table{n}.P_bat_bt.precision='%f';
VEHLIBStruct.table{n}.P_bat_bt.vector='ubat_bt.*ibat_bt';

VEHLIBStruct.table{n}.E_bat_bt=struct;
VEHLIBStruct.table{n}.E_bat_bt.condition='true';
VEHLIBStruct.table{n}.E_bat_bt.name='E_bat_bt';
VEHLIBStruct.table{n}.E_bat_bt.longname='Enerige pack batterie BT';
VEHLIBStruct.table{n}.E_bat_bt.type='double';
VEHLIBStruct.table{n}.E_bat_bt.unit='Ws';
VEHLIBStruct.table{n}.E_bat_bt.precision='%f';
VEHLIBStruct.table{n}.E_bat_bt.vector='cumtrapz(tsim,ubat_bt.*ibat_bt)';

VEHLIBStruct.table{n}.dod_bt=struct;
VEHLIBStruct.table{n}.dod_bt.condition='true';
VEHLIBStruct.table{n}.dod_bt.name='dod_bt';
VEHLIBStruct.table{n}.dod_bt.longname='Profondeur de decharge de la batterie BT';
VEHLIBStruct.table{n}.dod_bt.type='double';
VEHLIBStruct.table{n}.dod_bt.unit='%';
VEHLIBStruct.table{n}.dod_bt.precision='%f';
VEHLIBStruct.table{n}.dod_bt.vector='dod_bt';

VEHLIBStruct.table{n}.ibat_bt_mono=struct;
VEHLIBStruct.table{n}.ibat_bt_mono.condition='true';
VEHLIBStruct.table{n}.ibat_bt_mono.name='ibat_bt_mono';
VEHLIBStruct.table{n}.ibat_bt_mono.longname='Courant monobloc batterie BT';
VEHLIBStruct.table{n}.ibat_bt_mono.type='double';
VEHLIBStruct.table{n}.ibat_bt_mono.unit='A';
VEHLIBStruct.table{n}.ibat_bt_mono.precision='%f';
VEHLIBStruct.table{n}.ibat_bt_mono.vector='ibat_bt/VD.BATT_BT.Nbranchepar';

VEHLIBStruct.table{n}.ubat_bt_mono=struct;
VEHLIBStruct.table{n}.ubat_bt_mono.condition='true';
VEHLIBStruct.table{n}.ubat_bt_mono.name='ubat_bt_mono';
VEHLIBStruct.table{n}.ubat_bt_mono.longname='Tension monobloc batterie BT';
VEHLIBStruct.table{n}.ubat_bt_mono.type='double';
VEHLIBStruct.table{n}.ubat_bt_mono.unit='V';
VEHLIBStruct.table{n}.ubat_bt_mono.precision='%f';
VEHLIBStruct.table{n}.ubat_bt_mono.vector='ubat_bt/VD.BATT_BT.Nblocser';

VEHLIBStruct.table{n}.P_bat_bt_mono=struct;
VEHLIBStruct.table{n}.P_bat_bt_mono.condition='true';
VEHLIBStruct.table{n}.P_bat_bt_mono.name='P_bat_bt_mono';
VEHLIBStruct.table{n}.P_bat_bt_mono.longname='Puissance monobloc batterie BT';
VEHLIBStruct.table{n}.P_bat_bt_mono.type='double';
VEHLIBStruct.table{n}.P_bat_bt_mono.unit='W';
VEHLIBStruct.table{n}.P_bat_bt_mono.precision='%f';
VEHLIBStruct.table{n}.P_bat_bt_mono.vector='ubat_bt.*ibat_bt/(VD.BATT_BT.Nblocser*VD.BATT_BT.Nbranchepar)';

VEHLIBStruct.table{n}.E_bat_bt_mono=struct;
VEHLIBStruct.table{n}.E_bat_bt_mono.condition='true';
VEHLIBStruct.table{n}.E_bat_bt_mono.name='E_bat_bt_mono';
VEHLIBStruct.table{n}.E_bat_bt_mono.longname='Puissance monobloc batterie BT';
VEHLIBStruct.table{n}.E_bat_bt_mono.type='double';
VEHLIBStruct.table{n}.E_bat_bt_mono.unit='Ws';
VEHLIBStruct.table{n}.E_bat_bt_mono.precision='%f';
VEHLIBStruct.table{n}.E_bat_bt_mono.vector='cumtrapz(tsim,ubat_bt.*ibat_bt/(VD.BATT_BT.Nblocser*VD.BATT_BT.Nbranchepar))';

VEHLIBStruct.table{n}.flag_stop_bat_bt=struct;
VEHLIBStruct.table{n}.flag_stop_bat_bt.condition='true';
VEHLIBStruct.table{n}.flag_stop_bat_bt.name='flag_stop_bat_bt';
VEHLIBStruct.table{n}.flag_stop_bat_bt.longname='Flag arret tension batterie BT';
VEHLIBStruct.table{n}.flag_stop_bat_bt.type='double';
VEHLIBStruct.table{n}.flag_stop_bat_bt.unit='';
VEHLIBStruct.table{n}.flag_stop_bat_bt.precision='%f';
VEHLIBStruct.table{n}.flag_stop_bat_bt.vector='flag_stop_bat_bt';

% 'VARIABLES DCDC_1'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES DCDC_1';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.isurv_ht=struct;
VEHLIBStruct.table{n}.isurv_ht.condition='true';
VEHLIBStruct.table{n}.isurv_ht.name='isurv_ht';
VEHLIBStruct.table{n}.isurv_ht.longname='Courant VD.DCDC_1 cote HT';
VEHLIBStruct.table{n}.isurv_ht.type='double';
VEHLIBStruct.table{n}.isurv_ht.unit='A';
VEHLIBStruct.table{n}.isurv_ht.precision='%f';
VEHLIBStruct.table{n}.isurv_ht.vector='isurv_ht';

VEHLIBStruct.table{n}.usurv_ht=struct;
VEHLIBStruct.table{n}.usurv_ht.condition='true';
VEHLIBStruct.table{n}.usurv_ht.name='usurv_ht';
VEHLIBStruct.table{n}.usurv_ht.longname='Tension VD.DCDC_1 cote HT';
VEHLIBStruct.table{n}.usurv_ht.type='double';
VEHLIBStruct.table{n}.usurv_ht.unit='V';
VEHLIBStruct.table{n}.usurv_ht.precision='%f';
VEHLIBStruct.table{n}.usurv_ht.vector='usurv_ht';

VEHLIBStruct.table{n}.rcy_dcdc=struct;
VEHLIBStruct.table{n}.rcy_dcdc.condition='true';
VEHLIBStruct.table{n}.rcy_dcdc.name='rcy_dcdc';
VEHLIBStruct.table{n}.rcy_dcdc.longname='Rapport cyclique VD.DCDC_1 cote HT';
VEHLIBStruct.table{n}.rcy_dcdc.type='double';
VEHLIBStruct.table{n}.rcy_dcdc.unit='';
VEHLIBStruct.table{n}.rcy_dcdc.precision='%f';
VEHLIBStruct.table{n}.rcy_dcdc.vector='rcy_dcdc';

% 'VARIABLES DCDC_2'

n=length(VEHLIBStruct.table)+1;

% id et metatable
VEHLIBStruct.table{n}.id=num2str(n);

VEHLIBStruct.table{n}.metatable=struct;
VEHLIBStruct.table{n}.metatable.name='VARIABLES DCDC_2';
VEHLIBStruct.table{n}.metatable.date='date';
VEHLIBStruct.table{n}.metatable.sourcefile='source';
VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';

% variables
VEHLIBStruct.table{n}.isurv2_ht=struct;
VEHLIBStruct.table{n}.isurv2_ht.condition='true';
VEHLIBStruct.table{n}.isurv2_ht.name='isurv2_ht';
VEHLIBStruct.table{n}.isurv2_ht.longname='Courant VD.DCDC_2 cote HT';
VEHLIBStruct.table{n}.isurv2_ht.type='double';
VEHLIBStruct.table{n}.isurv2_ht.unit='A';
VEHLIBStruct.table{n}.isurv2_ht.precision='%f';
VEHLIBStruct.table{n}.isurv2_ht.vector='isurv2_ht';

VEHLIBStruct.table{n}.usurv2_ht=struct;
VEHLIBStruct.table{n}.usurv2_ht.condition='true';
VEHLIBStruct.table{n}.usurv2_ht.name='usurv2_ht';
VEHLIBStruct.table{n}.usurv2_ht.longname='Tension VD.DCDC_2 cote HT';
VEHLIBStruct.table{n}.usurv2_ht.type='double';
VEHLIBStruct.table{n}.usurv2_ht.unit='V';
VEHLIBStruct.table{n}.usurv2_ht.precision='%f';
VEHLIBStruct.table{n}.usurv2_ht.vector='usurv2_ht';

VEHLIBStruct.table{n}.rcy_dcdc2=struct;
VEHLIBStruct.table{n}.rcy_dcdc2.condition='true';
VEHLIBStruct.table{n}.rcy_dcdc2.name='rcy_dcdc2';
VEHLIBStruct.table{n}.rcy_dcdc2.longname='Rapport cyclique VD.DCDC_2 cote HT';
VEHLIBStruct.table{n}.rcy_dcdc2.type='double';
VEHLIBStruct.table{n}.rcy_dcdc2.unit='';
VEHLIBStruct.table{n}.rcy_dcdc2.precision='%f';
VEHLIBStruct.table{n}.rcy_dcdc2.vector='rcy_dcdc2';

% Recherche des variables a ajouter dans les projets utilisateurs: on
% recherche des fichiers nommes "StructureType4VEHLIB_monProjet" sous le
% dossier utilisateurs/monProjet/utilitaires
initdata;
rep=lsFiles(INIT.UsersFolder,'',true, true);
P=path;

for ind=1:length(rep)
    [~,dossier]=fileparts(rep{ind});
    if ~strcmp(dossier,'templates')        % le repertoire templates est un template !
        file=strcat('StructureType4VEHLIB_',dossier,'.m');
        if exist(fullfile(rep{ind},'Utilities',file),'file')
            try
                chaine=['[VEHLIBStruct, comp, tabl] =',file(1:length(file)-2),'(VEHLIBStruct, comp, tabl);'];
                eval(chaine);
            catch ME
                fprintf(1,'%s\n',['Probleme a l execution de : ',file]);
            end
        end
    end
end

clear n

end

function NewChaine = parseExpression(chaine,resStructName)

var = regexp(chaine,'\s*(\w+)\(\s*(\w+)\s*,\s*(\w+)\s*\.\*\s*(\w+)\s*\)','tokens'); % cumtrapz(tsim,cmt.*wmt) \s* 0 ou plusieurs espaces
if isempty(var)
    var = regexp(chaine,'\s*(\w+)\(\s*(\w+)\s*,\s*(\w+)\s*\)','tokens'); % cumtrapz(tsim,pveh);
    if isempty(var)
        var = regexp(chaine,'\s*\.\*\s*','split'); % cmt.*wmt
        if length(var) == 1
            var = regexp(chaine,'\s*\+\s*','split'); % a+b+c
            NewChaine=[resStructName,'.',var{1}];
            for j = 2:length(var)
                NewChaine=[NewChaine,'+',resStructName,'.',var{j}];
            end
        else
            NewChaine=[resStructName,'.',var{1}];
            for j = 2:length(var)
                NewChaine=[NewChaine,'.*',resStructName,'.',var{j}];
            end
        end
    else
        var = var{1}; %extrait les chaines depuis le cell array
        NewChaine = strcat(var{1},'(',resStructName,'.',var{2},',',resStructName,'.',var{3},')');
    end
else
    var = var{1}; %extrait les chaines depuis le cell array
    NewChaine = strcat(var{1},'(',resStructName,'.',var{2},',',resStructName,'.',var{3},'.*',resStructName,'.',var{4},')');
end

% chaine = 'cumtrapz(tsim,c_acm1.*w_acm1)';
% resStructName = 'r';
% test_parseExp(chaine, resStructName)
end

