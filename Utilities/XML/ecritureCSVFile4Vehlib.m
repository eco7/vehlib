
% Function: ecritureCSVFile4Vehlib
% Fonction d'ecriture de donnees CSV format VEH
%
% Arguments d'appel:
%
% Struct = structure de donnees compatible avec le format XMLVEH
% filename : nom du fichier CSV
% pathname : repertoire de stockage du fichier
%
% Exemple de lancement:
% (start code)
% ecritureCSVFile4Vehlib(Struct,'myFile.xml','d:\temp\');
% (end)
%
% Auteur: BJ
%
% Date de creation: Juillet 2015
% ------------------------------------------------------
function ecritureCSVFile4Vehlib(Struct,filename,pathname,vars,freq)

if nargin == 4
    freq=1;
end

fprintf('Ecriture de fichier ''%s''\n',filename);
if ~exist('pathname','var')
    pathname='';
end
fullFileName = fullfile(pathname,filename);
filename2=regexp(fullFileName,filesep,'split');
filename2 = filename2{end};
if ~strcmp(filename,filename2)
    %Construct an MException object to represent the error.
    err = MException('mat2xml2b:notValidFileName', ...
        'Filename must not contain file separators (%s): %s',filesep,filename);
    %Throw the exception to stop execution and display an error message.
    throw(err)
end
fid=fopen(fullFileName,'w+');
if fid == -1
    %Construct an MException object to represent the error.
    err = MException('mat2xml2b:fileNotWritable', ...
        'Write error: %s',filename);
    %Throw the exception to stop execution and display an error message.
    throw(err)
end
[Struct, err, errS] = verifFomatXML4Vehlib(Struct);
% err = 0;
if err < 0
    fclose(fid);
    delete(fullFileName);%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%DELICAT (permission fichiers...?)
    %Construct an MException object to represent the error.
    err = MException('mat2xml2b:verifFomatXMLVEH', ...
        'Incompatible structure, error code: %d',err);
    %Throw the exception to stop execution and display an error message.
    display(errS)
    throw(err)
end

mat=[];

for j=1:length(vars)
    mat=[mat; XmlValue(Struct,vars{j})'];
end

mat=mat(:,1:freq:end);

fmt=[repmat('%s\t',1,length(vars))];
fmt=[fmt,'\n'];
fprintf(fid,fmt,vars{:});

fmt=[repmat('%f\t',1,length(vars))];
fmt=[fmt,'\n'];

fprintf(fid,fmt,mat);

fclose(fid);

