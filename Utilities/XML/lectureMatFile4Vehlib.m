% Function: [matStruct, ERR]=lectureMatFile4Vehlib(param,matStruct)
% Fonction de lecture d'un mat-file dans explgr
%
%  C Copyright IFSTTAR LTE 1999-2011
%
% Objet:
%
% Lecture d un fichier mat file au format du controlDesk Expt
%
% et renseignement d'une structure compatible avec les specification xml de VEHLIB
%
% Lecture / ecriture de donnees en format XML
% http://lx-veh2/mediawiki/index.php/Format_XML
%
% Arguments d'appel:
%
% param.FileName : nom du fichier ascii
% param.PathName : repertoire de stockage du fichier
% param.syncVoies: nom de la voie de synchronisatiohn (optionnel)
% param.syncSeuil : seuil de syncronisation (optionnel par defaut 0.5)
% param.SousEch : parametre pour sous echantillonner les sorties
% matStruct : argument optionnel. A utiliser si l'on veut rajouter une
% table dans une structure existante. Dans ce cas, le programme conserve la
% partie header de la structure d'origine.
%
% Arguments de retour:
%
% matStruct : structure matlab conforme aux specification xml pour vehlib
%
%  -----------------------------------------------------------
function [matStruct, ERR]=lectureMatFile4Vehlib(param,matStruct)
ERR=struct([]);

if ~isfield(param,'FileName')
     %Construct an MException object to represent the error.
    ERR = MException('lectureMatFile4Vehlib:lectureMatFile4Vehlib', ...
        'FileName is not a field of param structure');
    %Throw the exception to stop execution and display an error message.
    %throw(ERR)
end
if ~isfield(param,'PathName')
     %Construct an MException object to represent the error.
    ERR = MException('lectureMatFile4Vehlib:lectureMatFile4Vehlib', ...
        'PathName is not a field of param structure');
    %Throw the exception to stop execution and display an error message.
    %throw(ERR)
end
if ~isfield(param,'SousEch')
    param.SousEch=1;
end

FileName=param.FileName;
PathName=param.PathName;

if nargin==2
    % l'utilisateur veut rajouter une table
    [matStruct, err] = verifFomatXML4Vehlib(matStruct);
    if err < 0
        %Construct an MException object to represent the error.
        ERR = MException('lectureMatFile4Vehlib:verifFomatXMLVEH', ...
            'Incompatible structure, error code: %d',err);
        %Throw the exception to stop execution and display an error message.
        %throw(ERR)
    end
    indice=length(matStruct.table)+1;
else
    indice=1;
end

% Version du script de lecture
matVersion='0.1';

fprintf(1,'%s%s\n','Lecture du fichier mat    : ',FileName);

d={''};
try
    eval(['load ''',fullfile(PathName,FileName),''';']);
catch
    %Construct an MException object to represent the error.
    err = MException('lectureAscii4Vehlib:unableToLoadFile', ...
        'Unable to load file: %s',fullfile(PathName,FileName));
    %Throw the exception to stop execution and display an error message.
    throw(err)
end

[~,structName,~]=fileparts(FileName);
% if length(structName)>31
%     % le nom de la structure est limite a 31 char
%     structName=structName(1:31);
% end
% le nom de structure est en minuscule dans CDeskExpt
if exist(['',lower(structName),''],'var')
    structName=lower(structName);
end  
if exist(['',structName,''],'var')
    % Identification du type de fichier
    if isfield(eval(structName),'Description') && isfield(eval([structName,'.Description']),'GeneratorVersion')
        % Format des fichiers control desk 3.7
        fileType=eval([structName,'.Description.GeneratorVersion']);
        versionCDesk=3;
    elseif isfield(eval(structName),'Info') && isfield(eval([structName,'.Info']),'RevisionMajor')
        % Format des fchiers control desk NG 4.X
        fileType=['ControlDeskNG_Rev',num2str(eval([structName,'.Info.RevisionMajor']))];
        versionCDesk=4;
    end

elseif exist('JOURNEE','var')
    % Format des fichiers mat du projet EQUILIBRE
    fileType='EQUILIBRE';
elseif exist('trace_x','var')
    'A gerer'
    fileType='ControlDesk old format'
    if ~exist(matStruct,'var')
        matStruct = struct;
    end
    return
else
    'A gerer'
    % matFile quelconque
    eval(['load ',fullfile(PathName,FileName),';']);
    d=who;
    fileType='matfile_quelconque';
    if ~exist(matStruct,'var')
        matStruct = struct;
    end
    return
end

if indice==1
    % La structure et le head n'existe pas.
    % Renseignement de la structure
    matStruct = struct;

    % Head
    matStruct.head = struct;
    matStruct.head.version=matVersion;
    matStruct.head.date=date;
    matStruct.head.project='';
    matStruct.head.comments=['Created by lectureMat4Vehlib, version : ',matStruct.head.version];
    matStruct.head.type=fileType;

end

if indice==1
    % Table Numero indice
    matStruct.table=cell(1);
end

matStruct.table{indice}=struct;
matStruct.table{indice}.id=num2str(indice);

%table.metatable
matStruct.table{indice}.metatable=struct;
matStruct.table{indice}.metatable.name='';
matStruct.table{indice}.metatable.date=date;
matStruct.table{indice}.metatable.sourcefile=FileName;
matStruct.table{indice}.metatable.comments='';

% table{indice}.variable
if exist('versionCDesk','var') && (versionCDesk==3 || versionCDesk==4)
    
    % Syncronisation du fichier sur une voie si necessaire
    if(exist('param','var') && isfield(param,'syncVoies') )
        if ~isfield(param,'syncSeuil')
            param.syncSeuil=0.5;
        end
        % Synchronisation du fichier avec la voie param.syncVoies
        for ind=1:length(eval([structName,'.Y']))
            if versionCDesk==3
                variableName=eval(strcat(structName,'.Y(',num2str(ind),').Name'));
            elseif versionCDesk==4
                variableName=eval(strcat(structName,'.Y(',num2str(ind),').Path'));
                if contains(variableName,'Labels')
                    variableName=strcat(variableName,eval(strcat(structName,'.Y(',num2str(ind),').Name')));
                end
                variableName=strrep(variableName,'Labels','');
            end
            if strfind(variableName,param.syncVoies)
                break
            end
            if ind == length(eval([structName,'.Y']))
                disp('Syncronization error. Signal not found');
                % Pas de syncro
                indice_sync=1;
            end
        end
        indice_sync=find(double(eval(strcat(structName,'.Y(',num2str(ind),').Data')))>=param.syncSeuil);
        
        if isempty(indice_sync)
            ERR = MException('lecture_MatFile4Vehlib:pb',['Probleme de synchronisation des voies fichier mat : ',param.fileName]);
            errordlg(['Probleme de synchronisation des voies du fichier mat : ',param.fileName],'lectureMatFile4Vehlib','modal');
            throw(ERR)
        end
        indice_sync=indice_sync(1);
    else
        % Pas de syncro
        indice_sync=1;
    end
    
    nomvar='Time_dsp';
    matStruct.table{indice}.(nomvar)=struct;
    matStruct.table{indice}.(nomvar).name=nomvar;
    matStruct.table{indice}.(nomvar).longname=nomvar;
    matStruct.table{indice}.(nomvar).type='double';
    matStruct.table{indice}.(nomvar).unit='';
    matStruct.table{indice}.(nomvar).precision='%f';
    indice1=length(eval(strcat(structName,'.X(1).Data')));
    if length(eval(strcat(structName,'.X')))~=1
        for jkl=1:length(eval(strcat(structName,'.X')))
            if indice1 ~=  length(eval(strcat(structName,'.X(',num2str(jkl),').Data')))
                % Soit il y a plusieurs frequences d'acquisition : lectureMatFile4Vehlib ne sais pas faire,
                % Soit le flight recorder de dSPACE a enregistre des vecteurs de longueurs differentes !
                indice1=min(indice1,length(eval(strcat(structName,'.X(',num2str(jkl),').Data'))));
            end
        end
    end
    if length(eval(strcat(structName,'.X')))==0
        ERR = MException('lectureMatFile4Vehlib:lectureMatFile4Vehlib', ...
            'Le fichier ne contient pas de donnees');
        return
    end
    
    matStruct.table{indice}.(nomvar).vector=double(eval(strcat(structName,'.X(1).Data(',num2str(indice_sync),':',num2str(param.SousEch),':',num2str(indice1),')')))- ...
        double(eval(strcat(structName,'.X(1).Data(',num2str(indice_sync),')')));
    
    for ind=1:length(eval([structName,'.Y']))
        if versionCDesk==3
            variableName=eval(strcat(structName,'.Y(',num2str(ind),').Name'));
            if length(variableName)>namelengthmax
                variableName=variableName(length(variableName)-63:length(variableName));
            end
            variableName=strrep(variableName,'"','');
            variableName=strrep(variableName,'/','_');
            variableName=strrep(variableName,'{','');
            variableName=strrep(variableName,'}','');
            variableName=strrep(variableName,'\n','');
            variableName=strrep(variableName,'In1','');
            variableName=strrep(variableName,'.','');
            variableName=strrep(variableName,'Occurence1','');
            variableName=regexprep(variableName,'\s','');
            variableName = genvarname(variableName);
        elseif versionCDesk==4
            variableName=eval(strcat(structName,'.Y(',num2str(ind),').Path'));
            if isempty(strfind(eval(strcat(structName,'.Y(',num2str(ind),').Name')),'Out')) & ...
               isempty(strfind(eval(strcat(structName,'.Y(',num2str(ind),').Name')),'In'))
                variableName=strcat(variableName,eval(strcat(structName,'.Y(',num2str(ind),').Name')));
            end
            if length(variableName)>namelengthmax
                variableName=variableName(length(variableName)-63:length(variableName));
            end
            s=regexp(variableName,'/','split');
            variableName=s{end};
            variableName=strrep(variableName,'Model Root/','');
            variableName=strrep(variableName,'/','_');
            variableName=strrep(variableName,'"','');
            variableName=strrep(variableName,'+','');
            variableName=strrep(variableName,'\n','');
            variableName=strrep(variableName,'In1','');
            variableName=strrep(variableName,'(','');
            variableName=strrep(variableName,')','');
            variableName=strrep(variableName,'<','');
            variableName=strrep(variableName,'>','');
            variableName=strrep(variableName,'.','');
            variableName=strrep(variableName,'Occurence1','');
            variableName=strrep(variableName,'Labels','');

            variableName=regexprep(variableName,'\s','');
            variableName = genvarname(variableName);
        end

        matStruct.table{indice}.(variableName)=struct;
        matStruct.table{indice}.(variableName).name=variableName;
        matStruct.table{indice}.(variableName).longname=variableName;
        matStruct.table{indice}.(variableName).type='double';
        matStruct.table{indice}.(variableName).unit='';
        matStruct.table{indice}.(variableName).precision='%f';
        
        matStruct.table{indice}.(variableName).vector=double(eval(strcat(structName,'.Y(',num2str(ind),').Data(',num2str(indice_sync),':',num2str(param.SousEch),':',num2str(indice1),')')));
    end
elseif strcmpi(fileType,'EQUILIBRE')
    listeVar=fieldnames(JOURNEE);
    for nvar=1:length(listeVar)
        variableName=listeVar{nvar};
        matStruct.table{indice}.(variableName)=struct;
        matStruct.table{indice}.(variableName).name=variableName;
        matStruct.table{indice}.(variableName).longname=variableName;
        matStruct.table{indice}.(variableName).type='double';
        matStruct.table{indice}.(variableName).unit='';
        matStruct.table{indice}.(variableName).precision='%f';
        matStruct.table{indice}.(variableName).vector=double(JOURNEE.(variableName));
end

[matStruct, err] = verifFomatXML4Vehlib(matStruct);
if err < 0
    %Construct an MException object to represent the error.
    ERR = MException('lectureMatFile4Vehlib:verifFomatXMLVEH', ...
        'Incompatible structure, error code: %d',err);
    %Throw the exception to stop execution and display an error message.
    %throw(err)
end    

end

% ------------------------------------------------------
