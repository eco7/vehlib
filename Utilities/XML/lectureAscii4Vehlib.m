% Function: lectureAscii4Vehlib
% Fonction de lecture d un fichier ascii avec #DEBUT et banniere
%
%  C Copyright IFSTTAR LTE 1999-2011
%
% Objet:
%
% Lecture d un fichier ascii avec #DEBUT et banniere
%
% separateur de nom de variable: espaces
%
% et renseignement d'une structure compatible avec les specification xml de VEHLIB
%
% Lecture / ecriture de donnees en format XML
% http://lx-veh2/mediawiki/index.php/Format_XML
%
% Arguments d'appel:
%
% param.FileName : nom du fichier ascii
% param.PathName : repertoire de stockage du fichier
% param.Toffset  : optionnel pour recaler des fichiers
% asciiStruct : argument optionnel. A utiliser si l'on veut rajouter une
% table dans une structure existante. Dans ce cas, le programme conserve la
% partie header de la structure d'origine.
%
% Arguments de retour:
%
% asciiStruct : structure matlab conforme aux specification xml pour vehlib
%
% -----------------------------------------------------------------------------
function [asciiStruct,ERR]=lectureAscii4Vehlib(param,asciiStruct)

ERR=struct([]);

if nargin<1
    %Construct an MException object to represent the error.
    ERR = MException('lectureAsciiFile4Vehlib:lectureAsciiFile4Vehlib', ...
        'Incompatible number of input argument');
    %Throw the exception to stop execution and display an error message.
    throw(ERR)
end

if ~isfield(param,'FileName')
     %Construct an MException object to represent the error.
    ERR = MException('lectureAsciiFile4Vehlib:lectureAsciiFile4Vehlib', ...
        'FileName is not a field of param structure');
    %Throw the exception to stop execution and display an error message.
    throw(ERR)
end
if ~isfield(param,'PathName')
     %Construct an MException object to represent the error.
    ERR = MException('lectureAsciiFile4Vehlib:lectureAsciiFile4Vehlib', ...
        'PathName is not a field of param structure');
    %Throw the exception to stop execution and display an error message.
    throw(ERR)
end

FileName=param.FileName;
PathName=param.PathName;

if isfield(param,'Toffset')
    Toffset=param.Toffset;
else
    Toffset=0;
end

if nargin==2
    % l'utilisateur veut rajouter une table
    [asciiStruct, err] = verifFomatXML4Vehlib(asciiStruct);
    if err < 0
        %Construct an MException object to represent the error.
        ERR = MException('lectureAsciiFile4Vehlib:verifFomatXMLVEH', ...
            'Incompatible structure, error code: %d',err);
        %Throw the exception to stop execution and display an error message.
        throw(ERR)
    end
    indice=length(asciiStruct.table)+1;
else
    indice=1;
end

% Version du script de lecture
asciiVersion='0.1';

if ~exist(fullfile(PathName,FileName),'file')
    %Construct an MException object to represent the error.
    ERR = MException('lectureAscii4Vehlib:fileNotFound', ...
        'File not found: %s',fullfile(PathName,FileName));
    %Throw the exception to stop execution and display an error message.
    throw(ERR)
end

fid=fopen(fullfile(PathName,FileName),'r');
if fid==-1
    %Construct an MException object to represent the error.
    ERR = MException('lectureAscii4Vehlib:unableToOpenFile', ...
        'Enable to open file: %s',fullfile(PathName,FileName));
    %Throw the exception to stop execution and display an error message.
    throw(ERR)
end

fprintf(1,'%s%s\n\n','Lecture du fichier        : ',fullfile(PathName,FileName));

ligne=[];
i=0;
while isempty(strfind(ligne,'#DEBUT'))
    %identification entete
    ligne=fgetl(fid);
    i=i+1;
    if i>=300
%         errordlg(strcat('Entete #DEBUT pas trouve apres la lecture de 300 lignes du fichier !',fullfile(PathName,FileName)));
%         return;
        disp(strcat('Entete #DEBUT pas trouve apres la lecture de 300 lignes du fichier :',fullfile(PathName,FileName)));
        disp('On essaye de lire sans le #DEBUT');
        % Rewind the file
        fseek(fid,0,-1);
        break;
    end
end

ligne=fgetl(fid);
% deconcatenation de la ligne de nom de variable
nomvar=regexp(deblank(ligne),'\s+','split');
if isempty(nomvar{1})
    % une tabulation en premiere colonne
    nomvar=nomvar(2:end);
end

ligne=fgetl(fid);
for ind_nv = 1:length(nomvar)
    if sum(nomvar{ind_nv}(nomvar{ind_nv}>127))
        %     warning('lectureAscii:nomvar','Bad variable name %s',nomvar{ind_nv})
        strold=nomvar{ind_nv};
        nomvar{ind_nv}(nomvar{ind_nv}>127)='_';
        %     fprintf('Warning: %s replaced by %s\n',strold,nomvar{ind_nv});
    end
end
% deconcatenation de la ligne de nom d unite
nomunit=regexp(deblank(ligne),'\s+','split');
for ind_nv = 1:length(nomunit)
    if sum(nomunit{ind_nv}(nomunit{ind_nv}>127))
        %     warning('lectureAscii:nomvar','Bad unit name %s',nomunit{ind_nv})
        strold=nomunit{ind_nv};
        nomunit{ind_nv}(nomunit{ind_nv}>127)='_';
        %     fprintf('Warning: %s replaced by %s\n',strold,nomunit{ind_nv});
    end
end

numvar=length(nomvar);
numunit=length(nomunit);

%fprintf(1,'\n%s%d\n\n','  Nombre de variables identifiees       : ',numvar);
%fprintf(1,'\n%s%d\n\n','  Nombre d''unites identifiees           : ',numunit);


%lecture des variables
A=fscanf(fid,'%f',[numvar,Inf]);
A=A';

% Fermeture du fichier
fclose(fid);

if indice==1
    % La structure et le head existe
    % Renseignement de la structure
    asciiStruct = struct;

    % Head
    asciiStruct.head = struct;
    asciiStruct.head.version=asciiVersion;
    asciiStruct.head.date=date;
    asciiStruct.head.project='';
    asciiStruct.head.comments=['Created by lectureAscii4Vehlib, version : ',asciiStruct.head.version];
    asciiStruct.head.type='ascii formatted for Vehlib';
end

if indice==1
    % Table Num indice
    asciiStruct.table=cell(1);
end

asciiStruct.table{indice}=struct;
asciiStruct.table{indice}.id=num2str(indice);

%table.metatable
asciiStruct.table{indice}.metatable=struct;
asciiStruct.table{indice}.metatable.name=FileName;
asciiStruct.table{indice}.metatable.date=date;
asciiStruct.table{indice}.metatable.sourcefile=fullfile(PathName,FileName);
asciiStruct.table{indice}.metatable.comments='';

% table.variable
for ind=1:numvar
    asciiStruct.table{indice}.(nomvar{ind})=struct;
%     asciiStruct.table{indice}.(nomvar{ind}).id=num2str(ind);
    asciiStruct.table{indice}.(nomvar{ind}).name=nomvar{ind};
    asciiStruct.table{indice}.(nomvar{ind}).type='double';
    if numvar==numunit
        asciiStruct.table{indice}.(nomvar{ind}).unit=nomunit{ind};
    else
        asciiStruct.table{indice}.(nomvar{ind}).unit='';
    end
    asciiStruct.table{indice}.(nomvar{ind}).precision='%f';
    % Prise en compte de l'offset    
    if Toffset~=0 && ind==1
        asciiStruct.table{indice}.(nomvar{ind}).vector=A(find(A(:,1)>=Toffset),ind)-Toffset;
    else
        asciiStruct.table{indice}.(nomvar{ind}).vector=A(find(A(:,1)>=Toffset),ind);
    end
end

[asciiStruct err] = verifFomatXML4Vehlib(asciiStruct);
if err < 0
    %Construct an MException object to represent the error.
    ERR = MException('lectureAscii4Vehlib:verifFomatXMLVEH', ...
        'Incompatible structure, error code: %d',err);
    %Throw the exception to stop execution and display an error message.
    throw(ERR)
end    


end
