function varList = getXMLVarList(xml)
% function varList = getXMLVarList(xml)
%Fonction pour trouver la liste de variables d'une structure xml
% parametres: 
% - xml: [struct] structure type XML VEHLIB
% valeurs retounées:
% - varList: [1xn cell (px1 cell)] cellule qui contient des cellules (liste
% de variables de chaque table

varList = cell(size(xml.table));
for ind = 1:length(xml.table)
    fieldList = fieldnames(xml.table{ind});
    IF = cellfun(@isempty,regexp(fieldList,'^id$|^metatable$','start','once'));
    varList{ind} = fieldList(IF);
end
end