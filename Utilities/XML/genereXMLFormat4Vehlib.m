% fonction de generation de structure XML4Vehlib
%
% argument d'entre
%       entetes : nom de la variable (string ou cell de longueur 1) / noms des variables (cell {'var1' 'var2' ...})
%       [nom_long] : nom long de la variable (string ou cell de longueur 1) / noms long des variables (cell {'variable1' 'variable2' ...}) [option]
%       [unit] : unite de la variable (string ou cell de longueur 1) / unites des variables (cell {'unit1' 'unit2' ...}) [option]
%       [type] : type de donnees (single,logical,int16,int8 ou uint8) [option]
%       data : vecteur de donnees / matrice des donnes en colonne
%       [OldStructure] : strcuture existante a completer [option]
%       [nom_table] : nom de la table a completer ou nom de la nouvelle table / si vide creation d'une nouvelle table de nom "table_numero"[option]
%
% exemple de lancement
%
% [ERR,Structure]=genereXMLFormat4Vehlib('truc','','',[1 2 3],'','')
%
% [ERR,Structure]=genereXMLFormat4Vehlib({'a' 'b'},{'A' 'B'},{'m/s' 'km/h'},[1 2;2 3;3 4],Structure,'table_1')

function [ERR,Structure]=genereXMLFormat4Vehlib(entetes,nom_long,unit,type,data,OldStructure,nom_table)

Version=0.5;
%compatibilite avec la version 0.1
if nargin ==6
    warning('ATTENTION! le type de donnees (single,int16,int8,logical...) doit etre renseigne\n Mis a sa valeur par defaut (DOUBLE)\n ce message generera une ERREUR dans les versions futures','');
    %glissement des arguments d'entree:
    nom_table = OldStructure;
    OldStructure = data;
    data = type;
    type = '';
end
% mise en forme

if ~iscell(entetes)
    entetes={entetes};
end

if ~isempty(nom_long)
    if ~iscell(nom_long)
        nom_long={nom_long};
    end
end

if ~isempty(unit)
    if ~iscell(unit)
        unit={unit};
    end
end

if ~isempty(type)
    if ~iscell(type)
        type={type};
    end
end

if ~isempty(data)
    if length(entetes)==1
        if size(data,2)~=1
            % les donnees sont disposees en colonnes
            data=data';
        end
    end
else
    disp('Pas de donnees !')
    return
end

if ~isempty(OldStructure)
    % l'utilisateur veut completer une structure existante
    [Structure, ERR] = verifFomatXML4Vehlib(OldStructure);
    if ERR < 0
        %Construct an MException object to represent the error.
        ERR = MException('genereXMLFormat4Vehlib:verifFomatXMLVEH', ...
            'Incompatible Oldstructure, error code: %d',ERR);
        %Throw the exception to stop execution and display an error message.
        throw(ERR)
    end
    indice=length(Structure.table)+1;
else
    indice=1;
end

if ~isempty(nom_table)
    % l'utilisateur donne un nom de table
    if ~isempty(OldStructure)
        liste_table={};
        for i=1:indice-1
            liste_table=[liste_table {Structure.table{i}.metatable.name}];
        end
        ind_table=find(strcmp(liste_table,nom_table));
        if length(ind_table)==1
            % ajout dans une table existante
            indice=ind_table;
            % verification de la taille des variables
            champs=fieldnames(OldStructure.table{indice});
            if eval(['size(data,1)~=length(OldStructure.table{indice}.',champs{3},'.vector)'])
               ERR=(strcat('variable de longueur differente de la table deja existante : ',entetes,' - ',nom_table));
               return
            end          
        elseif length(ind_table)>1
            disp('plusieurs table on le meme nom, creation d''une nouvelle table')
            nom_table=[nom_table,'_',num2str(indice+1)];
        end 
    end
else
    nom_table=['table_',num2str(indice)];
end

% Head
if indice == 1
    Structure.head = struct;
    Structure.head.version=Version;
    Structure.head.date=date;
    Structure.head.project='';
    Structure.head.comments=['Created by genereXMLFormat4Vehlib, version : ',num2str(Structure.head.version)];
    Structure.head.type='xml';
end

% Fin de head

% Table
if ~isempty(OldStructure) && indice<=length(Structure.table)
    % ajout dans une table existante
    Structure.table{indice}.metatable.date=date;
else
    if indice == 1
        Structure.table=cell(1);
    end
    % creation d'une nouvelle table
    Structure.table{indice}=struct;
    Structure.table{indice}.id=num2str(indice);
    
    % table.metatable
    
    Structure.table{indice}.metatable=struct;
    Structure.table{indice}.metatable.name=nom_table;
    Structure.table{indice}.metatable.date=date;
%     Structure.table{indice}.metatable.sourcefile='';
%     Structure.table{indice}.metatable.comments='';
end

% table{indice}.variable

for ind=1:length(entetes)
    % creation de la structure au format vehlib
    nomvar=entetes{ind};
    Structure.table{indice}.(nomvar)=struct;
    Structure.table{indice}.(nomvar).name=nomvar;
    if ~isempty(nom_long)
        Structure.table{indice}.(nomvar).longname=nom_long{ind}; % regexprep(nomvar,'_',' ');
    end
    if ~isempty(type)
        Structure.table{indice}.(nomvar).type=type{ind};        
    else
        Structure.table{indice}.(nomvar).type='double';
    end
    if ~isempty(unit)
        Structure.table{indice}.(nomvar).unit=unit{ind};
    end
    Structure.table{indice}.(nomvar).precision='%f';
    Structure.table{indice}.(nomvar).vector=data(:,ind);
end

% fin des variables

% fin de table

[Structure, ERR] = verifFomatXML4Vehlib(Structure);
if ERR < 0
    %Construct an MException object to represent the error.
    ERR = MException('genereXMLFormat4Vehlib:verifFomatXMLVEH', ...
        'Incompatible structure, error code: %d',ERR);
    %Throw the exception to stop execution and display an error message.
    throw(ERR)
end

end
