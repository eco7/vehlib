function vectorCell = getXMLVector(XMLstruct,varName)
% function getXMLVector(XMLstruct,varName)
% parametres:
% -XMLstruct [struct]: XMLStruct selon le format VEHLIB
% -varName [string]: nom de la variable
% valeurs de retour:
% vectorCell [cell]: cellule qui contient les vecteurs des 'Variable'
% si la variable n'existe pas pour une table, l'element correspondant dans
% la cellule est vide.
vectorCell = cell(size(XMLstruct.table));
varCell = getXMLVariable(XMLstruct,varName);
indices = ~cellfun(@isempty,varCell);
% vectorCell(indices) = cellfun(@(x) getfield(x,'vector'),varCell(indices),'uniformoutput',false);
vectorCell(indices) = cellfun(@(x) x.vector,varCell(indices),'uniformoutput',false);

end