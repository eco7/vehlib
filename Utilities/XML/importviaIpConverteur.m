% Function: importViaIPEconverter
% Fonction d'import d'un fichier issu des enregistreur IPETRONIK
%
%  C Copyright IFSTTAR LTE 1999-2011
%
% Objet:
%
% Fonction d'import d'un fichier issu des enregistreur IPETRONIK
%
% Necessite d'avoir une version du logiciel IPECOnverter.exe install� sur le pc
% Ne fonctionne que sous windows
%
% Arguments d'appel:
%
% param.IPEconverterName : Nom de l executable
% param.IPEconverterPath : Repertoire de IPEconverter
% FileNameDAT : nom du fichier a importer
% PathNameDAT : repertoire de stockage du fichier a importer
%
% Arguments de retour:

% FileNameCsv : nom du fichier extrait
% PathNameCsv : repertoire du fichier extrait

%
% ERR
%
% -----------------------------------------------------------------------------
function [ERR, FileNameCsv, PathNameCsv]=importviaIpConverteur(param, FileNameDAT, PathNameDAT)

ERR=struct([]);

% Gestion  des erreurs
if nargin~=3
    %Construct an MException object to represent the error.
    ERR = MException('importviaIpConverteur:importviaIpConverteur', ...
        'Incompatible number of input argument');
end

if ~ispc
    %Construct an MException object to represent the error.
    ERR = MException('importviaIpConverteur:importviaIpConverteur', ...
        'This program run on Windows only');
    %Throw the exception to stop execution and display an error message.
    throw(ERR)
end

if  ~isfield(param,'IPEconverterName') || ~isfield(param,'IPEconverterPath') || ...
        ~exist(param.IPEconverterPath,'dir')
    %Construct an MException object to represent the error.
    ERR = MException('importviaIpConverteur:importviaIpConverteur', ...
        'Problem with IPEconverterName or Path');
end


PathNameCsv=PathNameDAT;

if ~exist(PathNameDAT,'dir')
    %Construct an MException object to represent the error.
    ERR = MException('importviaIpConverteur:importviaIpConverteur', ...
        'Problem with PathNameDAT');
end

if ~exist(fullfile(PathNameDAT,FileNameDAT),'file')
    %Construct an MException object to represent the error.
    ERR = MException('importviaIpConverteur:importviaIpConverteur', ...
        'Problem with input fileName/dir');
end

test=fullfile(param.IPEconverterPath, param.IPEconverterName);
if ~exist(test,'file')
     %Construct an MException object to represent the error.
    ERR = MException('importviaIpConverteur:importviaIpConverteur', ...
        'Problem with IPEconverterName or Path');
end

% Construction de la ligne de commande
cmd=param.IPEconverterName;
sourceFile=fullfile(PathNameDAT,FileNameDAT);
destFolder=PathNameCsv;
arguments='/CSV /NMSG';
cmd=[cmd, ' ', sourceFile, ' ', destFolder, ' ', arguments];

try
    currentFolder=pwd;
    cd(param.IPEconverterPath);
    eval(['!' cmd]);
    cd(currentFolder);
catch ME
    %Construct an MException object to represent the error.
    ERR = MException('importviaIpConverteur:importviaIpConverteur', ...
        'Problem while converting the file');
end

[~,name,~]=fileparts(FileNameDAT);
FileNameCsv=[name,'.csv'];

% IPEconverter ne renvoie pas d'erreur. On test si le fichier out existe
test=fullfile(PathNameCsv,FileNameCsv);
if ~exist(test,'file')
    %Construct an MException object to represent the error.
    ERR = MException('importviaIpConverteur:importviaIpConverteur', ...
        'Output (csv) File not found !');
end

end
