function xls2txt(filename,indFeuille)
% XLS2TXT Converts XLS in TXT (parser function for xls2txt.py), unix/win
% XLS2TXT(filename) lit le (filename) et ecrit un  fichier TXT par feuille
% presente dans le XLS, les fichiers s'appelent comme le fichier XLS avec
% un suffixe de numero de feuille.
% Exemple: xls2txt('test.xls') ecrira 'test_001.txt', 'test_002.txt'... et
% ainsi pour chaque feuille.
%
% XLS2TXT(filename,indFeuille): fait l'operation juste pour la feuille
% indiquee.
%
% See Also importArbinTxt
%
% TODO: mettre les fichiers dans un repertoire du nom du ficheir.xls et les
% appeler par son nom:001_Feuille1.txt, 002_Feuille2.txt, ...



myPythonScript = which('xls2txt.py');

if isempty(myPythonScript)
    fprintf('ERROR: xls2txt.py not found\n')
    return
end

myCMD = sprintf('python %s %s',myPythonScript, filename);
if exist('indFeuille','var')
    myCMD = sprintf('%s %d',myCMD, indFeuille);
else
    myCMD = sprintf('%s',myCMD);
end
[A B] = dos(myCMD);
%TODO gestion de la sortie stderr dans python
end