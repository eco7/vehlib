function [ xmlS ] = filtreXML( xmlS, indT, I)
%FILTREXML Filtre tous les vecteurs d'une table XML en fonction du booleen I
%   xmlS = Structure XML
%   indT = indice de table
%   I = booleen

fieldList = fieldnames(xmlS.table{indT});

for i = 3:length(fieldList)
    xmlS.table{indT}.(fieldList{i}).vector = xmlS.table{indT}.(fieldList{i}).vector(I);
end

