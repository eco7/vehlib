function varCell = getXMLVariable(XMLstruct,varName)
% function getXMLVariable(XMLstruct,varName)
% parametres:
% -XMLstruct [struct]: XMLStruct selon le format VEHLIB
% -varName [string]: nom de la variable
% valeurs de retour:
% varCell [cell struct]: cellule qui contient des structure 'Variable'
% si la variable n'existe pas pour une table, l'element correspondant dans
% la cellule est vide.
varCell = cell(size(XMLstruct.table));
indices = cellfun(@(x) isfield(x,varName),XMLstruct.table);
varCell(indices) = cellfun(@(x) getfield(x,varName),XMLstruct.table(indices),'uniformoutput',false);
end