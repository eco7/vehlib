function uninitpath(dirvehlib)
% function uninitpath(dirvehlib)
% Undo initpath

%TODO test in windows

if ~exist('dirvehlib','var')
    dirvehlib = which('uninitpath');
    dirvehlib = fileparts(dirvehlib);%remove uninitpath.m
    dirvehlib = fileparts(dirvehlib);%remove 'Utilities'
end
%1 cleanup 'dirvehlib':
abs_path = absolute_path(dirvehlib);

fprintf('dirvehlib is %s\n',abs_path)
%2. get path and split it into parts
P = path;
P = regexp(P,':','split');

Pvehlib = regexpFiltre(P,abs_path);
cellfun(@rmpath,Pvehlib);
fprintf('Removed %d folders from path\n',length(Pvehlib))
end