% Function: break_link
% Fonction qui "casse" les liens du modele avec la bibliotheque VEHLIB
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
% break-link est une fonction qui casse les liens du modele avec la
% bibliotheque VEHLIB.
%
% Argument:
%   model : nom du modele
%   libName : nom de la librairie ou est stocke les blocs a rendre autonome
%
% Remarque:
%
% Pour trouver les disabled link dans une libraire, ou un modele:
% (start code)
% b=find_system(model,'lookundermasks','all');
% for i=1:length(b)
%     d=get(get_param(b{i},'handle'));
%     if isfield(d,'LinkStatus') && ~strcmp(get_param(b{i},'Linkstatus'),'resolved')&& ~strcmp(get_param(b{i},'Linkstatus'),'none')
%         b{i}
%         get_param(b{i},'Linkstatus') % repond inactive
%     end
% end
% (end)
% -------------------------------------------------------------------
function []=break_link(model,libName)

% Ouverture de la bibliotheque VEHLIB
open_system(libName);

% unlock library
set_param(libName,'lock','off');

% Ouverture du modele
open_system(model)

% Recherche de tous les sous-blocs du modele
b=find_system(model,'lookundermasks','all');

for i=1:length(b)
    d=get(get_param(b{i},'handle'));
    if isfield(d,'LinkStatus') && ~isempty(getfield(d,'ReferenceBlock'))
        set(get_param(b{i},'handle'),'LinkStatus','none');
    end
end

% Fermeture sans sauvegarde 
close_system(libName,0);

