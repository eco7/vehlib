clearvars
load wks_sans_cpl.mat;
var_wks_1=who;
[ERR,R1]=affect_struct(var_wks_1);

clearvars -except R1  

load wks_avec_cpl.mat;
var_wks_2=who;
[ERR,R2]=affect_struct(var_wks_2);

clearvars -except R1 R2

R2=rmfield(R2,'R1');


R1=rmfield(R2,'r_elec');
R2.nom='toto';
R1.nom='toto10';
R2.nom1=10;
R1.nom1='toto10';
R1.A=ones(3,3,3,3);
R2.A=ones(3,3);
R1.B=ones(3,3,3,3);
R2.B=ones(3,2,2,4);
R1.C=[ 1 2 3 nan 5];
R2.C=[ 1 2 nan nan 5];
R1.D=[ 1 2 3 nan 5];
R2.D=[ 1 2 nan 4 5];

R1.E=[ 1 2 3 inf 5];
R2.E=[ 1 2 nan 4 5];

R1.F=rand(3,2,3);
R2.F=rand(3,2,3);

R1.F(2,2,2)=NaN;
R2.F(2,2,2)=NaN;

R1.VD.ADCPL1.kred=2;

comp_struct(R1,R2);



















