clearvars
load bug_vehlib_mod.mat;
%load wks_sim_pentes;

var_wks_1=who;
[ERR,R1]=affect_struct(var_wks_1);

clearvars -except R1  

load bug_vehlib_sim_pentes.mat;
%load wks_vehlib;
var_wks_2=who;
[ERR,R2]=affect_struct(var_wks_2);
R2=rmfield(R2,'R1');
clearvars -except R1 R2


[i_struct,i_cell,R1c,R2c,field_R1c,field_R2c]=comp_struct(R1,R2);


if ~isempty(i_struct)
    fprintf('%s %s','Il y as des sous structures : ');
    fprintf('\n\n')
    
    for ii=1:length(i_struct)
        fprintf('%s %s','pasage niveau sous structure champ : ',field_R1c{i_struct(ii)});
        fprintf('\n\n')

        S1=getfield(R1c,field_R1c{i_struct(ii)});
        S2=getfield(R2c,field_R2c{i_struct(ii)});
        [i_sous_struct]=comp_struct(S1,S2);
    end
end

if ~isempty(i_cell)
    fprintf('%s %s','Il y as des cell : ');
    fprintf('\n\n')
    
    for ii=1:length(i_cell)
        fprintf('%s %s','pasage niveau champ des cell : ',field_R1c{i_cell(ii)});
        fprintf('\n\n')
        
        C1=getfield(R1c,field_R1c{i_cell(ii)});
        C2=getfield(R2c,field_R2c{i_cell(ii)});
        
        for jj=1:length(C1)
            if isstruct(C1{jj})
                fprintf('%s %d %s','le champ',jj, 'est une structure');
                fprintf('\n\n')
                [i_struct_cell,~,R1c_cell,R2c_cell,field_R1c_cell,field_R2c_cell]=comp_struct(C1{jj},C2{jj});
                
                if ~isempty(i_struct_cell)
                    fprintf('%s %s','Il y as des sous structures : ');
                    fprintf('\n\n')
                    
                    for kk=1:length(i_struct_cell)
                        fprintf('%s %s','pasage niveau sous structure champ : ',field_R1c_cell{i_struct_cell(kk)});
                        fprintf('\n\n')
                        

                        S1_cell=getfield(R1c_cell,field_R1c_cell{i_struct_cell(kk)});
                        S2_cell=getfield(R2c_cell,field_R1c_cell{i_struct_cell(kk)});
                        [i_sous_struct]=comp_struct(S1_cell,S2_cell);
                    end
                end
            else
                fprintf('%s %d %s','attention le champ',jj, 'n''est pas une structure, pas de verification pour l''instant');
                fprintf('\n\n')
            end
        end
        
        
    end
end















