function [i_struct,i_cell,R1c,R2c,field_R1c,field_R2c]=comp_struct(R1,R2)
field_R2=fieldnames(R2);
field_R1=fieldnames(R1);

%% verification que l'on as mes meme chanp dans les deux struct suppression das champs présent dans une seule des struct
i_nchamp1=[];
i_nchamp2=[];
for ii=1:size(field_R1,1);
    if ~isfield(R2,field_R1{ii})
        fprintf('%s %s','champ existant dans R1 et pas R2 :',field_R1{ii});
        fprintf('\n\n');
        i_nchamp1=[i_nchamp1 ii];
    end
end

for ii=1:size(field_R2,1);
    if ~isfield(R1,field_R2{ii})
        fprintf('%s %s \n','champ existant dans R2 et pas R1 :',field_R2{ii});
        fprintf('\n\n');
        i_nchamp2=[i_nchamp2 ii];
    end
end

R1c=rmfield(R1,field_R1(i_nchamp1));
R2c=rmfield(R2,field_R2(i_nchamp2));

field_R2c=fieldnames(R2c);
field_R1c=fieldnames(R1c);

%% verification que les champs contienne les meme formats de donne et suppression des champs si ce n'est pas le cas
i_nnum1=[];
i_nnum2=[];
i_struct=[];
i_nstruct1=[];
i_cell=[];
i_ncell1=[];
i_nchar1=[0];
i_num=[];

for ii=1:size(field_R1c,1);
    var_R1c=getfield(R1c,field_R1c{ii});
    var_R2c=getfield(R2c,field_R1c{ii});
    
    if isstruct(var_R1c)        
        if ~isstruct(var_R2c) 
            fprintf('%s %s','le champ :',field_R1c{ii},'est une structure dans R1 et pas dans R2');
            fprintf('\n\n');
            i_nstruct1=[i_nstruct1 ii];   
        else % si l'on as des structures on detecte les niveau de sous structure
            i_struct=[i_struct ii];
        end   
    end
    
    if iscell(var_R1c)        
        if ~iscell(var_R2c) 
            fprintf('%s %s','le champ :',field_R1c{ii},'est un cell dans R1 et pas dans R2');
            fprintf('\n\n');
            i_nscell1=[i_ncell1 ii];   
        else % si l'on as des cell
            i_cell=[i_cell ii];
        end   
    end
    
    if isnumeric(var_R1c)
        if ~isnumeric(var_R2c)
            fprintf('%s %s','le champ :',field_R1c{ii},' est un array dans R1 et pas dans R2');
            fprintf('\n\n');
            i_nnum1=[i_nnum1 ii]; 
        else % on stocke toute les indices ou l'on as des valeurs numeric
            i_num=[i_num ii];
        end
    end
    
    if ischar(var_R1c)
        if ~ischar(var_R2c)
            fprintf('%s %s','le champ :',field_R1c{ii},' est une chaine de caracteres dans R1 et pas dans R2');
            fprintf('\n\n')
            i_nchar1=[i_nchar1 ii];
        else
            if strcmp(var_R1c,var_R2c)==0;
                fprintf('%s \n','les chaines de caracteres des champs R1 et R2 sont differentes:')
                fprintf('%s %s\n','champ :',field_R1c{ii});
                fprintf('%s %s %s','chaine de caractere :',var_R1c,var_R2c);   
                fprintf('\n\n')
            end
        end
    end
end

%% comparaison des valeurs numerique
for jj=1:length(i_num)
    var_R1c=getfield(R1c,field_R1c{i_num(jj)});
    var_R2c=getfield(R2c,field_R1c{i_num(jj)});
    
    d1=ndims(var_R1c);
    d2=ndims(var_R2c);
    s1=size(var_R1c);
    s2=size(var_R2c);
    ind_nan_ok=1;
    ind_inf_ok=1;
    
    if d1~=d2
        fprintf('%s %s %s\n','les dimensions des variables du champ : ',field_R1c{i_num(jj)}, ' sont differentes')
        fprintf('%s %d','dimensions dans R1 :',d1);
        fprintf('\n')
        fprintf('%s %d','dimensions dans R2 :',d2);
        fprintf('\n\n')
    elseif sum(abs(s1-s2))~=0
        fprintf('%s %s %s\n','les tailles des variables du champ : ',field_R1c{i_num(jj)}, ' sont differentes')
        fprintf('%s %s','taille dans R1 :',num2str(s1));
        fprintf('\n')
        fprintf('%s %s','taille dans R2 :',num2str(s2));
        fprintf('\n\n')
    else
        ind_nan1=find(isnan(var_R1c));
        ind_nan2=find(isnan(var_R2c));
        
        ind_inf1=find(isinf(var_R1c));
        ind_inf2=find(isinf(var_R2c));
        
        ind1=find(~isinf(var_R1c) & ~isnan(var_R1c));
        ind2=find(~isinf(var_R2c) & ~isnan(var_R2c));
        
        if ~isempty(ind_nan1)
            if ~isempty(ind_nan2)
                if length(ind_nan1)~=length(ind_nan2)
                    fprintf('%s %s','il n''y as pas le memes nombre de valeurs a nan pour le champ : ',field_R1c{i_num(jj)});
                    fprintf('\n\n')
                    ind_nan_ok=0; 
                elseif sum(abs(ind_nan1-ind_nan2))~=0
                    fprintf('%s %s','il n''y as pas les memes valeurs a nan pour le champ : ',field_R1c{i_num(jj)});
                    fprintf('\n\n')
                    ind_nan_ok=0; 
                end
            elseif isempty(ind_nan2)
                fprintf('%s %s','il y as des valeurs a nan dans R1 et pas dans R2 pour le champ : ',field_R1c{i_num(jj)});
                fprintf('\n\n')
                ind_nan_ok=0; 
            end
        elseif isempty(ind_nan1)  & ~isempty(ind_nan2)
            fprintf('%s %s','il y as des valeurs a nan dans R2 et pas dans R1 pour le champ : ',field_R1c{i_num(jj)});
            fprintf('\n\n')
            ind_nan_ok=0;
        end
        
        if ~isempty(ind_inf1)
            if ~isempty(ind_inf2)
                if length(ind_inf1)~=length(ind_inf2)
                    fprintf('%s %s','il n''y as pas le memes nombre de valeurs a inf dans le champ : ',field_R1c{i_num(jj)});
                    fprintf('\n\n')
                    ind_inf_ok=0;
                elseif sum(abs(ind_inf1-ind_inf2))~=0
                    fprintf('%s %s','il n''y as pas les memes valeurs a inf dans le champ : ',field_R1c{i_num(jj)});
                    fprintf('\n\n')
                    ind_inf_ok=0;
                end
            elseif isempty(ind_inf2)
                fprintf('%s %s','il y as des valeurs a inf dans R1 et pas dans R2 pour le champ : ',field_R1c{i_num(jj)});
                fprintf('\n\n')
                ind_inf_ok=0;
            end
        elseif isempty(ind_inf1)  & ~isempty(ind_inf2)
            fprintf('%s %s','il y as des valeurs a inf dans R2 et pas dans R1 pour le champ : ',field_R1c{i_num(jj)});
            fprintf('\n\n')
            ind_inf_ok=0;
        end
        
        if ind_inf_ok==1 & ind_nan_ok==1
            if sum( abs( var_R1c(ind1)-var_R2c(ind2) ) )~=0;
                fprintf('%s %s','il y as des valeurs differentes dans R1 et R2 pour le champ : ',field_R1c{i_num(jj)});
                fprintf('\n\n')
            end
        end
        
    end
end

% if ~isempty(i_struct)
%     fprintf('%s %s','Il y as des sous structures : ');
%     fprintf('\n\n')
%     
%     for ii=1:length(i_struct)
%         fprintf('%s %s','pasage niveau sous structure champ : ',field_R1c{i_struct(ii)});
%         fprintf('\n\n')
% 
%         S1=getfield(R1c,field_R1c{i_struct(ii)});
%         S2=getfield(R2c,field_R2c{i_struct(ii)});
%         comp_struct(S1,S2);
%        
%     end
% end
% 
% if ~isempty(i_cell)
%     fprintf('%s %s','Il y as des cell : ');
%     fprintf('\n\n')
%     
%     for ii=1:length(i_cell)
%         fprintf('%s %s','pasage niveaudes cell : ',field_R1c{i_cell(ii)});
%         fprintf('\n\n')
% 
%         S1=getfield(R1c,field_R1c{i_struct(ii)});
%         S2=getfield(R2c,field_R2c{i_struct(ii)});
%         comp_struct(S1,S2);
%        
%     end
% end

return


















