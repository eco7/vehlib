% Function: runAllTests(vehlibOnly,excludeFolders)
%
%  (C) Copyright IFSTTAR LTE 1999-2011
%
%
% Objet:
%
% Faire tourner tous les tests places dans les sous dossier TestCase des
% utilisateurs, si presents.
% vehlibOnly : true or false if you don't want to run Users test cases
% excludeFolders : a cell array of string pointing to the folder you want
% to exclude
%
% See also : runtests, Xunit si release < 8.2 (R2013b)
% See also : unitTest si release > 8.2 (R2013b)
% -----------------------------------------------------------
function runAllTests(vehlibOnly,excludeFolders)

error(nargchk(0,2,nargin));

if nargin == 0 || nargin == 1
    excludeFolders={};
end

if nargin == 0
    vehlibOnly=false;
    runAllTests(vehlibOnly);
    vehlibOnly=true;
    runAllTests(vehlibOnly);
    return
end

if vehlibOnly ~= 0 && vehlibOnly ~= 1
    chaine='Wrong Argument: must be a boolean';
    ERR=MException('runAllTests:Arg',chaine);
    throw(ERR)
end

% Evaluation du script initdata
eval('initdata;');

if vehlibOnly
    runTestsSwVers(fullfile(INIT.TestCaseFolder,'Forward'),'-verbose');
    runTestsSwVers(fullfile(INIT.TestCaseFolder,'Backward'),'-verbose');
    runTestsSwVers(fullfile(INIT.TestCaseFolder,'Utilities'),'-verbose');
end

if ~vehlibOnly
    rep=lsFiles(INIT.UsersFolder,'',true);
    P=path;
    excludeFolders(end+1)={'templates'};
    for ind=1:length(rep)
        [~,dossier]=fileparts(rep{ind});
        a=cellfun(@(x) strcmp(dossier, x), excludeFolders, 'UniformOutput',false);       
        if ~sum(cell2mat(a))
            rep_test=fullfile(INIT.UsersFolder,dossier,'cas_test'); % old versions
            new_rep_test=fullfile(INIT.UsersFolder,dossier,'Utilities','TestCase');
            if exist(rep_test,'dir')
                runTestsSwVers(rep_test,'-verbose');
            elseif exist(new_rep_test,'dir')
                 runTestsSwVers(new_rep_test,'-verbose');
            end
        end
    end
end
end

function runTestsSwVers(folder,arg)
% Switch depending on matlab version
try
    if verLessThan('matlab','8.2')
        disp(['Test du dossier : ',folder]);
        runtests(folder,arg);
    else
        disp(['Test du dossier : ',folder]);
%        pause;
        results = runtests(folder);
    end
catch ME
    disp(['Erreur a l execution des cas test du dossier : ',folder]);
end
end


