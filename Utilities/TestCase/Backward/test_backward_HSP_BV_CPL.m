% function test_suite =test_backward_HSP(testCase)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 23-07-12(EV): Creation 

function test_suite =test_backward_HSP_BV_CPL(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 
warning off;

function test_HSP_BV_2EMB_CPL_Prog_dyn_NEDC_BV2r_acm1_aval(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV2r','Prius_HSP_BV_2EMB_CPL','HSP_BV_2EMB_CPL', 'param_backward_prog_dyn_test_HSP_BV_2EMB',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)]; 
%expected_out=[4.0925    0.0042];
%expected_out=[4.0591    0.0196]; %Changement seuil cinematique BV2r en recup on est en premiere même e, dessous de 10 km/h
expected_out=[3.9756    0.0077]; 
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<0.1,'test_HSP_BV_2EMB_Prog_dyn_NEDC_BV2r : max bilanP>0.1')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    %verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 0.1)
end





function test_HSP_BV_2EMB_CPL_Prog_dyn_ARTROUT_BV2r_acm1_aval(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTROUT_BV2r','Prius_HSP_BV_2EMB_CPL','HSP_BV_2EMB_CPL', 'param_backward_prog_dyn_test_HSP_BV_2EMB',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)]; 
expected_out=[4.2302    0.0357];
%expected_out=[4.2351    0.0357]; % Changement des composants pour etre coherent avec HSP_BV
expected_out=[4.1936   0.0361];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<0.1,'test_HSP_BV_2EMB_Prog_dyn_NEDC_BV2r : max bilanP>0.1')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    %verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 0.1)
end




function test_HSP_BV_2EMB_CPL_Prog_dyn_ARTAUTO_acm1_aval(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTAUTO','Prius_HSP_BV_2EMB_CPL','HSP_BV_2EMB_CPL', 'param_backward_prog_dyn_test_HSP_BV_2EMB',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)]; 
%expected_out=[6.26764   0.0318];
expected_out=[6.2700  0.0318];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<0.1,'test_HSP_BV_2EMB_Prog_dyn_NEDC_BV2r : max bilanP>0.1')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
   % verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 0.1)
end




function test_HSP_BV_2EMB_CPL_Prog_dyn_ARTROUT_acm1_amont(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTROUT','Prius_HSP_BV_2EMB_CPL','HSP_BV_2EMB_CPL', 'param_backward_prog_dyn_test_HSP_BV_2EMB_2',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)]; 
expected_out=[ 3.870382   0.021085];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<0.1,'test_HSP_BV_2EMB_Prog_dyn_ARTROUT : max bilanP>0.1')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 0.1)
end





function test_HSP_BV_2EMB_CPL_Prog_dyn_ARTURB_BV2r_acm1_amont(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTURB_BV2r','Prius_HSP_BV_2EMB_CPL','HSP_BV_2EMB_CPL', 'param_backward_prog_dyn_test_HSP_BV_2EMB_2',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)]; 
expected_out=[ 4.507401 0.002138];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<0.1,'test_HSP_BV_2EMB_Prog_dyn_ARTROUT : max bilanP>0.1')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 0.1)
end




function test_HSP_BV_2EMB_CPL_Prog_dyn_ARTAUTO_acm1_amont(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTAUTO','Prius_HSP_BV_2EMB_CPL','HSP_BV_2EMB_CPL', 'param_backward_prog_dyn_test_HSP_BV_2EMB_2',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)]; 
expected_out=[   6.10555    0.02484];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
    assertTrue(max(abs(bilanP(2:end)))<0.1,'test_HSP_BV_2EMB_Prog_dyn_ARTROUT : max bilanP>0.1')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 0.1)
end


