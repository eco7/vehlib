% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes

% definition pas de temps
param.pas_temps = 1;

% calcul de l'acceleration (1 a gauche 2 a droite 3 centree)
param.calc_accel = 1;

% definition echantillonnage couple 
% param.nbsampl=500;

% Parametre pour debugger
param.verbose=0;

% Type de moteur thermique (willans, frot_lin ou DRIVE)
param.type_moth = 'willans';

% Type de minimisation de H (a choisir : analytique ou numerique)
param.optim='analytique';

% Gestion energie %
% proportion de freinage regeneratif autorisee
param.maxRegen = 1;
% dod pour le calcul de E, R, iMax et iMin batterie
param.dodC = 50;
% E nominal pour le calcul de la machine
param.E_nom = 200;
% Convergence dans un intervalle ou calcul pour un parametre de lagrange fixe
param.cvgce=0;
% Valeur du parametre de lagrange
param.lambda= 2.995300292968750;
% valeurs attendues (test)
% dSocTest = -1.937856777126662;
% consoLp100KmTest = 3.789352797262324;

