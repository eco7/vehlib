%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% INRETS - LTE Juin 2010
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% définition pas de temps
param.pas_temps=1;

% Parametre pour debugger
param.verbose=0;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='lagrange';

% Convergence dans un intervalle ou calcul pour un parametre de lagrange fixe ?
param.cvgce=1;

% tdebug pour donner l'instant ou l'on veut tracer la fonction (0 sinon)
param.tdebug=0;

% Autorisation du flux serie en freinage (1: autorise, 0: interdit) 
% non implemente
%param.freinage_flux_serie=1;

% Arret du groupe electrogene autorise (1) ou interdit (0)
% param.stopstart=1;
% mode : 0 groupe tout le temps en marche 1 arret groupe autorise 2 stop start
param.mode=1;

% Parametre de convergence
param.paramCvgce='param.lambda';

% Fonction et critere de convergence (la structure s'appelle ResXml)
param.fonctionCvgce='soc=XmlValue(ResXml,''soc'');';
param.critereCvgce='soc(end)';

