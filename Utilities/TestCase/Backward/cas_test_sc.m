function [ ] = cas_test_sc(psc_dem)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

param.pas_temps=1;
initdata;
CIN_ECE15
Pack_SC_MW_144F_48V
eval(SC.Nom_element);
Pack_AXELEC
eval(BATT.Nom_bloc);

u0sc=zeros(size(psc_dem));
ux=zeros(size(psc_dem));
usc=zeros(size(psc_dem));
isc=zeros(size(psc_dem));
% isc2=zeros(size(psc_dem));
  % condition initial t=0
    isc(1)=0;
    u0sc(1)=SC.Vc0.*SC.Nblocser;
    ux(1)=SC.Vx0.*SC.Nblocser;
%     [ERR,usc(1)]=calc_sc(ERR,SC,0,param.pas_temps,isc(1),u0sc(1),ux(1));

for j=1:length(psc_dem);
    % boucle temporelle

        % variables electriques SC en fonction de la puissance
        [ERR,isc(j),~,u0sc,ux,~,~,usc(j),~,~,~,~] = calc_isc_psc(INIT,CYCL,SC,'',BATT,'',param,psc_dem,u0sc,j,ux,zeros(size(CYCL.temps)),zeros(size(CYCL.temps)),zeros(size(CYCL.temps)));
%         [ERR,isc(j),u0sc,ux,usc(j)] = calc_isc_psc_simple(INIT,CYCL,SC,param.pas_temps,psc_dem,u0sc,j,ux);


  fprintf('\n%s%d\n','Appel calc_isc_psc a l instant : ',j);        
  fprintf('%s%.2f\n','Valeur de psc_dem : ',psc_dem(j));
  fprintf('%s%.3f\n','Valeur de isc : ',isc(j));
  fprintf('%s%.3f\n','Valeur de usc : ',usc(j)./SC.Nblocser);
  fprintf('%s%.3f\n','Valeur de u0sc : ',u0sc(j)./SC.Nblocser);
  fprintf('%s%.3f\n','Valeur de psc : ',isc(j).*usc(j));
  
%   [ERR,isc2(j)]=calc_isc(ERR,SC,j,param.pas_temps,usc,u0sc,ux);
%   
%   fprintf('%s%d\n','Appel calc_isc a l instant : ',j);        
%   fprintf('%s%.3f\n','Valeur de isc : ',isc2(j));
%   fprintf('%s%.3f\n','Valeur de usc : ',usc(j)./SC.Nblocser);
%   fprintf('%s%.2f\n','Valeur de psc : ',isc2(j).*usc(j));
        


if j>1
    [ERR,usc(j),u0sc(j),ux(j)]=calc_sc(ERR,SC,j,param.pas_temps,isc(j),u0sc(j-1),ux(j-1));
else
    [ERR,usc(j),u0sc(j),ux(j)]=calc_sc(ERR,SC,j,param.pas_temps,isc(j),u0sc(j),ux(j));
end
  fprintf('%s%d\n','Appel calc_sc a l instant : ',j);        
  fprintf('%s%.3f\n','Valeur de isc : ',isc(j));
  fprintf('%s%.3f\n','Valeur de usc : ',usc(j)./SC.Nblocser);
  fprintf('%s%.3f\n','Valeur de u0sc : ',u0sc(j)./SC.Nblocser);
  fprintf('%s%.2f\n \n','Valeur de psc : ',isc(j).*usc(j));

pause
end

end

