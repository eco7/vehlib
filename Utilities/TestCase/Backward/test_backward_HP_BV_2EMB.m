% function test_suite =test_backward_HP_BV_2EMB(testCase)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1
% 10-11-2016 (EV) : Les cas test faisnat appel a des archis utilisant le
% moteur NES sont commenté et déplacé dans Users/LTE

function test_suite =test_backward_HP_BV_2EMB(testCase)
 if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 
 
function test_HP_BV_2EMB_prog_dyn_308_dsoc0(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB','HP_BV_2EMB','param_backward_prog_dyn_test_308_dsoc0',1,1,50);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[ 4.024626   0.000461];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
end

function test_HP_BV_2EMB_prog_dyn_308_dsoc0_rapp_libre(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB','HP_BV_2EMB','param_backward_prog_dyn_test_308_dsoc0',1,1,50);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[3.857149  0.003094];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
end

function test_HP_BV_2EMB_lagrange_308_cycle(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB','HP_BV_2EMB','param_backward_lagrange_2EMB_test_308_cycle',1,1,50);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[4.012473   0.073338];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
end

function test_HP_BV_2EMB_prog_dyn_308_dsoc2(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB','HP_BV_2EMB','param_backward_prog_dyn_test_308',1,1,50);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[ 4.281367  -2.029538];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
end

function test_HP_BV_2EMB_lagrange_308_dsoc2(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB','HP_BV_2EMB','param_backward_lagrange_2EMB_test_308',1,1,50);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[4.278060  -2.026147];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
end



% function test_HP_BV_2EMB_CLIO_lagrange(testCase)
% format long
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','CLIO_1L5DCI_HP_BV_2EMB_NES_NHP','HP_BV_2EMB','param_backward_lagrange_test_2EMB',1,1,50);
% conso100=XmlValue(ResXml,'conso100');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end)];
% %expected_out=[3.04162 2.1099];
% expected_out=[3.0329 2.1099];
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
% 
% 
% function test_HP_BV_2EMB_CLIO_Prog_dyn_NEDC_tt_thermique(testCase)
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','CLIO_1L5DCI_HP_BV_2EMB_NES_NHP','HP_BV_2EMB','param_backward_prog_dyn_test_2EMB',1,1,50);
% conso100=XmlValue(ResXml,'conso100');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end)];
% expected_out=[3.078284   1.320424];
% %expected_out=[3.08641   1.40054]; % modif calc_emb 09-11-11
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% bilanP=XmlValue(ResXml,'bilanP');
% out=bilanP(2:end);
% expected_out=0*ones(size(out));
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% 
% 
% function test_HP_BV_2EMB_CLIO_Prog_dyn_NEDC(testCase)
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','CLIO_1L5DCI_HP_BV_2EMB_NES_NHP','HP_BV_2EMB','param_backward_prog_dyn_test_2EMB_2',1,1,50);
% conso100=XmlValue(ResXml,'conso100');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end)];
% %expected_out=[3.131214   0.015159]; 
% expected_out=[3.1231   0.015159]; 
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% bilanP=XmlValue(ResXml,'bilanP');
% out=bilanP(2:end);
% expected_out=0*ones(size(out));
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% 
% 
% function test_HP_BV_2EMB_CLIO_Prog_dyn_NEDC_ne % cinematique non echantillone regulieremnt(testCase)
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV_ne','CLIO_1L5DCI_HP_BV_2EMB_NES_NHP','HP_BV_2EMB','param_backward_prog_dyn_test_2EMB_ne',1,1,50);
% conso100=XmlValue(ResXml,'conso100');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end)];
% %expected_out=[3.1293    0.0391]; 
% %expected_out=[3.1211    0.0391];
% expected_out=[3.121138  0.020023]; % Suppression des points de recup avec pertes > puissance meca (07-05-16)
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% bilanP=XmlValue(ResXml,'bilanP');
% out=bilanP(2:end);
% expected_out=0*ones(size(out));
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% 
% 
% function test_HP_BV_2EMB_CLIO_Prog_dyn_ARTURB_BV_classe3(testCase)
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTURB_BV_Classe3','CLIO_1L5DCI_HP_BV_2EMB_NES_NHP','HP_BV_2EMB','param_backward_prog_dyn_test_2EMB',1,1,50);
% conso100=XmlValue(ResXml,'conso100');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end)];
% %expected_out=[4.156159254191826   0.530108639183794];% modif calc_emb 09-11-11
% %expected_out=[4.16504   0.48496];
% %expected_out=[4.1562   0.5301];
% expected_out=[4.148861   0.553792]; % Suppression des points de recup avec pertes > puissance meca (07-05-16)
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% bilanP=XmlValue(ResXml,'bilanP');
% out=bilanP(2:end);
% expected_out=0*ones(size(out));
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% 
% 
% function test_HP_BV_2EMB_CLIO_Prog_dyn_HYZAUTO_rap_optimise(testCase)
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_HYZAUTO','CLIO_1L5DCI_HP_BV_2EMB_NES_NHP','HP_BV_2EMB','param_backward_prog_dyn_test_2EMB_rap_opt',1,1,50);
% conso100=XmlValue(ResXml,'conso100');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end)];
% %expected_out=[4.605743940973587  -0.008200787240582];% changement dans le calcul des limites plusmodif calc_emb le 07-11-11 (EV)
% %expected_out=[4.54376  -0.00139]; % correction bug calcul des limites le 07_05_12 (EV)
% %expected_out=[ 4.606026960404303  -0.008200787240582]; 
% %expected_out=[ 4.6042  -0.0082]; 
% expected_out=[4.603803 -0.004470]; % Suppression des points de recup avec pertes > puissance meca (07-05-16)
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% bilanP=XmlValue(ResXml,'bilanP');
% out=bilanP(2:end);
% expected_out=0*ones(size(out));
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% 
% function test_HP_BV_2EMB_CLIO_Prog_dyn_HYZROUTE_rap_optimise_tt_therm(testCase)
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_HYZROUT','CLIO_1L5DCI_HP_BV_2EMB_NES_NHP','HP_BV_2EMB','param_backward_prog_dyn_test_2EMB_rap_opt_tt_thermique',1,1,50);
% conso100=XmlValue(ResXml,'conso100');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end)];
% %expected_out=[3.083915  -0.051512]; % correction bug calcul des limites le 07_05_12 (EV)
% %expected_out=[3.223785  -0.210989];
% %expected_out=[3.2042  -0.1621];
% expected_out=[3.205562 -0.263157]; % Suppression des points de recup avec pertes > puissance meca (07-05-16)
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% bilanP=XmlValue(ResXml,'bilanP');
% out=bilanP(2:end);
% expected_out=0*ones(size(out));
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% 
% function test_HP_BV_2EMB_BAT_SC_CLIO_lagrange(testCase)
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','CLIO_1L5DCI_HP_BV_2EMB_NES_BAT_SC','HP_BV_2EMB_SC','param_backward_lagrange_test_2EMB_BAT_SC',1,1);
% conso100=XmlValue(ResXml,'conso100');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end)];
% %expected_out=[3.403809034631151  -6.581297387870194];% modif calc_emb 09-11-11
% %expected_out=[3.412581710964736  -6.581297387870194];
% expected_out=[3.4038  -6.58134];
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
% 
% function test_HP_BV_2EMB_prog_dyn_fit_carto_NEDC(testCase)
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','CLIO_1L5DCI_HP_BV_2EMB_NES_NHP','HP_BV_2EMB','param_backward_prog_dyn_test_2EMB_fit_carto',1,1,50);
% conso100=XmlValue(ResXml,'conso100');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end)];
% %expected_out=[3.003972018076269   3.491732935086560];
% expected_out=[2.9979   3.4917];
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)

function test_HP_BV_2EMB_prog_dyn_308_dsoc0Bis(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB','HP_BV_2EMB','param_backward_prog_dyn_test_308_dsoc0',1,1,50);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[ 4.024626   0.000461];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
end

function test_HP_BV_2EMB_lagrange_308_cycleBis(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB','HP_BV_2EMB','param_backward_lagrange_2EMB_test_308_cycle',1,1,50);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[4.012473   0.073338];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
end

function test_HP_BV_2EMB_prog_dyn_308_dsoc2Bis(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB','HP_BV_2EMB','param_backward_prog_dyn_test_308',1,1,50);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[ 4.281367  -2.029538];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
end

function test_HP_BV_2EMB_lagrange_308_dsoc2Bis(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB','HP_BV_2EMB','param_backward_lagrange_2EMB_test_308',1,1,50);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[4.278060  -2.026147];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
end

function test_HP_BV_2EMB_lagrange_analytique_NEDC(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV_lisse','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB','HP_BV_2EMB','param_backward_lagrange_test_analytique_HP_BV_2EMB',1,1,50);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[3.7824  0.7229652]; % Sans auxiliaires et rendt coupleur parfait
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
end


