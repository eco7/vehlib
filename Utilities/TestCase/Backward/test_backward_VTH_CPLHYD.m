% function test_suite =test_backward_VTH_CPLHYD(testCase)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

function test_suite =test_backward_VTH_CPLHYD(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 
format long


function testVTH_CPLHYD_BUS_ECE15(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ECE15','BUS_CPLHYD','VTH_CPLHYD','param_backward_test_VTH',1,1);
conso100=XmlValue(ResXml,'conso100');
out=[conso100];
% expected_out=[39.445]; % en rev ??
%expected_out=[39.573]; % en rev 143
expected_out=[39.5072]; % Appel a la fonction resultats
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.01)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.01)
end

% function testVTH_CPLHYD_BUS_ECE15_2(testCase)
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ECE15','BUS_CPLHYD','VTH_CPLHYD','param_backward_10Hz_test',1,1);
% conso100=XmlValue(ResXml,'conso100');
% out=[conso100];
% % expected_out=[38.230]; % en rev ??
% expected_out=[38.613]; % en rev 143
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.01)

function testVTH_CPLHYD_BUS_RS(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_BUS_RS','BUS_CPLHYD','VTH_CPLHYD','param_backward_test_VTH',1,1);
conso100=XmlValue(ResXml,'conso100');
out=[conso100];
% expected_out=[44.429]; % en rev ??
%expected_out=[44.552]; % en rev 143
expected_out=[43.756]; % en rev 207 
% ! differents choix de rapport pour les points ou le moteur thermique ne
% satisfait pas la dynamique demandee.
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.01)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.01)
end

function testVTH_CPLHYD_BUS_RS_rap_opt(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_BUS_RS','BUS_CPLHYD','VTH_CPLHYD','param_backward_VTH_BV_rap_opt',1,1);
conso100=XmlValue(ResXml,'conso100');
out=[conso100];
expected_out=[37.228]; % en rev 207
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.01)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.01)
end

function testVTH_CPLHYD_BUS_SORT1(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_SORT1_V2T','BUS_CPLHYD','VTH_CPLHYD','param_backward_test_VTH',1,1);
conso100=XmlValue(ResXml,'conso100');
out=[conso100];
%expected_out=[56.055]; % en rev 207
expected_out=[55.9274]; % Appel a la fonction resultats
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.01)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.01)
end

function testVTH_CPLHYD_BUS_SORT2(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_SORT2_V2T','BUS_CPLHYD','VTH_CPLHYD','param_backward_test_VTH',1,1);
conso100=XmlValue(ResXml,'conso100');
out=[conso100];
%expected_out=[44.813]; % en rev 207
expected_out=[44.74038]; % Appel a la fonction resultats
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.01)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.01)
end

function testVTH_CPLHYD_BUS_SORT2_rap_opt(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_SORT2_V2T','BUS_CPLHYD','VTH_CPLHYD','param_backward_VTH_BV_rap_opt',1,1);
conso100=XmlValue(ResXml,'conso100');
out=[conso100];
%expected_out=[44.375]; % en rev 207
expected_out=[44.3028]; % Appel a la fonction resultats
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.01)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.01)
end


function testVTH_CPLHYD_BUS_SORT3(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_SORT3_V2T','BUS_CPLHYD','VTH_CPLHYD','param_backward_test_VTH',1,1);
conso100=XmlValue(ResXml,'conso100');
out=[conso100];
%expected_out=[39.763]; % en rev 207
expected_out=[39.7175]; % Appel a la fonction resultats
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.01)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.01)
end
