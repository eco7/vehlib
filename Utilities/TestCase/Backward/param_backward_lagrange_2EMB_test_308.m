% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???
% 26-08-2011 (EV) : version 1
%

% d??finition pas de temps
param.pas_temps=1;

% d??finition pas courant
param.pas_isc=5;

% definition pas couple 
param.pas_cprim2_cpl=0.1;

% Parametre pour debugger
param.verbose=0;

% autorisation mode tout thermique
param.tout_thermique=0;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='lagrange';

% Convergence dans un intervalle ou calcul pour un parametre de lagrange fixe ?
param.cvgce=0;

param.fit_pertes_moth=0;

param.fit_pertes_acm=0;

% Valeur du parametre de lagrange
%param.lambda=3.17; % NES_NHP plus deux batt exide plus ral moth ?? MOTH.ral dans calc_emb NEDC_BV
%param.lambda=3.245; % NES_NHP plus deux batt exide plus ral moth ?? ECU.wmt_patinage dans calc_emb
%param.lambda=2.97292; 
param.lambda= 2.890154871806612;

%param.lambda= 2;


param.lambda_cst='oui';


% tdebug pour donner l'instant ou l'on veut tracer la fonction (0 sinon)
param.tdebug=0;

