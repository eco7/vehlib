% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???
% 26-08-2011 (EV) : version 1
%

% definition pas de temps
param.pas_temps=1;

% definition pas courant
param.pas_isc=5;

% definition pas couple 
param.pas_cprim2_cpl=0.1;

% penalite obj sur les Polluant
param.K=0.1;

% Parametre pour debugger
param.verbose=0;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='lagrange';

% Valeur maxi du hamiltonien lorsqu'aucune soluton n'est envisageable
param.Ham_hors_limite=Inf;

% Convergence dans un intervalle ou calcul pour un parametre de lagrange fixe ?
param.cvgce=1;

% Recherche mode tout thermique
param.tout_thermique=0;

% Valeur du parametre de lagrange 
param.lambda1_cst='oui';
param.lambda2_cst='non';

 param.lambda1=3;
 param.lambda2=-100;

% Parametre de convergence
param.paramCvgce='param.lambda1';

% Fonction et critere de convergence (la structure s'appelle ResXml)
param.fonctionCvgce='soc=XmlValue(ResXml,''soc'');';
param.critereCvgce='soc(end)';

% tdebug pour donner l'instant ou l'on veut tracer la fonction (0 sinon)
%param.tdebug=[140 150];
param.tdebug=0;

