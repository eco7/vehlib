%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% INRETS - LTE Juin 2010
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???

% definition pas de temps
param.pas_temps=1;

% Parametre pour debugger
param.verbose=0;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='ligne';


% Autorisation du flux serie en freinage (1: autorise, 0: interdit) 
% non implemente
%param.freinage_flux_serie=1;

% strategie en ligne
% strat=0; % pas de strategie isc =0
% strat=1; % pbat = valeur moyenne de pres sur les Xdernieres valeurs
% strat=2; % pbat= filtre passe bas du premier ordre sur pres
% strat=3; % pbat= filtre passe bas du deuxieme ordre sur pres

param.strat=0;

%param.strat=1;
%param.t_moy=50;
%param.pini=30000;

%param.strat=2;
param.fc=1/500;
%param.pini=2100; % ECE 15

param.pini=3600; % deb_autonomie
%param.strat=3;
%param.fc2=1/100;

% param.strat=4;
% param.t_moy=200;
% param.pini=2000;
% param.tarret=20;

% Correction eventuel de la psc_cible pour essayer de ramener u0sc
% a la valeur initiale
%  psc_corr(t)=(K_corr*(u0sc(t-1)-u0sc(1)))*(P_pond_batt/pbat_cible(t));
param.K_corr=3.5*400; % si O pas de correction sur psc_cible
%param.P_pond_batt=400; % Puissance de ponderation pour la batterie si correction

