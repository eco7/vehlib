% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???
% 26-08-2011 (EV-BJ) : version 1

% definition pas de temps
param.pas_temps=1;
param.pas_temps_accel=0.5;

% Parametre pour debugger
param.verbose_accel=0;
param.verbose=0;

% Param???tre pour arr???ter la simulation lorsque l'on atteint 100 km/h
param.arret=0;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='prog_dyn';

% Calcul matriciel des cout (pour tout les points de tout le graphe en un
% calcul).
param.calc_mat=1;

param.fit_pertes_moth=0;

param.fit_pertes_acm=0;

% calcul des limites a partir du meilleur cehmin pr??c??demment trouv??
param.limite_chemin=0;

% calcul des 2 meilleur chemin
param.calc_2chemin=0;

% Precision graphe
%param.prec_graphe=-0.01; % en soc 
param.prec_graphe=-0.01; % en soc 
% Variation tension SC entre debut et fin du cycle
param.du0=0;


param.dsoc=2.026; % avec param.prec_graphe=-0.05;
% Recherche mode tout thermique
param.tout_thermique=0;

% limite sur soc min max
param.soc_max=80;
param.soc_min=20;

% Autorisation du flux serie en freinage (1: autorise, 0: interdit)
param.freinage_flux_serie=1;

% Initialisation des grandeurs
% param.var(1)=SC.Vc0*SC.Nblocser; % initialisation u0sc
% param.var(2)=100-INIT.Dod0; % initialisation soc
% param.var(3)=0; % initialisation ux
% 
% % nombre de var ?? conserver ?? 'linstant pr??cedent
param.Nb_var_p=0;