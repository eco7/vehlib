% function test_suite =test_backward_VTH_BV_2U_TWC(testCase)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
function test_suite =test_backward_VTH_BV_2U_TWC(testCase)
 if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
 end
end

function test_VTH_BV_2U(testCase)

% type d'optimisation
param.optim = '2U_TWC';

% Fichier parametres

param.verbose = 0;
param.MemLog = 0;
param.debug = 0;

% parametre de proportionalite sur la masse et le Cp du catalyseur
% 2 : catalyseur deux fois plus lourd (ou Cp deux fois plus grand)
% si non precise : valeur par defaut = 1
% param.k_cata = 2;

% forme du coefficient de convection
% saturation a 0 (satur=1) ou droite passant par zero (satur=0)
% valeur par defaut 1
% param.satur = 0;

%% parametres de la programmation dynamique
% initialisation du calcul de programmation dynamique
param.T_init = 298;
param.cost2go_init = 0;

% parametres de grille de temperature
param.pas_T = 0.1;

% poids relatif conso-pollu
param.alpha = 5;

% parametres de la commande
% choisir soit param.phi_cst soit param.phi_min/max/pas
% param.phi_cst = 1.0;
param.phi_min = 0.7;
param.phi_max = 1.1;
param.pas_phi = 0.01;

% choisir soit param.delta_AA_cst soit param.delta_AA_min/max/pas
% param.delta_AA_cst = 0;
param.delta_AA_min = -30;
param.delta_AA_max = 10;
param.pas_delta_AA = 2;

% debug : enregistrement de l'utilisation memoire 
% et des etats intermediaires pour pouvoir reprendre le calcul
param.pas_temps = 1;

param.champ_modif = {'MOTH.Vd'};
param.valeur_modif = [1.8];

param.pourquoi = 'test tout thermique';
param.comment = 'conso pollu a=5';

% Valeur de reference pour adimensionner : Phi = 1 et AA opti
param.ValueRef='Nominal'; % 'EURO4'
param.consoref = 6.2289;
param.COref = 0.5067;
param.HCref = 0.0526;
param.NOxref = 0.2106;

%[ERR, vehlib, VD, res]=VEHLIB_backward('CIN_test_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB_kokam_CONTROL','HP_BV_2EMB','param_backward_2EMB_3U_TWC;',1,1);

% Pour s'affranchir du fichier parametre
[vehlib, VD]=vehsim_auto2('Peugeot_308SW_1L6VTI16V_CONTROL','VTH_BV','CIN_NEDC_BV_lisse',40,2,0);
ERR = [];
% Lancement des calculs en mode backward
[ERR, vehlib, VD, res]=calc_backward(ERR,vehlib,param,VD);

out = [res.conso_lp100km res.CO_gpkm_rnd res.HC_gpkm_rnd res.NOx_gpkm_rnd];
expected_out = [7.5874 0.144 0.024 0.035];
tol = 1e-3;
	
verifyEqual( testCase, out, expected_out, 'AbsTol', tol);

end

function test_VTH_BV_2U_2(testCase)

% type d'optimisation
param.optim = '2U_TWC';

% Fichier parametres

param.verbose = 0;
param.MemLog = 0;
param.debug = 0;

% parametre de calcul de la courbe de Cmin pour le moteur thermique
% 'frot' : courbe de frottement modele DRIVE
% 'nul' : limite de couple a zero : interdire C<0
param.cmt_min = 'frot';

% parametre de proportionalite sur la masse et le Cp du catalyseur
% 2 : catalyseur deux fois plus lourd (ou Cp deux fois plus grand)
% si non precise : valeur par defaut = 1
% param.k_cata = 2;

% forme du coefficient de convection
% saturation a 0 (satur=1) ou droite passant par zero (satur=0)
% valeur par defaut 1
% param.satur = 0;

%% parametres de la programmation dynamique
% initialisation du calcul de programmation dynamique
param.T_init = 298;
param.cost2go_init = 0;

% parametres de grille de temperature
param.pas_T = 0.1;

% poids relatif conso-pollu
param.alpha = 0;

% parametres de la commande
% choisir soit param.phi_cst soit param.phi_min/max/pas
param.phi_cst = 1.0;
% param.phi_min = 0.7;
% param.phi_max = 1.1;
% param.pas_phi = 0.01;

% choisir soit param.delta_AA_cst soit param.delta_AA_min/max/pas
param.delta_AA_cst = 0;
% param.delta_AA_min = -30;
% param.delta_AA_max = 10;
% param.pas_delta_AA = 2;

% debug : enregistrement de l'utilisation memoire 
% et des etats intermediaires pour pouvoir reprendre le calcul
param.pas_temps = 1;

param.champ_modif = {'MOTH.Vd'};
param.valeur_modif = [1.8];

param.pourquoi = 'test tout thermique';
param.comment = 'conso seule a=0';

% Valeur de reference pour adimensionner : Phi = 1 et AA opti
param.ValueRef='Nominal'; % 'EURO4'
param.consoref = 6.2289;
param.COref = 0.5067;
param.HCref = 0.0526;
param.NOxref = 0.2106;

[vehlib, VD]=vehsim_auto2('Peugeot_308SW_1L6VTI16V_CONTROL','VTH_BV','CIN_NEDC_BV_lisse',40,2,0);
ERR = [];
% Lancement des calculs en mode backward
[ERR, vehlib, VD, res]=calc_backward(ERR,vehlib,param,VD);

% Comparaison des resultats
out = [res.conso_lp100km res.CO_gpkm_fwd res.HC_gpkm_fwd res.NOx_gpkm_fwd];
expected_out = [6.5740 0.5302 0.0563 0.2121];
tol = 1e-3;
	
verifyEqual( testCase, out, expected_out, 'AbsTol', tol);

end



%  function test_HP_BV_2EMB_308_TWC(testCase)
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB_TWC','HP_BV_2EMB','param_backward_lagrange_2EMB_TWC_test',1,1);
% conso100=XmlValue(ResXml,'conso100');
% co_gkm=XmlValue(ResXml,'co_gkm');
% hc_gkm=XmlValue(ResXml,'hc_gkm');
% nox_gkm=XmlValue(ResXml,'nox_gkm');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end) co_gkm hc_gkm nox_gkm];
% expected_out=[ 5.3235 -9.5855 1.3699 0.1943  0.2926]; 
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001);
% 
% function test_HP_BV_2EMB_308_TWC_cvgce(testCase)
% [vehlib, VD]=vehsim_auto2('Peugeot_308SW_1L6VTI16V_HP_BV_2EMB_TWC','HP_BV_2EMB','CIN_NEDC_BV',40,1,0);
% eval('param_backward_lagrange_2EMB_TWC_cvgce_test');
% ERR=struct([]);
% fct_bckw=@(param) calc_HP_BV_2EMB_TWC(ERR,vehlib,param,VD);
% [ERR,~,~,ResXml]=dicho(@(lambda) call_fct_backward(fct_bckw,param,lambda),60,0.1,[0 20],30,1,0);
% conso100=XmlValue(ResXml,'conso100');
% co_gkm=XmlValue(ResXml,'co_gkm');
% hc_gkm=XmlValue(ResXml,'hc_gkm');
% nox_gkm=XmlValue(ResXml,'nox_gkm');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end) co_gkm hc_gkm nox_gkm];
% expected_out=[4.9574 -0.03738 0.83189 0.1134 0.1569];
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001);
% 
% function test_HP_BV_2EMB_TWC_Conso(testCase)
% % Cas test avec lambda2 nulle : on doit optimiser la consommation seulement...
% [ERR, ~, ~, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB_TWC','HP_BV_2EMB','param_backward_lagrange_2EMB_TWC_test2',1,1,50);
% conso100=XmlValue(ResXml,'conso100');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end)];
% expected_out=[4.278164  -2.026995]; % Presque les memes resultats que HP_BV_2EMB ! [4.278060  -2.026147];
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001);

