% function test_suite =test_backward_VTH(testCase)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

function test_suite =test_backward_VTH(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 
format long

function testVTH_BV_CLIO_ECE15(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ECE15','CLIO_1L5DCI','VTH_BV','param_backward_test_VTH',1,1);
conso100=XmlValue(ResXml,'conso100');
out=[conso100];
expected_out=[4.9393]; % appel a fonction resultats
  expected_out=[ 4.90831];
%expected_out=[4.9499]; % Release 215
%expected_out=[5.065119]; % release 195
%expected_out=[4.9499]; Valeur avant la release r76 (2011-05-27)
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

function testVTH_BV_CLIO_ECE15_2(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ECE15','CLIO_1L5DCI','VTH_BV','param_backward_test_10Hz',1,1);
conso100=XmlValue(ResXml,'conso100');
out=[conso100];
%expected_out=[4.8417]; % appel a fonction resultats
 expected_out=[4.804340];
%expected_out=[4.8428]; % Release 215
%expected_out=[4.949]; % release 195
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

function testVTH_BV_CLIO_ECE15_3(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ECE15','CLIO_1L5DCI','VTH_BV','param_backward_VTH_BV_rap_opt',1,1);
conso100=XmlValue(ResXml,'conso100');
out=[conso100];
%expected_out=[3.8258];  % appel a fonction resultats
expected_out=[3.79567];
%expected_out=[3.8365]; % Release 215
%expected_out=[4.125];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

function testVTH_BV_EP6_NEDC_BV(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V','VTH_BV','param_backward_test_VTH',1,1);
conso100=XmlValue(ResXml,'conso100');
out=[conso100];
expected_out=[ 6.6816]; % Modif patinage a couple <0 dans calc_emb.m
%expected_out=[ 6.7033]; % appel a fonction resultats
%expected_out=[6.7049]; % Release 215
%expected_out=[6.7116];
%expected_out=[6.967]; % release 195
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

function testVTH_BV_EP6_TWC_NEDC_BV(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V_TWC','VTH_BV','param_backward_test_VTH',1,1);
conso100=XmlValue(ResXml,'conso100');
co_gkm=XmlValue(ResXml,'co_gkm');
hc_gkm=XmlValue(ResXml,'hc_gkm');
nox_gkm=XmlValue(ResXml,'nox_gkm');
out=[conso100 co_gkm hc_gkm nox_gkm];
%expected_out=[6.967 0.7879 0.1013 0.1213]; % Ancienne valeur de Texh
%expected_out=[6.967 1.0417 0.140 0.1873]; % Texh valeurs intermediaires
%expected_out=[6.967 1.994 0.285 0.4353]; %  Texh calculee a partir de la cartographie 2D - Release 195
%expected_out=[6.7116 3.3428 0.4911 0.7895]; INIT.Tcat0=450;
%expected_out=[6.7116 4.0564 0.5998 0.9753]; 
%expected_out=[6.7049  3.2695 0.4800 0.7705];  % Release 215
%expected_out=[6.7033  3.2695 0.4800 0.7705]; % appel a fonction resultats
%expected_out=[6.6816  3.3416 0.4910 0.7896]; % Modif patinage a couple <0 dans calc_emb.m

expected_out=[6.6815  0.6258 0.0774 0.0824]; % modified with 2016 TWC
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

function testVTH_BV_EP6_TWC_WLTC(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_WLTC_Classe3','Peugeot_308SW_1L6VTI16V_TWC','VTH_BV','param_backward_test_VTH',1,1);
conso100=XmlValue(ResXml,'conso100');
co_gkm=XmlValue(ResXml,'co_gkm');
hc_gkm=XmlValue(ResXml,'hc_gkm');
nox_gkm=XmlValue(ResXml,'nox_gkm');
out=[conso100 co_gkm hc_gkm nox_gkm];
%expected_out=[6.5824 1.6164 0.2285 0.3416]; 
%expected_out=[6.5824 0.5327 0.06348 0.05943]; % modified with 2016 TWC
expected_out=[6.57291  0.532162   0.063421 0.059398];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

function testVTH_BV_308SW_mt_niv0(testCase)
if exist('Peugeot_308SW_1L6VTI16V_mt_niv0','file') % fichiers Peugeot_308SW_1L6VTI16V_mt_niv0 et MTHERM_EP6_mt_niv0 dans le projet LTE
    [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV','Peugeot_308SW_1L6VTI16V_mt_niv0','VTH_BV','param_backward_test_VTH',1,1);
    conso100=XmlValue(ResXml,'conso100');
    out=[conso100];
    expected_out=[6.6837]; % Modif patinage a couple <0 dans calc_emb.m
    %expected_out=[6.7000]; % appel a fonction resultats
    %expected_out=[6.7015]; % Release 215
    %expected_out=[6.89723]; % Release 195
    %expected_out=[6.7068];
    if verLessThan('matlab','8.2')
        testCase=[];
        assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
    else
        verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
    end
    
end

function testVTH_BV_Kadjar_NEDC(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV6','Renault_Kadjar_TCE130','VTH_BV','param_backward_test_VTH',1,1);
conso100=XmlValue(ResXml,'conso100');
out=[conso100];
%expected_out=[6.3786];
expected_out=[6.2342]; % Modified auxiliary power consumption from 400 to 250W

if verLessThan('matlab','8.2')
    testCase=[];
    assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
    verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end





