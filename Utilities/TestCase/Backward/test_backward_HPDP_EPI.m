% function test_suite =test_backward_HP_BV_2EMB(testCase)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 09-11-11 (EV) : version 1
%
% lancement runtests('test_backward_HP_BV_2EMB') ou runtests seul pour tout
% lancer

function test_suite =test_backward_HPDP_EPI(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 


function test_HPDP_EPI_PRIUS_Prog_dyn_ARTAUTO(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTAUTO','PRIUS_II','HPDP_EPI_SURV','param_backward_prog_dyn_test_HPDP_EPI',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
%expected_out=[5.5820    0.0373]; 
%expected_out=[5.5916    0.0373];  % rajout limitation sur cmt en amont du calcul du bilan de puissance L89
%expected_out=[5.5912    0.0024];  % prise en compte frein meca quand acm1 non alimente en recup car trop de pertes
expected_out= [5.61238    0.04686]; % correction bug prise en compte limites elec et couple max MOTH dans les donn??es
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EPI_PRIUS_Prog_dyn_ARTAUTO : max bilanP>20')

else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end



function test_HPDP_EPI_PRIUS_Prog_dyn_ARTROUT(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTROUT','PRIUS_II','HPDP_EPI_SURV','param_backward_prog_dyn_test_HPDP_EPI',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
%expected_out=[3.8592    0.0405]; 
%expected_out=[3.8629    0.0405]; % rajout limitation sur cmt en amont du calcul du bilan de puissance L89
%expected_out=[3.8571    0.0404];  % prise en compte frein meca quand acm1 non alimente en recup car trop de pertes
expected_out=[3.9390    0.0284]; % correction bug prise en compte limites elec et couple max MOTH dans les donn??es
bilanP=XmlValue(ResXml,'bilanP');

if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EPI_PRIUS_Prog_dyn_ARTROUT : max bilanP>20');
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end



function test_HPDP_EPI_PRIUS_Prog_dyn_fit_carto_NEDC_ne(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_ne','PRIUS_II','HPDP_EPI_SURV', 'param_backward_prog_dyn_test_HPDP_EPI_fit_carto_ne',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
%expected_out=[3.73154   0.00039];  % 06/02/14: release 174, suite a modif des cartographies de la prius (moteur et mvv) dans vehlib (R154) 
expected_out=[3.74260    0.00858];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EPI_PRIUS_Prog_dyn_ARTAUTO : max bilanP>20');
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end



function test_HPDP_EPI_PRIUS_Prog_dyn_fit_carto_HYZAUTO(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_HYZAUTO','PRIUS_II','HPDP_EPI_SURV', 'param_backward_prog_dyn_test_HPDP_EPI_fit_carto',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
% expected_out=[ 5.3201    0.0378];  % correction bug chemin2varcom_mat elhyb==0.5 et pas 2 quand moth tourne
%expected_out=[ 5.3205   0.03777]; % correction bug couple max MOTH dans les donn??es
%expected_out=[ 5.3213   0.03777];  % 06/02/14: release 174, suite a modif des cartographies de la prius (moteur et mvv) dans vehlib (R154)
expected_out=[ 5.3183    0.02441];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EPI_PRIUS_Prog_dyn_HYZAUTO : max bilanP>20');
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end





function test_HPDP_EPI_PRIUS_Prog_dyn_fit_carto_HYZURB(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_HYZURB','PRIUS_II','HPDP_EPI_SURV', 'param_backward_prog_dyn_test_HPDP_EPI_fit_carto',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
%expected_out=[ 3.7469    0.0026];  % 06/02/14: release 174, suite a modif des cartographies de la prius (moteur et mvv) dans vehlib (R154)
expected_out=[ 3.7642    0.0058];  
bilanP=XmlValue(ResXml,'bilanP');

if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EPI_PRIUS_Prog_dyn_ARTAUTO : max bilanP>20');
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end

%% Cas carto 3D plus presence DC/DC
function test_HPDP_EPI_PRIUS_Prog_dyn_carto_3D_ARTURB(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTURB','PRIUS_II_carto_3D','HPDP_EPI_SURV','param_backward_prog_dyn_test_HPDP_EPI_carto_3D',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[ 4.2951   0.01279    ];  
%bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
   % assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EPI_PRIUS_Prog_dyn_ARTAUTO : max bilanP>20');
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    %verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end


function test_HPDP_EPI_PRIUS_Prog_dyn_carto_3D_ARTAUTO(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTAUTO','PRIUS_II_carto_3D','HPDP_EPI_SURV','param_backward_prog_dyn_test_HPDP_EPI_carto_3D',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)]; 
expected_out=[ 5.7585   0.00724];

%bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    %assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EPI_PRIUS_Prog_dyn_ARTAUTO : max bilanP>20');
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    %verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end



