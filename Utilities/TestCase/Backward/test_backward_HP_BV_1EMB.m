% function test_suite =test_backward_HP_BV_1EMB(testCase)
%
% (C) Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%

function test_suite =test_backward_HP_BV_1EMB(testCase)
 if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end;

function test_HP_BV_1EMB_308_TWC_WLTC_Classe3(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_WLTC_Classe3_BV','Peugeot_308SW_1L6VTI16V_HP_BV_1EMB_TWC','HP_BV_1EMB','param_backward_lagrange_test_1EMB_TWC',1,1);
conso100=XmlValue(ResXml,'conso100');
co_gkm=XmlValue(ResXml,'co_gkm');
hc_gkm=XmlValue(ResXml,'hc_gkm');
nox_gkm=XmlValue(ResXml,'nox_gkm');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end) co_gkm hc_gkm nox_gkm];
%expected_out=[5.3504 -0.0523 1.8891 0.2734 0.4274]; % lambda1= 2.270996; K=0; lambda2=0;
%expected_out=[5.3504 -0.0523 0.4540 0.0547 0.0537]; % lambda1= 2.270996; K=0; lambda2=0; 2016 TWC model
%expected_out=[5.804 -0.1667 0.5237 0.0642 0.0664]; % lambda1=2.282031 ; K=0; lambda2=0; Changing Battery and EM
expected_out=[5.8584 -1.18897 0.52696 0.064545 0.066645]; % Le cycle WLTC_Classe3_BV est adapté pour le banc moteur
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end
2839432976 
function test_HP_BV_1EMB_308_WLTC_Classe3(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_WLTC_Classe3','Peugeot_308SW_1L6VTI16V_HP_BV_1EMB_TWC','HP_BV_1EMB','param_backward_lagrange_test_1EMB',1,1);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
co_gkm=XmlValue(ResXml,'co_gkm');
hc_gkm=XmlValue(ResXml,'hc_gkm');
nox_gkm=XmlValue(ResXml,'nox_gkm');
out=[conso100 soc(1)-soc(end) co_gkm hc_gkm nox_gkm];
%expected_out=[5.4355 0.0091]; % lambda1=2.41015625;  K=0; lambda2=0;
%expected_out=[5.2868 0.00096 0.44789 0.0540 0.05294]; 
expected_out=[5.2868 -0.24526   0.44789   0.0540   0.05294];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

function test_HP_BV_1EMB_308_TWC_WLTC_cvgce(testCase)
[vehlib, VD]=vehsim_auto2('Peugeot_308SW_1L6VTI16V_HP_BV_1EMB_TWC','HP_BV_1EMB','CIN_WLTC_Classe3',40,1,0);
eval('param_backward_lagrange_test_1EMB_TWC_cvgce');
ERR=struct([]);
fct_bckw=@(param) calc_HP_BV_1EMB(ERR,vehlib,param,VD);
%[ERR,lambda1,~,ResXml]=dicho(@(lambda1) call_fct_backward(fct_bckw,param,lambda1),60,0.1,[4.828125 5],15,1,1);
[ERR,lambda1,~,ResXml]=dicho(@(lambda1) call_fct_backward(fct_bckw,param,lambda1),60,0.1,[4.795312-0.01 4.795312+0.01],15,1,1);
conso100=XmlValue(ResXml,'conso100');
co_gkm=XmlValue(ResXml,'co_gkm');
hc_gkm=XmlValue(ResXml,'hc_gkm');
nox_gkm=XmlValue(ResXml,'nox_gkm');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end) co_gkm hc_gkm nox_gkm];
%expected_out=[5.36080 0.01329 1.83532 0.26515 0.41329];
%expected_out=[5.3166 -0.0825 0.5137 0.0639 0.0697];% 2016 TWC model and with battery PACK_A123_20Ah_20kW
expected_out=[5.3691  -0.0667   0.53694   0.06737   0.075112];
expected_out=[5.37780  0.04157  0.641564  0.083289  0.1022721];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.01);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.01);
end
