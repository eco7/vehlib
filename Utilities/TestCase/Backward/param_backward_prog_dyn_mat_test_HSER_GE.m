% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???
% 23-07-2012 (EV) : version 1

% d??finition pas de temps
param.pas_temps=1;

% Parametre pour debugger
param.verbose=0;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='prog_dyn';

% Calcul matriciel des cout (pour tout les points de tout le graphe en un
% calcul).
param.calc_mat=1;

% Precision graphe
param.prec_graphe=-0.05; % en soc 
%param.modulo_prec_graph=1;

param.dsoc=0; % avec param.prec_graphe=-0.01;
%param.dsoc=1.463; % avec param.prec_graphe=-0.005;

% limite sur soc min max
param.soc_max=80;
param.soc_min=20;

% Autorisation du flux serie en freinage (1: autorise, 0: interdit)
param.freinage_flux_serie=1;

% Initialisation des grandeurs
param.var(2)=100-VD.INIT.Dod0; % initialisation soc

% nombre de var ?? conserver ?? 'linstant pr??cedent
param.Nb_var_p=0;

% mode : 0 tout electrique interdit, 1 tout electrique autorise, 2 stop start
param.mode_elect=1;

% saturation a zero pour les faible puissance negative du groupe
% electrogene
param.pge_satur=-1e-6;