function test_suite =test_backward_HSER_GE(testCase)

if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 

function test_backward_HSER_GE_lagrange(testCase)

format long
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC','Peugeot_308_HYBS_PVAR','HSER_GE','param_backward_lagrange_test_HSER_GE',1,1,40);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(end)-soc(1)];
expected_out=[ 3.8908 -0.4442];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

function test_backward_HSER_GE_lagrange_dicho(testCase)

format long
[vehlib, VD]=vehsim_auto2('Peugeot_308_HYBS_PVAR','HSER_GE','CIN_NEDC',40, 1,0);
eval('param_backward_lagrange_cvgce_test_HSER_GE');
ERR=struct([]);
fct_bckw=@(param) calc_HSER_GE(ERR,vehlib,param,VD);
[ERR,X,Y,ResXml]=dicho(@(lambda) call_fct_backward(fct_bckw,param,lambda),60,1.0,[2 4],30,1,0);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(end)-soc(1)];
expected_out=[ 3.891 -0.444];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.01)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.01)
end

function test_backward_HSER_GE_Prog_dyn(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC','Peugeot_308_HYBS_PVAR','HSER_GE','param_backward_prog_dyn_test_HSER_GE',1,1,40);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(end)-soc(1)];
% expected_out=[5.271 0.003]; %precision 0.02
% expected_out=[ 3.9526 0.1178]; %precision 0.05 et Dsoc =0
expected_out=[ 3.9883   -0.0278];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

function test_backward_HSER_GE_Prog_dyn_mat(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTROUT','Peugeot_308_HYBS_PVAR','HSER_GE','param_backward_prog_dyn_mat_test_HSER_GE',1,1,40);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(end)-soc(1)];
expected_out=[ 4.1542    -0.0215];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

function test_backward_HSER_GE_Prog_dyn_periodes_ZEV(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_HYZURB','Peugeot_308_HYBS_PVAR','HSER_GE','param_backward_prog_dyn_periodes_ZEV_test_HSER_GE',1,1,40);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(end)-soc(1)];
expected_out=[ 1.5988   -5.0006];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

function test_backward_HSER_GE_RE_max(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC','Peugeot_308_HYBS_PVAR','HSER_GE','param_backward_RE_max_test_HSER_GE',1,1,40);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(end)-soc(1)];
% expected_out=[ 10.3891 -19.5246]; % jusqu'a r164, et la modif de puissance_opti_carto
%expected_out=[ 10.4128 19.5246]; % Release 215
expected_out=[10.3852  19.5246]; % Appel a la fonction resultats
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

function test_backward_HSER_GE_tout_elec(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC','Peugeot_308_HYBS_PVAR','HSER_GE','param_backward_tout_elec_test_HSER_GE',1,1,40);
conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(end)-soc(1)];
expected_out=[ 0  -23.4894];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end