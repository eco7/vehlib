% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???
% 26-08-2011 (EV-BJ) : version 1

% définition pas de temps
param.pas_temps=1;

% Parametre pour debugger
param.verbose=0;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='prog_dyn';

% calcul matricielle
param.calc_mat=1;

% calcul des pertes par formule
param.fit_pertes=0;

% Precision graphe
param.prec_graphe=-0.05; % en soc 

param.pas_wacm2=50*pi/30; % precision sur la discretisation des vitesses pour le choix de wmt on tombera 
param.pas_cacm1=5;      % precision sur discretisation couple dans calcul des limites en recup
param.pas_cmt=5;        % precision sur la discretisation des couples pour la résolution du système (NB on interpole linéairement entre les valeurs).
param.pas_wmt=50*pi/30;        % precision sur la discretisation des couples piur la résolution du système (NB on interpole linéairement entre les valeurs).

%param.dsoc=-0.103; % avec param.prec_graphe=-0.01;
param.dsoc=0; % avec param.prec_graphe=-0.05;
% Recherche mode tout thermique
% param.tout_thermique=0;

% limite sur soc min max
param.soc_max=70;
param.soc_min=20;

% Autorisation du flux serie en freinage (1: autorise, 0: interdit)
param.freinage_flux_serie=1;

% Existance du mode "split" en tout elec
param.elec_split=1;

% Existance du mode hybrid serie
param.hybrid_serie=1;

% limitation de vit max elec à ECU.vwaxele
param.exist_vit_max_elec=1;

% Initialisation des grandeurs
% param.var(1)=SC.Vc0*SC.Nblocser; % initialisation u0sc
% param.var(2)=100-INIT.Dod0; % initialisation soc
% param.var(3)=0; % initialisation ux
% 
% % nombre de var à conserver à 'linstant précedent
param.Nb_var_p=0;