%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% INRETS - LTE Juin 2010
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???

% d??finition pas de temps
param.pas_temps=1;

% Parametre pour debugger
param.verbose=0;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='ligne';

% Type de strategie en ligne
param.strat=0;

% Regulateur de soc cible
param.soc_cible=40;
param.kp=0.01;
param.ki=0.0003;

% Frequence de coupure
param.fc=1/10;

% mode : 0 tout electrique interdit, 1 tout electrique autorise, 2 stop start
param.mode_elect=2;

param.pmin_ge=5000;

param.pini=param.pmin_ge;
% saturation a zero pour les faible puissance negative du groupe
% electrogene
param.pge_satur=-1e-6;