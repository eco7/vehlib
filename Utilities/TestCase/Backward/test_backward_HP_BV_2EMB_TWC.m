% function test_suite =test_backward_HP_BV_2EMB(testCase)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

function test_suite =test_backward_HP_BV_2EMB_TWC(testCase)
 if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
 end
end
% 
% function test_tmp(testCase)
% if verLessThan('matlab','8.2')
% 	testCase=[];
% 	assertVectorsAlmostEqual(1,1,'absolute',0.0001)
% else
% 	verifyEqual( testCase, 1, 1, 'AbsTol', 0.0001)
% end
% end


function test_HP_BV_2EMB_3U_308SW(testCase)

% type d'optimisation
param.optim = '3U_TWC';

% Fichier parametres
param.verbose = 0;
param.MemLog = 0;

% temperature pour le calcul des parametres batterie
param.tbat = 20;

% parametre de calcul des courbes de Cmin/Cmax pour la machine electrique
param.Ures = 600;

% puissance de la machine electrique
param.Pacm1 = 25000;

% initialisation du calcul de programmation dynamique
param.cost2go_init = 0;

% parametres de grille de soc
%param.pas_soc = 0.02;
param.pas_p0batt_theo = 850; % exprime en puissance
param.soc_max = 80;
param.soc_min = 20;

% parametres de grille de temperature
param.pas_T = 4;

% poids relatif conso-pollu
param.alpha = 5/3;
param.beta = 5/3;
param.gamma = 5/3;

% delta de soc a la fin du calcul :
% soc_fin = soc_init + param.dsoc;
param.dsoc = 0;

% parametres de la commande
param.phi_min = 0.99;
param.phi_max = 1.01;
param.pas_phi = 0.01;

param.delta_AA_min = -30;
param.delta_AA_max = 0;
param.pas_delta_AA = 10;

param.pas_T_var = 0;
param.pas_temps = 2;

% Taille maxi pour les arcs
%param.max_size_XU = 5e9; % 256Gb Ram
param.max_size_XU = 5e9/16; % 16Gb Ram

% calcul de l'acceleration centree
param.calc_accel=3;

%param.lg_calcul = 30;
param.fit_pertes_acm = 0;

% Optimisation des ecritures au detriment de la lisibilite pour diminuer
% l'usage de la ram (cf graphe...)
param.optiRam = 1;

% Utilisation de csortrows (csortrows.c, algorithme de tri-merge sort- ecrit en langage C) en lieu et place 
% de sortrows pour gain de rapidite et usage ram (cf graphe...)
param.versionC = 1;

% programmation dynamique
param.online = 0;

% Valeur de reference pour adimensionner : Phi = 1 et AA opti (svn Release 640)
param.ValueRef='EURO6'; % 'EURO4';

%[ERR, vehlib, VD, res]=VEHLIB_backward('CIN_test_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB_kokam_CONTROL','HP_BV_2EMB','param_backward_2EMB_3U_TWC;',1,1);
numcal_increment;

% Pour s'affranchir du fichier parametre
[vehlib, VD]=vehsim_auto2('Peugeot_308SW_1L6VTI16V_HP_BV_2EMB_kokam_CONTROL','HP_BV_2EMB','CIN_ECE15_BV',40,2,0);
ERR = [];
% Lancement des calculs en mode backward
[ERR, vehlib, VD, res]=calc_backward(ERR,vehlib,param,VD);

out = [res.conso_lp100km res.CO_gpkm res.HC_gpkm res.NOx_gpkm];
%expected_out = [3.9714 0.7854 0.01299 0.09127]; % Bug sur calcul tension batterie !
%expected_out = [3.6029 0.9252 0.0207 0.0435]; % Ajout auxiliaires electriques
%expected_out = [3.786135742297747   0.935880966823322   0.021652771122560   0.045336158764176];
expected_out = [3.908732007891608   0.934615725387635   0.021443277702632   0.044249657544761];
tol = 1e-3;
	
verifyEqual( testCase, out, expected_out, 'AbsTol', tol);

end

function test_HP_BV_2EMB_3U_Kadjar_ECE15(testCase)

% type d'optimisation
param.optim = '3U_TWC';

% Fichier parametres
param.verbose = 0;
param.MemLog = 0;

% temperature pour le calcul des parametres batterie
param.tbat = 20;

% parametre de calcul des courbes de Cmin/Cmax pour la machine electrique
param.Ures = 600;

% puissance de la machine electrique
param.Pacm1 = 25000;

% initialisation du calcul de programmation dynamique
param.cost2go_init = 0;

% parametres de grille de soc
%param.pas_soc = 0.02;
param.pas_p0batt_theo = 850; % exprime en puissance
param.soc_max = 80;
param.soc_min = 20;

% parametres de grille de temperature
param.pas_T = 4;

% poids relatif conso-pollu
param.alpha = 5/3;
param.beta = 5/3;
param.gamma = 5/3;

% delta de soc a la fin du calcul :
% soc_fin = soc_init + param.dsoc;
param.dsoc = 0;

% parametres de la commande
param.phi_min = 0.99;
param.phi_max = 1.01;
param.pas_phi = 0.01;

param.delta_AA_min = -30;
param.delta_AA_max = 0;
param.pas_delta_AA = 10;

param.pas_T_var = 0;
param.pas_temps = 2;

% Taille maxi pour les arcs
%param.max_size_XU = 5e9; % 256Gb Ram
param.max_size_XU = 5e9/16; % 16Gb Ram

% calcul de l'acceleration centree
param.calc_accel=3;

%param.lg_calcul = 30;
param.fit_pertes_acm = 0;

% Optimisation des ecritures au detriment de la lisibilite pour diminuer
% l'usage de la ram (cf graphe...)
param.optiRam = 1;

% Utilisation de csortrows (csortrows.c, algorithme de tri-merge sort- ecrit en langage C) en lieu et place 
% de sortrows pour gain de rapidite et usage ram (cf graphe...)
param.versionC = 1;

% programmation dynamique
param.online = 0;

% Valeur de reference pour adimensionner : Phi = 1 et AA opti (svn Release 640)
param.ValueRef='EURO6'; % 'EURO4';

%[ERR, vehlib, VD, res]=VEHLIB_backward('CIN_test_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB_kokam_CONTROL','HP_BV_2EMB','param_backward_2EMB_3U_TWC;',1,1);
numcal_increment;

% Pour s'affranchir du fichier parametre
[vehlib, VD]=vehsim_auto2('Renault_Kadjar_TCE130_HP_BV_2EMB_CONTROL','HP_BV_2EMB','CIN_ECE15_BV',40,2,0);
ERR = [];
% Lancement des calculs en mode backward
[ERR, vehlib, VD, res]=calc_backward(ERR,vehlib,param,VD);

out = [res.conso_lp100km res.CO_gpkm res.HC_gpkm res.NOx_gpkm];
%expected_out = [4.124827530177860   1.120485260824715   0.023285260655927   0.050763050104293];
expected_out = [4.285518801096007   1.061993423763912   0.023121103955871   0.050587350346938];
tol = 1e-3;
	
verifyEqual( testCase, out, expected_out, 'AbsTol', tol);

end

function test_HP_BV_2EMB_3U_Kadjar_NEDC(testCase)

% type d'optimisation
param.optim = '3U_TWC';

% Fichier parametres
param.verbose = 0;
param.MemLog = 0;

% temperature pour le calcul des parametres batterie
param.tbat = 20;

% parametre de calcul des courbes de Cmin/Cmax pour la machine electrique
param.Ures = 600;

% puissance de la machine electrique
param.Pacm1 = 25000;

% initialisation du calcul de programmation dynamique
param.cost2go_init = 0;

% parametres de grille de soc
%param.pas_soc = 0.02;
param.pas_p0batt_theo = 850; % exprime en puissance
param.soc_max = 80;
param.soc_min = 20;

% parametres de grille de temperature
param.pas_T = 100;

% poids relatif conso-pollu
param.alpha = 0;
param.beta = 0;
param.gamma = 0;

% delta de soc a la fin du calcul :
% soc_fin = soc_init + param.dsoc;
param.dsoc = 0;

% parametres de la commande
% param.phi_min = 0.993;
% param.phi_max = 1.008;
% param.pas_phi = 0.015;
param.phi_cst = 1.;

% param.delta_AA_min = -30;
% param.delta_AA_max = 0;
% param.pas_delta_AA = 10;
param.delta_AA_cst = 0;


param.pas_T_var = 0;
param.pas_temps = 1;

% Taille maxi pour les arcs
%param.max_size_XU = 5e9; % 256Gb Ram
param.max_size_XU = 5e9/16; % 16Gb Ram

% calcul de l'acceleration centree
param.calc_accel=3;

%param.lg_calcul = 30;
param.fit_pertes_acm = 0;

% Optimisation des ecritures au detriment de la lisibilite pour diminuer
% l'usage de la ram (cf graphe...)
param.optiRam = 1;

% Utilisation de csortrows (csortrows.c, algorithme de tri-merge sort- ecrit en langage C) en lieu et place 
% de sortrows pour gain de rapidite et usage ram (cf graphe...)
param.versionC = 1;

% programmation dynamique
param.online = 0;

% Valeur de reference pour adimensionner : Phi = 1 et AA opti (svn Release 640)
param.ValueRef='EURO6'; % 'EURO4';

%[ERR, vehlib, VD, res]=VEHLIB_backward('CIN_test_BV','Peugeot_308SW_1L6VTI16V_HP_BV_2EMB_kokam_CONTROL','HP_BV_2EMB','param_backward_2EMB_3U_TWC;',1,1);
numcal_increment;

% Pour s'affranchir du fichier parametre
[vehlib, VD]=vehsim_auto2('Renault_Kadjar_TCE130_HP_BV_2EMB_CONTROL','HP_BV_2EMB','CIN_NEDC_BV_lisse',45,2,0);
ERR = [];
% Lancement des calculs en mode backward
[ERR, vehlib, VD, res]=calc_backward(ERR,vehlib,param,VD);

out = [res.conso_lp100km res.CO_gpkm res.HC_gpkm res.NOx_gpkm];
%expected_out = [3.9113    0.2431    0.0159    0.2186];
%expected_out = [4.097901439116097   0.227075149526308   0.014055376013073   0.226763903612566];
expected_out = [4.097901439116114   0.229787812207636   0.014336870404243   0.227286541425483];
tol = 1e-3;
	
verifyEqual( testCase, out, expected_out, 'AbsTol', tol);

end

