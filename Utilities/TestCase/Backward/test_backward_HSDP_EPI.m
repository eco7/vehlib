% function test_suite =test_backward_HSP(testCase)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 23-07-12(EV): Creation 

function test_suite =test_backward_HSDP_EPI(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 


function test_HSDP_EPI_Prog_dyn_ARTROUT(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTROUT','PRIUS_HSDP_EPI','HSDP_EPI','param_backward_prog_dyn_test_HSDP_EPI',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[3.82204  0.02408];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HSDP_EPI_Prog_dyn_ARTROUT : max bilanP>20')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end




function test_HSDP_EPI_Prog_dyn_NEDC(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC','PRIUS_HSDP_EPI','HSDP_EPI','param_backward_prog_dyn_test_HSDP_EPI',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[ 3.53492   0.04565];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HSDP_EPI_Prog_dyn_NEDC : max bilanP>20')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end



function test_HPDP_EPI_HSP_2EMB_Prog_dyn_HYZAUTO(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_HYZAUTO','PRIUS_HSDP_EPI','HSDP_EPI','param_backward_prog_dyn_test_HSDP_EPI',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[5.19587   0.04431];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HSDP_EPI_Prog_dyn_HYZAUTO : max bilanP>20')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end




