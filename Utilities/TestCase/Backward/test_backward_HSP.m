% function test_suite =test_backward_HSP(testCase)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 23-07-12(EV): Creation 

function test_suite =test_backward_HSP(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 


function test_HPDP_HSP_2EMB_Prog_dyn_ARTROUT(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTROUT','PRIUS_HSP_2EMB','HSP_2EMB', 'param_backward_prog_dyn_test_HSP_2EMB',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)]; 
%expected_out=[4.01457  0.044819];
expected_out=[3.94409   0.03142]; % Changement des composants pour etre coherent avec HSP_BV
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EPI_PRIUS_Prog_dyn_ARTAUTO : max bilanP>20')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)   
end





function test_HPDP_HSP_2EMB_Prog_dyn_NEDC(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC','PRIUS_HSP_2EMB','HSP_2EMB', 'param_backward_prog_dyn_test_HSP_2EMB',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
%expected_out=[ 3.85144   0.04133]; % 06/02/14: release 174, suite a modif des cartographies de la prius (moteur et mvv) dans vehlib (R154)
%expected_out=[ 3.85158   0.04134];
%expected_out=[ 3.8473   0.04134];
%expected_out=[ 3.8497   0.04134];
expected_out=[3.70209   0.01832]; % Changement des composants pour etre coherent avec HSP_BV
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EPI_PRIUS_Prog_dyn_ARTAUTO : max bilanP>20')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end




% function test_HPDP_HSP_2EMB_Prog_dyn_HYZAUTO(testCase)
% 
% [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_HYZAUTO','PRIUS_HSP_2EMB','HSP_2EMB', 'param_backward_prog_dyn_test_HSP_2EMB',1,1,50);
% 
% conso100=XmlValue(ResXml,'conso100');
% soc=XmlValue(ResXml,'soc');
% out=[conso100 soc(1)-soc(end)];
% % expected_out=[5.87345   0.04641]; % Correction valeur MOTH.wmt_maxi PRIUS pour passer accel max
% % expected_out=[5.87368   0.04641];
% expected_out=[5.47717  0.03453]; % Changement des composants pour etre coherent avec HSP_BV
% bilanP=XmlValue(ResXml,'bilanP');
% if verLessThan('matlab','8.2')
% 	testCase=[];
% 	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
%     assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EPI_PRIUS_Prog_dyn_ARTAUTO : max bilanP>20')
% else
% 	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
%     verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
% end





function test_HPDP_HSP_1EMB_Prog_dyn_ARTAUTO(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTAUTO','PRIUS_HSP_1EMB','HSP_1EMB', 'param_backward_prog_dyn_test_HSP_1EMB',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
%expected_out=[6.03554   0.00339];
expected_out=[6.03519   0.00339];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EPI_PRIUS_Prog_dyn_ARTAUTO : max bilanP>20')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end




function test_HPDP_HSP_1EMB_Prog_dyn_ARTURB(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTURB','PRIUS_HSP_1EMB','HSP_1EMB', 'param_backward_prog_dyn_test_HSP_1EMB',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
%expected_out=[4.74587   0.04674];
expected_out=[4.7361   0.04674];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EPI_PRIUS_Prog_dyn_ARTAUTO : max bilanP>20')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end

