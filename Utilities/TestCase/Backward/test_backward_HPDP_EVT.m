% function test_suite =test_backward_HP_BV_2EMB(testCase)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 09-11-11 (EV) : version 1

function test_suite =test_backward_HPDP_EVT(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 


function test_HPDP_EVT_PRIUS_Prog_dyn_ARTAUTO(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTAUTO','PRIUS_EVT','HPDP_EVT_SURV_RED','param_backward_prog_dyn_test_HPDP_EVT',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
% expected_out=[ 6.1666    0.0372 ];  % Correction valeur MOTH.wmt_maxi PRIUS pour passer accel max
expected_out=[6.16546   0.03717 ];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001);
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EVT_PRIUS_Prog_dyn_ARTAUTO : max bilanP>20');
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001);
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end



function test_HPDP_EVT_PRIUS_Prog_dyn_ARTROUT(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTROUT','PRIUS_EVT','HPDP_EVT_SURV_RED','param_backward_prog_dyn_test_HPDP_EVT',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
%expected_out=[ 4.1499    0.0476]; % correction bug couple max MOTH dans les donnees
expected_out=[ 4.1500    0.0475];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001);
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EVT_PRIUS_Prog_dyn_ARTROUT : max bilanP>20');
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001);
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end




function test_HPDP_EVT_PRIUS_RED_mat_Prog_dyn_ARTURB(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTURB','PRIUS_EVT','HPDP_EVT_SURV_RED','param_backward_prog_dyn_test_HPDP_EVT_mat',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[  4.6998  0.0227];
bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001);
    assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EVT_PRIUS_RED_mat_Prog_dyn_ARTURB : max bilanP>20');
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001);
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end


%% CAs carto 3D avec DC/DC
function test_HPDP_EVT_PRIUS_RED_mat_Prog_dyn_ARTURB_carto_3D(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTURB','PRIUS_EVT_carto_3D','HPDP_EVT_SURV_RED','param_backward_prog_dyn_test_HPDP_EVT_carto_3D',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[  4.1498   0.02754 ];
%bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001);
    %assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EVT_PRIUS_RED_mat_Prog_dyn_ARTURB : max bilanP>20');
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001);
    %verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end

function test_HPDP_EVT_PRIUS_RED_mat_Prog_dyn_ARTAUTO_carto_3D(testCase)

[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ARTAUTO','PRIUS_EVT_carto_3D','HPDP_EVT_SURV_RED','param_backward_prog_dyn_test_HPDP_EVT_carto_3D',1,1,50);

conso100=XmlValue(ResXml,'conso100');
soc=XmlValue(ResXml,'soc');
out=[conso100 soc(1)-soc(end)];
expected_out=[ 5.72024 0.01958];
%bilanP=XmlValue(ResXml,'bilanP');
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001);
    %assertTrue(max(abs(bilanP(2:end)))<20,'test_HPDP_EVT_PRIUS_RED_mat_Prog_dyn_ARTURB : max bilanP>20');
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001);
    %verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 20)
end




