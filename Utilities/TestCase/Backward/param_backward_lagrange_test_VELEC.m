% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???
% 26-08-2011 (EV) : version 1
%

% définition pas de temps
param.pas_temps=1;

% définition pas courant
param.pas_isc=5;

% Parametre pour debugger
param.verbose=0;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='lagrange';

% Convergence dans un intervalle ou calcul pour un parametre de lagrange fixe ?
param.cvgce=0;

param.lambda=0.37; 


% tdebug pour donner l'instant ou l'on veut tracer la fonction (0 sinon)
param.tdebug=0;

