% function test_suite =test_backward_VELEC(testCase)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

function test_suite =test_backward_VELEC(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 

function testAX_ELEC_ligne(testCase)
format long
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ECE15','AX_ELE','VELEC','param_backward_ligne_test_VELEC',1,1);
ah=XmlValue(ResXml,'ah');
out=[ah(end)];
%expected_out=[0.8764]; % prise en compte couple min moteur et calculateur
expected_out=[0.8694]; 
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

function testAX_ELEC_BAT_SC_prog_dyn(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ECE15','AX_ELE_BAT_SC_DCDCSC','VELEC_BAT_SC','param_backward_prog_dyn_test_VELEC',1,1);
ah=XmlValue(ResXml,'ah');
tsim=XmlValue(ResXml,'tsim');
isc=XmlValue(ResXml,'isc');
ah=XmlValue(ResXml,'ah');
bilanP=XmlValue(ResXml,'bilanP');

Ieffsc=eval('sqrt(trapz(tsim,isc.*isc)/(tsim(end)-tsim(1)))');
out=[ah(end) Ieffsc];
%expected_out=[1.13017 78.74867]; % changement SC.minTension de 0.45 à 0.5 SC.maxtension
expected_out=[ 1.125728641213551  75.892031962940081];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue( trapz(bilanP(2:end))<1e-8,'expected energy balance bilanP<1e-10')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 1e-10)  
end



function testAX_ELEC_BAT_SC_ligne(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ECE15','AX_ELE_BAT_SC_DCDCSC','VELEC_BAT_SC','param_backward_ligne_test_VELEC',1,1);
ah=XmlValue(ResXml,'ah');
tsim=XmlValue(ResXml,'tsim');
isc=XmlValue(ResXml,'isc');
bilanP=XmlValue(ResXml,'bilanP');

Ieffsc=eval('sqrt(trapz(tsim,isc.*isc)/(tsim(end)-tsim(1)))');
out=[ah(end) Ieffsc];
expected_out=[1.06237 0];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue( trapz(bilanP(2:end))<1e-8,'expected energy balance bilanP<1e-10')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 1e-10)  
end


function testAX_ELEC_BAT_SC_lagrange(testCase)
[ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_ECE15','AX_ELE_BAT_SC_DCDCSC','VELEC_BAT_SC','param_backward_lagrange_test_VELEC',1,1);
ah=XmlValue(ResXml,'ah');
tsim=XmlValue(ResXml,'tsim');
isc=XmlValue(ResXml,'isc');
ah=XmlValue(ResXml,'ah');
bilanP=XmlValue(ResXml,'bilanP');

Ieffsc=eval('sqrt(trapz(tsim,isc.*isc)/(tsim(end)-tsim(1)))');
out=[ah(end) Ieffsc];
%expected_out=[1.12459 81.50248]; % changement SC.minTension de 0.45 à 0.5 SC.maxtension
expected_out=[1.137816704234629  77.317293780407070];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.0001)
    assertTrue( trapz(bilanP(2:end))<1e-8,'expected energy balance bilanP<1e-10')
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.0001)
    verifyEqual( testCase, max(abs(bilanP(2:end))), 0, 'AbsTol', 1e-10)
end
