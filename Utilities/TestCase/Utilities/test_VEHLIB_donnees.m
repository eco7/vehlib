% Function: test_VEHLIB_donnees
% Test des fichiers de donnees externes avec la bibliotheque vehlib
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
% Cette fonction utilise la bibliotheque XUnit pour realiser des tests 
%
% de validation des differents types de fichier externe compatibles avec la bibliotheque
%
% VEHLIB.
%
function test_suite =test_VEHLIB_donnees(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 
%format long

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_formatXML
function test_VEHLIB_formatXML(testCase)
eval('initdata');

[~,Struct]=genereXMLFormat4Vehlib({'x' 'y'},{'X' 'Y'},{'S.U.' 'S.U.'},{'double' 'double'},[0 eps;1 0;2 0;3 0],'','');

out=[sum(XmlValue(Struct,'x')) sum(XmlValue(Struct,'y')) length(Struct.table{1}.x.vector)];
expected_out=[6 eps 4];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',eps);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', eps);
end

fileName='test.xml';
pathName=fullfile(INIT.VEHLIB_ROOT,'Utilities','TestCase','Utilities','Measurement');
ecritureXMLFile4Vehlib(Struct,fileName,pathName);
clear Struct
Struct=lectureXMLFile4Vehlib(fileName,pathName);
out=[sum(XmlValue(Struct,'x')) sum(XmlValue(Struct,'y')) length(Struct.table{1}.x.vector)];
expected_out=[6 eps 4];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',eps);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', eps);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_formatMAT
function test_VEHLIB_formatMAT(testCase)
eval('initdata');

param.FileName='rec1_084.mat';
param.PathName=fullfile(INIT.VEHLIB_ROOT,'Utilities','TestCase','Utilities','Measurement');
Struct=lectureMatFile4Vehlib(param);
out= XmlValue(Struct,'Time_dsp');
out=out(end);
expected_out=20.350524258420933;
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.00001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.00001);
end

fileName='test.xml';
ecritureXMLFile4Vehlib(Struct,fileName,param.PathName);
clear Struct
Struct=lectureXMLFile4Vehlib(fileName,param.PathName);
out= XmlValue(Struct,'Time_dsp');
out=out(end);
expected_out=20.350524258420933;
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.00001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.00001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_formatASCII
function test_VEHLIB_formatASCII(testCase)
eval('initdata');
param.FileName='test_ASCII.txt';
param.PathName=fullfile(INIT.VEHLIB_ROOT,'Utilities','TestCase','Utilities','Measurement');
[Struct,~]=lectureAscii4Vehlib(param);
out= XmlValue(Struct,'var1');
out=[out(end) length(out)];
expected_out=[0 6];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.00001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.00001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test2_VEHLIB_formatASCII
function test2_VEHLIB_formatASCII(testCase)
eval('initdata');
param.FileName='test2_ASCII.txt';
param.PathName=fullfile(INIT.VEHLIB_ROOT,'Utilities','TestCase','Utilities','Measurement');
[Struct,~]=lectureAscii4Vehlib(param);
out= XmlValue(Struct,'var1');
out=[out(end) length(out)];
expected_out=[0 6];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.00001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.00001);
end