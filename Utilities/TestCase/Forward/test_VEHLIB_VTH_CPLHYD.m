% Function: test_VEHLIB_VTH_CPLHYD
% Test du vehicule conventionnel sur des cycles de reference
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
% Cette fonction utilise la bibliotheque XUnit pour realiser des tests 
%
% de validation des modeles de vehicule conventionnel avec coupleur
%
% hydraulique dans la bibliotheque VEHLIB.
%
function test_suite =test_VEHLIB_VTH_CPLHYD(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 
%format long

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_ECE15
function test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_ECE15(testCase)
vehlib_f='BUS_CPLHYD';
architecture='VTH_CPLHYD';
cinem='CIN_ECE15';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
expected_out=[38.118];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_SORT1
function test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_SORT1(testCase)
vehlib_f='BUS_CPLHYD';
architecture='VTH_CPLHYD';
cinem='SORT1';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[52.222]; 06/2011 probleme de precision ?
expected_out=[52.2349];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_SORT2
function test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_SORT2(testCase)
vehlib_f='BUS_CPLHYD';
architecture='VTH_CPLHYD';
cinem='SORT2';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[43.008]; 06/2011 probleme de precision ?
expected_out=[43.0155];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_SORT3
function test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_SORT3(testCase)
vehlib_f='BUS_CPLHYD';
architecture='VTH_CPLHYD';
cinem='SORT3';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[38.877]; 06/2011 probleme de precision ?
expected_out=[38.8816];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_BUS_CC
function test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_BUS_CC(testCase)
vehlib_f='BUS_CPLHYD';
architecture='VTH_CPLHYD';
cinem='CIN_BUS_CC';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
expected_out=[52.294];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_BUS_IS
function test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_BUS_IS(testCase)
vehlib_f='BUS_CPLHYD';
architecture='VTH_CPLHYD';
cinem='CIN_BUS_IS';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
expected_out=[34.082];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_BUS_RS
function test_VEHLIB_VTH_CPLHYD_BUS_CPLHYD_BUS_RS(testCase)
vehlib_f='BUS_CPLHYD';
architecture='VTH_CPLHYD';
cinem='CIN_BUS_RS';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
expected_out=[43.926];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end
