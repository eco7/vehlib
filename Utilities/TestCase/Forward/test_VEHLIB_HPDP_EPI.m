% Function: test_VEHLIB_HPDP_EPI
% Test du vehicule hybride parallele a derivation de puissance sur des cycles de reference
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
% Cette fonction utilise la bibliotheque XUnit pour realiser des tests 
%
% de validation des modeles de vehicule hybride parallele a derivation de puissance dans la bibliotheque
%
% VEHLIB.
%
function test_suite =test_VEHLIB_HPDP_EPI(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 
%format long

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_PRIUS_JAPON_CIN_NEDC
function test_VEHLIB_PRIUS_JAPON_CIN_NEDC(testCase)
vehlib_f='PRIUS_JAPON';
architecture='HPDP_EPI';
cinem='CIN_NEDC';
int_dod=[40 50];
lect_mod=2;
method=2;
precision=0.05;
verbose=0;
warning off;
[erreur,consoZero,conso100,deltaDod,dodIni]=charge_sustain(method,vehlib_f,architecture,cinem,int_dod,lect_mod,precision,verbose);
warning on;
out1=[deltaDod];
expected_out1=[0.0];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out1,expected_out1,'absolute',0.05)
else
	verifyEqual( testCase, out1, expected_out1, 'AbsTol', 0.05)
end
out2=[conso100];
% expected_out2=[5.32]; 06/2011 - avant modifs BMS ??
% expected_out2=[5.1073];
expected_out2=[5.096]; % algebraic loop in ohmic losses : add a memory in current node
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out2,expected_out2,'absolute',0.01)
else
	verifyEqual( testCase, out2, expected_out2, 'AbsTol', 0.01)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_PRIUS_JAPON_CIN_1015
function test_VEHLIB_PRIUS_JAPON_CIN_1015(testCase)
vehlib_f='PRIUS_JAPON';
architecture='HPDP_EPI';
cinem='CIN_1015';
int_dod=[40 50];
lect_mod=2;
method=2;
precision=0.05;
verbose=0;
warning off;
[erreur,consoZero,conso100,deltaDod,dodIni]=charge_sustain(method,vehlib_f,architecture,cinem,int_dod,lect_mod,precision,verbose);
warning on;
out1=[deltaDod];
expected_out1=[0.0];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out1,expected_out1,'absolute',0.05)
else
	verifyEqual( testCase, out1, expected_out1, 'AbsTol', 0.05)
end
out2=[conso100];
% expected_out2=[4.78]; 06/2011 -avant modifs BMS ??
%expected_out2=[4.5848];
expected_out2=[4.884]; % algebraic loop in ohmic losses : add a memory in current node
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out2,expected_out2,'absolute',0.01)
else
	verifyEqual( testCase, out2, expected_out2, 'AbsTol', 0.01)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_PRIUS_II_CIN_NEDC
function test_VEHLIB_PRIUS_II_CIN_NEDC(testCase)
vehlib_f='PRIUS_II';
architecture='HPDP_EPI';
cinem='CIN_NEDC';
int_dod=[40 50];
lect_mod=2;
method=2;
precision=0.05;
verbose=0;
warning off;
[erreur,consoZero,conso100,deltaDod,dodIni]=charge_sustain(method,vehlib_f,architecture,cinem,int_dod,lect_mod,precision,verbose);
warning on;
out1=[deltaDod];
expected_out1=[0.0];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out1,expected_out1,'absolute',0.05)
else
	verifyEqual( testCase, out1, expected_out1, 'AbsTol', 0.05)
end
out2=[conso100];
expected_out2=[3.83];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out2,expected_out2,'absolute',0.01)
else
	verifyEqual( testCase, out2, expected_out2, 'AbsTol', 0.01)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_PRIUS_II_CIN_HYZURB
function test_VEHLIB_PRIUS_II_CIN_HYZURB(testCase)
vehlib_f='PRIUS_II';
architecture='HPDP_EPI';
cinem='CIN_HYZURB';
int_dod=[40 50];
lect_mod=2;
method=2;
precision=0.05;
verbose=0;
warning off;
[erreur,consoZero,conso100,deltaDod,dodIni]=charge_sustain(method,vehlib_f,architecture,cinem,int_dod,lect_mod,precision,verbose);
warning on;
out1=[deltaDod];
expected_out1=[0.0];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out1,expected_out1,'absolute',0.05)
else
	verifyEqual( testCase, out1, expected_out1, 'AbsTol', 0.05)
end
out2=[conso100];
%expected_out2=[3.76];
expected_out2=[3.742]; % algebraic loop in ohmic losses : add a memory in current node
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out2,expected_out2,'absolute',0.01)
else
	verifyEqual( testCase, out2, expected_out2, 'AbsTol', 0.01)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_PRIUS_II_CIN_HYZROUT
function test_VEHLIB_PRIUS_II_CIN_HYZROUT(testCase)
vehlib_f='PRIUS_II';
architecture='HPDP_EPI';
cinem='CIN_HYZROUT';
int_dod=[20 60];
lect_mod=2;
method=2;
precision=0.05;
verbose=0;
warning off;
[erreur,consoZero,conso100,deltaDod,dodIni]=charge_sustain(method,vehlib_f,architecture,cinem,int_dod,lect_mod,precision,verbose);
warning on;
out1=[deltaDod];
expected_out1=[0.0];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out1,expected_out1,'absolute',0.05)
else
	verifyEqual( testCase, out1, expected_out1, 'AbsTol', 0.05)
end
out2=[conso100];
% expected_out2=[4.3];
% expected_out2=[4.3126]; % Modif suite ? modif VD.MOTH.wmt_maxi pour passer les accel max.
expected_out2=[4.298]; % algebraic loop in ohmic losses : add a memory in current node
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out2,expected_out2,'absolute',0.01)
else
	verifyEqual( testCase, out2, expected_out2, 'AbsTol', 0.01)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_PRIUS_II_CIN_HYZAUTO
function test_VEHLIB_PRIUS_II_CIN_HYZAUTO(testCase)
vehlib_f='PRIUS_II';
architecture='HPDP_EPI';
cinem='CIN_HYZAUTO';
int_dod=[30 50];
lect_mod=2;
method=2;
precision=0.05;
verbose=0;
warning off;
[erreur,consoZero,conso100,deltaDod,dodIni]=charge_sustain(method,vehlib_f,architecture,cinem,int_dod,lect_mod,precision,verbose);
warning on;
out1=[deltaDod];
expected_out1=[0.0];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out1,expected_out1,'absolute',0.05)
else
	verifyEqual( testCase, out1, expected_out1, 'AbsTol', 0.05)
end
out2=[conso100];
expected_out2=[5.45];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out2,expected_out2,'absolute',0.01)
else
	verifyEqual( testCase, out2, expected_out2, 'AbsTol', 0.01)
end

