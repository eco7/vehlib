% Function: test_VEHLIB_HPDP_EPI
function test_suite =test_VEHLIB_HP_BV_1EMB(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end
%format long

function test_VEHLIB_CLIO_CIN_NEDC(testCase)
vehlib_f='CLIO_1L5DCI_HP_BV_1EMB_UQM';
architecture='HP_BV_1EMB';
cinem='CIN_NEDC_BV';
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,40,1,1);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)/(distance(end)/1000);

out2=[conso100 soc(1)-soc(end)];
expected_out2=[3.6698 -1.3884];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out2,expected_out2,'absolute',0.06);
else
	verifyEqual( testCase, out2, expected_out2, 'AbsTol', 0.1);
end

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function test_VEHLIB_CLIO_1EMB_UQM_CIN_NEDC(testCase)
% vehlib_f='CLIO_1L5DCI_HP_BV_1EMB_UQM';
% architecture='HP_BV_1EMB';
% cinem='CIN_NEDC';
% int_dod=[30 40];
% lect_mod=1;
% method=2;
% precision=0.05;
% verbose=0;
% warning off;
% [erreur,consoZero,conso100,deltaDod,dodIni]=charge_sustain(method,vehlib_f,architecture,cinem,int_dod,lect_mod,precision,verbose);
% warning on;
% out1=[deltaDod];
% expected_out1=[0.0];
% if verLessThan('matlab','8.2')
% 	testCase=[];
% 	assertVectorsAlmostEqual(out1,expected_out1,'absolute',0.05);
% else
% 	verifyEqual( testCase, out1, expected_out1, 'AbsTol', 0.05);
% end
% out2=[conso100];
% %expected_out2=[5.32]; 06/2011 ???
% expected_out2=[3.8617];
% if verLessThan('matlab','8.2')
% 	testCase=[];
% 	assertVectorsAlmostEqual(out2,expected_out2,'absolute',0.01);
% else
% 	verifyEqual( testCase, out2, expected_out2, 'AbsTol', 0.01);
% end
% 
% function test_VEHLIB_Peugeot_308SW_CIN_HYZURB_BV_TWC_1(testCase)
% vehlib_f='Peugeot_308SW_1L6VTI16V_HP_BV_1EMB_TWC';
% architecture='HP_BV_1EMB_TWC';
% cinem='CIN_HYZURB_BV';
% [vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,40,1,1);
% VD.ECU.lambda2=0;
% VD.ECU.lambda1=2.21191;
% assignin('base','VD',VD);
% warning off;
% sim(vehlib.simulink);
% bdclose(vehlib.simulink);
% warning on;
% 
% co_gkm=cocum(end)/(distance(end)/1000);
% hc_gkm=hccum(end)/(distance(end)/1000);
% nox_gkm=noxcum(end)/(distance(end)/1000);
% conso100=100*conso(end)/(distance(end)/1000);
% 
% out2=[conso100 soc(1)-soc(end) co_gkm hc_gkm nox_gkm];
% %expected_out2=[6.823 -0.010 1.9793 0.2832 0.4332]; % en 2010b
% expected_out2=[6.835 -0.067 1.9768 0.2827 0.4324]; % en 2016a
% if verLessThan('matlab','8.2')
% 	testCase=[];
% 	assertVectorsAlmostEqual(out2,expected_out2,'absolute',0.06);
% else
% 	verifyEqual( testCase, out2, expected_out2, 'AbsTol', 0.1);
% end
% 
% function test_VEHLIB_Peugeot_308SW_CIN_HYZURB_BV_TWC_2(testCase)
% vehlib_f='Peugeot_308SW_1L6VTI16V_HP_BV_1EMB_TWC';
% architecture='HP_BV_1EMB_TWC';
% cinem='CIN_HYZURB_BV';
% [vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,40,1,1);
% VD.ECU.lambda2=-3;
% VD.ECU.lambda1=8.61328;
% assignin('base','VD',VD);
% warning off;
% sim(vehlib.simulink);
% bdclose(vehlib.simulink);
% warning on;
% 
% co_gkm=cocum(end)/(distance(end)/1000);
% hc_gkm=hccum(end)/(distance(end)/1000);
% nox_gkm=noxcum(end)/(distance(end)/1000);
% conso100=100*conso(end)/(distance(end)/1000);
% 
% out2=[conso100 soc(1)-soc(end) co_gkm hc_gkm nox_gkm];
% %expected_out2=[7.309 -0.0594 1.569 0.2194 0.3205]; % en 2010b
% expected_out2=[7.344 -0.2626 1.571 0.2196 0.3206]; % en 2016a
% if verLessThan('matlab','8.2')
% 	testCase=[];
% 	assertVectorsAlmostEqual(out2,expected_out2,'absolute',0.3);
% else
% 	verifyEqual( testCase, out2, expected_out2, 'AbsTol', 0.02);
% end