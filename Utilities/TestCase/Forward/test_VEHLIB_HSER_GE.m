% Function: test_VEHLIB_VTH_BV
% Test du vehicule hybride serie sur des cycles de reference
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
% Cette fonction utilise la bibliotheque XUnit pour realiser des tests 
%
% de validation des modeles de vehicule conventionnel dans la bibliotheque
%
% VEHLIB.
%
function test_suite =test_VEHLIB_HSER_GE(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 
%format long

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_HSER_GE_306_ECE15
% function test_VEHLIB_HSER_GE_306_ECE15(testCase)
% vehlib_f='D306_HYBS_PVAR';
% architecture='HSER_GE';
% cinem='CIN_ECE15';
% Dod0=40;
% [vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
% assignin('base','VD',VD);
% warning off;
% sim(vehlib.simulink);
% warning on;
% conso100=100*conso(end)./(distance(end)/1000);
% soc=soc;
% out=[conso100 soc(1)-soc(end)];
% expected_out=[5.108 0.0031];
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);


function test_VEHLIB_HSER_GE_308_NEDC(testCase)

vehlib_f='Peugeot_308_HYBS_PVAR';
architecture='HSER_GE';
cinem='CIN_NEDC';
Dod0=40;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100 soc(1)-soc(end)];
% expected_out=[5.0881  -2.7321];
expected_out=[5.0370   -2.4658]; % Suite a la release 148 concernant le pilotage du GE
expected_out=[5.033 -2.522]; % algebraic loop in ohmic losses : add a memory in current node
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

% % Function: test_VEHLIB_HSER_GE_306_ARTURB
% function test_VEHLIB_HSER_GE_306_ARTURB(testCase)
% vehlib_f='D306_HYBS_PVAR';
% architecture='HSER_GE';
% cinem='CIN_ARTURB';
% Dod0=40;
% [vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
% assignin('base','VD',VD);
% warning off;
% sim(vehlib.simulink);
% warning on;
% conso100=100*conso(end)./(distance(end)/1000);
% soc=soc;
% out=[conso100 soc(1)-soc(end)];
% expected_out=[5.048 0.0095];
% assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);