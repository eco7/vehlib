% Function: test_VEHLIB_composant_vehicule
% Test des modeles vehicule de la bibliotheque VEHLIB
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% 
% Test des modeles vehicule de la bibliotheque VEHLIB
%
%----------------------------------------------------------------------------
function test_suite = test_VEHLIB_composant_vehicule(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; % pour que chaque test deviennent indépendant
warning('off')

%----------------------------------------------------------------------------
% Function: testVehiculeType1
% Test du modele vehicule de la bibliotheque
%
% Objet:
% Test du modele vehicule de la bibliotheque
%
% Test presentant une solution analytique simple
%
function testVehiculeType1(testCase)
create_modele_vehicule(1)


initdata;
VD.VEHI.ntypveh=1;
VD.VEHI.Gpes     = 9.81 ; 
VD.VEHI.Patm     = 101325 ;
VD.VEHI.Tatm     = 298.15 ;
VD.VEHI.Ratm     = 287 ;
VD.VEHI.Roatm    = VD.VEHI.Patm/VD.VEHI.Ratm/VD.VEHI.Tatm;
VD.VEHI.Rpneu     = 1;
VD.VEHI.Nbroue    = 1;
VD.VEHI.Inroue    = 1;
VD.VEHI.Mveh     = 10;
VD.VEHI.Cx       = 1;
VD.VEHI.Sveh     = 1;
VD.VEHI.a	=  0.01;
VD.VEHI.b	=  0.;
VD.VEHI.fcst	=  0.;
VD.VEHI.fvit1	=  0.;
VD.VEHI.fvit2	=  0.;
VD.VEHI.a0=0;
VD.VEHI.a1=0;
VD.VEHI.a2=0;

VD.INIT.t_ech=.1;
VD.INIT.Dod0=60;

assignin('base','VD',VD);

Cmot=100;
set_param('test_vehicule/Cmot','Value',num2str(Cmot)); % modif param du block simulink, Valeur de la source
set_param('test_vehicule/Masse','Value',num2str(VD.VEHI.Mveh)); % modif param du block simulink, Valeur de la source
set_param('test_vehicule','StopTime',num2str(100)); % modif param du block simulink, Valeur de la source

simout = sim('test_vehicule');
close_system('VEHLIB');
close_system('test_vehicule',0); % fermeture system courant
close_system('simulink'); % fermeture system courant
%bdclose;

out=[simout.vit(end)];
expected_out=[sqrt((Cmot-VD.VEHI.a*VD.VEHI.Mveh*VD.VEHI.Gpes)/(.5*VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx))];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end


%----------------------------------------------------------------------------
% Function: create_modele_vehicule
% Creation "a la volee" d'un modele simulink du vehicule
%
%
function create_modele_vehicule(ntypveh)
open_system('simulink')
open_system('VEHLIB') % ouverture VEHLIB

new_system('test_vehicule');  % creation nouveau system
open_system('test_vehicule'); % ouverture du systeme crée

add_block('VEHLIB/Composants/Vehicules/Vehicule','test_vehicule/Vehicule'); % ajout block de la library

%if ntypbat==1
%    set_param('test_battery/Batterie', 'BlockChoice','Batterie')
%elseif ntypbat==2
%    set_param('test_battery/Batterie', 'BlockChoice','Batterie S fonc')
%elseif ntypbat==3
%    set_param('test_battery/Batterie', 'BlockChoice','Batterie Modele 3')
%end
    
%CMot, Jmot
add_block('simulink/Sources/Constant','test_vehicule/Jmot'); % ajout block simulink
set_param('test_vehicule/Jmot','Value','1'); % modif param du block simulink, Valeur de la source
add_block('simulink/Sources/Constant','test_vehicule/Cmot'); % ajout block simulink
set_param('test_vehicule/Cmot','Value','100'); % modif param du block simulink, Valeur de la source

add_block('simulink/Signal Routing/Mux','test_vehicule/Mux'); % ajout block simulink

add_line('test_vehicule','Cmot/1','Mux/1'); % ajout line /1 signifie premiere entree ou sortie
add_line('test_vehicule','Jmot/1','Mux/2'); % ajout line /1 signifie premiere entree ou sortie
add_line('test_vehicule','Mux/1','Vehicule/2'); % ajout line /1 signifie premiere entree ou sortie

% Cfrein_av, Cfrein_ar
add_block('simulink/Sources/Constant','test_vehicule/Cfrein_av'); % ajout block simulink
set_param('test_vehicule/Cfrein_av','Value','0'); % modif param du block simulink, Valeur de la source
add_block('simulink/Sources/Constant','test_vehicule/Cfrein_ar'); % ajout block simulink
set_param('test_vehicule/Cfrein_ar','Value','0'); % modif param du block simulink, Valeur de la source
add_block('simulink/Signal Routing/Mux','test_vehicule/Mux2'); % ajout block simulink
add_line('test_vehicule','Cfrein_av/1','Mux2/1'); % ajout line /1 signifie premiere entree ou sortie
add_line('test_vehicule','Cfrein_ar/1','Mux2/2'); % ajout line /1 signifie premiere entree ou sortie
add_line('test_vehicule','Mux2/1','Vehicule/1'); % ajout line /1 signifie premiere entree ou sortie

% Masse_bil
add_block('simulink/Sources/Constant','test_vehicule/Masse'); % ajout block simulink
set_param('test_vehicule/Masse','Value','0'); % modif param du block simulink, Valeur de la source
add_block('simulink/Signal Routing/Goto','test_vehicule/Goto'); % ajout block simulink
set_param('test_vehicule/Goto','GotoTag','Masse_bil'); % modif param du block simulink, Valeur de la source
set_param('test_vehicule/Goto','TagVisibility','Global'); % modif param du block simulink, Valeur de la source
add_line('test_vehicule','Masse/1','Goto/1'); % ajout line /1 signifie premiere entree ou sortie


% tsim
add_block('simulink/Sources/Clock','test_vehicule/Clock'); % ajout block simulink
add_block('simulink/Sinks/To Workspace','test_vehicule/tsim'); % ajout block simulink
set_param('test_vehicule/tsim','VariableName','tsim'); % modif param du block simulink, Valeur de la source
set_param('test_vehicule/tsim','SaveFormat','Array'); % modif param du block simulink, Valeur de la source
set_param('test_vehicule/tsim','SampleTime','VD.INIT.t_ech'); % modif param du block simulink, Valeur de la source
add_line('test_vehicule','Clock/1','tsim/1'); % ajout line /1 signifie premiere entree ou sortie
