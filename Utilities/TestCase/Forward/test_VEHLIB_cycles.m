% Function: test_VEHLIB_cycles
% Test des fichiers cinematiques avec la bibliotheque vehlib
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
% Cette fonction utilise la bibliotheque XUnit pour realiser des tests 
%
% de validation des differents types de cycle presents dans la bibliotheque
%
% VEHLIB.
%
function test_suite =test_VEHLIB_cycles(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 
%format long

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_CLIO_ECE15
function test_VEHLIB_VTH_BV_CLIO_ECE15(testCase)
vehlib_f='CLIO_1L5DCI';
architecture='VTH_BV';
cinem='CIN_ECE15';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=conso100;
expected_out=4.8871;
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_CLIO_ECE15_2
function test_VEHLIB_VTH_BV_CLIO_ECE15_2(testCase)
vehlib_f='CLIO_1L5DCI';
architecture='VTH_BV';
cinem='CIN_ECE15';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
vehlib=rmfield(vehlib,'CYCL');
VD=rmfield(VD,'CYCL');
vehlib.CYCL='MON_CYCLE';

% "Creation" d'une nouvelle cinematique
eval('CIN_ECE15');
VD.CYCL.temps=CYCL.temps;
VD.CYCL.vitesse=CYCL.vitesse;
clear CYCL;
VD.CYCL.ntypcin=1;
VD.CYCL.pente=0;
VD.CYCL.recharge=0;
[~, vehlib, VD]=ini_calcul(vehlib, VD);

assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=conso100;
expected_out=4.8871;
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

function test_VEHLIB_VTH_BV_CLIO_ECE15_3(testCase)
vehlib_f='CLIO_1L5DCI';
architecture='VTH_BV';
cinem='CIN_ECE15';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
vehlib=rmfield(vehlib,'CYCL');
VD=rmfield(VD,'CYCL');
vehlib.CYCL='MON_CYCLE';

% "Creation" d'une nouvelle cinematique avec pente constante
eval('CIN_ECE15');
VD.CYCL.temps=CYCL.temps;
VD.CYCL.vitesse=CYCL.vitesse;
clear CYCL;
VD.CYCL.ntypcin=1;
VD.CYCL.pente=1;
VD.CYCL.recharge=0;
[~, vehlib, VD]=ini_calcul(vehlib, VD);

assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
expected_out=5.4327;
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

function test_VEHLIB_VTH_BV_CLIO_ECE15_4(testCase)
vehlib_f='CLIO_1L5DCI';
architecture='VTH_BV';
cinem='CIN_ECE15';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
vehlib=rmfield(vehlib,'CYCL');
VD=rmfield(VD,'CYCL');
vehlib.CYCL='MON_CYCLE';

% "Creation" d'une nouvelle cinematique avec pente variable
eval('CIN_ECE15');
VD.CYCL.temps=CYCL.temps;
VD.CYCL.vitesse=CYCL.vitesse;
clear CYCL;
VD.CYCL.ntypcin=1;
VD.CYCL.ntyppente=3;	% cinematique pente-distance
VD.CYCL.PKpente=[0 400 401 1000]; % distance en m
VD.CYCL.penteFpk=[0 0 1 1]; % pente en %
VD.CYCL.recharge=0;
[~, vehlib, VD]=ini_calcul(vehlib, VD);

assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=conso100;
expected_out=5.2026;
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end
