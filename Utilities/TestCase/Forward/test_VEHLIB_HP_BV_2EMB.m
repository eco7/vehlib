% Function: test_VEHLIB_HP_BV_2EMB
function test_suite =test_VEHLIB_HP_BV_2EMB(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end
%format long

function test_VEHLIB_Renault_Kadjar_CONTROL(testCase)
vehlib_f='Renault_Kadjar_TCE130_HP_BV_2EMB_CONTROL';
architecture='HP_BV_2EMB';
cinem='CIN_NEDC_BV6';
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,25,1,1);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)/(distance(end)/1000);
co_gkm = trapz(tsim,co)./(distance(end)/1000);
hc_gkm = trapz(tsim,hc)./(distance(end)/1000);
nox_gkm = trapz(tsim,nox)./(distance(end)/1000);

out=[conso100 soc(end)-soc(1)  co_gkm hc_gkm nox_gkm];
%expected_out=[4.66 -0.403 0.132 0.016 0.101];
expected_out=[4.66 -0.403 2.39 0.029 0.843];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.06);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.1);
end
