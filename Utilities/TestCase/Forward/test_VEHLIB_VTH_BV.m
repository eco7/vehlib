% Function: test_VEHLIB_VTH_BV
% Test du vehicule conventionnel sur des cycles de reference
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
% Cette fonction utilise la bibliotheque XUnit pour realiser des tests 
%
% de validation des modeles de vehicule conventionnel dans la bibliotheque
%
% VEHLIB.
%
function test_suite =test_VEHLIB_VTH_BV(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end
%format long

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_CLIO_ECE15
function test_VEHLIB_VTH_BV_CLIO_ECE15(testCase)
vehlib_f='CLIO_1L5DCI';
architecture='VTH_BV';
cinem='CIN_ECE15';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
% expected_out=[4.88]; 06/2011 - Pb precision ?
expected_out=[4.8871];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_CLIO_NEDC_BV
function test_VEHLIB_VTH_BV_CLIO_NEDC_BV(testCase)
vehlib_f='CLIO_1L5DCI';
architecture='VTH_BV';
cinem='CIN_NEDC_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[3.92]; Rapport Mild HYBRID N° LTE 0725 VEHLIB V4.0
%expected_out=[3.92]; 06/2011 - Pb precision ?
%expected_out=[3.9233]; 07/2011 - 'use input nearest' pr LUT rapport de boite
expected_out=[3.9125];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_CLIO_HYZURB_BV
function test_VEHLIB_VTH_BV_CLIO_HYZURB_BV(testCase)
vehlib_f='CLIO_1L5DCI';
architecture='VTH_BV';
cinem='CIN_HYZURB_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[4.99]; Rapport Mild HYBRID N° LTE 0725 VEHLIB V4.0
%expected_out=[4.986]; 07/2011 - 'use input nearest' pr LUT rapport de boite
expected_out=[4.963];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function: test_VEHLIB_VTH_BV_CLIO_HYZROUT_BV(testCase)
function test_VEHLIB_VTH_BV_CLIO_HYZROUT_BV(testCase)
vehlib_f='CLIO_1L5DCI';
architecture='VTH_BV';
cinem='CIN_HYZROUT_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[4.12]; Rapport Mild HYBRID N° LTE 0725 VEHLIB V4.0
%expected_out=[4.124]; 07/2011 - 'use input nearest' pr LUT rapport de boite
expected_out=[4.111];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_CLIO_HYZAUTO_BV
function test_VEHLIB_VTH_BV_CLIO_HYZAUTO_BV(testCase)
vehlib_f='CLIO_1L5DCI';
architecture='VTH_BV';
cinem='CIN_HYZAUTO_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
% expected_out=[4.68]; Rapport Mild HYBRID N° LTE 0725 VEHLIB V4.0
% expected_out=[4.684]; 07/2011 - 'use input nearest' pr LUT rapport de boite
expected_out=[4.681];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_308_ECE15
function test_VEHLIB_VTH_BV_308_ECE15(testCase)
vehlib_f='Peugeot_308SW_1L6VTI16V';
architecture='VTH_BV';
cinem='CIN_ECE15';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
expected_out=[8.249];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_308_NEDC_BV
function test_VEHLIB_VTH_BV_308_NEDC_BV(testCase)
vehlib_f='Peugeot_308SW_1L6VTI16V';
architecture='VTH_BV';
cinem='CIN_NEDC_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[6.587]; 07/2011 - 'use input nearest' pr LUT rapport de boite
expected_out=[6.573];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_308_HYZURB_BV
function test_VEHLIB_VTH_BV_308_HYZURB_BV(testCase)
vehlib_f='Peugeot_308SW_1L6VTI16V';
architecture='VTH_BV';
cinem='CIN_HYZURB_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[8.787]; 07/2011 - 'use input nearest' pr LUT rapport de boite
%expected_out=[8.791]; % Matlab R2010b
expected_out=[8.7891]; % Matlab R2018a
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_308_HYZROUT_BV
function test_VEHLIB_VTH_BV_308_HYZROUT_BV(testCase)
vehlib_f='Peugeot_308SW_1L6VTI16V';
architecture='VTH_BV';
cinem='CIN_HYZROUT_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[6.839]; 07/2011 - 'use input nearest' pr LUT rapport de boite
expected_out=[6.837];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_308_HYZAUTO_BV
function test_VEHLIB_VTH_BV_308_HYZAUTO_BV(testCase)
vehlib_f='Peugeot_308SW_1L6VTI16V';
architecture='VTH_BV';
cinem='CIN_HYZAUTO_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
expected_out=[6.851];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_CLIO_III_ECE15
function test_VEHLIB_VTH_BV_CLIO_III_ECE15(testCase)
vehlib_f='CLIO_III_1L2Turbo';
architecture='VTH_BV';
cinem='CIN_ECE15';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[7.037];
% expected_out=[7.290]; % Rajout des rendements reducteur et boite de vitesse
expected_out=[7.199]; % fermeture de la carto moteur
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_CLIO_III_NEDC_BV
function test_VEHLIB_VTH_BV_CLIO_III_NEDC_BV(testCase)
vehlib_f='CLIO_III_1L2Turbo';
architecture='VTH_BV';
cinem='CIN_NEDC_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[5.791];
% expected_out=[5.978]; % Rajout des rendements reducteur et boite de vitesse
expected_out=[5.939]; % fermeture de la carto moteur
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_CLIO_III_HYZURB_BV
function test_VEHLIB_VTH_BV_CLIO_III_HYZURB_BV(testCase)
vehlib_f='CLIO_III_1L2Turbo';
architecture='VTH_BV';
cinem='CIN_HYZURB_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[7.517];
% expected_out=[7.995]; % Rajout des rendements reducteur et boite de vitesse
expected_out=[7.928]; % fermeture de la carto moteur
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_CLIO_III_HYZROUT_BV
function test_VEHLIB_VTH_BV_CLIO_III_HYZROUT_BV(testCase)
vehlib_f='CLIO_III_1L2Turbo';
architecture='VTH_BV';
cinem='CIN_HYZROUT_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[6.145];
% expected_out=[6.403]; % Rajout des rendements reducteur et boite de vitesse
expected_out=[6.391]; % fermeture de la carto moteur
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001);
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_CLIO_III_HYZAUTO_BV
function test_VEHLIB_VTH_BV_CLIO_III_HYZAUTO_BV(testCase)
vehlib_f='CLIO_III_1L2Turbo';
architecture='VTH_BV';
cinem='CIN_HYZAUTO_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
%expected_out=[6.520];
% expected_out=[6.795]; % Rajout des rendements reducteur et boite de vitesse
expected_out=[6.793]; % fermeture de la carto moteur
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_Driver
function test_VEHLIB_VTH_BV_Driver(testCase)
if license('test', 'stateflow')
    vehlib_f='Peugeot_308SW_1L6VTI16V_DRIVER';
    architecture='VTH_BV';
    cinem='TRAJET_ARTROUT_Vmax';
    Dod0=20;
    [vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
    assignin('base','VD',VD);
    warning off;
    sim(vehlib.simulink);
    bdclose(vehlib.simulink);
    warning on;
    conso100=100*conso(end)./(distance(end)/1000);
    out=[conso100];
    expected_out=[5.8551];
else
    out=[0];
    expected_out=[0];
end
if verLessThan('matlab','8.2')
    testCase=[];
    assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
    verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_TWC
function test_VEHLIB_VTH_BV_TWC(testCase)
vehlib_f='Peugeot_308SW_1L6VTI16V_TWC';
architecture='VTH_BV';
cinem='CIN_NEDC_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
co_gkm=cocum(end)/(distance(end)/1000);
hc_gkm=hccum(end)/(distance(end)/1000);
nox_gkm=noxcum(end)/(distance(end)/1000);
conso100=100*conso(end)/(distance(end)/1000);

out=[conso100 co_gkm hc_gkm nox_gkm];
expected_out=[6.5732 0.6102 0.0753 0.0797];
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_Kadjar
function test_VEHLIB_VTH_BV_Kadjar(testCase)
vehlib_f='Renault_Kadjar_TCE130';
architecture='VTH_BV';
cinem='CIN_NEDC_BV6';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
out=[conso100];
% expected_out=[6.2545];
expected_out=[6.1181]; % Modified auxiliary power consumption from 400 to 250W
if verLessThan('matlab','8.2')
    testCase=[];
    assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
    verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VTH_BV_Kadjar_CONTROL
function test_VEHLIB_VTH_BV_Kadjar_CONTROL(testCase)
vehlib_f='Renault_Kadjar_TCE130_CONTROL';
architecture='VTH_BV';
cinem='CIN_NEDC_BV6';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
conso100=100*conso(end)./(distance(end)/1000);
co_gkm = trapz(tsim,co)./(distance(end)/1000);
hc_gkm = trapz(tsim,hc)./(distance(end)/1000);
nox_gkm = trapz(tsim,nox)./(distance(end)/1000);
out=[conso100 co_gkm hc_gkm nox_gkm];
%expected_out=[5.9185 0.4079    0.0355    0.1147];
%expected_out=[6.123 0.134 0.036 0.137];
expected_out=[6.123 1.958 0.0466 1.2935]; % prise en compte de l'efficacite avec lambda
if verLessThan('matlab','8.2')
    testCase=[];
    assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
    verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end