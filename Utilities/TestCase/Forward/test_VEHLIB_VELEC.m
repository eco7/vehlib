% Function: test_VEHLIB_VELEC
% Test du vehicule electrique sur des cycles de reference
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Cette fonction utilise la bibliotheque XUnit pour realiser des tests 
%
% de validation des modeles de vehicule electrique dans la bibliotheque
%
% VEHLIB.
%
function test_suite =test_VEHLIB_VELEC(testCase)
if verLessThan('matlab','8.2')
	initTestSuite;
else
	test_suite = functiontests(localfunctions);
end; 
%format long

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VELEC_AX_ELE_CIN_ECE15
function test_VEHLIB_VELEC_AX_ELE_CIN_ECE15(testCase)
vehlib_f='AX_ELE';
architecture='VELEC';
cinem='CIN_ECE15';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
deltaDod=dod(end)-dod(1);
out=[deltaDod];
%expected_out=[0.737];
expected_out=[0.728]; % algebraic loop in ohmic losses : add a memory in current node
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

% ------------------------------------------------
% Function: test_VEHLIB_VELEC_AX_ELE_CIN_NEDC
function test_VEHLIB_VELEC_AX_ELE_CIN_NEDC(testCase)
vehlib_f='AX_ELE';
architecture='VELEC';
cinem='CIN_NEDC';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
deltaDod=dod(end)-dod(1);
out=[deltaDod];
%expected_out=[8.988];
expected_out=[8.945]; % algebraic loop in ohmic losses : add a memory in current node
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

% ------------------------------------------------
% Function: test_VEHLIB_VELEC_AX_ELE_CIN_HYZURB_BV
function test_VEHLIB_VELEC_AX_ELE_CIN_HYZURB_BV(testCase)
vehlib_f='AX_ELE';
architecture='VELEC';
cinem='CIN_HYZURB_BV';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
deltaDod=dod(end)-dod(1);
out=[deltaDod];
%expected_out=[3.208];
%expected_out=[3.2153]; % 19/10/2016 - Nouveau calcul des pertes ACM1 pour limitations batterie
expected_out=[3.189]; % algebraic loop in ohmic losses : add a memory in current node
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

% ------------------------------------------------
% Function: test_VEHLIB_VELEC_AX_ELE_CIN_HYZURB_BV
function test_VEHLIB_AIXAM_E_CITY_generation_cycle(testCase)
%vehlib_f='AIXAM_E_CITY_generation_cycles'; % svn before revision 133 ?
vehlib_f='AIXAM_E_CITY'; % svn revision 133
architecture='VELEC';
cinem='simulation_conduite_e_city_grise';
Dod0=20;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
deltaDod=dod(end)-dod(1);
out=[deltaDod];
% expected_out=[8.6053]; % svn Before revision 123
% expected_out=[15.6102]; % svn revision 123 : modif inertie VD.ACM1 et modifi Capacite monobloc Pb AGM
%expected_out=[15.4640]; % svn revision 124 : modif conso VD.ACM1 sur le point (0,0)
%expected_out=[15.4664]; % 19/10/2016 - Nouveau calcul des pertes ACM1 pour limitations batterie
%expected_out=[15.7437]; % Passage en R2018b de matlab et legeres modifications de la sfonction generation_cycle.c
%expected_out=[15.44211]; % 2022/05/02 Pourquoi ?
expected_out=[15.422]; % algebraic loop in ohmic losses : add a memory in current node
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: test_VEHLIB_VELEC_Citroen_C0_CIN_ECE15
function test_VEHLIB_VELEC_Citroen_C0_CIN_ECE15(testCase)
vehlib_f='Citroen_C0';
architecture='VELEC';
cinem='CIN_ECE15';
Dod0=40;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
bdclose(vehlib.simulink);
warning on;
deltaDod=dod(end)-dod(1);
out=[deltaDod];
%expected_out=[0.5425];
%expected_out=[0.5421]; % 19/10/2016 Nouveau modele cellule + limitation batterie du modele simulink
expected_out=[0.536]; % algebraic loop in ohmic losses : add a memory in current node
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

% Function: test_VEHLIB_VELEC_Citroen_C0_CIN_NEDC
function test_VEHLIB_VELEC_Citroen_C0_CIN_NEDC(testCase)
vehlib_f='Citroen_C0';
architecture='VELEC';
cinem='CIN_NEDC';
Dod0=40;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
warning on;
bdclose(vehlib.simulink);
deltaDod=dod(end)-dod(1);
out=[deltaDod];
%expected_out=[7.3683];
%expected_out=[7.4007]; % 19/10/2016 Nouveau modele cellule + limitation batterie du modele simulink
expected_out=[7.367]; % algebraic loop in ohmic losses : add a memory in current node
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

% Function: test_VEHLIB_VELEC_Citroen_C0_CIN_ARTROUT
function test_VEHLIB_VELEC_Citroen_C0_CIN_ARTROUT(testCase)
vehlib_f='Citroen_C0';
architecture='VELEC';
cinem='CIN_ARTROUT';
Dod0=40;
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
assignin('base','VD',VD);
warning off;
sim(vehlib.simulink);
warning on;
bdclose(vehlib.simulink);
deltaDod=dod(end)-dod(1);
out=[deltaDod];
%expected_out=[11.6317];
%expected_out=[11.7205]; % 19/10/2016 Nouveau modele cellule + limitation batterie du modele simulink
expected_out=[11.685]; % algebraic loop in ohmic losses : add a memory in current node
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end

% Function: test_VEHLIB_VELEC_Citroen_C0_Driver
function test_VEHLIB_VELEC_Citroen_C0_Driver(testCase)
if license('test', 'stateflow')
    vehlib_f='Citroen_C0_DRIVER';
    architecture='VELEC';
    cinem='TRAJET_CEVE_Wstops';
    Dod0=40;
    [vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);
    assignin('base','VD',VD);
    warning off;
    sim(vehlib.simulink);
    bdclose(vehlib.simulink);
    warning on;
    bdclose(vehlib.simulink);
    deltaDod=dod(end)-dod(1);
    out=[deltaDod];
    %expected_out=[12.7188];
    %expected_out=[10.666];
    expected_out=[10.660]; % algebraic loop in ohmic losses : add a memory in current node
else
    out=[0];
    expected_out=[0];
end
if verLessThan('matlab','8.2')
	testCase=[];
	assertVectorsAlmostEqual(out,expected_out,'absolute',0.001)
else
	verifyEqual( testCase, out, expected_out, 'AbsTol', 0.001)
end


