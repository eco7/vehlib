% Function: lecture_MNT_IGN
%
% fonction de lecture du modele numerique de terrain (MNT) en projection lambert 93
%
% Argument d'appel:
% 
% file - chemin complet du ficher ascii IGN BDAlti couvrant la zone (de la forme 'dpt_69_asc.asc')
% 
% Arguments de retour:
% 
% X - vecteur des coordonnees en m en x du MNT
% 
% Y - vecteur des coordonnees en m en y du MNT
% 
% A - matrice des altitudes en m du MNT
%
% Description du fichier Modele Numerique de Terrain de RGE_ALTI V2 :
% Point d'origne : angle NO
% Orientation des lignes et des colonnes : celle des axes de coordonnees
% Decoupage en dalle jointives de 5km de cote qui fait 1000 noeuds par 1000
% noeuds.
% ==> L'altitude est donnee au CENTRE du pixel ou de la maille de 5m de cote, matrice de
% 1000*1000 noeuds soit 5km de cote.
% 
% Remarque : cela pose un probleme pour interpoler sur un point qui est a
% pas/2 en bord d'une dalle (interp2 va renvoyer NaN). En toute rigueur il faudrait associer les 2
% dalles adjacentes avant d'appeler la fonction interp2.
%
function [X,Y,A]=lecture_MNT_IGN(file)

% ajout du dossier et des sous dossiers de le base de donnee IGN BDAlti au path matlab
% if ispc
%     addpath(genpath('\\larissa\LTE_VEH\BASES_DE_DONNEES\BDALTI'));
% elseif isunix
%     addpath(genpath('/mnt/LTE_VEH/BASES_DE_DONNEES/BDALTI'));
% else
%     disp('lecture_MNT_IGN : Premier utilisateur mac (?) de cette fonction, a vous d''ecrire dans le script la ligne de code : addpath(genpath(''\\larissa\LTE_VEH\BASES_DE_DONNEES\BDALTI'')); ')
% end


fid=fopen(file);

if fid == -1
    fprintf(1,'%s %s\n','Cannot open file : ',file);
end

ncols=str2double(sscanf(fgetl(fid),['ncols' '%s']));
nrows=str2double(sscanf(fgetl(fid),['nrows' '%s']));
xllcorner=str2double(sscanf(fgetl(fid),['xllcorner' '%s']));
yllcorner=str2double(sscanf(fgetl(fid),['yllcorner' '%s']));
cellsize=str2double(sscanf(fgetl(fid),['cellsize' '%s']));
NODATA_value=str2double(sscanf(fgetl(fid),['NODATA_value' '%s']));

% matrice des altitudes en fonction du maillage x,y
A=fscanf(fid,'%f',[ncols,nrows]);

fclose(fid);

A=A';
A(A==NODATA_value)=NaN;

% echelle X,Y en lambert 93
X=xllcorner+cellsize/2:cellsize:xllcorner+cellsize*(ncols);
Y=yllcorner-cellsize/2+cellsize*(nrows):-cellsize:yllcorner;