function [deg]=dms2deg(dms)
% conversion en degres de coordonnees de format {'Ndd.mm.ss.dec'}
dms=cell2mat(dms);
deg=zeros(size(dms,1));
degre=str2num(dms(:,2:3));
minute=str2num(dms(:,5:6));
seconde=str2num(dms(:,8:end));
deg=degre+minute./60+seconde./3600;
signe=dms(:,1);
deg(signe=='S')=-deg(signe=='S');
deg(signe=='W')=-deg(signe=='W');
end
