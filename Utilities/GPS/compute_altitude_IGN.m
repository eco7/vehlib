function [alt, ERR] = compute_altitude_IGN(coordonnee, pathDalle, carteType)
% This function calculate the altitude with RGEALTI V2 or BDALTI V2
% input:
% coordonnee:[longitude and latitude] are in degre.decimal values
% pathDalle : the path to locate the database
% carteType: to be choosen between 'BDALTI' or 'RGEALTI';
% output:
% altitude is in meter
% To be tested with several dalle ...

ERR=struct([]);

if length(coordonnee(1,:))~=2
    chaine=' first argument  of function compute_altitude_IGN should be a vector of 2 columns\n ';
    ERR=MException('IGN:compute_altitude_IGN',chaine)
    alt=NaN;
    return;
end
longitude=coordonnee(:,1);
latitude=coordonnee(:,2);
% Passage en projection lambert93
[X,Y]=projLambert93(longitude,latitude);

% Create the list of the 
fileList=lsFiles(pathDalle,'.asc');

if isempty(fileList)
    chaine=' No Dalle found\n';
    ERR=MException('IGN:compute_altitude_IGN',chaine)
    alt=NaN;
    return;
end

% les fichiers comprennent l'extension _MNT_ pour Modele Numerique de Terrain
% Ici tous les fichiers match ==> fileList est inchange
[ fileList ] = findPattern( fileList, '_MNT_' );
 
x=NaN(size(fileList));
y=NaN(size(fileList));

% Create the grid
for ind=1:length(fileList)
   %regexp(fileList{ind},FXX_(.*)_(.*)_MNT','tokens')
   [~, f, ~]=fileparts(fileList{ind});
   expr=regexp(f,'_','split');
   if strcmp(carteType,'BDALTI')
       x(ind)=str2double(expr{4})*1000; % passage en km -> m
       y(ind)=str2double(expr{5})*1000;
       a=(regexp(expr{2},'(.*)M','tokens'));
       pas(ind)=str2double(a{1});
   elseif strcmp(carteType,'RGEALTI')
       x(ind)=str2double(expr{3})*1000; % passage en km -> m
       y(ind)=str2double(expr{4})*1000;
       pas(ind)=5;
       % Les noeuds sont au centre des mailles, la couverture est a pas/2
       % du premier noeud (Nord Ouest)
       x(ind) = x(ind) - pas(ind)/2;
       y(ind) = y(ind) + pas(ind)/2;
    end
end

if length(unique(pas))~=1
    disp('Probleme : le pas est variable');
    return
else
    couverture=pas(1)*1000; % Le pas est de 5m (RGEALTI) ou 25m (BDALTI); il y a une matrice de 1000x1000 points par fichier
end

Xpos=X;
Xtemp=Xpos;
Ypos=Y;
Ytemp=Ypos;
alt=NaN*length(Xpos);

while ~isempty(Xtemp)
    % Recherche de la dalle qui contient le premier point
    dalleNumber = find(Xtemp(1)>x & Xtemp(1)<x+couverture & Ytemp(1)<y & Ytemp(1)>y-couverture, 1);
    if isempty(dalleNumber)
        % Un probleme; il manque des dalles ???
        disp('Probleme : manque t il des dalles ?');
        break
    end
    
    % Recherche de tous les points compris dans la dalle
    coordTrouvee = find(Xpos>x(dalleNumber) & Xpos<x(dalleNumber)+couverture & Ypos <y(dalleNumber) & Ypos>y(dalleNumber)-couverture);
    
    % Calcul des altitudes de ces points
    [X,Y,A]=lecture_MNT_IGN(fileList{dalleNumber});
    % Les noeuds (X,Y) sont au centre du pixel. 
    % Pour les noeuds en bord de grille (pas/2 en dehors des noeuds du
    % bord) interp2 va renvoyer NaN (cf lecture_MNT_IGN) Il faudrait
    % assembler 2 grilles adjacentes ...
    % On sature aiu maximum a pas/2 pres pour simplification
    alt(coordTrouvee)=interp2(X,Y,A, ...
        min(max(min(X),Xpos(coordTrouvee)),max(X)), ...
        min(max(min(Y),Ypos(coordTrouvee)),max(Y)));

    % Petite verification ...
    if sum((min(X)-Xpos(coordTrouvee))>pas/2)>=1 | sum((Xpos(coordTrouvee)-max(X))>pas/2)>=1 | ...
            sum((min(Y)-Ypos(coordTrouvee))>pas/2)>=1 | sum((Ypos(coordTrouvee)-max(Y))>pas/2)>=1
        disp('compute_altitude_IGN : Probleme');
    end
    % Epuration des vecteurs Xtemp et Ytemp
    coordTrouvee = find(Xtemp>x(dalleNumber) & Xtemp<x(dalleNumber)+couverture & Ytemp <y(dalleNumber) & Ytemp>y(dalleNumber)-couverture);
    Xtemp = setdiff(Xtemp,Xtemp(coordTrouvee));
    Ytemp = setdiff(Ytemp,Ytemp(coordTrouvee));
    
    if isempty(coordTrouvee)
        % Un probleme; il manque des dalles ???
        disp('Probleme : manque t il des dalles ?');
        break
    end
end

end


