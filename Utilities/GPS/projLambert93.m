% Function: projLambert93
%
% Objet:
%
% fonction de transformation des coordonnees depuis WSG84 en projection cartographique conique de lambert93
%
% notes techniques IGN NTG71
%
% notes techniques IGN NTG87-1
%
% Arguments d'appel:
%
% longitude = longitude en degre
%
% latitude = latitude en degre
% 
% Arguments de retour:
%
% X = position est/ouest en metre (+est -ouest)
%
% Y = position nord/sud en metre (+nord -sud)

function [X,Y]=projLambert93(longitude,latitude)
% passage des coordonnees en radian
longitude=longitude*pi/180;
latitude=latitude*pi/180;

% % constante pour lambert 93
% ellipsoide GRS80
a=6378137;          % 1/2 grand axe ellipsoide GRS80
f=1/298.257222101;  % aplatissement ellipsoide GRS80
e=sqrt(2*f-f^2);    % premiere excentricite de l'ellipsoide
% projection conique conforme secante
lon0=3*pi/180;      % meridien central 3� E Greenwich
lat0=46.5*pi/180;   % latitude origine (46.3 ?)
lat1=44*pi/180;     % parallele echelle conservee
lat2=49*pi/180;     % parallele echelle conservee
X0=700000;          % coordonnees de l'origine
Y0=6600000;         % coordonnees de l'origine

% determination des parametres de projection cas secant
[e,n,c,lon_c,Xs,Ys]=alg0054(a,e,lon0,lat0,lat1,lat2,X0,Y0);

% projection coordonnees
[X,Y]=alg0003(e,n,c,lon_c,Xs,Ys,longitude,latitude);

%%
    % transformation coordonnees
    function [X,Y]=alg0003(e,n,c,lon_c,Xs,Ys,longitude,latitude)
        Lat_iso=alg0001(latitude,e);
        X=Xs + c.*exp(-n.*Lat_iso).*sin(n.*(longitude-lon_c));
        Y=Ys - c.*exp(-n.*Lat_iso).*cos(n.*(longitude-lon_c));
    end

    % determination des parametres de calcul cas tangent
    function [e,n,c,lon_c,Xs,Ys]=alg0019(a,e,lon0,lat0,k0,X0,Y0)
        lon_c=lon0;
        n=sin(lat0);
        N=alg0021(lat0,a,e);
        Lat_iso0=alg0001(lat0,e);
        c=k0.*N.*cot(lat0).*exp(n.*Lat_iso0);
        Xs=X0;
        Ys=Y0+k0.*N.*cot(lat0);
    end

    % determination des parametres de calcul cas secant
    function [e,n,c,lon_c,Xs,Ys]=alg0054(a,e,lon0,lat0,lat1,lat2,X0,Y0)
        lon_c=lon0;
        N1=alg0021(lat1,a,e);
        N2=alg0021(lat2,a,e);
        lat_iso1=alg0001(lat1,e);
        lat_iso2=alg0001(lat2,e);
        
        n=log((N2.*cos(lat2))/(N1.*cos(lat1)))/(lat_iso1-lat_iso2);
        
        c=((N1.*cos(lat1))/n).*exp(n.*lat_iso1);
        
        if lat0==pi/2
            Xs=X0;
            Ys=Y0;
        else
            lat_iso0=alg0001(lat0,e);
            Xs=X0;
            Ys=Y0+c.*exp(-n.*lat_iso0);
        end
            
    end

    % calcul de la grande normale
    function [N]=alg0021(lat,a,e)
        N=a./sqrt(1-e^2.*sin(lat)^2);
    end

    % calcul de la latitude isometrique
    function [Lat_iso]=alg0001(latitude,e)
        
        Lat_iso=log(tan(pi/4+latitude/2).*((1-e.*(sin(latitude)))./(1+e.*(sin(latitude)))).^(e/2));
        
    end

end
