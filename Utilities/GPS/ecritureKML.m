function ecritureKML(Lat,Lon,Alt,FileNameKml,PathNameKml)
%
%
n1=length(Lat);
n2=length(Lon);
if (n1~=n2)
   error('Lat and Lon vectors should have the same length');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creating kml file
%
fp=fopen(fullfile(PathNameKml,FileNameKml),'w');
if (fp==-1)
    message=sprint('Unable to open file %s.kml',fullfile(PathNameKml,FileNameKml));
    error(message);
end
fprintf(fp,'<?xml version="1.0" encoding="UTF-8"?>\n');
fprintf(fp,'<kml xmlns="http://earth.google.com/kml/2.1">\n');
fprintf(fp,'<Document>\n');
fprintf(fp,'<name>%s</name>\n',FileNameKml);
fprintf(fp,'<description>test</description>\n');
%
%Symbol styles definition
fprintf(fp,'<Style id="mystyle">\n');
fprintf(fp,'   <IconStyle>\n');
fprintf(fp,'      <scale>%.2f</scale>\n',1); %scale adjusted for .png image sizes
fprintf(fp,'      <Icon><href>%s</href></Icon>\n','icon');
fprintf(fp,'   </IconStyle>\n');
fprintf(fp,'   <LineStyle>\n');
fprintf(fp,'      <color>%s</color>\n','red');
fprintf(fp,'      <width>%d</width>\n',2);
fprintf(fp,'   </LineStyle>\n');
fprintf(fp,'</Style>\n');
fprintf(fp,'\n');


fprintf(fp,'    <Placemark>\n');
    fprintf(fp,'      <description><![CDATA[Test]]></description>\n');
fprintf(fp,'      <name>Line</name>\n');
fprintf(fp,'      <visibility>1</visibility>\n');
fprintf(fp,'      <open>1</open>\n');
fprintf(fp,'      <styleUrl>mystyle</styleUrl>\n');
fprintf(fp,'      <LineString>\n');
fprintf(fp,'        <extrude>0</extrude>\n');
fprintf(fp,'        <tessellate>0</tessellate>\n');
fprintf(fp,'        <altitudeMode>clampToGround</altitudeMode>\n');
fprintf(fp,'        <coordinates>\n');
for k=1:n1
    fprintf(fp,'%.6f,%.6f,%.6f\n',Lon(k),Lat(k),Alt(k));
end
fprintf(fp,'        </coordinates>\n');
fprintf(fp,'      </LineString>\n');
fprintf(fp,'    </Placemark>\n');

fprintf(fp,'</Document>\n');
fprintf(fp,'</kml>\n');

fclose(fp);

