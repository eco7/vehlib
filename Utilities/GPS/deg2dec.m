% Function: deg2dec
function [ out ] = deg2dec( in )
% Convert from degre to decimal values
floor_in=floor(in);
deg_in=in-floor_in;

dec_in=deg_in*100/60;
out=floor_in+dec_in;

end

