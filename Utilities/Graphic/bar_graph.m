% Function: bar_graph
% Utilitaires de VEHLIB pour tracer des diagrammes a bar.
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Utilitaires de VEHLIB pour tracer des diagrammes a bar.
%
% Arguments:
% valeur: matrice des valeurs n*m
%              
% lab:    cell array de labels a afficher en x : taille n
%
% legende:cell arrays de legende: taille m
%
% titre: titre du graphique
%
%
% exemple de lancement :
% bar_graph(rand(2,3),{'prems';'deuz'},{'v1';'v2';'v3'},'titre')
%
% Avril 2006
% ------------------------------------------------------
function [erreur]=bar_graph(valeur,lab,legende,titre)

if length(lab)~=length(valeur(:,1))
    'erreur1'
    erreur=1;
    return
end

if ~isempty(legende) & length(legende)~=length(valeur(1,:))
    'erreur2'
    erreur=2;
    return
end

bar(valeur,'group');

% Positionnement des tirets et suppression des etiquettes
set(gca,'Xtick',1:length(valeur(:,1)),'XtickLabel','');
% nouvelles etiquettes
lab=strrep(lab,'_','\_');
hx=get(gca,'Xlabel');
set(hx,'unit','data');
pos=get(hx,'position');
y=pos(2);

clear t
for i=1:length(lab)
    t(i)=text(i,y,lab{i,:});
end
set(t,'Rotation',70,'HorizontalAlignment','right');

if ~isempty(legende)
    legend(legende);
end
title(titre)

grid on;
