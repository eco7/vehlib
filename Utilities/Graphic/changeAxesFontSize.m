function changeAxesFontSize(hf,legSize,tSize,xSize,ySize)
%changeAxesFontSize change la taille des polices d'une figure
% changeAxesFontSize(hf,legSize,tSize,xSize,ySize)
%
% hf: handle de figure
% legSize: 1x1 double taille pour les legendes
% tSize: 1x1 double taille pour les titres (optionnel)
% xSize: 1x1 double taille pour les xlabel (optionnel)
% ySize: 1x1 double taille pour les ylabel (optionnel)
%Si ySize non fourni, il prend xSize pour les ylabel
%Si xSize non fourni, il prend tSize pour les xlabel et ylabel
%Si tSize non fourni, il prend legSize pour tous
if ~exist('tSize','var')
    tSize = legSize;
end
if ~exist('xSize','var')
    xSize = tSize;
end
if ~exist('ySize','var')
    ySize = xSize;
end
ha = findobj(hf,'type','axes','tag','');%axes (not legend) handle list
hl = findobj(hf,'type','axes','tag','legend');%legend hadle list

%legSize: legend font size
ht = findobj(hl,'type','text');%legend text
set(ht,'FontSize',legSize);

%tSize: titles font size
ht = get(ha,'title');
if iscell(ht)
%     ht = cell2mat(ht);
    ht = [ht{:}];%MATLAB2017 compatibility
end
set(ht,'FontSize',tSize);
%xSize: xlabel font size
ht = get(ha,'xlabel');
if iscell(ht)
%     ht = cell2mat(ht);
    ht = [ht{:}];%MATLAB2017 compatibility
end
set(ht,'FontSize',xSize);
%ySize: ylabel font size
ht = get(ha,'ylabel');
if iscell(ht)
%     ht = cell2mat(ht);
    ht = [ht{:}];%MATLAB2017 compatibility
end
set(ht,'FontSize',ySize);

set(ha,'FontSize',ySize);
end