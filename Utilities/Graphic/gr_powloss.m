%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Function:      gr_powloss                                                 %
%Objective:     plots power distribution for vehicles                      %
%Inputs:        vehicle architecture                                       %
%                                                                          %
%Author:        Felicitas Mensing
%Date:          04/03/2011
%                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function[ERR]=gr_powloss(arch,MOTH,cons_mt,cmt,wmt,caccm1,caccm2,cprim_emb1,wprim_emb1,csec_emb1,wsec_emb1,csec_bv,wsec_bv,csec_red,wsec_red,croue,wroue)

ERR=[];

switch upper(arch)
    
    case 'VTH_BV'
        %make strings that will be evaluated in 'base'
       
        %plot pie chart
        set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));
        
        P_fuel=cons_mt.*MOTH.pci;       %power in fuel
        P_eng=cmt.*wmt;                 %power out from engine
        P_acc=(caccm1+caccm2).*wmt;     %power used by auxiliaries
        P_1=cprim_emb1.*wprim_emb1;     %power clutch engine side
        P_Jeng=P_eng-P_1-P_acc;         %power used by engine inertia
        P_2=csec_emb1.*wsec_emb1;       %power transmitted through clutch
        P_3=csec_bv.*wsec_bv;           %power transmitted through gear box
        P_4=csec_red.*wsec_red;         %power tranmitted through final drive gear
        P_out=croue.*wroue;             %power out at wheels
        
        Loss_tot=P_fuel-P_out;
        
        %sum, final percentages over cycle
        %percent of fuel power going into ...
        SPer_fuel_Leng=sum((P_fuel-P_eng))/sum(P_fuel)*100;
        SPer_fuel_Pacc=sum(abs(P_acc))/sum(P_fuel)*100;
        SPer_fuel_PJmt=sum(P_Jeng)/sum(P_fuel)*100;
        SPer_fuel_Pout=sum(P_out)/sum(P_fuel)*100;
        SPer_fuel_Lclutch=sum((P_1-P_2))/sum(P_fuel)*100;
        SPer_fuel_Lgears=sum((P_2-P_3))/sum(P_fuel)*100;
        SPer_fuel_LFD=sum((P_3-P_4))/sum(P_fuel)*100;
        %percent engine power going into ...
        SPer_eng_Pacc=sum(abs(P_acc))/sum(P_eng)*100;
        SPer_eng_PJmt=sum(P_Jeng)/sum(P_eng)*100;
        SPer_eng_Pout=sum(P_out)/sum(P_eng)*100;
        SPer_eng_Lclutch=sum((P_1-P_2))/sum(P_eng)*100;
        SPer_eng_Lgears=sum((P_2-P_3))/sum(P_eng)*100;
        SPer_eng_LFD=sum((P_3-P_4))/sum(P_eng)*100;
%         %total power loss going into ...
        SPer_loss_Leng=sum((P_fuel-P_eng))/sum(Loss_tot)*100;
        SPer_loss_Pacc=sum(abs(P_acc))/sum(Loss_tot)*100;
        SPer_loss_PJmt=sum(P_Jeng)/sum(Loss_tot)*100;
        SPer_loss_Lclutch=sum(P_1-P_2)/sum(Loss_tot)*100;
        SPer_loss_Lgears=sum(P_2-P_3)/sum(Loss_tot)*100;
        SPer_loss_LFD=sum(P_3-P_4)/sum(Loss_tot)*100;
        
        x = [SPer_fuel_Pout SPer_fuel_LFD SPer_fuel_Lgears SPer_fuel_Pacc SPer_fuel_PJmt SPer_fuel_Leng SPer_fuel_Lclutch];
        explode = [0 0 0 0 0 0 0];
        subplot(131)
        pie(x,explode)
        colormap jet
        title('Energy distribution(avg): Energy in fuel goes to x percent into ...');
        legend('Road','Final drive','Gears','Auxiliaries','Engine inertia','Engine losses','Clutch');
        subplot(132)
        x = [SPer_eng_Pout SPer_eng_LFD SPer_eng_Lgears SPer_eng_Pacc SPer_eng_PJmt SPer_eng_Lclutch];
        explode = [0 0 0 0 0 0];
        pie(x,explode)
        colormap jet
        title('Energy distribution(avg): Energy out engine goes to x percent into ...');
        legend('Road','Final drive','Gears','Auxiliaries','Engine inertia','Clutch');        
        subplot(133)
        x = [SPer_loss_LFD SPer_loss_Lgears SPer_loss_Pacc SPer_loss_PJmt SPer_loss_Leng SPer_loss_Lclutch];
        explode = [0 0 0 0 0 0];
        pie(x,explode)
        colormap jet
        title('Loss distribution(avg)');
        legend('Final drive','Gears','Auxiliaries','Engine inertia','Engine losses','Clutch');

        set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));       
        
    case 'VELEC'
        iacm1=wsec_emb1;
        uacm1=csec_emb1;
        
        iacc=wprim_emb1;
        uacc=cprim_emb1;
        
        ibat=caccm1;
        ubat=caccm2;
        pertes_bat=cons_mt;
              
        %plot pie chart
        set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));
        
        P_in=ibat.*ubat+pertes_bat;
        P_bat=ibat.*ubat;
        P_acc=iacc.*uacc;
        P_mt_in=iacm1.*uacm1;
        P_mt=cmt.*wmt;
        P_3=csec_bv.*wsec_bv;
        P_4=csec_red.*wsec_red;
%         P_Jmt=P_mt-P_3;
        P_out=croue.*wroue;
        
%         Pbal=P_bat-P_acc-P_mt_in;
        
        Loss_tot=P_in-P_out;
        
        %sum, final percentages over cycle
        %percent of pow power going into ...
        SPer_pow_LBat=sum(P_in-P_bat)/sum(P_in)*100;
        SPer_pow_Lmt=sum(P_mt_in-P_mt)/sum(P_in)*100;
        SPer_pow_Pacc=sum(P_acc)/sum(P_in)*100;
%         SPer_pow_PJmt=sum(P_Jmt)/sum(P_in)*100;
        SPer_pow_Pout=sum(P_out)/sum(P_in)*100;
        SPer_pow_LFD=sum(P_3-P_4)/sum(P_in)*100;
        %percent motor power going into ...
%         SPer_mt_PJmt=sum(P_Jmt)/sum(P_mt)*100;
        SPer_mt_Pout=sum(P_out)/sum(P_mt)*100;
        SPer_mt_LFD=sum(P_3-P_4)/sum(P_mt)*100;
        %total power loss going into ...
        SPer_loss_LBat=sum(P_in-P_bat)/sum(Loss_tot)*100;
        SPer_loss_Lmt=sum(P_mt_in-P_mt)/sum(Loss_tot)*100;
        SPer_loss_Pacc=sum(P_acc)/sum(Loss_tot)*100;
%         SPer_loss_PJmt=sum(P_Jmt)/sum(Loss_tot)*100;
        SPer_loss_LFD=sum(P_3-P_4)/sum(Loss_tot)*100;
    
        
       %power distribution average in pie chart
        x = [SPer_pow_Pout SPer_pow_LFD SPer_pow_Pacc SPer_pow_Lmt SPer_pow_LBat];
        explode = [0 0 0 0 0];
        subplot(131)
        pie(x,explode)
        colormap jet
        title('Power distribution(avg): Energy out batt goes to x percent into ...');
        legend('Power out road','Losses final drive','Power auxiliaries','Motor power losses','Losses battery');
        subplot(132)
        x = [SPer_mt_Pout SPer_mt_LFD];
        explode = [0 0];
        pie(x,explode)
        colormap jet
        title('Power distribution(avg): power out motor goes to x percent into ...');
        legend('Power out road','Losses final drive');
        subplot(133)
        x = [SPer_loss_LFD SPer_loss_Pacc SPer_loss_Lmt SPer_loss_LBat];
        explode = [0 0 0 0];
        pie(x,explode)
        colormap jet
        title('Loss distribution(avg)');
        legend('Losses final drive','Power auxiliaries','motor power losses','Losses battery');

    

        set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));
        
        
        
    otherwise
        ERR='this vehicle architecture is not supported by this program';
        evalin('base',ERR);
end
        