function fig2png(fileFig)

filePng = regexprep(fileFig,'.fig$','.png');
h = openfig(fileFig);

fprintf('%s >>> %s',fileFig,filePng);
myStyle = hgexport('factorystyle');
myStyle.Resolution = '300';
hgexport(h, filePng,hgexport('factorystyle'), 'Format', 'png');
fprintf(' OK\n');

end