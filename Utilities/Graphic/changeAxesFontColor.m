function changeAxesFontColor(hf,legColor,tColor,xColor,yColor)
%changeAxesFontColor Change la couleur des polices d'une figure
% changeAxesFontSize(hf,legColor,tColor,xColor,yColor)
%
% hf: handle de figure
% legColor: 1x3 double couleur pour les legendes
% tColor: 1x3 double couleur pour les titres (optionnel)
% xColor: 1x3 double couleur pour les xlabel (optionnel)
% yColor: 1x3 double couleur pour les ylabel (optionnel)
% Si yColor non fourni, il prend xColor pour les ylabel
% Si xColor non fourni, il prend tColor pour les xlabel et ylabel
% Si tColor non fourni, il prend legColor pour tous
if ~exist('tColor','var')
    tColor = legColor;
end
if ~exist('xColor','var')
    xColor = tColor;
end
if ~exist('yColor','var')
    yColor = xColor;
end
ha = findobj(hf,'type','axes','tag','');%axes (not legend) handle list
hl = findobj(hf,'type','axes','tag','legend');%legend hadle list
set(hl,'TextColor',legColor);
set(hl,'EdgeColor',legColor);
%legColor: legend font size
% ht = findobj(hl,'type','text');%legend text
% set(ht,'Color',legColor);

%tColor: titles font size
ht = get(ha,'title');
if iscell(ht)
    ht = cell2mat(ht);
end
set(ht,'Color',tColor);
%xColor: xlabel font size
ht = get(ha,'xlabel');
if iscell(ht)
    ht = cell2mat(ht);
end
set(ht,'Color',xColor);
%yColor: ylabel font size
ht = get(ha,'ylabel');
if iscell(ht)
    ht = cell2mat(ht);
end
set(ht,'Color',yColor);

% set(ha,'FontColor',yColor);
end