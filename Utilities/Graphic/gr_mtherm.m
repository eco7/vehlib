% -------------------------------------------------------------------------
% function []=gr_mtherm(archit,MOTH,wmt,cmt)
%
% IFSTTAR-LTE Dec 2004
% Utilitaires de VEHLIB pour tracer des cartographies de moteur thermique.
%
% MOTH = structure renseignant les caracteristiques du moteur a cartographier.
% Doit respecter les contraintes de VEHLIB, soit:
% MOTH.Reg_2dconso= vecteur ligne des regimes en rd/s (size:1*a).
% MOTH.Cpl_2dconso= vecteur ligne des couple en Nm (size:1*b).
% MOTH.Conso_2d   = matrice des debits instantanees de carburants en g/s (size:a*b).
% MOTH.wmt_max    = vecteur ligne des regime (rd/s) pour calcul couple maxi (size:1*c)
% MOTH.cmt_max    = vecteur lihne des couples (Nm)  (size(:1*c)
% MOTH.nom        = string contenant le nom du fichier moteur
% wmt et cmt      = points de fonctionnements en regime et couple sur le dernier calcul effectu� (optionnels).
% archit          = string architecture au sens vehlib (peut etre remplace par chaine nulle)
%
% exemple de lancement sans arguments:
% Initalise an engine model :
% MTHERM_EP6
% gr_mtherm('',MOTH)
% exemple de lancement avec couple et choix du type de tracé gr_mtherm('',MOTH,wmt,cmt,'CSP','2D')
% -------------------------------------------------------------------------
function []=gr_mtherm(archit,MOTH,wmt,cmt,type_carto,type_graph)
%% Requetes utilisateur
% Type de carto a trace
if nargin ~=6 && MOTH.ntypmoth ~=5
    type_carto = questdlg('Quelle cartographie voulez vous afficher ?', ...
        'Choix d''une cartographie moteur','CSP','Debit de Carburant','EFF','CSP');
    if MOTH.ntypmoth == 10
        type_graph = questdlg('Graph 2D (type contour) ou 3D (type surf) ?', ...
            'Choix d''une representation graphique','2D','3D','2D');
    else
        type_graph = '2D';
    end
end
% Fenetre de trace
set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));
hold off;

%% Calcul
if MOTH.ntypmoth == 10
    % Modele cartographie
    % Mappage de la carto existante
    [R,C] = meshgrid(MOTH.Reg_2dconso,MOTH.Cpl_2dconso);
    DCARB = MOTH.Conso_2d';
    F = TriScatteredInterp(R(:),C(:),DCARB(:));
    
    % Choix d un pas de maillage de la cartographie
    %   En Regime
    reg = min(MOTH.Reg_2dconso):(max(MOTH.Reg_2dconso)-min(MOTH.Reg_2dconso))/100:max(MOTH.Reg_2dconso);
    %   En Couple
    cple = max(0,min(MOTH.Cpl_2dconso)):(max(MOTH.Cpl_2dconso)-min(MOTH.Cpl_2dconso))/100:max(MOTH.Cpl_2dconso);
    
    % Creation d'une nouvelle cartographie
    [REG,CPLE] = meshgrid(reg,cple);
    CONSO = F(REG,CPLE);
    
    P_kW = REG .* CPLE /1000;
    CSP = CONSO * 3600 ./P_kW;
    EFF = 100 * P_kW*1000 ./ (MOTH.pci * CONSO);
    
    % Reference ISO
    seuilbas_cple = 0.01*max(MOTH.cmt_max);
    min_CSP = ceil(min(min(CSP(~isnan(CSP) & ~isinf(CSP) & CPLE>seuilbas_cple)))/10)*10;
    max_EFF = ceil(max(max(EFF(~isnan(EFF) & ~isinf(EFF) & CPLE>seuilbas_cple)))/10)*10;
    
    %% Tracage
    switch type_graph
        case '2D'
            if isfield(MOTH,'wmt_min')
                plot(MOTH.wmt_min*30/pi,MOTH.cmt_min);
            else
                plot(MOTH.wmt_max*30/pi,MOTH.cmt_min);
            end
            hold on;
            grid on;
            % Trace la courbe de pleine charge
            wmt_max = [MOTH.wmt_max MOTH.ral];
            cmt_max = [MOTH.cmt_max 0];
            [wmt_max, I] = sort(wmt_max);
            cmt_max =cmt_max(I);
            plot(wmt_max*30/pi,cmt_max);
            
            % Trace la courbe de fonctionnement optimum pour les hybrides
            if exist('archit','var') & ~isempty(archit) & ~(strcmp(archit,'VTH_BV') | strcmp(archit,'VTH_CPLHYD')) & isfield(MOTH,'wmt_opti')
                plot(MOTH.wmt_opti*30/pi,MOTH.cmt_opti,'r');
            end
            ax=['[0 ',num2str(MOTH.wmt_max(length(MOTH.wmt_max))*30/pi),' ',num2str(floor(min(MOTH.cmt_min)*1.1)),' ',num2str(ceil(max(MOTH.cmt_max)*1.1)),']'];
            eval(['axis(',ax,')']);
            
            % Trace les points de fonctionnement du moteur pendant le dernier calcul si existant
            if nargin >= 4
                % plot(wmt*30/pi,cmt,'k+');
                % occurences des pt de fonctionnement
                mat_fct = [wmt(:)*30/pi,cmt(:)];
                [mat_fct_uniq,~,occurences] = unique(mat_fct,'rows');
                plot_fct = [mat_fct_uniq, accumarray(occurences, 1)];
                scatter(plot_fct(:,1),plot_fct(:,2),plot_fct(:,3)*10,'filled','or');
                
            end
            
            % Choix des labels
            xlabel('Regime moteur thermique en Tr/mn');
            ylabel('Couple sur l arbre en Nm');
            if exist('archit','var') && ~isempty(archit) && ~(strcmp(archit,'VTH_BV') | strcmp(archit,'VTH_CPLHYD')) & isfield(MOTH,'wmt_opti')
                legend('Couple enveloppe','Couple de frottement ','Couple optimum calculateur','Points de fonctionnement sur le cycle');
            elseif evalin('base','exist(''wmt'',''var'')')
                legend('Couple enveloppe','Points de fonctionnement sur le cycle');
            else
                legend('Couple enveloppe');
            end
            
            % Restriction des surfaces aux limites de la carto
            wmt_max = [MOTH.wmt_max MOTH.ral];
            cmt_max = [MOTH.cmt_max 0];
            [wmt_max, I] = sort(wmt_max);
            cmt_max =cmt_max(I);
            CMAX = interp1(wmt_max,cmt_max,REG);
            
            CMAX(MOTH.wmt_max<MOTH.ral) = 0;
            CSP(CPLE>CMAX) = NaN;
            CONSO(CPLE>CMAX) = NaN;
            EFF(CPLE>CMAX) = NaN;
            
            % Apres l affichage de la legende, on trace les iso csp
            % Trace en specifiant les iso consommations a ploter
            if strcmp(type_carto,'CSP')
                palier_pct = [0 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.4, 0.5, 0.75, 1, 1.25, 1.5];
                niveaux = min_CSP * ones(1,length(palier_pct));
                niveaux = niveaux + niveaux.*palier_pct;
                [c,h]=contour(REG*30/pi,CPLE,CSP,niveaux);
                % Rajout des labels sur les courbes
                clabel(c,h);
                % Choix d un titre
                if isfield(MOTH,'nom')
                    title(strcat('Consommation specifique (g/kWh) du moteur thermique :',strrep(MOTH.nom,'_','\_')));
                else
                    title('Consommation specifique (g/kWh) du moteur thermique');
                end
            elseif strcmp(type_carto,'EFF')
                palier_pct = [0 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.4, 0.5, 0.75, 1, 1.25, 1.5];
                niveaux = max_EFF * ones(1,length(palier_pct));
                niveaux = niveaux - niveaux.*palier_pct;
                [c,h]=contour(REG*30/pi,CPLE,EFF,niveaux);
                % Rajout des labels sur les courbes
                clabel(c,h);
                % Choix d un titre
                if isfield(MOTH,'nom')
                    title(strcat('Rendement (%) du moteur thermique :',strrep(MOTH.nom,'_','\_')));
                else
                    title('Rendement (%) du moteur thermique');
                end
            else
                [c,h]=contour(REG*30/pi,CPLE,CONSO,[0 0.1:0.2:0.5 1:0.5:20]);
                % Rajout des labels sur les courbes
                clabel(c,h);
                % Choix d un titre
                if isfield(MOTH,'nom')
                    title(strcat('Debit de carburant (g/s) du moteur thermique :',strrep(MOTH.nom,'_','\_')));
                else
                    title('Debit de carburant (g/s) du moteur thermique');
                end
            end
            
        case '3D'
            % Calcul pour la courbe de pleine charge et optimum pour les hybrides
            cmax = interp1(MOTH.wmt_max,MOTH.cmt_max,reg);
            cons_cmax = interp2(REG,CPLE,CONSO,reg,cmax);
            csp_cmax = interp2(REG,CPLE,CSP,reg,cmax);
            if isfield(MOTH,'wmt_min')
                cmin = interp1(MOTH.wmt_min,MOTH.cmt_min,reg);
            else
                cmin = interp1(MOTH.wmt_max,MOTH.cmt_min,reg);
            end
            cons_cmin = interp2(REG,CPLE,CONSO,reg,cmin);
            csp_cmin = interp2(REG,CPLE,CONSO,reg,cmin);
            
            if exist('archit','var') & ~isempty(archit) & ~(strcmp(archit,'VTH_BV') | strcmp(archit,'VTH_CPLHYD'))
                copti = interp1(MOTH.wmt_opti,MOTH.cmt_opti,reg);
                cons_copti = interp2(REG,CPLE,CONSO,reg,copti);
                csp_copti = interp2(REG,CPLE,CSP,reg,copti);
            end
            
            % Trace
            switch type_carto
                case 'CSP'
                    % Trace courbe pleine charge et coupure d'injection
                    % Pour le confort visuel
                    csp_cmax(csp_cmax>2*min_CSP) = NaN;
                    csp_cmax(csp_cmax<0) = NaN;
                    csp_cmin(csp_cmin>2*min_CSP) = NaN;
                    csp_cmin(csp_cmin<0) = NaN;
                    
                    plot3(reg*30/pi,cmax,csp_cmax,'LineWidth',2);
                    hold on;
                    grid on;
                    plot3(reg*30/pi,cmin,csp_cmin,'LineWidth',2);
                    
                    % Trace la courbe de fonctionnement optimum pour les hybrides
                    if exist('archit','var') & ~isempty(archit) & ~(strcmp(archit,'VTH_BV') | strcmp(archit,'VTH_CPLHYD'))
                        % Pour le confort visuel
                        csp_copti(csp_copti>2*min_CSP) = NaN;
                        csp_copti(csp_copti<0) = NaN;
                        plot3(reg*30/pi,copti,csp_copti,'r');
                    end
                    ax=['[0 ',num2str(MOTH.wmt_max(length(MOTH.wmt_max))*30/pi),' ',num2str(floor(min(MOTH.cmt_min)*1.1)),' ',num2str(ceil(max(MOTH.cmt_max)*1.1)),']'];
                    eval(['axis(',ax,')']);
                    
                    % Trace les points de fonctionnement du moteur pendant le dernier calcul si existant
                    if nargin >= 4
                        csp = interp2(REG,CPLE,CSP,wmt,cmt);
                        % Pour le confort visuel
                        csp(csp>2*min_CSP) = NaN;
                        csp(csp<0) = NaN;
                        scatter3(wmt*30/pi,cmt,csp,'filled','k');
                    end
                    
                    
                case 'Debit de Carburant'
                    % Trace courbe pleine charge et coupure d'injection
                    plot3(reg*30/pi,cmax,cons_cmax,'LineWidth',2);
                    hold on;
                    grid on;
                    plot3(reg*30/pi,cmin,cons_cmin,'LineWidth',2);
                    
                    % Trace la courbe de fonctionnement optimum pour les hybrides
                    if exist('archit','var') & ~isempty(archit) & ~(strcmp(archit,'VTH_BV') | strcmp(archit,'VTH_CPLHYD'))
                        plot3(reg*30/pi,copti,cons_copti,'r');
                    end
                    ax=['[0 ',num2str(MOTH.wmt_max(length(MOTH.wmt_max))*30/pi),' ',num2str(floor(min(MOTH.cmt_min)*1.1)),' ',num2str(ceil(max(MOTH.cmt_max)*1.1)),']'];
                    eval(['axis(',ax,')']);
                    
                    
                    % Trace les points de fonctionnement du moteur pendant le dernier calcul si existant
                    if nargin >= 4
                        conso = interp2(REG,CPLE,CONSO,wmt,cmt);
                        scatter3(wmt*30/pi,cmt,conso,'filled','k');
                    end
                    
            end
            
            % Choix des labels
            xlabel('Regime moteur thermique en Tr/mn');
            ylabel('Couple sur l arbre en Nm');
            if strcmp(type_carto,'CSP')
                zlabel('CSP en g/kWh');
            else
                zlabel('Conso en g/s');
            end
            if exist('archit','var') && ~isempty(archit) && ~(strcmp(archit,'VTH_BV') | strcmp(archit,'VTH_CPLHYD'))
                legend('Couple enveloppe','Couple de frottement ','Couple optimum calculateur','Points de fonctionnement sur le cycle',2);
            elseif evalin('base','exist(''wmt'',''var'')')
                legend('Couple enveloppe','Points de fonctionnement sur le cycle',2);
            else
                legend('Couple enveloppe',2);
            end
            
            % Restriction des surfaces aux limites de la carto
            CMAX = interp1(MOTH.wmt_max,MOTH.cmt_max,REG);
            CSP(CPLE>CMAX) = NaN;
            CONSO(CPLE>CMAX) = NaN;
            
            % Apres l affichage de la legende, on trace les iso csp
            % Trace en specifiant les iso consommations a ploter
            if strcmp(type_carto,'CSP')
                % Pour le confort visuel
                CSP(CSP>2*min_CSP) = NaN;
                CSP(CSP<0) = NaN;
                
                surf(REG*30/pi,CPLE,CSP,'FaceAlpha',0.7,'EdgeColor','none')
                cbar = colorbar;
                set(get(cbar,'ylabel'),'String', 'CSP en g/kWh');
                % Choix d un titre
                if isfield(MOTH,'nom')
                    title(strcat('Consommation specifique (g/kWh) du moteur thermique :',strrep(MOTH.nom,'_','\_')));
                end
            else
                surf(REG*30/pi,CPLE,CONSO,'FaceAlpha',0.7,'EdgeColor','none')
                cbar = colorbar;
                set(get(cbar,'ylabel'),'String', 'Conso en g/s');
                % Choix d un titre
                if isfield(MOTH,'nom')
                    title(strcat('Debit de carburant (g/s) du moteur thermique :',strrep(MOTH.nom,'_','\_')));
                end
            end
    end
elseif MOTH.ntypmoth == 3
    % Choix d un pas de maillage de la cartographie
    %   En Regime
    reg = MOTH.ral:(MOTH.wmt_maxi-MOTH.ral)/100:MOTH.wmt_maxi;
    %   En Couple
    N=reg*30/pi;
    cmt_max = (MOTH.Tmax_N2*N.^2 + MOTH.Tmax_N*N + MOTH.Tmax_cst)*MOTH.Tmax/MOTH.Tmax_Tref;
    cmt_min = -(MOTH.PMF_cst + MOTH.PMF_N*N)*MOTH.Vd/(MOTH.Rev*2*pi);
    Cmax=max(cmt_max);
    cple = 0:Cmax/100:Cmax;
    
    % Creation d'une nouvelle cartographie
    [REG,CPLE] = meshgrid(reg,cple);
    CONSO = (REG.*CPLE + REG.*(MOTH.PMF_cst + MOTH.PMF_N*(REG*30/pi))*MOTH.Vd/(MOTH.Rev*2*pi))...
        ./(MOTH.n_fi*(MOTH.n_c0-MOTH.n_cA./(MOTH.n_cB+(REG*30/pi)))*MOTH.pci);
    
    P_kW = REG .* CPLE /1000;
    CSP = CONSO * 3600 ./P_kW;
    EFF = 100 * P_kW*1000 ./ (MOTH.pci * CONSO);
    
    % Reference ISO
    seuilbas_cple = 0.01*Cmax;
    min_CSP = ceil(min(min(CSP(~isnan(CSP) & ~isinf(CSP) & CPLE>seuilbas_cple)))/10)*10;
    max_EFF = ceil(max(max(EFF(~isnan(EFF) & ~isinf(EFF) & CPLE>seuilbas_cple)))/10)*10;
    
    %% Tracage
    % Trace la courbe de pleine charge
    plot([min(N)-eps N max(N)+eps],[0 cmt_max 0]);
    hold on;
    grid on;
    % Trace la courbe de coupure d'injection
    plot([min(N)-eps N max(N)+eps],[0 cmt_min 0]);
    
    ax=['[0 ',num2str(MOTH.wmt_maxi*30/pi*1.1),' ',num2str(floor(min(cmt_min)*1.1)),' ',num2str(ceil(max(cmt_max)*1.1)),']'];
    eval(['axis(',ax,')']);
    
    % Trace les points de fonctionnement du moteur pendant le dernier calcul si existant
    if nargin >= 4
        plot(wmt*30/pi,cmt,'k+');
    end
    
    % Choix des labels
    xlabel('Regime moteur thermique en Tr/mn');
    ylabel('Couple sur l arbre en Nm');
    if nargin >= 4
        legend('Couple enveloppe','Points de fonctionnement sur le cycle');
    else
        legend('Couple enveloppe');
    end
    
    % Restriction des surfaces aux limites de la carto
    CMAX = interp1(reg,cmt_max,REG);
    CSP(CPLE>CMAX) = NaN;
    CONSO(CPLE>CMAX) = NaN;
    EFF(CPLE>CMAX) = NaN;
    
    % Apres l affichage de la legende, on trace les iso csp
    % Trace en specifiant les iso consommations a ploter
    if strcmp(type_carto,'CSP')
        palier_pct = [0 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.4, 0.5, 0.75, 1, 1.25, 1.5];
        niveaux = min_CSP * ones(1,length(palier_pct));
        niveaux = niveaux + niveaux.*palier_pct;
        [c,h]=contour(REG*30/pi,CPLE,CSP,niveaux);
        % Rajout des labels sur les courbes
        clabel(c,h);
        % Choix d un titre
        if isfield(MOTH,'nom')
            title(strcat('Consommation specifique (g/kWh) du moteur thermique :',strrep(MOTH.nom,'_','\_')));
        end
    elseif strcmp(type_carto,'EFF')
        palier_pct = [0 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.4, 0.5, 0.75, 1, 1.25, 1.5];
        niveaux = max_EFF * ones(1,length(palier_pct));
        niveaux = niveaux - niveaux.*palier_pct;
        [c,h]=contour(REG*30/pi,CPLE,EFF,niveaux);
        % Rajout des labels sur les courbes
        clabel(c,h);
        % Choix d un titre
        if isfield(MOTH,'nom')
            title(strcat('Rendement (%) du moteur thermique :',strrep(MOTH.nom,'_','\_')));
        end
    else
        [c,h]=contour(REG*30/pi,CPLE,CONSO,[0 0.1:0.2:0.5 1:0.5:20]);
        % Rajout des labels sur les courbes
        clabel(c,h);
        % Choix d un titre
        if isfield(MOTH,'nom')
            title(strcat('Debit de carburant (g/s) du moteur thermique :',strrep(MOTH.nom,'_','\_')));
        end
    end
    
elseif MOTH.ntypmoth == 5
    VD.MOTH = MOTH;
    eval('initdata');
    VD.INIT = INIT;
        
    %% discretisation en couple et regime pour tracer les iso
    % vecteur regime
    d_w = 2;
    w_min = VD.MOTH.ral;
    w_max = VD.MOTH.wmt_maxi;
    if mod(w_min,d_w)==0
        wmt_vect = w_min:d_w:w_max;
    else
        wmt_vect = [w_min ceil(w_min/d_w)*d_w:d_w:w_max];
    end
    if w_max~=0
        wmt_vect = [wmt_vect w_max];
    end
    sz_w = size(wmt_vect);
    % vecteur couple et courbes de couple min/max
    d_C = 1;
    N = wmt_vect*30/pi;
    Cmt_min = -(VD.MOTH.PMF_cst + VD.MOTH.PMF_N*N + VD.MOTH.PMF_N2*N.^2)*VD.MOTH.Vd/(VD.MOTH.Rev*2*pi);
    % Cartography done for stoechiometric combustion, and
    % Optimal Spark Advance
    Phi = 1;
    delta_AA = 0;
    if isfield(VD.MOTH,'p_intake')
        Padm = interp1(VD.MOTH.p_intake_wmt,VD.MOTH.p_intake,wmt_vect,'linear','extrap');
    else
        Padm = 1000;
    end
    [~, Cmt_max] = calc_mt_3U(wmt_vect',Phi,delta_AA,Padm',1,VD);
    Cmt_max = Cmt_max';
    C_max = max(Cmt_max);
    C_min = min(Cmt_min);
    if mod(C_min,d_C)==0
        Cmt = C_min:d_C:C_max;
    else
        Cmt = [C_min ceil(C_min/d_C)*d_C:d_C:C_max];
    end
    if C_max~=0
        Cmt = [Cmt C_max];
    end
    sz_C = size(Cmt');
    % matrice couple, regime, PME pour le calcul des grandeurs
    wmt_mat = repmat(wmt_vect,sz_C);
    Cmt_mat = repmat(Cmt',sz_w);
    N_mat = wmt_mat*30/pi;
    
    %% calcul des grandeurs associees :
    hors_limite = Cmt_mat>repmat(Cmt_max,sz_C) | Cmt_mat<repmat(Cmt_min,sz_C);
    % - CSP
    [dcarb, ~] = calc_mt_3U_reverse(Cmt_mat,N_mat,Phi,delta_AA,VD);
    
    P_mat = wmt_mat.*Cmt_mat/1000; %kW
    CSP = dcarb./P_mat*3600; %g/kWh
    CSP(hors_limite & Cmt_mat>0) = nan;
    CSP(Cmt_mat<0) = inf;
    
    hold on;
    palier_pct = [0 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.4, 0.5, 0.75, 1, 1.25, 1.5];
    seuilbas_cple = 0.01*Cmt_max;
    min_CSP = ceil(min(min(CSP(~isnan(CSP) & ~isinf(CSP) & Cmt_mat>seuilbas_cple)))/10)*10;
    niveaux = min_CSP * ones(1,length(palier_pct));
    niveaux = niveaux + niveaux.*palier_pct;
    
    %% Tracage
    [c,h]=contour(wmt_mat*30/pi,Cmt_mat,CSP,niveaux);
    % Rajout des labels sur les courbes
    clabel(c,h);
    hold on;
    grid on;
    
    % Trace la courbe de coupure d'injection
    plot([min(N)-eps N max(N)+eps],[0 Cmt_min 0]);
    % Trace la courbe de pleine charge
    plot([min(N)-eps N max(N)+eps],[0 Cmt_max 0]);
    
    ax=['[0 ',num2str(MOTH.wmt_maxi*30/pi*1.1),' ',num2str(floor(min(Cmt_min)*1.1)),' ',num2str(ceil(max(Cmt_max)*1.1)),']'];
    eval(['axis(',ax,')']);
    
    % Trace les points de fonctionnement du moteur pendant le dernier calcul si existant
    if nargin >= 4
        plot(wmt*30/pi,cmt,'k+');
    end
 
    legend('BSFC (g/kWh)','\Gamma_{min}','\Gamma_{max}','Op. Points','Location','Southeast')
    % legend('CSE (g/kWh)','C_{min}','C_{max}','Chaud','Froid','Location','Southeast')
    if isfield(MOTH,'nom')
        title(strcat('Consommation specifique (g/kWh) du moteur thermique :',strrep(MOTH.nom,'_','\_')));
    end
    xlabel('Regime moteur thermique en Tr/mn');
    ylabel('Couple sur l arbre en Nm');
    
end

hold off;

end

