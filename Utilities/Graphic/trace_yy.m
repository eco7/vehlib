% Function: trace_yy
% Realisation de graphiques sur 2 axes
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% ---------------------------------------------------------
%
% Objet:
% 
% Realisation de graphiques sur 2 axes
%
% Arguments d'appel:
%
% - taille    = taille de font
%
% - linew     = line width
%
% - abs_g     = Abscisses pour le graphique de gauche
%
% - ord_g     = Ordonnees  pour le graphique de gauche
%
% - abs_d     = Abscisses pour le graphique de droite
%
% - ord_d     = Ordonnees  pour le graphique de droite
%
% - xlim_g    = limites abscisses de gauche
%
% - ylim_g    = limites ordonnees de gauche
%
% - ytick_g   = Tick abs de g 
%
% - xlim_d    = limites abscisses de droite
%
% - ylim_d    = limites ordonnees de droite
%
% - ytick_d   = Tick abs de d
%
% - label_g   = label gauche
%
% - label_d  = label droit 
%
% Exemple d'appel:
% (start code)
% trace_yy(12,2,[tsim tsim],[vit_dem*3.6 vit*3.6],[tsim tsim],[wmt*30/pi wprim_emb1*30/pi],[0 200],[0 60],[0 20 40 60],[0 200],[0 3000],[0 1000 2000 3000],'vitesse km/h','Regime tr/mn')
% (end)
% ---------------------------------------------------------
function [erreur]=trace_yy(taille,linew,abs_g,ord_g,abs_d,ord_d,xlim_g,ylim_g,ytick_g,xlim_d,ylim_d,ytick_d,label_g,label_d)

% initialisation
erreur=0;

figure

[ax,h1,h2]=plotyy(abs_g,ord_g,abs_d,ord_d)
grid on;

% ax(1) is left axis
set(ax(1),'Xlim',xlim_g,'Ylim',ylim_g);
set(ax(1),'YTick',ytick_g,'fontsize',taille);
set(get(ax(1),'Ylabel'),'String',label_g,'fontsize',taille);
set(ax(1),'LineWidth',linew)

set(ax(2),'Xlim',xlim_d,'Ylim',ylim_d);
set(ax(2),'YTick',ytick_d,'fontsize',taille);
set(get(ax(2),'Ylabel'),'String',label_d,'fontsize',taille);
set(ax(2),'LineWidth',linew)

set(h1,'LineWidth',linew)
set(h2,'LineWidth',linew)

%set(gcf,'Position',[256 304 512*85/105*1.2 384*105/85*1.2])
