function gr_iso(type_graph,fig,nb_lig,nb_col,pos,N_hot,C_hot,N_cold,C_cold,shape,VD,param,lang)

% occurences des pt de fonctionnement
mat_hot = [N_hot', C_hot'];
[mat_hot_uniq,~,occurences] = unique(mat_hot,'rows');
plot_hot = [mat_hot_uniq, accumarray(occurences, 1)];

mat_cold = [N_cold', C_cold'];
[mat_cold_uniq,~,occurences] = unique(mat_cold,'rows');
plot_cold = [mat_cold_uniq, accumarray(occurences, 1)];
        
switch type_graph
    case 'chaine_elec'
        %% discretisation en couple et regime pour tracer les iso
        % vecteur de regime
        d_w = 10;
        w_min = min(VD.ACM1.Regmot_pert);
        w_max = max(VD.ACM1.Regmot_pert);
        if mod(w_min,d_w)==0
            wacm1 = w_min:d_w:w_max;
        else
            wacm1 = [w_min ceil(w_min/d_w)*d_w:d_w:w_max];
        end
        if mod(w_max,d_w)~=0
            wacm1 = [wacm1 w_max];
        end 
        sz_w = size(wacm1);
        % vecteur de couple et courbes de couple min/max
        d_C = 5;
        Cacm1_max = interp2(VD.ACM1.Tension_cont,VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot,repmat(param.Ures,sz_w),wacm1);
        Cacm1_min = -Cacm1_max;
        C_max = max(Cacm1_max);
        C_min = -C_max;
        if mod(C_min,d_C)==0
            Cacm1 = C_min:d_C:C_max;
        else
            Cacm1 = [C_min ceil(C_min/d_C)*d_C:d_C:C_max];
        end
        if C_max ~= max(Cacm1_max) %C_max~=0
            Cacm1 = [Cacm1 C_max];
        end
        sz_C = size(Cacm1');
        % grilles de couple/regime pour le calcul des grandeurs
        wacm1_mat = repmat(wacm1,sz_C);
        Cacm1_mat = repmat(Cacm1',sz_w);
        
        %% calcul des grandeurs
        Pacm1 = wacm1_mat.*Cacm1_mat;
        % pertes machine
        Qacm1 = griddata(VD.ACM1.Regmot_pert,VD.ACM1.Cmot_pert,VD.ACM1.Pert_mot',wacm1_mat,Cacm1_mat);
        % pertes batterie
        Pbat = Pacm1 + Qacm1;
        if isfield(VD.BATT,'E')
            % anciens calculs (AGdB)
            E = VD.BATT.E;
            R = VD.BATT.R;
            delta = E^2 - 4*R*Pbat;
            delta(delta<0) = nan;
            Ibat = (E-sqrt(delta))/(2*R);
            Qbat = R*Ibat.^2;
        else
            [~,Ibat,~,~,~,~,R]=calc_batt([],VD.BATT,param.pas_temps,Pbat,100-VD.INIT.Dod0,0,-1,0,param);
            Qbat = R.*Ibat.^2;
        end
        rend = zeros(size(Pacm1));
        dchrg = Cacm1_mat>0 & Pacm1+Qacm1+Qbat~=0;
        chrg = Cacm1_mat<=0 & Pacm1~=0;
        rend(dchrg) = 100*Pacm1(dchrg)./(Pacm1(dchrg)+Qacm1(dchrg)+Qbat(dchrg));
        rend(chrg) = 100*(Pacm1(chrg)+Qacm1(chrg)+Qbat(chrg))./Pacm1(chrg);
        hors_limite = Cacm1_mat>repmat(Cacm1_max,sz_C) | Cacm1_mat<repmat(Cacm1_min,sz_C);
        rend(hors_limite)=nan;
        
        %% trace des iso
        figure(fig)
        subplot(nb_lig,nb_col,pos)
        hold on
        plot(wacm1*30/pi,Cacm1_min,wacm1*30/pi,Cacm1_max)
        [C,h]=contour(wacm1*30/pi,Cacm1,rend,[5:5:80 81:1:100]);
        if ~isempty(plot_hot)
            scatter(plot_hot(:,1),plot_hot(:,2),plot_hot(:,3)*20,'filled',[shape 'r']);
        end
        if ~isempty(plot_cold)
            scatter(plot_cold(:,1),plot_cold(:,2),plot_cold(:,3)*20,'filled',[shape 'b']);
        end
        v = [92 91 90 85 80 75 60 50];
        clabel(C,h,v)
        if isequal(lang,'en')
            ylabel('Torque (Nm)')
            xlabel('Motor speed (rpm)')
            legend('\Gamma_{min}','\Gamma_{max}','\eta (%)','Hot','Cold')
        elseif isequal(lang,'fr')
            ylabel('Couple (Nm)')
            xlabel('R�gime moteur (tr/min)')
            legend('C_{min}','C_{max}','\eta (%)','Chaud','Froid')
        end
            
    case {'m_therm_CSP','m_therm_Texh','m_therm_hin','m_therm_CO','m_therm_HC','m_therm_NO','m_therm_EFF'}
        %% discretisation en couple et regime pour tracer les iso
        % vecteur regime
        d_w = 2;
        w_min = VD.MOTH.ral;
        w_max = VD.MOTH.wmt_maxi;
        if mod(w_min,d_w)==0
            wmt = w_min:d_w:w_max;
        else
            wmt = [w_min ceil(w_min/d_w)*d_w:d_w:w_max];
        end
        if w_max~=0
            wmt = [wmt w_max];
        end
        sz_w = size(wmt);
        % vecteur couple et courbes de couple min/max
        d_C = 1;
        N = wmt*30/pi;
        if VD.MOTH.ntypmoth==4 || VD.MOTH.ntypmoth==5
            Cmt_min = -(VD.MOTH.PMF_cst + VD.MOTH.PMF_N*N + VD.MOTH.PMF_N2*N.^2)*VD.MOTH.Vd/(VD.MOTH.Rev*2*pi);
            % calcul du couple max
            if isfield(param,'phi_cst')
                Phi = param.phi_cst;
            else
                Phi = 1;
            end
            if isfield(param,'delta_AA_cst')
                delta_AA = param.delta_AA_cst;
            else
                delta_AA = 0;
            end
            if isfield(VD.MOTH,'p_intake')
                Padm = interp1(VD.MOTH.p_intake_wmt,VD.MOTH.p_intake,wmt,'linear','extrap');
            else
                Padm = 1000;
            end
            [~, Cmt_max] = calc_mt_3U(wmt',Phi,delta_AA,Padm',1,VD);
            Cmt_max = Cmt_max';
        else
            Cmt_max = (VD.MOTH.Tmax_N2*N.^2 + VD.MOTH.Tmax_N*N + VD.MOTH.Tmax_cst)*VD.MOTH.Tmax/VD.MOTH.Tmax_Tref;
            Cmt_min = -(VD.MOTH.PMF_cst + VD.MOTH.PMF_N*N)*VD.MOTH.Vd/(VD.MOTH.Rev*2*pi);
        end
        C_max = max(Cmt_max);
        C_min = min(Cmt_min);
        if mod(C_min,d_C)==0
            Cmt = C_min:d_C:C_max;
        else
            Cmt = [C_min ceil(C_min/d_C)*d_C:d_C:C_max];
        end
        if C_max~=0
            Cmt = [Cmt C_max];
        end
        sz_C = size(Cmt');
        % matrice couple, regime, PME pour le calcul des grandeurs
        wmt_mat = repmat(wmt,sz_C);
        Cmt_mat = repmat(Cmt',sz_w);
        N_mat = wmt_mat*30/pi;
        PME_mat = VD.MOTH.Rev*2*pi*1e-2*Cmt_mat/VD.MOTH.Vd;
        
        %% calcul des grandeurs associees :
        hors_limite = Cmt_mat>repmat(Cmt_max,sz_C) | Cmt_mat<repmat(Cmt_min,sz_C);
        % - CSP
        if VD.MOTH.ntypmoth==4 || VD.MOTH.ntypmoth==5
            [dcarb, Padm] = calc_mt_3U_reverse(Cmt_mat,N_mat,Phi,delta_AA,VD);
        else
            dcarb = (wmt_mat.*Cmt_mat + wmt_mat.*(VD.MOTH.PMF_cst + VD.MOTH.PMF_N*N_mat)*VD.MOTH.Vd/(VD.MOTH.Rev*2*pi))...
                ./(VD.MOTH.n_fi*(VD.MOTH.n_c0-VD.MOTH.n_cA./(VD.MOTH.n_cB+N_mat))*VD.MOTH.pci);
        end
        if isequal(type_graph,'m_therm_CSP')
            P_mat = wmt_mat.*Cmt_mat/1000; %kW
            CSP = dcarb./P_mat*3600; %g/kWh
            CSP(hors_limite & Cmt_mat>0) = nan;
            CSP(Cmt_mat<0) = inf;
            
            figure(fig)
            subplot(nb_lig,nb_col,pos)
            hold on
            [C,h] = contour(N,Cmt,CSP,[240 243.5 245:5:255 260:10:1000]);
            plot(N,Cmt_min,N,Cmt_max)
            if ~isempty(plot_hot)
                scatter(plot_hot(:,1),plot_hot(:,2),plot_hot(:,3)*20,'filled',[shape 'r']);
            end
            if ~isempty(plot_cold)
                scatter(plot_cold(:,1),plot_cold(:,2),plot_cold(:,3)*20,'filled',[shape 'b']);
            end
            v = [240 243.5 245 250 255 260 270 280 300 350 500 1000];
            clabel(C,h,v)
            if isequal(lang,'en')
                legend('BSFC (g/kWh)','\Gamma_{min}','\Gamma_{max}','Hot','Cold','Location','Southeast')
            elseif isequal(lang,'fr')
                legend('CSE (g/kWh)','C_{min}','C_{max}','Chaud','Froid','Location','Southeast')
            end
        elseif isequal(type_graph,'m_therm_EFF')
            P_mat = wmt_mat.*Cmt_mat/1000; %kW
            
            CSP = dcarb./P_mat*3600; %g/kWh
            CSP(hors_limite & Cmt_mat>0) = nan;
            CSP(Cmt_mat<0) = inf;
            
            EFF = 100 * P_mat*1000 ./ (VD.MOTH.pci * dcarb);
            EFF(hors_limite & Cmt_mat>0) = nan;
            EFF(Cmt_mat<0) = inf;
            
            figure(fig)
            subplot(nb_lig,nb_col,pos)
            hold on
            [C,h] = contour(N,Cmt,EFF,[5 10 15 20 25 30:2:40]);
            plot(N,Cmt_min,N,Cmt_max)
            if ~isempty(plot_hot)
                scatter(plot_hot(:,1),plot_hot(:,2),plot_hot(:,3)*20,'filled',[shape 'r']);
            end
            if ~isempty(plot_cold)
                scatter(plot_cold(:,1),plot_cold(:,2),plot_cold(:,3)*20,'filled',[shape 'b']);
            end
            clabel(C,h)
            if isequal(lang,'en')
                legend('Efficiency (%)','\Gamma_{min}','\Gamma_{max}','Hot','Cold','Location','Southeast')
            elseif isequal(lang,'fr')
                legend('Rendement (%)','C_{min}','C_{max}','Chaud','Froid','Location','Southeast')
            end
        else
            % - HinA_mCp
            if VD.MOTH.ntypmoth==4 || VD.MOTH.ntypmoth==5
                lambdaFG = 1/Phi;
            else
                lambdaFG = VD.MOTH.lambda;
            end
            debitGaz = dcarb.*(1+lambdaFG*VD.MOTH.AStoechio);
            hin = VD.TWC.Hin_coeff(1)*debitGaz + VD.TWC.Hin_coeff(2);
            hin(hin<0)=0;
            hin(hors_limite) = nan;
            if VD.MOTH.ntypmoth==4 || VD.MOTH.ntypmoth==5
                
                % Temperatures ambiante, admission et echappement
                Tamb = VD.INIT.Tamb+273;
                T_adm = Tamb + VD.MOTH.Tadm_N*N_mat + VD.MOTH.Tadm_cst;
                AA_opt = 20;
                n_Phi = VD.MOTH.bPhi + VD.MOTH.aPhi*Phi;
                n_AA = VD.MOTH.bAA + VD.MOTH.aAA*(delta_AA-AA_opt).^2;
                n_N = VD.MOTH.bN + VD.MOTH.aN*N_mat;
                n_Padm = VD.MOTH.bP + VD.MOTH.aP*Padm;
                Texh = T_adm + VD.MOTH.pci*1e3./(VD.MOTH.cp_gaz*(1+lambdaFG*VD.MOTH.AStoechio)).*...
                    n_Phi.*n_AA.*n_N.*n_Padm;
                
                AA_0 = interp2(VD.MOTH.AA_Padm,VD.MOTH.AA_N,VD.MOTH.AA0_N_Padm',Padm,N_mat);
                AA = delta_AA + AA_0;
                % Emissions echappement en ppm ou pourcent
                if VD.MOTH.ntypmoth==4
                    %CO
                    CO_FG_pc = 1e-3*VD.MOTH.CO_c1*(Phi-1+sqrt((Phi-1).^2+VD.MOTH.CO_c2));
                    CO_FG_pc = repmat(CO_FG_pc,size(Padm));
                    %HC
                    f1_HC = VD.MOTH.f1_1_HC + ((Phi-VD.MOTH.f1_H_HC)>0)*(VD.MOTH.f1_2_HC-VD.MOTH.f1_1_HC);
                    f2_HC = VD.MOTH.f2_1_HC*Phi.^2 + VD.MOTH.f2_2_HC*Phi + VD.MOTH.f2_3_HC;
                    f3_HC = VD.MOTH.f3_1_HC*N_mat.^2 + VD.MOTH.f3_2_HC*N_mat + VD.MOTH.f3_3_HC;
                    HC_FG_ppm = f1_HC.*AA + f2_HC + f3_HC - VD.MOTH.HC0;
                    %NO
                    f1_NO = VD.MOTH.f1_1_NO*(1-(1-Phi).*((Phi-VD.MOTH.f1_H_NO)>0))+VD.MOTH.f1_2_NO;
                    f2_NO = VD.MOTH.f2_1_NO*Phi.^2 + VD.MOTH.f2_2_NO*Phi + VD.MOTH.f2_3_NO;
                    f3_NO = VD.MOTH.f3_1_NO*Phi.^2 + VD.MOTH.f3_2_NO*Phi + VD.MOTH.f3_3_NO;
                    NOx_FG_ppm = max(VD.MOTH.NO0 , f1_NO.*AA + f2_NO.*Padm + f3_NO);
                else
                    CO_FG_pc = 1e-4*VD.MOTH.x_CO_1*(Phi - 1 + sqrt((Phi-1).^2 + VD.MOTH.x_CO_2));
                    CO_FG_pc = repmat(CO_FG_pc,size(Padm));
                    HC_FG_ppm = max(VD.MOTH.x_HC_min, VD.MOTH.x_HC_1*N + VD.MOTH.x_HC_2*(Phi-0.9).^2 + VD.MOTH.x_HC_3*AA + VD.MOTH.x_HC_4*Padm + VD.MOTH.x_HC_5);
                    NOx_FG_ppm = max(VD.MOTH.x_NO_min, VD.MOTH.x_NO_1*Padm + VD.MOTH.x_NO_2*(Phi-0.9).^2 + VD.MOTH.x_NO_3*AA + VD.MOTH.x_NO_4);
                end
            else
                % - Texh
                Texh = VD.MOTH.Texh_cst + VD.MOTH.Texh_Pad*N_mat.*PME_mat;
                % - polluants
                CO_FG_pc=VD.MOTH.CO_cst + VD.MOTH.CO_Pad*N_mat.*PME_mat;
                HC_FG_ppm=VD.MOTH.HC_cst + VD.MOTH.HC_Pad*N_mat.*PME_mat;
                NOx_FG_ppm=VD.MOTH.NOx_cst + VD.MOTH.NOx_Pad*N_mat.*PME_mat;
            end
            
            Texh(hors_limite) = nan;
            CO_FG_pc(hors_limite) = nan;
            HC_FG_ppm(hors_limite) = nan;
            NOx_FG_ppm(hors_limite) = nan;
            
            %% trace des iso
            if isequal(type_graph,'m_therm_Texh')
                figure(fig)
                subplot(nb_lig,nb_col,pos)
                hold on
                contour(N,Cmt,Texh,'showtext','on')
                plot(N,Cmt_min,N,Cmt_max)
                if ~isempty(plot_hot)
                    scatter(plot_hot(:,1),plot_hot(:,2),plot_hot(:,3)*20,'filled',[shape 'r']);
                end
                if ~isempty(plot_cold)
                    scatter(plot_cold(:,1),plot_cold(:,2),plot_cold(:,3)*20,'filled',[shape 'b']);
                end
                if isequal(lang,'en')
                    legend('T_{gas} (K)','\Gamma_{min}','\Gamma_{max}','Hot','Cold')
                elseif isequal(lang,'fr')
                    legend('T_{gaz} (K)','C_{min}','C_{max}','Chaud','Froid')
                end
            elseif isequal(type_graph,'m_therm_hin')
                figure(fig)
                subplot(nb_lig,nb_col,pos)
                hold on
                contour(N,Cmt,hin,[0.001 0.025 0.05:0.05:0.25],'showtext','on')
                plot(N,Cmt_min,N,Cmt_max)
                if ~isempty(plot_hot)
                    scatter(plot_hot(:,1),plot_hot(:,2),plot_hot(:,3)*20,'filled',[shape 'r']);
                end
                if ~isempty(plot_cold)
                    scatter(plot_cold(:,1),plot_cold(:,2),plot_cold(:,3)*20,'filled',[shape 'b']);
                end
                if isequal(lang,'en')
                    legend('hA/mC_p','\Gamma_{min}','\Gamma_{max}','Hot','Cold')
                elseif isequal(lang,'fr')
                    legend('hA/mC_p','C_{min}','C_{max}','Chaud','Froid')
                end
            elseif isequal(type_graph,'m_therm_CO')
                figure(fig)
                subplot(nb_lig,nb_col,pos)
                hold on
                contour(N,Cmt,CO_FG_pc,'showtext','on')
                plot(N,Cmt_min,N,Cmt_max)
                if ~isempty(plot_hot)
                    scatter(plot_hot(:,1),plot_hot(:,2),plot_hot(:,3)*20,'filled',[shape 'r']);
                end
                if ~isempty(plot_cold)
                    scatter(plot_cold(:,1),plot_cold(:,2),plot_cold(:,3)*20,'filled',[shape 'b']);
                end
                if isequal(lang,'en')
                    legend('[CO] (%)','\Gamma_{min}','\Gamma_{max}','Hot','Cold')
                elseif isequal(lang,'fr')
                    legend('[CO] (%)','C_{min}','C_{max}','Chaud','Froid')
                end
            elseif isequal(type_graph,'m_therm_HC')
                figure(fig)
                subplot(nb_lig,nb_col,pos)
                hold on
                contour(N,Cmt,HC_FG_ppm,'showtext','on')
                plot(N,Cmt_min,N,Cmt_max)
                if ~isempty(plot_hot)
                    scatter(plot_hot(:,1),plot_hot(:,2),plot_hot(:,3)*20,'filled',[shape 'r']);
                end
                if ~isempty(plot_cold)
                    scatter(plot_cold(:,1),plot_cold(:,2),plot_cold(:,3)*20,'filled',[shape 'b']);
                end
                if isequal(lang,'en')
                    legend('[HC] (ppm)','\Gamma_{min}','\Gamma_{max}','Hot','Cold')
                elseif isequal(lang,'fr')
                    legend('[HC] (ppm)','C_{min}','C_{max}','Chaud','Froid')
                end
            elseif isequal(type_graph,'m_therm_NO')
                figure(fig)
                subplot(nb_lig,nb_col,pos)
                hold on
                contour(N,Cmt,NOx_FG_ppm,'showtext','on')
                plot(N,Cmt_min,N,Cmt_max)
                if ~isempty(plot_hot)
                    scatter(plot_hot(:,1),plot_hot(:,2),plot_hot(:,3)*20,'filled',[shape 'r']);
                end
                if ~isempty(plot_cold)
                    scatter(plot_cold(:,1),plot_cold(:,2),plot_cold(:,3)*20,'filled',[shape 'b']);
                end
                if isequal(lang,'en')
                    legend('[NO] (ppm)','\Gamma_{min}','\Gamma_{max}','Hot','Cold')
                elseif isequal(lang,'fr')
                    legend('[NO] (ppm)','C_{min}','C_{max}','Chaud','Froid')
                end
            end
        end
        %ylim([-47 142]); 
        box on;
        if isequal(lang,'en')
            ylabel('Torque (Nm)')
            xlabel('Engine speed (rpm)')
        elseif isequal(lang,'fr')
            ylabel('Couple (Nm)')
            xlabel('R�gime moteur (tr/min)')
        end
end