function enlarge_figure(figure_handle)
% Enlarge figures in order to show them on the display
% figure_handle is a handle to the figure
% if no argument are given all figures currently opened are treated.
%
% Example
%    x = -pi:pi/10:pi;
%    y = tan(sin(x)) - sin(tan(x));
%    h=figure;plot(x,y);
%    enlarge_figure(h);
%

error(nargchk(0,1,nargin));

if nargin == 0
    h=findobj('type','figure');
else
    try
        if ~strcmp(get(figure_handle,'type'),'figure')
            error('MATLAB:enlarge_figure:Not a handle to figure',...
                'The argument is not a handle to a figure');
        end
    catch
        error('MATLAB:enlarge_figure:Not a handle to a matlab object',...
            'The argument is not a handle to a matlab object');
    end
    h = figure_handle;
end

% Default parameters
param.position=[20 250 1100 550];
param.paperPosition=[1 1 27 19];
param.paperType='A4';
param.paperOrientation='landscape';
param.paperUnits='centimeter';
param.fontSize=16;
param.fontName='Times';
param.lineWidth=2;
param.fontAngle='italic';

for in=1:length(h)
    set(h,'paperType',param.paperType,'paperOrientation',param.paperOrientation,'paperUnits',param.paperUnits,'paperposition',param.paperPosition)
    set(h,'position',param.position)
    grid on;
    a=get(h(in),'children');
    for ain=1:length(a)
        if ~strcmp(get(a(ain),'type'),'axes')
            continue
        end
        set(a(ain),'fontSize',param.fontSize);
        set(a(ain),'fontName',param.fontName);
        set(a(ain),'lineWidth',param.lineWidth);
        set(a(ain),'fontAngle',param.fontAngle);
        xl=get(a(ain),'Xlabel');
        set(xl,'fontSize',param.fontSize);
        yl=get(a(ain),'Ylabel');
        set(yl,'fontSize',param.fontSize);
        l=get(a(ain),'children');
        for jn=1:length(l)
            if ~strcmp(get(l(jn),'type'),'line')
                continue
            end
            set(l(jn),'lineWidth',param.lineWidth);
        end
    end
end

