function fillLineMarker(hl,empty)
% function fillLineMarker(hl,empty)
% Fonction pour remplir ou vider les marqueurs d'un plot.
% hl (nx1) line handles
% empty ([optional] bool):
% si =false (par defaut), comportement normal remplit les marqueurs
% si =true vide les marqueurs ('markerfacecolor','none')
%
% Pour touver les handles des line d'un axes faire ceci:
% hl = findobj('parent',ha,'type','line'), ou ha est un handle d'axes
if ~exist('empty','var')
    empty = false;
end

if empty
 for ind=1:length(hl)
set(hl(ind),'markerfacecolor','none');
end   
else
for ind=1:length(hl)
set(hl(ind),'markerfacecolor',get(hl(ind),'color'));
end
end
end