% Function: gr_vitepi
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function []=gr_vitepi
%
% Trace du diagramme de vitesse du train epi
% ------------------------------------------------------
function []=gr_vitepi

attente=waitbar(0,'Trace vitesses train. Veuillez patienter ...');
set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));

hold off;

%chaine='plot(tsim,wacm1*30/pi,tsim,wacm2*30/pi,tsim,wmt*30/pi);';
chaine='plot(tsim,wacm1*30/pi,tsim,wacm2*30/pi,tsim,wmt*30/pi);';
evalin('base',chaine);
hold on;
grid on;

xlabel('temps en sec');
ylabel('Vitesses train en tr/min');
legend('moteur electrique','machine de variation de vitesse','moteur thermique');
title(strcat('Vitesses du train epicycloidal'));

hold off;
close(attente);
