function changeAxesColor(ha,newColor)
%changeAxesColor change la couleur de fond d'un (ou plusieurs) axes
%
% changeAxesColor(ha,newColor)
% newColor: couleur selon la sepecification MATLAB:
% ex: 'r','g','b'...
% ex2: [1 0 0],[0 1 0], [0 0 1]

% h = findobj('type','axes');
for ind = 1:length(h)
    set(ha(ind),'Color',newColor)
end
end