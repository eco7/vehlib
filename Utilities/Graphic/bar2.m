% Function: bar2
% Trace d'un diagramme a barre
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% Objet :
% Tracees de diagramme a barre
%
% Argumants d'appel:
% t: occurence pour cumul (ce peut etre le temps)
%
% b: variable que l'on veut classer
%
% nbinter: nbre intervalles de classe
%
% --------------------------
function []= bar2(t,b,nbinter)


clear Inter Bbar

attente=waitbar(0,'Trace d un diagramme a barres. Veuillez patienter ...');

% Reconstruction du pas de temps.
t2=t(2:length(t));
pas=t2-t(1:length(t)-1);
pas=[0; pas];

% nb inter: nbre intervalles entre mini et maxi
Min= min(b);
Max= max(b);
r=fix(Max-Min);
Vinter= fix(Max-Min)./nbinter;
Inter=Min:Vinter:Max;

B2bar=zeros(size(Inter));
for j=1:length(Inter)-1
    B2bar(j)=sum(pas(find(b>=Inter(j) & b<Inter(j+1))));
end
B2bar(length(Inter))=sum(pas(find(b>=Inter(length(Inter)))));
set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));
bar(Inter,B2bar);

waitbar(1.);
close(attente);
