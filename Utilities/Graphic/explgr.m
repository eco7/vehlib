% Function: [explgr_struct]=explgr(input_struct)
%   Script d'exploitation graphique des resultats de simulation avec la bibliotheque VEHLIB.
%
%  C Copyright Univ. G Eiffel Eco7 1999-2022
%
%
% Objet:
%
%   Script d'exploitation graphique des resultats de simulation avec la bibliotheque VEHLIB.
%
%   explgr permet egalement de lire un fichier externe
%
%   Deux methodes d appel :
%   - Sans arguments: ouvre un selecteur de fichier
%   - la structure vehlib pour exploiter une simulation avec la
%   bibliotheque
%
% Arguments d'appel:
%
% input_struct :
%  - structure vehlib renseignant les fichiers de donnees
%  d'une simulation avec la bibliotheque VEHLIB.
% ou
% - structure xml pour proposer le gui de traces graphiques
%
% Cette structure est partagee par tous les sous programmes de explgr sans passer en argument
% d'appel de ces sous-programmes (Variables within nested functions are accessible
% to more than just their immediate function).
%
% Types de fichiers graphiques pris en charge:
%
%       - *.xml : fichier au format xml
%       - *.mat : matfile - Pour compatibilite avec DSpace,
%       les formats pris en charge sont issus du CrontolDesk Expt
%       - autres extensions supposees etre des fichiers ASCII avec le formatage suivant:
%
%       Un entete libre avant la variable #DEBUT
%
%       Une liste de ncol variables sur 1 ligne, separateur espace ou Tab
%
%       Une liste de ncol unite sur 1 ligne, separateur espace ou Tab
%
%       Une matrice a la taille nbligne*ncol (NaN, Inf autorises)
%       - *.m : M-File - A priori, un fichier lisible par matlab, toutes
%       les variables du fichier sont disponibles (OBSOLETE)
%
%
%   (see explgr.png)
%
% ---------------------------------------------------------
function [explgr_struct]=explgr(input_struct)

if nargin~=0
    % on teste si l'argument d'entree est une structure qui repond au format xml
    [~, err] = verifFomatXML4Vehlib(input_struct);
    if err < 0
        %la structure n est pas compatible xml
        if isstruct(input_struct) && isfield(input_struct,'simulink')
            % c'est a priori un modele simulink qui appel explgr
            explgr_struct=miseEnForme4VEHLIB(input_struct,'base');
            explgr_struct_int.vehlib=input_struct;
        else
            %Construct an MException object to represent the error.
            err = MException('explgr:ArgNotCompatible', ...
                'Input Argument is not compatible');
            %Throw the exception to stop execution and display an error message.
            throw(err)
        end
    else
        % la structure est au format xml
        explgr_struct=input_struct;
        explgr_struct_int.typeTrace=struct([]);
    end
else
    explgr_struct=struct([]);
    % Lecture d'un fichier externe
    lectureFile_fcn;
    explgr_struct_int.typeTrace=struct([]);
    if isempty(explgr_struct)
        % L'utilisateur a annule la lecture d'un fichier externe
        return;
    end
end

% Constitution de la liste des traces proposes
[explgr_struct_int]=explgrlistepopupmenu(explgr_struct_int);

if ~isempty(findobj('Tag','TagFigExplgr'))
    close(findobj('Tag','TagFigExplgr'))
end
if ~isempty(findobj('Tag','TagFigTrace'))
    close(findobj('Tag','TagFigTrace'))
end

figurePos = [10 50 1050 260];
figureAxePos = [10 340 1050 500];
f=figurePos+figureAxePos;
defaultscreensize=[1400 1050];
[pixfactor,Taille_car,f]=gui_taille(f,defaultscreensize);

btnHt = 40;
figurePos = figurePos.*pixfactor;
figureAxePos = figureAxePos.*pixfactor;

traceBtnPos = [180 10 100 btnHt].* pixfactor;
fermerBtnPos = [500 10 100 btnHt].* pixfactor;

listAbsPos = [20 60 350 150].* pixfactor;
txtAbsPos=[20 220 350 btnHt/2].* pixfactor;
listOrdPos = [400 60 350 150].* pixfactor;
txtOrdPos=[400 220 350 btnHt/2].* pixfactor;

frameOptionPos= [760 60 220 220+btnHt./2-60].* pixfactor;

txtTracePos=[770 210 200 btnHt/2].* pixfactor;
ListTracePos = [770 190 200 btnHt./2].* pixfactor;

RdioBtnhonPos= [770 160 btnHt btnHt./2].* pixfactor;
txtHonPos=[800 160 170 btnHt./2].* pixfactor;
RdioBtnadimPos= [770 140 btnHt btnHt./2].* pixfactor;
txtAdimPos=[800 140 170 btnHt./2].* pixfactor;
RdioBtnlegPos= [770 120 btnHt btnHt./2].* pixfactor;
txtlegPos=[800 120 170 btnHt./2].* pixfactor;

subplotBtnPos = [940 90 btnHt./2 btnHt./2].* pixfactor;
subplotTxtnPos = [770 90 170 btnHt./2].* pixfactor;

bancBtnPos=[800 10 100 btnHt].* pixfactor;

if evalin('base','exist(''VD'',''var'')') && evalin('base','isstruct(VD)') && evalin('base','isfield(VD,''INIT'')')
    fig_Name=['Exploitation graphique des simulations de VEHLIB, Version : ',evalin('base','VD.INIT.version')];
else
    fig_Name='Exploitation graphique des simulations de VEHLIB';
end

FigH = figure('Color',[0.5 0.5 0.75], ...
    'Units','pixels', ...
    'MenuBar','none', ...
    'Name',fig_Name, ...
    'NumberTitle','off', ...
    'Resize','off', ...
    'Position',figurePos, ...
    'Tag','TagFigExplgr');

% les boutons
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Callback','', ...
    'Enable','on', ...
    'Position',traceBtnPos, ...
    'String','Tracer', ...
    'FontSize',Taille_car, ...
    'Callback',@trace_fcn, ...
    'Tag','TagPushTrace');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Callback','', ...
    'Enable','on', ...
    'Position',fermerBtnPos, ...
    'String','Fermer', ...
    'FontSize',Taille_car, ...
    'Callback',@close_fcn, ...
    'Tag','TagPushTraceFerme');


% Liste d affichage des entetes des variables
[entete,cumulLongueurT]=listeEntete(explgr_struct);


uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',listAbsPos, ...
    'String',entete,...
    'Style','listbox', ...
    'FontSize',Taille_car, ...
    'Value',3, ...
    'Tag','tagListExplgrAbs');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Position',txtAbsPos, ...
    'String','Selection de l''axe des abscisses :', ...
    'FontSize',Taille_car, ...
    'Style','text', ...
    'Tag','tagTxtAbs');

% la liste d affichage de variables ordonne
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',listOrdPos, ...
    'String',entete,...
    'Style','listbox', ...
    'FontSize',Taille_car, ...
    'Value',4, ...
    'Tag','tagListExplgrOrd');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Position',txtOrdPos, ...
    'String','Selection de l''axe des ordonnees :', ...
    'FontSize',Taille_car, ...
    'Style','text', ...
    'Tag','tagTxtOrd');

% Frame pour option graphiques
uicontrol('Parent',FigH, ...
    'BackgroundColor',[0.5 0.5 0.5], ...
    'Units','pixels', ...
    'Position',frameOptionPos, ...
    'Style','frame', ...
    'Tag','FrameExplgr');

% Selection des types de traces disponibles
entete=cell(size(explgr_struct_int.typeTrace));
for ind2=1:length(explgr_struct_int.typeTrace)
    entete(ind2)=explgr_struct_int.typeTrace(ind2).entete;
end

uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Callback','', ...
    'Enable','on', ...
    'Position',ListTracePos, ...
    'Style','Popupmenu', ...
    'String',entete, ...
    'FontSize',Taille_car, ...
    'Callback',@plotTypeSelection_fcn, ...
    'Tag','TagPopselectTrace');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Position',txtTracePos,...
    'String','Choix du type de trace :', ...
    'Style','text', ...
    'FontSize',Taille_car, ...
    'Tag','tagTxtAbs');

% Radio bouton de superposition des courbes
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'BackgroundColor',[0.75 0.75 0.75], ...
    'Callback','set(0,''CurrentFigure'',findobj(''Tag'',''TagFigTrace''));', ...
    'Position',RdioBtnhonPos, ...
    'Style','radiobutton', ...
    'Tag','tagRdiohon');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Position',txtHonPos, ...
    'String','Superposer', ...
    'FontSize',Taille_car, ...
    'Style','text', ...
    'Tag','tagTxtHon');

%radio bouton d adimensionnement des courbes
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'BackgroundColor',[0.75 0.75 0.75], ...
    'Callback','set(0,''CurrentFigure'',findobj(''Tag'',''TagFigTrace''));', ...
    'Position',RdioBtnadimPos, ...
    'Style','radiobutton', ...
    'Tag','tagRdioadim');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Position',txtAdimPos, ...
    'String','Adimensionner', ...
    'FontSize',Taille_car, ...
    'Style','text', ...
    'Tag','tagTxtAdim');

%radio bouton de suppression des legendes
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'BackgroundColor',[0.75 0.75 0.75], ...
    'Callback','set(0,''CurrentFigure'',findobj(''Tag'',''TagFigTrace''));', ...
    'Position',RdioBtnlegPos, ...
    'Style','radiobutton', ...
    'Tag','tagRdioleg');
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Position',txtlegPos, ...
    'String','Supp. legende', ...
    'Style','text', ...
    'FontSize',Taille_car, ...
    'Tag','tagTxtleg');

uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Position',subplotBtnPos, ...
    'Style','edit', ...
    'String','1', ...
    'Tag','tagSubplot', ...
    'Callback',@subplot_fcn);
uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'ListboxTop',0, ...
    'Position',subplotTxtnPos,...
    'String','Nombre de sous graphiques :', ...
    'Style','text', ...
    'FontSize',Taille_car, ...
    'Tag','tagTxtSubplot');

uicontrol('Parent',FigH, ...
    'Units','pixels', ...
    'Callback','', ...
    'Enable','on', ...
    'Position',bancBtnPos, ...
    'String','Lecture d''un fichier', ...
    'Callback',@lectureFile_fcn, ...
    'FontSize',Taille_car, ...
    'Tag','TagBtnLecture');

% la fenetre graphique
figure( ...
    'Name','Graphiques VEHLIB', ...
    'NumberTitle','off', ...
    'Position',figureAxePos, ...
    'Visible','Off', ...
    'Tag','TagFigTrace');


% Necesaire pour rafraichir la structure de donnee explgr_struct en
% sortant de la fonction.
% waitfor(findobj('tag','TagFigExplgr'));

% Function: trace_fcn
% fonction qui realise les plot pour explgr
% ----------------------------------------------------------------------
    function trace_fcn(~,~)
        global jcourb leg
        
        % recherche de l'objet
        hToPopselectTrace=findobj('Tag','TagPopselectTrace','Type','uicontrol');
        
        %recherche de la valeur selectionne
        VToPopselectTrace=get(hToPopselectTrace,'Value');
        
        if VToPopselectTrace==1 || VToPopselectTrace==2
            %%%%%%% TRACE EN Y(X) OU DIAGRAMME A BARRE %%%%%%%%%%
            % recherche de l'entete dans la liste des abscisses
            hToListExplgrAbs=findobj('Tag','tagListExplgrAbs','Type','uicontrol');
            % recherche de la valeur de l'entete selectionne
            valeur1=get(hToListExplgrAbs,'Value');
            % on recherche la table et le nom de la variable correspondant a valeur1
            numT1=find(cumulLongueurT<valeur1,1,'last');
            nomV1=fieldnames(explgr_struct.table{numT1});
            nomV1=nomV1{valeur1-cumulLongueurT(numT1)};
            
            if VToPopselectTrace==1
                % recherche de l'entete dans la liste des ordonnees
                hToListExplgrOrd=findobj('Tag','tagListExplgrOrd','Type','uicontrol');
                % recherche de la valeur de l'entete selectionne
                valeur2=get(hToListExplgrOrd,'Value');
                % on recherche la table et le nom de la variable correspondant a valeur1
                numT2=find(cumulLongueurT<valeur2,1,'last');
                nomV2=fieldnames(explgr_struct.table{numT2});
                nomV2=nomV2{valeur2-cumulLongueurT(numT2)};
            else
                % on ne superpose pas les courbes pour un diagramme a barre
                jcourb=96;
                numT2=1;
                nomV2=fieldnames(explgr_struct.table{numT2});
                nomV2=nomV2{3}; % le temps;
                % !!!!! on suppose que la premiere variable de la table{1} est toujours le temps !!!!
            end
            
            if eval(['length(explgr_struct.table{',num2str(numT1),'}.',nomV1,'.vector)==length(explgr_struct.table{',num2str(numT2),'}.',nomV2,'.vector)'])
                if(isempty(findobj('Tag','TagFigTrace','Type','figure')))
                    figure( ...
                        'Name','Graphiques VEHLIB', ...
                        'NumberTitle','off', ...
                        'Position',figureAxePos, ...
                        'Visible','Off', ...
                        'Tag','TagFigTrace');
                end
                
                set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));
                
                if(~get(findobj('Tag','tagRdiohon'),'Value'))
                    % on ne superpose pas les courbes
                    jcourb=96;
                end
                
                if ~strcmp(nomV1,'id') &&  ~strcmp(nomV2,'id') && ~strcmp(nomV1,'metatable') &&  ~strcmp(nomV2,'metatable')
                    % Sinon c'est un entete.
                    jcourb=jcourb+1;
                    if eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''unit'')'])
                        [facteur, offset, newUnit_abs]=unifyUnit(explgr_struct.table{numT1}.(nomV1).unit);
                    else
                        newUnit_abs='';
                        facteur=1;
                        offset=0;
                    end
                    chaineVectorV1=['explgr_struct.table{',num2str(numT1),'}.',nomV1,'.vector*',num2str(facteur),'+',num2str(offset),';'];
                    assignin('base','abss',eval(chaineVectorV1));
                    evalin('base',strcat('absc.',char(jcourb),'=abss;'));
                    
                    if eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''unit'')'])
                        [facteur, offset, newUnit_ord]=unifyUnit(explgr_struct.table{numT2}.(nomV2).unit);
                    else
                        newUnit_ord='';
                        facteur=1;
                        offset=0;
                    end
                    chaineVectorV2=['explgr_struct.table{',num2str(numT2),'}.',nomV2,'.vector*',num2str(facteur),'+',num2str(offset),';'];
                    assignin('base','ordd',eval(chaineVectorV2));
                    evalin('base',strcat('ord.',char(jcourb),'=ordd;'));
                    
                    % Le label/legende en X
                    if eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''longname'')']) && eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''unit'')'])
                        leg_abs=[eval(['explgr_struct.table{',num2str(numT1),'}.' nomV1 '.longname ']),' (',newUnit_abs,')'];
                    elseif eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''unit'')'])
                        leg_abs=[eval(['explgr_struct.table{',num2str(numT1),'}.' nomV1 '.name ']),' (',newUnit_abs,')'];
                    elseif eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''longname'')'])
                        leg_abs=eval(['explgr_struct.table{',num2str(numT1),'}.' nomV1 '.longname ']);
                    else
                        leg_abs=eval(['explgr_struct.table{',num2str(numT1),'}.' nomV1 '.name ']);
                    end
                    
                    
                    % Le label/legende en Y
                    if eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''longname'')']) && eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''unit'')'])
                        leg_ord=[eval(['explgr_struct.table{',num2str(numT2),'}.' nomV2 '.longname ']),' (',newUnit_ord,')'];
                    elseif eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''unit'')'])
                        leg_ord=[eval(['explgr_struct.table{',num2str(numT2),'}.' nomV2 '.name ']),' (',newUnit_ord,')'];
                    elseif eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''longname'')'])
                        leg_ord=eval(['explgr_struct.table{',num2str(numT2),'}.' nomV2 '.longname ']);
                    else
                        leg_ord=eval(['explgr_struct.table{',num2str(numT2),'}.' nomV2 '.name ']);
                    end
                    
                    leg{jcourb-96}=[deblank(strrep(leg_abs,'_','\_')) ' - ' deblank(strrep(leg_ord,'_','\_')) ];
                    % on stocke les valeurs maxi pour eventuellement adimensionner les courbes
                    
                    evalin('base',strcat('adim_absc(',num2str(jcourb),'-96)=1;'));
                    evalin('base',strcat('adim_ord(',num2str(jcourb),'-96)=max(abs(ord.',char(jcourb),'));'));
                    
                    if VToPopselectTrace==1
                        %%%%%%% TRACE EN Y(X) %%%%%%%%%%
                        if(get(findobj('Tag','tagRdioadim'),'Value')==1)
                            % on adimensionne les courbes
                            str=strcat('absc.',char(97),'/adim_absc(',num2str(1),')',',','ord.',char(97),'/adim_ord(',num2str(1),')');
                            for j=98:jcourb
                                str=strcat(str,',absc.',char(j),'/adim_absc(',num2str(j),'-96)',',','ord.',char(j),'/adim_ord(',num2str(j),'-96)');
                            end
                        else
                            str=strcat('absc.',char(97),',','ord.',char(97));
                            for j=98:jcourb
                                str=strcat(str,',absc.',char(j),',','ord.',char(j));
                            end
                        end
                        
                        
                        strace=strcat('plot(',str,')');
                        evalin('base',strace);
                        
                        grid on;
                        % Creation d un titre
                        if isfield(explgr_struct,'vehlib')
                            % pour les simulations avec vehlib
                            if  isfield(explgr_struct.vehlib,'simulink');
                                S=load('numcal');
                                titre=['Modele simulink: ',strrep(explgr_struct.vehlib.simulink,'_',' '),' - Modele vehicule: ',strrep(explgr_struct.vehlib.nom,'_',' '),' - No de calcul: ',num2str(S.numcal)];
                                title(titre,'Interpreter','none');
                            end
                        else
                            if isfield(explgr_struct.table{numT1}.metatable,'sourcefile') && ~isempty(explgr_struct.table{numT1}.metatable.sourcefile)
                                title(deblank(strrep(explgr_struct.table{numT1}.metatable.sourcefile,'_',' ')),'Interpreter','none')
                            else
                                title(deblank(strrep(explgr_struct.table{numT1}.metatable.name,'_',' ')),'Interpreter','none')
                            end
                        end
                        
                        if(get(findobj('Tag','tagRdioleg'),'Value')==0)
                            if(jcourb==97)
                                % On met des labels sur les axes                             
                                if eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''longname'')']) && eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''unit'')'])
                                    xlabel([eval(['explgr_struct.table{',num2str(numT1),'}.' nomV1 '.longname ']),' (',newUnit_abs,')'])
                                elseif eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''unit'')'])
                                    xlabel([eval(['explgr_struct.table{',num2str(numT1),'}.' nomV1 '.name ']),' (',newUnit_abs,')'])
                                elseif eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''longname'')'])
                                    xlabel(eval(['explgr_struct.table{',num2str(numT1),'}.' nomV1 '.longname ']))
                                else
                                    xlabel(eval(['explgr_struct.table{',num2str(numT1),'}.' nomV1 '.name ']))
                                end
                                
                                if eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''longname'')']) && eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''unit'')'])
                                    ylabel([eval(['explgr_struct.table{',num2str(numT2),'}.' nomV2 '.longname ']),' (',newUnit_ord,')'])
                                elseif eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''unit'')'])
                                    ylabel([eval(['explgr_struct.table{',num2str(numT2),'}.' nomV2 '.name ']),' (',newUnit_ord,')'])
                                elseif eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''longname'')'])
                                    ylabel(eval(['explgr_struct.table{',num2str(numT2),'}.' nomV2 '.longname ']))
                                else
                                    ylabel(eval(['explgr_struct.table{',num2str(numT2),'}.' nomV2 '.name ']))
                                end
                            else
                                %on met une legende
                                legende=['''' leg{1}];
                                for j=2:jcourb-96
                                    legende=[legende,''',''',leg{j} ];
                                end
                                legende=[legende,'''' ];
                                eval(['legend(' legende ')' ]);
                            end
                        else
                            evalin('base','legend off');
                        end
                    elseif VToPopselectTrace==2
                        %%%%%%% TRACE D UN DIAGRAMME A BARRES %%%%%%%%%%
                        set(findobj('Tag','tagRdiohon'),'Value',0);
                        nbinter=30;
                        str=strcat('ord.',char(97),',','absc.',char(97),',',num2str(nbinter));
                        strace=strcat('bar2(',str,')');
                        evalin('base',strace);
                        set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));
                        
                        % On met des labels sur les axes
                        if eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''longname'')']) && eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''unit'')'])
                            xlabel([eval(['explgr_struct.table{',num2str(numT1),'}.' nomV1 '.longname ']),' (',newUnit_abs,')'])
                        elseif eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''unit'')'])
                            xlabel([eval(['explgr_struct.table{',num2str(numT1),'}.' nomV1 '.name ']),' (',newUnit_abs,')'])
                        elseif eval(['isfield(explgr_struct.table{',num2str(numT1),'}.' nomV1 ',''longname'')'])
                            xlabel(eval(['explgr_struct.table{',num2str(numT1),'}.' nomV1 '.longname ']))
                        else
                            xlabel(eval(['explgr_struct.table{',num2str(numT1),'}.' nomV1 '.name ']))
                        end
                        
                        if eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''longname'')']) && eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''unit'')'])
                            ylabel([eval(['explgr_struct.table{',num2str(numT2),'}.' nomV2 '.longname ']),' (',newUnit_ord,')'])
                        elseif eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''unit'')'])
                            ylabel([eval(['explgr_struct.table{',num2str(numT2),'}.' nomV2 '.name ']),' (',newUnit_ord,')'])
                        elseif eval(['isfield(explgr_struct.table{',num2str(numT2),'}.' nomV2 ',''longname'')'])
                            ylabel(eval(['explgr_struct.table{',num2str(numT2),'}.' nomV2 '.longname ']))
                        else
                            ylabel(eval(['explgr_struct.table{',num2str(numT2),'}.' nomV2 '.name ']))
                        end
                            
                        
                    end
                    set(findobj('Tag','TagFigTrace'),'Visible','on');
                end
            else
                warning('explgr_xml:trace_fcn','abcisse et ordonnee n''ont pas la meme longueur')
            end
        else
            %%%%%%% TRACE AUTOMATIQUE %%%%%%%%%%
            if(isempty(findobj('Tag','TagFigTrace','Type','figure')))
                figure( ...
                    'Name','Graphiques VEHLIB', ...
                    'NumberTitle','off', ...
                    'Position',figureAxePos, ...
                    'Visible','Off', ...
                    'Tag','TagFigTrace');
            end
            set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));
            evalin('base',char(explgr_struct_int.typeTrace(VToPopselectTrace).scriptName));
            set(findobj('Tag','TagFigTrace'),'Visible','on');
        end
    end

% Function: plotTypeSelection_fcn
% Fonction de validation/invalidation des choix dans l'interface en
% fonction du type de trace selectionne
% ----------------------------------------------------------------------
    function plotTypeSelection_fcn(hObject,~)
        
        %recherche de la valeur selectionne
        value=get(hObject,'Value');
        
        if value==1   % Trace en y(x)
            set(findobj('tag','tagListExplgrAbs'),'enable','on');
            set(findobj('tag','tagListExplgrOrd'),'enable','on');
            set(findobj('tag','tagRdiohon'),'enable','on');
            set(findobj('tag','tagRdioadim'),'enable','on');
            set(findobj('tag','tagRdioleg'),'enable','on');
        elseif value==2  % Trace diag a barres en fonction du temps
            set(findobj('tag','tagListExplgrAbs'),'enable','on');
            set(findobj('tag','tagListExplgrOrd'),'enable','off');
            set(findobj('tag','tagRdiohon'),'enable','off');
            set(findobj('tag','tagRdioadim'),'enable','off');
            set(findobj('tag','tagRdioleg'),'enable','off');
        elseif value> 2   % Traces automatiques
            set(findobj('tag','tagListExplgrAbs'),'enable','off');
            set(findobj('tag','tagListExplgrOrd'),'enable','off');
            set(findobj('tag','tagRdiohon'),'enable','off');
            set(findobj('tag','tagRdioadim'),'enable','off');
            set(findobj('tag','tagRdioleg'),'enable','off');
        end
        
    end

% Function: lectureFile_fcn
% Fonction pour lire des fichiers de donnees depuis le gui de explgr
% ----------------------------------------------------------------------
    function lectureFile_fcn(hObject,~)
        
        %         % la fonction est rappelee depuis le gui apres une simulation vehsim
        %         if exist('explgr_struct','var')
        %             explgr_struct=rmfield(explgr_struct,'typeTrace');
        %             explgr_struct=rmfield(explgr_struct,'vehlib');
        %         end
        
        
        % Selection d'un fichier
        [fichier,chemin]=uigetfile('*.*','Choix d''un fichier de donnees');
        if fichier==0
            return
        end
        if exist('hObject','var')
            set(findobj('Tag','TagFigTrace'),'Visible','off');
            set(findobj('Tag','TagFigExplgr'),'Visible','off');
        end
        h=waitbar(0,'Lecture du fichier - Veuillez patienter');
        
        %         if isfield(explgr_struct,'file')
        %             lengthstruct=length(explgr_struct.file)+1;
        %         else
        %             lengthstruct=1;
        %         end
        
        if ~isempty(strfind(fichier,'.mat'))
            % foramt mat-file
            % Read the mat-file and instanciate the structure
            param.FileName=fichier;
            param.PathName=chemin;
            if isempty(explgr_struct)
                explgr_struct=lectureMatFile4Vehlib(param);
            else
                explgr_struct=lectureMatFile4Vehlib(param,explgr_struct);
            end
            
            waitbar(1,h);
        elseif ~isempty(strfind(fichier,'.xml'))
            % Format xml
            % Read the xml-file and instanciate the structure
            if isempty(explgr_struct)
                explgr_struct=lectureXMLFile4Vehlib(fichier,chemin);
            else
                explgr_struct=lectureXMLFile4Vehlib(fichier,chemin,explgr_struct);
            end
            waitbar(1,h);
        elseif ~isempty(strfind(fichier,'.m'))
            % Lecture d'un M-file.
            %Construct an MException object to represent the error.
            err = MException('explgr:NotImplemented', ...
                'This option is not still available');
            %Throw the exception to stop execution and display an error message.
            throw(err)
            
            %[retour,nom,var,unite,archit]=lecture_mfile(fichier,chemin,archit);
            %waitbar(1,h);
        else
            % Lecture d un fichier ascii avec mise en forme (#DEBUT et banniere)
            % Lecture du fichier
            param.FileName=fichier;
            param.PathName=chemin;
            if isempty(explgr_struct)
                explgr_struct=lectureAscii4Vehlib(param);
            else
                explgr_struct=lectureAscii4Vehlib(param,explgr_struct);
            end
            waitbar(1,h);
        end
        close(h);
        
        if exist('hObject','var')
            if hObject==findobj('Tag','TagBtnLecture')
                % Si c'est un callback du bouton, il faut mettre a jour la liste d affichage des entetes des variables
                [entete,cumulLongueurT]=listeEntete(explgr_struct);
                
                set(findobj('Tag','tagListExplgrAbs'),'string',entete);
                set(findobj('Tag','tagListExplgrOrd'),'string',entete);
            end
            set(findobj('Tag','TagFigExplgr'),'Visible','on');
            if(~isempty(findobj('Tag','TagFigTrace','Type','figure')))
                set(findobj('Tag','TagFigTrace'),'Visible','on');
            end
        end
    end

% Function: subplot_fcn
% Fonction pour realiser des subplot dans la fenetre graphique
% ----------------------------------------------------------------------
    function subplot_fcn(hObject,~)
        
        val=str2double(get(hObject,'String'));
        set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));
        for i=val:-1:1
            ax(i)=subplot(val,1,i);
        end
        % link x axes for zooming
        linkaxes(ax, 'x')
    end

% Function: close_fcn
% Fonction pour fermer explgr
% ----------------------------------------------------------------------
    function close_fcn(~,~)
        
        close(gcbf);
        close(findobj('Tag','TagFigTrace'));
    end

% Function: explgrlistepopupmenu
% fonction de selection des types de tracees proposes par explgr
%
%  C Copyright IFSTTAR LTE 1999-2011
% ----------------------------------------------------------------------
    function [explgr_struct_int]=explgrlistepopupmenu(explgr_struct_int)
        
        if ~isfield(explgr_struct_int,'typeTrace')
            explgr_struct_int.typeTrace=struct([]);
        end
        
        n=1;
        explgr_struct_int.typeTrace(n).entete={'Trace en Y(X)';};
        explgr_struct_int.typeTrace(n).scriptName={'';};
        
        n=n+1;
        explgr_struct_int.typeTrace(n).entete={'Trace d un diagramme a barre';};
        explgr_struct_int.typeTrace(n).scriptName={'bar2';};
        
        if isfield(explgr_struct_int,'vehlib') && isfield(explgr_struct_int.vehlib,'ACM1')
            n=n+1;
            explgr_struct_int.typeTrace(n).entete={'Fonct moteur electrique';};
            explgr_struct_int.typeTrace(n).scriptName={'gr_melec(VD.ACM1,wacm1,cacm1)';};
        end
        
        if isfield(explgr_struct_int,'vehlib') && isfield(explgr_struct_int.vehlib,'ACM2')
            n=n+1;
            explgr_struct_int.typeTrace(n).entete={'Fonct generateur electrique';};
            explgr_struct_int.typeTrace(n).scriptName={'gr_melec(VD.ACM2,wacm2,cacm2)';};
        end
        
        if isfield(explgr_struct_int,'vehlib') && isfield(explgr_struct_int.vehlib,'MOTH')
            n=n+1;
            explgr_struct_int.typeTrace(n).entete={'Fonct moteur thermique MT';};
            nom=strcat('gr_mtherm(''',explgr_struct_int.vehlib.architecture,''',VD.MOTH,wmt,cmt);');
            explgr_struct_int.typeTrace(n).scriptName={nom};
        end
        
        if isfield(explgr_struct_int,'vehlib') && isfield(explgr_struct_int.vehlib,'architecture')
            if strcmpi(explgr_struct_int.vehlib.architecture,'HPDP_EPI')||strcmpi(explgr_struct_int.vehlib.architecture,'HPDP_EPI_SURV')
                n=n+1;
                explgr_struct_int.typeTrace(n).entete={'Diagramme des vitesses du train epicycloidal';};
                nom=strcat('gr_vitepi;');
                explgr_struct_int.typeTrace(n).scriptName={nom};
            end
        end
        
        if isfield(explgr_struct_int,'vehlib') && isfield(explgr_struct_int.vehlib,'architecture')
            if strcmpi(explgr_struct_int.vehlib.architecture,'VELEC')||strcmpi(explgr_struct_int.vehlib.architecture,'VTH_BV')
                n=n+1;
                explgr_struct_int.typeTrace(n).entete={'Power Balance';};
                if(strcmpi(explgr_struct_int.vehlib.architecture,'VELEC'))
                    nom=strcat('gr_powloss(''',explgr_struct_int.vehlib.architecture,''',0,pertes_bat,cacm1,wacm1,ibat,ubat,iacc,uacc,iacm1,uacm1,cprim_red,wprim_red,csec_red,wsec_red,croue,wroue);');
                    explgr_struct_int.typeTrace(n).scriptName={nom};
                elseif strcmpi(explgr_struct_int.vehlib.architecture,'VTH_BV')
                    nom=strcat('gr_powloss(''',explgr_struct_int.vehlib.architecture,''',VD.MOTH,cons_mt,cmt,wmt,caccm1,caccm2,cprim_emb1,wprim_emb1,csec_emb1,wsec_emb1,csec_bv,wsec_bv,csec_red,wsec_red,croue,wroue);');
                    explgr_struct_int.typeTrace(n).scriptName={nom};
                end
            end
        end
        
    end

% Function: listeEntete
% fonction pour realiser la liste des entetes des variables explgr
% ----------------------------------------------------------------------
    function [entete,cumulLongueurT]=listeEntete(explgr_struct)
        entete=cell(0,1);
        longueurT=zeros(size(explgr_struct.table));
        for indT=1:length(explgr_struct.table)
            nom=fieldnames(explgr_struct.table{indT});
            longueurT(indT)=length(nom);
            cumulLongueurT=[0 cumsum(longueurT)];
            entete=[entete;cell(length(nom),1)];
            entete{cumulLongueurT(indT)+1}='';
            entete{cumulLongueurT(indT)+2}=explgr_struct.table{indT}.metatable.name;
            for indd=3:length(nom)
                ind=cumulLongueurT(indT)+indd;
                if eval(['isfield(explgr_struct.table{',num2str(indT),'}.' nom{indd} ',''unit'')'])
                [~, ~, newUnit] = unifyUnit(explgr_struct.table{indT}.(nom{indd}).unit);
                end
                if eval(['isfield(explgr_struct.table{',num2str(indT),'}.' nom{indd} ',''longname'')']) && eval(['isfield(explgr_struct.table{',num2str(indT),'}.' nom{indd} ',''unit'')'])
                    entete{ind}=[eval(['explgr_struct.table{',num2str(indT),'}.' nom{indd} '.longname ']),' (',newUnit,')'];
                elseif eval(['isfield(explgr_struct.table{',num2str(indT),'}.' nom{indd} ',''longname'')'])
                    entete{ind}=eval(['explgr_struct.table{',num2str(indT),'}.' nom{indd} '.longname ']);
                elseif eval(['isfield(explgr_struct.table{',num2str(indT),'}.' nom{indd} ',''unit'')'])
                    entete{ind}=[eval(['explgr_struct.table{',num2str(indT),'}.' nom{indd} '.name ']),' (',newUnit,')'];
                else
                    entete{ind}=eval(['explgr_struct.table{',num2str(indT),'}.' nom{indd} '.name ']);
                end
                
            end
        end
    end


    function [facteur, offset, newUnit] = unifyUnit(unit)
        facteur=1;
        offset=0;
        newUnit=unit;
        % Temperature
        if strcmp(unit,'K') | strcmp(unit,'_K') | strcmp(unit,'deg K')
            newUnit = 'deg C*';
            offset=-273.15;
        end
        % Rotation speed
        if strcmp(unit,'rd/s') | strcmp(unit,'rd/sec')
            newUnit = 'tr/mn*';
            facteur=30/pi;
        end
        % Long. speed
        if strcmp(unit,'m/s')
            newUnit = 'km/h*';
            facteur=3.6;
        end
        % Power
        if strcmp(unit,'W')
            newUnit='kW*';
            facteur=1./1000;
        end
        % Energy
        if strcmp(unit,'Ws')
            newUnit='Wh*';
            facteur=1./3600;
        end
        % Mass flow
%         if strcmp(unit,'kg/h')
%             newUnit='g/s';
%             facteur=1000./3600;
%         end
    end

end


