function tileWindows(hf,nr,nc,screen_nr)
% function tileWindows(hf,nf,nc,screen_nr)
% Arrange figures in tile.
% INPUTS: 
% hf: array of figures (if not given, it will search for all figures)
% nr (1x1 int): number of rows
% nc (1x1 int): number of columns
% screen_nr (1x1 int): display number (multidisplay), default = 1


% NOTE:
% screen coordinates: (0,0) = left up corner
% figure coordinates: (0,0) = left bottom corner
% bigscr: big rectangle of all displays
% scr: selected display

if ~exist('hf','var')
    hf = findobj('type','figure');
end
if isempty(hf)
    hf = findobj('type','figure');
    Is = [hf.Number];
    [~, Is] = sort(Is);
    hf = hf(Is);
end
if ~exist('nr','var')
    nr =[];
end
if ~exist('nc','var')
    nc =[];
end
if ~exist('screen_nr','var')
    screen_nr = 1;
end
scr = get(0,'ScreenSize');
bigscr = get(0,'ScreenSize');%rectangle of all displays
ScreenDevices = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
for ind = 1:length(ScreenDevices)
MainBounds{ind} = ScreenDevices(ind).getDefaultConfiguration().getBounds();
end
scr = MainBounds{screen_nr};
scr = [scr.getMinX scr.getMinY scr.getMaxX scr.getMaxY];

% hf = sort(get(0,'Children'));

if isempty(nr) && isempty(nc)
    nr = 2;
end
if isempty(nr)
    nr = ceil(length(hf)/nc);
end
if isempty(nc)
    nc = ceil(length(hf)/nr);
end

fprintf('number of rows: %d, columns: %d\n',nr,nc);
width = floor((scr(3)-scr(1))/nc);
height = floor((scr(4)-scr(2))/nr);

ind_f = 1; %figure index
for ind_r = 1:nr %row index
    for ind_c = 1:nc %column index
        left = scr(1) + width*(ind_c-1);
        up = scr(2) + height*(ind_r-1);
        bottom = up + height;
        %         bottom = 1 + height*(ind_f-1);
        %transform up from screen coordinates to figure coordinates:
        bottom2 = bigscr(4)-bottom;
        rect = [left bottom2 width height];
        set(hf(ind_f),'OuterPosition',rect);
        ind_f = ind_f + 1;
        if ind_f>length(hf)
            return
        end
    end
end
end