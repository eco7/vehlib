function gr_mtherm_control(N,Padm,Phi,delta_AA,N_fct,Cmt_fct,VD)

% Occurences des points de fonctionnement
mat_fct = [N_fct(:), Cmt_fct(:)];
[mat_fct_uniq,~,occurences] = unique(mat_fct,'rows');
plot_fct = [mat_fct_uniq, accumarray(occurences, 1)];

dcarb = zeros(length(Padm),length(N));
Cmt = zeros(length(Padm),length(N));
for i_N = 1:length(N)
    for i_P = 1:length(Padm)
        [dcarb(i_P,i_N), Cmt(i_P,i_N)] = calc_mt_3U(N(i_N)*pi/30,Phi,delta_AA,Padm(i_P),size(Phi),VD);
    end
end

CSP = dcarb*3600./(pi/30*N.*Cmt/1000);
CSP(Cmt<0) = nan;

Cmt_max = max(max(Cmt));
Cmt_vec = 0:floor(Cmt_max);
sz_N = size(N);
sz_C = size(Cmt_vec');
sz_P = size(Padm');
N_mat = repmat(N,sz_C);
Cmt_mat = repmat(Cmt_vec',sz_N);
N_grid = repmat(N,sz_P);
CSP_Cmt = griddata(N_grid,Cmt,CSP,N_mat,Cmt_mat);

figure()
hold on; 
palier_pct = [0 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.4, 0.5, 0.75, 1, 1.25, 1.5];
seuilbas_cple = 0.01*Cmt_max;
min_CSP = ceil(min(min(CSP_Cmt(~isnan(CSP_Cmt) & ~isinf(CSP_Cmt) & Cmt_mat>seuilbas_cple)))/10)*10;
niveaux = min_CSP * ones(1,length(palier_pct));
niveaux = niveaux + niveaux.*palier_pct;
[c,h]=contour(N,Cmt_vec,CSP_Cmt,niveaux);
% Rajout des labels sur les courbes
clabel(c,h);
plot(N,Cmt(1,:),N,Cmt(end,:))
scatter(plot_fct(:,1),plot_fct(:,2),plot_fct(:,3)*10,'filled','or');
title('CSP (kWh)'); xlabel('R�gime (rpm)'); ylabel('Couple (Nm)');

end
