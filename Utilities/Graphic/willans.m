% Function: willans
% Tracee de courbes de willans pour un moteur thermique
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Mfile de trace des droites de willans dans le plan 
% puissance-debit de carburant ou dans le plan couple-debit de carburant
% par iso-regime
% Utilisation de la cartographie du moteur thermique (donnees de VEHLIB: VD.MOTH),
% Saturation par le couple maxi du regime considere
% Extrapolation a debit nul eventuelle
%
% Argument d'appel:
% VD.MOTH: structure de donnee VD.MOTH de vehlib
% method: 
% - oui: extrapolation de debit de carburant nul
% - non: on se limite a la cartographie de donnees
% %------------------------------------------------------------------
function []=willans(VD.MOTH,method)

if nargin ==0
    errordlg('Probleme d''appel a la fonction willans','willans')
elseif nargin==1
    %choix de la methode de calcul
    method='';
    while  isempty(method) | ( ~strcmp(method,'oui') & ~strcmp(method,'non'))
        method=input('Extrapolation a debit carburant nul degre 2 (oui/non)?\n','s');
    end
end

w=0;
while  isempty(w) | ( w<1 | w>2)
  fprintf(1,'%s\n','Trace couple-debit a isoregime    taper 1');
  fprintf(1,'%s\n','Trace puissance-debit a isoregime taper 2');
  w=input('Votre valeur:');
end

figure
hold on;
plot_option={ 'b+-';'r+-';'m+-';'g+-';'k+-'};
op=0;
legende='legend(';
for i=1:length(VD.MOTH.Reg_2dconso)
   clear X Y
   % Calcul du couple maxi au regime considere
   cmax=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,VD.MOTH.Reg_2dconso(i));
   if strcmp(method,'oui')
      k=3;
   else
      k=0;
   end
   for j=1:length(VD.MOTH.Cpl_2dconso)
      if VD.MOTH.Cpl_2dconso(j)<=cmax
         k=k+1;
         if w==2
            X(k)=VD.MOTH.Cpl_2dconso(j)*VD.MOTH.Reg_2dconso(i)/1000;
         else
            X(k)=VD.MOTH.Cpl_2dconso(j);
         end
         Y(k)=VD.MOTH.Conso_2d(i,j);
      end
   end
   if strcmp(method,'oui')
      % extrapolation jusqu a debit nul
      [courbe,s]=polyfit(Y(4:length(Y)),X(4:length(Y)),3);
      Y(1)=0;
      X(1)=polyval(courbe,Y(1));
      Y(2)=min(VD.MOTH.Conso_2d(i,:))/3;
      X(2)=polyval(courbe,Y(2));
      Y(3)=min(VD.MOTH.Conso_2d(i,:))/2;
      X(3)=polyval(courbe,Y(3));      
   end
   if op>=(length(plot_option)-1)
      op=1;
   else
      op=op+1;
   end
   plot(X,Y,plot_option{op});
   legende=[legende,strcat('''isoN: ',num2str(round(VD.MOTH.Reg_2dconso(i)*30/pi)),' tr/mn'',')];
end
legende=strcat(legende(1:length(legende)-1),',4)');
eval(legende);
if w==2
   xlabel('Puissance en kW');
else
   xlabel('Couple en Nm');
end
ylabel('Debit de carburant en g/s');
