function fig2pdf(fileFig)

fileOut = regexprep(fileFig,'.fig$','.pdf');
h = openfig(fileFig);

P = get(h,'position');
set(h,'PaperUnits','points')
set(h,'paperposition',[0 0 P(3) P(4)])
set(h, 'PaperSize', [P(3) P(4)]);

% set(h,'paperposition',[0 0 20 20])
% set(h, 'PaperSize', [25 25]);
%nettoyage des accents
%axes (title, xlabel, ylabel)
ha = findobj(h,'type','axes','tag','');
ht = {get(ha,'title');get(ha,'xlabel');get(ha,'ylabel');};
%legendes
hl = findobj(h,'type','axes','tag','legend');%legend hadle list
ht2 = findobj(hl,'type','text');%legend text

% ht = {get(ha,'title');get(ha,'xlabel');get(ha,'ylabel');findobj(hl,'type','text')};

while iscell(ht)
    ht = [ht{:}];
end

ht = [ht(:); ht2(:)];
S = get(ht,'string');
S = regexprep(S,{'è','é','ê','ë'},{char(232) char(233) char(234) char(235)});
% S = regexprep(S,'é','\"e');%e aigue: TODO bricoler en LaTeX
% S = regexprep(S,'é','e');
for ind = 1 : length(ht)
    if ismember('é',S{ind})
        fprintf('IcI\n');
    end
set(ht(ind),'string',S{ind});
end
%export
fprintf('%s >>> %s',fileFig,fileOut);
myStyle = hgexport('factorystyle');
myStyle.Resolution = '300';
myStyle.FontMode = 'fixed';
myStyle.FixedFontSize = 20;
% set(h, 'PaperUnits','centimeters');
% set(h, 'PaperPosition',[1 1 10 4]);
hgexport(h, fileOut,hgexport('factorystyle'), 'Format', 'pdf','fontencoding','latin1','LineWidthMin',0.2000, 'Background','none');
fprintf(' OK\n');

end