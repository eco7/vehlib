% Function: gr_melec
% ------------------------------------------------------
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% function []=gr_melec(ACM1,wmg1,cmg1)
%
% fonction de tracee de cartographie MAchine Electrique
%
% ACM1 = structure renseignant les caracteristiques du moteur a
% cartographier. Doit respecter les contraintes de VEHLIB.
% wacm1 et cacm1 = points de fonctionnements en regime et couple.
% ------------------------------------------------------
function []=gr_melec(ACM1,wacm1,cacm1,fit_pertes)

% if nargin==4 && fit_pertes==1
%     [coeff_acm1] = carto_resmat('MCT_MSA_N1_PRIUS_II',0);
%     c1_acm1=coeff_acm1(1);
%     c2_acm1=coeff_acm1(2);
%     c3_acm1=coeff_acm1(3);
%     c4_acm1=coeff_acm1(4);
%     c5_acm1=coeff_acm1(5);
%     c6_acm1=coeff_acm1(6);
%     c7_acm1=coeff_acm1(7);
%     c8_acm1=coeff_acm1(8);
%     c9_acm1=coeff_acm1(9);
% end

if nargin==4 && fit_pertes==1
    if isfield(ACM1,'c1')
        c1_acm1=ACM1.c1;
    else
        c1_acm1=0;
    end
    if isfield(ACM1,'c2')
        c2_acm1=ACM1.c2;
    else
        c2_acm1=0;
    end
    if isfield(ACM1,'c3')
        c3_acm1=ACM1.c3;
    else
        c3_acm1=0;
    end
    if isfield(ACM1,'c4')
        c4_acm1=ACM1.c4;
    else
        c4_acm1=0;
    end
    if isfield(ACM1,'c5')
        c5_acm1=ACM1.c5;
    else
        c5_acm1=0;
    end
    if isfield(ACM1,'c6')
        c6_acm1=ACM1.c6;
    else
        c6_acm1=0;
    end
    if isfield(ACM1,'c7')
        c7_acm1=ACM1.c7;
    else
        c7_acm1=0;
    end
    if isfield(ACM1,'c8')
        c8_acm1=ACM1.c8;
    else
        c8_acm1=0;
    end
    if isfield(ACM1,'c9')
        c9_acm1=ACM1.c9;
    else
        c9_acm1=0;
    end
end

clear val elev
%global ACM1 ECU VEHI TRAN CYCL BATT ACM2 MOTH PANT INIT

%ACM1=evalin('base','ACM1');

hold off;

%attente=waitbar(0,'Trace d une cartographie moteur electrique. Veuillez patienter ...');
set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));

v=min(ACM1.Regmot_pert):25:max(ACM1.Regmot_pert);
v=v';
w=min(ACM1.Cmot_pert):5:max(ACM1.Cmot_pert);
if nargin==4 && fit_pertes==1
    v_mat=repmat(v,[1 length(w)]);
    w_mat=repmat(w,[length(v) 1]);
   assignin('base','v_mat',v_mat)
   assignin('base','w_mat',w_mat)
    elev=(c1_acm1*w_mat.*w_mat+c2_acm1*w_mat+c3_acm1).*v_mat.*v_mat+...
             (c4_acm1*w_mat.*w_mat+c5_acm1*w_mat+c6_acm1).*v_mat+...
             (c7_acm1*w_mat.*w_mat+c8_acm1*w_mat+c9_acm1);

else
    elev=griddata(ACM1.Regmot_pert,ACM1.Cmot_pert,ACM1.Pert_mot',v,w);
    elev=elev';
    size(elev);
end
%waitbar(0.09);

for i=1:length(v)
   % calcul du couple maxi au regime considere
   cmax=interp1(ACM1.Regmot_cmax,ACM1.Cmax_mot(:,length(ACM1.Cmax_mot(1,:))),v(i));
   cmin=interp1(ACM1.Regmot_cmax,ACM1.Cmin_mot(:,length(ACM1.Cmin_mot(1,:))),v(i));
  % waitbar((i/length(v)+0.1)*0.9);
   for j=1:length(w)
      if w(j)>cmax | w(j)<cmin
         val(i,j)=NaN;
      else
         p=w(j)*v(i);
         if w(j)>0
           if elev(i,j)+p~=0
              val(i,j)=100*p/(elev(i,j)+p);
           else
              val(i,j)=0;
           end
         else
           if p~=0
              val(i,j)=100*(p+elev(i,j))/p;
           else
              val(i,j)=0;
           end
         end        
      end
   end
end




plot(ACM1.Regmot_cmax*30/pi,ACM1.Cmax_mot(:,length(ACM1.Cmax_mot(1,:))));
hold on;
grid on;
ax=['[0 ',num2str(ACM1.Regmot_cmax(length(ACM1.Regmot_cmax))*30/pi),' ', ...
      num2str(min(ACM1.Cmin_mot(:,length(ACM1.Cmin_mot(1,:))))-10),' ',num2str(max(ACM1.Cmax_mot(:,length(ACM1.Cmax_mot(1,:))))+10),']'];
eval(['axis(',ax,')']);
plot(ACM1.Regmot_cmax*30/pi,ACM1.Cmax_mot(:,1),'k--');
plot(ACM1.Regmot_cmax*30/pi,ACM1.Cmin_mot(:,length(ACM1.Cmin_mot(1,:))));
plot(ACM1.Regmot_cmax*30/pi,ACM1.Cmin_mot(:,1),'k--');

if exist('wacm1','var') && exist('cacm1','var')
    %plot(wacm1*30/pi,cacm1,'b+');
    mat_fct = [wacm1(:)*30/pi,cacm1(:)];
    [mat_fct_uniq,~,occurences] = unique(mat_fct,'rows');
    plot_fct = [mat_fct_uniq, accumarray(occurences, 1)];
    scatter(plot_fct(:,1),plot_fct(:,2),plot_fct(:,3)*10,'filled','or');
end

xlabel('Regime moteur electrique en Tr/mn');
ylabel('Couple sur l arbre en Nm');
leg1=['Couple enveloppe traction a : ' num2str(ACM1.Tension_cont(length(ACM1.Tension_cont))) ' V'];
leg2=['Couple enveloppe traction a : ' num2str(ACM1.Tension_cont(1)) ' V'];
leg3=['Couple enveloppe recuperation a : ' num2str(ACM1.Tension_cont(length(ACM1.Tension_cont))) ' V'];
leg4=['Couple enveloppe recuperation a : ' num2str(ACM1.Tension_cont(1)) ' V'];

legend(leg1,leg2,leg3,leg4,'Points de fonctionnement sur le cycle');

% Apres l affichage de la legende, on trace les iso rendements
[c,h]=contour(v*30/pi,w,val',round([50 65 78 87 93 97 99 100 ]*max(max(val))/100));
clabel(c,h);


if isfield(ACM1,'nom')
    title(['Rendement moteur et onduleur en % :',ACM1.nom]);
else
    title(['Rendement moteur et onduleur en %']);
end

hold off;
%close(attente);


%------------------------------------------MISES A JOUR-------------------------------------------%
% 01/10/2003. MDR. Modification ligne 63 : evalin('base','exist...') au lieu de exist(...).
% 30/10/2003. MDR et RT. Modification lignes 61,62. Cmin la place de -Cmax.
% 11/02/2004. MDR. Modification des valeurs appelees par la fonction pour
% plus de flexibilite.
