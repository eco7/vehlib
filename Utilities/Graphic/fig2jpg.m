function fig2jpg(fileFig)

filePng = regexprep(fileFig,'.fig$','.jpg');
h = openfig(fileFig);

fprintf('%s >>> %s',fileFig,filePng);
myStyle = hgexport('factorystyle');
myStyle.Resolution = '300';
hgexport(h, filePng,hgexport('factorystyle'), 'Format', 'jpeg','Resolution',300);
fprintf(' OK\n');

end