function maxiWindows(h)
if ~exist('h','var')
    h = findobj('type','figure');
end
scr = get(0,'ScreenSize');


width = floor(scr(3));
height = floor(scr(4));

ind_w = 1;
for ind = 1:length(h)
    left = 1;
    up = scr(4);
    bottom = up - height;
    rect = [left bottom width height];
    set(h(ind),'OuterPosition',rect);
end
end