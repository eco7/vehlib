% ------------------------------------------------------
% function []=gr_melec_pelec(ACM1,wmg1,pmg1)
%
% fonction de tracee de cartographie MAchine Electrique de type
% rendement(regime,puisance)
%
% ACM1 = structure renseignant les caracteristiques du moteur a
% cartographier. Doit respecter les contraintes de VEHLIB.
% wacm1 et pacm1 = points de fonctionnements en regime et puissance elec.
% ------------------------------------------------------
function []=gr_melec_pelec(ACM1,wacm1,pacm1)

clear val elev
%global ACM1 ECU VEHI TRAN CYCL BATT ACM2 MOTH PANT INIT

%ACM1=evalin('base','ACM1');

hold off;

attente=waitbar(0,'Trace d une cartographie moteur electrique. Veuillez patienter ...');
set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));

v=min(ACM1.Regmot_pert):25:max(ACM1.Regmot_pert);
v=v';
pelec=min(ACM1.Pelec_pert):2000:max(ACM1.Pelec_pert);
pertes=griddata(ACM1.Regmot_pert,ACM1.Pelec_pert,ACM1.Pertes_pelec,v,pelec);
pertes=pertes';
waitbar(0.09);

for i=1:length(v)
   % calcul du couple maxi au regime considere
   pmax=interp1(ACM1.Regmot_cmax,ACM1.Pelec_max(:,length(ACM1.Pelec_max(1,:))),v(i));
   pmin=interp1(ACM1.Regmot_cmax,ACM1.Pelec_min(:,length(ACM1.Pelec_min(1,:))),v(i));
   waitbar((i/length(v)+0.1)*0.9);
   for j=1:length(pelec)
      if pelec(j)>pmax | pelec(j)<pmin
         val(i,j)=NaN;
      else
         if pelec(j)>0         
              val(i,j)=(pelec(j)-pertes(i,j))/pelec(j)*100;
         elseif pelec(j)<0     
             val(i,j)=-pelec(j)/(pertes(i,j)-pelec(j))*100;
         else
             val(i,j)=0;
         end   
      end
   end
end

plot(wacm1*30/pi,pacm1,'b+');
hold on;
grid on;
ax=['[0 ',num2str(ACM1.Regmot_cmax(length(ACM1.Regmot_cmax))*30/pi),' ', ...
      num2str(min(ACM1.Pelec_min(:,length(ACM1.Pelec_min(1,:))))-10),' ',num2str(max(ACM1.Pelec_max(:,length(ACM1.Pelec_max(1,:))))+10),']'];
%eval(['axis(',ax,')']);
plot(ACM1.Regmot_cmax*30/pi,ACM1.Pelec_max(:,1),'k--');
plot(ACM1.Regmot_cmax*30/pi,ACM1.Pelec_max(:,length(ACM1.Pelec_max(1,:))),'r','linewidth',2);
plot(ACM1.Regmot_cmax*30/pi,ACM1.Pelec_min(:,length(ACM1.Pelec_min(1,:))),'r','linewidth',2);
plot(ACM1.Regmot_cmax*30/pi,ACM1.Pelec_min(:,1),'k--');



xlabel('Regime moteur electrique en Tr/mn');
ylabel('Puissance electrique en W');
leg1=['Puissance enveloppe traction a : ' num2str(ACM1.Tension_cont(length(ACM1.Tension_cont))) ' V'];
leg2=['Puissance enveloppe traction a : ' num2str(ACM1.Tension_cont(1)) ' V'];
leg3=['Puissance enveloppe recuperation a : ' num2str(ACM1.Tension_cont(length(ACM1.Tension_cont))) ' V'];
leg4=['Puissance enveloppe recuperation a : ' num2str(ACM1.Tension_cont(1)) ' V'];

legend(leg1,leg2,leg3,leg4,'Points de fonctionnement sur le cycle');

% Apres l affichage de la legende, on trace les iso rendements
[c,h]=contour(v*30/pi,pelec,val',round([50 65 78 87 93 97 99 100 ]*max(max(val))/100));
clabel(c,h);


if isfield(ACM1,'nom')
    title(['Rendement moteur et onduleur en % :',ACM1.nom]);
else
    title(['Rendement moteur et onduleur en %']);
end

hold off;
close(attente);


%------------------------------------------MISES A JOUR-------------------------------------------%
% 01/10/2003. MDR. Modification ligne 63 : evalin('base','exist...') au lieu de exist(...).
% 30/10/2003. MDR et RT. Modification lignes 61,62. Cmin la place de -Cmax.
% 11/02/2004. MDR. Modification des valeurs appelees par la fonction pour
% plus de flexibilite.
