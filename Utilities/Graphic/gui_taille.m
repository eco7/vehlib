% Function: gui_taille
% Calcul le facteur d'echelle en fonction de la definition de l'ecran
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
% Calcul le facteur d'echelle en fonction de la definition de l'ecran
% (par rapport a un ecran par defaut de 1400*1050)
%
% Argument:
% figurePos: Position et taille de la la figure en pixel [XPos YPos Largeur Hauteur]
%
% Retour:
% pixfactor: Facteur d'echelle [Largeur Hauteur Largeur Hauteur]
%
% Taille_car: Taille pour police de caractere (a approfondir)
%
% figurePos: obsolete
% 
% -----------------------------------------------------------------
   
function [pixfactor,Taille_car,figurePos]=gui_taille(figurePos,defaultscreensize)
   
   Taille_car=8;
   
   oldRootUnits = get(0,'Units');
   set(0,'Units','pixels');
   screensize=get(0,'Screensize');
   %set(0,'Units','characters');
   %screensize_char=get(0,'Screensize');

   % Calcul du facteur d'echelle par rapport a la resolution de l'ecran.
   if nargin==1
   defaultscreensize=[1400 1050]; % l,h
   end
   pix(1)=screensize(3)./defaultscreensize(1); % Facteur pr largeur en pixel
   pix(2)=screensize(4)./defaultscreensize(2); % Facteur pr hauteur en pixel
   
   % en unite cacracteres
   pixfactor=[pix(1) pix(2) pix(1) pix(2)];
   
   set(0, 'Units', oldRootUnits);
