% Function: initdata
% initdata renseigne la structure INIT de vehlib
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%
% Objet:
%
% Creation de la structure INIT permettant de renseigner les conditions initiales 
% des modeles de la librairie VEHLIB et certains parametres generaux des
% modeles et des scripts de VEHLIB.
%
% =============================================================================

% initialisation generale a tous les composants

% Version de la bibliotheque vehlib
INIT.version='2011a';

% Frequence d echantillonnage des donnees
INIT.t_ech=0.1;
%INIT.t_ech=-1;

% Temperature ambiante
INIT.Tamb=20;

% Repertoire de post traitement
if isunix
   INIT.sep='/';
else
   INIT.sep='\';
end

INIT.VEHLIB_ROOT=which('initdata');

s=regexp(INIT.VEHLIB_ROOT,filesep);
INIT.VEHLIB_ROOT=INIT.VEHLIB_ROOT(1:s(end-2)-1);

clear s;

INIT.InitialFolder=fullfile(INIT.VEHLIB_ROOT,'Utilities','Initial');
INIT.ResultsFolder=fullfile(INIT.VEHLIB_ROOT,'Results');
INIT.UsersFolder=fullfile(INIT.VEHLIB_ROOT,'Users');
INIT.RoadFolder=fullfile(INIT.VEHLIB_ROOT,'Data','Road');
INIT.TestCaseFolder=fullfile(INIT.VEHLIB_ROOT,'Utilities','TestCase');

% Conditions de decharge initiales de la batterie
INIT.Dod0=40;
INIT.Ubat0=0;
INIT.Vc0=0;
INIT.Tbat0=INIT.Tamb;
INIT.Dod_bt=20;

INIT.Tcat0=INIT.Tamb;
%INIT.Tcat0=450;

% Champ obsolete a supprimer a terme !
INIT.propriete='LTE';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% REVISIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
