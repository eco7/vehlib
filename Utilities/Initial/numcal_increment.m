% Function: numcal_increment
% Fonction d'increment du numero de calcul pour la bibliotheque de modele VEHLIB 
%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
%
% Fonction d'increment du numero de calcul pour la bibliotheque de modele VEHLIB 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ù
function []=numcal_increment(VD)

if nargin == 0
    initdata
    VD.INIT=INIT;
end

% Lecture et increment du numero de calcul
fnum=fullfile(VD.INIT.InitialFolder,'numcal.mat');

% Le fichier numcal.mat n'existe pas a la premiere execution de VEHLIB
try
    load(fnum, 'numcal');
catch
   % Si le fichier n'existe pas on va le creer
    numcal=0;
end

if(numcal<9999)
 numcal=numcal+1;
else
 numcal=1;
end

% Attention, il faut avoir la permission en ecriture
% verification ....
try
  save(fnum,'numcal');
catch
   errmsg = lasterr;
   if(~isempty(errmsg))
      %uiwait(errordlg(errmsg,strcat('VEHLIB version:',num2str(VD.INIT.version),' '),'modal'));
      fprintf(1,'%s\n',errmsg);
   end
end

