% Function: calc_exhaust_emissions
% fonction de calcul des emissions du pot catalytique 3 voies
%
% Copyright IFSTTAR LTE 1999-2011
%
% Objet:
%
% Arguments appel :
% Arguments de sortie :
%
% 07-01-2014 (BJ) : version 1

function [ERR, Tcat, Tcatg, Texh, Tcoll, CO_eff, HC_eff, NOx_eff, CO, HC, NOx,dQs,dQin,dQgen]=calc_exhaust_emissions(ERR, param, VD, indice, wmt, cmt, dcarb, Tcat_p, Tcatg_p, Tcoll_p, CO_eff_p, HC_eff_p, NOx_eff_p, elhyb)
                                                 
%if sum(size(cmt))~=sum(size(wmt))
%    chaine=' taille argument entree incompatible \n size(cmt) : %s\n  size(wmt) : %s\n ';
%    ERR=MException('BackwardModel:calc_mt',chaine,num2str(size(cmt)),num2str(size(wmt)));
%    [dcarb,cmt,cfrein_meca_mt]=backward_erreur(length(cmt));
%    return;
%end
if VD.TWC.ntypcata == 2
    
    % lambda feedGas (exces d'air sortie moteur)
    lambdaFG=1;
    
    % debit Air en kg/s
    debitAir=lambdaFG*VD.MOTH.AStoechio*dcarb/1000;
    debitGaz=debitAir*(1+lambdaFG/VD.MOTH.AStoechio);
    
    % emissions echappement en ppm
    CO_FG_ppm=interp1(VD.MOTH.lambda,VD.MOTH.COppm,lambdaFG);
    HC_FG_ppm=interp1(VD.MOTH.lambda,VD.MOTH.HCppm,lambdaFG);
    NOx_FG_ppm=interp1(VD.MOTH.lambda,VD.MOTH.NOxppm,lambdaFG);
    
    % Temperatures ambiante et echappement
    Tamb=VD.INIT.Tamb+273;
    
    %Texh=VD.MOTH.exh1*cmt.^2+VD.MOTH.exh2*wmt.^0.25+VD.MOTH.exh3.*cmt.*wmt+273;
    Texh=interp2(VD.MOTH.Reg_2dTexh,VD.MOTH.Cpl_2dTexh,VD.MOTH.Texh_2d,wmt,cmt);
    Texh=max(Texh,273+VD.INIT.Tamb);
    Texh(dcarb<=0 & wmt>=VD.MOTH.ral_min)=VD.MOTH.Texh_min; % Moteur tournant, coupure d'injection
    Texh(dcarb<=0 & wmt<VD.MOTH.ral_min)=273+VD.INIT.Tamb; % Moteur arrete
    
    % Modele thermique collecteur echappement
    % Modele thermique catalyseur
    dQsColl=VD.TWC.HsAColl*(Tamb-Tcoll_p); % Echange avec l'exterieur
    dQinColl=VD.TWC.HsinAColl*(Texh-Tcoll_p); % Enthalpie gaz echappement
    dQinColl(elhyb==0)=0;
    
    Tcoll=Tcoll_p+(dQsColl+dQinColl)/(VD.TWC.MColl*VD.TWC.CpColl)*param.pas_temps;
    
    % ROL model (A developper)
    lambdaTP=1; % Exces d'air echappement (sortie catalyseur)
    
    % Modele thermique catalyseur
    
    % Heat transferred from the TWC to the surroundings
    dQs=VD.TWC.HsA*(Tamb-Tcat_p);
    
    % Heat transferred from the gas to the solid
    dQin=VD.TWC.HinA*VD.TWC.Ageo*(Tcatg_p-Tcat_p)*ones(size(elhyb));
    dQin(elhyb==0)=0;
    
    % Heat transfer due to Conduction
    dQcond=0;
    
    % Heat transfer due to Exothermic reactions
    dQgen=(-1)*VD.TWC.Kreac*((debitGaz*1000/VD.MOTH.Mexh)* ...
        ((CO_eff_p*CO_FG_ppm*1e-6)*VD.TWC.dH0CO+ ...
        (HC_eff_p*HC_FG_ppm*1e-6)*(0.86*VD.TWC.dH0C3H6/3+0.14*VD.TWC.dH0CH4)));
    Tcat=Tcat_p+(dQs+dQin+dQcond+dQgen) ...
        /((1-VD.TWC.e)*VD.TWC.RhoS*VD.TWC.CpCata)*param.pas_temps;
    
    K1=VD.TWC.CpGaz*debitGaz/VD.TWC.Vcs;
    Tcatg=((Tcoll.*K1)+VD.TWC.HinA*VD.TWC.Ageo*Tcat)./(K1+VD.TWC.HinA*VD.TWC.Ageo);
    
    CO_eff=exp(-VD.TWC.aCO*(VD.TWC.dCO./(lambdaTP-VD.TWC.CO0)).^VD.TWC.mCO-VD.TWC.aT*(VD.TWC.dT./(Tcat-VD.TWC.T0)).^VD.TWC.mT);
    HC_eff=exp(-VD.TWC.aHC*(VD.TWC.dHC./(lambdaTP-VD.TWC.HC0)).^VD.TWC.mHC-VD.TWC.aT*(VD.TWC.dT./(Tcat-VD.TWC.T0)).^VD.TWC.mT);
    NOx_eff=exp(-VD.TWC.aNOx*(VD.TWC.dNOx./(lambdaTP-VD.TWC.NOx0)).^VD.TWC.mNOx-VD.TWC.aT*(VD.TWC.dT./(Tcat-VD.TWC.T0)).^VD.TWC.mT);
    
    % Emissions massiques echappement en g/s
    CO=(debitGaz*1000).*(CO_FG_ppm*1e-6).*(1-CO_eff);
    HC=(debitGaz*1000).*(HC_FG_ppm*1e-6).*(1-HC_eff);
    NOx=(debitGaz*1000).*(NOx_FG_ppm*1e-6).*(1-NOx_eff);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Recalculate heat with newer values of Tcat and Tcatg (mandatory for
    % Hamiltonian calculation)
    % Heat transferred from the TWC to the surroundings
    dQs=VD.TWC.HsA*(Tamb-Tcat);
    
    % Heat transferred from the gas to the solid
    dQin=VD.TWC.HinA*VD.TWC.Ageo*(Tcatg-Tcat).*ones(size(elhyb));
    dQin(elhyb==0)=0;
    
else
    % Modele type 1
    
    % lambda feedGas (exces d'air sortie moteur)
    lambdaFG=1;
    
    % debit Air en kg/s
    debitAir=lambdaFG*VD.MOTH.AStoechio*dcarb/1000;
    debitGaz=debitAir*(1+lambdaFG/VD.MOTH.AStoechio);
    
    % emissions echappement en ppm
    CO_FG_ppm=interp1(VD.MOTH.lambda,VD.MOTH.COppm,lambdaFG);
    HC_FG_ppm=interp1(VD.MOTH.lambda,VD.MOTH.HCppm,lambdaFG);
    NOx_FG_ppm=interp1(VD.MOTH.lambda,VD.MOTH.NOxppm,lambdaFG);
    
    % Temperatures ambiante et echappement
    Tamb=VD.INIT.Tamb+273;
    %Texh=VD.MOTH.exh1*cmt.^2+VD.MOTH.exh2*wmt.^0.25+VD.MOTH.exh3.*cmt.*wmt+273;
    Texh=interp2(VD.MOTH.Reg_2dTexh,VD.MOTH.Cpl_2dTexh,VD.MOTH.Texh_2d,wmt,cmt);
    Texh=max(Texh,273+VD.INIT.Tamb);
    % ROL model (A developper)
    lambdaTP=1; % Exces d'air echappement (sortie catalyseur)
    
    % Modele thermique
    dQs=VD.TWC.HsA*(Tamb-Tcat_p); % Echange avec l'exterieur
    dQin=VD.TWC.HinA*(Texh-Tcat_p); % Enthalpie gaz echappement
    dQgen=(debitGaz*1000/VD.MOTH.Mexh)* ...
        ((CO_eff_p*CO_FG_ppm*1e-6)*VD.TWC.dH0CO+ ...
        (HC_eff_p*HC_FG_ppm*1e-6)*(0.86*VD.TWC.dH0C3H6/3+0.14*VD.TWC.dH0CH4));
    % Heat generated by conversion of pollutants (CO et HC; quid des NOx ??) in VD.TWC
    
    Tcat=Tcat_p+(dQs+dQin-dQgen)/(VD.TWC.MCata*VD.TWC.CpCata)*param.pas_temps;
    
    % Conversion efficiency as a function of Tcat and lambdaTP
    CO_eff=exp(-VD.TWC.aCO*(VD.TWC.dCO./(lambdaTP-VD.TWC.CO0)).^VD.TWC.mCO-VD.TWC.aT*(VD.TWC.dT./(Tcat-VD.TWC.T0)).^VD.TWC.mT);
    HC_eff=exp(-VD.TWC.aHC*(VD.TWC.dHC./(lambdaTP-VD.TWC.HC0)).^VD.TWC.mHC-VD.TWC.aT*(VD.TWC.dT./(Tcat-VD.TWC.T0)).^VD.TWC.mT);
    NOx_eff=exp(-VD.TWC.aNOx*(VD.TWC.dNOx./(lambdaTP-VD.TWC.NOx0)).^VD.TWC.mNOx-VD.TWC.aT*(VD.TWC.dT./(Tcat-VD.TWC.T0)).^VD.TWC.mT);
    
    % Emissions massiques echappement en g/s
    CO=(debitGaz*1000).*(CO_FG_ppm*1e-6).*(1-CO_eff);
    HC=(debitGaz*1000).*(HC_FG_ppm*1e-6).*(1-HC_eff);
    NOx=(debitGaz*1000).*(NOx_FG_ppm*1e-6).*(1-NOx_eff);
    
    Tcoll = Texh;
    Tcatg = Tcat;
    
end
end
