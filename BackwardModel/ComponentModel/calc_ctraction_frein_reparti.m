% Function: calc_ctraction_frein_reparti
%
% fonction de calcul du couple de traction / freinage avec des strategies de freinage d'apres les travaux de stage de S. Hazel 
%
% * optimisation de la stabilite du vehicule
% * optimisation de la recuperation d'energie au freinage sur l'essieu avant ou l'essieu arriere
% 
%
% Argument d'appel:
% 
% VD.VEHI - structure des parametres vehicule
% 
% VD.CYCL - cycle de conduite
% 
% VD.ECU - parametre du calculateur (indication de la strategie)
% 
% ctraction - couple de traction/frein avant repartition
% 
%
% Exemple de lancement:
% (start code)
% [ctraction]=calc_ctraction_frein_reparti(ctraction);
% (end)
% 
% Resultat:
% 
% ctraction - couple de traction/frein apres repartition
% 


function [ctraction]=calc_ctraction_frein_reparti(ctraction)

% calcul des courbes de repartition de la force de freinage

% caractéristique vehicule

L=VD.VEHI.Empatt ;                       % empattement
La=(1-VD.VEHI.Masse_frac).*VD.VEHI.Empatt ; % distance essieu avant plan du centre de gravite
Lb=L-La ;                             % distance essieu arriere plan du centre de gravite
h=VD.VEHI.Hcg ;                          % hauteur du centre de gravite
g=VD.VEHI.Gpes ;                         % constante gravite
mu_max=VD.VEHI.adherance ;               % coefficient d'adherence max


mu=abs(VD.CYCL.accel)./g ; % coefficient de deceleration (a/g)
mu(mu<0)=0;

% calcul de l'angle de la pente
if VD.CYCL.ntyppente==2 || VD.CYCL.ntyppente==3
     % suppression des doublons pour interpolation
     dist_calc=cumtrapz(VD.CYCL.temps,VD.CYCL.vitesse); % distance calculee a partir de la vitesse VD.CYCL.vitesse
     penteFpk=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,dist_calc,'linear',0); % reechantillonage pente fonction distance claculee. 
     alpha=atan(penteFpk/100);
    if dist_calc(end)>VD.CYCL.PKpente(end)
        warning('BackwardModel:calc_repart_frein_2 : cycle mal defini, PKpente trop court')
    end
     
elseif VD.CYCL.ntyppente==0
    alpha=atan(VD.CYCL.pente/100);
else
    chaine=' modele de pente non reconnu ';
    ERR=MException('BackwardModel:calc_repart_frein_2',chaine);
end

% courbe de stabilite
R_Fav1_Ftot=(Lb.*cos(alpha)+(mu+sin(alpha)).*h)./(L.*cos(alpha));

% courbe reglementaire ECE
R_Fav2_Ftot=((mu+0.07).*(Lb.*cos(alpha)+(mu+sin(alpha)).*h)./(0.85.*L.*cos(alpha).*mu));

% f-line (blocage roues avants)
R_Fav3_Ftot=mu_max.*(Lb.*cos(alpha)+(mu+sin(alpha)).*h)./(L.*cos(alpha).*mu);

% r-line (blocaqe roues arrieres)
R_Far3_Ftot=mu_max.*((La.*cos(alpha)-(mu+sin(alpha)).*h)./(L.*cos(alpha).*mu));

R_Fav4_Ftot=1-R_Far3_Ftot;

% repartition de frein av 
% strategie stabilite
R_stab=R_Fav1_Ftot;
% strategie recup max av
R_recup_max_av=min(ones(size(VD.CYCL.temps)),min(R_Fav2_Ftot,R_Fav3_Ftot));
R_recup_max_av(isinf(R_recup_max_av))=1;
% strategie recup max ar
R_recup_max_ar=max(zeros(size(VD.CYCL.temps)),R_Fav4_Ftot);
R_recup_max_ar(isinf(R_recup_max_ar))=0;

if VD.ECU.strat_frein==1
    % recup max
    if VD.VEHI.pos_acm1==1     
        % essieu avant moteur, calcul du taux de freinage avant
        Repart=R_recup_max_av;
    elseif VD.VEHI.pos_acm1==2 
        % essieu arriere moteur, calcul du taux de freinage arriere
        Repart=1-R_recup_max_ar;
    end
elseif VD.ECU.strat_frein==2
    % stabilite
    if VD.VEHI.pos_acm1==1     
        % essieu avant moteur, calcul du taux de freinage avant
        Repart=R_stab;
    elseif VD.VEHI.pos_acm1==2 
        % essieu arriere moteur, calcul du taux de freinage arriere
        Repart=1-R_stab;
    end
end

% Le vehicule avance
ctraction(VD.CYCL.vitesse>=0)=max(ctraction(VD.CYCL.vitesse>=0),Repart(VD.CYCL.vitesse>=0).*ctraction(VD.CYCL.vitesse>=0));
% le vehicule recule
ctraction(VD.CYCL.vitesse<0)=min(ctraction(VD.CYCL.vitesse<0),Repart(VD.CYCL.vitesse<0).*ctraction(VD.CYCL.vitesse<0));
end
