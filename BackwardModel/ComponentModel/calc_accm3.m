% function [ERR,csec_belt1,csec_belt2]=calc_accm3(wsec,dwsec,J)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Accessoires mecaniques de type 3

function [ERR,csec_belt1,csec_belt2]=calc_accm3(ERR,VD,wsec,dwsec,J)

if VD.ACC.ntypacc~=3
    ERR=MException('BackwardModel:calc_accm3',strcat('Type d''accessoires non reconnues : ',num2str(VD.ACC.ntypacc)));
    csec_belt1=[];
    csec_belt2=[];
    return;
end

if J~=0
    ERR=MException('BackwardModel:calc_accm','Integrer les inerties dans le calcul des auxiliaires mecaniques');
    csec_belt1=[];
    csec_belt2=[];
    return;
end

% Vitesse arbre primaire 1 (alternateur)
A=ones(size(wsec))*VD.BELT1.kred1*(1-VD.BELT1.glisst1);
wprim1=A.*wsec;

% Couples sur primaire 1
% Alternateur
cprim1_alt=-1*(VD.ACC.Pacc_elec./VD.ACC.rdmoy_acm2)./wprim1;
cprim1_alt(find(isnan(cprim1_alt)))=0;
cprim1_alt(find(isinf(cprim1_alt)))=0;

% Servo-direction
cprim1_servo=-1*(VD.ACC.Pservo)./wprim1;
cprim1_servo(find(isnan(cprim1_servo)))=0;
cprim1_servo(find(isinf(cprim1_servo)))=0;

% Ventilateur ref. moteur (hydrostatique)
cprim1_ventil=-1*(interp1(VD.ACC.Pventil_wmt,VD.ACC.Pventil_pventil,wsec))./wprim1;
cprim1_ventil(find(isnan(cprim1_ventil)))=0;
cprim1_ventil(find(isinf(cprim1_ventil)))=0;

% Compresseur d'air
cprim1_comp=-1*(interp1(VD.ACC.Pcomp_wmt,VD.ACC.Pcomp_pcomp,wsec))./wprim1;
cprim1_comp(find(isnan(cprim1_comp)))=0;
cprim1_comp(find(isinf(cprim1_comp)))=0;

% somme
cprim1=cprim1_alt+cprim1_servo+cprim1_ventil+cprim1_comp;

% Couple sur le secondaire 1
A=ones(size(cprim1))*VD.BELT1.kred1.*VD.BELT1.rend1;
indice=find(cprim1<0);
A(indice)=VD.BELT1.kred1./VD.BELT1.rend1;
csec_belt1=A.*cprim1;

% Vitesse arbre primaire 2 (compresseur de climatisation)
A=ones(size(wsec))*VD.BELT1.kred2*(1-VD.BELT1.glisst2);
wprim2=A.*wsec;

% Couple sur primaire 2
cprim2=zeros(size(cprim1));
% Couple sur le secondaire 2
A=ones(size(cprim2))*VD.BELT1.kred2.*VD.BELT1.rend2;
indice=find(cprim2<0);
A(indice)=VD.BELT1.kred2./VD.BELT1.rend2;
csec_belt2=A.*cprim2;

