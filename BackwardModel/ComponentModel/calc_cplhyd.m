% Function: calc_cplhyd
% fonction de calcul des conditions de fonctionnement du coupleur hydraulique
%
% Copyright IFSTTAR LTE 1999-2011
%
% Objet:
% Cette fonction calcul les conditions de fonctionnement du coupleur
% hydraulique en premiere vitesse
%
% etat_cplhyd : 1 mode couple
% etat_cplhyd : NaN le moteur ne permet pas de satisfaire les conditions
% demandees en mode coupleur.
% etat_cplhyd : 0 mode ponte
%
% primaire: cote moteur thermqiue
% secondaire : cote boite de vitesse
%
% En mode coupleur, la vitesse mini est saturee au ralenti moteur pour des
% soucis de stabilite. La saturation au regime maxi du moteur en apporte
% encore, mais perturbe le calcul de la trajectoire de vitesse veh optimale.
%
% 28-08-2011 (BJ) : version 1
% 12_09_2012 (BJ) : rajout des rapports imposes
% 05_2015 Ajout des rapports de boite optimise et fonctionnement avec
% matrice 3D pour optimisation trajectoire (Feli)

function [ERR,cprim,wprim,dwprim,etat_cplhyd]=calc_cplhyd(ERR,VD,csec,wsec,dwsec,elhyb)

% en mode pontage, couples et vitesses sont egales
cprim=csec;
wprim=wsec;
dwprim=dwsec;

etat_cplhyd=zeros(size(csec));

if VD.CYCL.ntypcin==3
    % Rapports imposes
    % Vecteurs de tailles length(VD.CYCL.temps)
    
    % Arret en premiere
    ind_arret_premiere=find(VD.CYCL.vitesse==0 & VD.CYCL.rappvit==1);
    etat_cplhyd(ind_arret_premiere)=1;
    wprim(ind_arret_premiere)=VD.MOTH.ral; % depend de elhyb ?
    dwprim(ind_arret_premiere)=0; % on se fixe une valeur d'acceleration angulaire
    rv=0;
    lambda=min(max(VD.CONV.lambda),max(0,interp1(VD.CONV.rappvit_Ce,VD.CONV.lambda,rv)));
    mu=min(max(VD.CONV.mu),max(0,interp1(VD.CONV.rappvit_Cs,VD.CONV.mu,rv)));
    % Couple sur l etage d entree (pompe) du convertisseur
    cprim(ind_arret_premiere)=lambda.*wprim(ind_arret_premiere).^2;
    
    % Arret, point mort
    ind_arret_PM=find(VD.CYCL.vitesse==0 & VD.CYCL.rappvit==0);
    etat_cplhyd(ind_arret_PM)=0;
    wprim(ind_arret_PM)=VD.MOTH.ral; % depend de elhyb ?
    dwprim(ind_arret_PM)=0;
    cprim(ind_arret_PM)=0;
    
    % Arret, hors PM et premiere
    ind_arret_impossible=find(VD.CYCL.vitesse==0 & VD.CYCL.rappvit>1);
    etat_cplhyd(ind_arret_impossible)=0;
    wprim(ind_arret_impossible)=NaN;
    dwprim(ind_arret_impossible)=0;
    cprim(ind_arret_impossible)=0;
    
    % vitesse non nulle, rapport impose en premiere
    ind_vit_premiere=find(VD.CYCL.vitesse~=0 & VD.CYCL.rappvit==1);
    rapp=csec(ind_vit_premiere)./(wsec(ind_vit_premiere).^2);
    rapp(isnan(rapp))=0;
    rapp=max(0,rapp);
    % Calcul du rapport de vitesse
    [~,rv]=calc_rv(VD,rapp);
    wprim(ind_vit_premiere)=wsec(ind_vit_premiere)./rv;
    dwprim(ind_vit_premiere)=dwsec(ind_vit_premiere)./rv;
    lambda=min(max(VD.CONV.lambda),max(0,interp1(VD.CONV.rappvit_Ce,VD.CONV.lambda,rv)));
    % Couple sur l etage d entree (pompe) du convertisseur
    cprim(ind_vit_premiere)=lambda.*wprim(ind_vit_premiere).^2;
    %wprim(ind_vit_premiere)=min(wprim(ind_vit_premiere),VD.MOTH.wmt_maxi);
    wprim(ind_vit_premiere)=max(wprim(ind_vit_premiere),VD.MOTH.ral);
    
    % vitesse non nulle, rapport Point Mort
    ind_vit_PM_traction=find(VD.CYCL.vitesse~=0 & VD.CYCL.rappvit==0 & csec>0);
    etat_cplhyd(ind_vit_PM_traction)=0;
    wprim(ind_vit_PM_traction)=NaN; % Il faut que le moteur fournisse du couple
    dwprim(ind_vit_PM_traction)=0;
    cprim(ind_vit_PM_traction)=0;
    ind_vit_PM_recup=find(VD.CYCL.vitesse~=0 & VD.CYCL.rappvit==0 & csec<=0);
    etat_cplhyd(ind_vit_PM_recup)=0;
    wprim(ind_vit_PM_recup)=VD.MOTH.ral;
    dwprim(ind_vit_PM_recup)=0;
    cprim(ind_vit_PM_recup)=0;
    
    % vitesse non nulle, hors PM et mode coupleur
    ind_vit_Rapport=find(VD.CYCL.vitesse~=0 & VD.CYCL.rappvit>1);
    % En roulage, rapports imposes hors coupleur
    etat_cplhyd(ind_vit_Rapport)=0;
    
    wprim(ind_vit_Rapport)=wsec(ind_vit_Rapport);
    dwprim(ind_vit_Rapport)=dwsec(ind_vit_Rapport);
    cprim(ind_vit_Rapport)=csec(ind_vit_Rapport);
    
else
    % Rapports libres
    indice_cplhyd=2; % premier element du vecteur: PM puis les rapports de 1 a n
    if isvector(csec)
        % vecteurs de taille nbrapp+1
        if VD.CYCL.vitesse==0 % VD.CYCL.vitesse est un scalaire...
            %%%%%%%%%%%%%%%% Vehicule a l'arret %%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%% Mode coupleur %%%%%%%%%%%%%%%%%
            % En premiere, donc mode coupleur
            etat_cplhyd(indice_cplhyd)=1; % Mode coupleur en premiere
            wprim(indice_cplhyd)=VD.MOTH.ral; % depend de elhyb ?
            dwprim(indice_cplhyd)=0; % on se fixe une valeur d'acceleration angulaire
            rv=0;
            lambda=min(max(VD.CONV.lambda),max(0,interp1(VD.CONV.rappvit_Ce,VD.CONV.lambda,rv)));
            % Couple sur l etage d entree (pompe) du convertisseur
            cprim(indice_cplhyd)=lambda.*wprim(indice_cplhyd).^2;
            %%%%%%%%%%%%%%%%%%% Point Mort %%%%%%%%%%%%%%%%%%
            % Point Mort
            etat_cplhyd(1)=0;
            wprim(1)=VD.MOTH.ral; % depend de elhyb
            dwprim(1)=0;
            cprim(1)=0;
            %%%%%%%%%%%%%% Rapports superieurs %%%%%%%%%%%%%%%%%%%%%%%
            % rapports superieur
            etat_cplhyd(indice_cplhyd+1:end)=0;
            wprim(indice_cplhyd+1:end)=NaN;
            dwprim(indice_cplhyd+1:end)=0;
            cprim(indice_cplhyd+1:end)=csec(indice_cplhyd+1:end);                        
        else
            %%%%%%%%%%%%%% Vitesse non nulle %%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%% Mode coupleur %%%%%%%%%%%%%%%%%%%%
            rapp=csec(indice_cplhyd)./(wsec(indice_cplhyd).^2);
            rapp(isnan(rapp))=0;
            rapp=max(0,rapp);
            % Calcul du rapport de vitesse
            [~,rv]=calc_rv(VD,rapp);
            wprim(indice_cplhyd)=wsec(indice_cplhyd)./rv;
            dwprim(indice_cplhyd)=dwsec(indice_cplhyd)./rv;
            lambda=min(max(VD.CONV.lambda),max(0,interp1(VD.CONV.rappvit_Ce,VD.CONV.lambda,rv)));
            % Couple sur l etage d entree (pompe) du convertisseur
            cprim(indice_cplhyd)=lambda.*wprim(indice_cplhyd).^2;
            % On sature la vitesse de rotation de l'arbre primaire a la vitesse de
            % ralenti moteur en premiere
            wprim(indice_cplhyd)=max(wprim(indice_cplhyd),VD.MOTH.ral);
            
            % On sature la vitesse de rotation de l'arbre primaire a la vitesse
            % maximum du moteur en premiere
            %wprim(indice_cplhyd)=min(wprim(indice_cplhyd),VD.MOTH.wmt_maxi);
            %%%%%%%%%%%%%%%%%%% Point Mort %%%%%%%%%%%%%%%%%%
            % Point Mort
            etat_cplhyd(1)=0;
            wprim(1)=max(VD.MOTH.ral,wsec(1));
            dwprim(1)=dwsec(1);
            cprim(1)=csec(1);
            
            %%%%%%%%%%%%%%% En roulage, rapports superieurs %%%%%%%%%%%%%%%%%%
            etat_cplhyd(indice_cplhyd+1:end)=0; % Coupleur ponte
            wprim(indice_cplhyd+1:end)=wsec(indice_cplhyd+1:end);
            dwprim(indice_cplhyd+1:end)=dwsec(indice_cplhyd+1:end);
            cprim(indice_cplhyd+1:end)=csec(indice_cplhyd+1:end);
       end
        
        
%         % On sature la vitesse de l'arbre primaire du coupleur lorsque la
%         % variable elhyb vaut 1 au point mort (indetermination).
%         if wprim(1)<VD.MOTH.ral && elhyb(1)==1
%             wprim(1)=VD.MOTH.ral;
%         end
        
    elseif ismatrix(csec)
        % size(nbrapp+1,length(tsim))
        
        %%%%%%%%%%%%%%%% Vehicule a l'arret %%%%%%%%%%%%%%%%%%
        [indice]=find(VD.CYCL.vitesse==0);
        kindice = sub2ind(size(wsec),ones(size(indice))*indice_cplhyd, indice);
        %%%%%%%%%%%%%%%%%%ù En premiere, donc mode coupleur %%%%%%%%%%%%%%
        etat_cplhyd(kindice)=1; % Mode coupleur en premiere
        wprim(kindice)=VD.MOTH.ral; % depend de elhyb ?
        dwprim(kindice)=0; % on se fixe une valeur d'acceleration angulaire
        rv=0;
        lambda=min(max(VD.CONV.lambda),max(0,interp1(VD.CONV.rappvit_Ce,VD.CONV.lambda,rv)));
        % Couple sur l etage d entree (pompe) du convertisseur
        cprim(kindice)=lambda.*wprim(kindice).^2;
%         % On sature la vitesse de rotation de l'arbre primaire a la vitesse de
%         % ralenti moteur en premiere
%         wprim(kindice)=max(wprim(kindice),VD.MOTH.ral);
%         
%         % On sature la vitesse de rotation de l'arbre primaire a la vitesse
%         % maximum du moteur en premiere
%         wprim(kindice)=min(wprim(kindice),VD.MOTH.wmt_maxi);

        %%%%%%%%%%%%%%%%%%% Point Mort %%%%%%%%%%%%%%%%%%
        [indice]=find(VD.CYCL.vitesse==0);
        kindice = sub2ind(size(wsec),ones(size(indice)), indice);
        % Point Mort
        etat_cplhyd(kindice)=0;
        wprim(kindice)=VD.MOTH.ral; % depend de elhyb
        dwprim(kindice)=0;
        cprim(kindice)=0;
        
%         % On sature la vitesse de l'arbre primaire du coupleur lorsque la
%         % variable elhyb vaut 1 au point mort (indetermination).
%         if wprim(kindice)<VD.MOTH.ral & elhyb(kindice)==1
%             !! wprim(kindice)=VD.MOTH.ral;
%         end
        %%%%%%%%%%%%%% Rapports superieurs %%%%%%%%%%%%%%%%%%%%%%%
        kindice=[];
        for ii=indice_cplhyd+1:length(wsec(:,1))
            [indice]=find(VD.CYCL.vitesse==0);
            ijindice = sub2ind(size(wsec),ones(size(indice))*ii, indice);
            kindice=[kindice ijindice];
        end
        % rapports superieur
        etat_cplhyd(kindice)=0;
        wprim(kindice)=wsec(kindice);
        dwprim(kindice)=dwsec(kindice);
        cprim(kindice)=csec(kindice);
        
        %%%%%%%%%%%%%% Vitesse non nulle %%%%%%%%%%%%%%%%%%%%%%%%                
        [indice]=find(VD.CYCL.vitesse~=0);
        kindice = sub2ind(size(wsec),ones(size(indice))*indice_cplhyd, indice);
        
        %%%%%%%%%%%%%%%%%% Mode coupleur %%%%%%%%%%%%%%%%%
        rapp=csec(kindice)./(wsec(kindice).^2);
        rapp(isnan(rapp))=0;
        rapp=max(0,rapp);
        % Calcul du rapport de vitesse
        [~,rv]=calc_rv(VD,rapp);
        wprim(kindice)=wsec(kindice)./rv;
        dwprim(kindice)=dwsec(kindice)./rv;
        lambda=min(max(VD.CONV.lambda),max(0,interp1(VD.CONV.rappvit_Ce,VD.CONV.lambda,rv)));
        % Couple sur l etage d entree (pompe) du convertisseur
        cprim(kindice)=lambda.*wprim(kindice).^2;

        % On sature la vitesse de rotation de l'arbre primaire a la vitesse de
        % ralenti moteur en premiere
        wprim(kindice)=max(wprim(kindice),VD.MOTH.ral);
        
        % On sature la vitesse de rotation de l'arbre primaire a la vitesse
        % maximum du moteur en premiere
        %wprim(kindice)= min(wprim(kindice),VD.MOTH.wmt_maxi); 
        % On retrouve alors les cas test de la revision 143 de gestion_energie
        
        %%%%%%%%%%%%%%%%%%%%%%% Point Mort %%%%%%%%%%%%%%%%%%%%%ù
        [indice]=find(VD.CYCL.vitesse~=0);
        kindice = sub2ind(size(wsec),ones(size(indice)), indice);
        etat_cplhyd(kindice)=0;
        wprim(kindice)=max(VD.MOTH.ral,wsec(kindice)); 
        dwprim(kindice)=dwsec(kindice);
        cprim(kindice)=csec(kindice);
        
        %%%%%%%%%%%%%% Rapports superieurs %%%%%%%%%%%%%%%%%%%%%%%
        kindice=[];
        for ii=indice_cplhyd+1:length(wsec(:,1))
            [indice]=find(VD.CYCL.vitesse==0);
            ijindice = sub2ind(size(wsec),ones(size(indice))*ii, indice);
            kindice=[kindice ijindice];
        end
        etat_cplhyd(kindice)=0;
        wprim(kindice)=wsec(kindice);
        dwprim(kindice)=dwsec(kindice);
        cprim(kindice)=csec(kindice);

   else
        % size(length(tsim),length(tsim),nbrapp+1)
        
        %%%%%%%%%%%%%%%% Vehicule a l'arret %%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%% Mode coupleur %%%%%%%%%%%%%%%%%
        [indice,jindice]=find(VD.CYCL.vitesse(:,:)==0);
        kindice = sub2ind(size(wsec),indice,jindice,repmat(indice_cplhyd,size(indice)));
        % En premiere, donc mode coupleur
        etat_cplhyd(kindice)=1; % Mode coupleur en premiere
        wprim(kindice)=VD.MOTH.ral; % depend de elhyb ?
        dwprim(kindice)=0; % on se fixe une valeur d'acceleration angulaire
        rv=0;
        lambda=min(max(VD.CONV.lambda),max(0,interp1(VD.CONV.rappvit_Ce,VD.CONV.lambda,rv)));
        % Couple sur l etage d entree (pompe) du convertisseur
        cprim(kindice)=lambda.*wprim(kindice).^2;
         % On sature la vitesse de rotation de l'arbre primaire a la vitesse de
         % ralenti moteur en premiere
         wprim(kindice)=max(wprim(kindice),VD.MOTH.ral);
         
         % On sature la vitesse de rotation de l'arbre primaire a la vitesse
         % maximum du moteur en premiere
         %wprim(kindice)=min(wprim(kindice),VD.MOTH.wmt_maxi);

        %%%%%%%%%%%%%%%%%%% Point Mort %%%%%%%%%%%%%%%%%%
        [indice,jindice]=find(VD.CYCL.vitesse(:,:)==0);
        kindice = sub2ind(size(wsec),indice,jindice,ones(size(indice)));
        % Point Mort
        etat_cplhyd(kindice)=0;
        wprim(kindice)=VD.MOTH.ral; % depend de elhyb
        dwprim(kindice)=0;
        cprim(kindice)=0;
        
%         % On sature la vitesse de l'arbre primaire du coupleur lorsque la
%         % variable elhyb vaut 1 au point mort (indetermination).
%         if wprim(kindice)<VD.MOTH.ral & elhyb(kindice)==1
%             !! wprim(kindice)=VD.MOTH.ral;
%         end
        
        %%%%%%%%%%%%%% Rapports superieurs %%%%%%%%%%%%%%%%%%%%%%%
        kindice=[];
        for ii=indice_cplhyd+1:length(wsec(1,1,:))
            [indice,jindice]=find(VD.CYCL.vitesse(:,:)==0);
            ijindice = sub2ind(size(wsec),indice,jindice,repmat(ii,size(indice)));
            kindice=[kindice ijindice];
        end
        % rapports superieur
        etat_cplhyd(kindice)=0;
        wprim(kindice)=wsec(kindice);
        dwprim(kindice)=dwsec(kindice);
        cprim(kindice)=csec(kindice);
        
        %%%%%%%%%%%%%% Vitesse non nulle %%%%%%%%%%%%%%%%%%%%%%%%
        [indice,jindice]=find(VD.CYCL.vitesse(:,:)~=0);
        kindice = sub2ind(size(wsec),indice,jindice,repmat(indice_cplhyd,size(indice)));
        
        %%%%%%%%%%%% Mode coupleur %%%%%%%%%%%%%%%%%%%%
        rapp=csec(kindice)./(wsec(kindice).^2);
        rapp(isnan(rapp))=0;
        rapp=max(0,rapp);
        % Calcul du rapport de vitesse
        [~,rv]=calc_rv(VD,rapp);
        wprim(kindice)=wsec(kindice)./rv;
        dwprim(kindice)=dwsec(kindice)./rv;
        lambda=min(max(VD.CONV.lambda),max(0,interp1(VD.CONV.rappvit_Ce,VD.CONV.lambda,rv)));
        % Couple sur l etage d entree (pompe) du convertisseur
        cprim(kindice)=lambda.*wprim(kindice).^2;

        % On sature la vitesse de rotation de l'arbre primaire a la vitesse de
        % ralenti moteur en premiere
        wprim(kindice)=max(wprim(kindice),VD.MOTH.ral);
        
        % On sature la vitesse de rotation de l'arbre primaire a la vitesse
        % maximum du moteur en premiere
        %wprim(kindice)= min(wprim(kindice),VD.MOTH.wmt_maxi);

        %%%%%%%%%%%%%%%%%%% Point Mort %%%%%%%%%%%%%%%%%%
        [indice,jindice]=find(VD.CYCL.vitesse(:,:)~=0);
        kindice = sub2ind(size(wsec),indice,jindice,ones(size(indice)));
        % Point Mort
        etat_cplhyd(kindice)=0;
        
        wprim(kindice)=max(VD.MOTH.ral,wsec(kindice));
        dwprim(kindice)=dwsec(kindice);
        cprim(kindice)=csec(kindice);
        
%         % On sature la vitesse de l'arbre primaire du coupleur lorsque la
%         % variable elhyb vaut 1 au point mort (indetermination).
%         if wprim(kindice)<VD.MOTH.ral & elhyb(kindice)==1
%            !!  wprim(kindice)=VD.MOTH.ral;
%         end

        %%%%%%%%%%%%%%% En roulage, rapports superieurs %%%%%%%%%%%%%%%%%%
        kindice=[];
        for ii=indice_cplhyd+1:length(wsec(1,1,:))
            [indice,jindice]=find(VD.CYCL.vitesse(:,:)~=0);
            ijindice = sub2ind(size(wsec),indice,jindice,repmat(ii,size(indice)));
            kindice=[kindice ijindice];
        end
        etat_cplhyd(kindice)=0;
        wprim(kindice)=wsec(kindice);
        dwprim(kindice)=dwsec(kindice);
        cprim(kindice)=csec(kindice);
        
    end
      
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Fonction de calcul du rapport de vitesse
% lambda*mu*wprim2=Csec, soit:
% lambda*mu/rv2=Csec/wsec**2
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [erreur,rv]=calc_rv(VD,rapp)

erreur=0;

rv1d=0:0.1:1;
lambda=interp1(VD.CONV.rappvit_Ce,VD.CONV.lambda,rv1d,'linear','extrap');
mu=interp1(VD.CONV.rappvit_Cs,VD.CONV.mu,rv1d,'linear','extrap');
produit=lambda.*mu./(rv1d.^2);
produit(isinf(produit))=2*max(produit(~isinf(produit)));
rv=interp1(produit,rv1d,rapp,'linear','extrap');
%rv=max(0,min(rv,1));
end
