% function 
%
% C Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Calcul des perfos maxi du moteur thermique au regard de ce qui est demande par le
% cycle de conduite
% Arguments appel :
% Arguments de sortie :
%

function [ERR, cmt_max, cmt_min]=calc_enveloppe_mt(ERR, MOTH, wmt)

switch MOTH.ntypmoth
    case {10, 2}
        cmt_max = interp1(MOTH.wmt_max,MOTH.cmt_max,wmt);
        cmt_min = interp1(MOTH.wmt_max,MOTH.cmt_min,wmt);
    case 3
        N = wmt*30/pi;
        cmt_min = -(MOTH.PMF_cst + MOTH.PMF_N*N)*MOTH.Vd/(MOTH.Rev*2*pi);
        cmt_max =  (MOTH.Tmax_N2*N.^2 + MOTH.Tmax_N*N + MOTH.Tmax_cst)*MOTH.Tmax/MOTH.Tmax_Tref;
    otherwise
        chaine='calc_enveloppe_mt : Unknown engine type' ;
        ERR=MException('BackwardModel:calc_enveloppe_mt',chaine);
        cmt_min = NaN*ones(size(wmt));
        cmt_max = NaN*ones(size(wmt));
end

        
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
