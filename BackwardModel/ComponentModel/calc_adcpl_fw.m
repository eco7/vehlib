% Function [ERR,wprim1,dwprim1,wprim2,dwprim2,csec,Jsec]=calc_adcpl_fw(ERR,ADCPL,cprim1,J1,cprim2,J2,wsec,dwsec)
% 
% © Copyright IFSTTAR LTE 1999-2011 
%
% objet : simulation du bloc coupleur en forward
% Cette fonction calcul le couple, la vitesse, l'acceleration et l'inertie
% ramenee au secondaire. Si on connait les inertie precedente il n'est pas
% necessaire de connaitre les accelerations. et visversa.
%
% ADCPL.MOTH ---(cprim1,J1)-->|----->(Jsec,csec) 
%                       ^
%                       |
%                      red
% MELEC---(cprim2,J2)-->|
%
% Imput argments:
% 
% ADCPL  :  Structure donnée du coupleur
% cprim1 :  couple primaire 1
% J1     :  inertie ramenee au primaire
% cprim  :  couple primaire 2
% J2     :  inertie ramenee au primaire
% wsec   :  vitesse du secondaire 
% dwsec  :  derivee de la vitesse du secondaire
% 
% 
% Output arguments:
% 
% ERR   : erreur
% wprim1: vitesse du  primaire 1
% dwprim1: derive de la vitesse du  primaire 1
% wprim2: vitesse du  primaire 2
% dwprim2: derive de la vitesse du  primaire 2
% csec: couple r�sultant au secondaire
% Jsec: inertie ramenee au secondaire
%
% 15/02/2011(VR-EV): version 1 


function [ERR,wprim1,dwprim1,wprim2,dwprim2,csec,Jsec]=calc_adcpl_fw(ERR,ADCPL,cprim1,J1,cprim2,J2,wsec,dwsec)

kred=ADCPL.kred;
krend=ADCPL.rend;

A=ones(size(cprim2))*krend;

csec=cprim1+(cprim2.*(A.^sign(cprim2.*wsec)))*kred;  % selon le signe de la puissance de la machine elec, on divise 
                                                     % ou on multiplie pas krend

Jsec=J1+J2*(A.^sign(cprim2.*wsec))*kred^2; % le rendement n'intervient pas au carre

% Vitesse arbre primaire
wprim1=wsec;
wprim2=wsec*kred;

% Acceleration arbre primaire
dwprim1=dwsec;
dwprim2=dwsec*kred;

