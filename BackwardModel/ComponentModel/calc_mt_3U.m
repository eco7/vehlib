function [dcarb, Cmt] = calc_mt_3U(wmt_i,Phi,delta_AA,Padm,szD,VD)


if ~isequal(size(Phi),size(delta_AA))
    disp('ERROR')
end

Tamb = VD.INIT.Tamb+273;
lambda = 1./Phi;
N = wmt_i*30/pi;

%% Calcul du debit de carburant
% coefficient de remplissage
r_phi = repmat(VD.MOTH.remp_cst,szD);
r_phi(Phi>VD.MOTH.remp_Phi_cst) = interp1([VD.MOTH.remp_Phi_cst VD.MOTH.remp_Phi_max],[VD.MOTH.remp_cst VD.MOTH.remp_max],Phi(Phi>VD.MOTH.remp_Phi_cst),'linear','extrap');
remplissage = r_phi - VD.MOTH.remp_r_N*(VD.MOTH.remp_N_max-N).^2;

% temperature admission
T_adm = Tamb + VD.MOTH.Tadm_N*N + VD.MOTH.Tadm_cst;

dgaz = remplissage.*Padm*10^2*VD.MOTH.Vd*10^(-3).*N/(VD.MOTH.Rev*60)*VD.MOTH.M_gf./(VD.MOTH.R_gaz*T_adm); %kg/s
dcarb = dgaz*1000./(1+VD.MOTH.AStoechio./Phi); %g/s

%% Calcul du couple moteur
n_comb = VD.MOTH.n_c0 - VD.MOTH.n_cA./(VD.MOTH.n_cB + N);

%AA_0 = interp2(VD.MOTH.AA_Padm,VD.MOTH.AA_N,VD.MOTH.AA0_N_Padm',Padm,N);
n_AA = 1 - VD.MOTH.k_AA*delta_AA.^2;

i1 = lambda<VD.MOTH.lambda1;
i2 = lambda>=VD.MOTH.lambda1 & lambda<VD.MOTH.lambda2;
i3 = lambda>=VD.MOTH.lambda3 & lambda<VD.MOTH.lambda4;

n_rich(i1) = VD.MOTH.g1*lambda(i1) - VD.MOTH.g0;
n_rich(i2) = VD.MOTH.e1 + (1-VD.MOTH.e1)*sin((lambda(i2)-VD.MOTH.lambda1)/(1-VD.MOTH.lambda1));
n_rich(lambda>=VD.MOTH.lambda2)=1;
n_rich(i3) = VD.MOTH.b_lambda + VD.MOTH.a_lambda*lambda(i3);
n_rich(lambda>=VD.MOTH.lambda4)=0;

Cmt_f = (VD.MOTH.PMF_cst + VD.MOTH.PMF_N*N + VD.MOTH.PMF_N2*N.^2)*VD.MOTH.Vd/(2*pi*VD.MOTH.Rev);

Cmt = VD.MOTH.n_ind*n_comb.*n_rich.*n_AA...
    .*dcarb*VD.MOTH.pci./(N*pi/30)...
    - Cmt_f;

end
