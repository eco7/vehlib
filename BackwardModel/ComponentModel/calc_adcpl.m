% function [ERR,cprim1,wprim1,dwprim1,wprim2,dwprim2,cprim2]=calc_adcpl(ERR,ADCPL,csec,wsec,dwsec,cprim,J,sens);
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% calcul des couples vitesses et derives des primaires des coupleurs meca
% on entre les valeurs aux secondaires et le couple de l'un des deux
% primaires
% 1 est le primaire sans reduction
% 2 est le primaire avec reduction
%
% Arguments appel
% ERR : struct pour la gestion des erreurs qui seront gerees par les programmes appelant
% csec, wsec, dwsec : valeur au niveau du secondaire
% cprim : valeur de l'un des couples primaires
% J : Inertie propre ou ramenee ?
% ADCPL : structure de donnees du coupleur
% sens : 
% 1 : on fournie le couple primaire 2 (arbre sur lequel on as reduction)
% 2 : on fourni le couple primaire 1 (arbre sur lequel pas de reduction)
% 
% Arguments de sortie :
% ERR : 
% cprim1,wprim1,dwprim1,wprim2,dwprim2,cprim2 : veleur au niveau des primaires
% 
% 26-08-11 (EV): version 1 

function [ERR,cprim1,wprim1,dwprim1,wprim2,dwprim2,cprim2]=calc_adcpl(ERR,ADCPL,csec,wsec,dwsec,cprim,J,sens)
kred=ADCPL.kred;
krend=ADCPL.rend;
Csec_pertes=ADCPL.Csec_pert;
if nargin==7 | sens==1 % on fournie le couple primaire 2 (arbre sur lequel on as reduction)
    % Couple sur primaire1 suivant signe
    cprim2=cprim;
    
    A=ones(size(cprim2))*(kred.*krend);
    indice=find(cprim2<0);
    A(indice)=(kred./krend);
    
    B=ones(size(cprim2))*J;
    [Li,Co]=size(cprim2);
    cprim1=csec-cprim2.*A+B.*dwsec; % PAs sur de la gestion des inerties dans ce cas
    
    % Vitesse arbre primaire
    wprim1=wsec;
    wprim2=kred*wsec;
    
    % Acceleration arbre primaire
    dwprim1=dwsec;
    dwprim2=kred*dwsec;
    
elseif  sens==2 % on fourni le couple primaire 1 (arbre sur lequel pas de reduction)
    cprim1=cprim;
    % Vitesse arbre primaire
    wprim1=wsec;
    wprim2=kred*wsec;
    
    % Acceleration arbre primaire
    dwprim1=dwsec;
    dwprim2=kred*dwsec;
    
    A=ones(size(cprim))*krend;
    B=ones(size(cprim))*J;
    cprim2=(csec-cprim1+B.*dwsec)./(kred.*(A.^sign((csec-cprim1+B.*dwsec).*wprim2)));
end

