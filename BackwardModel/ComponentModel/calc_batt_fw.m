% function [ERR,Ubat,soc,ah,E,R,RdF]=calc_batt_fw(ERR,BATT,pas,Ibat,soc_p,ah_p)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul de la tension de la batterie à partir du courant
%
% Arguments appel
% ERR : struct pour la gestion des erreurs qui seront gerees par les programmes appelant
% BATT : structure vehlib pour renseigner l'architecture batterie
% pas : pas d'integration
% Ibat : courant batterie
% soc_p, ah_p : soc et ampere.heure à l'instant precedent 
% 
% Arguments de sortie :
% ERR : 
% Ubat,soc,ah,E,R :
% RdF : rendement faradic
% 
%
% 26-08-11 (EV) : version 1

function [ERR,Ubat,soc,ah,E,R,RdF,Ibat_max_calc,Ubat_max_calc,Ibat_min_calc,Ubat_min_calc]=calc_batt_fw(ERR,BATT,pas,Ibat,soc_p,ah_p)
%#eml
eml.extrinsic('MException')

% recherche des donnees pour T = 20 (EV :25-02-10)
if isfield(BATT,'tbat')
   [~,Li_T20]=min(abs(BATT.tbat-20));
elseif isfield(BATT,'Tbat_table')
   [~,Li_T20]=min(abs(BATT.Tbat_table-20));
else
    Li_T20=1;
end

E=interp1(BATT.dod_ocv,BATT.ocv(Li_T20,:),100-soc_p,'linear','extrap');
Ibat_max=interp1(BATT.DoD_Ibat_max,BATT.Ibat_max,100-soc_p,'linear','extrap');
Ibat_min=interp1(BATT.DoD_Ibat_min,BATT.Ibat_min,100-soc_p,'linear','extrap');
RdF=ones(size(Ibat));
RdF(Ibat<0)=BATT.RdFarad;

R=ones(size(Ibat));

if isscalar(soc_p) & ~isscalar(Ibat)
    soc_p=soc_p*ones(size(Ibat));
end
if BATT.ntypbat==1
    Rd=interp1(BATT.dod,BATT.Rd(Li_T20,:),100-soc_p,'linear','extrap');
    Rc=interp1(BATT.dod,BATT.Rc(Li_T20,:),100-soc_p,'linear','extrap');
elseif BATT.ntypbat==2
    Rd=interp1(BATT.dod_ocv,BATT.rd(Li_T20,:),100-soc_p,'linear','extrap');
    Rc=interp1(BATT.dod_ocv,BATT.rc(Li_T20,:),100-soc_p,'linear','extrap');
elseif BATT.ntypbat==3
    Rd=interp1(BATT.dod,BATT.Rd(Li_T20,:),100-soc_p,'linear','extrap');
    Rc=interp1(BATT.dod,BATT.Rc(Li_T20,:),100-soc_p,'linear','extrap');
end

R(Ibat>=0)=Rd(Ibat>=0);
R(Ibat<0)=Rc(Ibat<0);

Ibat_max_ubatmin=(E-BATT.Ubat_min)./Rd;
Ibat_max_calc=min(Ibat_max_ubatmin,Ibat_max);

Ibat_min_ubatmax=(E-BATT.Ubat_max)./Rc;
Ibat_min_calc=max(Ibat_min_ubatmax,Ibat_min);


R=R*BATT.Nblocser/BATT.Nbranchepar;
Rc=Rc*BATT.Nblocser/BATT.Nbranchepar;
Rd=Rd*BATT.Nblocser/BATT.Nbranchepar;
E=E*BATT.Nblocser;
Ubat=E-R.*Ibat;

Ibat_max_calc=Ibat_max_calc*BATT.Nbranchepar;
Ubat_max_calc=E-Rd.*Ibat_max_calc;
Ibat_min_calc=Ibat_min_calc*BATT.Nbranchepar;
Ubat_min_calc=E-Rc.*Ibat_min_calc;


soc=soc_p-(RdF.*Ibat/BATT.Nbranchepar/3600/BATT.Cahbat_nom*100).*pas; % rajouter eventuellement limitation SOC batterie
ah=ah_p+(RdF.*Ibat/3600).*pas;


[Li,Co]=size(Ibat);
if sum(sum(isnan(Ibat)))==Li*Co
    chaine=' Tout les elements de Ibat = NaN';
    ERR=MException('BackwardModel:calc_batt_fw',chaine);
end
