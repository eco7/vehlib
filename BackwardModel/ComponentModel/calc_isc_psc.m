% function [ERR,isc_cible,ibat,u0sc,ux,u0bat,soc,usc,ubat,pbat,rbat,ah] = calc_isc_psc2(VD,param,psc_cible,u0sc,j,ux,soc,pres,ah)
% © Copyright IFSTTAR LTE 1999-2011
%
% Objet:
%
% Arguments appel :
% Arguments de sortie :
%
% 12-03-2012 (AS) : ajout DT/C et DT=0 pour j=1
% 26-08-2011 (EV) : version 1

function [ERR,isc_cible,ibat,u0sc,ux,u0bat,soc,usc,ubat,pbat,rbat,ah] = calc_isc_psc(VD,param,psc_cible,u0sc,j,ux,soc,pres,ah)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
Ndec=50;
pas_dec=psc_cible(j)/Ndec;
DT=param.pas_temps;
ERR=[];

Rsc=VD.SC.Rs/VD.SC.Nbranchepar*VD.SC.Nblocser; % R totale equivalente
Rx=VD.SC.Rx/VD.SC.Nbranchepar*VD.SC.Nblocser; % Rx totale equivalente
Taux=VD.SC.Rx*VD.SC.Cx;
C=VD.SC.C*VD.SC.Nbranchepar/VD.SC.Nblocser; % C totale equivalente



if psc_cible(j)~=0
    psc_cible_p=psc_cible(j):-pas_dec:0; % jusqu'ou fait on decroitre la puissance ?
else
    psc_cible_p=0;
end

if j>1
    delta_p=(u0sc(j-1)-ux(j-1)*exp(-DT/Taux)).^2-4*psc_cible_p*(DT/C+Rsc+Rx*(1-exp(-DT/Taux)));
    delta_p(delta_p<0)=nan;
    if sum(isnan(delta_p))==length(delta_p)
        chaine=strcat('delta <0');
        ERR=MException('BackwardModel:calc_isc_psc',chaine);        
    end
    isc_cible_p=( u0sc(j-1)-ux(j-1)*exp(-DT/Taux)-sqrt(delta_p))./ (2*(DT/C+Rsc+Rx*(1-exp(-DT/Taux))));
else
    delta_p=(u0sc(1)-ux(1)).^2-4*psc_cible_p*(Rsc);
    delta_p(delta_p<0)=nan;
    if sum(isnan(delta_p))==length(delta_p)
        chaine=strcat('delta <0');
        ERR=MException('BackwardModel:calc_isc_psc',chaine);        
    end
    isc_cible_p=( u0sc(1)-ux(1)-sqrt(delta_p))./ (2*Rsc);
end

isc_p=isc_cible_p;

if j>1
    [ERR,usc_p,u0sc_p,ux_p]=calc_sc(ERR,VD.SC,j,param.pas_temps,isc_p,u0sc(j-1),ux(j-1));
else
    [ERR,usc_p,u0sc_p,ux_p]=calc_sc(ERR,VD.SC,j,param.pas_temps,isc_p,u0sc(1),ux(1));
end

psc_p=isc_p.*usc_p;

if isfield(VD,'DCDC_2') && ~isempty(VD.DCDC_2)
    % Calcul en aval des super capacites
    [ERR,psc_res_p]=calc_dcdc(1,ERR,VD.DCDC_2,usc_p,psc_p);
else
    psc_res_p=psc_p;
end

% Calculs batterie
if ~isempty(VD.BATT)
    pbat_res_p=pres(j)-psc_res_p;
    
    % Conditions de fonctionnement batterie
    if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
        [ERR,pbat_p]=calc_dcdc(2,ERR,VD.DCDC_1,0,pbat_res_p);
    else
        pbat_p=pbat_res_p;
    end
    
    if j>1
        [ERR,ibat_p,ubat_p,soc_p,ah_p,u0bat_p,rbat_p,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_p,soc(j-1),ah(j-1),pres(j),0);
        if isempty(ERR)
            ah(j)=ah_p(find(~isnan(ibat_p),1));
            soc(j)=soc_p(find(~isnan(ibat_p),1));
        else
            ah(j)=nan;
            soc(j)=nan;
        end
    else
        [ERR,ibat_p,ubat_p,~,~,u0bat_p,rbat_p,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_p,soc(1),ah(1),pres(j),0);
    end
    
    if isempty(ERR)
        ibat=ibat_p(find(~isnan(ibat_p),1));
        ubat=ubat_p(find(~isnan(ibat_p),1));
        u0bat=u0bat_p;
        rbat=rbat_p(find(~isnan(ibat_p),1));
        pbat=pbat_p(find(~isnan(ibat_p),1));
    
        isc_cible=isc_p(find(~isnan(ibat_p),1));
        u0sc(j)=u0sc_p(find(~isnan(ibat_p),1));
        ux(j)=ux_p(find(~isnan(ibat_p),1));
        usc=usc_p(find(~isnan(ibat_p),1));
    else
        ibat=ibat_p(1);
        ubat=ubat_p(1);
        u0bat=u0bat_p;
        rbat=rbat_p(1);
        pbat=pbat_p(1);
    
        isc_cible=isc_p(find(~isnan(usc_p),1));
        u0sc(j)=u0sc_p(find(~isnan(usc_p),1));
        ux(j)=ux_p(find(~isnan(usc_p),1));
        usc=usc_p(find(~isnan(usc_p),1));
    end
    
else
    ibat=nan;
    ubat=nan;
    u0bat=nan;
    rbat=nan;
    soc=nan;
    pbat=nan;
    
    isc_cible=isc_p(find(~isnan(usc_p),1));
    u0sc(j)=u0sc_p(find(~isnan(usc_p),1));
    ux(j)=ux_p(find(~isnan(usc_p),1));
    usc=usc_p(find(~isnan(usc_p),1));
    
end
