function [ERR,VD,res]=calc_essieu(ERR,vehlib,param,VD)

% Tronc commun a toutes les architectures
% Calcul de la cinematique et des efforts a la roue
% Calcul du reducteur de pont
% En sortie : conditions sur le primaire de reducteur (amont du pont)
% La structure res contient aussi toutes les variables intermediaires :
% - cinematique
% - bilan des masses et inerties
% - bilan des forces et couple a la roue
% Pas d'hypothese sur la repartition du freinage avant-arriere (total renvoye)

res = struct([]);

% Calcul cinematiques complementaires
% Avec re-echantillonnage eventuel
if ~isfield(param,'pas_temps') || param.pas_temps==0
    param.pas_temps=1;
end
[ERR,VD,~,res]=calc_cinemat(ERR,VD,param,res);
if ~isempty(ERR), return, end

% Calcul masse vehicule et inertie
[ERR,res(1).masse,res(1).Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
if ~isempty(ERR), return, end

% Calcul des efforts a la roue
[ERR,res] = calc_efforts_vehicule_new(ERR,VD,res);
if ~isempty(ERR), return, end

% Dans certain cycle CIN_ARTURB_BV_classe3 par exemple, il peut arriver
% que croue>0 alors que on est au point mort, impossible avec des archi avec
% BV on met alors le couple a zero
% TODO : reflechir a enclancher un rapport au lieu de supprimer le couple

if VD.CYCL.ntypcin==3
    ind_croue_pm=find(res.croue>0 & VD.CYCL.rappvit==0);
    if ~isempty(ind_croue_pm)
        chaine=strcat('in some point, a positive wheel torque exist with neutral gear box position \n corresponding time :',num2str(ind_croue_pm),'\n wheel torque fixe to zero in these cases');
        warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine);
        res.croue(ind_croue_pm)=0;
    end
end

% Connexion
res.csec_red=res.croue;
res.wsec_red=res.wroue;
res.dwsec_red=res.dwroue;

% Calcul des conditions en amont du reducteur de pont
[ERR,res.cprim_red,res.wprim_red,res.dwprim_red]= ...
    calc_red(ERR,VD.RED,res.csec_red,res.wsec_red,res.dwsec_red,0);
if ~isempty(ERR), return, end

% Connexion
res.csec_bv=res.cprim_red;
res.wsec_bv=res.wprim_red;
res.dwsec_bv=res.dwprim_red;

end