function [ERR,res]=calc_efforts_vehicule_new(ERR,VD,res)

% Nouvelle version 05/09/2017
% Simplifie l'appel et le retour en introduisant la stucture res
% Cette structure fournit :
% - en entree la masse et l'inertie equivalente Jveh
% - en sortie les efforts de resistance a l'avancement et les conditions a la roue : croue, wroue, dwroue 

if VD.CYCL.ntyppente==2 | VD.CYCL.ntyppente==3
     res.penteFpk=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,VD.CYCL.distance,'linear',0); % reechantillonage pente fonction distance calculee. 
     res.Fpente=res.masse.*VD.VEHI.Gpes.*sin(atan(res.penteFpk/100));
    if VD.CYCL.distance(end)>VD.CYCL.PKpente(end)
        %warning('BackwardModel:calc_efforts_vehicule : cycle mal defini, PKpente trop court')
    end
     
elseif VD.CYCL.ntyppente==0
    res.Fpente=res.masse.*VD.VEHI.Gpes.*sin(atan(VD.CYCL.pente/100));
else
    chaine=' modele de pente non reconnu ';
    ERR=MException('BackwardModel:calc_efforts_vehicule',chaine);
end

if VD.VEHI.ntypveh==1
    res.Froul=res.masse.*VD.VEHI.Gpes.*(VD.VEHI.a+VD.VEHI.b*VD.CYCL.vitesse.^2);
    res.Faero=(VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx/2)*VD.CYCL.vitesse.^2;   
    res.Fres=res.Froul+res.Faero;
elseif VD.VEHI.ntypveh==2
    res.Force1=VD.VEHI.fcst.*(VD.CYCL.vitesse>0.01);
    res.Force2=VD.VEHI.fvit1*VD.CYCL.vitesse.*(VD.CYCL.vitesse>0.01);
    res.Force3=VD.VEHI.fvit2*(VD.CYCL.vitesse.^2).*(VD.CYCL.vitesse>0.01);
    res.Fres=res.Force1+res.Force2+res.Force3;
elseif VD.VEHI.ntypveh==3
    res.Froul=res.masse.*VD.VEHI.Gpes.*(VD.VEHI.a0 + VD.VEHI.a1*VD.CYCL.vitesse + VD.VEHI.a2*VD.CYCL.vitesse.^2);
    res.Faero=(VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx/2)*VD.CYCL.vitesse.^2;   
    res.Fres=res.Froul+res.Faero;
end

% La force de resistance a l'avancement est nulle a l'arret du vehicule, et
% opposee a la vitesse du vehicule
res.Fres=res.Fres.*sign(VD.CYCL.vitesse);
res.Fres=res.Fres+res.Fpente;
 
cres=res.Fres*VD.VEHI.Rpneu;

res.wroue=VD.CYCL.vitesse/VD.VEHI.Rpneu;
res.dwroue=VD.CYCL.accel/VD.VEHI.Rpneu;

res.croue=cres+res.Jveh.*VD.CYCL.accel/VD.VEHI.Rpneu;

return
