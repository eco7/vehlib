% function [ERR,csec_belt1,csec_belt2]=calc_accm2(wsec,dwsec,J)
%
% © Copyright IFSTTAR LTE 1999-2011
% Accessoires mecaniques de type 2
%
function [ERR,csec_belt1,csec_belt2]=calc_accm2(VD,wsec,dwsec,J)

ERR=[];

if VD.ACC.ntypacc~=2 && VD.ACC.ntypacc~=4
    ERR=MException('BackwardModel:calc_accm2',strcat('Type d''accessoires non reconnues : ',num2str(VD.ACC.ntypacc)));
    csec_belt1=[];
    csec_belt2=[];
    return;
end

if J~=0
    ERR=MException('BackwardModel:calc_accm','Integrer les inerties dans le calcul des auxiliaires mecaniques');
    csec_belt1=[];
    csec_belt2=[];
    return;
end

if VD.ACC.ntypacc==2
    % Vitesse arbre primaire 1 (alternateur)
    A=ones(size(wsec))*VD.BELT1.kred1*(1-VD.BELT1.glisst1);
    wprim1=A.*wsec;
    
    % Couple sur primaire 1
    cprim1=-1*(VD.ACC.Pacc_elec./VD.ACC.rdmoy_acm2)./wprim1;
    cprim1(find(isnan(cprim1)))=0;
    cprim1(find(isinf(cprim1)))=0;
    % Couple sur le secondaire 1
    A=ones(size(cprim1))*VD.BELT1.kred1.*VD.BELT1.rend1;
    indice=find(cprim1<0);
    A(indice)=VD.BELT1.kred1./VD.BELT1.rend1;
    csec_belt1=A.*cprim1;
    
    % Vitesse arbre primaire 2 (compresseur de climatisation)
    A=ones(size(wsec))*VD.BELT1.kred2*(1-VD.BELT1.glisst2);
    wprim2=A.*wsec;
    
    % Couple sur primaire 2
    cprim2=-1*(interp1(VD.ACC.Pcomp_waccm2,VD.ACC.Pcomp_pcomp,wprim2))./wprim2;
    cprim2(find(isnan(cprim2)))=0;
    cprim2(find(isinf(cprim2)))=0;
    % Couple sur le secondaire 2
    A=ones(size(cprim2))*VD.BELT1.kred2.*VD.BELT1.rend2;
    indice=find(cprim2<0);
    A(indice)=VD.BELT1.kred2./VD.BELT1.rend2;
    csec_belt2=A.*cprim2;
elseif VD.ACC.ntypacc==4
    % Vitesse arbre primaire 1 (alternateur)
    A=ones(size(wsec))*VD.BELT1.kred1*(1-VD.BELT1.glisst1);
    wprim1=A.*wsec;
    
    % Couple sur primaire 1
    Q_acm2=interp1(VD.ACC.alt.mechLossMap_rpm_x,VD.ACC.alt.mechLossMap_W_y,wprim1,'linear','extrap');
    cprim1=-1*(VD.ACC.Pacc_elec+Q_acm2)./wprim1;
    cprim1(find(isnan(cprim1)))=0;
    cprim1(find(isinf(cprim1)))=0;
    
    % Couple sur le secondaire 1
    A=ones(size(cprim1))*VD.BELT1.kred1.*VD.BELT1.rend1;
    indice=find(cprim1<0);
    A(indice)=VD.BELT1.kred1./VD.BELT1.rend1;
    csec_belt1=A.*cprim1;
    
    % Vitesse arbre primaire 2 (compresseur de climatisation)
    A=ones(size(wsec))*VD.BELT1.kred2*(1-VD.BELT1.glisst2);
    wprim2=A.*wsec;
    
    % Couple sur primaire 2
    cprim2=-1*(interp1(VD.ACC.powerMap_rpm_x,VD.ACC.powerMap_W_y,wprim2))./wprim2;
    cprim2(find(isnan(cprim2)))=0;
    cprim2(find(isinf(cprim2)))=0;
    % Couple sur le secondaire 2
    A=ones(size(cprim2))*VD.BELT1.kred2.*VD.BELT1.rend2;
    indice=find(cprim2<0);
    A(indice)=VD.BELT1.kred2./VD.BELT1.rend2;
    csec_belt2=A.*cprim2;
end