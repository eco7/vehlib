%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function [ERR,vit,dvit,Fres,Faero,Froul,Fpente]=calc_efforts_vehicule_fw(ERR,VD,dist_p,vit_p,masse,Jveh,croue,pas)
%
% Calcul la vitesse et l'acceleration du vehicule pour une entree de couple
% donnee, et un vehicule donnee.
%
% Input Arguments
% VD = Vehicle Data (structure)
% dist_p = distance parcourus (permet de prendre en compte la pente)
% vit_p = vitesse precedente
% masse = masse totale du VD.VEHIcule (kg)
% Jveh = Inertia
% croue = couple applique a la roue, = sortie du reducteur (N.m)
% pas = pas de temps de la simulation (s)
%
% Output Arguments
% vit = vitese du vehicule (m.s-1)
% dvit = acceleration du vehicule (m.s-2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ERR,vit,dvit,Fres,Faero,Froul,Fpente]=calc_efforts_vehicule_fw(ERR,VD,dist_p,vit_p,masse,Jveh,croue,pas)
 
%calculs des forces r?sistantes
if VD.CYCL.ntyppente==2 | VD.CYCL.ntyppente==3
     % suppression des doublons pour interpolation
%      B=[VD.CYCL.PKpente' VD.CYCL.penteFpk'];
%      B=unique(B,'rows');
     penteFpk=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,dist_p); 
     Fpente=masse.*VD.VEHI.Gpes.*sin(atan(penteFpk/100));
elseif VD.CYCL.ntyppente==0
    Fpente=masse*VD.VEHI.Gpes*sin(atan(VD.CYCL.pente/100));
else
    chaine=' modele de pente non reconnu ';
    ERR=MException('BackwardModel:calc_efforts_VD.VEHIcule',chaine);
end

if VD.VEHI.ntypveh==1
    Froul=masse.*VD.VEHI.Gpes.*(VD.VEHI.a+VD.VEHI.b*vit_p.^2);
    Faero=(VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx/2)*vit_p.^2 ;
    Fres=Froul+Faero;
elseif VD.VEHI.ntypveh==2
    Fres=(VD.VEHI.fcst+VD.VEHI.fvit1.*vit_p+VD.VEHI.fvit2*vit_p.^2).*(vit_p>0.01);
    Fres=Fres;
elseif VD.VEHI.ntypveh==3
    Froul=masse.*VD.VEHI.Gpes.*(VD.VEHI.a0 + VD.VEHI.a1*vit_p + VD.VEHI.a2*vit_p.^2);
    Faero=(VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx/2)*vit_p.^2;
    Fres=Froul+Faero;
end

Fres=Fres+Fpente;
%assignin('base','Fres',Fres)

cres=Fres*VD.VEHI.Rpneu;
dwroue=(croue-cres)./Jveh;
dvit=dwroue*VD.VEHI.Rpneu;
vit=pas.*dvit+vit_p;


% fprintf(1,'%s %.1f \n','fmot_fw : ',Fmot) 
% fprintf(1,'%s %.1f \n','fres_fw : ',Fres) 
% fprintf(1,'%s %.1f \n','vit : ',vit) 
% fprintf(1,'%s %.1f \n','dvit : ',dvit) 
return
   

