% function [ERR,cprim,wprim,dwprim,etat_emb]=calc_emb(VD,csec,wsec,dwsec,elhyb)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet :
%
% Arguments appel :
% VD : structure donnees vehicule
% csec,wsec,dwsec : couple vitesse et derive vitesse secondaire
% elhyb : mode elec ou hybride
% type_appel : 
% 1 : Cas rapport de boite imposee et vecteur en entree temps/sortie
% 2 : Cas rapport de boite libre et matrice temps * nrap +1 en entree/sortie
% 3 : Cas rapport de boite libre et tableau 3D en entree/sortie temps, sy, nrap+1
%
% Arguments de sortie :
% ERR : sortie en erreur si taille des entrees incohérente
% cprim,wprim,dwprim : couple vitesse et derive vitesse primaire
% etat_emb : etat de l'embrayge
% 0   ferme
% 1   ouvert
% 0.5 glissant
% Note: La vitesse de l'arbre primaire de l'embrayage est indeterminee
% au point mort dans ce type de modele. On fait le choix d'utiliser la variable
% elhyb pour lever l'indetermination. Necessaire pour les auxiliaires.
%
% 26-08-2011 (BJ) : version 1
% 31-05-2016 (EV) :  rajout de l'argument d'ppel type_appel
% 28-10-21 (EV) : correction cas rtype_appel =2 (rapport d eboite libre,
% rajout condition  cprim(2,:) >0 . NB si cprim > on limitera wprim à
% wmt_ral et pas wmt_patinage

function [ERR,cprim,wprim,dwprim,etat_emb]=calc_emb(VD,csec,wsec,dwsec,elhyb,type_appel)
ERR=[];

if nargin==5
    if VD.CYCL.ntypcin==3 | isvector(csec) % rapport de boite imposes par la cinematique
        type_appel=1;
    elseif VD.CYCL.ntypcin==1 % rapports de boite libre
        type_appel=2;
    end
end

if ~isscalar(elhyb) && (sum(size(elhyb)==size(wsec))~=2 || sum(size(elhyb)==size(csec))~=2 )
    ERR=MException('BackwardModel:calc_emb','size of input argument non supported');
    [cprim,wprim,dwprim,etat_emb]=backward_erreur(length(VD.CYCL.temps));
end

cprim=csec;
wprim=wsec;
dwprim=dwsec;

if type_appel==1 % rapport de boite imposes par la cinematique
    etat_emb=zeros(size(VD.CYCL.temps));
    etat_emb(find(VD.CYCL.rappvit==0))=1;
    
    % On sature la vitesse de rotation de l'arbre primaire a la vitesse de
    % patinage moteur en premiere
   
    indice=find(wprim<VD.ECU.wmt_patinage & VD.CYCL.rappvit==1 & elhyb>=1 & cprim>0); % On sature a wmt_patinage i cprim>0, on saturera a wmt_ral si besoin si C<10 (cf L101
    % indice=find(wprim<VD.ECU.wmt_patinage & VD.CYCL.rappvit==1 & elhyb>=1);
    if ~isempty(indice)
        wprim(indice)=VD.ECU.wmt_patinage;
        etat_emb(indice)=0.5;
    end
    
    % Recalcul de dwmprim (pas recalculer : attention si on le recalcul
    % et si echantillonage <1Hz)
    % dwprim(2:length(VD.CYCL.temps))=diff(wprim)./diff(VD.CYCL.temps);
    % dwprim(1)=0;
    
elseif type_appel==2 %Cas rapport de boite libre et matrice temps * nrap +1 en entree/sortie
    etat_emb=zeros(size(csec));
    etat_emb(2,:)=1;
    
    % On sature la vitesse de rotation de l'arbre primaire a la vitesse de
    % patinage moteur en premiere
    
    indice=find(wprim(2,:)<VD.ECU.wmt_patinage & elhyb(2,:)>=1 & cprim(2,:) >0); % On sature a wmt_patinage i cprim>0, on saturera a wmt_ral si besoin si C<10 (cf L101
    if ~isempty(indice)
        wprim(2,indice)=VD.ECU.wmt_patinage;
        etat_emb(2,indice)=0.5;
    end
    
    
elseif type_appel==3 % Cas rapport de boite libre et tableau 3D en entree/sortie temps, sy, nrap+1
    etat_emb=zeros(size(csec));
    etat_emb(2,:)=1;
    
    % On sature la vitesse de rotation de l'arbre primaire a la vitesse de
    % patinage moteur en premiere
    
    indice=find(wprim(:,:,2)<VD.ECU.wmt_patinage & elhyb(:,:,2)>=1);
    if ~isempty(indice)
        wprim(:,indice,2)=VD.ECU.wmt_patinage;
        etat_emb(:,indice,2)=0.5;
    end
    
end

% On sature la vitesse de l'arbre primaire de l'embrayage lorsque la
% variable elhyb vaut 1.
indice=find(wprim<VD.MOTH.ral & elhyb>=1);
if ~isempty(indice)
    wprim(indice)=VD.MOTH.ral;
    etat_emb(indice)=1;
end

% Cas recuperation (Csec <0) et wprim > wsec, impossible
% dans ce cas on impose cprim=0 et embrayage ouvert
indice=find(csec<0 & wprim>wsec & elhyb>=1);
if ~isempty(indice)
    etat_emb(indice)=1;
    cprim(indice)=0;
end


