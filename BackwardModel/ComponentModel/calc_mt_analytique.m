function [MOTH,ERR] = calc_mt_analytique(MOTH, param)
ERR = struct([]);
%% Cmax = MOTH.coeffCmax(1)*wmt.^2 + MOTH.coeffCmax(2)*wmt + coeffCmax(3) %%
% Modele du couple max comme une parabole
% copie des donnees depuis la structure VD
vecWmtMax = MOTH.wmt_max';
vecCmtMax = MOTH.cmt_max';

% exclusion des couples nuls (existant pour des raisons numeriques)
iNotZero = find(vecCmtMax~=0);
vecWmtMax = vecWmtMax(iNotZero);
vecCmtMax = vecCmtMax(iNotZero);

% determination de la parabole la plus proche (moindre carres)
MOTH.coeffCmax = polyfit(vecWmtMax, vecCmtMax, 2);
MOTH.coeffCmax(3) = MOTH.coeffCmax(3) - 22;

%% Modele de la consommation : trois modeles possibles
% copie des donnees depuis la structure VD
conso = MOTH.Conso_2d;
reg = MOTH.Reg_2dconso;
cpl = MOTH.Cpl_2dconso;

% construction de matrices de la meme taille que la conso
matCpl = repmat(cpl', size(reg))';
matReg = repmat(reg', size(cpl));

% passage des matrices en vecteurs (linear indexing)
vecCpl = matCpl(:);
vecReg = matReg(:);
vecConso = conso(:);

% exclusion des valeurs > Cmax ou < ralenti (out of bounds)
vecCmax = MOTH.coeffCmax(1)*vecReg.^2 + MOTH.coeffCmax(2)*vecReg + ones(size(vecReg))*MOTH.coeffCmax(3);
vecInOut = vecCpl<=vecCmax & vecReg>=MOTH.ral;
iIn = find(vecInOut == 1);

vecCpl = vecCpl(iIn);
vecReg = vecReg(iIn);
vecConso = vecConso(iIn);

if strcmpi(param.type_moth,'willans')
    % Conso = MOTH.coeff(1)*wmt.*cmt + MOTH.coeff(2)*wmt
    % Frot = MOTH.cf
    vecAWillans = [vecReg.*vecCpl vecReg];
    MOTH.coeff = vecAWillans\vecConso;
    MOTH.cf = MOTH.coeff(2)/MOTH.coeff(1); %Nm
elseif strcmpi(param.type_moth,'frot_lin')
    % Conso = MOTH.coeff(1)*wmt.*cmt + MOTH.coeff(2)*wmt + MOTH.coeff(3)*wmt.^2
    % Frot = MOTH.cf(1) + MOTH.cf(2)*wmt
    vecAFrotLin = [vecReg.*vecCpl vecReg vecReg.^2];
    MOTH.coeff = vecAFrotLin\vecConso;
    MOTH.cf(1) = MOTH.coeff(2)/MOTH.coeff(1); %Nm
    MOTH.cf(2) = MOTH.coeff(3)/MOTH.coeff(1); %Nm /(rad/s)
elseif strcmpi(param.type_moth, 'DRIVE')
    % Conso = (wmt.*cmt + MOTH.coeff(1)*wmt.^2 + MOTH.coeff(2)*wmt)./ ...
    %( VD.MOTH.pci*( MOTH.coeff(3) - MOTH.coeff(4)./(MOTH.coeff(5) + wmt) ))
    % Frot = MOTH.cf(1) + MOTH.cf(2)*wmt
    zdata = vecConso;
    funDrive = @(x)(vecReg.*vecCpl + x(1)*vecReg.^2 + x(2)*vecReg)./...
        (VD.MOTH.pci*(repmat(x(3),size(vecReg))-x(4)./(repmat(x(5),size(vecReg))+vecReg)))...
        -zdata;
    x0Drive = [0.0305 14.229 0.4*0.98 0.4*300*2*pi/60 2000*2*pi/60];
    MOTH.coeff = lsqnonlin(funDrive,x0Drive);
    MOTH.cf = [MOTH.coeff(2) MOTH.coeff(1)];
else
    ERR = MException('calc_HP_BV_2EMB_analytique:unknownParameter',...
        'type de moteur (param.type_moth) non defini');
end
end