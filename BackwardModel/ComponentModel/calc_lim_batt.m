%Calcul de la puissance de charge et de d�charge maximum
%Calcul de la puissance qu'il faut pour atteindre les SoC limites de la batterie

function [p_bat_socmin,p_bat_socmax,Pd_lim,Pc_lim,Ibat_socmin,Ibat_socmax]=calc_lim_batt(BATT,param,E,R,soc,RdF,pas,Ibat_min_calc,Ubat_ibat_min_calc,Ibat_max_calc,Ubat_ibat_max_calc)

%Pour SoC max
if ~isfield(param,'soc_max')
    param.soc_max=100;
end
if ~isfield(param,'soc_min')
    param.soc_min=20;
end
Ibat_socmax=36*BATT.Cahbat_nom*(soc - param.soc_max)/(RdF*pas);
Ubat_socmax=(E/BATT.Nblocser-R*(BATT.Nbranchepar/BATT.Nblocser).*Ibat_socmax)*BATT.Nblocser;
Ibat_socmax= Ibat_socmax*BATT.Nbranchepar;
p_bat_socmax=Ibat_socmax*Ubat_socmax;
if p_bat_socmax>-10e-5 
    p_bat_socmax=0;
end

%Pour SoC min
Ibat_socmin=36*BATT.Cahbat_nom*(soc - param.soc_min)/(RdF*pas);
Ubat_socmin=(E/BATT.Nblocser-R*(BATT.Nbranchepar/BATT.Nblocser).*Ibat_socmin)*BATT.Nblocser;
Ibat_socmin= Ibat_socmin*BATT.Nbranchepar;
p_bat_socmin=Ibat_socmin*Ubat_socmin;
if p_bat_socmin<10e-5
    p_bat_socmin=0;
end

Pd_lim = Ibat_max_calc*Ubat_ibat_max_calc;
Pc_lim =Ibat_min_calc*Ubat_ibat_min_calc;
end