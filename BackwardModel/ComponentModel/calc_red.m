% function [ERR,cprim,wprim,dwprim]=calc_red(ERR,RED,csec,wsec,dwsec,J)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (BJ) : version 1

function [ERR,cprim,wprim,dwprim]=calc_red(ERR,RED,csec,wsec,dwsec,J)

% Couple sur primaire suivant signe
A=ones(size(csec))*1/(RED.kred.*RED.rend);
indice=find(csec<0);
A(indice)=(RED.rend./RED.kred);
B=ones(size(csec))*J;
cprim=A.*csec+B.*dwsec;

% Vitesse arbre primaire
A=ones(size(wsec))*RED.kred;
wprim=A.*wsec;

% Acceleration arbre primaire
A=ones(size(dwsec))*RED.kred;
dwprim=A.*dwsec;

