function [dcarb, Padm] = calc_mt_3U_reverse(Cmt,N,Phi,delta_AA,VD)
% delta_AA = AA - AA_0

Tamb = VD.INIT.Tamb+273;
lambda = 1./Phi;

%% Calcul du couple moteur
n_comb = VD.MOTH.n_c0 - VD.MOTH.n_cA./(VD.MOTH.n_cB + N);

n_AA = 1 - VD.MOTH.k_AA*delta_AA.^2;

i1 = lambda<VD.MOTH.lambda1;
i2 = lambda>=VD.MOTH.lambda1 & lambda<VD.MOTH.lambda2;
i3 = lambda>=VD.MOTH.lambda3 & lambda<VD.MOTH.lambda4;

n_rich(i1) = VD.MOTH.g1*lambda(i1) - VD.MOTH.g0;
n_rich(i2) = VD.MOTH.e1 + (1-VD.MOTH.e1)*sin((lambda(i2)-VD.MOTH.lambda1)/(1-VD.MOTH.lambda1));
n_rich(lambda>=VD.MOTH.lambda2)=1;
n_rich(i3) = VD.MOTH.b_lambda + VD.MOTH.a_lambda*lambda(i3);
n_rich(lambda>=VD.MOTH.lambda4)=0;

Cmt_f = (VD.MOTH.PMF_cst + VD.MOTH.PMF_N*N + VD.MOTH.PMF_N2*N.^2)*VD.MOTH.Vd/(2*pi*VD.MOTH.Rev);

dcarb = (Cmt + Cmt_f).*(N*pi/30)...
        ./(VD.MOTH.n_ind*n_comb.*n_rich.*n_AA*VD.MOTH.pci);
dcarb = max(0, dcarb);

%% Calcul du debit de carburant
% coefficient de remplissage
r_phi = repmat(VD.MOTH.remp_cst,size(Phi));
r_phi(Phi>VD.MOTH.remp_Phi_cst) = interp1([VD.MOTH.remp_Phi_cst VD.MOTH.remp_Phi_max],[VD.MOTH.remp_cst VD.MOTH.remp_max],Phi(Phi>VD.MOTH.remp_Phi_cst),'linear','extrap');
remplissage = r_phi - VD.MOTH.remp_r_N*(VD.MOTH.remp_N_max-N).^2;

% temperature admission
T_adm = Tamb + VD.MOTH.Tadm_N*N + VD.MOTH.Tadm_cst;

dgaz = dcarb.*(1+VD.MOTH.AStoechio./Phi)/1000;
Padm = dgaz*(VD.MOTH.Rev*60)./(remplissage*1e2*VD.MOTH.Vd*1e-3.*N).*(VD.MOTH.R_gaz*T_adm)/VD.MOTH.M_gf;

end
