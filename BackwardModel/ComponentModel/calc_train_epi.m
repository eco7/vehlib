% function [ERR,csoleil,ccour,wsoleil]=calc_train_epi(ERR,EPI,cps,wps,wcour);
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
%
% Arguments appel
% ERR : struct pour la gestion des erreurs qui seront gerees par les programmes appelant
% EPI : struct de données pour le train epicycloîadale
% cps : couple sur le porte satellite
% wps : vitesse de rotation porte satellites
% wcour : vitesse de rotation de la couronne
% conv : convention sur les puissance
%        1 : type prius : les puissance PS et soleil sont > 0 quand elles
%           rentrent dans le train la puissance couronne > 0 quand elle sort (le
%           rendement est appliqué à la couronne)
%        2 : type ampera : les puissance couronne et soleil sont > 0 quand elles
%           rentrent dans le train la puissance soleil > 0 quand elle sort (le
%           rendement est appliqué au PS)
% Arguments de sortie :
% ERR : 
% csoleil : couple sur le soleil
% ccour : couple sur la couronne
% wsoleil : vitesse de rotation du soleil
% 
% 10-10-11 (EV): version 1 
% 23-05-12 (EV): Correction bug (pb quand (cps.*wcour)=0 pour le calcul du signe

function [ERR,csoleil,ccour,wsoleil,wsat_ps]=calc_train_epi(ERR,EPI,cps,wps,wcour,conv)

kred=EPI.kb; % Raison du train
krend=EPI.rend_train; % rendemant du train

% Relation de willis sur les vitesses
wsoleil=kred*wcour+(1-kred)*wps;
wsat_ps=(wps-wcour)*EPI.Ncour/EPI.Nsat;

% Relation sur les couples (rendement uniquement sur la couronne)
if nargin==5 || (nargin==6 && conv==1)
    
    csoleil=-cps/(1-kred);
    
    A=ones(size(cps))*krend;
    B=sign(cps.*wcour);
    B(wcour==0 & cps>=0)=1;
    B(wcour==0 & cps<0)=-1;
    ccour=(-cps*kred/(1-kred)).*A.^B;
    
elseif (nargin==6 && conv==2)
    
    csoleil=cps./((1-kred)*(krend.^sign(cps.*wps)));
    ccour=-cps*kred./((1-kred)*(krend.^sign(cps.*wps)));
    
else
   chaine=' Argument non consistent';
   ERR=MException('BackwardModel:calc_train_epi',chaine);
   throw(ERR)
end


