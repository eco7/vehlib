% function [ERR,csec,wsec,dwsec,Jsec]=calc_bv_fw(ERR,VD,cprim,wprim,dwprim,Jprim,rapport)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% simulation du bloc boite de vitesse en forward
% Cette fonction calcul le couple, la vitesse, l'acceleration et l'inertie
% ramenee au secondaire. Elle recois le rapport de boite actuelle.
% 
% Si on connait les inertie precedentes il n'est pas
% necessaire de connaitre les accelerations. et visversa.
% les parametre 7 n'est utile que dans le cas ou les numeros de
% rapport ne sont pas dans le cycle (ntypcin =1). Il faut alors les fixer.
% Nous choisissons ici que les ordres de passages sont donnes par
% l'exterieur, cf. calc_calc_fw
%
% --(Cprim,Jprim)-->[VD.BV(rapport)]--->(Csec,Jsec)
%   (wprim,dwprim)        |          (wsec,dwsec)
%                       ordre  
% 
% Parameters:
% 
% ERR :  erreur
% VD.CYCL : structure cinematique
% VD.BV :  Structure de la boite de vitesse, qui contient le rapport de 
%           reduction et le rendement en fonction du rapport
% cprim :  couple au primaire
% wprim :  vitesse au primaire
% dwprim :  acceleration au primaire
% Jprim :  inertie ramenee
% rapport :  numero de rapport
% type_appel : 5 : on ne rajoute pas le point mort
% type_appel : 6 : on ne rajoute pas le point mort et on as des rapport en
% entree
% 
%
% Results:
% 
%  ERR: erreur
%  csec: coule au secondaire
%  wsec: vitesse au secondaire
%  dwsec: acceleration au secondaire
%  Jsec: inertie ramenee
%  16/02/2011 (VR) : Version 1


function [ERR,csec,wsec,dwsec,Jsec]=calc_bv_fw(ERR,VD,cprim,wprim,dwprim,Jprim,rapport,type_appel)

if nargin ==6
    type_appel=0;
end

if VD.CYCL.ntypcin~=3 && VD.CYCL.ntypcin~=1
    ERR=MException('BackwardModel:calc_bv',strcat('Type de cinematique :',num2str(VD.CYCL.ntypcin),' non reconnue'));
    csec=[];
    wsec=[];
    dwsec=[];
    return;
end

if VD.CYCL.ntypcin==3
    % On rajoute le point mort et on calcule les rapports de demultiplication
    rapports=[0 VD.BV.k];
    if max(VD.CYCL.rappvit)>length(VD.BV.k)
        % Les rapports de boite du cycle ne sont pas adaptees a la boite !
        ERR=MException('BackwardModel:calc_bv','Les rapports de boite du cycle ne sont pas adaptees a la boite !');
        csec=[];
        wsec=[];
        dwsec=[];
        return;
    end

    kred=rapports(VD.CYCL.rappvit+1);

    % On rajoute le point mort et on calcule les rendements de demultiplication
    rendements=[0 VD.BV.Ro];
    krend=rendements(VD.CYCL.rappvit+1);
    
    A=ones(size(cprim)).*(kred.*krend);
    indice=find(cprim<0);
    A(indice)=(kred(indice)./krend(indice));
    % On corrige les divisions par zero au point mort.
    A(find(isnan(A)))=0;
    A(find(isinf(A)))=0;
    
    if Jprim~=0
        B=A*(Jprim+VD.BV.Jprim_bv(VD.CYCL.rappvit+1)); % J est l inertie de l arbre d 'entree (primaire)
    else
        B=zeros(size(cprim));
    end
    
    csec=A.*cprim+B.*dwprim;

    % Vitesse arbre secondaire
    A=ones(size(wprim))./kred;
    A(find(isnan(A)))=0;
    A(find(isinf(A)))=0;
    
    wsec=A.*wprim;

    % Acceleration arbre secondaire
    dwsec=A.*dwprim; 

    % Inertie sur le secondaire
    Jsec=0;
    
elseif VD.CYCL.ntypcin==1 % Rapports de boite libre
    if nargin==7
        
        rapports=[0 VD.BV.k];
        kred_bv=rapports(rapport+1);
        vect=[0 VD.BV.Ro];
        rend=vect(rapport+1);
        
        A=ones(size(cprim)).*rend;
        csec=cprim.*(A.^sign(cprim.*wprim)).*kred_bv;
        
        Jprim_bv=[0 VD.BV.Jprim_bv];
        Jsec=(Jprim+Jprim_bv(rapport+1)).*(A.^sign(cprim.*wprim)).*kred_bv.^2;
        
        wsec=wprim./kred_bv;
        wsec(kred_bv==0)=0; % au Point Mort wsec est NaN - A suivre ...
        dwsec=dwprim./kred_bv;
        dwsec(kred_bv==0)=0;

    elseif type_appel==6
        rapports=[VD.BV.k];
        kred_bv=rapports(rapport);
        vect=[VD.BV.Ro];
        rend=vect(rapport);
        
        A=ones(size(cprim)).*rend;
        csec=cprim.*(A.^sign(cprim.*wprim)).*kred_bv;
        
        wsec=wprim./kred_bv;
        wsec(kred_bv==0)=0; % au Point Mort wsec est NaN - A suivre ...
        dwsec=dwprim./kred_bv;
        dwsec(kred_bv==0)=0;

    else
        if type_appel ~=5
            rapports=[0 VD.BV.k];
            M=ndgrid(1:max(VD.BV.rapport)+1,VD.CYCL.temps);
            kred_bv=rapports(M);
            rendements=[0 VD.BV.Ro];
            A=rendements(M);
            csec=cprim.*(A.^sign(cprim.*wprim)).*kred_bv;
            Jprim_bv=[0 VD.BV.Jprim_bv];
            Jsec=(Jprim+Jprim_bv(M)).*(A.^sign(cprim.*wprim)).*kred_bv.^2;
            
            wsec=wprim./kred_bv;
            dwsec=dwprim./kred_bv;
        elseif type_appel==5
            rapports=[VD.BV.k];
            M=ndgrid(1:max(VD.BV.rapport),VD.CYCL.temps);
            kred_bv=rapports(M);
            rendements=[VD.BV.Ro];
            A=rendements(M);
            csec=cprim.*(A.^sign(cprim.*wprim)).*kred_bv;
            
            wsec=wprim./kred_bv;
            dwsec=dwprim./kred_bv;
        end
        
    end
    
    
%%%%%%%%%%%%%%%%%%% ce qui ???tait fait avant: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    
%     % on genere une matrice nrapports*temps
%     % On rajoute le point mort et on calcule les rapports de demultiplication
%     rapports=[0 VD.BV.k];
%     str='M=[ones(size(VD.CYCL.temps))';
%     for i=2:max(VD.BV.rapport)+1
%         str=strcat(str,';',num2str(i),'*ones(size(VD.CYCL.temps))');
%     end
%     str=strcat(str,'];');
%     eval(str)
%     kred=rapports(M);
% 
%     % On rajoute le point mort et on calcule les rendements de demultiplication
%     rendements=[0 VD.BV.Ro];
%     krend=rendements(M);
% 
%     % les grandeurs c,w,dw deviennent des matrices
%     strc='cprim=[cprim';
%     strw='wprim=[wprim';
%     strdw='dwprim=[dwprim';
%     for i=2:max(VD.BV.rapport)+1
%         strc=strcat(strc,';cprim');
%         strw=strcat(strw,';wprim');
%         strdw=strcat(strdw,';dwprim');
%     end
%     strc=strcat(strc,'];');
%     strw=strcat(strw,'];');
%     strdw=strcat(strdw,'];');
%     eval(strc);
%     eval(strw);
%     eval(strdw);
end
return
