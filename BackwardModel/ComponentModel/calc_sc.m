% function [ERR,usc,u0sc,ux]=calc_sc(ERR,SC,indice,pas,isc,u0sc_p,ux_p)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Rmque : indice n'est l� que pour l'initialisation si indice=1
% Si on a pas bsoin d'init on met l'indice de calcul en cours ou 0
%
% Arguments appel :
% Arguments de sortie :
% 
% 26-10-09 (EV) : N'y as t'il pas un pb dans la cas de plusieurs branche en //
% A priori OK maintenant 

function [ERR,usc,u0sc,ux]=calc_sc(ERR,SC,indice,pas,isc,u0sc_p,ux_p,model_SC)
%#eml
eml.extrinsic('MException')

if nargin==7
    model_SC=1;
end
% u0sc_p isc ux peuvent etre des vecteurs , usc u0sc peuvent etre des
% matrice de hauteur length(u0sc_p) de longueur length(isc)

isc=isc/SC.Nbranchepar;
u0sc_p=u0sc_p/SC.Nblocser;
ux_p=ux_p/SC.Nblocser;
[Li,Co]=size(isc);

if model_SC==2 % On neglige ux
     if indice==1 % si indice =1 on as u0sc_p et ux scalaire (pas besoin de creer des matrices)
        ur=SC.Rs.*isc;
        u0sc=(SC.Vc0).*ones(1,Co);
        ux=0*ones(1,Co);
    else
        u0sc=u0sc_p-1./(SC.C).*isc*pas;
        ux=0*ones(1,Co);
        ur=(SC.Rs.*isc);
    end
    
    u0sc=u0sc*SC.Nblocser;
    ux=ux*SC.Nblocser;
    ur=ur*SC.Nblocser;
    usc=u0sc-ux-ur;
else
    if indice==1 % si indice =1 on as u0sc_p et ux scalaire (pas besoin de creer des matrices)
        ur=SC.Rs.*isc;
        u0sc=(SC.Vc0).*ones(1,Co);
        ux=0*ones(1,Co);
    else
        u0sc=u0sc_p-1./(SC.C).*isc*pas;
        ux=( (ux_p-SC.Rx.*isc)*exp(-pas/(SC.Rx.*SC.Cx))  + SC.Rx.*isc);
        ur=(SC.Rs.*isc);
    end
    
    u0sc=u0sc*SC.Nblocser;
    ux=ux*SC.Nblocser;
    ur=ur*SC.Nblocser;
    usc=u0sc-ux-ur;
end
%% Il faut limiter la tension max en recharge mais en decharge a priori on
%% peut laiser depasser et mettre une eventuel limite plus haute
% idem en recharge
prec_num=1+1e-10; % pour la programmation dynamique sinon on risque de mettre a NaN tout les arc chemins (notamment
%si uOsc final = SC.max

usc( (usc<SC.minTension*SC.Nblocser & isc>0) | (usc>SC.maxTension.*SC.Nblocser*prec_num & isc<0)  ) = NaN;
u0sc( (usc<SC.minTension*SC.Nblocser & isc>0) | (usc>SC.maxTension.*SC.Nblocser*prec_num & isc<0) ) = NaN;

[Li,Co]=size(usc);
if sum(sum(isnan(usc)))==Li*Co
    chaine=' Tout les elements de usc = NaN';
    ERR=MException('BackwardModel:calc_sc',chaine);
end
    
return;


