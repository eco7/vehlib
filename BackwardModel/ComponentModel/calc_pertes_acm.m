% function [ERR,qacm1]=calc_pertes_acm(ERR,ACM,cacm1,wacm1,dwacm1);
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Calcul des pertes de la machine
% Arguments appel :
% ERR: structure erreur
% ACM: structure de parametre de la machine ACM
% cacm1: couple de la machine (scalaire, vecteur, matrice, tableau )
% wacm1: vitesse de rotation en rd/s
% dwacm1: derivee de la vitesse de rotation
% extrap: parametre extrapolation (si 1 les points hors carto sont
% interpolle, sinnon NaN).
% fit: Parametres fittage carto, si un les pertes sont calcules a partir
% d'un poynome de degré 2 en T et w.
% type carto : certo 2D ou 3D (vitesse, couple, Ubus)
% Arguments de sortie :
% ERR: Structure erreur
% qacm1: pertes dans la machine
%
% 26-08-2011 (BJ) : version 1

function [ERR,qacm1,Uacm1]=calc_pertes_acm(ERR,ACM,cacm1,wacm1,dwacm1,extrap,fit,type_carto,Ubus);

if sum(size(cacm1))~=sum(size(wacm1))
    chaine=' taille argument entree incompatible : \n size(cacm1) : %s \n  size(wacm1) : %s \n ';
    ERR=MException('BackwardModel:calc_pertes_acm',chaine,num2str(size(cacm1)),num2str(size(wacm1)));
    qacm1=backward_erreur(length(cacm1));
    return;
end

if nargin<8
    type_carto='2D';
end

ERR=[];
qacm1=NaN*ones(size(cacm1));
ind_ppos=find(~isnan(cacm1) & ~isinf(cacm1) & cacm1.*wacm1>=0); % indice ou la puissance est positive
ind_pneg=find(~isnan(cacm1) & ~isinf(cacm1) & cacm1.*wacm1<0); % indice ou la puissance est negative

if strcmp(type_carto,'2D')
    if (nargin==7) && fit==1 % calcul les pertes a partir de coeff c1 a c8
        % on verifie que les coeff de fittage existent dans ACM
        if isfield(ACM,'c1') && isfield(ACM,'c8')
            sP1=sign(wacm1);
            sP1(wacm1==0)=1;
            qacm1=(ACM.c1*cacm1.*cacm1+ACM.c2*sP1.*cacm1+ACM.c3).*wacm1.*wacm1+...
                (ACM.c4*cacm1.*cacm1+ACM.c5*sP1.*cacm1+ACM.c6).*abs(wacm1)+...
                (ACM.c7*cacm1.*cacm1+ACM.c8*sP1.*cacm1);
        else
            chaine=' Les coefficients de fittage des pertes ne sont pas des champs de acm1 ';
            ERR=MException('BackwardModel:calc_pertes_acm',chaine);
        end
    elseif (nargin==6 || nargin==7) && extrap==1 % calcul pertes avec interpollation carto et on extrapole les valeur si on sort de la carto
        qacm1(ind_ppos)=interp2(ACM.Regmot_pert,ACM.Cmot_pert,ACM.Pert_mot',abs(wacm1(ind_ppos)),abs(cacm1(ind_ppos)),'linear',max(max(ACM.Pert_mot)));
        qacm1(ind_pneg)=interp2(ACM.Regmot_pert,ACM.Cmot_pert,ACM.Pert_mot',abs(wacm1(ind_pneg)),-abs(cacm1(ind_pneg)),'linear',max(max(ACM.Pert_mot)));
    else % calcul pertes avec interpollation carto
        qacm1(ind_ppos)=interp2(ACM.Regmot_pert,ACM.Cmot_pert,ACM.Pert_mot',abs(wacm1(ind_ppos)),abs(cacm1(ind_ppos)));
        qacm1(ind_pneg)=interp2(ACM.Regmot_pert,ACM.Cmot_pert,ACM.Pert_mot',abs(wacm1(ind_pneg)),-abs(cacm1(ind_pneg)));
    end
    
    if isfield(ACM,'U_pert_mot_min')
        Uacm1(ind_ppos)=interp2(ACM.Regmot_pert,ACM.Cmot_pert,ACM.U_pert_mot_min',abs(wacm1(ind_ppos)),abs(cacm1(ind_ppos)));
        Uacm1(ind_pneg)=interp2(ACM.Regmot_pert,ACM.Cmot_pert,ACM.U_pert_mot_min',abs(wacm1(ind_pneg)),-abs(cacm1(ind_pneg)));
    end
    
    if sum(isnan(qacm1))== numel(qacm1)
        ind=find(isnan(qacm1));
        chaine=' Probleme de calcul des pertes Machine electrique !\n ind : %s \n cacm1 : %s \n  wacm1 : %s \n qacm1 : %s \n ';
        ERR=MException('BackwardModel:calc_pertes_acm',chaine,num2str(ind),num2str(cacm1),num2str(wacm1),num2str(qacm1));
        return;
    end
    
elseif strcmp(type_carto,'3D')
    if extrap==1 % calcul pertes avec interpollation carto et on extrapole les valeur si on sort de la carto
        qacm1(ind_ppos)=interp3(ACM.Cmot_pert,ACM.Regmot_pert,ACM.Tension_cont,ACM.Pert_mot_3D,abs(cacm1(ind_ppos)),abs(wacm1(ind_ppos)),Ubus(cacm1(ind_ppos)),'linear',max(max(max(ACM.Pert_mot))));       
        qacm1(ind_pneg)=interp3(ACM.Cmot_pert,ACM.Regmot_pert,ACM.Tension_cont,ACM.Pert_mot_3D,-abs(cacm1(ind_pneg)),abs(wacm1(ind_pneg)),Ubus(cacm1(ind_ppos)),'linear',max(max(max(ACM.Pert_mot))));
    else % calcul pertes avec interpollation carto
        qacm1(ind_ppos)=interp3(ACM.Cmot_pert,ACM.Regmot_pert,ACM.Tension_cont,ACM.Pert_mot_3D,abs(cacm1(ind_ppos)),abs(wacm1(ind_ppos)),Ubus(ind_ppos));       
        qacm1(ind_pneg)=interp3(ACM.Cmot_pert,ACM.Regmot_pert,ACM.Tension_cont,ACM.Pert_mot_3D,-abs(cacm1(ind_pneg)),abs(wacm1(ind_pneg)),Ubus(ind_pneg));
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
