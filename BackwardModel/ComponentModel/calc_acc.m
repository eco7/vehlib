% function [ERR,pacc]=calc_acc(VD)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet :
% calcul de la puissance des accessoisres le long du cycle
% avec prise en compte d'une puissance ?? l'arr??t ou en roulage si
% VD.ACC.Pacc_elec_arr existe
%
% 01/04/2011 (EV)

function [ERR,pacc]=calc_acc(VD)

% Initiaisation
pacc=zeros(size(VD.CYCL.temps));

ERR=[];

if sum(VD.ACC.ntypacc==[1 4])~=1
    ERR=MException('BackwardModel:calc_acc',strcat('Type d''accessoires non reconnues : ',num2str(VD.ACC.ntypacc)));
    csec_belt1=[];
    csec_belt2=[];
    return;
end

if VD.ACC.ntypacc==1 % cas accessoire electriques constant
    if ~isfield(VD.ACC,'Pacc_elec_arr')
        pacc=VD.ACC.Pacc_elec*ones(size(VD.CYCL.temps));
    else
        seuil_vit0=0.01;
        pacc(VD.CYCL.vitesse<=seuil_vit0)=VD.ACC.Pacc_elec_arr;
        pacc(VD.CYCL.vitesse>seuil_vit0)=VD.ACC.Pacc_elec_roul;
    end
elseif VD.ACC.ntypacc==4 % cas accessoires electriques non constant defini par la cinematique
    pacc=VD.CYCL.Pacc_elec;
end
