% function [ERR,cprim,wprim,dwprim]=calc_bv(VD,csec,wsec,dwsec,J)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul des couple,vitesse et derive du primaire de la boite de vitesse 
% en fonction des valeurs aux secondaire
% Arguùents entree
% VD : donnees vehicule
% csec,wsec,dwsec,J : couple vitesse derivee de la vitesse secondaire et inertie ramenee
% type_appel :
% 1 : cas raport de boite imposee la dimensions 2 est le temps. On as en
% entrees/sorties des tailles identiques vecteur ou matrice ou tableau. On
% duplique les kred et rend selon les dimensions autres que 2. 
% 2 : cas rapport de boite libre et vecteurs (1*temps) en entree on sort
% des matrices nrapport+1 * temps
% 3 : cas rapport de boite libre et tableaux vitesse*vitesse en entree (eco-conduite) on
% sort des tableaux vitesse*vitesse*nrapport+1
% 4 : cas rapport de boite libre mais on as deja des tableaux nrap+1 *
% temps en entree on sort des tableaux nrap+1
% 5 : idem à 2 main on ne rajoute pas le point mort
%
% 26-08-11 (BJ-EV) : version 1
% 31-05-16 (EV) : Rajout type_appel

function [ERR,cprim,wprim,dwprim]=calc_bv(VD,csec,wsec,dwsec,J,type_appel)

if nargin==5
    if VD.CYCL.ntypcin==3 % rapport de boite imposes par la cinematique
        type_appel=1;
    elseif VD.CYCL.ntypcin==1 % rapports de boite libre
        type_appel=2;
    end
end

ERR=[];

if VD.CYCL.ntypcin~=3 & VD.CYCL.ntypcin~=1
    ERR=MException('BackwardModel:calc_bv',strcat('Type de cinematique :',num2str(VD.CYCL.ntypcin),' non reconnue'));
    cprim=[];
    wprim=[];
    dwprim=[];
    return;
end


% On rajoute le point mort et on calcule les rapports de demultiplication
if type_appel~=5
    rapports=[0 VD.BV.k];
    rendements=[0 VD.BV.Ro];
elseif type_appel==5
    rapports=[VD.BV.k];
    rendements=[VD.BV.Ro];
end

if VD.CYCL.ntypcin==3 % rapport de boite imposes par la cinematique
    if max(VD.CYCL.rappvit)>length(VD.BV.k)
        % Les rapports de boite du cycle ne sont pas adaptees a la boite !
        ERR=MException('BackwardModel:calc_bv','Les rapports de boite du cycle ne sont pas adaptees a la boite !');
        cprim=[];
        wprim=[];
        dwprim=[];
        return;
    end
end
    
% On rajoute le point mort et on calcule les rendements de demultiplication
% rendements=[0 VD.BV.Ro];
    
if type_appel==1 % rapport de boite imposes par la cinematique   
      
    kred=rapports(VD.CYCL.rappvit+1);
    krend=rendements(VD.CYCL.rappvit+1);
    
    % On dupplique selon les dimensions autres que 2.
    % Rmque : Dans le cas vecteur ligne en entree les lignes suivantes ne font rien
    s_csec=size(csec);
    s_csec(2)=1;
    kred=repmat(kred,s_csec);
    krend=repmat(krend,s_csec);
    
elseif type_appel==2 | type_appel==5 %cas rapport de boite libre et vecteurs (1*temps) en entree on sort nrapport+1 * temps
   
    % si vecteur, size(csec)=length(rapport)+1,temps)
    kred=repmat(rapports', size(csec));
    krend=repmat(rendements', size(csec));
    
    csec=repmat(csec,length(rapports), 1);
    wsec=repmat(wsec,length(rapports), 1);
    dwsec=repmat(dwsec,length(rapports), 1);
    
    
elseif type_appel==3 % cas rapport de boite libre et tableaux vitesse*vitesse en entree (eco-conduite) on
    % sort des tableaux vitesse*vitesse*nrapport+1
    kred=permute(rapports,[1 3 2]); %permutation kred pour creation tableau 1*1*nrapport+1
    krend=permute(rendements,[1 3 2]);
    
    kred=repmat(kred , size(csec));
    krend=repmat(krend , size(csec));
    
    csec=repmat(csec,[ 1 1 length(rapports)]);
    wsec=repmat(wsec,[ 1 1 length(rapports)]);
    dwsec=repmat(dwsec,[ 1 1 length(rapports)]);
    
elseif type_appel==4 %cas rapport de boite libre mais on as deja des tableaux nrap+1 * temps en entree on sort des tanbleaux nrap+1

    kred=repmat(rapports',[1 size(csec,2)]); 
    krend=repmat(rendements',[1 size(csec,2)]);
    
end

% Couple sur primaire suivant signe
A=ones(size(csec)).*1./(kred.*krend);
indice=find(csec<0);
A(indice)=(krend(indice)./kred(indice));
% On corrige les divisions par zero au point mort.
A(find(isnan(A)))=0;
A(find(isinf(A)))=0;

% Pour les cas en rapport de boite libre il faudra "eliminer" les valeurs
% ou le Point mort est impossible
if type_appel==2 %cas rapport de boite libre et vecteurs (1*temps) en entree on sort
    % des matrices nrapport+1 * temps
    indice=(csec(1,:)>0);
    A(1,indice)=inf;
elseif type_appel==3 % cas rapport de boite libre et tableaux vitesse*vitesse en entree (eco-conduite) on
    % sort des tableaux vitesse*vitesse*nrapport+1
    [indice,jindice]=find(csec(:,:,1)>0);
    indice = sub2ind(size(csec),indice,jindice,ones(size(indice)));
    A(indice)=inf;     
end

if J~=0
    B=A/J; % J est l inertie de l arbre d 'entree (primaire)
else
    B=zeros(size(csec));
end
cprim=A.*csec+B.*dwsec;

% Vitesse arbre primaire
A=ones(size(wsec)).*kred;
wprim=A.*wsec;

% Acceleration arbre primaire
A=ones(size(dwsec)).*kred;
dwprim=A.*dwsec;
    


