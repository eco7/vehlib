% Function: calc_mt
% fonction de calcul de la consommation du moteur thermique
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (BJ) : version 1
% 23-07-12(EV): rajout ~isnan(wmt) L33

function [ERR,dcarb,cmt,cfrein_meca_mt,dcarb_nl,cind]=calc_mt(ERR,MOTH,cmt,wmt,dwmt,elhyb,J,calc_lim,fit,prec_num)

if nargin <10
    prec_num=1e-10;
end
%ERR=[];
if sum(size(cmt))~=sum(size(wmt))
    chaine=' taille argument entree incompatible \n size(cmt) : %s\n  size(wmt) : %s\n ';
    ERR=MException('BackwardModel:calc_mt',chaine,num2str(size(cmt)),num2str(size(wmt)));
    [dcarb,cmt,cfrein_meca_mt]=backward_erreur(length(cmt));
    return;
end

if J~=0
    ERR=MException('BackwardModel:calc_mt','L''inertie moteur thermique non prise en compte localement :');
    [dcarb,cmt,cfrein_meca_mt]=backward_erreur(length(cmt));
    return;
end

% Initialisation du couple de freinage mecanique calcule sur l'arbre du moteur thermique
cfrein_meca_mt=zeros(size(cmt));
dcarb=zeros(size(cmt))*NaN;
ind=find(~isnan(cmt)&~isinf(cmt)&~isnan(wmt));
if nargin<8 || (nargin==8 && calc_lim~=1) || (nargin>=8 && calc_lim~=1 && fit~=1)
    dcarb(ind)=interp2(MOTH.Reg_2dconso,MOTH.Cpl_2dconso,MOTH.Conso_2d',wmt(ind),cmt(ind));
elseif nargin==9 && fit==1 && calc_lim~=1
    dcarb(ind)=(MOTH.c1*cmt(ind).*cmt(ind)+MOTH.c2.*cmt(ind)+MOTH.c3).*wmt(ind).*wmt(ind)+...
              (MOTH.c4*cmt(ind).*cmt(ind)+MOTH.c5.*cmt(ind)+MOTH.c6).*wmt(ind)+...
              (MOTH.c7*cmt(ind).*cmt(ind)+MOTH.c8.*cmt(ind));
    dcarb(cmt<0)=NaN;      
end

dcarb_nl=dcarb; % On garde le dcarb sans limitation
% prise en compte des limitations
indice_arret_mot=find(wmt<MOTH.ral & elhyb==1);
if ~isempty(indice_arret_mot)
    %ERR=MException('BackwardModel:calc_mt','Des valeurs de vitesse moteur inferieures au ralenti');
    %disp(['calc_mt: Des valeurs de vitesse moteur inferieures au ralenti aux indices: ',num2str(indice_arret_mot)])
    %disp(['calc_mt: Des valeurs de vitesse moteur inferieures au ralenti'])
    %if param.verbose >= 1
        %wmt(indice_arret_mot)
    %end
    wmt(indice_arret_mot)=NaN;
    dcarb(indice_arret_mot)=NaN;
end

indice_wmax=find(wmt>MOTH.wmt_maxi);
if ~isempty(indice_wmax)
    wmt(indice_wmax)=NaN;
    dcarb(indice_wmax)=NaN;
    if sum(sum(sum(wmt>MOTH.wmt_maxi)))==numel(wmt)
        chaine=' vitesse moteur thermique superieure ?? valeur max \n indice : %s\n wmt : %s \n  MOTH.wmt_maxi %s \n';
        ERR=MException('BackwardModel:calc_mt',chaine,num2str(indice_wmax),num2str(wmt),num2str(MOTH.wmt_maxi));
        [dcarb,cmt,cfrein_meca_mt]=backward_erreur(length(cmt));
        return;
    end
end

indice_cmax=find(cmt>interp1(MOTH.wmt_max,MOTH.cmt_max,wmt)+prec_num);
if ~isempty(indice_cmax)
    indice_cmax;
    cmt(indice_cmax)=NaN;
    dcarb(indice_cmax)=NaN;
    %interp1(MOTH.wmt_max,MOTH.cmt_max,wmt(indice_cmax))
    %ERR=MException('BackwardModel:calc_mt','Le moteur ne satisfait pas les performances dynamiques demandees');
    %return;
end

indice=find(cmt<interp1(MOTH.wmt_max,MOTH.cmt_min,wmt));
if ~isempty(indice)
    % Il faut mettre des freins mecaniques
    ctmp=interp1(MOTH.wmt_max,MOTH.cmt_min,wmt(indice));
    cfrein_meca_mt(indice)=cmt(indice)-ctmp;
    cmt(indice)=ctmp;
end

% NaN pour tous les points a couple negatifs (hors cartographie).
% s'il y a des points a couple positif, c'est pas normal !
ind=find(isnan(dcarb) & cmt<0 & ~isnan(wmt));
if ~isempty(ind)
    % Calcul de la conso pour le regime a couple nul
    if iscolumn(ind)
        ind=ind';
    end
    if nargin==11 && fit==1
        dcarb0=MOTH.c3.*wmt(ind).*wmt(ind)+MOTH.c6.*wmt(ind);
    else
        dcarb0=interp2(MOTH.Reg_2dconso,MOTH.Cpl_2dconso,MOTH.Conso_2d',wmt(ind),0);
    end
    % Calcul du couple de frottement pour le regime
    cmt_f=interp1(MOTH.wmt_max,MOTH.cmt_min,wmt(ind));
    % interpolation en courbe de frottement (conso nulle) et conso a couple nul
    if isrow(dcarb(ind))
        dcarb(ind)=-(dcarb0./cmt_f).*cmt(ind)+dcarb0;
    else
        dcarb(ind)=-(dcarb0./cmt_f').*cmt(ind)'+dcarb0;
    end
end

% Calcul du couple indique
cmt_f=interp1(MOTH.wmt_max,MOTH.cmt_min,wmt);
cind=cmt-cmt_f;