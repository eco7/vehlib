% Function: calc_mt_niv0
% fonction de calcul de la consommation du moteur thermique
%
% C Copyright IFSTTAR LTE 1999-2011
%
% Objet:
%
% Arguments appel :
% Arguments de sortie :
%
% 29-05-2012 (BJ) : version 1

function [ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt_niv0(ERR,MOTH,cmt,wmt,dwmt,elhyb,J,calc_lim)

% ERR=[];
if sum(size(cmt))~=sum(size(wmt))
    chaine=' taille argument entree incompatible \n size(cmt) : %s\n  size(wmt) : %s\n ';
    ERR=MException('BackwardModel:calc_mt',chaine,num2str(size(cmt)),num2str(size(wmt)));
    [dcarb,cmt,cfrein_meca_mt]=backward_erreur(length(cmt));
    return;
end

if J~=0
    ERR=MException('BackwardModel:calc_mt','L''inertie moteur thermique non prise en compte localement :');
    [dcarb,cmt,cfrein_meca_mt]=backward_erreur(length(cmt));
    return;
end

% Initialisation du couple de freinage mecanique calcule sur l'arbre du moteur thermique
cfrein_meca_mt=zeros(size(cmt));
dcarb=zeros(size(cmt))*NaN;
charge=zeros(size(cmt))*NaN;
emth=zeros(size(cmt))*NaN;
pmeff=zeros(size(cmt))*NaN;
pmind=zeros(size(cmt))*NaN;
ind=find(~isnan(cmt)&~isinf(cmt));
% dcarb(ind)=interp2(MOTH.Reg_2dconso,MOTH.Cpl_2dconso,MOTH.Conso_2d',wmt(ind),cmt(ind));

if MOTH.ntypmoth == 2
    indice=find(cmt<interp1(MOTH.wmt_max,MOTH.cmt_min,wmt));
    if ~isempty(indice)
        % Il faut mettre des freins mecaniques
        ctmp=interp1(MOTH.wmt_max,MOTH.cmt_min,wmt(indice));
        cfrein_meca_mt(indice)=cmt(indice)-ctmp;
        cmt(indice)=ctmp;
    end
    
    % Charge moteur
    charge=cmt./interp1(MOTH.wmt_max,MOTH.cmt_max,wmt);
    % Pression moyenne effective
    pmeff=MOTH.Rev*2*pi.*cmt./(MOTH.Ncyl*MOTH.Vd);
    % Pression moyenne indiquee
    pmind=(cmt-interp1(MOTH.wmt_max,MOTH.cmt_min,wmt)).*MOTH.Rev*2*pi./(MOTH.Ncyl*MOTH.Vd);
    % Rendement thermodynamique
    emth(charge<=MOTH.C1)=1;
    emth(charge>MOTH.C1)=MOTH.RendG1.*charge(charge>MOTH.C1)+MOTH.RendG2;
    emth=MOTH.RendTh.*emth;
    emth=(-(wmt-MOTH.RendA).^2+MOTH.RendB).*emth;
    
    % Expression du debit de carburant
    dcarb=1000*(MOTH.Ncyl*MOTH.Vd./MOTH.pci).*wmt./(MOTH.Rev*2*pi).*(1./emth).*pmind;
    
    % prise en compte des limitations
    indice_arret_mot=find(wmt<MOTH.ral & elhyb==1);
    if ~isempty(indice_arret_mot)
        %ERR=MException('BackwardModel:calc_mt','Des valeurs de vitesse moteur inferieures au ralenti');
        'calc_mt: Des valeurs de vitesse moteur inferieures au ralenti'
        wmt(indice_arret_mot)
        %wmt(indice_arret_mot)=MOTH.ral;
    end
    
    indice_wmax=find(wmt>MOTH.wmt_maxi);
    if ~isempty(indice_wmax)
        wmt(indice_wmax)=NaN;
        dcarb(indice_wmax)=NaN;
        if sum(sum(wmt>MOTH.wmt_maxi))==numel(wmt)
            chaine=' vitesse moteur thermique superieure ?? valeur max \n indice : %s\n wmt : %s \n  MOTH.wmt_maxi %s \n';
            ERR=MException('BackwardModel:calc_mt',chaine,num2str(indice_wmax),num2str(wmt),num2str(MOTH.wmt_maxi));
            [dcarb,cmt,cfrein_meca_mt]=backward_erreur(length(cmt));
            return;
        end
    end
    
    prec_num=1e-10;
    indice_cmax=find(cmt>interp1(MOTH.wmt_max,MOTH.cmt_max,wmt)+prec_num);
    if ~isempty(indice_cmax)
        indice_cmax;
        cmt(indice_cmax)=NaN;
        dcarb(indice_cmax)=NaN;
        %interp1(MOTH.wmt_max,MOTH.cmt_max,wmt(indice_cmax))
        %ERR=MException('BackwardModel:calc_mt','Le moteur ne satisfait pas les performances dynamiques demandees');
        %return;
    end
elseif MOTH.ntypmoth == 3
    % Model ISAT- Integration Alice 
    
    N = wmt*30/pi;
   
    Cmin = -(MOTH.PMF_cst + MOTH.PMF_N*N)*MOTH.Vd/(MOTH.Rev*2*pi);
    Cmax = (MOTH.Tmax_N2*N.^2 + MOTH.Tmax_N*N + MOTH.Tmax_cst)*MOTH.Tmax/MOTH.Tmax_Tref;
    
    indice=find(cmt<Cmin);
    if ~isempty(indice)
        % Il faut mettre des freins mecaniques
        cfrein_meca_mt(indice)=cmt(indice)-Cmin(indice);
        cmt(indice)=Cmin(indice);
    end

    % Limitation du couple maxi
    cmt(cmt>Cmax) = NaN;
    
    % Gestion des sur vitesses
    wmt(wmt>MOTH.wmt_maxi) = NaN;
 
    %%% Calcul de la conso
    % Conso pour couple positif
    dcarb = (wmt.*cmt + wmt.*(MOTH.PMF_cst + MOTH.PMF_N*N)*MOTH.Vd/(MOTH.Rev*2*pi))...
        ./(MOTH.n_fi*(MOTH.n_c0-MOTH.n_cA./(MOTH.n_cB+N))*MOTH.pci);
    dcarb(dcarb<=0) = 0;
    
    
end

