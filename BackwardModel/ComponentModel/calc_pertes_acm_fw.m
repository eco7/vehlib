% function [ERR,qacm1]=calc_pertes_acm_fw(ERR,ACM,pacm1,wacm1,dwacm1,extrap);
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Fonction de calcul de la machine electrique :
%
% Arguments appel :
% Arguments de sortie :
% 
% function [ERR,qacm1]=calc_pertes_acm(ERR,ACM,cacm1,wacm1,dwacm1);
% Calcul des pertes de la machine
%
% 26-08-2011 (EV) : version 1

function [ERR,qacm1]=calc_pertes_acm_fw(ERR,ACM,pacm1,wacm1,dwacm1,extrap,fit_pertes_acm);

if sum(size(pacm1))~=sum(size(wacm1))
    chaine=' taille argument entree incompatible \n size(pacm1) : %s\n  size(wacm1) : %s\n ';
    ERR=MException('BackwardModel:calc_pertes_acm_fw',chaine,num2str(size(pacm1)),num2str(size(wacm1)));
    qacm1=backward_erreur(size(pacm1));
    return
end

ERR=[];
qacm1=NaN*ones(size(pacm1));
ind=find(~isnan(pacm1) & ~isinf(pacm1));

if (nargin==6 && extrap==1) | (nargin==7 && extrap==1 && fit_pertes_acm~=1)
    qacm1(ind)=interp2(ACM.Regmot_pert,ACM.Pelec_pert,ACM.Pertes_pelec,wacm1(ind),pacm1(ind),'linear',max(max(ACM.Pertes_pelec)));

elseif nargin==7 && fit_pertes_acm==1
    sP1=sign(wacm1);
    sP1(wacm1==0)=1;
    wacm1=abs(wacm1);
    
    A1=ACM.c1*wacm1.*wacm1+ACM.c4.*wacm1+ACM.c7;
    A2=(ACM.c2.*(wacm1.^3)+ACM.c5*wacm1.*wacm1+ACM.c8.*wacm1);
    A3=ACM.c3.*(wacm1.^4)+ACM.c6.*(wacm1.^3);
    
    a=A1;
    b=-A2-2.*pacm1.*A1-wacm1.*wacm1;
    c=A1.*pacm1.*pacm1+A2.*pacm1+A3;
    
    delta=b.*b-4.*a.*c;
    qacm1=min((-b-sqrt(delta))./(2*a),(-b+sqrt(delta))./(2*a));
    qacm1(delta<0)=NaN;
    

else
    qacm1(ind)=interp2(ACM.Regmot_pert,ACM.Pelec_pert,ACM.Pertes_pelec,wacm1(ind),pacm1(ind));
end

if sum(isnan(qacm1))== numel(qacm1)
    ind=find(isnan(qacm1));
    chaine=' Probleme de calcul des pertes Machine electrique !\n ind : %s \n pacm1 : %s \n  wacm1 : %s \n qacm1 : %s \n ';
    ERR=MException('BackwardModel:calc_pertes_acm',chaine,num2str(ind),num2str(pacm1),num2str(wacm1),num2str(qacm1));
    return;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
