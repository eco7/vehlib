% function [cmt, dcarb] = calc_extrap_conso(VD, param, wmt, cmt, cmt_noSat, dcarb)
%
% Jef PHILIPPINE - 28/02/2019
%
% Fonction de recalcul du couple et du debit carburant pour les points de
% couple superieur au couple max.
% Les points a couple inferieur au couple min ne sont pas recalcules.
%
% La carto moteur est interpolee avec griddedInterpolant (fonction Matlab
% native), puis extrapolee pour les points de couple superieur au couple
% max.
%
% INPUTS
%     VD : structure VD classique. VD.INTP nous interesse.
%     INTP : structure d'interpolation. Contient INTP.F, structure de type
%     griddedInterpolant (cf. plus bas)
%     wmt : regime moteur (rad/s)
%     cmt_noSat : couple moteur AVANT saturation par la fonction calc_mt.m (Nm)
%     dcarb : debit carburant calcule en sortie de calc_mt.m (g/s)
%
% OUTPUTS
%     cmt : couple moteur recalcule avec des points en dehors de la carto (Nm)
%     dcarb : debit carburant recalcule avec des points en dehors de la carto (g/s)
%
% Exemple de commande pour INTP.F :
%
%     INTP.verbose = 1; % Afficher les indices des points exatrapoles
%     [INTP.N1, INTP.C1] = ndgrid(VD.MOTH.Reg_2dconso, VD.MOTH.Cpl_2dconso);
%     INTP.F = griddedInterpolant(INTP.N1, INTP.C1, VD.MOTH.Conso_2d, 'linear', 'linear');
%     VD.INTP = INTP;
%
% See also calc_VTH_BV


function [cmt, dcarb] = calc_extrap_conso(VD, param, wmt, cmt, cmt_noSat, dcarb)

if VD.MOTH.ntypmoth == 10
    % Verifier l'existence du champ VD.INTP
    if ~isfield(VD, 'INTP')
        error('Error in calc_extrap_conso: field VD.INTP unknown. Please create one (cf. example provided in calc_extrap_conso.m)')
    end
    
    % Trouver les points de depassement (transformes en NaN par calc_mt)
    indice_cmax = find(isnan(dcarb) & cmt_noSat>0);
    
    % Recuperation des anciennes valeurs de couple avant saturation
    cmt(indice_cmax) = cmt_noSat(indice_cmax);
    
    % Recalcul du couple moteur en dehors de la cartographie
    dcarb(indice_cmax) = VD.INTP.F(wmt(indice_cmax), cmt(indice_cmax));
    
elseif VD.MOTH.ntypmoth == 3
    
    % Trouver les points de depassement (transformes en NaN par calc_mt)
    indice_cmax = find(isnan(cmt) & cmt_noSat>0);
    
    % Recuperation des anciennes valeurs de couple avant saturation
    cmt(indice_cmax) = cmt_noSat(indice_cmax);
    
    % Recalcul du debit carburant
    N = wmt * 30 / pi;
    dcarb(indice_cmax) = (wmt(indice_cmax).*cmt(indice_cmax) + wmt(indice_cmax).*(VD.MOTH.PMF_cst + VD.MOTH.PMF_N*N(indice_cmax))*VD.MOTH.Vd/(VD.MOTH.Rev*2*pi))...
        ./(VD.MOTH.n_fi*(VD.MOTH.n_c0-VD.MOTH.n_cA./(VD.MOTH.n_cB+N(indice_cmax)))*VD.MOTH.pci);
end

% Display informations
if ~isempty(indice_cmax) && param.verbose == 1
    if iscolumn(indice_cmax) % Si column, le display plante, donc transformation vecteur
        indice_cmax =  indice_cmax';
    end
    disp(['calc_VTH_BV: points de conso extrap. aux indices: ', num2str(indice_cmax)]);
end


end
