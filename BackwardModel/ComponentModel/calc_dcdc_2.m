% function [ERR,pertes_dcdc]=calc_dcdc_2(ERR,DCDC,PHT,UBT,UHT)

% © Copyright IFSTTAR LTE 1999-2011 
%
% entree :
% ERR
% DCDC : Donnees du DCDC
% PHT : Puissance côté haute tension
% UBT : Tension Basse tension
% UHT : Tension Haute tension


function [ERR,pertes_dcdc]=calc_dcdc_2(ERR,DCDC,PHT,UBT,UHT)


alpha=UBT./UHT;
alpha(PHT>0)=1-UBT(PHT>0)./UHT(PHT>0);

IHT=PHT./UHT;
IBT=IHT.*UHT./UBT;

Pcond=(alpha.*DCDC.Vce+(1-alpha).*DCDC.VD).*abs(IBT); % Pertes conductions

tr=DCDC.tr*abs(IBT)/DCDC.Icnom ; % temps montee courant
tf=DCDC.tf*abs(IBT)/DCDC.Icnom ; % temps descente courant
Pc= 1/2*(tr+tf)*DCDC.fh.*UHT.*abs(IBT) ; % Pertes commutations

%Qrr= DCDC.Qrr*IBT/DCDC.Idnom; % charge de recouvrement inverse si ponderee
%par courant
Qrr= DCDC.Qrr;
Pdr=DCDC.fh*UHT*Qrr.*(PHT>0) ; % Pertes recouvrement diode, si PHT ==0 pas de pertes

pertes_dcdc=Pcond+Pc+Pdr;

ERR=[];