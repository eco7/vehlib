
% function [ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,mode_fct)
%
% Copyright IFSTTAR LTE 1999-2011 
% Object: The function bilan_masse_inertie calculates the total mass of the vehicle
% and global inertia at the wheels.
%
% Input Arguments
% ERR       : structure used to specify errors
% VD       : structure containing Vehicle Data (specifying vehicle characteristics)
% vehlib   : structure used to specify vehicle architecture and component files
% mode_fct : not used for now
% 
% Output arguments :
% ERR     : structure used to specify errors
% masse  : total mass for the vehicle (size is VD.CYCL.temps)
% Jveh   : total inertia calculated on the vehicle wheel (size is VD.CYCL.temps)
%
% Attention : Inertia from ICE or ACM1 is not present because it is
% calculated locally on the component shaft

% 26-08-11 (EV): version 1 


function [ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,mode_fct)


if isfield(VD.CYCL,'ntypcharge') && VD.CYCL.ntypcharge==2
    % Dans ce cas, le cycle comprend la masse totale du vehicule.
    masse=interp1(VD.CYCL.PKMass,VD.CYCL.massFpk,VD.CYCL.distance,'linear',VD.CYCL.massFpk(end)); % reechantillonage masse fonction distance calculee.
    Jveh=masse*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
    return
end
if isfield(VD.CYCL,'ntypcharge') && VD.CYCL.ntypcharge==3
    % Dans ce cas, le cycle comprend une charge additionnelle
    charge=interp1(VD.CYCL.PKMass,VD.CYCL.massFpk,VD.CYCL.distance,'linear',0); % reechantillonage masse fonction distance calculee.
else
    charge=zeros(size(VD.CYCL.temps));
end


switch vehlib.architecture
case 'VELEC_BAT_SC'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.VEHI.nbacm1*VD.ACM1.Masse_mg+VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar)+ ...
        VD.SC.Masse_cell*VD.SC.Nblocser.*VD.SC.Nbranchepar)*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'VELEC'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.VEHI.nbacm1*VD.ACM1.Masse_mg+VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'VTH_BV'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.MOTH.Masse_mth)*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'VTH_CPLHYD'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.MOTH.Masse_mth)*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'HP_BV_2EMB'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.MOTH.Masse_mth+VD.ACM1.Masse_mg+VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'HP_BV_2EMB_SC'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.MOTH.Masse_mth+VD.ACM1.Masse_mg+VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar)+ ...
        VD.SC.Masse_cell*VD.SC.Nblocser*VD.SC.Nbranchepar)*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'HSER_GE'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.VEHI.nbacm1*VD.ACM1.Masse_mg+VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar)+ ...
        VD.ACM2.Masse_mg+VD.MOTH.Masse_mth)*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue+VD.VEHI.nbacm1*VD.ACM1.J_mg.*VD.RED.kred^2;    
case 'HSER_PAC'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.VEHI.nbacm1*VD.ACM1.Masse_mg+VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar)+ ...
        VD.PAC_PILE.Masse_cell*VD.PAC_PILE.Ncell+VD.PAC_PILE.Masse_spac)*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue+VD.VEHI.nbacm1*VD.ACM1.J_mg.*VD.RED.kred^2;
case 'HP_VELO_PAC'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.VEHI.nbacm1*VD.ACM1.Masse_mg+VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar)+ ...
        VD.PAC_PILE.Masse_cell*VD.PAC_PILE.Ncell+VD.PAC_PILE.Masse_spac)*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue+VD.VEHI.nbacm1*VD.ACM1.J_mg.*VD.RED.kred^2;
case 'HPDP_EPI_SURV'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'HPDP_EVT_SURV'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'HPDP_EVT_SURV_RED'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'HSP_2EMB'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'HSP_BV_2EMB'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'HSP_BV_2EMB_CPL'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;   
case 'HSP_PLR_2EMB_CPL'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;   
case 'HSP_PLR_BV'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;    
case 'HSP_2EMB_RED'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'HSP_1EMB'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'HSP_1EMB_RED'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'HSDP_EPI'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'HP_BV_1EMB'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.MOTH.Masse_mth+VD.ACM1.Masse_mg+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;
case 'GENERAL'
    masse=charge+(VD.VEHI.Mveh+VD.VEHI.Charge+VD.ACM1.Masse_mg+VD.ACM2.Masse_mg+VD.MOTH.Masse_mth+ ...
        VD.BATT.Masse_bloc*(VD.BATT.Nblocser*VD.BATT.Nbranchepar))*ones(size(VD.CYCL.temps));
    Jveh=masse.*VD.VEHI.Rpneu^2+VD.VEHI.Nbroue*VD.VEHI.Inroue;       
    
otherwise
    ERR=MException('BackwardModel:bilan_masse_inertie',strcat('Architecture non reconnue : ',vehlib.architecture));
end

return
