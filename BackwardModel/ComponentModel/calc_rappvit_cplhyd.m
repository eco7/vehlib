% Function: calc_rappvit_cplhyd
% Choix du rapport de boite de vitesse pour les vehicule avec coupleur hydraulique
%
% © Copyright IFSTTAR LTE 1999-2011
%
% Objet:
% Choix du rapport de boite de vitesse
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (BJ) : version 1

function [ERR,rappvit,wmt,cmt,dwmt,dcarb,elhyb,etat_cplhyd]=calc_rappvit_cplhyd(ERR,VD,param,cmt,wmt,dwmt,dcarb,elhyb,etat_cplhyd,J)

if J~=0
    ERR=MException('BackwardModel:calc_rappvit_cplhyd','L''inertie moteur thermique non prise en compte localement :');
    cmt=[];
    wmt=[];
    dwmt=[];
    return;
end

if VD.ECU.bv_auto==2
    % Il s'agit d'une loi de changement de rapport sophistiquee (!)
    % On code une loi arbitraire
    delta_wmt=(VD.MOTH.wmt_maxi-VD.MOTH.ral)/5;
    VD.ECU.ouvert_chrap_M=[0 1];
    VD.ECU.wmt_chrap_M=[VD.MOTH.ral+3*delta_wmt VD.MOTH.wmt_maxi];
    VD.ECU.ouvert_chrap_D=[0 1];
    VD.ECU.wmt_chrap_D=[VD.MOTH.ral VD.MOTH.ral+2*delta_wmt];
    %plot(VD.ECU.ouvert_chrap_M,VD.ECU.wmt_chrap_M,VD.ECU.ouvert_chrap_D,VD.ECU.wmt_chrap_D)
end


% Calcul de la charge du moteur
charge=zeros(size(cmt));
charge=cmt./interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt);
charge(~isnan(charge) & ~isinf(charge))=max(0,charge(~isnan(charge) & ~isinf(charge)));
charge(~isnan(charge) & ~isinf(charge))=min(1,charge(~isnan(charge) & ~isinf(charge)));
charge(isnan(dcarb))=NaN;
charge(isinf(dcarb))=NaN;

% Initialisation des vecteurs
rappvit=ones(1,length(cmt(1,:)));
wmt_v=ones(1,length(cmt(1,:)));
cmt_v=ones(1,length(cmt(1,:)));
dcarb_v=ones(1,length(cmt(1,:)));
elhyb_v=ones(1,length(cmt(1,:)));
etat_cplhyd_v=ones(1,length(cmt(1,:)));

if ~isfield(param,'optim_rapport') || param.optim_rapport == 0
    % "Boite automatique"
    % Boucle pour le choix du rapport de boite.
    % Initialisation
    if VD.CYCL.vitesse(1)==0
        rappvit(1)=0;
        wmt_v(1)=wmt(rappvit(1)+1,1);
        cmt_v(1)=cmt(rappvit(1)+1,1);
        dcarb_v(1)=dcarb(rappvit(1)+1,1);
        elhyb_v(1)=elhyb(rappvit(1)+1,1);
    else
        w_M=interp1(VD.ECU.ouvert_chrap_M,VD.ECU.wmt_chrap_M,charge(:,1));
        w_D=interp1(VD.ECU.ouvert_chrap_D,VD.ECU.wmt_chrap_D,charge(:,1));
        % On choisit le rapport le plus grand possible
        i_M=find(~isnan(w_M));
        i_D=find(~isnan(w_D));
        rappvit(1)=i_M(max(find(i_M==i_D)))-1; % !
        while wmt(rappvit(1)+1,1)<VD.MOTH.ral && rappvit(1)>1
            rappvit(1)=max(rappvit(1)-1,1);
        end
        wmt_v(1)=wmt(rappvit(1)+1,1);
        cmt_v(1)=cmt(rappvit(1)+1,1);
        dcarb_v(1)=dcarb(rappvit(1)+1,1);
        elhyb_v(1)=elhyb(rappvit(1)+1,1);
        if rappvit(1)==1
            etat_cplhyd_v(1)=etat_cplhyd(rappvit(1)+1,1);
        else
            etat_cplhyd_v(1)=0;
        end
    end
    
    for t=2:length(cmt(1,:))
        if VD.CYCL.vitesse(t)==0
            rappvit(t)=0;
        elseif VD.CYCL.vitesse(t-1)==0 && VD.CYCL.vitesse(t)~=0
            rappvit(t)=1;
        else
            rappvit(t)=rappvit(t-1);
            if wmt(rappvit(t-1)+1,t)>interp1(VD.ECU.ouvert_chrap_M,VD.ECU.wmt_chrap_M,charge(rappvit(t)+1,t))
                rappvit(t)=rappvit(t-1)+1;
                rappvit(t)=min(VD.ECU.nbmax_rapbv,max(0,rappvit(t)));
            end
            if wmt(rappvit(t-1)+1,t)<interp1(VD.ECU.ouvert_chrap_D,VD.ECU.wmt_chrap_D,charge(rappvit(t)+1,t))
                rappvit(t)=rappvit(t-1)-1;
            end
            rappvit(t)=min(VD.ECU.nbmax_rapbv,max(0,rappvit(t)));
            % Le test suivant pour savoir si l'on est en traction realise en première
            % (au PM, la charge moteur peut-etre nulle)
            if charge(2,t)>0 && VD.CYCL.vitesse(t)~=0
                % on passe au travers de ce cas
                rappvit(t)=max(1,rappvit(t));
            end
        end
        while wmt(rappvit(t)+1,t)<VD.MOTH.ral && rappvit(t)>1
            % Gestion des sous regimes
            rappvit(t)=max(rappvit(t)-1,1);
        end
        while wmt(rappvit(t)+1,t)>VD.MOTH.wmt_maxi && rappvit(t)<VD.ECU.nbmax_rapbv
            % Gestion des sur regimes
            rappvit(t)=max(rappvit(t)+1,VD.ECU.nbmax_rapbv);
        end
        
        dcarb_v(t)=dcarb(rappvit(t)+1,t);
        cmt_v(t)=cmt(rappvit(t)+1,t);
        wmt_v(t)=wmt(rappvit(t)+1,t);
        elhyb_v(t)=elhyb(rappvit(t)+1,t);
        if rappvit(t)==1
            etat_cplhyd_v(t)=etat_cplhyd(rappvit(t)+1,t);
        else
            etat_cplhyd_v(t)=0;
        end
    end
else
    % Optimisation des rapports de boite
    
    % On enleve le PM si vitesse vehicule non nulle
    dd=dcarb(1,:);
    dd(VD.CYCL.vitesse~=0)=NaN;
    dcarb=[dd; dcarb(2:end,:)];
    
    rappvit=zeros(size(VD.CYCL.temps))*NaN;
    % La consommation minimum nous donne le rapport de boite
    [dcarb_v,indice]=min(dcarb,[],1);
    rappvit=indice-1;
    [sx, sy]=size(dcarb);
    ii=0:(sy-1);
    j=indice+ii*sx;
    cmt_v=cmt(j);
    wmt_v=wmt(j);
    elhyb_v=elhyb(j);    
    if sum(isnan(rappvit))
        'Aieee'
    end
end

% on ecrase les matrices wmt,cmt,dcarb,elhyb
dcarb=dcarb_v;
cmt=cmt_v;
wmt=wmt_v;
elhyb=elhyb_v;
etat_cplhyd=etat_cplhyd_v;

% Quelques tests sur les conditions moteur
indice_arret_mot_dysfunct=find(wmt<VD.MOTH.ral & elhyb==1 & rappvit~=0);
if ~isempty(indice_arret_mot_dysfunct)
    disp(['calc_rappvit_cplhyd: Probleme de calcul du moteur thermique aux indices: ',num2str(indice_arret_mot_dysfunct)]);
    %wmt(indice_arret_mot_dysfunct)
    %cmt(indice_arret_mot_dysfunct)
    %rappvit(indice_arret_mot_dysfunct)
    %ERR=MException('BackwardModel:calc_rappvit_cplhyd','Des valeurs de vitesse moteur inferieures au ralenti hors Point Mort');
end

