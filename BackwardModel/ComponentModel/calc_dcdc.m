% function [ERR,P2]=calc_dcdc(type,ERR,DCDC,U1,P1)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% type 1: Calcul de la puissance en amont du DC/DC (cote bus continu)
% type 2: Calcul de la puissance en aval du DC/DC (cote source)
% dans ce cas, on ne peut tester la tension cote source
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (BJ) : version 1

function [ERR,P2]=calc_dcdc(type,ERR,DCDC,U1,P1)
%#eml
eml.extrinsic('MException')

if type==1
% indice 1: cote source
% indice 2: cote bus continu
P2=ones(size(P1))*NaN;

% Rendement
P2(P1>=0)=P1(P1>=0).*DCDC.rend;
P2(P1<0)=P1(P1<0)./DCDC.rend;


% Limitation en puissance cote source
P2(P1>=DCDC.Pmax)=NaN;
P2(P1<DCDC.Pmin)=NaN;

% Limitation en tension cote source
P2(U1>=DCDC.Umax)=NaN;
P2(U1<DCDC.Umin)=NaN;

[Li,Co]=size(P1);
if sum(sum(isnan(P2)))==Li*Co
    chaine=' Tous les elements cote DCDC= NaN';
    ERR=MException('BackwardModel:calc_dcdc',chaine);
end

elseif type==2

% indice 1: cote bus continu
% indice 2: cote source
P2=ones(size(P1))*NaN;


% Rendement
P2(P1>=0)=P1(P1>=0)./DCDC.rend;
P2(P1<0)=P1(P1<0).*DCDC.rend;

% Limitation en puissance cote source
P2(P2>=DCDC.Pmax)=NaN;
P2(P2<DCDC.Pmin)=NaN;

[Li,Co]=size(P1);
if sum(sum(isnan(P2)))==Li*Co
    chaine=' Tous les elements cote DCDC= NaN';
    ERR=MException('BackwardModel:calc_dcdc',chaine);
end


end
return;


