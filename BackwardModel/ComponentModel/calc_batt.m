% function [ERR,Ibat,Ubat,soc,ah,E,R,RdF,Ibat_min_calc]=calc_batt(ERR,BATT,pas,pbat,soc_p,ah_p,pres,psc,param)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul des courant, tension batterie ?? partir de la puissance (cas backward)
%
% Arguments appel
% ERR : struct pour la gestion des erreurs qui seront gerees par les programmes appelant
% BATT : structure vehlib pour renseigner l'architecture batterie
% pas : pas d'integration
% pbat : puissance en sortie de la batterie
% soc_p, ah_p : soc et ampere.heure ?? l'instant precedent 
% pres,psc,param : parametre de gestion d'erreur (expliquer ici la gestion
% des erreurs)

% Arguments de sortie :
% ERR : 
% Ibat,Ubat,soc,ah,E,R,RdF,Ibat_min_calc :
% 
% 27_05_2011 (EV) : rajout calc des limites de courant pour ne pas d??passer
% les limites en tension : en programmation dynamique (pour le calcul des limites) dans ce cas on ne
% renvoi pas NaN mais le courant limit?? (cas param==prog_dyn). Dans les
% autres cas on renvoi NaN.

% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (BJ) : version 1
% 26-08-2011 (EV) : version 1

function [ERR,Ibat,Ubat,soc,ah,E,R,RdF,Ibat_min_calc,Ubat_ibat_min_calc,Ibat_max_calc,Ubat_ibat_max_calc]=calc_batt(ERR,BATT,pas,pbat,soc_p,ah_p,pres,psc,param,calc_ibmin_ibmax)
%#eml
eml.extrinsic('MException')

% pbat par elements
pbat=pbat/(BATT.Nblocser*BATT.Nbranchepar);

% pres et psc ne sont la que pour un calcul des limites batteries, peuvent
% etre mis a zero (voir pas int??gr?? ?? l'interface) si inutile pres est un scalaire

% recherche des donnees pour T = 20 degr??es (EV :25-02-10)
if isfield(BATT,'tbat')
   [~,Li_T20]=min(abs(BATT.tbat-20));
elseif isfield(BATT,'Tbat_table')
   [~,Li_T20]=min(abs(BATT.Tbat_table-20));
else 
   Li_T20=1;
end

E=interp1(BATT.dod_ocv,BATT.ocv(Li_T20,:),100-soc_p,'linear','extrap');
RdF=ones(size(pbat));
RdF(pbat<0)=BATT.RdFarad;
Ibat_min=interp1(BATT.DoD_Ibat_min,BATT.Ibat_min,100-soc_p,'linear','extrap');
Ibat_max=interp1(BATT.DoD_Ibat_max,BATT.Ibat_max,100-soc_p,'linear','extrap');

R=NaN*zeros(size(pbat));

if isscalar(soc_p) & ~isscalar(pbat) % pour l'interpolation on est oblige de generer un soc_p de la taille de pbat
    soc_pinterp=soc_p*ones(size(pbat));
    if BATT.ntypbat==1
        R(pbat>=0)=interp1(BATT.dod,BATT.Rd(Li_T20,:),100-soc_pinterp(pbat>=0),'linear','extrap');
        R(pbat<0)=interp1(BATT.dod,BATT.Rc(Li_T20,:),100-soc_pinterp(pbat<0),'linear','extrap');
    elseif BATT.ntypbat==2
         R(pbat>=0)=interp1(BATT.dod_ocv,BATT.rd(Li_T20,:),100-soc_pinterp(pbat>=0),'linear','extrap');
        R(pbat<0)=interp1(BATT.dod_ocv,BATT.rc(Li_T20,:),100-soc_pinterp(pbat<0),'linear','extrap');
    elseif BATT.ntypbat==3
        R(pbat>=0)=interp1(BATT.dod,BATT.Rd(Li_T20,:),100-soc_pinterp(pbat>=0),'linear','extrap');
        R(pbat<0)=interp1(BATT.dod,BATT.Rc(Li_T20,:),100-soc_pinterp(pbat<0),'linear','extrap');
    end
else
    if BATT.ntypbat==1
        R(pbat>=0)=interp1(BATT.dod,BATT.Rd(Li_T20,:),100-soc_p(pbat>=0),'linear','extrap');
        R(pbat<0)=interp1(BATT.dod,BATT.Rc(Li_T20,:),100-soc_p(pbat<0),'linear','extrap');
    elseif BATT.ntypbat==2
        R(pbat>=0)=interp1(BATT.dod_ocv,BATT.rd(Li_T20,:),100-soc_p(pbat>=0),'linear','extrap');
        R(pbat<0)=interp1(BATT.dod_ocv,BATT.rc(Li_T20,:),100-soc_p(pbat<0),'linear','extrap');
    elseif BATT.ntypbat==3
        R(pbat>=0)=interp1(BATT.dod,BATT.Rd(Li_T20,:),100-soc_p(pbat>=0),'linear','extrap');
        R(pbat<0)=interp1(BATT.dod,BATT.Rc(Li_T20,:),100-soc_p(pbat<0),'linear','extrap');
    end
end

delta=E.^2-4*R.*pbat;

delta(delta<0)=NaN; % on elimine tout de suite delta<0 pour passer embeded
Ibat=(E-sqrt(delta))./(2*R);
Ubat=E-R.*Ibat;

% gestion des erreurs
% A ce moment si usc superieure aux limites on as des NaN
% Il faut limiter ubat, delta< 0; ibat > ibat_max ;ibat <ibat_min (sauf cas
% ci dessous.

% Pour ibat_min:
% le seul cas possible et pas cretin est pres<0,psc<0 et pbatt<0 mais
% ibatt<ibatt_min. alors on limite pres par frein meca. On peut alors 
% considerer que ibat = ibat_min les autres cas ou ibat<ibatmin sont
% impossible ou cretin (on ne vas pas propulser et freiner en meme temps).

if nargin==7 & pres<0
    if isscalar(Ibat_min)
        Ibat(pbat<0 & Ibat<Ibat_min)=Ibat_min;
    else
        Ibat(pbat<0 & Ibat<Ibat_min)=Ibat_min(pbat<0 & Ibat<Ibat_min);
    end
    Ubat=E-R.*Ibat;
end

if nargin==8 & pres<0
    if isscalar(Ibat_min)
        Ibat(pbat<0 & psc<=0 & Ibat<Ibat_min)=Ibat_min;
    else
        Ibat(pbat<0 & psc<=0 & Ibat<Ibat_min)=Ibat_min(pbat<0 & psc<=0 & Ibat<Ibat_min);
    end
    Ubat=E-R.*Ibat;
end

if nargin==9 && (strcmp('prog_dyn',param.optim) || strcmp('3U_TWC',param.optim)) || (nargin==10 && calc_ibmin_ibmax==1)
    % Si le courant min explose la tension max, on recherche alors le
    % courant min qui permettra d'atteindre la tension max (pour le calcul
    % des limites en prog_dyn.
    Ibat_min_ubatmax=(E-BATT.Ubat_max)./R;
    Ibat=max(Ibat,Ibat_min_ubatmax);
    [Ibat_min_calc]=max(Ibat_min_ubatmax,Ibat_min);
    Ubat_ibat_min_calc=(E-R.*Ibat_min_calc)*BATT.Nblocser;
    
    
    Ibat_max_ubatmin=(E-BATT.Ubat_min)./R;
    Ibat=min(Ibat,Ibat_max_ubatmin);
    [Ibat_max_calc]=min(Ibat_max_ubatmin,Ibat_max);
    Ubat_ibat_max_calc=(E-R.*Ibat_max_calc)*BATT.Nblocser;
    
    % EV 23/09/2019
    Ibat_min_calc=Ibat_min_calc*BATT.Nbranchepar;
    Ibat_max_calc=Ibat_max_calc*BATT.Nbranchepar;
end

prec_num=1e-10;


% Ibat( isnan(delta) | Ibat>(Ibat_max+prec_num)  | Ibat<(Ibat_min-prec_num) | Ubat>BATT.Ubat_max | Ubat<BATT.Ubat_min) = NaN;
Ibat( isnan(delta) | Ibat>(Ibat_max)  | Ibat<(Ibat_min-prec_num) | Ubat>BATT.Ubat_max | Ubat<BATT.Ubat_min) = NaN;

Ubat(isnan(Ibat))=NaN;


soc=soc_p-(RdF.*(Ibat/3600.*pas)/(BATT.Cahbat_nom/100)); % rajouter eventuellement limitation SOC batterie
ah=ah_p+(RdF.*Ibat.*pas/3600)*BATT.Nbranchepar;

% recalcul pour le pack
Ibat=Ibat*BATT.Nbranchepar;
Ubat=Ubat*BATT.Nblocser;
E=E*BATT.Nblocser;
R=R*BATT.Nblocser/BATT.Nbranchepar;


[Li,Co]=size(pbat);
if sum(sum(isnan(Ibat)))==Li*Co
    chaine=' Tout les elements de Ibat = NaN';
    ERR=MException('BackwardModel:calc_batt',chaine);
end
