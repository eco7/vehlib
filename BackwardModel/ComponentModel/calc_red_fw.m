% function [ERR,csec,wsec,dwsec,Jsec,Pertes_red]=calc_red_fw(ERR,RED,cprim,wprim,dwprim,Jprim)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Function: calc_red_fw
% simulation du bloc reducteur en forward
%
% Descrition:
% 
% date: 15/02/2011
% Cette fonction calcul le couple, la vitesse, l'acceleration et l'inertie
% ramenee au secondaire. Si on connait les inertie precedente il n'est pas
% necessaire de connaitre les accelerations. et visversa.
%
% --(Cprim,Jprim)-->[reducteur]--->(Csec,Jsec)
%   (wprim,dwprim)                 (wsec,dwsec)
%
% Parameters:
% 
% param1 :  erreur
% param2 :  Structure du reducteur, qui contient le rapport de reduction et
%           le rendement.
% param3 :  couple au primaire
% param4 :  vitesse au primaire
% param5 :  acceleration au primaire
% param6 :  inertie ramenee
% 
% 
% Results:
% 
%  result1: erreur
%  result2: coule au secondaire
%  result3: vitesse au secondaire
%  result4: acceleration au secondaire
%  result5: inertie ramenee
%  result6: pertes du reducteur 
%
% 26-08-2011 (VR-EV) : version 1

function [ERR,csec,wsec,dwsec,Jsec,Pertes_red]=calc_red_fw(ERR,RED,cprim,wprim,dwprim,Jprim)

% Couple sur secondaire suivant signe

kred=RED.kred;
rend=RED.rend;


A=ones(size(cprim))*rend;
csec=cprim.*(A.^sign(cprim.*wprim))*kred;

Jsec=Jprim*(A.^sign(cprim.*wprim))*kred^2;

wsec=wprim/kred;
dwsec=dwprim/kred;

% Pertes du reducteur
Pertes_red=abs(cprim.*wprim-csec.*wsec);


