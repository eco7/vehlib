% function [ERR,rappvit,wmt,cmt,dwmt,elhyb]=calc_rappvit_bv(VD,param,cmt,wmt,dwmt,elhyb,J)
%
% © Copyright IFSTTAR LTE 1999-2011
%
% Objet:
% Choix du rapport de boite de vitesse
% Arguments appel : cmt, wmt ... sont de tailles (nrapport+1,length(cycles))
% Arguments de sortie : cmt, wmt ... sont de tailles (1,length(cycles))
%
% 26-08-2011 (BJ) : version 1

function [ERR,rappvit,wmt,cmt,dwmt,elhyb,dcarb]=calc_rappvit_bv(VD,param,cmt,wmt,dwmt,elhyb,dcarb,J)

ERR=[];

if J~=0
    ERR=MException('BackwardModel:calc_rappvit_bv','L''inertie moteur thermique non prise en compte localement :');
    cmt=[];
    wmt=[];
    dwmt=[];
    return;
end

% Initialisation des vecteurs
rappvit=ones(1,length(cmt(1,:)));
wmt_v=ones(1,length(cmt(1,:)));
cmt_v=ones(1,length(cmt(1,:)));
dcarb_v=ones(1,length(cmt(1,:)));
elhyb_v=ones(1,length(cmt(1,:)));

if ~isfield(param,'optim_rapport') | param.optim_rapport == 0
    % "Boite automatique"
    % Boucle pour le choix du rapport de boite.

    % Calcul de la charge du moteur
    charge=zeros(size(cmt));
    if VD.MOTH.ntypmoth == 10
        charge=cmt./interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt);
    elseif VD.MOTH.ntypmoth == 3
        charge=cmt./((VD.MOTH.Tmax_N2*(wmt*30/pi).^2 + VD.MOTH.Tmax_N*(wmt*30/pi) + VD.MOTH.Tmax_cst)*VD.MOTH.Tmax/VD.MOTH.Tmax_Tref);
    end
    %charge=max(0,charge);
    charge(~isnan(charge) & ~isinf(charge))=max(0,charge(~isnan(charge) & ~isinf(charge)));
    charge(~isnan(charge) & ~isinf(charge))=min(1,charge(~isnan(charge) & ~isinf(charge)));
    charge(isnan(dcarb))=NaN;
    charge(isinf(dcarb))=NaN;
    
    % Initialisation
    if VD.CYCL.vitesse(1)==0
        rappvit(1)=0;
        wmt_v(1)=wmt(rappvit(1)+1,1);
        cmt_v(1)=cmt(rappvit(1)+1,1);
        dcarb_v(1)=dcarb(rappvit(1)+1,1);
        elhyb_v(1)=elhyb(rappvit(1)+1,1);
    else
        w_M=interp1(VD.ECU.ouvert_chrap_M,VD.ECU.wmt_chrap_M,charge(:,1));
        w_D=interp1(VD.ECU.ouvert_chrap_D,VD.ECU.wmt_chrap_D,charge(:,1));
        % On choisit le rapport le plus grand possible
        i_M=find(~isnan(w_M));
        i_D=find(~isnan(w_D));
        rappvit(1)=i_M(max(find(i_M==i_D)))-1; % !
        while wmt(rappvit(1)+1,1)<VD.MOTH.ral & rappvit(1)>1
            rappvit(1)=max(rappvit(1)-1,1);
        end
        wmt_v(1)=wmt(rappvit(1)+1,1);
        cmt_v(1)=cmt(rappvit(1)+1,1);
        dcarb_v(1)=dcarb(rappvit(1)+1,1);
        elhyb_v(1)=elhyb(rappvit(1)+1,1);
    end
    for t=2:length(cmt(1,:))
        if VD.CYCL.vitesse(t)==0
            rappvit(t)=0;
        elseif VD.CYCL.vitesse(t-1)==0 & VD.CYCL.vitesse(t)~=0
            rappvit(t)=1;
        else
            rappvit(t)=rappvit(t-1);
            if wmt(rappvit(t-1)+1,t)>interp1(VD.ECU.ouvert_chrap_M,VD.ECU.wmt_chrap_M,charge(rappvit(t-1)+1,t))
                rappvit(t)=rappvit(t-1)+1;
            end
            if wmt(rappvit(t-1)+1,t)<interp1(VD.ECU.ouvert_chrap_D,VD.ECU.wmt_chrap_D,charge(rappvit(t-1)+1,t))
                rappvit(t)=rappvit(t-1)-1;
            end
            rappvit(t)=min(VD.ECU.nbmax_rapbv,max(0,rappvit(t)));
            if (charge(rappvit(t)+1,t)>0 | isnan(charge(rappvit(t)+1,t)) | isinf(charge(rappvit(t)+1,t))) & VD.CYCL.vitesse(t)~=0
                % on passe au travers de ce cas
                rappvit(t)=max(1,rappvit(t));
            end
        end
        while wmt(rappvit(t)+1,t)<VD.MOTH.ral & rappvit(t)>1
            % Gestion des sous regimes
            rappvit(t)=max(rappvit(t)-1,1);
        end
        while wmt(rappvit(t)+1,t)>VD.MOTH.wmt_maxi & rappvit(t)<VD.ECU.nbmax_rapbv
            % Gestion des sur regimes
            rappvit(t)=max(rappvit(t)+1,VD.ECU.nbmax_rapbv);
        end
        
        dcarb_v(t)=dcarb(rappvit(t)+1,t);
        cmt_v(t)=cmt(rappvit(t)+1,t);
        wmt_v(t)=wmt(rappvit(t)+1,t);
        dwmt_v(t)=dwmt(rappvit(t)+1,t);
        elhyb_v(t)=elhyb(rappvit(t)+1,t);
    end
else
    % Optimisation des rapports de boite
    
    % Le moteur ne satisfait pas les contraintes de regime
    indice=find(wmt<VD.MOTH.ral);
    dcarb(indice)=NaN;
    indice=find(wmt>VD.MOTH.wmt_maxi);
    dcarb(indice)=NaN;
    
    % On enleve le PM si vitesse vehicule non nulle
    dd=dcarb(1,:);
    dd(VD.CYCL.vitesse~=0)=NaN;
    dcarb=[dd; dcarb(2:end,:)];

    rappvit=zeros(size(VD.CYCL.temps))*NaN;
    % La consommation minimum nous donne le rapport de boite
    [dcarb_v,indice]=min(dcarb,[],1);
    rappvit=indice-1;
    [sx, sy]=size(dcarb);
    ii=0:(sy-1);
    j=indice+ii*sx;
    cmt_v=cmt(j);
    wmt_v=wmt(j);
    dwmt_v=dwmt(j);
    elhyb_v=elhyb(j);
%     for t = 1:length(VD.CYCL.temps)
%         rappvit(t) = find(dcarb(:,t) == min(dcarb(:,t)),1)-1;
%         dcarb_v(t)=dcarb(rappvit(t)+1,t);
%         cmt_v(t)=cmt(rappvit(t)+1,t);
%         wmt_v(t)=wmt(rappvit(t)+1,t);
%         elhyb_v(t)=elhyb(rappvit(t)+1,t);
%     end
    
    if sum(isnan(rappvit))
        'Aieee'
    end
end

% on ecrase les matrices wmt, cmt, dcarb, elhyb
dcarb=dcarb_v;
cmt=cmt_v;
wmt=wmt_v;
dwmt=dwmt_v;
elhyb=elhyb_v;

% Rapports optimises
%[dcarb,rappvit]=min(dcarb); % cmt<0 !
%rappvit=rappvit-1;
%wmt=wmt(rappvit);
%cmt=cmt(rappvit);
%elhyb=elhyb(rappvit);

% Quelques tests sur les conditions moteur
indice_arret_mot_dysfunct=find(wmt<VD.MOTH.ral & elhyb==1 & rappvit~=0);
if ~isempty(indice_arret_mot_dysfunct)
    indice_arret_mot_dysfunct
    wmt(indice_arret_mot_dysfunct)
    rappvit(indice_arret_mot_dysfunct)
    ERR=MException('BackwardModel:calc_rappvit_bv','Des valeurs de vitesse moteur inferieures au ralenti hors Point Mort');
end

