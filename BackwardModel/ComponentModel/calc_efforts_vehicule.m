% function [ERR,croue,croue_frein,wroue,dwroue]=calc_efforts_vehicule(ERR,VD,masse,Jveh)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Arguments appel :
% Arguments de sortie :
% 
% 26-08-2011 (BJ) : version 1
function [ERR,croue,wroue,dwroue,penteFpk,Fres,Faero,Froul,Fpente,Force1,Force2,Force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh)

Fres=0;
Faero=0;
Froul=0;
Fpente=0;
Force1=0;
Force2=0;
Force3=0;
penteFpk=zeros(size(VD.CYCL.temps));

if VD.CYCL.ntyppente==2 | VD.CYCL.ntyppente==3
     % suppression des doublons pour interpolation
     %dist_calc=cumtrapz(VD.CYCL.temps,VD.CYCL.vitesse); % distance calculee a partir de la vitesse VD.CYCL.vitesse
     penteFpk=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,VD.CYCL.distance,'linear',0); % reechantillonage pente fonction distance calculee. 
     Fpente=masse.*VD.VEHI.Gpes.*sin(atan(penteFpk/100));
    if VD.CYCL.distance(end)>VD.CYCL.PKpente(end)
        %warning('BackwardModel:calc_efforts_vehicule : cycle mal defini, PKpente trop court')
    end
elseif VD.CYCL.ntyppente == 1
     % suppression des doublons pour interpolation
     penteFpk=interp1(VD.CYCL.distpente_bal,VD.CYCL.pente_bal,VD.CYCL.distance,'linear',0); % reechantillonage pente fonction distance calculee. 
     Fpente=masse.*VD.VEHI.Gpes.*sin(atan(penteFpk/100));
elseif VD.CYCL.ntyppente==0
    Fpente=masse.*VD.VEHI.Gpes.*sin(atan(VD.CYCL.pente/100));
else
    chaine=' modele de pente non reconnu ';
    ERR=MException('BackwardModel:calc_efforts_vehicule',chaine);
end

if VD.VEHI.ntypveh==1
    Froul=masse.*VD.VEHI.Gpes.*(VD.VEHI.a+VD.VEHI.b*VD.CYCL.vitesse.^2);
    Faero=(VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx/2)*VD.CYCL.vitesse.^2;   
    Fres=Froul+Faero;
elseif VD.VEHI.ntypveh==2
    Force1=VD.VEHI.fcst.*(VD.CYCL.vitesse>0.01);
    Force2=VD.VEHI.fvit1*VD.CYCL.vitesse.*(VD.CYCL.vitesse>0.01);
    Force3=VD.VEHI.fvit2*(VD.CYCL.vitesse.^2).*(VD.CYCL.vitesse>0.01);
    Fres=Force1+Force2+Force3;
elseif VD.VEHI.ntypveh==3
    Froul=masse.*VD.VEHI.Gpes.*(VD.VEHI.a0 + VD.VEHI.a1*VD.CYCL.vitesse + VD.VEHI.a2*VD.CYCL.vitesse.^2);
    Faero=(VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx/2)*VD.CYCL.vitesse.^2;   
    Fres=Froul+Faero;
elseif VD.VEHI.ntypveh==4
    Froul=((VD.VEHI.LoadDistr(1)/(100*0.85*VD.VEHI.NomLoad(1))*masse).^(VD.VEHI.beta-1)* ...
        VD.VEHI.LoadDistr(1)/100*VD.VEHI.crrIso(1)/1000*VD.VEHI.Gpes + ...
        (VD.VEHI.LoadDistr(3)/(100*0.85*VD.VEHI.NomLoad(3))*masse).^(VD.VEHI.beta-1)* ...
        VD.VEHI.LoadDistr(3)/100*VD.VEHI.crrIso(3)/1000*VD.VEHI.Gpes + ...
        (VD.VEHI.LoadDistr(4)/(100*0.85*VD.VEHI.NomLoad(4))*masse).^(VD.VEHI.beta-1)* ...
        VD.VEHI.LoadDistr(4)/100*VD.VEHI.crrIso(4)/1000*VD.VEHI.Gpes).*masse;
    Faero=(VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx/2)*VD.CYCL.vitesse.^2;   
    Fres=Froul+Faero;
else
    chaine=' modele de vehicule non reconnu ';
    ERR=MException('BackwardModel:calc_efforts_vehicule',chaine);
end

% La force de resistance a l'avancement est nulle a l'arret du vehicule, et
% opposee a la vitesse du vehicule
Fres=Fres.*sign(VD.CYCL.vitesse);
Fres=Fres+Fpente;
 
cres=Fres*VD.VEHI.Rpneu;

wroue=VD.CYCL.vitesse/VD.VEHI.Rpneu;
dwroue=VD.CYCL.accel/VD.VEHI.Rpneu;

croue=cres+Jveh.*VD.CYCL.accel/VD.VEHI.Rpneu;

return
