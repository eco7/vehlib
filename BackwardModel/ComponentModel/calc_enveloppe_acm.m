% function [ERR,wacm1,cacm1]=calc_enveloppe_acm(ERR,ACM,wacm1,cacm1,uacm1);
%
% C Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Calcul des perfos maxi de la ME au regard de ce qui est demande par le
% cycle de conduite
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (BJ) : version 1

function [ERR,wacm1,cacm1]=calc_enveloppe_acm(ERR,ACM,wacm1,cacm1,uacm1)
eps=1e-10;
ERR=[];

depsst_umax=find(uacm1>max(ACM.Tension_cont));
depsst_umin=find(uacm1<min(ACM.Tension_cont));

if ~isempty(depsst_umax)
    ERR=MException('BackwardModel:calc_enveloppe_acm','la tension demandee est superieure a la tension max du moteur');
    depsst_umax
    fprintf(1,'%s\n','Tension demandee superieure a la tension max du moteur');
end

if ~isempty(depsst_umin)
    ERR=MException('BackwardModel:calc_enveloppe_acm','la tension demandee est inferieur a la tension min du moteur');
    depsst_umin
    fprintf(1,'%s\n','Tension demandee inferieure a la tension min du moteur');
end


cacm1_max=interp2(ACM.Regmot_cmax,ACM.Tension_cont,ACM.Cmax_mot',wacm1,uacm1);
cacm1_min=interp2(ACM.Regmot_cmax,ACM.Tension_cont,ACM.Cmin_mot',wacm1,uacm1);

 
% Depassement en phase motrice
depsst_mot=find(cacm1>(cacm1_max+eps));

% Depassement en phase freinage recuperatif
depsst_recup=find(cacm1<(cacm1_min-eps));

if ~isempty(depsst_mot)
    ERR=MException('BackwardModel:calc_enveloppe_acm1','Les performances de la machine elect. ne permettent pas de satisfaire la dynamique demandee');
    depsst_mot
    fprintf(1,'%s\n','La machine ne permet pas de satisfaire les perfos');
end
if ~isempty(depsst_recup)
    ERR=MException('BackwardModel:calc_enveloppe_acm1','Les performances de la machine elect. ne permettent pas de satisfaire la dynamique demandee');
    depsst_recup
    fprintf(1,'%s\n','La machine ne permet pas de satisfaire les perfos');
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
