% function [ERR,cprim,wprim,dwprim,etat_emb]=calc_emb(VD,csec,wsec,dwsec,elhyb)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% etat_emb:
% 0   ferme
% 1   ouvert
% 0.5 glissant
% Note: La vitesse de l'arbre primaire de l'embrayage est indeterminee
% au point mort dans ce type de modele. On fait le choix d'utiliser la variable
% elhyb pour lever l'indetermination. Necessaire pour les auxiliaires.
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (BJ) : version 1

function [ERR,cprim,wprim,dwprim,etat_emb]=calc_emb2(VD,csec,wsec,dwsec,elhyb)

ERR=[];

if 0==1
    ERR=MException('BackwardModel:calc_emb','Erreur inconnue');
    cprim=[];
    wprim=[];
    dwprim=[];
    return;
end

cprim=csec;
wprim=wsec;
dwprim=dwsec;

if VD.CYCL.ntypcin==3
    %Rapports imposés
    etat_emb=zeros(size(VD.CYCL.temps));
    etat_emb(find(VD.CYCL.rappvit==0))=1;

%     % On sature la vitesse de rotation de l'arbre primaire a la vitesse de
%     % patinage moteur en premiere
%     indice=find(wprim<VD.ECU.wmt_patinage & VD.CYCL.rappvit==1);
%     if ~isempty(indice)
%         wprim(indice)=VD.ECU.wmt_patinage;
%         etat_emb(indice)=0.5;
%     end
% 
%     % On sature la vitesse de l'arbre primaire de l'embrayage lorsque la
%     % variable elhyb vaut 1 au point mort (indetermination).
%     %indice=find(wprim<VD.MOTH.ral & VD.CYCL.rappvit==0 & elhyb==1);
%     indice=find(wprim<VD.MOTH.ral & elhyb==1);
%     if ~isempty(indice)
%         wprim(indice)=VD.MOTH.ral;
%         etat_emb(indice)=0.5;
%     end
    
    % Si elhyb =0 wmt et cmt =0
    %indice=find(wprim<VD.MOTH.ral & VD.CYCL.rappvit==0 & elhyb==1);
    indice=find(elhyb==0);
    if ~isempty(indice)
        wprim(indice)=0;
        cprim(indice)=0;
        etat_emb(indice)=1;
        dwprim(indice)=0;
    end
    
    % Recalcul de dwmprim
    %dwprim(2:length(VD.CYCL.temps))=diff(wprim)./diff(VD.CYCL.temps);
    %dwprim(1)=0;
    
else
    % Rapports libres
    etat_emb=zeros(size(csec));
    etat_emb(2,:)=1;

    % On sature la vitesse de rotation de l'arbre primaire a la vitesse de
    % patinage moteur en premiere
    indice=find(wprim(2,:)<VD.ECU.wmt_patinage);
    if ~isempty(indice)
        wprim(2,indice)=VD.ECU.wmt_patinage;
        etat_emb(2,indice)=0.5;
    end

    % On sature la vitesse de l'arbre primaire de l'embrayage lorsque au point mort
    % la variable elhyb vaut 1 (indetermination).
    indice=find(wprim(1,:)<VD.MOTH.ral & (elhyb(1,:)==1 | elhyb(1,:)==2));
    if ~isempty(indice)
        wprim(1,indice)=VD.MOTH.ral;
    end

end




