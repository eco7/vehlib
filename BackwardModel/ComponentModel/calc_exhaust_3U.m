function [CO, HC, NOx, T_1, CO_FG_pc, HC_FG_ppm, NOx_FG_ppm, dQcat_mCp, Texh] = calc_exhaust_3U(T_0,Phi,delta_AA,Padm,dcarb,wmt_i,szU,szX,VD,pas_temps,param,stepTime,zev)
% Calcul de la thermique catalyseur et des emissions de polluants
% NOTE: fonction utile pour le mode hybride et electrique
% En mode electrique, seule la convection externe est utilisee pour
% calculer la temperature du catalyseur

global MemLog


if zev
    % Mode elect. emiisions nulles mais il faut calculer la température
    CO = zeros(szX);
    HC = zeros(szX);
    NOx = zeros(szX);
    CO_FG_pc = zeros(szX);
    HC_FG_ppm = zeros(szX);
    NOx_FG_ppm = zeros(szX);
    Tamb = VD.INIT.Tamb+273;
    Texh = Tamb;
    dQs = VD.TWC.HsA_mCp*(Tamb-T_0);
    dQcat_mCp = dQs;
    T_1 = T_0 + dQs*pas_temps;
    return;
end

% Regime moteur
N = wmt_i*30/pi;
% lambda feedGas (exces d'air sortie moteur)
lambdaFG = 1./Phi;

AA_0 = interp2(VD.MOTH.AA_Padm,VD.MOTH.AA_N,VD.MOTH.AA0_N_Padm',Padm,N);
AA = delta_AA + AA_0;

%% concentrations et temperature
% debit gaz en g/s
debitGaz = dcarb.*(1+lambdaFG*VD.MOTH.AStoechio);
clearvars dcarb;

% Emissions echappement en ppm ou pourcent
%CO
CO_FG_pc = 1e-4*VD.MOTH.x_CO_1*(Phi - 1 + sqrt((Phi-1).^2 + VD.MOTH.x_CO_2));

%HC
HC_FG_ppm = max(VD.MOTH.x_HC_min, VD.MOTH.x_HC_1*N + VD.MOTH.x_HC_2*(Phi-0.9).^2 + VD.MOTH.x_HC_3*AA + VD.MOTH.x_HC_4*Padm + VD.MOTH.x_HC_5);

%NO
NOx_FG_ppm = max(VD.MOTH.x_NO_min, VD.MOTH.x_NO_1*Padm + VD.MOTH.x_NO_2*(Phi-0.9).^2 + VD.MOTH.x_NO_3*AA + VD.MOTH.x_NO_4);

% Temperatures ambiante, admission et echappement
Tamb = VD.INIT.Tamb+273;
T_adm = Tamb + VD.MOTH.Tadm_N*N + VD.MOTH.Tadm_cst;
AA_opt = 20;
n_Phi = VD.MOTH.bPhi + VD.MOTH.aPhi*Phi;
n_AA = VD.MOTH.bAA + VD.MOTH.aAA*(delta_AA - AA_opt).^2;
n_N = VD.MOTH.bN + VD.MOTH.aN*N;
n_Padm = VD.MOTH.bP + VD.MOTH.aP*Padm;
Texh = T_adm + VD.MOTH.pci*1e3./(VD.MOTH.cp_gaz*(1+lambdaFG*VD.MOTH.AStoechio)).*...
    n_Phi.*n_AA.*n_N.*n_Padm;

% masse molaire echappement
Mair = (1*32+3.76*28)/(1+3.76);
% carburant : 0.86*C3H6 + 0.14*CH4 ?
Mcarb = 0.86*(3*12+6*1)+0.14*(1*12+4*1);
Mexh = (Mcarb + lambdaFG*VD.MOTH.AStoechio*Mair)./(1+lambdaFG*VD.MOTH.AStoechio);

% debit gaz en mol/s
debitMol = debitGaz./Mexh;

%% efficacite de conversion du catalyseur
% eff_T ne depend que du point de depart de l'arc
% cas simple : non ammorce eff_T = 0
eff_T = zeros(szX);
% autre cas simple : completement amorce : eff_T = 1
% tolerance de 1e-4 (dans le log) => 1e-4 = 0 et 1-1e-4 = 1
i_chaud = T_0>VD.TWC.T0 + VD.TWC.dT/((-log(1-1e-4)/VD.TWC.aT)^(1/VD.TWC.mT));
eff_T(i_chaud) = 1;
% autres cas : calcul
i_froid = T_0<VD.TWC.T0 + VD.TWC.dT/((-log(1e-4)/VD.TWC.aT)^(1/VD.TWC.mT));
i_amorce = ~(i_chaud | i_froid);
eff_T(i_amorce) = exp(-VD.TWC.aT*(VD.TWC.dT./(T_0(i_amorce)-VD.TWC.T0)).^VD.TWC.mT);
clearvars i_chaud i_froid i_amorce
% si le cata est amorce alors il faut calculer l'efficacite
% due a la richesse
% poll_eff_lambda ne depend que du Dsoc (de la commande)
% cas simples : efficacite constante a partir d'une certaine richesse

CO_eff_lambda = ones(szU);
HC_eff_lambda = ones(szU);
NOx_eff_lambda = ones(szU);

% indice pour polluants non convertis
% tolerance de 1e-4 (dans le log) => 1e-4 = 0 et 1-1e-4 = 1
i_CO_nc = lambdaFG<(VD.TWC.lambda0_CO + VD.TWC.dlambda_CO*(-log(1-1e-4/0.8)/VD.TWC.a_CO)^(1/VD.TWC.m_CO));
i_HC_nc = lambdaFG<(VD.TWC.lambda0_HC + VD.TWC.dlambda_HC*(-log(1-1e-4/0.7)/VD.TWC.a_HC)^(1/VD.TWC.m_HC));
i_NOx_nc = lambdaFG>(VD.TWC.lambda0_NOx + VD.TWC.dlambda_NOx*(-log(1e-4)/VD.TWC.a_NOx)^(1/VD.TWC.m_NOx));
% efficacite correspondante = 0 (non convertis)
CO_eff_lambda(i_CO_nc) = 1-0.8;
HC_eff_lambda(i_HC_nc) = 1-0.7;
NOx_eff_lambda(i_NOx_nc) = 0;
% limites polluants convertis (efficacite 1 = valeur par defaut)
i_CO_c = lambdaFG>(VD.TWC.lambda0_CO + VD.TWC.dlambda_CO*(-log(1e-4/0.8)/VD.TWC.a_CO)^(1/VD.TWC.m_CO));
i_HC_c = lambdaFG>(VD.TWC.lambda0_HC + VD.TWC.dlambda_HC*(-log(1e-4/0.7)/VD.TWC.a_HC)^(1/VD.TWC.m_HC));
i_NOx_c = lambdaFG<(VD.TWC.lambda0_NOx + VD.TWC.dlambda_NOx*(-log(1-1e-4)/VD.TWC.a_NOx)^(1/VD.TWC.m_NOx));
% autres cas : necessite de calculer
i_CO_reste = ~(i_CO_nc|i_CO_c);
i_HC_reste = ~(i_HC_nc|i_HC_c);
i_NOx_reste = ~(i_NOx_nc|i_NOx_c);
CO_eff_lambda(i_CO_reste) = (1 - 0.8*exp(-VD.TWC.a_CO*((lambdaFG(i_CO_reste)-VD.TWC.lambda0_CO)/VD.TWC.dlambda_CO).^VD.TWC.m_CO));
HC_eff_lambda(i_HC_reste) = (1 - 0.7*exp(-VD.TWC.a_HC*((lambdaFG(i_HC_reste)-VD.TWC.lambda0_HC)/VD.TWC.dlambda_HC).^VD.TWC.m_HC));
NOx_eff_lambda(i_NOx_reste) = exp(-VD.TWC.a_NOx*((lambdaFG(i_NOx_reste)-VD.TWC.lambda0_NOx)/VD.TWC.dlambda_NOx).^VD.TWC.m_NOx);
if ~isfield(param,'MemLog') || param.MemLog ==1
    log4vehlib('memory');
end
clearvars i_CO_nc i_HC_nc i_NOx_nc i_CO_c i_HC_c i_NOx_c i_CO_reste i_HC_reste i_NOx_reste
% produit des efficacites
% certaines ne dependent que de l'etat initial d'autres de la commande
% : combinatoire necessaire
% CO_eff_0 = repmat(eff_T,szU).*repmat(CO_eff_lambda,szX);
% HC_eff_0 = repmat(eff_T,szU).*repmat(HC_eff_lambda,szX);
% NOx_eff_0 = repmat(eff_T,szU).*repmat(NOx_eff_lambda,szX);


%% Modele thermique catalyseur
% Heat transferred from the TWC to the surroundings
dQs = VD.TWC.HsA_mCp*(Tamb-T_0);
% depend uniquement de l'etat initial
% combinatoire pour le calcul de temp cata avec toutes les contributions
%dQs = repmat(dQs,szU);

% Heat transferred from the gas to the solid
% TODO : egal a zero si moteur coupe?
% dQin : temp cata depend etat initial et temp gaz depend commande
%T_0 = repmat(T_0,szU);
%Texh = repmat(Texh,szX);


%debitGaz = repmat(debitGaz,szX);

HinA_mCp = VD.TWC.Hin_coeff(1)*debitGaz + VD.TWC.Hin_coeff(2);
% saturation a 0 si H negatif
HinA_mCp(HinA_mCp<0)=0;
%dQin = repmat(HinA_mCp,szX).*(repmat(Texh,szX)-repmat(T_0,szU));

% debits massiques de polluants en sortie (g/s)
% la poll emise depend uniquement de la commande
% eff cata depend de etat init (temp cata) et de richesse (commande)

% debitMol = repmat(debitMol,szX);
% CO_FG_pc = repmat(CO_FG_pc,szX);
% HC_FG_ppm = repmat(HC_FG_ppm,szX);
% NOx_FG_ppm = repmat(NOx_FG_ppm,szX);

Mco = (12+16);
Mhc = Mcarb;
Mno = (1*14+1*16);
CO = repmat(debitMol,szX).*(repmat(CO_FG_pc,szX)*1e-2)*Mco.*(1-repmat(eff_T,szU).*repmat(CO_eff_lambda,szX));
HC = repmat(debitMol,szX).*(repmat(HC_FG_ppm,szX)*1e-6)*Mhc.*(1-repmat(eff_T,szU).*repmat(HC_eff_lambda,szX));
NOx = repmat(debitMol,szX).*(repmat(NOx_FG_ppm,szX)*1e-6)*Mno.*(1-repmat(eff_T,szU).*repmat(NOx_eff_lambda,szX));

% Heat transfer due to Exothermic reactions
% calcul des debits convertis en mol/s
% conc volumique = frac mol (gaz parfait)
%co_mol_s = CO_eff_0.*CO_FG_pc*1e-2.*debitMol;
%hc_mol_s = HC_eff_0.*HC_FG_ppm*1e-6.*debitMol;
% dQgen = -VD.TWC.K_mCp ... % -1 car les enthalpies dH0 sont negatives
%     *(co_mol_s*VD.TWC.dH0CO...
%     +hc_mol_s*(0.86*VD.TWC.dH0C3H6/3+0.14*VD.TWC.dH0CH4)); % /3 car mol de C ?
dQgen = -VD.TWC.K_mCp ... % -1 car les enthalpies dH0 sont negatives
    *(repmat(eff_T,szU).*repmat(CO_eff_lambda,szX).*(repmat(CO_FG_pc,szX)*1e-2).*repmat(debitMol,szX)*VD.TWC.dH0CO...
    +repmat(eff_T,szU).*repmat(HC_eff_lambda,szX).*(repmat(HC_FG_ppm,szX)*1e-6).*repmat(debitMol,szX)*(0.86*VD.TWC.dH0C3H6/3+0.14*VD.TWC.dH0CH4)); % /3 car mol de C ?

dQcat_mCp = (repmat(dQs,szU)+ ...
    repmat(HinA_mCp,szX).*(repmat(Texh,szX)-repmat(T_0,szU))+ ...
    dQgen);
clearvars dQgen
T_1 = repmat(T_0,szU) + dQcat_mCp*pas_temps;


if ~isfield(param,'MemLog') || param.MemLog ==1
    log4vehlib('memory');
end

end
