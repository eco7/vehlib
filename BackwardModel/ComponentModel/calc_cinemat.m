% function [ERR,VD,dist,res]=calc_cinemat(ERR,VD,param,res)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul cinematiques
% Calcul vitesse et acceleration
%
% Les calculs sont faits avec une frequence fixes uniquement
%
% 26-08-11 (BJ) : version 1
% 14-09-2012 (AS, BJ) : correction de l echantillonnage des donnes du cycle
% de conduite.
function [ERR,VD,dist,res]=calc_cinemat(ERR,VD,param,res)

%Re-echantillonnage de la base de temps
if param.pas_temps ==- 1 % On ne fait rien
  echantillonnage=0;
else
  echantillonnage=1; % sinon on r??chantillone a pas_temps  
end
% if min(diff(VD.CYCL.temps)) == max(diff(VD.CYCL.temps)) 
%     % le cycle est regulierement echantillone
%     if min(diff(VD.CYCL.temps)) ~=1/freq
%         % Re echantillonage
%         echantillonnage=1;
%     end
%     % else
%     % echantillonnage=1;
% else
%     % on as indique une frequence d'echantillonage et on as un cycle 
%     % par regulier, on se fait jeter
%     if freq~=0
%         chaine='Incoherent data between non regular sampling of VD.CYCL.temps and param.pas_temps' ;
%         ERR=MException('BackwardModel:calc_cinemat',chaine);
%     end
% end

if echantillonnage == 1
    temps=VD.CYCL.temps(1):param.pas_temps:VD.CYCL.tmax;
    VD.CYCL.vitesse=interp1(VD.CYCL.temps,VD.CYCL.vitesse,temps);
    VD.CYCL.vitesse(VD.CYCL.vitesse<1.e-2)=0;
    if VD.CYCL.ntypcin==3
        VD.CYCL.rappvit=round(interp1(VD.CYCL.temps,VD.CYCL.rappvit,temps,'nearest'));
        %VD.CYCL.rappvit=round(interp1(VD.CYCL.temps,VD.CYCL.rappvit,temps));
    end
    VD.CYCL.temps=temps;
end

% VD.CYCL.accel=diff(VD.CYCL.vitesse);
% VD.CYCL.accel=VD.CYCL.accel*abs(freq);
if ~isfield(param,'calc_accel') || param.calc_accel == 1
    % Calcul "historique"; integrale a gauche: cad accel entre i et i+1 affecte a vitesse(i+1)
    VD.CYCL.accel=diff(VD.CYCL.vitesse)./diff(VD.CYCL.temps);
    VD.CYCL.accel=[ 0 VD.CYCL.accel];
    VD.CYCL.pas_temps=diff(VD.CYCL.temps);
    VD.CYCL.pas_temps=[ 0 VD.CYCL.pas_temps];
elseif param.calc_accel == 2
    % Integrale a droite: cad accel entre i et i+1 affecte a vitesse(i)
    VD.CYCL.accel=diff(VD.CYCL.vitesse)./diff(VD.CYCL.temps);
    VD.CYCL.accel=[ VD.CYCL.accel 0];
    VD.CYCL.pas_temps=diff(VD.CYCL.temps);
    VD.CYCL.pas_temps=[ VD.CYCL.pas_temps 0];
elseif param.calc_accel == 3
    % Methode centree
    var=diff(VD.CYCL.vitesse);
    var1=var(1:end-1)+var(2:end);
    t=diff(VD.CYCL.temps);
    t1=t(1:end-1)+t(2:end);
    VD.CYCL.accel=var1./t1;
    VD.CYCL.accel=[var(1)/(2*t(1)) VD.CYCL.accel var(end)/(2*t(end))];
    %VD.CYCL.accel = ([diff(vitesse) 0]+[0 diff(vitesse)])/(2*param.pas_temps);
    VD.CYCL.pas_temps=[t1(1)/2 t1/2 t1(end)/2];
end

if iscolumn(VD.CYCL.rappvit)
    VD.CYCL.rappvit=VD.CYCL.rappvit';
end

if isfield(param,'calc_vit') && param.calc_vit == 1
    % Calcul Selon EcoDriving - on affecte la vitesse moyenne aux calculs de la chaine de traction
    VD.CYCL.vitesse_origine=VD.CYCL.vitesse;
    VD.CYCL.vitesse=(VD.CYCL.vitesse(1:end-1)+VD.CYCL.vitesse(2:end))/2;
    VD.CYCL.vitesse=[VD.CYCL.vitesse 0];
end

VD.CYCL.distance=cumsum(VD.CYCL.pas_temps.*VD.CYCL.vitesse);
dist=VD.CYCL.distance(end);


%  suppression des doublons dans VD.CYCL.PKpente
if VD.CYCL.ntyppente==2 | VD.CYCL.ntyppente==3
    B=[VD.CYCL.PKpente' VD.CYCL.penteFpk']; % suppression des doublons pour interpolation
    B=unique(B,'rows');
    VD.CYCL.PKpente=B(:,1)';
    VD.CYCL.penteFpk=B(:,2)';
end

if exist('res','var')
    res(1).tsim = VD.CYCL.temps;
    res.vit = VD.CYCL.vitesse;
    res.acc = VD.CYCL.accel;
    res.distance = VD.CYCL.distance;
end

end
