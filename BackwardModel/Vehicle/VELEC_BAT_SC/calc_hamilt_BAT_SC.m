% function [ERR,isc_cible,lambda,ham,usc,u0sc,ux,soc]=calc_hamilt_BAT_SC(ERR,param,lambda,pas_temps,pas_isc,t,pres,soc,ah,usc,u0sc,ux)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Minimisation de la courbe des courants BAT/VD.SC admissibles pour la valeur
% de lambda
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

function [ERR,isc_cible,lambda,ham,usc,u0sc,ux,soc]=calc_hamilt_BAT_SC(ERR,VD,param,lambda,pas_temps,pas_isc,t,pres,soc,ah,usc,u0sc,ux)
%#eml
eml.extrinsic('fprintf')
eml.extrinsic('evalin')
% Calcul des limites
borne_max=VD.SC.maxCourant*VD.SC.Nbranchepar;
borne_min=(-1)*VD.SC.maxCourant*VD.SC.Nbranchepar;
isc_s=borne_min:pas_isc:borne_max;


% Calculs super condensateur
if t~=1
    [ERR,usc_s,u0sc_s,ux_s]=calc_sc(ERR,VD.SC,t,pas_temps,isc_s,u0sc(t-1),ux(t-1));
else
    [ERR,usc_s,u0sc_s,ux_s]=calc_sc(ERR,VD.SC,1,pas_temps,isc_s,VD.SC.Vc0*VD.SC.Nblocser,VD.SC.Vx0*VD.SC.Nblocser);
end


%if ~isempty(ERR)
%    ERR=[];
%    facteur_hors_limite=1;
%end

% Puissance supercondensateur
psc_s=usc_s.*isc_s;
p0sc_s=u0sc_s.*isc_s;

if isfield(VD,'DCDC_2') && ~isempty(VD.DCDC_2)
    % Recalcul en aval des super capacites
    [ERR,psc_res_s]=calc_dcdc(1,ERR,VD.DCDC_2,usc_s,psc_s);
else
    psc_res_s=psc_s;
end

% Conditions de fonctionnement batterie
pbat_res_s=pres(t)-psc_res_s;

% Conditions de fonctionnement en aval de la batterie
if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
    [ERR,pbat_s]=calc_dcdc(2,ERR,VD.DCDC_1,0,pbat_res_s);
else
    pbat_s=pbat_res_s;
end

% Calculs batterie
if t==1
    [ERR,ibat_s,ubat_s,soc_s,~,~,rbat]=calc_batt(ERR,VD.BATT,pas_temps,pbat_s,soc(1),ah(1),pres(t),psc_res_s);
else
    [ERR,ibat_s,ubat_s,soc_s,~,~,rbat]=calc_batt(ERR,VD.BATT,pas_temps,pbat_s,soc(t-1),ah(t-1),pres(t),psc_res_s);
end

%if ~isempty(ERR)
%    ERR=[];
%    facteur_hors_limite=1; 
%end

% On verifie que la tension du DCDC est compatible
if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
    [ERR,pbat_res_s]=calc_dcdc(1,ERR,VD.DCDC_1,ubat_s,pbat_s); 
    %if isnan(pbat_res)
     %   facteur_hors_limite=1;
    %end
end

ibat_s(isnan(pbat_res_s))=NaN;

lambda_ncst=false;
exist_param=0;
exist_param=evalin('base','exist(''param'')');
%exist_param=evalin('base','isfield(param,''lambda_ncst'')');

if exist_param==1 & evalin('base','isfield(param,''lambda_ncst'')')
lambda_ncst=evalin('base','strcmp(param.lambda_cst,''non'')'); % pour faire fonctionner en embeded
end

%if t>1
%if t>1 & strcmp(param.lambda_cst,'non')
if t>1 && lambda_ncst==1 % pour faire fonctionner en embeded
    % Recalcul de lambda 
    pas=pas_temps;
    psc_s=usc_s.*isc_s;
    R=(VD.SC.Rs.*VD.SC.Nblocser/VD.SC.Nbranchepar);
    C=(VD.SC.C./VD.SC.Nblocser*VD.SC.Nbranchepar);
    Rx=(VD.SC.Rx.*VD.SC.Nblocser/VD.SC.Nbranchepar);
    Cx=(VD.SC.Cx./VD.SC.Nblocser*VD.SC.Nbranchepar);
    taux=Rx*Cx;
    Re=R+Rx*exp(-pas/(taux));
    det=u0sc_s.^2.*((ux_s*exp(-pas/taux)-u0sc_s).^2-4*Re*psc_s);
    det(det<0)=0; % pourquoi cela peut il arriver ?? Comment traiter effectivement ce cas (EV 22/09/11)
    dP0=-1/(Re*C) * ( 1  -  ux_s*(exp(-pas/(taux)))./(2*u0sc_s)- ...
    (u0sc_s.^2-2*u0sc_s.*ux_s*(exp(-pas/(taux)))-2*Re*psc_s-(ux_s.^2)/2*exp(-2*pas/taux) )./sqrt(det));
    dP0(det<0)=0;
    %dP0=zeros*ones(size(psc_s));
    lambda_s=lambda(t-1)./(1+dP0.*pas);
else
    lambda_s=ones(size(isc_s)).*lambda(1);
end



% Appel de la fonction ad hoc suivant critere
%ham=ham_fonct(lambda(t),p0sc,ibat(t),soc(t)); % min nrj batt
ham=ham_fonct2(lambda_s,p0sc_s,ibat_s); % min Ieff bat
%ham=ham_fonct3(lambda(t),isc(t),p0sc,ibat(t),soc(t+1)); % min pjoules

% Recherche du minimum de la courbe
indice=find(ham==min(ham));

% Affectation des grandeurs avant sortie
isc_cible=isc_s(indice(1));
usc(t)=usc_s(indice(1));
ux(t)=ux_s(indice(1));
u0sc(t)=u0sc_s(indice(1));
soc(t)=soc_s(indice(1));
lambda(t)=lambda_s(indice(1));
hamilt=ham(indice(1));

%fprintf(1,'%d %f %f %f %f %f\n',t,isc_cible,usc(t),u0sc(t),ubat(indice(1)),ibat(indice(1)));

%if ham > max_ham
%    max_ham=ham.*2;
%end

%if facteur_hors_limite==1
    %ham=max_ham+10*abs(isc(t)); % pb de convergence de solve hamilt quand on a des limites avec isc >0 et isc<0
%     ham=max_ham;
%end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fonction a minimiser
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ham]=ham_fonct(lambda,p0sc,ibat,soc)
% Minimisation de la consommation d'nrj batterie
if ibat>=0
    RdF=1;
else
    RdF=VD.BATT.RdFarad;
end
E=interp1(VD.BATT.dod_ocv,VD.BATT.ocv(1,:),100-soc,'linear','extrap')*VD.BATT.Nblocser;
ham=RdF.*E.*ibat+p0sc.*lambda;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ham]=ham_fonct2(lambda,p0sc,ibat)
% Minimisation du courant efficace batterie
ham=ibat.*ibat+p0sc.*lambda;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ham]=ham_fonct3(lambda,isc,p0sc,ibat,soc)
% Minimisation des pertes batterie+supercondensateur

% Resistance batterie
if ibat>=0
    RdF=1;
    if VD.BATT.ntypbat==1 || VD.BATT.ntypbat==2
        R=interp1(VD.BATT.dod,VD.BATT.Rd(1,:),100-soc,'linear','extrap')*VD.BATT.Nblocser/VD.BATT.Nbranchepar;
    elseif VD.BATT.ntypbat==3
        R=interp1(VD.BATT.SOC_param,VD.BATT.Rd(1,:),100-soc,'linear','extrap')*VD.BATT.Nblocser/VD.BATT.Nbranchepar;
    end
else
    RdF=VD.BATT.RdFarad;
    if VD.BATT.ntypbat==1 || VD.BATT.ntypbat==2
        R=interp1(VD.BATT.dod,VD.BATT.Rc(1,:),100-soc,'linear','extrap')*VD.BATT.Nblocser/VD.BATT.Nbranchepar;
    elseif VD.BATT.ntypbat==3
        R=interp1(VD.BATT.SOC_param,VD.BATT.Rc(1,:),100-soc,'linear','extrap')*VD.BATT.Nblocser/VD.BATT.Nbranchepar;
    end
end


% Resistance Super condensateur
Rsc=VD.SC.Rs.*VD.SC.Nblocser/VD.SC.Nbranchepar;

ham=ibat.*ibat.*R+isc.*isc.*Rsc+p0sc.*lambda;
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



