%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tentative de strategie en ligne basee sur des lois empiriques
% En roulage : 
%       * en traction : moyenne glissante (param.t_moy sec.) des puissances reseaux >0
%       * en recup : on ne demande rien a la batterie
% A l'arret:
%       * on calcul la puissance batterie de maniere a recharger la VD.SC en
%       param.tarret sec.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,pbat_cible]=user_defined_on_line_strategy_VELEC_BAT_SC(ERR,VD,ind,param,pbat_cible,pres,u0sc);
global arret psc_arret pbat_roulage

if ind==1
    if VD.CYCL.vitesse(ind)==0
        arret=0;
    else
        arret=1;
    end
end

% Strategie en roulage
if VD.CYCL.vitesse(ind)~=0
    if arret==1
        arret=0;
    end
    if pres(ind)>0
        % En traction
        if ind>param.t_moy
            vect=pres(ind-param.t_moy:ind-1);
            pbat_cible(ind)=mean(vect(vect>0));
        elseif ind>1
            vect=[pres(1:ind) repmat(param.pini,1,param.t_moy./param.pas_temps-ind)];
            pbat_cible(ind)=mean(vect(vect>0));
        else
            pbat_cible(ind)=param.pini;
        end
        %pbat_cible(ind)=30000;
    else
        pbat_cible(ind)=0;
    end
end

% A l arret du vehicule
if VD.CYCL.vitesse(ind)==0
    if arret==0
        arret=1;
        % On calcule la puissance necessaire pour recharger completement 
        % les UC pendant param.tarret sec.
        if ind>1
            nrj_sc=max(0,0.5*VD.SC.C*(VD.SC.maxTension^2-(u0sc(ind-1)/VD.SC.Nblocser)^2)*VD.SC.Nblocser*VD.SC.Nbranchepar);
        else
            nrj_sc=max(0,0.5*VD.SC.C*(VD.SC.maxTension^2-VD.SC.Vc0^2)*VD.SC.Nblocser*VD.SC.Nbranchepar);
        end
        psc_arret=nrj_sc/param.tarret;
    end
    pbat_cible(ind)=pres(ind)+psc_arret;
end
