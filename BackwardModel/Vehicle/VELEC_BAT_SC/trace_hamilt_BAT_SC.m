% function [ERR,isc_cible,hamilt]=trace_hamilt_BAT_SC(ERR,param,lambda,pas_temps,t,pres,soc,ah,usc,u0sc,ux);
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV-BJ) : version 1

function [ERR,isc_cible,hamilt]=trace_hamilt_BAT_SC(ERR,VD,param,lambda,pas_temps,t,pres,soc,ah,usc,u0sc,ux);

% Calcul des limites
borne_max=VD.SC.maxCourant*VD.SC.Nbranchepar;
borne_min=(-1)*VD.SC.maxCourant*VD.SC.Nbranchepar;
pas=(borne_max-borne_min)/50;

isc_trace=[];
hamilt_trace=[];
for isc_cible=borne_min:pas:borne_max
    [hamilt]=calc_hamilt_BAT_SC(ERR,VD,param,lambda,pas_temps,t,pres,soc,ah,usc,u0sc,ux);
    isc_trace=[isc_trace isc_cible];
    hamilt_trace=[hamilt_trace hamilt];
end

figure(1)
plot(isc_trace,hamilt_trace,'r+-')

pause
