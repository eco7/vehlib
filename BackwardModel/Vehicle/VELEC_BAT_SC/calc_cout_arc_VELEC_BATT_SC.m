% function [ERR,cout,u0sc,soc,ux]=calc_cout_arc_VELEC_BATT_SC(ERR,param,pres,isc,u0sc_p,soc_p,ux_p,i)
% 
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% 
% 23-08-2010  (EV) : Correction bug ordre renvoi u0sc,soc,ah,ux doit être le meme
% que ordre appel u0sc_p,soc_p,ah_p,ux_p sinon inversion dans
% solve_graph


function [ERR,cout,u0sc,soc,ux]=calc_cout_arc_VELEC_BATT_SC(ERR,VD,param,pres,isc,u0sc_p,soc_p,ux_p,i)
pres=pres(i);

[Li,Co]=size(u0sc_p);
if sum(sum(isnan(u0sc_p)))==Li*Co | sum(sum(isnan(ux_p)))==Li*Co | sum(sum(isnan(soc_p)))==Li*Co 
    ERR=MException('BackwardModel:calc_cout_arc', 'appel a calc_cout_arc : tout les elements d''un des vecteur precedent a NaN');
    return;
end


% pres scalaire
% u0sc_p,ux_p isc peuvent être des vecteurs
% usc,u0sc soc peuvent être des matrice
ERR=[ ];
Co=length(isc);
isc=repmat(isc,length(u0sc_p),1);
u0sc_p=repmat(u0sc_p',1,Co);
ux_p=repmat(ux_p',1,Co);

[ERR,usc,u0sc,ux]=calc_sc(ERR,VD.SC,0,param.pas_temps,isc,u0sc_p,ux_p);

% Puissance supercondensateur
psc=usc.*isc;

if isfield(VD,'DCDC_2') && ~isempty(VD.DCDC_2)
    % Calcul en aval des super capacites
    [ERR,psc_res]=calc_dcdc(1,ERR,VD.DCDC_2,usc,psc);
else
    psc_res=psc;
end

if param.freinage_flux_serie==0
    % En freinage, on interdit de faire du flux serie
    indice=find(pres<0 & psc<pres);
    psc(indice)=NaN;
end

% Calcul en aval des batteries
pbat_res=pres-psc_res;

% Conditions de fonctionnement batterie
if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
    [ERR,pbat]=calc_dcdc(2,ERR,VD.DCDC_1,0,pbat_res);
else
    pbat=pbat_res;
end

% Calculs batterie

soc_p=repmat(soc_p',1,Co);

[ERR,Ibat,Ubat,soc,ah]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat,soc_p,0,pres,psc);



% On verifie que la tension du DCDC est compatible
if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
    [ERR,pbat_res]=calc_dcdc(1,ERR,VD.DCDC_1,Ubat,pbat);
    Ibat(isnan(pbat_res))=NaN;
end

% Critere de cout des arcs
cout=Ibat.*Ibat*param.pas_temps; % peut aussi devenir cout = ibat*ibat par exemple
%
%cout=Ibat*param.pas_temps; % peut aussi devenir cout = ibat*param.pas_temps par exemple
% gestion des erreurs
% Pour le graphe il vaut mieux renvoyer des couts infini que des cout NaN
cout(isnan(cout))=Inf;

if~isempty(ERR)
    ERR=MException('BackwardModel:calc_cout_arc', 'tout les arcs sont impossible, les caractéristique du système ne permettent pas de satisfaire les conditions demandees !');
    return;
end

return

