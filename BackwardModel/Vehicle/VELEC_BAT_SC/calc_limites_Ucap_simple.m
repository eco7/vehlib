% function [ERR,u0sc_max,u0sc_min,isc_max,isc_min]=calc_limites_Ucap_simple(ERR,param)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

function [ERR,u0sc_max,u0sc_min,isc_max,isc_min]=calc_limites_Ucap_simple(ERR,VD,param)

ii_max=size(VD.CYCL.temps);
u0sc_min=zeros(ii_max);
u0sc_max=zeros(ii_max);
isc_max=zeros(ii_max);
isc_min=zeros(ii_max);

C=VD.SC.C/VD.SC.Nblocser*VD.SC.Nbranchepar;

u0sc_min(1)=VD.SC.Vc0*VD.SC.Nblocser;
u0sc_max(1)=VD.SC.Vc0*VD.SC.Nblocser;
% Attention, ici courant maxi des VD.SC represente la limite maximum en tesion
% dans le graphe, c'est donc de la charge des VD.SC
isc_max(1)=-VD.SC.maxCourant*VD.SC.Nbranchepar;
isc_min(1)=VD.SC.maxCourant*VD.SC.Nbranchepar;

if isfield(param,'prec_graphe_z')
    param.prec_graphe=param.prec_graphe_z;
end

% recherche limite sup début cycle
for ii=2:1:length(VD.CYCL.temps)
    lim_max=0;
    isc_max(ii)=-VD.SC.maxCourant*VD.SC.Nbranchepar;
    u0sc_max(ii)=u0sc_max(ii-1)-isc_max(ii)/C*param.pas_temps;
    if u0sc_max(ii)>VD.SC.maxTension*VD.SC.Nblocser
         lim_max=1;
    end
    
    if isc_max(ii-1)~=0
        while lim_max==1
            lim_max=0;
            isc_max(ii)=isc_max(ii)+param.prec_graphe;
            u0sc_max(ii)=u0sc_max(ii-1)-isc_max(ii)/C*param.pas_temps;
            if u0sc_max(ii)>VD.SC.maxTension*VD.SC.Nblocser
                lim_max=1;
            end
        end
    else
        isc_max(ii)=0;
        u0sc_max(ii)=u0sc_max(ii-1);
    end
end

u0sc_max(end)=u0sc_max(1)+param.du0;
u0sc_max_d=u0sc_max;
max_u0sc_max=max(u0sc_max);

% recherche limite sup fin cycle
for ii=length(VD.CYCL.temps):-1:2
    lim_max=0;
    isc_max(ii)=VD.SC.maxCourant*VD.SC.Nbranchepar;
    u0sc_max(ii-1)=u0sc_max(ii)+isc_max(ii)/C*param.pas_temps;
    if u0sc_max(ii-1)>max_u0sc_max
        lim_max=1;
    end
    while lim_max==1
        lim_max=0;
        isc_max(ii)=isc_max(ii)-param.prec_graphe;
        u0sc_max(ii-1)=u0sc_max(ii)+isc_max(ii)/C*param.pas_temps;
        if u0sc_max(ii-1)>max_u0sc_max
            lim_max=1;
        end
    end
    if isc_max(ii)==0
        break
    end
end

u0sc_max=min(u0sc_max,u0sc_max_d);




% recherche limite inf debut cycle
for ii=2:1:length(VD.CYCL.temps)
    lim_min=0;
    isc_min(ii)=VD.SC.maxCourant*VD.SC.Nbranchepar;
    u0sc_min(ii)=u0sc_min(ii-1)-isc_min(ii)/C*param.pas_temps;
    if u0sc_min(ii)<VD.SC.minTension*VD.SC.Nblocser
         lim_min=1;
    end
    
    if isc_min(ii-1)~=0
        while lim_min==1
            lim_min=0;
            isc_min(ii)=isc_min(ii)-param.prec_graphe;
            u0sc_min(ii)=u0sc_min(ii-1)-isc_min(ii)/C*param.pas_temps;
            if u0sc_min(ii)<VD.SC.minTension*VD.SC.Nblocser
                lim_min=1;
            end
        end
    else
        isc_min(ii)=0;
        u0sc_min(ii)=u0sc_min(ii-1);
    end
end


u0sc_min(end)=u0sc_min(1)+param.du0;
u0sc_min_d=u0sc_min;
min_u0sc_min=min(u0sc_min);

% recherche limite inf fin cycle
for ii=length(VD.CYCL.temps):-1:2
    lim_min=0;
    isc_min(ii)=-VD.SC.maxCourant*VD.SC.Nbranchepar;
    u0sc_min(ii-1)=u0sc_min(ii)+isc_min(ii)/C*param.pas_temps;
    if u0sc_min(ii-1)<min_u0sc_min
        lim_min=1;
    end
    while lim_min==1
        lim_min=0;
        isc_min(ii)=isc_min(ii)+param.prec_graphe;
        u0sc_min(ii-1)=u0sc_min(ii)+isc_min(ii)/C*param.pas_temps;
        if u0sc_min(ii-1)<min_u0sc_min
            lim_min=1;
        end
    end
    if isc_min(ii)==0
        break
    end
end

u0sc_min=max(u0sc_min,u0sc_min_d);



