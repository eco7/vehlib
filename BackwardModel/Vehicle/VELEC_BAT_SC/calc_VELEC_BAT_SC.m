% function [ERR,VD,ResXml]=calc_VELEC_BAT_SC(ERR,vehlib,param,VD)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul d'un vehicule electrique muni d'une association
% batterie/super-condensateur dans VEHLIB en mode backward
%
% Arguments appel :
% Arguments de sortie :
%
% 26/10/09 (EV) :
% En recup si le courant batt + ucap ne permet pas de recuperer toute
% l'energie que se passe t'il ??
% A priori OK quand meme car meme si qacm1 est calcule pour un couple
% moteur sans frein meca, de totue fa???on si on vient limiter a cause des
% courants on recupere quand meme tout ce que l'on peut. Seul cacm1, qacm1
% et donc l'eventuel frein meca sont faux
%

function [ERR,VD,ResXml]=calc_VELEC_BAT_SC(ERR,vehlib,param,VD)


% Calcul cinematiques complementaires
% Avec re-echantillonnage eventuel
if ~isfield(param,'pas_temps') | param.pas_temps==0
    param.pas_temps=1; % si param_pas temps n'existe pas ou =0 on reechantillone ? 1s
end
[ERR,VD,dist]=calc_cinemat(ERR,VD,param);

if ~isempty(ERR)
ResXml=struct([]); %     [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end

% Initialisation
soc=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));
E=zeros(size(VD.CYCL.temps));
R=zeros(size(VD.CYCL.temps));
usc=zeros(size(VD.CYCL.temps));
u0sc=zeros(size(VD.CYCL.temps));
ux=zeros(size(VD.CYCL.temps));
isc=zeros(size(VD.CYCL.temps));
isc_cible=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
pbat=zeros(size(VD.CYCL.temps));
uacm1=zeros(size(VD.CYCL.temps));
lambda=zeros(size(VD.CYCL.temps));

% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
if ~isempty(ERR)
ResXml=struct([]); %     [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end

% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,penteFpk,fres,faero,froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end
cveh=croue;
Proue=croue.*wroue;

% Connexion
csec_red=croue;
wsec_red=wroue;
dwsec_red=dwroue;

% Calcul des conditions en amont du reducteur
[ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);
if ~isempty(ERR)
ResXml=struct([]); %     [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end

% Connexion
wacm1=wprim_red;
dwacm1=dwprim_red;
cacm1=cprim_red/VD.VEHI.nbacm1+VD.ACM1.J_mg.*dwacm1;

% Prise en compte des couples min du moteur et impos??? par le calculateur (alors le reste
% est du frein meca)
cacm1=max(cacm1,interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,1),wacm1));
cacm1=max(cacm1,interp1(VD.ECU.Vit_Cmin,VD.ECU.Cpl_min,wacm1));

% Recalcul de la partie frein meca au niveau de la roue
cprim_red=(cacm1-VD.ACM1.J_mg.*dwacm1)*VD.VEHI.nbacm1;
wprim_red=wacm1;
dwprim_red=dwacm1;
%[ERR,csec_red,wsec_red,dwsec_red,Pertes_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
[ERR,csec_red,wsec_red,dwsec_red,~,Pertes_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
cfrein_meca=csec_red-croue;

% Calcul des conditions en amont de la machine electrique
% Ici, on ne peut pas s assurer que la Me satisfait les conditions
% dynamiques demandees (on ne connait pas sa tension d'alimentation).
[Li,Co]=size(cacm1);
wacm1=repmat(wacm1,Li,1);
[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
if ~isempty(ERR)
    soc=NaN;
    ah=NaN;
ResXml=struct([]); %     [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);
if ~isempty(ERR)
    soc=NaN;
    ah=NaN;
ResXml=struct([]); %     [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end

% Puissance reseau electrique
pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc;

soc(1)=100-VD.INIT.Dod0;

% Cas prog dynamique
if strcmp(lower(param.optim),'prog_dyn')
    [ERR,u0sc_min,u0sc_max,u0sc,ux,isc,usc,psc,u0bat,ibat,ubat,pbat,rbat,soc,ah]= ...
        calc_prog_dyn_VELEC_BAT_SC(ERR,param,VD,pres);
end

% Cas calcul variationnel
if strcmp(lower(param.optim),'lagrange')
    [ERR,u0sc,ux,isc,usc,psc,u0bat,ibat,ubat,pbat,rbat,soc,ah,lambda]= ...
        calc_lagrange_VELEC_BAT_SC(ERR,param,vehlib,VD,pres);
end


%% Cas calcul en ligne
if strcmp(lower(param.optim),'ligne')
    [ERR,u0sc,ux,isc,usc,psc,u0bat,ibat,ubat,pbat,rbat,soc,ah,pbat_cible]= ...
        calc_ligne_VELEC_BAT_SC(ERR,param,vehlib,VD,pres);
end

if ~isempty(ERR)
    ERR
ResXml=struct([]); %     [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end


% Post-calculs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% La machine permet-elle de satisfaire les conditions demandees
uacm1=ubat;
[ERR,wacm1,cacm1]=calc_enveloppe_acm(ERR,VD.ACM1,wacm1,cacm1,uacm1);
if ~isempty(ERR)
ResXml=struct([]); %     [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end

if isfield(VD,'DCDC_2') && ~isempty(VD.DCDC_2)
    % Recalcul en aval des super capacites
    [ERR,psc_res]=calc_dcdc(1,ERR,VD.DCDC_2,usc,psc);
else
    psc_res=psc;
end

if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
    % Recalcul en aval des batteries
    [ERR,pbat_res]=calc_dcdc(1,ERR,VD.DCDC_1,ubat,pbat);
else
    pbat_res=pbat;
end

%% Bilan de puissance et d'energie
if VD.SC.Rx~=0
    Pcx=ux.*(isc-ux/VD.SC.Rx/VD.SC.Nblocser*VD.SC.Nbranchepar);
    Pjsc=VD.SC.Nblocser*VD.SC.Nbranchepar*VD.SC.Rs*(isc./VD.SC.Nbranchepar).^2+ux.*ux/VD.SC.Rx*VD.SC.Nbranchepar/VD.SC.Nblocser;
    % Calcul d une resistance equivallente
    rsc=Pjsc./(isc./VD.SC.Nbranchepar).^2;
else
    rsc=(VD.SC.Nblocser*VD.SC.Nbranchepar*VD.SC.Rs).*ones(size(isc));
    Pjsc=rsc.*(isc./VD.SC.Nbranchepar).^2;
    Pcx=0;
end
p0sc=u0sc.*isc;

Pjbat=rbat.*ibat.*ibat;
p0bat=u0bat.*ibat;

qdcdc_2=abs(psc_res-psc);
qdcdc_1=abs(pbat_res-pbat);

bilanP=p0bat+p0sc-Pjbat-Pjsc-Pcx-qdcdc_2-qdcdc_1-VD.VEHI.nbacm1*qacm1-pacc-Pertes_red-Proue-cfrein_meca.*wroue-VD.ACM1.J_mg.*dwacm1.*wacm1*VD.VEHI.nbacm1;

% Post-calculs(Fin)  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if param.verbose>=1
    % Quelques resultats
    fprintf(1,'%s %.3f %s\n','La consommation du vehicule est de : ',ah(end),'Ah');
    fprintf(1,'%s %.3f %s\n','Soit : ',ah(end)/(dist/1000),'Ah/km');
    fprintf(1,'%s %.3f %s\n','La variation de SOC sur le cycle est de : ',soc(1)-soc(end),'%');
    fprintf(1,'%s %.1f %s\n','La distance parcourue est de : ',dist,'m');
    fprintf(1,'%s %.1f %s\n','La masse du vehicule est de : ',mean(masse),'kg');
end

if param.verbose>=1
    fprintf(1,'%s %.2f %s\n','Le courant efficace de la batterie : ',sqrt( trapz(VD.CYCL.temps,ibat.*ibat)/(VD.CYCL.temps(end)-VD.CYCL.temps(1)) ),'A');
    fprintf(1,'%s %.2f %s\n','Le courant moyen de la batterie : ',trapz(VD.CYCL.temps,ibat)/(VD.CYCL.temps(end)-VD.CYCL.temps(1)),'A');
    fprintf(1,'%s %.2f %s\n','Le courant efficace dans les supercapacites : ',sqrt(trapz(VD.CYCL.temps,isc.*isc)/(VD.CYCL.temps(end)-VD.CYCL.temps(1))),'A');
    fprintf(1,'%s %.6f %s\n','Les pertes joules dans les supercapacites : ',trapz(VD.CYCL.temps,Pjsc)/3600,'Wh');
    fprintf(1,'%s %.6f %s\n','Les pertes joules dans les batteries : ',trapz(VD.CYCL.temps,Pjbat)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','Les pertes dans les machines electrique : ',trapz(VD.CYCL.temps,VD.VEHI.nbacm1*qacm1)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie fournie par les ucap (isc.*u0sc): ',trapz(VD.CYCL.temps,p0sc)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie fournie par les ucap uOsc(end)-u0sc(1): ',0.5*VD.SC.C*VD.SC.Nbranchepar/VD.SC.Nblocser*(u0sc(1)^2-u0sc(end)^2)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie fournie par la batterie (source parfaite) : ',trapz(VD.CYCL.temps,p0bat)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie fournie par la batterie : ',trapz(VD.CYCL.temps,ibat.*ubat)/3600,'Wh');
    fprintf(1,'%s %.3f %s\n','Soit : ',(trapz(VD.CYCL.temps,ibat.*ubat)/3600/(dist/1000))./1000,'kWh/km');
    fprintf(1,'%s %.2f %s\n','Les pertes dans le reducteur : ',trapz(VD.CYCL.temps,Pertes_red)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie consommee par les accessoires : ',trapz(VD.CYCL.temps,pacc)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie dissipee dans les freins : ',trapz(VD.CYCL.temps,cfrein_meca.*wroue)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie inertie motelec "perdue" : ',trapz(VD.CYCL.temps,VD.ACM1.J_mg.*dwacm1.*wacm1)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie Pcx "perdue" : ',trapz(VD.CYCL.temps,Pcx)/3600,'Wh');
    
    figure
    clf
    if strcmp(lower(param.optim),'prog_dyn')
        %pb du point origine
        plot(VD.CYCL.temps(2:end),bilanP(2:end))
    else
        plot(VD.CYCL.temps,bilanP)
    end
    grid
    legend('bilan de puissance')
    
    prouet=wroue.*croue;
    prouet(prouet<0)=0;
    prouer=wroue.*croue;
    prouer(prouer>0)=0;
    fprintf(1,'%s %.1f %s %s %.1f %s %s %.1f %s\n','L''energie fournie aux roues motrices : ',trapz(VD.CYCL.temps,Proue)/3600,'Wh',...
        'en traction',trapz(VD.CYCL.temps,prouet)/3600,'Wh','en recup',trapz(VD.CYCL.temps,prouer)/3600,'Wh');
    
    % trajectoire optimale
    figure
    clf
    hold on
    
    if strcmp(lower(param.optim),'prog_dyn')
        % limites du graphe
        plot(VD.CYCL.temps,u0sc_min,'b',VD.CYCL.temps,u0sc_max,'b')
    else
        plot(VD.CYCL.temps,ones(length(VD.CYCL.temps))*VD.SC.maxTension*VD.SC.Nblocser,'b')
        plot(VD.CYCL.temps,ones(length(VD.CYCL.temps))*VD.SC.minTension*VD.SC.Nblocser,'b')
        
    end
    plot(VD.CYCL.temps,u0sc,'r')
    grid
    title('trajectoire optimale')
    ylabel('u0sc en V')
    
    % trace sollicitation batterie
    Inter=-125:25:350;
    figure;
    classe_energie(VD.CYCL.temps,(ibat),3,min(ibat),max(ibat),10,0,0,1,{'titre'},1,Inter)
end

% Cycle
tsim=VD.CYCL.temps;
vit=VD.CYCL.vitesse;
acc=VD.CYCL.accel;
vit_dem=vit;
distance=cumtrapz(VD.CYCL.temps,VD.CYCL.vitesse); % distance calculee a partir de la vitesse VD.CYCL.vitesse
pente=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,distance,'linear',0); % reechantillonage pente fonction distance claculee.     

% miseEnForme4VEHLIB fait appel a des parametres (evalin('caller'...)
% des strcutures INIT, CYCL, ...
INIT=VD.INIT;
CYCL=VD.CYCL;
VEHI=VD.VEHI;
ACM1=VD.ACM1;
ACC=VD.ACC;
[ResXml]=miseEnForme4VEHLIB(vehlib,'caller');
% Ajout des grandeurs synthétiques dans une nouvelle table de la structure xml
[ResXml]=miseEnForme4GestionEnergie(ResXml);

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [nrj]=critere_cvgce(VD,ud,uf)

nrj=0.5*VD.SC.C.*(uf.^2-ud.^2).*VD.SC.Nblocser.*VD.SC.Nbranchepar/3600;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function [ERR,u0sc_min,u0sc_max,u0sc,ux,isc,usc,psc,ibat,ubat,pbat,rbat,soc,ah]=calc_prog_dyn_VELEC_BAT_SC(ERR,param,VD,pres)
%
% Optimisation de la gestion de l energie dans une vehicule electrique
% avec batterie et super capacites
% Methode de la programmation dynamique
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,u0sc_min,u0sc_max,u0sc,ux,isc,usc,psc,u0bat,ibat,ubat,pbat,rbat,soc,ah]=calc_prog_dyn_VELEC_BAT_SC(ERR,param,VD,pres)

% Initialisation
u0sc_min=zeros(size(VD.CYCL.temps));
u0sc_max=zeros(size(VD.CYCL.temps));
u0sc=zeros(size(VD.CYCL.temps));
ux=zeros(size(VD.CYCL.temps));
isc=zeros(size(VD.CYCL.temps));
usc=zeros(size(VD.CYCL.temps));
psc=zeros(size(VD.CYCL.temps));
u0bat=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
pbat=zeros(size(VD.CYCL.temps));
rbat=zeros(size(VD.CYCL.temps));
soc=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));

% Les initialisations de u0sc, soc, ah et ux sont faites dans
% param_prog_dynamique

[ERR,u0sc_max,u0sc_min]=calc_limites_Ucap_simple(ERR,VD,param);

% Ici on sort une tension, il faut rentrer des indices
[ERR, lim_inf,lim_sup] = limite2indice_u0sc(ERR, VD,u0sc_min,u0sc_max,param);


%%% Revoir calcul des limites et les pb d'arrondie (calcul sur les
%%% tension puis faire rentrer les limites dans le graphe (EV
%%% 12-11-09).

var_com_inf=VD.SC.maxCourant*VD.SC.Nbranchepar*ones(size(VD.CYCL.temps));
var_com_sup=-VD.SC.maxCourant*VD.SC.Nbranchepar*ones(size(VD.CYCL.temps));

[ERR,poids_min,meilleur_chemin]= ...
    solve_graph(ERR,@(ERR,ii,isc,u0sc_p,ux_p,soc_p) calc_cout_arc_VELEC_BATT_SC(ERR,VD,param,pres,isc,u0sc_p,ux_p,soc_p,ii),...
    param,lim_sup,lim_inf,var_com_inf,var_com_sup);

fprintf(1,'%s : %f\n','Le poids du graphe est : ',poids_min);

if ~isempty(ERR)
    return;
end

% On as le meilleur chemin on peut recalculer les autres grandeurs le
% long de ce chemin

% Recalcul du courant
[ERR,isc]=chemin2varcomSC(meilleur_chemin,param,var_com_inf);

% if param.verbose
% figure(4)
% hold on
% plot(VD.CYCL.temps,meilleur_chemin,'r',VD.CYCL.temps,lim_inf,'b',VD.CYCL.temps,lim_sup,'b')
% end

u0sc(1)=VD.SC.Vc0*VD.SC.Nblocser;
usc(1)=u0sc(1);
soc(1)=100-VD.INIT.Dod0;
ah(1)=0;
ubat(1)=interp1(VD.BATT.dod_ocv,VD.BATT.ocv(1,:),100-soc(1),'linear','extrap')*VD.BATT.Nblocser;
E(1)=interp1(VD.BATT.dod_ocv,VD.BATT.ocv(1,:),100-soc(1),'linear','extrap')*VD.BATT.Nblocser;
ibat(1)=0;
psc(1)=0;
pbat(1)=0;
ux(1)=0;

for i=2:length(VD.CYCL.temps)
    [ERR,usc(i),u0sc(i),ux(i)]=calc_sc(ERR,VD.SC,i,param.pas_temps,isc(i),u0sc(i-1),ux(i-1));
    psc(i)=usc(i)*isc(i);
    if isfield(VD,'DCDC_2') && ~isempty(VD.DCDC_2)
        % Calcul en aval des super capacites
        [ERR,psc_res(i)]=calc_dcdc(1,ERR,VD.DCDC_2,usc(i),psc(i));
    else
        psc_res(i)=psc(i);
    end
    pbat_res(i)=pres(i)-psc_res(i);
    % Conditions de fonctionnement batterie
    if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
        [ERR,pbat(i)]=calc_dcdc(2,ERR,VD.DCDC_1,0,pbat_res(i));
    else
        pbat(i)=pbat_res(i);
    end
    [ERR,ibat(i),ubat(i),soc(i),ah(i),u0bat(i),rbat(i),RdF(i)]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat(i),soc(i-1),ah(i-1),pres(i),psc(i));
    if ~isempty(ERR)
        fprintf(1,'%s %d %s\n','Instant :',i,' sec.');
    end
    % On verifie que la tension du DCDC est compatible
    %(fait dans calc_cout_arc, dc normalt inutile)
    if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
        [ERR,pbat_res(i)]=calc_dcdc(1,ERR,VD.DCDC_1,ubat(i),pbat(i));
    end
end


% initialisation: pas rigoureusement vrai et ca faussera le bilan de
% puissance (mais peut etre mieux que zero ...)
isc(1)=isc(2);
ibat(1)=ibat(2);

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%function [ERR,u0sc,ux,isc,usc,psc,u0bat,ibat,ubat,pbat,rbat,soc,ah]=calc_lagrange_VELEC_BAT_SC(ERR,param,vehlib,VD,pres)
%
% Optimisation de la gestion de l energie dans une vehicule electrique
% avec batterie et super capacites
% Methode des multiplicateurs de lagrange
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,u0sc,ux,isc,usc,psc,u0bat,ibat,ubat,pbat,rbat,soc,ah,lambda]=calc_lagrange_VELEC_BAT_SC(ERR,param,vehlib,VD,pres)
global max_ham;
max_ham=1e6;

% Initialisation
u0sc=zeros(size(VD.CYCL.temps));
ux=zeros(size(VD.CYCL.temps));
isc=zeros(size(VD.CYCL.temps));
usc=zeros(size(VD.CYCL.temps));
psc=zeros(size(VD.CYCL.temps));
u0bat=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
pbat=zeros(size(VD.CYCL.temps));
rbat=zeros(size(VD.CYCL.temps));
soc=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));
lambda=zeros(size(VD.CYCL.temps));
isc_cible=zeros(size(VD.CYCL.temps));
soc(1)=100-VD.INIT.Dod0;

% Critere de sortie sur convergence etat de charge des VD.SC
NRJ_SC_utile=critere_cvgce(VD,VD.SC.minTension,VD.SC.maxTension);

if param.cvgce==1
    critere_sortie=NRJ_SC_utile*param.precision;
else
    % valeur arbitraire
    critere_sortie=1;
    param.nboucle_max=20; % juste pour passer dans la boucle
end

% Choix d'une valeur initiale de lambda
if param.cvgce==0
    % Choix du lambda fixe ou ?
    lambda(1)=param.lambda;
elseif param.cvgce==1
    lambda_ini=param.int_lambda; % Ibat^2
    lambda(1)=param.int_lambda(1);
end

critere_boucle=2*critere_sortie;
nboucle=0;

while (abs(critere_boucle)>critere_sortie) & nboucle<param.nboucle_max
    % Dichotomie pour convergence sur NRJ super condensateur
    if nboucle==1
        % Borne inf par defaut
        critere_inf=critere_boucle;
        lambda_inf=lambda(1);
        lambda(1)=lambda_ini(2);
    elseif nboucle==2
        % Organisation de l'intervalle
        if critere_boucle>critere_inf
            critere_sup=critere_boucle;
            lambda_sup=lambda(1);
        else
            % reorgranisation de l'intervalle
            critere_sup=critere_inf;
            lambda_sup=lambda_inf;
            critere_inf=critere_boucle;
            lambda_inf=lambda(1);
        end
        if critere_inf.*critere_sup>0
            f = errordlg('Probleme de valeurs initiales', 'calc_VELEC_BAT_SC','modal');
            critere_inf
            lambda_inf
            critere_sup
            lambda_sup
            break
        end
    end
    if nboucle>2
        if critere_boucle>0
            critere_sup=critere_boucle;
            lambda_sup=lambda(1);
        else
            critere_inf=critere_boucle;
            lambda_inf=lambda(1);
        end
    end
    if nboucle>=2
        lambda(1)=(lambda_inf+lambda_sup)/2;
    end
    
    fprintf(1,'%s %f %s %d\n','Valeur de lambda : ',lambda(1),' Boucle N:',nboucle);
    
    %% boucle sur le temps du cycle
    for t=1:length(VD.CYCL.temps)
        
        if t==param.tdebug
            % Tracee des possibles pour l'instant et la valeur de lambda
            [ERR,isc_cible(t),hamilt,ham]=trace_hamilt_BAT_SC(ERR,VD,param,lambda,param.pas_temps,t,pres,soc,ah,usc,u0sc,ux);
        end
        
        % Minimisation des possibles pour l'instant courant et la
        % valeur de lambda
        [ERR,isc_cible(t),lambda]=calc_hamilt_BAT_SC(ERR,VD,param,lambda,param.pas_temps,param.pas_isc,t,pres,soc,ah,usc,u0sc,ux);
        
        if t==param.tdebug
            fprintf(1,'%s %f \n','hamiltonien min :',hamilt);
            fprintf(1,'%s %d \n','courant correspondant :',isc_cible(t));
        end
        
        % Calcul backward une fois isc_cible de t fix???
        % Courant super-condensateur
        isc(t)=isc_cible(t);
        
        % Calculs super condensateur
        if t~=1
            [ERR,usc(t),u0sc(t),ux(t)]=calc_sc(ERR,VD.SC,t,param.pas_temps,isc(t),u0sc(t-1),ux(t-1));
        else
            [ERR,usc(t),u0sc(t),ux(t)]=calc_sc(ERR,VD.SC,t,param.pas_temps,isc(t),VD.SC.Vc0*VD.SC.Nblocser,VD.SC.Vx0*VD.SC.Nblocser);
        end
        
        % Puissance supercondesateur
        psc(t)=usc(t).*isc(t);
        
        if isfield(VD,'DCDC_2') && ~isempty(VD.DCDC_2)
            % Calcul en aval des super capacites
            [ERR,psc_res(t)]=calc_dcdc(1,ERR,VD.DCDC_2,usc(t),psc(t));
        else
            psc_res(t)=psc(t);
        end
        
        pbat_res(t)=pres(t)-psc_res(t);
        
        % Conditions de fonctionnement batterie
        if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
            [ERR,pbat(t)]=calc_dcdc(2,ERR,VD.DCDC_1,0,pbat_res(t));
        else
            pbat(t)=pbat_res(t);
        end
        
        % Calculs batterie
        if t==1
            [ERR,ibat(t),ubat(t),bidon,bidon,u0bat(t),rbat(t)]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat(t),soc(1),ah(1),pres(t),psc(t));
        else
            [ERR,ibat(t),ubat(t),soc(t),ah(t),u0bat(t),rbat(t)]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat(t),soc(t-1),ah(t-1),pres(t),psc(t));
        end
        if ~isempty(ERR)
ResXml=struct([]); %             [ah,soc]=backward_erreur(length(VD.CYCL.temps));
            return;
        end
        % On verifie que la tension du DCDC est compatible
        if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
            [ERR,pbat_res(t)]=calc_dcdc(1,ERR,VD.DCDC_1,ubat(t),pbat(t));
        end
        if ~isempty(ERR)
            fprintf(1,'%s %d %s\n','Instant :',t,'Probleme de calcul du DCDC batterie');
ResXml=struct([]); %             [ah,soc]=backward_erreur(length(VD.CYCL.temps));
            return;
        end
        
        if param.verbose>=2
            fprintf(1,'%s %.1f %s %.2f  %s %.2f %s %.2f   %s %.2f %s %.2f\n','calc_VELEC_BAT_SC - Temps: ',VD.CYCL.temps(t),'Pres:',pres(t),'Isc',isc(t), ...
                'Usc',usc(t)./VD.SC.Nblocser,'Ibat',ibat(t),'Ubat',ubat(t)./VD.BATT.Nblocser);
        end
        
    end % fin de la boucle sur le temps du cycle
    
    if param.cvgce==0
        break;
    end
    
    critere_boucle=critere_cvgce(VD,u0sc(1)./VD.SC.Nblocser,u0sc(end)./VD.SC.Nblocser);
    nboucle=nboucle+1;
    fprintf(1,'%s %f %s %f %s\n','Nrj utile des VD.SC : ',NRJ_SC_utile,' Wh Critere de sortie : ',critere_boucle,' Wh');
end % fin boucle while

if (abs(critere_boucle)<critere_sortie) & param.cvgce==1
    % Sortie sur critere de convergance satisfait
    fprintf(1,'%s\n','Sortie sur critere de convergence satisfait')
    fprintf(1,'%s %f %s %f %s\n','Nrj utile des VD.SC : ',NRJ_SC_utile,' Wh Critere de sortie : ',critere_boucle,' Wh');
elseif param.cvgce==1
    % Sortie sur nbre maxi d iterations atteintes
    fprintf(1,'%s\n','Sortie sur nbre maxi d iterations atteintes')
    fprintf(1,'%s\n','ATTENTION : la precision n est pas satisfaite')
    fprintf(1,'%s %f %s %f %s\n','Nrj utile des VD.SC : ',NRJ_SC_utile,' Wh Critere de sortie : ',critere_boucle,' Wh');
elseif param.cvgce==0
    fprintf(1,'%s %f %s %f %s\n','Nrj utile des VD.SC : ',NRJ_SC_utile,' Wh Critere de sortie : ',critere_boucle,' Wh');
end

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Gestion de l energie dans une vehicule electrique
% avec batteire et super capacites
% Methode de calcul en ligne
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,u0sc,ux,isc,usc,psc,u0bat,ibat,ubat,pbat,rbat,soc,ah,pbat_cible]=calc_ligne_VELEC_BAT_SC(ERR,param,vehlib,VD,pres)

% Initialisation
u0sc=zeros(size(VD.CYCL.temps));
ux=zeros(size(VD.CYCL.temps));
isc=zeros(size(VD.CYCL.temps));
usc=zeros(size(VD.CYCL.temps));
psc=zeros(size(VD.CYCL.temps));
u0bat=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
pbat=zeros(size(VD.CYCL.temps));
rbat=zeros(size(VD.CYCL.temps));
soc=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));
isc_cible=zeros(size(VD.CYCL.temps));
pbat_cible=zeros(size(VD.CYCL.temps));

soc(1)=100-VD.INIT.Dod0;
u0sc(1)=VD.SC.Vc0*VD.SC.Nblocser;

% Boucle sur le temps du cycle
for ind=1:length(VD.CYCL.temps)
    %% Implentation strategie en ligne
    if param.strat==0
        pbat_cible(ind)=pres(ind);
        
    elseif param.strat==1
        % Strategie valeur moyenne
        [ERR,pbat_cible(ind)]=moyenne_glissante(ERR,VD,param,pres,ind);
    elseif param.strat==2
        % Strategie filtre premier ordre
        if ind~=1
            [ERR,pbat_cible]=filtre_ordre1(ERR,VD,param,pres,pbat_cible,ind);
        else
            pbat_cible(1)=param.pini;
        end
    elseif param.strat==3
        % Strategie filtre 2nd ordre
        [ERR,pbat_cible]=filtre_ordre2(ERR,VD,param,pres,pbat_cible,ind);
    elseif param.strat==4
        % Strategie utilisateur
        [ERR,pbat_cible]=user_defined_on_line_strategy_VELEC_BAT_SC(ERR,VD,ind,param,pbat_cible,pres,u0sc);
     elseif param.strat==5
        % Strategie utilisateur
            if ind~=1
                p=(2*pres(ind)./ubat(ind-1)^2-param.lambda)*ubat(ind-1)^2/2;
            else
                p=0;
            end
            pbat_cible(ind)=pres(ind)-p;
   end % fin de boucle sur calcul de pbat_cible en ligne
    
    psc_cible(ind)=pres(ind)-pbat_cible(ind); % on fait abstraction du/des DCDC
    
    if ind~=1
        if isfield(param,'K_corr')
            %psc_corr(ind)=( param.K_corr* (u0sc(ind-1)-u0sc(1))^3 ) * (param.P_pond_batt/pbat_cible(ind));
            psc_corr(ind)=( param.K_corr* (u0sc(ind-1)-u0sc(1))^3 ) /pbat_cible(ind);
            psc_cible(ind)=pres(ind)-pbat_cible(ind)+psc_corr(ind);
        end
    end
 
 [ERR,isc_cible(ind),ibat(ind),u0sc,ux,u0bat(ind),soc,usc(ind),ubat(ind),pbat(ind),rbat(ind),ah] = calc_isc_psc(VD,param,psc_cible,u0sc,ind,ux,soc,pres,ah);
 isc=isc_cible;
    
    % On verifie que la tension du DCDC est compatible
    if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
        [ERR,pbat_res(ind)]=calc_dcdc(1,ERR,VD.DCDC_1,ubat(ind),pbat(ind));
    end
    if ~isempty(ERR)
        fprintf(1,'%s %d %s\n','Instant :',ind,'Probleme de calcul du DCDC batterie');
ResXml=struct([]); %         [ah,soc]=backward_erreur(length(VD.CYCL.temps));
        return;
    end
    
    fprintf(1,'%s %.1f %s %.2f  %s %.2f %s %.2f   %s %.2f %s %.2f\n','calc_VELEC_BAT_SC - Temps: ',VD.CYCL.temps(ind),'Pres:',pres(ind),'Isc',isc(ind), ...
        'Usc',usc(ind)./VD.SC.Nblocser,'Ibat',ibat(ind),'Ubat',ubat(ind)./VD.BATT.Nblocser)
end % fin de la boucle sur le temps du cycle
%figure
%plot(u0bat.*ibat-pbat-rbat.*ibat.*ibat)
psc=isc.*usc;

return;

