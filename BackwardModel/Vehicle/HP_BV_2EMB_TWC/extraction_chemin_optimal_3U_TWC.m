function [res,VD,param] = extraction_chemin_optimal_3U_TWC(pathname, name)
% Extraction du chemin optimal d'un graphe a 2 dimensions.
% Les 2 dimensions sont état de charge batterie et émissions de polluant
% param.alpha, beta et gamma viennent pondérer le poids des emissions
% Utilisation de la programmation dynamique pour calculer le chemin optimal
% for example see Utilities/TestCase/Backward/test_backward_HP_BV_2EMB_TWC.m
%

ERR = [];

load([pathname filesep name],'list_name');
nb_files = length(list_name);
long_calcul = 1;

if nb_files<4 % possible de tout charger en memoire
    for i = 1:nb_files
    clearvars -except ERR commande_opti ind_precedent long_calcul...
                      pathname list_name i nb_files param VD vehlib
        tic
        load([pathname filesep list_name{i} '.mat'])
        if param.verbose
            fprintf('chargement log : %.1f s\n',toc)
            fprintf(['log : ' list_name{i} '\n'])
        end
        
        % regarder les instants correspondant au fichier en cours
        % liste de variables commande_opti_nb
        commande_opti_nb = who('commande_opti_*');
        % indices correspondants
        ind = str2double(erase(commande_opti_nb,'commande_opti_'));
        % long_calcul : plus grand des plus grands indices des differents fichiers
        long_calcul = max(long_calcul,max(ind));
        
        for i_extract=1:length(ind)
            eval(['commande_opti{' num2str(ind(i_extract)) '} = commande_opti_' num2str(ind(i_extract)) ';'])
            eval(['ind_precedent{' num2str(ind(i_extract)) '} = ind_precedent_' num2str(ind(i_extract)) ';'])
        end
    end
else % pas possible de tout charger en memoire
    load([pathname filesep list_name{1} '.mat'],'param','VD','vehlib');
end

clearvars -except...
    ERR pathname list_name nb_files long_calcul...
    cost2go_1 soc_1 commande_opti ind_precedent ...
    cacm1_elec csec_cpl wsec_cpl dwsec_cpl ibat_min pas_temps wacm1 dwacm1 wacm1_elec dwacm1_elec wmt dwmt ...
    tps_calcul Dsoc_tract Dsoc_recup pas_soc soc_inf soc_sup lenX lenU VD param vehlib

% Reconstitution du graphe
soc_init = 100 - VD.INIT.Dod0;
soc_fin = soc_init + param.dsoc;

T_init = VD.INIT.Tamb+273;

% soc_1 / cost2go_1 du dernier fichier (fin cycle) pour tester le soc final
load([pathname filesep list_name{nb_files} '.mat'],'soc_1','cost2go_1')

if ~isfield(param,'online') || param.online == 0
    % on cherche tous les soc finaux correspondants au bilan batterie demande
    % ou qui rechargent la batterie
    % (et qui correspondent a des temperatures differentes)
    ind_BB = find((soc_1 - soc_fin)>=-10^-9);
else
    % Il n'y a qu'un chemin, on est en ligne
    ind_BB = 1;
end
% on choisit celui qui minimise le cout
[~,ind_min] = min(cost2go_1(ind_BB));

% on repasse en indexation globale
ind_min_BBN = ind_BB(ind_min);

if nb_files<4 
    % les vecteurs entiers sont en memoire
    % initialisation pour le dernier indice
    ind_opti(long_calcul) = ind_precedent{long_calcul}(ind_min_BBN);
    ind_com_opti(long_calcul) = commande_opti{long_calcul}(ind_min_BBN);
    taille_matrice(long_calcul) = length(commande_opti{long_calcul});
    
    % parcours du cycle depuis la fin pour remonter au chemin optimal
    for i_chemn = long_calcul:-1:2
        ind_com_opti(i_chemn-1) = commande_opti{i_chemn-1}(ind_opti(i_chemn));
        ind_opti(i_chemn-1) = ind_precedent{i_chemn-1}(ind_opti(i_chemn));
        taille_matrice(i_chemn-1) = length(commande_opti{i_chemn-1});
    end
else
    for ii = nb_files:-1:1
        
        tic
        load([pathname filesep list_name{ii} '.mat'])
        fprintf('chargement log : %.1f s\n',toc)
        fprintf(['log : ' list_name{ii} '\n'])
        
        % regarder les instants correspondant au fichier en cours
        % liste de variables commande_opti_nb
        commande_opti_nb = who('commande_opti_*');
        % indices correspondants
        ind = str2double(erase(commande_opti_nb,'commande_opti_'));
        % long_calcul : plus grand des plus grands indices des differents fichiers
        long_calcul = max(long_calcul,max(ind));
        
        if ii==nb_files
            i_max = max(ind);
        else
            i_max = max(ind)+1;
        end
        i_min = min(ind)+1;
        
        % initialisation pour le dernier indice (seulement dernier fichier)
        if ii==nb_files
            eval(['ind_com_opti(long_calcul) = commande_opti_' num2str(long_calcul) '(ind_min_BBN);'])
            eval(['ind_opti(long_calcul) = ind_precedent_' num2str(long_calcul) '(ind_min_BBN);'])
            eval(['taille_matrice(long_calcul) = length(commande_opti_' num2str(long_calcul) ');'])
        end
        
        % parcours du cycle depuis la fin pour remonter au chemin optimal
        % fichier par fichier
        for i_chemn = i_max:-1:i_min
            eval(['ind_com_opti(i_chemn-1) = commande_opti_' num2str(i_chemn-1) '(ind_opti(i_chemn));'])
            eval(['ind_opti(i_chemn-1) = ind_precedent_' num2str(i_chemn-1) '(ind_opti(i_chemn));'])
            eval(['taille_matrice(i_chemn-1) = length(commande_opti_' num2str(i_chemn-1) ');'])
        end
        
        clearvars -except...
            ERR pathname list_name nb_files ...
            long_calcul BB count soc_init T_init temps temps ...
            ind_com_opti ind_opti taille_matrice ...
            cacm1_elec csec_cpl  wsec_cpl dwsec_cpl ibat_min pas_temps wacm1 dwacm1 wacm1_elec dwacm1_elec wmt dwmt ...
            tps_calcul Dsoc_tract Dsoc_recup pas_soc  soc_inf soc_sup lenX lenU VD param vehlib

    end
end

% Recalcul des grandeurs de la chaine de traction
ERR = [];
[ERR,VD,~]=calc_cinemat(ERR,VD,param);
% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,~,Fres,Faero,Froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
% to be continued ...

% Recherche du mode electrique
mode_elec = ind_com_opti==0;

Tcat_1 = T_init;

temps = VD.CYCL.temps;

Tcat = nan(1,long_calcul);

wacm1_opti = nan(1,long_calcul);
dwacm1_opti = nan(1,long_calcul);
cacm1 = nan(1,long_calcul);
qacm1 = nan(1,long_calcul);

phi_opti = nan(1,long_calcul);
delta_AA_opti = nan(1,long_calcul);
padm_opti = nan(1,long_calcul);
dcarb = nan(1,long_calcul);

Dsoc_opti = nan(1,long_calcul);
ibat = nan(1,long_calcul);
ubat = nan(1,long_calcul);
pbat = nan(1,long_calcul);

cmt = nan(1,long_calcul);

nd_CO = nan(1,long_calcul);
nd_HC = nan(1,long_calcul);
nd_NOx = nan(1,long_calcul);
% cost2go_fwd = zeros(1,long_calcul+1);
% cost2go_rnd = zeros(1,long_calcul+1);

% Reconstitution de l'espace des commandes
if isfield(param,'pas_phi')
    phi = param.phi_min:param.pas_phi:param.phi_max;
elseif isfield(param,'phi_cst')
    phi = param.phi_cst;
else
    phi =  1; % Temporary, to be calculated on the fly   
end
l_phi = length(phi);

if isfield(param,'pas_delta_AA')
    delta_AA = param.delta_AA_min:param.pas_delta_AA:param.delta_AA_max;
elseif isfield(param,'delta_AA_cst')
    delta_AA = param.delta_AA_cst;
else
    delta_AA = 0; % Temporary, to be calculated on the fly
end
l_AA = length(delta_AA);


commande_phi = repmat(phi,1,l_AA);
commande_AA = repmat(delta_AA,l_phi,1);
commande_AA = commande_AA(:)';
commande_2U = [commande_phi;commande_AA];
commande_2U0 = commande_2U;

% Calcul des auxiliaires electriques sur le cycle complet
[ERR,pacc]=calc_acc(VD);

for i_bcl = 1:long_calcul
    % on ne veut pas faire l'analyse de memoire dans calc_poll_temp
    % pour l'exploitation : param.debug doit valoir 0
    param.debug = 0;

    if param.online == 3
        % Recalculate phi et SA from DNN
        [phi] = online_strategy_HP_BV_2EMB_3U_TWC_NN_phi(VD, param, wroue(i_bcl),croue(i_bcl),100-VD.INIT.Dod0,Tcat_1);
        [delta_AA] = online_strategy_HP_BV_2EMB_3U_TWC_NN_SA(VD, param, wroue(i_bcl),croue(i_bcl),100-VD.INIT.Dod0,Tcat_1);
        commande_2U0 = [phi; delta_AA];
    end

    % Reconstruire les variables de commande dont la taille varie
    % a chaque pas de temps
    Dsoc = (Dsoc_tract(i_bcl):pas_soc(i_bcl):Dsoc_recup(i_bcl))';
    %Dsoc = linspace(Dsoc_tract(i_bcl),Dsoc_recup(i_bcl),100)';
    l_commande_2U0 = length(commande_2U0(2,:));
    commande_soc = repmat(Dsoc,l_commande_2U0,1);
    commande_soc = commande_soc(:)';
    commande_2U = repmat(commande_2U0,1,length(Dsoc));
    commande_3U = [commande_2U;commande_soc];
    
    phi = commande_3U(1,:);
    delta_AA = commande_3U(2,:);
    Dsoc = commande_3U(3,:);
   
%     cost2go_0_rnd = cost2go_rnd(i_bcl);
%     cost2go_0_fwd = cost2go_fwd(i_bcl);
    
    Tcat(i_bcl) = Tcat_1;
    
    if ~mode_elec(i_bcl)
        wacm1_opti(i_bcl) = wacm1(i_bcl);
        dwacm1_opti(i_bcl)= dwacm1(i_bcl);

        Dsoc_opti(i_bcl) = Dsoc(ind_com_opti(i_bcl));
        
        % calcul de pbat en fonction du dsoc
        ibat(i_bcl) = -Dsoc_opti(i_bcl)/100*(VD.BATT.Nbranchepar*VD.BATT.Cahbat_nom*3600)/param.pas_temps;
        [ERR,ubat(i_bcl)]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat(i_bcl),100-VD.INIT.Dod0,0);
        pbat(i_bcl) = ubat(i_bcl).*ibat(i_bcl);
        
        % Puissance electrique des machines electriques
        pacm1_elec = pbat(i_bcl)-pacc(i_bcl);

        % calcul des pertes : carto inverse de la machine electrique
        qacm1(i_bcl) = interp2(VD.ACM1.Regmot_pert,VD.ACM1.Pelec_pert,VD.ACM1.Pertes_pelec,wacm1_opti(i_bcl),pacm1_elec,...
            'linear',max(max(VD.ACM1.Pertes_pelec))); %dernier argument : valeur constante pour tous les points hors du domaine
        
        % couples correspondants pour la ME et le MTH
        cacm1(i_bcl) = (pacm1_elec/VD.VEHI.nbacm1-qacm1(i_bcl))/wacm1_opti(i_bcl);
        cprim2_cpl = cacm1(i_bcl) - VD.ACM1.J_mg*dwacm1_opti(i_bcl);
        
        % Les variables sur le secondaire du coupleur sont indices avec le cycle de conduite
        [ERR,cprim1_cpl,~,~,~,~,~]=calc_adcpl(ERR,VD.ADCPL,csec_cpl(i_bcl),wsec_cpl(i_bcl),dwsec_cpl(i_bcl),cprim2_cpl,0,1);
        
        cmt(i_bcl) = cprim1_cpl + VD.MOTH.J_mt.*dwmt(i_bcl);        
        
        %[cmt(i_bcl), cacm1(i_bcl)] = calc_me_mt(Dsoc_opti(i_bcl),1,wacm1_opti(i_bcl),dwacm1(i_bcl),wmt(i_bcl),dwmt(i_bcl),csec_cpl(i_bcl),VD,param.pas_temps);
        if param.online >= 2
            % Quelques tests de verification
            cmt_f = (VD.MOTH.PMF_cst + VD.MOTH.PMF_N*(wmt(i_bcl)*30/pi) + VD.MOTH.PMF_N2*(wmt(i_bcl)*30/pi).^2)*VD.MOTH.Vd/(2*pi*VD.MOTH.Rev);
            if cmt(i_bcl)< (-1)*cmt_f
                fprintf(1,'\n%s\n','prediction is lower than engine friction torque');
                fprintf(1,'%s %.2f %s %.2f\n','cmt =',cmt(i_bcl),' cmt_f =',(-1)*cmt_f);
                cmt(i_bcl) = (-1)*cmt_f;
            end
        end

        N = wmt(i_bcl)*30/pi;
        if ~isfield(param,'phi_lut')
            phi_opti(i_bcl) = phi(ind_com_opti(i_bcl));
        else
            phi_opti(i_bcl) = interp2(VD.ECU.Speed_2dphi, VD.ECU.Torque_2dphi', VD.ECU.phi_2d', wmt(i_bcl), ...
                max(min(cmt(i_bcl),max(VD.ECU.Torque_2dphi)),min(VD.ECU.Torque_2dphi)));
        end
        if ~isfield(param,'delta_AA_lut')
            delta_AA_opti(i_bcl) = delta_AA(ind_com_opti(i_bcl));
        else
            delta_AA_opti(i_bcl) = 0;
        end

        [dcarb(i_bcl), padm_opti(i_bcl)] = calc_mt_3U_reverse(cmt(i_bcl),N,phi_opti(i_bcl),delta_AA_opti(i_bcl),VD);       

        
        %[nd_CO_rnd(i_bcl), nd_HC_rnd(i_bcl), nd_NOx_rnd(i_bcl), T_1_rnd] = calc_exhaust_3U(T_rnd(i_bcl),phi_opti(i_bcl),delta_AA_opti(i_bcl),padm_opti(i_bcl),dcarb(i_bcl),wmt(i_bcl),1,1,VD,param.pas_temps,param,i_bcl,mode_elec(i_bcl));
        %T_1_rnd = round(T_1_rnd/param.pas_T)*param.pas_T;
        [nd_CO(i_bcl), nd_HC(i_bcl), nd_NOx(i_bcl), Tcat_1] = calc_exhaust_3U(Tcat(i_bcl),phi_opti(i_bcl),delta_AA_opti(i_bcl),padm_opti(i_bcl),dcarb(i_bcl),wmt(i_bcl),1,1,VD,param.pas_temps,param,i_bcl,mode_elec(i_bcl));
    else
        wacm1_opti(i_bcl) = wacm1_elec(i_bcl);
        dwacm1_opti(i_bcl)= dwacm1(i_bcl);
        cacm1(i_bcl) = cacm1_elec(i_bcl); %TODO : est-ce la meilleure facon? peut-on partir de Dsoc_elec (non arrondi)?
        Dsoc_opti(i_bcl) = Dsoc_tract(i_bcl); % Dsoc_elec est Dsoc_tract quand le mode electrique est accessible
        ibat(i_bcl) = -Dsoc_opti(i_bcl)/100*(VD.BATT.Nbranchepar*VD.BATT.Cahbat_nom*3600)/param.pas_temps;
        [ERR,ubat(i_bcl)]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat(i_bcl),100-VD.INIT.Dod0,0);
        pbat(i_bcl) = ubat(i_bcl).*ibat(i_bcl);
        % Puissance electrique des machines electriques
        pacm1_elec = pbat(i_bcl)-pacc(i_bcl);
        % calcul des pertes : carto inverse de la machine electrique
        qacm1(i_bcl) = pacm1_elec-wacm1_opti(i_bcl).*cacm1(i_bcl);
        dcarb(i_bcl)= 0;
        cmt(i_bcl) = 0;
        %[nd_CO_rnd(i_bcl), nd_HC_rnd(i_bcl), nd_NOx_rnd(i_bcl), T_1_rnd] = calc_exhaust_3U(T_rnd(i_bcl),[],[],[],dcarb(i_bcl),wmt(i_bcl),1,1,VD,param.pas_temps,param,i_bcl,mode_elec(i_bcl));
        %T_1_rnd = round(T_1_rnd/param.pas_T)*param.pas_T;
        [nd_CO(i_bcl), nd_HC(i_bcl), nd_NOx(i_bcl), Tcat_1] = calc_exhaust_3U(Tcat(i_bcl),[],[],[],dcarb(i_bcl),wmt(i_bcl),1,1,VD,param.pas_temps,param,i_bcl,mode_elec(i_bcl));
    end
       
%     cout_arc_fwd = dcarb(i_bcl) + param.alpha...
%         *(nd_CO(i_bcl)/VD.TWC.CO_EURO4 + nd_HC(i_bcl)/VD.TWC.THC_EURO4 + nd_NOx(i_bcl)/VD.TWC.NOx_EURO4);
%     cout_arc_rnd = dcarb(i_bcl) + param.alpha...
%         *(nd_CO_rnd(i_bcl)/VD.TWC.CO_EURO4 + nd_HC_rnd(i_bcl)/VD.TWC.THC_EURO4 + nd_NOx_rnd(i_bcl)/VD.TWC.NOx_EURO4);
% 
%     cost2go_rnd(i_bcl+1) = cost2go_0_rnd + cout_arc_rnd;
%     cost2go_fwd(i_bcl+1) = cost2go_0_fwd + cout_arc_fwd;
end

wmt(mode_elec) = 0;

%% post-traitement : calcul des grandeurs pour la structure res
% Tcat(long_calcul) = Tcat_1;
% T_rnd(long_calcul) = T_1_rnd;
soc = soc_init + cumsum(Dsoc_opti);

CO = cumsum(nd_CO*param.pas_temps,'omitnan');
HC = cumsum(nd_HC*param.pas_temps,'omitnan');
NOx = cumsum(nd_NOx*param.pas_temps,'omitnan');

% indices de performance
d_km = sum(VD.CYCL.vitesse(1:long_calcul)*param.pas_temps)*1e-3;

conso_lp100km = sum(dcarb*param.pas_temps,'omitnan')./d_km*100/VD.MOTH.dens_carb;

CO_gpkm = CO(end)./d_km;
HC_gpkm = HC(end)./d_km;
NOx_gpkm = NOx(end)./d_km;

% efficacite catalyseur en fonction de la temperature (lambda non inclus)
% cas simple : non ammorce eff_T = 0
eff_Tcat = zeros(size(temps));
% autre cas : calcul
i_chaud_fwd = Tcat>VD.TWC.T0;
eff_Tcat(i_chaud_fwd) = exp(-VD.TWC.aT*(VD.TWC.dT./(Tcat(i_chaud_fwd)-VD.TWC.T0)).^VD.TWC.mT);

res.croue.x = temps;
res.croue.y = croue;
res.croue.label = 'Wheel torque';
res.croue.unit = 'Nm';

res.wroue.x = temps;
res.wroue.y = wroue;
res.wroue.label = 'Wheel speed';
res.wroue.unit = 'rd/s';

res.CO.x = temps;
res.CO.y = CO;
res.CO.label = 'Cumulative CO';
res.CO.unit = 'g';

res.HC.x = temps;
res.HC.y = HC;
res.HC.label = 'Cumulative HC';
res.HC.unit = 'g';

res.NOx.x = temps;
res.NOx.y = NOx;
res.NOx.label = 'Cumulative NOx';
res.NOx.unit = 'g';

res.dcarb.x = temps;
res.dcarb.y = dcarb;
res.dcarb.label = 'fuel mass flow';
res.dcarb.unit = 'g/s';

res.elhyb.x = temps;
res.elhyb.y = ~mode_elec;
res.elhyb.label = 'hybrid mode';
res.elhyb.unit = 'Nm';

res.cmt.x = temps;
res.cmt.y = cmt;
res.cmt.label = 'Engine torque';
res.cmt.unit = 'Nm';

res.wmt.x = temps;
res.wmt.y = wmt;
res.wmt.label = 'Engine speed';
res.wmt.unit = 'rad/s';

res.pmt.x = temps;
res.pmt.y = cmt.*wmt;
res.pmt.label = 'Power';
res.pmt.unit = 'W';

res.cacm1.x = temps;
res.cacm1.y = cacm1;
res.cacm1.label = 'Motor torque';
res.cacm1.unit = 'Nm';

res.wacm1.x = temps;
res.wacm1.y = wacm1_opti;
res.wacm1.label = 'Motor speed';
res.wacm1.unit = 'rad/s';

res.pacm1.x = temps;
res.pacm1.y = cacm1.*wacm1_opti;
res.pacm1.label = 'Power';
res.pacm1.unit = 'W';

res.qacm1.x = temps;
res.qacm1.y = qacm1;
res.qacm1.label = 'Motor Losses';
res.qacm1.unit = 'W';

res.padm.x = temps;
res.padm.y = padm_opti;
res.padm.label = 'Intake pressure';
res.padm.unit = 'mBar';

res.phi.x = temps;
res.phi.y = phi_opti;
res.phi.label = 'Equivalent air/fuel ratio';
res.phi.unit = '(-)';

res.deltaAA.x = temps;
res.deltaAA.y = delta_AA_opti;
res.deltaAA.label = 'Spark advance relative to the optimal';
res.deltaAA.unit = 'BTDC';

res.lenX.x = temps;
res.lenX.y = lenX;
res.lenX.label = 'Size of control variables';
res.lenX.unit = '/';

res.lenU.x = temps;
res.lenU.y = lenU;
res.lenU.label = 'Size of states variables';
res.lenU.unit = '/';

res.run = param;
res.tps_calcul = tps_calcul;

res.conso_lp100km = conso_lp100km;
res.CO_gpkm = CO_gpkm;
res.HC_gpkm = HC_gpkm;
res.NOx_gpkm = NOx_gpkm;

res.Tcat.x = temps;
res.Tcat.y = Tcat;
res.Tcat.label = 'Temperature';
res.Tcat.unit = 'K';

res.soc_inf.x = temps;
res.soc_inf.y = soc_inf;
res.soc_inf.label = 'SOC inf';
res.soc_inf.unit = '%';

res.soc_sup.x = temps;
res.soc_sup.y = soc_sup;
res.soc_sup.label = 'SOC sup';
res.soc_sup.unit = '%';

res.soc.x = temps;
res.soc.y = soc;
res.soc.label = 'SOC';
res.soc.unit = '%';

res.ubat.x = temps;
res.ubat.y = ubat;
res.ubat.label = 'Battery voltage';
res.ubat.unit = 'V';

res.ibat.x = temps;
res.ibat.y = ibat;
res.ibat.label = 'Battery current';
res.ibat.unit = 'A';

res.pbat.x = temps;
res.pbat.y = pbat;
res.pbat.label = 'Battery power';
res.pbat.unit = 'W';

res.Dsoc_tract.x = temps;
res.Dsoc_tract.y = Dsoc_tract;
res.Dsoc_tract.label = 'dSOC traction';
res.Dsoc_tract.unit = '%';

res.Dsoc_recup.x = temps;
res.Dsoc_recup.y = Dsoc_recup;
res.Dsoc_recup.label = 'dSOC recup';
res.Dsoc_recup.unit = '%';

res.pas_soc.x = temps;
res.pas_soc.y = pas_soc;
res.pas_soc.label = 'pas soc';
res.pas_soc.unit = '%';

res.eff_Tcat.x = temps;
res.eff_Tcat.y = eff_Tcat;
res.eff_Tcat.label = 'Efficiency';
res.eff_Tcat.unit = '-';

% res.cost2go_fwd.x = temps;
% res.cost2go_fwd.y = cost2go_fwd;
% res.cost2go_fwd.label = 'Cost to go';
% res.cost2go_fwd.unit = '-';

if param.verbose > 0
    % Sauvegarde d'un fichier de resultat
    res_name = strrep([list_name{1} '.mat'],'rec','res');
    save(res_name, 'res', 'VD', 'param');
end

if param.verbose == 0
    % Suppress rec*.mat file
    delete([list_name{1} '.mat']);
end

if param.verbose ~= 0
    % Resultats synthetiques et eventuellement graphes
    strat = {'poll'};
    type_strat = {'poll'};
    strat_label = {'Current calc'};
    filenames = {res_name};
    exploit_HP_BV_2EMB_3U_TWC(strat,type_strat, strat_label, filenames);
else
    fprintf('Consommation : %.4f l/100km \n',res.conso_lp100km);
    fprintf('Emissions de CO2 : %.4f g/km \n', ...
        44*res.conso_lp100km*VD.MOTH.dens_carb/(12+VD.MOTH.CO2carb)/100);
    fprintf('bilan batterie (sans arrondi) : %.3f %% \n',res.soc.y(end)-res.soc.y(1));
    fprintf('CO : %.3f g/km \n',res.CO_gpkm);
    fprintf('HC : %.3f g/km \n',res.HC_gpkm);
    fprintf('NOx : %.3f g/km \n',res.NOx_gpkm);
    
end

end
