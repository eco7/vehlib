% function [ERR,ham,Lambda,cprim2_cpl,ibat,soc,ubat]=calc_hamilt_elec_HP_BV_2EMB(ERR,VD,param,csec_cpl,wsec_cpl,dwsec_cpl,Lambda_p,j,soc_p,calc_min,Lambda_cst)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Minimisation de la courbe des courants BAT/SC admissibles pour la valeur
% de lambda
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

function [ERR,ham,Lambda1,Lambda2,cprim2_cpl,ibat,soc,ubat,Tcat, CO_eff, HC_eff, NOx_eff, CO, HC, NOx,indice_rap_opt,dQs,dQin,dQgen]=calc_hamilt_elec_HP_BV_2EMB_TWC(ERR,VD,param,csec_cpl,wsec_cpl,dwsec_cpl,Lambda1_p,Lambda2_p,j,soc_p,Tcat_p,CO_eff_p, HC_eff_p, NOx_eff_p,calc_min)

ERR= [];

if length(wsec_cpl)==1 && VD.CYCL.rappvit(j)==0
    wsec_cpl=0; % Si on est en tout ??lec au point mort le motelec ne tourne pas
    csec_cpl=0;
elseif length(wsec_cpl)~=1
    wsec_cpl(1)=0; 
    csec_cpl(1)=0;
end

[ERR,cprim2_cpl,wprim2_cpl,dwprim2_cpl]=calc_red(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0);

%Connexion
wacm1=wprim2_cpl;
dwacm1=dwprim2_cpl;
cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;

% Calcul des conditions en amont de la machine electrique
%%% limitations ME
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);

% Si cacm1<cacm1_min on fait de la recup et on vas faire le max possible
% le reste en frein meca, donc on sature cacm1 ?? cacm1_min
cacm1(cacm1<cacm1_min)=cacm1_min(cacm1<cacm1_min);

%[Li,Co]=size(cacm1);
%wacm1=repmat(wacm1,Li,1);
[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');
% ERR sera ?? un si au moins un ??l??ment ?? NaN ?? voir comment g??re

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);
if ~isempty(ERR)
    [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end

%if length(wsec_cpl)==1 % sinon on cr??e un vecteur inutilement
    pacc=pacc(j);
%end

% Puissance reseau electrique
pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc;

[ERR,ibat,ubat,soc,ah,E,R,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,soc_p,0,pres,0);

if strcmp(param.lambda1_cst,'non') % ce calcul ??tant long il est pr??ferable de la faire une fois pour toute 
    % et pas ?? chaque appel de la fonction
    if ~isfield(VD.BATT,'dE_dEn')
        if isfield(VD.BATT,'tbat')
            [bidon,Li_T20]=min(abs(VD.BATT.tbat-20));
        elseif isfield(VD.BATT,'Tbat_table')
            [bidon,Li_T20]=min(abs(VD.BATT.Tbat_table-20));
        end
        Ebatt=(trapz(VD.BATT.dod_ocv,VD.BATT.ocv(Li_T20,:))-cumtrapz(VD.BATT.dod_ocv,VD.BATT.ocv(Li_T20,:)))*3600*VD.BATT.Cahbat_nom/100*VD.BATT.Nblocser*VD.BATT.Nbranchepar;
        OCV=VD.BATT.ocv(1,:)*VD.BATT.Nblocser;
        dE_dEn(2:length(Ebatt)-1)=(OCV(3:end)-OCV(1:end-2)) ./ (Ebatt(3:end)-Ebatt(1:end-2));
        dE_dEn(1)=dE_dEn(2);
        dE_dEn(length(Ebatt))=dE_dEn(length(Ebatt)-1);
        VD.BATT.Enbatt=Ebatt;
        VD.BATT.dE_dEn=dE_dEn;
    end

    if j>1
        DOD=100-soc;
        dE_dEn=0*ones(1,length(DOD));
       
        dE_dEn=interp1(VD.BATT.dod_ocv,VD.BATT.dE_dEn,DOD);
        Pbat=ubat.*ibat;
        dQdEnsc=-VD.BATT.RdFarad./(2*R).*  (2*E.*dE_dEn-dE_dEn.*sqrt(E.^2-4*Pbat.*R)-((E.^2).*dE_dEn)./sqrt(E.^2-4*Pbat.*R)) ; % si seul la tension ?? vide d??pend de l'??nergie stock??
        %dQdEnsc=0;
%    if strcmp(FP.exist,'oui')
%         Ensc_min=FP.xmin*(1+FP.delta_min);
%         Ensc_max=FP.xmax*(1-FP.delta_max);
%         dfonc=0*ones(1,length(Cme));
%         dfonc(Ensc < Ensc_min)=2*FP.rmin*((Ensc(Ensc<Ensc_min)-(Ensc_min))/(FP.xmax^2));
%         dfonc(Ensc > Ensc_max)=2*FP.rmax*((Ensc(Ensc>Ensc_max)-(Ensc_max))/(FP.xmax^2));
%    else
%       dfonc=0;
%    end
        dfonc=0;
        Dt=param.pas_temps;
        Lambda=(Lambda_p-dfonc)./(1+dQdEnsc*Dt);
    else
        Lambda=Lambda_p*ones(1,length(cacm1));
    end

else
    Lambda1=Lambda1_p;
end

if strcmp(param.lambda2_cst,'non')
   
    dQdTcat=-(VD.TWC.HsA);
    Dt=param.pas_temps;
    Lambda2=(Lambda2_p)./(1+dQdTcat*Dt/(VD.TWC.MCata*VD.TWC.CpCata));
else
    Lambda2=Lambda2_p;
end

if isfield(VD,'TWC')
    % Calcul des emissions moteur    
    [ERR, Tcat, Texh, CO_eff, HC_eff, NOx_eff, CO, HC, NOx,dQs,dQin,dQgen]= ...
        calc_exhaust_emissions(ERR, param, VD ,j , 0, 0, 0, ...
        Tcat_p, CO_eff_p, HC_eff_p, NOx_eff_p);
end

%%% Calcul conso et hamiltonien
if isfield(param,'K')
    QCata=dQs;
    Tcat=Tcat_p+(dQs)/(VD.TWC.MCata*VD.TWC.CpCata)*param.pas_temps;

    ham=Lambda1.*ibat.*E.*RdF+...
        Lambda2.*1/(VD.TWC.MCata*VD.TWC.CpCata)*QCata;
   if  sum(j==param.tdebug)>=1
        
        plot(ibat,Lambda1.*ibat.*E.*RdF,'*k')
        hold on
        plot(ibat,Lambda2.*1/(VD.TWC.MCata*VD.TWC.CpCata).*QCata,'*m')
        plot(ibat,ham)
        pause
        hold off
    end

else
    ham=Lambda.*RdF.*ibat.*E;
end
%%% Limitations sur le courant batterie et le couple max

ham(isnan(ibat) | isnan(qacm1) | cacm1>cacm1_max | cacm1<cacm1_min) = param.Ham_hors_limite;

%%% En rapport de boite libre il faut interdire le Point mort si csec_cpl>0 et wacm1 >0
% le pb de acm1=0 se pose en fin de decel : on peut avoir wacm1=0 et
% csec_cpl>0 a cause des inerties et des limitations sur cacm1_min dans
% calc_HP_BV_2emB
if length(ham)>1 & csec_cpl(2)>0 & wacm1(2)>0
    ham(1)=param.Ham_hors_limite;
end

%%% calcul du min et des grandeurs associ??
indice_rap_opt=NaN;
if calc_min==1 && length(wsec_cpl)~=1 % Sinon on renvoit les grandeurs vectotielles pour trace hamilt notamment
    [ham,i_min]=min(ham);
    indice_rap_opt=i_min;
    ibat=ibat(i_min);
    ubat=ubat(i_min);
    soc=soc(i_min);
    cacm1=cacm1(i_min);
    qacm1=qacm1(i_min);
    cprim2_cpl=cprim2_cpl(i_min);
    Tcat=Tcat(i_min);
    Texh=Texh(i_min);
    CO_eff=CO_eff(i_min);
    HC_eff=HC_eff(i_min);
    NOx_eff=NOx_eff(i_min);
    CO=CO(i_min);
    HC=HC(i_min);
    NOx=NOx(i_min);
    dQs=dQs(i_min);
    dQin=0;
    dQgen=0;
    if strcmp(param.lambda1_cst,'non')
        Lambda1=Lambda1(i_min);
    end
    if strcmp(param.lambda2_cst,'non')
        Lambda2=Lambda2(i_min);
    end
end


return


