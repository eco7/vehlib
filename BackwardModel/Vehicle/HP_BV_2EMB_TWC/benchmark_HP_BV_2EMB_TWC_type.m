function [res, VD, param] = benchmark_HP_BV_2EMB_TWC_type(vehicle, cycle, Soc0, Dsoc, online)

if nargin ==0
    %vehicle = 'Peugeot_308SW_1L6VTI16V_HP_BV_2EMB_kokam_CONTROL';
    vehicle = 'Renault_Kadjar_TCE130_HP_BV_2EMB_CONTROL';
end
if nargin <=1
    cycle = 'CIN_WLTC_Classe3_BV6';
    % cycle = 'CIN_NEDC_BV';
    % cycle = 'CIN_ECE15_BV';
    % cycle = 'CIN_test_BV';
end
if nargin <=2
    Soc0 = 50;
end
if nargin <=3
    Dsoc = 0;
end
if nargin <=4
    online = 0;
end

% type d'optimisation
param.optim = '3U_TWC';

% Fichier parametres
param.verbose = 1; % 0 : no result/recorder files; no graphics; 1 : result/recorder files and graphics; 2 : debug mode
param.MemLog = 0; % 0: no memory usage calculation; 1: memory usage calculation

% temperature pour le calcul des parametres batterie
param.tbat = 20;

% parametre de calcul des courbes de Cmin/Cmax pour la machine electrique
param.Ures = 600;

% puissance de la machine electrique
param.Pacm1 = 25000;

% initialisation du calcul de programmation dynamique
param.cost2go_init = 0;

% parametres de grille de soc
%param.pas_soc = 0.02;
param.pas_p0batt_theo = 1500; % exprime en puissance
param.soc_max = 80;
param.soc_min = 20;

% parametres de grille de temperatureplo
param.pas_T = 100;
param.pas_T_var=0;

% poids relatif conso-pollu
param.alpha = 0;
param.beta = 0;
param.gamma = 0;

% delta de soc a la fin du calcul :
% soc_fin = soc_init + param.dsoc;
param.dsoc = Dsoc;

% parametres de la commande
% param.phi_min = 0.993;
% param.phi_max = 1.008;
% param.pas_phi = 0.015;
param.phi_cst = 1.;
%param.phi_lut = 1.;

% param.delta_AA_min = -30;
% param.delta_AA_max = 0;
% param.pas_delta_AA = 10;
param.delta_AA_cst = 0;
%param.delta_AA_lut = 1; Non implemente encore

param.pas_temps = 1;

% Taille maxi pour les arcs
%param.max_size_XU = 5e9; % 256Gb Ram
param.max_size_XU = 5e9/16; % 16Gb Ram

% calcul de l'acceleration centree
param.calc_accel=1;
%param.calc_accel=3;

%param.lg_calcul = 30;
param.fit_pertes_acm = 0;

% Optimisation des ecritures au detriment de la lisibilite pour diminuer
% l'usage de la ram (cf graphe...)
param.optiRam = 1;

% Utilisation de csortrows, cnearestindex (csortrows.c, algorithme de tri-merge sort- ecrit en langage C) en lieu et place 
% de sortrows pour gain de rapidite et usage ram (cf graphe...)
% Derniers tests montrent que csortrows peut intéressant en 2022a 
% (25% de temps de calcul en 2018a, qques pc en 2022a)
% versionC = 0 : aucune fonction C
% versionC = 1 : csortrows et cnearestindex
% versionC = 2 : cnearestindex seul (utilise pour pas_T_var seult)
param.versionC = 2;

% Parallelisation du code
param.parallel = 0;

% Valeur de reference pour adimensionner : Phi = 1 et AA opti (svn Release 640)
param.ValueRef='EURO6'; % 'EURO4';

% Pour coder des strategies en ligne
param.online = online;
if param.online == 2
    % Import NN
    NN_ModelName = 'NrjMngt_Conso_HP_BV_2EMB_TWC.h5';
    classNames = {'0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21'};
    nrj_mngt_neuralnet_1 = importKerasNetwork(NN_ModelName, 'Classes', classNames,'OutputLayerType','classification');
    param.NN.nrj_mngt_neuralnet=nrj_mngt_neuralnet_1;
    % Import Stats
    NN_settings = 'Settings_NrjMngt_Conso_HP_BV_2EMB_TWC.mat';
    load(NN_settings);
    param.NN.Settings=Settings;
end


numcal_increment;

% Read vehicle Data
[vehlib, VD]=vehsim_auto2(vehicle,'HP_BV_2EMB',cycle,100-Soc0,2,0);
ERR = [];

try
    %Vd = 1.6;
    for ij = 1:1 %length(Vd)
%         param.champ_modif{1} = 'MOTH.Vd';
%         param.valeur_modif(1) = Vd(ij);
%         param.champ_modif{2} = 'MOTH.Masse_mth';
%         param.valeur_modif(2) = 140*param.valeur_modif(1)/1.6;
%         param.champ_modif{3} = 'MOTH.J_mt';
%         param.valeur_modif(3) = 0.2*param.valeur_modif(1)/1.6;
        
        % Lancement des calculs en mode backward
        %[ERR, vehlib, VD, res]=calc_backward(ERR,vehlib,param,VD);
        tic;
        name = graphe_HP_BV_2EMB_3U_TWC(ERR, vehlib, param, VD);
        toc
        
        [res,VD,param] = extraction_chemin_optimal_3U_TWC('./', name);
        
    end
catch ME
    global MemLog
    name_memory = 'error.log';
    log4vehlib('open', name_memory);
    log4vehlib('error',ME);
    log4vehlib('close');
end

% Store param structure ...
VD.param = param;
	
end

