function [name, ERR] = graphe_HP_BV_2EMB_3U_TWC(ERR, vehlib, param, VD)
%
% Construction d'un grahe 2D pour véhicules hybrides parallele a 2 embrayages.
% Les 2 dimensions sont état de charge batterie et Température du catalyseur
% Les 3 commandes (3U) sont la pression admission, la richesse et l'avance relative a
% l'allumage optimal.
% param.alpha, beta et gamma viennent pondérer le poids des emissions de CO, HC et NOx
% Utilisation de la programmation dynamique pour calculer le chemin optimal
% name et le nom du fichier mat qui contient les commandes optimales à chaque instant 
% (celles qui minimisent le cost2go) et le point d'ou l'on vient en soc
%
% [res,VD,param] = extraction_chemin_optimal_3U_TWC(pathname, name)
% permet d'extraire le chemin optimal a partir du fichier mat
%
% for example see Utilities/TestCase/Backward/test_backward_HP_BV_2EMB_TWC.m
%
ERR = [];

cinem = vehlib.CYCL;

% Modification de la puissance de la machine electrique
Pme_max = param.Pacm1;
Cmax_Pme = interp2(VD.ACM1.Tension_cont,VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot,param.Ures,VD.ACM1.Regmot_cmax);
Pme_max_init = VD.ACM1.Regmot_cmax.*Cmax_Pme';
Pme_max_init = max(Pme_max_init);
kelec = Pme_max/Pme_max_init;
[VD.ACM1] = func_modif_me(kelec,VD.ACM1);

[~,VD.ACM1]=inverse_carto_melec(VD.ACM1,0);

% Calcul du nombre d elements en serie    
Pbatt_max = Pme_max/0.8;
% pour la batterie de la prius Ibat max et min sont des constantes
% entre 20% et 80% de soc, et sont nulles en dehors de ce domaine
Ibat_max = max(VD.BATT.Ibat_max);
% calcul du nombre de cellules correspondant
nb_cell = Pbatt_max/Ibat_max/interp2(VD.BATT.dod_ocv,VD.BATT.tbat_ocv,VD.BATT.ocv,100-VD.INIT.Dod0,param.tbat);
% une seule branche en parallele
VD.BATT.Nblocser = round(nb_cell);

% traitement des demandes de modification de champ dans param
if isfield(param,'champ_modif')
    for i=1:length(param.champ_modif)
        eval(['VD.' param.champ_modif{i} ' = param.valeur_modif(i);'])        
    end
end

%% fichier de log
if isfield(param,'fichier_init')
    load(param.fichier_init,'i_fichier')
else
    i_fichier = 1;
end

name_source = name_rec_file(cinem,VD,param);
name = [name_source '_' num2str(i_fichier)];
list_name{i_fichier} = name;

if ~isfield(param,'fichier_init')
    save(name,'list_name','-v7');
end

if isfield(param,'pas_phi')
    if isfield(param,'phi_cst') || isfield(param,'phi_lut')
        error('erreur de definition des parametres : il doit y avoir soit un pas soit une constante, soit une LUT pour Phi')
    end
    Phi = param.phi_min:param.pas_phi:param.phi_max;
elseif isfield(param,'phi_lut')
    if isfield(param,'phi_cst')
        error('erreur de definition des parametres : il doit y avoir soit un pas soit une constante, soit une LUT pour Phi')
    end
    Phi =  1; % Temporary, to be calculated on the fly
else
    Phi = param.phi_cst;
end
l_Phi = length(Phi);

if isfield(param,'pas_delta_AA')
    if isfield(param,'delta_AA_cst') || isfield(param,'delta_AA_lut')
        error('erreur de definition des parametres : il doit y avoir soit un pas soit une constante, soit une LUT pour delta_AA')
    end
    delta_AA = param.delta_AA_min:param.pas_delta_AA:param.delta_AA_max;
elseif isfield(param,'delta_AA_lut')
    if isfield(param,'delta_AA_cst')
        error('erreur de definition des parametres : il doit y avoir soit un pas soit une constante, soit une LUT pour delta_AA')
    end    
    delta_AA = 0; % Temporary, to be calculated on the fly
else
    delta_AA = param.delta_AA_cst;
end
l_AA = length(delta_AA);

commande_Phi = repmat(Phi,1,l_AA);
commande_AA = repmat(delta_AA,l_Phi,1);
commande_AA = commande_AA(:)';
commande_2U = [commande_Phi;commande_AA];
commande_2U0 = commande_2U;

%% Calcul de la chaine de traction %%
% Profil de vitesse
if ~isfield(param,'pas_temps') | param.pas_temps==0
    param.pas_temps=1;
end
[ERR,VD,~]=calc_cinemat(ERR,VD,param);
ntps = size(VD.CYCL.temps);

% Valeurs par defaut des parametres non renseignes
if ~isfield(param,'lg_calcul')
    param.lg_calcul = ntps(2);
% else
%     param.lg_calcul = param.lg_calcul-1;
end

% Calcul des couples et regimes
% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
if ~isempty(ERR)
    return;
end

% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,~,Fres,Faero,Froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
if ~isempty(ERR)
    return;
end

% Connexion
csec_red=croue;
wsec_red=wroue;
dwsec_red=dwroue;
% Calcul des conditions en amont du reducteur
[ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);
if ~isempty(ERR)
    return;
end

% Connexion
csec_bv=cprim_red;
wsec_bv=wprim_red;
dwsec_bv=dwprim_red;

% Calcul des conditions en amont de la boite de vitesse
% NB si on est en rapport libre ou optimaux on as des matrices 
% avec en nb de ligne le nb de rapport de boite plus pm
[ERR,cprim_bv,wprim_bv,dwprim_bv]=calc_bv(VD,csec_bv,wsec_bv,dwsec_bv,0);
if ~isempty(ERR)
    return;
end

% Connexion 
csec_emb1=cprim_bv;
wsec_emb1=wprim_bv;
dwsec_emb1=dwprim_bv;

%% discretisation en grille
% parametres de la grille : nombre de points en temperature
% sert pour le calcul de l'identifiant unique (multiplicateur de soc)
% hypothese : catalyseur toujours en dessous de 2000K
Tcat_max = 2000;
if ~param.pas_T_var
    nb_pts_T = (Tcat_max + 1)/param.pas_T;
    nb_pts_T = ceil(nb_pts_T/10000)*10000;
else
    x1 = 350; %K
    y1 = 0.05; %K/s
    x2 = 600;
    y2 = 0.25;
    a=(y2-y1)/(x2-x1);
    b = y1-a*x1;
    dTmin=0.1;
    
    Tgrid(1)=293;
    pas_T(1) = dTmin;
    i=1;
    while Tgrid(i)<Tcat_max
        i=i+1;
        pas_T(i)=max(dTmin,a*Tgrid(i-1)+b)*param.pas_temps;
        Tgrid(i) = Tgrid(i-1)+pas_T(i);
    end
    nb_pts_T = length(pas_T)+1;
    Tgrid = Tgrid(:);
    pas_T = pas_T(:);
end

save(name,'param','VD','vehlib','-append');
 
%% Calcul des conditions en mode tout electrique
% Prise en compte du frein meca. 
% Quand Croue <0, on considere que dans ce cas cprim1_cpl =0 (mode tt electrique)
% Embrayage ouvert
[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,zeros(size(wsec_emb1)));

% connexion
wsec_cpl_elec=wprim_emb1;
dwsec_cpl_elec=dwprim_emb1;
csec_cpl_elec=cprim_emb1;
[ERR,cprim2_cpl,wprim2_cpl,dwprim2_cpl]=calc_red(ERR,VD.ADCPL,csec_cpl_elec,wsec_cpl_elec,dwsec_cpl_elec,0);

% Connexion
wacm1_elec=wprim2_cpl;
dwacm1_elec=dwprim2_cpl;
cacm1_elec=cprim2_cpl+VD.ACM1.J_mg.*dwacm1_elec;

% limite de couple de la machine en mode electrique
Cacm1_max_elec = interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec);
Cacm1_min_elec = interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1_elec);

% On vient limiter cacm1 a sa valeur min (il y aura du frein mecanique)
cacm1_elec=max(cacm1_elec,Cacm1_min_elec);

if isfield(VD.ECU,'Vit_Cmin')
    cacm1_elec=max(cacm1_elec,interp1(VD.ECU.Vit_Cmin,VD.ECU.Cpl_min,wacm1_elec));
end

% en traction le mode elec est disponible si Ctot est inferieur a Cmax
zev = cacm1_elec<=Cacm1_max_elec;
% Si le mode electrique est impossible, on sature cacm1_elec a Cacm1_max_elec
cacm1_elec = min(cacm1_elec, Cacm1_max_elec);

%% Calcul des conditions en mode hybride
% embrayage ferme
[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,~]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,ones(size(csec_emb1)));
% connexion
wsec_cpl=wprim_emb1;
dwsec_cpl=dwprim_emb1;
csec_cpl=cprim_emb1;

wmt=wsec_cpl;
dwmt=dwsec_cpl;

% limite de couple du moteur thermique
%N = wmt*30/pi;
%Cmt_min = -(VD.MOTH.PMF_cst + VD.MOTH.PMF_N*N + VD.MOTH.PMF_N2*N.^2)*VD.MOTH.Vd/(VD.MOTH.Rev*2*pi);
% calcul du couple max
Padm = interp1(VD.MOTH.p_intake_wmt,VD.MOTH.p_intake,wmt,'linear','extrap');
if isfield(param,'phi_lut') && param.phi_lut == 1
    commande_Phi = interp2(VD.ECU.Reg_2dPhi2, VD.ECU.Padm_2dPhi2', VD.ECU.Phi_2d2', wmt, Padm)';
    commande_AA = repmat(commande_AA,length(wmt),1);
    Cmt_max=zeros(size(wmt));
    for ii=1:length(wmt)
        [~, Cmt_max(ii)] = calc_mt_3U(wmt(ii),commande_Phi(ii),commande_AA(ii),Padm(ii),1,VD);
    end
else
    [~, Cmt_max] = calc_mt_3U(wmt',commande_Phi,commande_AA,Padm',1,VD);
    Cmt_max = max(Cmt_max,[],2)'; % On ne garde que les maximums
    
end
    
% Connexion
cprim1_cpl=Cmt_max-VD.MOTH.J_mt.*dwsec_cpl;
[ERR,~,~,~,wprim2_cpl,dwprim2_cpl,cprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,cprim1_cpl,0,2);

%Connexion
wacm1=wprim2_cpl;
dwacm1=dwprim2_cpl;
cacm1_hyb_min=cprim2_cpl+VD.ACM1.J_mg.*dwacm1; % C'est la recuperation maxi

% Si cacm1_hyb > cacm1_max et mode elec impossible = on ne passe pas le cycle
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
    
I=find(cacm1_hyb_min>cacm1_max & zev==0,1);
if ~isempty(I)
    chaine='error in graphe_HP_BV_2EMB_3U_TWC : the power train cannot provide the required power' ;
    ERR=MException('BackwardModel:graphe_HP_BV_2EMB_3U_TWC',chaine);
    return
end

%% Calcul du pas de soc et des limites du graphe
[ERR,~,~,~,~,E]=calc_batt(ERR,VD.BATT,param.pas_temps,param.pas_p0batt_theo,100-VD.INIT.Dod0,0,-1,0,param);
pas_soc = 100*param.pas_temps*(param.pas_p0batt_theo/E)/(VD.BATT.Nbranchepar*VD.BATT.Cahbat_nom*3600);
pas_soc = ones(size(wroue))*pas_soc;

% Calcul des limites du graphe
[ERR,~,~,~,soc_min,soc_max,Dsoc_min,Dsoc_max,~]=calc_limites_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
if ~isempty(ERR)
    chaine='error in calc_limites_HP_BV_2EMB' ;
    ERR=MException('BackwardModel:graphe_HP_BV_2EMB_3U_TWC',chaine);
    return
end

param.prec_graphe = -pas_soc(1);
[ERR,~,~,~,soc_inf,soc_sup,Dsoc_tract,Dsoc_recup] =limite2indice_soc(VD,soc_min,soc_max,param,0,Dsoc_min,Dsoc_max);
if ~isempty(ERR)
    chaine='error in limite2indice_soc' ;
    ERR=MException('BackwardModel:graphe_HP_BV_2EMB_3U_TWC',chaine);
    return
end

% Calcul des auxiliaires electriques sur le cycle complet
[ERR,Pacc]=calc_acc(VD);

%% Normalisation pour somme ponderee %%
%CIN_NEDC_BV_lisse %chargement du NEDC pour le calcul des valeurs moyennes sur cycle normalise

km = sum(VD.CYCL.vitesse(1:end)*(VD.CYCL.temps(2)-VD.CYCL.temps(1)))*1e-3; %distance parcourue
km_s = km./VD.CYCL.temps(end); %distance moyenne parcourue en une seconde
% Normalized values with regards to CAFE consumption, EURO6 emissions
dcarb_norm = VD.TWC.Conso_Cafe/100*VD.MOTH.dens_carb*km_s; %debit carburant moyen
CO_norm = VD.TWC.CO_EURO6*km_s;
HC_norm = VD.TWC.THC_EURO6*km_s;
NOx_norm = VD.TWC.NOx_EURO6*km_s;

% enregistrement memoire
if param.MemLog
    global MemLog
    name_memory = ['mem_' erase(name_source,'rec_') '.log'];
    log4vehlib('open',name_memory);
end

if isfield(param,'parallel') && param.parallel == 1
    % creates and returns a thread-based pool
    pool = parpool('Threads');
    %pool = parpool('Processes');
end

%% initialisation
% premier point : soc init, T init et cout initial
if isfield(param,'fichier_init')
    load(param.fichier_init)
    i_init = i+1; clearvars i;
else
    soc_1 = 100 - VD.INIT.Dod0;
    T_1 = VD.INIT.Tamb+273;
    cost2go_1 = param.cost2go_init;
    i_init = 1;
end

lenX = ones(1,param.lg_calcul)*param.max_size_XU;
lenU = ones(1,param.lg_calcul)*param.max_size_XU;

tic

%% Boucle temporelle
for i = i_init:param.lg_calcul
    
if mod(i,5)==0 && param.verbose
    fprintf('iteration %.0f : %.1f s\n',i,toc)
end

% nouveau fichier si depassement de la taille maximale (2e9)
if mod(i,10)==0
    info = dir([name '.mat']);
    if info.bytes >= 2e9
        i_fichier = i_fichier + 1;
        name = [name_source '_' num2str(i_fichier)];
        list_name{i_fichier} = name;
        save(list_name{1},'list_name','-append');
        tps_debut = toc;
        save(name,'VD','param','tps_debut','-v7');
    end
end

soc_0 = soc_1;
T_0 = T_1;
cost2go_0 = cost2go_1;
clear cost2go_1 T_1 soc_1
if ~isfield(param,'online') || param.online == 0
    Dsoc = (Dsoc_tract(i):pas_soc(i):Dsoc_recup(i))';
elseif param.online == 1
    % on ne va pas forcement faire un bilan batterie nul : on lache les limites
    soc_inf(i) = -inf;
    soc_sup(i) = inf;
    if zev(i) == 1 && wroue(i)*croue(i) < 12000
        % Mode electrique  
        Dsoc=[];
    else
        % Mode hybride
        zev(i) = 0; % On interdit le mode electrique
        Dsoc = Dsoc_recup(i); % on met la Pmax
        Dsoc_tract(i) = Dsoc; % On limite l'intervalle de Dsoc=Dsoc_tract(i):pas_soc(i):Dsoc_recup(i) a un élément (cf extraction)
        Dsoc_recup(i) = Dsoc; % id
    end
elseif param.online == 2
    % on ne va pas forcement faire un bilan batterie nul : on lache les limites
    soc_inf(i) = -inf;
    soc_sup(i) = inf;
    [pacm1] = online_strategy_HP_BV_2EMB_3U_TWC_NN(VD, param, wroue(i),croue(i),soc_0,wacm1(i),zev(i),i);
    if isempty(pacm1)
        % Mode electrique  
        Dsoc=[];
    else
        % Mode hybride
        zev(i) = 0; % On interdit le mode electrique
        % Calculer le Dsoc. Verifier que pacm1 est possible/saturer sinon
        [ERR,qacm1] = calc_pertes_acm_fw(ERR,VD.ACM1,pacm1,wacm1(i),dwacm1(i),1);
        pbat = pacm1 + qacm1 + Pacc(i);
        [ERR,~,~,soc]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat,100-VD.INIT.Dod0,0);
        Dsoc = soc - (100-VD.INIT.Dod0);
        Dsoc = max(Dsoc, Dsoc_tract(i));
        Dsoc = min(Dsoc, Dsoc_recup(i));
        Dsoc_tract(i) = Dsoc; % On limite l'intervalle de Dsoc=Dsoc_tract(i):pas_soc(i):Dsoc_recup(i) a un élément (cf extraction)
        Dsoc_recup(i) = Dsoc; % id
        
    end
elseif param.online == 3
    % on ne va pas forcement faire un bilan batterie nul : on lache les limites
    soc_inf(i) = -inf;
    soc_sup(i) = inf;
    [pmt] = online_strategy_HP_BV_2EMB_3U_TWC_NN_pmt(VD, param, wroue(i),croue(i),soc_0,T_0,zev(i),i);
    if pmt == 0
        % Mode electrique  
        Dsoc=[];
    else
        % Mode hybride
        zev(i) = 0; % On interdit le mode electrique
        Cmt = pmt./wmt(i);
        cprim1_cpl = Cmt - VD.MOTH.J_mt.*dwmt(i);
        cprim2_cpl =(csec_cpl(i)-cprim1_cpl)/VD.ADCPL.kred;
        if cprim2_cpl>=0
            cprim2_cpl = cprim2_cpl / VD.ADCPL.rend;
        else
             cprim2_cpl = cprim2_cpl * VD.ADCPL.rend;
        end
        cacm1 = cprim2_cpl + VD.ACM1.J_mg*dwacm1(i);
        pacm1 = cacm1 .* wacm1(i);
        % Calculer le Dsoc. Verifier que pacm1 est possible/saturer sinon
        [ERR,qacm1] = calc_pertes_acm_fw(ERR,VD.ACM1,pacm1,wacm1(i),dwacm1(i),1);
        pbat = pacm1 + qacm1 + Pacc(i);
        [ERR,~,~,soc]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat,100-VD.INIT.Dod0,0);
        Dsoc = soc - (100-VD.INIT.Dod0);
        Dsoc = max(Dsoc, Dsoc_tract(i));
        Dsoc = min(Dsoc, Dsoc_recup(i));
        Dsoc_tract(i) = Dsoc; % On limite l'intervalle de Dsoc=Dsoc_tract(i):pas_soc(i):Dsoc_recup(i) a un élément (cf extraction)
        Dsoc_recup(i) = Dsoc; % id

        % Choix phi et SA - Erase phi and delta_AA
        [phi] = online_strategy_HP_BV_2EMB_3U_TWC_NN_phi(VD, param, wroue(i),croue(i),soc_0,T_0);
        [delta_AA] = online_strategy_HP_BV_2EMB_3U_TWC_NN_SA(VD, param, wroue(i),croue(i),soc_0,T_0);
        commande_2U0 = [phi; delta_AA];

    end
        
end
%Dsoc = linspace(Dsoc_tract(i),Dsoc_recup(i),100)';
%pas_soc(i) = Dsoc(2)-Dsoc(1);
%Dsoc = (Dsoc_min(i):pas_soc(i):Dsoc_max(i))';
l_commande_2U0 = length(commande_2U0(2,:));
commande_soc = repmat(Dsoc,l_commande_2U0,1);
commande_soc = commande_soc(:)';
commande_2U = repmat(commande_2U0,1,length(Dsoc));
commande_3U = [commande_2U;commande_soc];

Phi = commande_3U(1,:);
delta_AA = commande_3U(2,:);
Dsoc = commande_3U(3,:);

if length(Phi)> 65535
    disp('Cast de la var. : Phi en uint32 pas suffisant')
end

szU = size(Phi);
szX = size(T_0);

lenU(i) = length(Phi);
lenX(i) = length(T_0);

indice_commande = uint32(1):uint32(szU(2));

% le calcul du point de fonctionnement des moteurs depend uniquement
% de la valeur de la commande (hyp : param batterie independants du soc)
% il y a autant d'elements aux resultats que d'arcs accessibles
% calcul de Pbat en fonction du dsoc

Ibat = -Dsoc/100*(VD.BATT.Nbranchepar*VD.BATT.Cahbat_nom*3600)/param.pas_temps;
[ERR,Ubat]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,Ibat,100-VD.INIT.Dod0,0);
Pbat = Ubat.*Ibat;

% Puissance electrique des machines electriques
pacm1_elec = Pbat-Pacc(i);

% calcul des pertes : carto inverse de la machine electrique
Qacm1 = interp2(VD.ACM1.Regmot_pert,VD.ACM1.Pelec_pert,VD.ACM1.Pertes_pelec,repmat(wacm1(i),szU),pacm1_elec,...
    'linear',max(max(VD.ACM1.Pertes_pelec))); %dernier argument : valeur constante pour tous les points hors du domaine

% couples correspondants pour la ME et le MTH
cacm1 = (pacm1_elec/VD.VEHI.nbacm1-Qacm1)/wacm1(i);
cprim2_cpl = cacm1 - VD.ACM1.J_mg*dwacm1(i);

% Les variables sur le secondaire du coupleur sont indices avec le cycle de conduite
[ERR,cprim1_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl(i),wsec_cpl(i),dwsec_cpl(i),cprim2_cpl,0,1);

Cmt = cprim1_cpl + VD.MOTH.J_mt.*dwmt(i);

N = wmt(i)*30/pi;
if isfield(param,'phi_lut') && param.phi_lut == 1
    Phi = interp2(VD.ECU.Speed_2dPhi, VD.ECU.Torque_2dPhi', VD.ECU.Phi_2d', wmt(i), ...
        max(min(Cmt,max(VD.ECU.Torque_2dPhi)),min(VD.ECU.Torque_2dPhi)))';
    delta_AA = zeros(size(Phi));
end

[dcarb, Padm] = calc_mt_3U_reverse(Cmt,N,Phi,delta_AA,VD);

if ~zev(i) && min(Padm) >interp1(VD.MOTH.p_intake_wmt,VD.MOTH.p_intake,wmt(i),'linear','extrap') 
    % Intake pressure must not exceed max turbo pressure
    % During deceleration, zev mode is prefered (supposed optimale)
     fprintf(1,'%s %f %s %d\n','Pression admission insuffisante :',min(Padm),' a l indice :',i);
     fprintf(1,'%s %f\n','Pression maximum admission  :',interp1(VD.MOTH.p_intake_wmt,VD.MOTH.p_intake,wmt(i),'linear','extrap'));
     fprintf(1,'%s %f %s\n','vitesse vehicule :',VD.CYCL.vitesse(i),' m/s');
     fprintf(1,'%s %f %s\n','vitesse moteur thermique :',wmt(i)*30/pi,' rpm');
      fprintf(1,'%s %f %s %f\n','couple moteur thermique :',max(Cmt),' Nm',size(Cmt));
      fprintf(1,'%s %f %s %f\n','couple moteur electrique :',max(cacm1),' Nm',size(cacm1));
    [~,in] = min(Padm);
     Padm(in)=interp1(VD.MOTH.p_intake_wmt,VD.MOTH.p_intake,wmt(i),'linear','extrap');
     Phi(in)=1.0;
     Padm(~in)=NaN;
     dcarb(~in)=NaN;
     
else
    Padm(Padm>interp1(VD.MOTH.p_intake_wmt,VD.MOTH.p_intake,wmt(i),'linear','extrap')) = NaN; % assignes NaN to values where Padm > 1bar
    dcarb(isnan(Padm)) = NaN; % if you don't calculate exhaust
end

% Pollu et T dependent de la commande et de l'etat initial de T
% Size [szX, szU]
if param.alpha==0 && param.beta==0 && param.gamma==0
    % construction du graphe conso seule, pas besoin de pollu, la
    % taille du vecteur temperature doit etre coherente, mais les valeurs
    % n'importent pas
    T_1 = repmat(T_0,szU);
    nd_CO = zeros(size(T_1));
    nd_HC = zeros(size(T_1));
    nd_NOx = zeros(size(T_1));
else
    [nd_CO, nd_HC, nd_NOx, T_1] = calc_exhaust_3U(T_0,Phi,delta_AA,Padm,dcarb,wmt(i),szU,szX,VD,param.pas_temps,param,i,0);
    % T_1 = round(T_1/param.pas_T)*param.pas_T;
end

if zev(i)
    % traitement du mode tout elec
    % rajoute un dsoc supplementaire sans dcarb
    % Il faut rajouter une "colonne d'element"
    % (szU est incremente car une nouvelle commande est accessible)
    [nd_CO_elec, nd_HC_elec, nd_NOx_elec, T_1_elec] = calc_exhaust_3U(T_0,[],[],[],[],wmt(i),1,szX,VD,param.pas_temps,param,i,zev(i));
    % T_1_elec = round(T_1_elec/param.pas_T)*param.pas_T;
    Dsoc = [Dsoc_tract(i) Dsoc]; % Dsoc_elec est Dsoc_tract quand le mode electrique est accessible
    dcarb = [0 dcarb];
    T_1 = [T_1_elec T_1];
    nd_CO = [nd_CO_elec nd_CO];
    nd_HC = [nd_HC_elec nd_HC];
    nd_NOx = [nd_NOx_elec nd_NOx];
    clear T_1_elec nd_CO_elec nd_HC_elec nd_NOx_elec
    indice_commande = [uint32(0) indice_commande];
    szU = size(Dsoc);
end
clear T_0

if isfield(param,'optiRam') && param.optiRam==1
    cost2go_1 = repmat(cost2go_0,szU) + ...
        repmat(dcarb,szX)/dcarb_norm + param.alpha*nd_CO/CO_norm + param.beta*nd_HC/HC_norm + param.gamma*nd_NOx/NOx_norm;
    if param.MemLog
        log4vehlib('memory',{['Loop : ',num2str(i)]});
    end
    clear nd_CO nd_HC nd_NOx dcarb
    soc_1 = repmat(soc_0,szU) + repmat(Dsoc,szX);
    nd_ind_com = repmat(indice_commande,szX); clear indice_commande;
    nd_ind_0 = repmat((uint32(1:length(soc_0)))',szU); clear ind_0;
    
    cost2go_1 = cost2go_1(:);
    soc_1 = soc_1(:);
    nd_ind_com = nd_ind_com(:);
    nd_ind_0 = nd_ind_0(:);
    T_1 = T_1(:);
    %soc_1>=soc_inf(i)-0.1 & soc_1<=soc_sup(i)+0.1 soc_1>=param.soc_min & soc_1<=param.soc_max
    in_bounds = logical(soc_1>=soc_inf(i)-0.1 & soc_1<=soc_sup(i)+0.1  ...
        & ~isnan(cost2go_1) ...
        & ~isnan(T_1) & ~isnan(soc_1));
    cost2go_1 = cost2go_1(in_bounds);
    soc_1 = soc_1(in_bounds);
    nd_ind_com = nd_ind_com(in_bounds);
    nd_ind_0 = nd_ind_0(in_bounds);
    T_1 = T_1(in_bounds);
    clear in_bounds;
    
else
    
    nd_CO = nd_CO(:);
    nd_HC = nd_HC(:);
    nd_NOx = nd_NOx(:);
    T_1 = T_1(:);

       
    % depend de l'etat initial
    ind_0 = uint32(1:length(soc_0))';
    nd_ind_0 = repmat(ind_0,szU); clear ind_0;
    nd_ind_0 = nd_ind_0(:);
    nd_soc_0 = repmat(soc_0,szU);
    nd_soc_0 = nd_soc_0(:);
    nd_cost2go_0 = repmat(cost2go_0,szU);
    nd_cost2go_0 = nd_cost2go_0(:);
    
    % depend de la commande
    nd_ind_com = repmat(indice_commande,szX);
    nd_ind_com = nd_ind_com(:);
    nd_Dsoc = repmat(Dsoc,szX);
    nd_Dsoc = nd_Dsoc(:); clear Dsoc;
    nd_dcarb = repmat(dcarb,szX);
    nd_dcarb = nd_dcarb(:); clear dcarb;
    
    soc_1 = nd_soc_0 + nd_Dsoc; clear nd_soc_0 nd_Dsoc;
    
    % verification des contraintes : soc entre le min et le max
    % il peut y avoir des nan si certaines commandes sont en dehors
    % des domaines de fonctionnement moteur machine ou batterie
    in_bounds = logical(soc_1>=soc_inf(i)-0.1 & soc_1<=soc_sup(i)+0.1  ...
        & ~isnan(nd_dcarb) ...
        & ~isnan(T_1) & ~isnan(soc_1) ...
        & ~isnan(nd_CO) & ~isnan(nd_HC) & ~isnan(nd_NOx));
    
    % etats necessaire pour le calcul de l'identifiant
    T_1 = T_1(in_bounds);
    soc_1 = soc_1(in_bounds);
    % quantites sauvegardees (eval) pour le post-traitement
    nd_ind_0 = nd_ind_0(in_bounds);
    nd_ind_com = nd_ind_com(in_bounds);
    % expression des performances necessaire pour le calcul du cost to go
    nd_cost2go_0 = nd_cost2go_0(in_bounds);
    nd_dcarb = nd_dcarb(in_bounds);
    nd_CO = nd_CO(in_bounds);
    nd_HC = nd_HC(in_bounds);
    nd_NOx = nd_NOx(in_bounds);
    clear in_bounds
    
    nd_cout_arc = nd_dcarb/dcarb_norm + ...
        param.alpha*nd_CO/CO_norm + ...
        param.beta*nd_HC/HC_norm + ...
        param.gamma*nd_NOx/NOx_norm;
    if param.MemLog
        log4vehlib('memory',{['Loop : ',num2str(i)]});
    end
    clear nd_dcarb nd_CO nd_HC nd_NOx;
    cost2go_1 = nd_cost2go_0 + nd_cout_arc;
    clear nd_cost2go_0 nd_cout_arc

end

if isfield(param,'versionC') && param.versionC == 1
    % Creer un identifiant unique pour les elements de meme soc et de meme temperature
    if ~param.pas_T_var
        identifiant = uint32(round(soc_1/pas_soc(i) + 1)*nb_pts_T+round(T_1/param.pas_T+1));
    else
        I = cnearestindex(T_1,Tgrid);
        identifiant = uint32(round((soc_1/pas_soc(i) + 1)*nb_pts_T))+I;
    end
    % on ordonne sur deux colonnes : identifiant puis cout
    % de facon a avoir en premier les couts min pour chaque id
    [id_dans_ordre,ordre_cout] = csortrows(identifiant, cost2go_1); clear identifiant;
    % on determine l'indice du premier element avec unique :
    % indice du cout min dans le tableau ordonne
    [~,i_min_dans_ordre] = unique(id_dans_ordre); clear id_dans_ordre;
    % on cherche l'indice du cout min dans le tableau original non ordonne
    i_min = ordre_cout(i_min_dans_ordre); clear i_min_dans_ordre ordre_cout;
else
    % Creer un identifiant unique pour les elements de meme soc et de meme temperature 
    if ~param.pas_T_var
        identifiant = round(soc_1/pas_soc(i) + 1)*nb_pts_T+round(T_1/param.pas_T+1);
    elseif isfield(param,'versionC') && param.versionC == 2
        I = cnearestindex(T_1,Tgrid);
        identifiant = uint32(round((soc_1/pas_soc(i) + 1)*nb_pts_T))+I;
    else
        TTgrid = repmat(Tgrid',length(T_1),1);
        TT_1 = repmat(T_1,1,length(Tgrid));

        [~,I] = min(abs(TTgrid-TT_1),[],2);
        clear TT_1 TTgrid
        identifiant = round((soc_1/pas_soc(i) + 1)*nb_pts_T+I);
    end
    % on ordonne sur deux colonnes : identifiant puis cout
    % de facon a avoir en premier les couts min pour chaque id
    if isfield(param,'parallel') && param.parallel == 1
        [id_dans_ordre, ordre_cout] = fetchOutputs(parfeval(@sortrows, 2, [identifiant,cost2go_1])); clear identifiant;
    else
        [id_dans_ordre,ordre_cout] = sortrows([identifiant,cost2go_1]); clear identifiant;
    end
    % on determine l'indice du premier element avec unique :
    % indice du cout min dans le tableau ordonne
    [~,i_min_dans_ordre] = unique(id_dans_ordre(:,1)); clear id_dans_ordre;
    % on cherche l'indice du cout min dans le tableau original non ordonne
    i_min = ordre_cout(i_min_dans_ordre); clear i_min_dans_ordre ordre_cout;
end

T_1 = T_1(i_min);
soc_1 = soc_1(i_min);
cost2go_1 = cost2go_1(i_min);

% sauvegardes d'etat du code pour pouvoir reprendre si erreur
if isfield(param,'debug') && param.debug
    if mod(i,20)==0
        name_boucle = ['loop_' erase(name_source,'log_') num2str(i_fichier) '_A'];
        save(name_boucle,'i','soc_1','T_1','cost2go_1','i_fichier','list_name');
    elseif mod(i,20)==10
        name_boucle = ['loop_' erase(name_source,'log_') num2str(i_fichier) '_B'];
        save(name_boucle,'i','soc_1','T_1','cost2go_1','i_fichier','list_name');
    end
end

% enregistrement progressif du chemin optimal dans un fichier de logs
eval(['ind_precedent_' num2str(i) ' = nd_ind_0(i_min);']);
eval(['commande_opti_' num2str(i) ' = nd_ind_com(i_min);']);
eval(['save(''' name ''', ''ind_precedent_' num2str(i) ''', ''commande_opti_' num2str(i) ''', ''-append'');']);
eval(['clearvars commande_opti_' num2str(i) ' ind_precedent_' num2str(i)])

clear nd_ind_0 i_min nd_ind_com

end

if isfield(param,'parallel') && param.parallel == 1
    % delete thread-based pool
    delete(pool );
end

%_________________________________________________________________________%
%_________________________________________________________________________%
clearvars -except wacm1  dwacm1 wacm1_elec dwacm1_elec wmt dwmt csec_cpl wsec_cpl dwsec_cpl Ibat_min ...
    VD soc_1 cost2go_1 pas_temps commande_opti ind_precedent ...
    cacm1_elec param tps_calcul name i_fichier name_source list_name ...
    Dsoc_tract Dsoc_recup pas_soc soc_inf soc_sup lenX lenU ERR pool

tps_calcul = toc;
if param.verbose
    fprintf('temps de calcul : %.0f min\n',tps_calcul/60)
end

save(name,'VD','param','cost2go_1','soc_1',...
    'wacm1','dwacm1','wacm1_elec','dwacm1_elec','cacm1_elec','wmt','dwmt','csec_cpl','wsec_cpl','dwsec_cpl', ...
    'Dsoc_tract','Dsoc_recup','pas_soc','soc_inf','soc_sup',...
    'tps_calcul','lenX','lenU','-append')

if param.MemLog
    log4vehlib('close');
end



% Argument de retour de la fonction doit être le premier fichier XXX_1 
% (qui contient list_name)
name = list_name{1};


end
