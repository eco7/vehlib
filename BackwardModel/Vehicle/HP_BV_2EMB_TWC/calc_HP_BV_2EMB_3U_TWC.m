function [ERR, VD, res] = calc_HP_BV_2EMB_3U_TWC(ERR, vehlib, param, VD)

name = graphe_HP_BV_2EMB_3U_TWC(ERR, vehlib, param, VD);

[res,VD,param] = extraction_chemin_optimal_3U_TWC('./', name);

VD.param = param;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

