function pacm1 = online_strategy_HP_BV_2EMB_3U_TWC_NN(VD, param, wroue,croue,soc_fwd,wacm1,zev,ind)
% online strategies for HP_BV_2EMB_3U_TWC model
% based on expert rules, or neural network, or whatever you may think of
%
% Inputs:
% wroue : wheel speed in rd/s
% croue : wheel torque in Nm
% soc : current value of soc
% zev : a boolean indicating if all electric mode is available (1), 0 otherwise
% Outputs:
% pacm1 : if empty, that means that the all electric mode is selected
% Value of mechanical acm1 power otherwise
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mean_croue = param.NN.Settings.Stats(1,1);
var_croue = param.NN.Settings.Stats(1,2);
mean_wroue = param.NN.Settings.Stats(2,1);
var_wroue = param.NN.Settings.Stats(2,2);
mean_dsoc = param.NN.Settings.Stats(3,1); 
var_dsoc = param.NN.Settings.Stats(3,2);
interval = unique(diff(param.NN.Settings.y_classes));

if param.online == 2
    % Neural Net
    rapp = VD.CYCL.rappvit(ind);
    if zev == 1 && rapp == 0
        % Electric mode
        pacm1=[];
    else
        % Min and max torque from EM
        Cacm1_min_elec = interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
        Cacm1_max_elec = interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
        if croue<0
            % Wheel Torque limitations due to EM size
            croue_recup = VD.ADCPL.kred*VD.RED.kred*VD.BV.k(rapp)*Cacm1_min_elec/(VD.ADCPL.rend*VD.BV.Ro(rapp)*VD.RED.rend); % recalcul de croue
            croue = max(croue,croue_recup);
        end
        classe = classify(param.NN.nrj_mngt_neuralnet,[(croue-mean_croue)/sqrt(var_croue),(wroue-mean_wroue)/sqrt(var_wroue),(((100-VD.INIT.Dod0+param.dsoc)-soc_fwd)-mean_dsoc)/(sqrt(var_dsoc))]);
        pacm1_nn = param.NN.Settings.y_classes(classe);

        % ME Power limitation in traction and regen modes
        pacm1_nn = min(max(pacm1_nn,Cacm1_min_elec*wacm1),Cacm1_max_elec*wacm1);
        if pacm1_nn > 0
            proue_nn = pacm1_nn*VD.BV.Ro(max(1,rapp))*VD.RED.rend*VD.ADCPL.rend; % recalcul de la proue
            interval_proue = interval*VD.BV.Ro(max(1,rapp))*(VD.RED.rend*VD.ADCPL.rend); % et de l intervalle de classe
        else
            proue_nn = pacm1_nn/(VD.BV.Ro(max(1,rapp))*(VD.RED.rend*VD.ADCPL.rend)); % recalcul de la proue
            interval_proue = interval/(VD.BV.Ro(max(1,rapp))*(VD.RED.rend*VD.ADCPL.rend)); % et de l intervalle de classe
        end
        if zev == 1 && (abs(proue_nn-wroue*croue) < interval_proue || double(classe) == 1)
            % Electric Mode
            pacm1=[];
        else
            % Hybrid mode otherwise
            pacm1 = pacm1_nn;
        end
    end
end
