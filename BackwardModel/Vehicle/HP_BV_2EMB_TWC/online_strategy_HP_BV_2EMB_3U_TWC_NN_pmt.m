function pmt_nn = online_strategy_HP_BV_2EMB_3U_TWC_NN_pmt(VD, param, wroue,croue,soc_fwd,tcata,zev,ind)
% online strategies for HP_BV_2EMB_3U_TWC model
% based on expert rules, or neural network, or whatever you may think of
%
% Inputs:
% wroue : wheel speed in rd/s
% croue : wheel torque in Nm
% soc : current value of soc
% zev : a boolean indicating if all electric mode is available (1), 0 otherwise
% Outputs:
% pacm1 : if empty, that means that the all electric mode is selected
% Value of mechanical acm1 power otherwise
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mean_tcata = param.NN.Settings_pmt.Stats(1,1);
var_tcata = param.NN.Settings_pmt.Stats(1,2);
mean_croue = param.NN.Settings_pmt.Stats(2,1);
var_croue = param.NN.Settings_pmt.Stats(2,2);
mean_wroue = param.NN.Settings_pmt.Stats(3,1);
var_wroue = param.NN.Settings_pmt.Stats(3,2);
mean_dsoc = param.NN.Settings_pmt.Stats(4,1);
var_dsoc = param.NN.Settings_pmt.Stats(4,2);

% Neural Net
rapp = VD.CYCL.rappvit(ind);
if zev == 1 && rapp == 0
    % Electric mode
    pmt_nn=0;
else
    classe = classify(param.NN.nrj_mngt_neuralnet_pmt, ...
        [(tcata-mean_tcata)/sqrt(var_tcata), ...
        (croue-mean_croue)/sqrt(var_croue), ...
        (wroue-mean_wroue)/sqrt(var_wroue), ...
        (((100-VD.INIT.Dod0+param.dsoc)-soc_fwd)-mean_dsoc)/(sqrt(var_dsoc))]);
    pmt_nn = param.NN.Settings_pmt.y_classes(classe);
    fprintf(1,'pmt %d %.0f\n',classe,pmt_nn);
end
end
