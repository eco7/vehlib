% function [ERR, VD, ResXml]=calc_HP_BV_2EMB_TWC(ERR,vehlib,param,VD)
%
% Copyright IFSTTAR LTE 1999-2011
%
% Objet:
% Calcul d'un vehicule electrique muni d'une association
% batterie/super-condensateur dans VEHLIB en mode backward
%
% Arguments appel :
% Arguments de sortie :
%
% EV 26/10/09
% En recup si le courant batt + ucap ne permet pas de recuperer toute
% l'energie que se passe t'il ??
% A priori OK quand meme car meme si qacm1 est calcule pour un couple
% moteur sans frein meca, de toute facon si on vient limiter a cause des
% courants on recupere quand meme tout ce que l'on peut. Seul cacm1, qacm1
% et donc l'eventuel frein meca sont faux

function [ERR, VD, ResXml]=calc_HP_BV_2EMB_TWC(ERR,vehlib,param,VD)
format long

% Calcul cinematiques complementaires
% Avec re-echantillonnage eventuel
if ~isfield(param,'pas_temps') | param.pas_temps==0
    param.pas_temps=1; % si param_pas temps n'existe pas ou =0 on reechantillone ? 1s
end
[ERR,VD,dist]=calc_cinemat(ERR,VD,param);

if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

if (isfield(param,'fit_pertes_acm') & param.fit_pertes_acm==1)
    [VD.ACM1] = fitcarto(VD.ACM1,0);
elseif ~isfield(param,'fit_pertes_acm')
    param.fit_pertes_acm=0;
end

if (isfield(param,'fit_pertes_moth') & param.fit_pertes_moth==1)
    [VD.MOTH] = fitcarto(VD.MOTH,0);
elseif ~isfield(param,'fit_pertes_moth')
    param.fit_pertes_moth=0;
end

% Initialisation
soc=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));
E=zeros(size(VD.CYCL.temps));
R=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
pbat=zeros(size(VD.CYCL.temps));

% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,penteFpk,fres,faero,froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Dans certain cycle CIN_ARTURB_BV_classe3 par exemple, il peut arriver
%que croue>0 alors que on est au point mort, impossible avec des archi avec
%BV on met alors le couple a zero

if VD.CYCL.ntypcin==3
    ind_croue_pm=find(croue>0 & VD.CYCL.rappvit==0);
    if ~isempty(ind_croue_pm)
        chaine=strcat('in some point, a positive wheel torque exist with neutral gear box position \n corresponding time :',num2str(ind_croue_pm),'\n wheel torque fixe to zero in these cases');
        warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine);
        croue(ind_croue_pm)=0;
    end
end

if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Connexion
csec_red=croue;
wsec_red=wroue;
dwsec_red=dwroue;

% Calcul des conditions en amont du reducteur
[ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);
if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Connexion
csec_bv=cprim_red;
wsec_bv=wprim_red;
dwsec_bv=dwprim_red;

% Calcul des conditions en amont de la boite de vitesse
% NB si on est en rapport libre ou optimaux on as des matrices
% avec en nb de ligne le nb de rapport de boite plus pm
[ERR,cprim_bv,wprim_bv,dwprim_bv]=calc_bv(VD,csec_bv,wsec_bv,dwsec_bv,0);
if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Connexion
csec_emb1=cprim_bv;
wsec_emb1=wprim_bv;
dwsec_emb1=dwprim_bv;


%% Prise en compte du frein meca quand Croue <0, on considere que dans ce cas cprim1_cpl =0

[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,0);

% connexion
wsec_cpl=wprim_emb1;
dwsec_cpl=dwprim_emb1;
csec_cpl=cprim_emb1;
[ERR,cprim2_cpl,wprim2_cpl,dwprim2_cpl]=calc_red(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0);

% Connexion
wacm1=wprim2_cpl;
dwacm1=dwprim2_cpl;
cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;

% On vient limiter cacm1 a sa valeur max
cacm1=max(cacm1,interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1));

if isfield(VD.ECU,'Vit_Cmin')
    cacm1=max(cacm1,interp1(VD.ECU.Vit_Cmin,VD.ECU.Cpl_min,wacm1));
end

% Recalcul de la partie frein meca au niveau de la roue
cprim2_cpl=(cacm1-VD.ACM1.J_mg.*dwacm1)*VD.VEHI.nbacm1;

[ERR,~,~,~,~,csec_cpl]=calc_adcpl_fw(ERR,VD.ADCPL,0,0,cprim2_cpl,0,wsec_cpl,dwsec_cpl);

% connexion
wsec_emb1=wsec_cpl;
dwsec_emb1=dwsec_cpl;
csec_emb1=csec_cpl;

wprim_bv=wsec_emb1;
dwprim_bv=dwsec_emb1;
cprim_bv=csec_emb1;

[ERR,csec,wsec,dwsec]=calc_bv_fw(ERR,VD,cprim_bv,wprim_bv,dwprim_bv,0,0);

% Connexion
wprim_red=wsec_bv;
dwprim_red=dwsec_bv;
cprim_red=csec_bv;

[ERR,csec_red,wsec_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
cfrein_meca=csec_red-croue;

%% strategie selon les cas (doivent renvoyer des vecteurs cprim2_cpl et
%% elhyb de taille CYCL.temps)
if strcmp(lower(param.optim),'ligne')
    %[ERR,cprim2_cpl,elhyb]=calc_ligne_HP_BV_2EMB(ERR,param,CYCL,ADCPL,BV,RED,BATT,VEHI,ACC,ACM1,MOTH,CALC,INIT,csec_emb1,wsec_emb1,dwsec_emb1);
    'Strategie inexistante'
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
elseif strcmp(lower(param.optim),'lagrange_TWC')
    [ERR,VD,cprim2_cpl,elhyb,lambda1,lambda2,indice_rap_opt,Tcat,Texh,co_eff,hc_eff,nox_eff,co,hc,nox]=calc_lagrange_HP_BV_2EMB_TWC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
end

if ~isempty(ERR)
    %ERR1=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
    %ERR=addCause(ERR1,ERR);
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(1);
    ResXml=[];
    return
end

if sum(size(csec_emb1)==[1+size(VD.BV.rapport,2) size(VD.CYCL.vitesse,2)])==2 % Cas ou on optimise les rapports de boite
    csec_emb1=csec_emb1(sub2ind(size(csec_emb1),indice_rap_opt,1:length(indice_rap_opt)));
    wsec_emb1=wsec_emb1(sub2ind(size(wsec_emb1),indice_rap_opt,1:length(indice_rap_opt)));
    dwsec_emb1=dwsec_emb1(sub2ind(size(dwsec_emb1),indice_rap_opt,1:length(indice_rap_opt)));
    
    % connexion
    wprim_bv=wsec_emb1;
    dwprim_bv=dwsec_emb1;
    cprim_bv=csec_emb1;
    
    rap_opt=indice_rap_opt-1;
end


if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2,NaN);
    return;
end

% Calcul conditions en amont de l'embrayage (a refaire une fois fait le
% choix de elhyb)

[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,elhyb);
if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2,NaN);
    return;
end

% Calcul conditions en amont du coupleur
% connexion

wsec_cpl=wprim_emb1;
dwsec_cpl=dwprim_emb1;
csec_cpl=cprim_emb1;
[ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,cprim2_cpl,0);

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);
if ~isempty(ERR)
ResXml=struct([]); %     [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end

if strcmp(lower(param.optim),'ligne')  | strcmp(lower(param.optim),'lagrange')
    
    % Connexion
    wacm1=wprim2_cpl;
    dwacm1=dwprim2_cpl;
    cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;
    
    wsec_emb2=wprim1_cpl;
    dwsec_emb2=dwprim1_cpl;
    csec_emb2=cprim1_cpl;
    
    %% Pb revoir calc_emb cas passage elhyb 0 a 1, valeur cmt, wmt si elhyb ==0
    %% et
    [ERR,cprim_emb2,wprim_emb2,dwprim_emb2,etat_emb]=calc_emb2(VD,csec_emb2,wsec_emb2,dwsec_emb2,elhyb);
    
    wmt=wprim_emb2;
    dwmt=dwprim_emb2;
    cmt=cprim_emb2+VD.MOTH.J_mt.*dwmt;
    
    % Calcul des conditions de fonctionnement du moteur thermique
    [ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,elhyb,0);
    if ~isempty(ERR)
        disp(ERR.message);
        choix=1; % pour saturer les couples et faire les calculs tout de meme
        if choix==0
            ResXml=struct([]); %             [conso100,co2_gkm,co2_inst]=backward_erreur(length(VD.CYCL.temps),2);
            return;
        elseif choix==1
            indice=find(cmt>interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt));
            cmt(indice)=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt(indice));
            [ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt(ERR,VD,cmt,wmt,dwmt,elhyb,0);
        end
    end
    
    % Calcul des conditions en amont de la machine electrique
    [Li,Co]=size(cacm1);
    wacm1=repmat(wacm1,Li,1);
    [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');
    if ~isempty(ERR)
        ResXml=struct([]); %         [ah,soc]=backward_erreur(length(VD.CYCL.temps));
        return;
    end
    
    % Puissance reseau electrique
    pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc;
    % EN tout thermique il faut remettre pres a pacc
    pres(elhyb==2)=pacc(elhyb==2);
    qacm1(elhyb==2)=0;
    
    % calcul batteries
    soc(1)=100-VD.INIT.Dod0;
    ah(1)=0;
    [ERR,ibat(1),ubat(1),~,~,E(1),R(1),RdF(1)]=calc_batt(ERR,VD.BATT,param.pas_temps,pres(1),soc(1),ah(1),pres,0);
    
    for j=2:length(VD.CYCL.temps)
        [ERR,ibat(j),ubat(j),soc(j),ah(j),E(j),R(j),RdF(j)]=calc_batt(ERR,VD.BATT,param.pas_temps,pres(j),soc(j-1),ah(j-1),pres(j),0);
    end
end
pbat=ibat.*ubat;

%% Bilan de puissance et d'energie

Pjbat=R.*ibat.*ibat;

p0bat=E.*ibat;
Pertes_red=abs(cprim_red.*wprim_red-csec_red.*wsec_red);
Pertes_bv=abs(cprim_bv.*wprim_bv-csec_bv.*wsec_bv);
Pertes_cpl=abs(cprim1_cpl.*wprim1_cpl+cprim2_cpl.*wprim2_cpl-csec_cpl.*wsec_cpl);
Pertes_emb1=abs(cprim_emb1.*wprim_emb1-csec_emb1.*wsec_emb1);

Proue=croue.*wroue;

% Pertes motelec en mode tout thermiques
Pf_motelec=0*ones(1,length(VD.CYCL.temps));
Pf_motelec(elhyb==2)=-cacm1(elhyb==2).*wacm1(elhyb==2);

% Recalcul du frein meca supplementaires due aux limitations batterie ou motelec :
%cfrein_meca_pelec_mot  represente le frein
% meca a mettre si c'est la batterie ou la puissance
% elec du motelec qui est limitante et pas le cacm1_min.
% Il est calcule niveau motelec. a recalculer
% niveau roue et ajouter a cfrein_meca
% NB on ramene le couple sans les rendements sinon il faut aussi
% recalculer les pertes dans chaque organes
if exist('cfrein_meca_pelec_mot')
    BV.k1=[0 BV.k];
    cfrein_meca_pelec_mot_roue=cfrein_meca_pelec_mot*ADCPL.kred_cpl.*BV.k1(VD.CYCL.rappvit+1)*RED.kred;
    cfrein_meca=cfrein_meca+cfrein_meca_pelec_mot_roue;
end

if param.verbose>=1
    fprintf(1,'%s %.6f %s\n','Les pertes joules dans les batteries : ',trapz(VD.CYCL.temps,Pjbat)/3600,'Wh');
    Eacm1=VD.VEHI.nbacm1*trapz(VD.CYCL.temps,qacm1)/3600;
    fprintf(1,'%s %.1f %s\n','Les pertes dans la machine electrique acm1 : ',Eacm1,'Wh');
    fprintf(1,'%s %.1f %s\n','L''energie fournie par la batterie (source parfaite) : ',trapz(VD.CYCL.temps,p0bat)/3600,'Wh'); % ne tiens pas compte du RdF donc ne sera pas nul a Dsoc nul
    fprintf(1,'%s %.1f %s\n','L''energie fournie par la batterie (source parfaite pluds RdF) : ',trapz(VD.CYCL.temps,p0bat.*RdF)/3600,'Wh'); % ne tiens pas compte du RdF donc ne sera pas nul a Dsoc nul
    Ebat=-trapz(VD.CYCL.temps,ibat.*ubat)/3600;
    fprintf(1,'%s %.1f %s\n','L''energie fournie par la batterie : ',Ebat,'Wh');
    Ered=trapz(VD.CYCL.temps,Pertes_red)/3600;
    fprintf(1,'%s %.1f %s\n','Les pertes dans le reducteur : ',Ered,'Wh');
    Ebv=trapz(VD.CYCL.temps,Pertes_bv)/3600;
    fprintf(1,'%s %.1f %s\n','Les pertes dans la boite de vitesse : ',Ebv,'Wh');
    Eemb1=trapz(VD.CYCL.temps,Pertes_emb1)/3600;
    fprintf(1,'%s %.1f %s\n','Les pertes dans la boite de vitesse : ',Eemb1,'Wh');
    Ecpl=trapz(VD.CYCL.temps,Pertes_cpl)/3600;
    fprintf(1,'%s %.1f %s\n','Les pertes dans le coupleur: ',Ecpl,'Wh');
    Eacc=trapz(VD.CYCL.temps,pacc)/3600;
    fprintf(1,'%s %.1f %s\n','L''energie consommee par les accessoires : ',Eacc,'Wh');
    Efrein=-trapz(VD.CYCL.temps,cfrein_meca.*wroue)/3600;
    fprintf(1,'%s %.1f %s\n','L''energie dissipee dans les freins :',Efrein,'Wh');

end

bilanP=cmt.*wmt+p0bat-Pjbat-VD.VEHI.nbacm1*qacm1-Pf_motelec-pacc-Pertes_red-Pertes_cpl-Pertes_emb1-Proue-Pertes_bv-VD.ACM1.J_mg.*dwacm1.*wacm1-VD.MOTH.J_mt.*dwmt.*wmt-cfrein_meca.*wroue;

%bilan local batterie motelec
% cas ou c'est Ibat qui limite en recup
% on boucle bilanP comme des sagoins par bilanP_local
% il faudrait refaire toute la demarche en forward
%bilanP_loc=ibat.*ubat-pacc-cacm1.*wacm1-qacm1-Pf_motelec;

%indice=find(bilanP>1e-5 & ibat==BATT.Ibat_min & cmt==0)
%bilanP(indice)=bilanP(indice)-bilanP_loc(indice);

if param.verbose>=2
    figure(10)
    clf
    if ~strcmp(lower(param.optim),'prog_dyn')
        plot(VD.CYCL.temps,bilanP)
    else %pb du point origine
        plot(VD.CYCL.temps(2:end),bilanP(2:end))
    end
    grid
    legend('bilan de puissance')
end

prouet=wroue.*croue;
prouet(prouet<0)=0;
prouer=wroue.*croue;
prouer(prouer>0)=0;

if param.verbose>=1
    fprintf(1,'%s %.1f %s %s %.1f %s %s %.1f %s\n','L''energie fournie aux roues : ',trapz(VD.CYCL.temps,croue.*wroue)/3600,'Wh',...
        'en traction',trapz(VD.CYCL.temps,prouet)/3600,'Wh','en recup',trapz(VD.CYCL.temps,prouer)/3600,'Wh');
end

%trajectoire optimale
if param.verbose>=2 & ~strcmp(lower(param.optim),'prog_dyn')
    figure
    clf
    hold on
    
    if strcmp(lower(param.optim),'prog_dyn')
        % limites du graphe
        plot(VD.CYCL.temps,soc_min,'b',VD.CYCL.temps,soc_max,'b')
        assignin('base','soc_min',soc_min);
        assignin('base','soc_max',soc_max);
        plot(VD.CYCL.temps,soc)
    else
        plot(VD.CYCL.temps,soc)
        % plot(VD.CYCL.temps,ones(length(VD.CYCL.temps))*SC.minTensionAdmissible*SC.Nblocser,'b')
        
    end
    %plot(VD.CYCL.temps,u0sc,'r')
    grid
    title('trajectoire optimale')
    ylabel('soc en %')
end

if param.verbose>=3
    Eroue=trapz(VD.CYCL.temps,croue.*wroue)/3600;
    E_2emb=[Eroue Eacc Ered Efrein Ebat Eacm1 Ebv Eemb1 Ecpl]/(dist(end)/1000);
    assignin('base','E_2emb',E_2emb);
    figure(100)
    clf
    axes('fontsize',12)
    hold on   
    bar([1 2],[E_2emb' 0*E_2emb']',0.3,'stacked')
    axis([0.5 1.5 0 1.1*sum(E_2emb)])
    legend('wheel energy','auxiliary energy','final gear losses','brake losses','battery losses','em1 losses','gear box losses','clutch losses','coupling device losses')
    ylabel('Wh/km','Fontsize',12)
    title('energy balance','Fontsize',12)
    
    figure(101)
    clf 
    pie3(E_2emb,[0 1 1 1 1 1 1 1 1])
    %legend('wheel energy','auxiliary energy','final gear losses','brake losses','em1 losses','em2 losses','battery losses','planetary gear lossses')
    legend('wheel energy','auxiliary energy','final gear losses','battery losses','brake losses','em1 losses','gear box losses','clutch losses','coupling device losses','Location',[0.15 0.7 0.1 0.1])
    view(90,30)
    colormap hsv
end

% Cycle
tsim=VD.CYCL.temps;
vit=VD.CYCL.vitesse;
distance=cumtrapz(VD.CYCL.temps,VD.CYCL.vitesse);
acc=VD.CYCL.accel;
vit_dem=vit;
pente=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,dist,'linear',0); % reechantillonage pente fonction distance calculee.

% Resultats d'emission
if isfield(VD,'TWC')
    co_gkm=trapz(tsim,co)/(distance(end)/1000);
    hc_gkm=trapz(tsim,hc)/(distance(end)/1000);
    nox_gkm=trapz(tsim,nox)/(distance(end)/1000);
end

if isfield(VD,'TWC') && param.verbose >0
    fprintf(1,'%s %f %s\n','Emission de CO :',co_gkm,'g/km');
    fprintf(1,'%s %f %s\n','Emission de HC :',hc_gkm,'g/km');
    fprintf(1,'%s %f %s\n','Emission de NOx :',nox_gkm,'g/km');
    PE4=1/3*(co_gkm/VD.TWC.CO_EURO4+hc_gkm/VD.TWC.THC_EURO4+nox_gkm/VD.TWC.NOx_EURO4);
    PE5=1/3*(co_gkm/VD.TWC.CO_EURO5+hc_gkm/VD.TWC.THC_EURO5+nox_gkm/VD.TWC.NOx_EURO5);
    fprintf(1,'%s %f\n','Soit une ponderation par rapport a la norme EURO IV : :',PE4);
    fprintf(1,'%s %f\n','Soit une ponderation par rapport a la norme EURO V : :',PE5);

end

[ResXml]=miseEnForme4VEHLIB(vehlib,'caller');

% Calcul de resultats synthetiques
[Res]=resultats(VD,param,ResXml);
conso100=Res.conso100;
cumcarb=Res.cumcarb;
co2_eq=Res.co2_eq;
CO2_gkm=Res.CO2_gkm;
ibat_eff=Res.ibat_eff;
ibat_moy=Res.ibat_moy;

% Ajout des grandeurs synth??tiques dans une nouvelle table de la structure xml
[ResXml]=miseEnForme4GestionEnergie(ResXml);


% trace sollicitation batterie
% Inter=-100:25:200;
% figure;
% classe_energie(VD,VD.CYCL.temps,(ibat),3,min(ibat),max(ibat),10,0,0,1,{'titre'},1,Inter)
% return;

% Cycle
tsim=VD.CYCL.temps;
vit=VD.CYCL.vitesse;
acc=VD.CYCL.accel;
vit_dem=vit;
distance=cumtrapz(VD.CYCL.temps,VD.CYCL.vitesse); % distance calculee a partir de la vitesse VD.CYCL.vitesse
pente=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,distance,'linear',0); % reechantillonage pente fonction distance claculee.

[ResXml]=miseEnForme4VEHLIB(vehlib,'caller');
% Ajout des grandeurs synth??tiques dans une nouvelle table de la structure xml
[ResXml]=miseEnForme4GestionEnergie(ResXml);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,VD,cprim2_cpl,elhyb,lambda1,lambda2,indice_rap_opt,Tcat,Texh,CO_eff,HC_eff,NOx_eff,CO,HC,NOx]=calc_lagrange_HP_BV_2EMB_TWC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
[Li,Co]=size(csec_emb1);
elhyb=ones(1,Co);
lambda1(1)=param.lambda1;
lambda2(1)=param.lambda2;
soc(1)=100-VD.INIT.Dod0;
if isfield(VD.INIT,'Tcat0')
    Tcat(1)=VD.INIT.Tcat0+273;
else
    Tcat(1)=VD.INIT.Tamb+273;
end

CO_eff(1)=0;
HC_eff(1)=0;
NOx_eff(1)=0;
if ~isfield(param,'lambda1_cst')
    param.lambda1_cst='oui';
end
if ~isfield(param,'lambda2_cst')
    param.lambda2_cst='oui';
end
indice_rap_opt=NaN;

for j=1:length(VD.CYCL.temps)
    if j==1
        lambda1_p=lambda1(1);
        lambda2_p=lambda2(1);
        soc_p=soc(1);
        Tcat_p(1)=Tcat(1);
        CO_eff_p(1)=CO_eff(1);
        HC_eff_p(1)=HC_eff(1);
        NOx_eff_p(1)=NOx_eff(1);
    else
        lambda1_p=lambda1(j-1);
        lambda2_p=lambda2(j-1);
        soc_p=soc(j-1);
        Tcat_p=Tcat(j-1);
        CO_eff_p=CO_eff(j-1);
        HC_eff_p=HC_eff(j-1);
        NOx_eff_p= NOx_eff(j-1);
    end
    
    [ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,repmat(elhyb,[Li 1]));
    % connexion
    wsec_cpl=wprim_emb1;
    dwsec_cpl=dwprim_emb1;
    csec_cpl=cprim_emb1;
    
%     if  sum(j==param.tdebug)>=1
%         borne_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,1),wsec_emb1(j)*VD.ADCPL.kred);
%         borne_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,1),wsec_emb1(j)*VD.ADCPL.kred);
%         cprim2_cpl_trace=borne_min:param.pas_cprim2_cpl:borne_max;
%         % Tracee des possibles pour l'instant et la valeur de lambda
%         %[ERR,ham_hyb_trace]=calc_hamilt_hyb_HP_BV_2EMB_TWC(ERR,CYCL,ADCPL,BATT,VEHI,ACC,ACM1,MOTH,TWC,param,cprim2_cpl_trace,csec_cpl(:,j),wsec_cpl(:,j),dwsec_cpl(:,j),lambda_p,j,soc_p,0,param.lambda_cst);
%         [ERR,ham_hyb_trace,Lambda1_hyb_trace,Lambda2_hyb_trace,~,ibat_hyb_trace,soc_hyb_trace,dcarb_hyb_trace,ubat_hyb_trace,Tcat_hyb_trace,Texh_hyb_trace, CO_eff_hyb_trace, HC_eff_hyb_trace, NOx_eff_hyb_trace, CO_hyb_trace, HC_hyb_trace, NOx_hyb_trace,~,dQs_hyb_trace,dQin_hyb_trace,dQgen_hyb_trace]=...
%             calc_hamilt_hyb_HP_BV_2EMB_TWC(ERR,VD,param,cprim2_cpl_trace,csec_cpl(:,j),wsec_cpl(:,j),dwsec_cpl(:,j),lambda1_p,lambda2_p,j,soc_p,Tcat_p,CO_eff_p, HC_eff_p, NOx_eff_p,0);
%         assignin('base','ham_hyb_trace',ham_hyb_trace);
%         assignin('base','cprim2_cpl_trace',cprim2_cpl_trace);
%         assignin('base','ibat_hyb_trace',ibat_hyb_trace);
%         assignin('base','dcarb_hyb_trace',dcarb_hyb_trace);
%         assignin('base','Tcat_hyb_trace',Tcat_hyb_trace);
%         assignin('base','NOx_eff_hyb_trace',NOx_eff_hyb_trace);
%         assignin('base','NOx_hyb_trace',NOx_hyb_trace);
%         assignin('base','ubat_hyb_trace',ubat_hyb_trace);
%         assignin('base','dQs_hyb_trace',dQs_hyb_trace);
%         assignin('base','dQin_hyb_trace',dQin_hyb_trace);
%         assignin('base','dQgen_hyb_trace',dQgen_hyb_trace);
%         %[ERR,ham_therm_trace,bidon,cprim2_therm_trace]=calc_hamilt_therm_HP_BV_2EMB(ERR,CYCL,ADCPL,BATT,VEHI,ACC,ACM1,MOTH,param,csec_cpl(:,j),wsec_cpl(:,j),dwsec_cpl(:,j),lambda_p,j,soc_p,0,param.lambda_cst);
%        
%         if Li>1
%             [min_ham_hyb,J_min_ham]=min(ham_hyb_trace,[],2);
%             [min_ham,I_min_ham]=min(min_ham_hyb);
%             ham_hyb=min_ham_hyb
%         else
%             [min_ham,Imin_ham]=min(ham_hyb_trace);
%             ham_hyb=min_ham
%         end
%        % ham_therm=ham_therm_trace
%     end
    
    borne_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wsec_emb1(j)*VD.ADCPL.kred);
    borne_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wsec_emb1(j)*VD.ADCPL.kred);
    cprim2_cpl_vect=borne_min:param.pas_cprim2_cpl:borne_max;
    
    % Calcul des points possibles de convergence (point des catos mt ou
    % acm1 ou limites mt ou acm).
%     wmt=wsec_cpl(j);  
%     wacm1=wsec_cpl(j)*VD.ADCPL.kred;
%     dwmt=dwsec_cpl(j);
%     dwacm1=dwsec_cpl(j)*VD.ADCPL.kred;
%     
%     cmt_max=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt);
%     cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,end),wacm1);
%     cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,end),wacm1);
% 
%    % point carto mt 
%     cmt_cartomt=[VD.MOTH.Cpl_2dconso(VD.MOTH.Cpl_2dconso<cmt_max)  cmt_max]; 
%     cprim2_cpl_carto_mt= (csec_cpl(j)-(cmt_cartomt-VD.MOTH.J_mt*dwmt))/(VD.ADCPL.kred*VD.ADCPL.rend);
%     
%     %point carto acm1
%     cacm1_carto_acm1=[cacm1_min  VD.ACM1.Cmot_pert(VD.ACM1.Cmot_pert>cacm1_min & VD.ACM1.Cmot_pert<cacm1_max) cacm1_max];
%     cprim2_cpl_carto_acm1=cacm1_carto_acm1-VD.ACM1.J_mg*dwacm1 ;
%     
%     cprim2_cpl_vect=sort([cprim2_cpl_carto_mt cprim2_cpl_carto_acm1]);
    
%     if j==11 | j==600 | j ==900 | j==1000
%         'toto'
%     end
    % Recalcule des point mt correspondant pour suppression des points hors
    % limites
%     cmt_cprim2_cpl_vect=csec_cpl(j)+VD.MOTH.J_mt*dwmt-cprim2_cpl_vect*(VD.ADCPL.kred*VD.ADCPL.rend);
%     
%     cprim2_cpl_vect=cprim2_cpl_vect(cmt_cprim2_cpl_vect>0 & cmt_cprim2_cpl_vect<cmt_max);
    
    [ERR,ham_hyb(j),lambda1_hyb(j),lambda2_hyb(j),cprim2_cpl_hyb(j),ibat_hyb(j),soc_hyb(j),dcarb_hyb(j),ubat_hyb(j),Tcat_hyb(j),Texh_hyb(j), ...
        CO_eff_hyb(j), HC_eff_hyb(j), NOx_eff_hyb(j), CO_hyb(j), HC_hyb(j), NOx_hyb(j),indice_rap_opt_hyb(j),dQs_hyb(j),dQin_hyb(j),dQgen_hyb(j)]=...
        calc_hamilt_hyb_HP_BV_2EMB_TWC(ERR,VD,param,cprim2_cpl_vect,csec_cpl(j),wsec_cpl(j),dwsec_cpl(j),lambda1_p,lambda2_p,j,soc_p,Tcat_p,CO_eff_p, HC_eff_p, NOx_eff_p,1);
    
    if ~isempty(ERR)
        [cprim2_cpl,elhyb,lambda1,lambda2,indice_rap_opt]=backward_erreur(length(VD.CYCL.temps));
        return;
    end
    
    
    if isfield(param,'tout_thermique') & param.tout_thermique==1;
        %[ERR,ham_therm(j),lambda_therm(j),cprim2_cpl_therm(j),ibat_therm(j),soc_therm(j),dcarb_therm(j),ubat_therm(j),indice_rap_opt_therm(j)]=calc_hamilt_therm_HP_BV_2EMB(ERR,CYCL,ADCPL,BATT,VEHI,ACC,ACM1,MOTH,param,csec_cpl(:,j),wsec_cpl(:,j),dwsec_cpl(:,j),lambda_p,j,soc_p,1,param.lambda_cst);
    else
        ham_therm(j)=Inf;
        lambda_therm(j)=NaN;
        cprim2_cpl_therm(j)=NaN;
        ibat_therm(j)=NaN;
        soc_therm(j)=NaN;
        dcarb_therm(j)=NaN;
        ubat_therm(j)=NaN;
        indice_rap_opt_therm(j)=NaN;
    end
    
    [ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,0);
    
    % connexion
    wsec_cpl=wprim_emb1;
    dwsec_cpl=dwprim_emb1;
    csec_cpl=cprim_emb1;
    
%     if  sum(j==param.tdebug)>=1
%         %[ERR,ham_elec_trace,~,~,cprim2_elec_trace]=calc_hamilt_elec_HP_BV_2EMB(ERR,CYCL,ADCPL,BATT,VEHI,ACC,ACM1,MOTH,param,csec_cpl(:,j),wsec_cpl(:,j),dwsec_cpl(:,j),lambda_p,j,soc_p,0,param.lambda_cst);
%         [ERR,ham_elec_trace,~,~,cprim2_elec_trace]=calc_hamilt_elec_HP_BV_2EMB_TWC(ERR,VD,param,csec_cpl(:,j),wsec_cpl(:,j),dwsec_cpl(:,j),lambda1_p,lambda2_p,j,soc_p,Tcat_p,CO_eff_p, HC_eff_p, NOx_eff_p,0)
% %         figure(100)
% %         clf
%         figure
%         ham_hyb_trace(ham_hyb_trace==param.Ham_hors_limite  )=max(ham_hyb_trace(ham_hyb_trace~=param.Ham_hors_limite  ));
% %        plot(cprim2_cpl_trace,ham_hyb_trace,cprim2_elec_trace,ham_elec_trace,'r*',cprim2_therm_trace,ham_therm_trace,'g*')
%         plot(cprim2_cpl_trace,ham_hyb_trace,'g',cprim2_elec_trace,ham_elec_trace,'k*')
%         xlabel('cprim2_cpl')
%         ylabel('ham')
%         legend('ham hybride','ham elec','ham therm')
%         ham_elec=ham_elec_trace
%         clear ham_hyb_trace ham_elec_trace ham_elec_trace cprim2_elec_trace
%         title(num2str(j))
%         pause
%     end
    
    [ERR,ham_elec(j),lambda1_elec(j),lambda2_elec(j),cprim2_cpl_elec(j),ibat_elec(j),soc_elec(j),ubat_elec(j),Tcat_elec(j),...
        CO_eff_elec(j), HC_eff_elec(j), NOx_eff_elec(j), CO_elec(j), HC_elec(j), NOx_elec(j),indice_rap_opt_elec(j),dQs_elec(j),dQin_elec(j),dQgen_elec(j)]=...
        calc_hamilt_elec_HP_BV_2EMB_TWC(ERR,VD,param,csec_cpl(:,j),wsec_cpl(:,j),dwsec_cpl(:,j),lambda1_p,lambda2_p,j,soc_p,Tcat_p,CO_eff_p, HC_eff_p, NOx_eff_p,1);
    
    if isinf(ham_hyb(j)) && isinf(ham_elec(j))
        chaine=strcat(' ham_elec et hybride NaN',j);
        ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
        [cprim2_cpl,elhyb,lambda1,lambda2,indice_rap_opt]=backward_erreur(length(VD.CYCL.temps));
        return
    end
    
    
    if ham_hyb(j)<ham_elec(j) && ham_hyb(j)<ham_therm(j)
        ham(j)=ham_hyb(j);
        cprim2_cpl(j)=cprim2_cpl_hyb(j);
        if j~=1
            lambda1(j)=lambda1_hyb(j);
            lambda2(j)=lambda2_hyb(j);
            soc(j)=soc_hyb(j);
            Tcat(j)=Tcat_hyb(j);
            CO_eff(j)=CO_eff_hyb(j);
            HC_eff(j)=HC_eff_hyb(j);
            NOx_eff(j)= NOx_eff_hyb(j);
        end
        ibat(j)=ibat_hyb(j);
        ubat(j)=ubat_hyb(j);
        dcarb(j)=dcarb_hyb(j);
        Texh(j)=Texh_hyb(j);
        CO(j)=CO_hyb(j);
        HC(j)=HC_hyb(j);
        NOx(j)=NOx_hyb(j);
        elhyb(j)=1;
        indice_rap_opt(j)=indice_rap_opt_hyb(j);
        dQs(j)=dQs_hyb(j);
        dQin(j)=dQin_hyb(j);
        dQgen(j)=dQgen_hyb(j);
        
    elseif ham_elec(j)<=ham_hyb(j) & ham_elec(j)<=ham_therm(j)
        ham(j)=ham_elec(j);
        cprim2_cpl(j)=cprim2_cpl_elec(j);
        if j~=1
            lambda1(j)=lambda1_elec(j);
            lambda2(j)=lambda2_elec(j);
            soc(j)=soc_elec(j);
            Tcat(j)=Tcat_elec(j);
            CO_eff(j)=CO_eff_elec(j);
            HC_eff(j)=HC_eff_elec(j);
            NOx_eff(j)= NOx_eff_elec(j);
        end
        ibat(j)=ibat_elec(j);
        ubat(j)=ubat_elec(j);
        dcarb(j)=0;
        Texh(j)=VD.INIT.Tamb+273;;
        CO(j)=CO_elec(j);
        HC(j)=HC_elec(j);
        NOx(j)=NOx_elec(j);
        elhyb(j)=0;
        indice_rap_opt(j)=indice_rap_opt_elec(j);
        dQs(j)=dQs_elec(j);
        dQin(j)=dQin_elec(j);
        dQgen(j)=dQgen_elec(j);
    else
        'calc_HP_BV_2EMB_TWC_L633'
        ham_elec(j)
        ham_hyb(j)
        ham(j)=ham_therm(j);
        lambda(j)=lambda_therm(j);
        cprim2_cpl(j)=cprim2_cpl_therm(j);
        if j~=1
            soc(j)=soc_therm(j);
        end
        ibat(j)=ibat_therm(j);
        ubat(j)=ubat_therm(j);
        dcarb(j)=dcarb_therm(j);;
        elhyb(j)=2;
        indice_rap_opt(j)=indice_rap_opt_therm(j);
    end
    
    if ham==param.Ham_hors_limite
        chaine=strcat(' ham = Ham_hors_limite, indice',j);
        ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
    end
    
    if param.verbose>=1
        if rem(j,100)==0 | j==1 | j == length(VD.CYCL.temps)
            fprintf(1,'%s %.1f %s %.2f  %s %.2f %s %.2f \n','calc_HP_BV_2EMB - Indice: ',j,'cprm2',cprim2_cpl(j),...
                'Ibat',ibat(j),'Ubat',ubat(j)./VD.BATT.Nblocser);
        end
    end
end

if VD.CYCL.ntypcin==1 % rapport de boite optimise : on recree les vecteurs a partir des tableaux
    %csec_emb1=csec_emb1(sub2ind(size(csec_emb1),indice_rap_opt_1,1:length(indice_rap_opt_1)));
    %wsec_emb1=wsec_emb1(sub2ind(size(wsec_emb1),indice_rap_opt_1,1:length(indice_rap_opt_1)));
    %dwsec_emb1=dwsec_emb1(sub2ind(size(dwsec_emb1),indice_rap_opt_1,1:length(indice_rap_opt_1)));
    %elec_pos=elec_pos(sub2ind(size(elec_pos),indice_rap_opt_1,1:length(indice_rap_opt_1)));
    VD.CYCL.rappvit=indice_rap_opt-1;
    VD.CYCL.ntypcin=3;
    assignin('base','indice_rap_opt',indice_rap_opt)
end

assignin('base','lambda1',lambda1);
assignin('base','lambda2',lambda2);
assignin('base','Tcat',Tcat);
assignin('base','Texh',Texh);
assignin('base','CO_eff',CO_eff);
assignin('base','HC_eff',HC_eff);
assignin('base','NOx_eff',NOx_eff);
assignin('base','CO',CO);
assignin('base','HC',HC);
assignin('base','NOx',NOx);
assignin('base','ham_elec',ham_elec);
assignin('base','ham_hyb',ham_hyb);
assignin('base','ham_therm',ham_therm);
assignin('base','dQin',dQin);
assignin('base','dQs',dQs);
assignin('base','dQgen',dQgen);

return

