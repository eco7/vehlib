% function [ERR,ham,Lambda,cprim2_cpl,ibat,soc,dcarb,ubat]=calc_hamilt_hyb_HP_BV_2EMB_TWC(ERR,VD,param,cprim2_cpl,csec_cpl,wsec_cpl,dwsec_cpl,Lambda_p,j,soc_p,calc_min,Lambda_cst)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

function [ERR,ham,Lambda1,Lambda2,cprim2_cpl,ibat,soc,dcarb,ubat,Tcat,Texh, CO_eff, HC_eff, NOx_eff, CO, HC, NOx,indice_rap_opt,dQs,dQin,dQgen]=calc_hamilt_hyb_HP_BV_2EMB_TWC(ERR,VD,param,cprim2_cpl,csec_cpl,wsec_cpl,dwsec_cpl,Lambda1_p,Lambda2_p,j,soc_p,Tcat_p,CO_eff_p, HC_eff_p, NOx_eff_p,calc_min)

% Calcul du couple du moteur thermique
% limitation MTH en recup notamment le freinage fera la reste mais Cth est
% limite a Cth min
%wmt=wsec_cpl;
%cmt_max=interp1(MOTH.wmt_max,MOTH.cmt_max,wmt);
%cmt_min=interp1(MOTH.wmt_max,MOTH.cmt_min,wmt);

if length(wsec_cpl)~=1 % cas rapport de boite libre
    cprim2_cpl=repmat(cprim2_cpl,length(wsec_cpl),1);
end

%if length(wsec_cpl)~=1 % cas rapport de boite libre
[ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,repmat(csec_cpl,1,length(cprim2_cpl)),repmat(wsec_cpl,1,length(cprim2_cpl)),repmat(dwsec_cpl,1,length(cprim2_cpl)),cprim2_cpl,0);
if ~isempty(ERR)
    [ham,Lambda1,Lambda2,cprim2_cpl,ibat,soc,dcarb,ubat,Tcat,Texh, CO_eff, HC_eff, NOx_eff, CO, HC, NOx,indice_rap_opt,dQs,dQin,dQgen]=backward_erreur(1);
    return;
end
% else
%     [ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl]=calc_adcpl(ERR,csec_cpl,wsec_cpl,dwsec_cpl,cprim2_cpl,0,ADCPL);
% end


% Connexion
wmt=wprim1_cpl;
dwmt=dwprim1_cpl;
cmt=cprim1_cpl+VD.MOTH.J_mt.*dwmt;
wacm1=wprim2_cpl;
dwacm1=dwprim2_cpl;
cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;

% Calcul du couple du moteur thermique
% limitation MTH en recup notamment le freinage fera la reste mais Cth est
% limite a Cth min
cmt_max=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt);
cmt_min=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_min,wmt);


% Calcul des conditions en amont de la machine electrique
%% Remarque : les limitations sur cacm1 max min doivent etre ecrite en
%% dehors de cette fonction
%
%[Li,Co]=size(cacm1);
%wacm1=repmat(wacm1,1,Co);
%[ERR,qacm1]=calc_pertes_acm1(ERR,repmat(cacm1',1,length(wsec_cpl)),wacm1,dwacm1,ACM1);
[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
if ~isempty(ERR)
    cacm1
    wacm1
    disp(ERR.message)
    [ham,Lambda1,Lambda2,cprim2_cpl,ibat,soc,dcarb,ubat,Tcat,Texh, CO_eff, HC_eff, NOx_eff, CO, HC, NOx,indice_rap_opt,dQs,dQin,dQgen]=backward_erreur(1);
    return;
end

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);
if ~isempty(ERR)
    [ham,Lambda1,Lambda2,cprim2_cpl,ibat,soc,dcarb,ubat,Tcat,Texh, CO_eff, HC_eff, NOx_eff, CO, HC, NOx,indice_rap_opt,dQs,dQin,dQgen]=backward_erreur(1);
    return;
end

%if length(wsec_cpl)==1 % sinon on cr??e un vecteur inutilement
    pacc=pacc(j);
%end

pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc;

[ERR,ibat,ubat,soc,ah,E,R,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,soc_p,0,pres,0);
if ~isempty(ERR)
    [ham,Lambda1,Lambda2,cprim2_cpl,ibat,soc,dcarb,ubat,Tcat,Texh, CO_eff, HC_eff, NOx_eff, CO, HC, NOx,indice_rap_opt,dQs,dQin,dQgen]=backward_erreur(1);
    return;
end

[ERR,dcarb]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,1,0);
if ~isempty(ERR)
    [ham,Lambda1,Lambda2,cprim2_cpl,ibat,soc,dcarb,ubat,Tcat,Texh, CO_eff, HC_eff, NOx_eff, CO, HC, NOx,indice_rap_opt,dQs,dQin,dQgen]=backward_erreur(1);
    return;
end


if isfield(VD,'TWC')
    % Calcul des emissions moteur
    [ERR, Tcat, Texh, CO_eff, HC_eff, NOx_eff, CO, HC, NOx,dQs,dQin,dQgen]= ...
        calc_exhaust_emissions(ERR, param, VD, j, wmt, cmt, dcarb, ...
        Tcat_p, CO_eff_p, HC_eff_p, NOx_eff_p);
end

if strcmp(param.lambda1_cst,'non') % ce calcul etant long il est preferable de la faire une fois pour toute 
    % et pas a chaque appel de la fonction
    if ~isfield(VD.BATT,'dE_dEn')
        if isfield(VD.BATT,'tbat')
            [bidon,Li_T20]=min(abs(VD.BATT.tbat-20));
        elseif isfield(VD.BATT,'Tbat_table')
            [bidon,Li_T20]=min(abs(VD.BATT.Tbat_table-20));
        end
        Ebatt=(trapz(VD.BATT.dod_ocv,VD.BATT.ocv(Li_T20,:))-cumtrapz(VD.BATT.dod_ocv,VD.BATT.ocv(Li_T20,:)))*3600*VD.BATT.Cahbat_nom/100*VD.BATT.Nblocser*VD.BATT.Nbranchepar;
        OCV=VD.BATT.ocv(1,:)*VD.BATT.Nblocser;
        dE_dEn(2:length(Ebatt)-1)=(OCV(3:end)-OCV(1:end-2)) ./ (Ebatt(3:end)-Ebatt(1:end-2));
        dE_dEn(1)=dE_dEn(2);
        dE_dEn(length(Ebatt))=dE_dEn(length(Ebatt)-1);
        VD.BATT.Enbatt=Ebatt;
        VD.BATT.dE_dEn=dE_dEn;
    end

    if j>1
        DOD=100-soc;
        %OCV=BATT.ocv(1,:);
        %Enbatt = BATT.Cahbat_nom/100 * BATT.Nblocser *...
        %    ( trapz(BATT.dod_ocv,OCV)-...
        %      trapz( [ BATT.dod_ocv(BATT.dod_ocv<DOD) DOD ] , [ OCV(BATT.dod_ocv<DOD) interp1(BATT.dod_ocv,OCV,DOD)] ));
        %Enbatt=Enbatt+Ibat.*E*Dt; %Energie contenue dans la batterie
        %dQdEnsc=-BATT.RdFarad./R*(BATT.K*Enbatt-(BATT.K*E.*E-2*BATT.K*R*Pbatt)
        %./sqrt(E.*E-4*Pbatt.*R)) ; % D??riv?? de Q (puissance entrante dans la batterie parfaite)
        dE_dEn=0*ones(1,length(DOD));
        %dE_dEn(delta>=0)=interp1(BATT.dod_ocv,BATT.dE_dEn,DOD(delta>=0));
        dE_dEn=interp1(VD.BATT.dod_ocv,VD.BATT.dE_dEn,DOD);
        Pbat=ubat.*ibat;
        dQdEnsc=-VD.BATT.RdFarad./(2*R).*  ( 2*E.*dE_dEn - dE_dEn.*sqrt(E.^2-4*Pbat.*R) - ((E.^2).*dE_dEn)./sqrt(E.^2-4*Pbat.*R) );  % si seul la tension ?? vide d??pend de l'??nergie stock??
        %dQdEnsc=0;;
        %    if strcmp(FP.exist,'oui')
        %         Ensc_min=FP.xmin*(1+FP.delta_min);
        %         Ensc_max=FP.xmax*(1-FP.delta_max);
        %         dfonc=0*ones(1,length(Cme));
        %         dfonc(Ensc < Ensc_min)=2*FP.rmin*((Ensc(Ensc<Ensc_min)-(Ensc_min))/(FP.xmax^2));
        %         dfonc(Ensc > Ensc_max)=2*FP.rmax*((Ensc(Ensc>Ensc_max)-(Ensc_max))/(FP.xmax^2));
        %    else
        %       dfonc=0;
        %    end
        dfonc=0;
        Dt=param.pas_temps;
        Lambda=(Lambda_p-dfonc)./(1+dQdEnsc*Dt);
    else
        Lambda=Lambda_p*ones(1,length(cacm1));
    end

else
    Lambda1=Lambda1_p;
end

if strcmp(param.lambda2_cst,'non')
    dQdTcat=-(VD.TWC.HsA+VD.TWC.HinA);
    lambdaFG=1;
    debitAir=lambdaFG*VD.MOTH.AStoechio*dcarb/1000;
    debitGaz=debitAir*(1+lambdaFG/VD.MOTH.AStoechio);
    lambdaTP=1;
    CO_FG_ppm=interp1(VD.MOTH.lambda,VD.MOTH.COppm,lambdaFG);
    HC_FG_ppm=interp1(VD.MOTH.lambda,VD.MOTH.HCppm,lambdaFG);
    NOx_FG_ppm=interp1(VD.MOTH.lambda,VD.MOTH.NOxppm,lambdaFG);
    
    %CO_eff=exp(-VD.TWC.aCO*(VD.TWC.dCO./(lambdaTP-VD.TWC.CO0)).^VD.TWC.mCO-VD.TWC.aT*(VD.TWC.dT./(Tcat-VD.TWC.T0)).^VD.TWC.mT);
   % HC_eff=exp(-VD.TWC.aHC*(VD.TWC.dHC./(lambdaTP-VD.TWC.HC0)).^VD.TWC.mHC-VD.TWC.aT*(VD.TWC.dT./(Tcat-VD.TWC.T0)).^VD.TWC.mT);
    %NOx_eff=exp(-VD.TWC.aNOx*(VD.TWC.dNOx./(lambdaTP-VD.TWC.NOx0)).^VD.TWC.mNOx-VD.TWC.aT*(VD.TWC.dT./(Tcat-VD.TWC.T0)).^VD.TWC.mT);
    %dNOx_effdTcat=-VD.TWC.aT*VD.TWC.mT*(VD.TWC.dT^VD.TWC.mT)./((Tcat-VD.TWC.T0).^(VD.TWC.mT+1)).*exp(-VD.TWC.aNOx*(VD.TWC.dNOx./(lambdaTP-VD.TWC.NOx0)).^VD.TWC.mNOx-VD.TWC.aT*(VD.TWC.dT./(Tcat-VD.TWC.T0)).^VD.TWC.mT);
    %dCO_effdTcat=-VD.TWC.aT*VD.TWC.mT*(VD.TWC.dT^VD.TWC.mT)./((Tcat-VD.TWC.T0).^(VD.TWC.mT+1)).*CO_eff;
    %dHC_effdTcat=-VD.TWC.aT*VD.TWC.mT*(VD.TWC.dT^VD.TWC.mT)./((Tcat-VD.TWC.T0).^(VD.TWC.mT+1)).*HC_eff;
    %dNOx_effdTcat=-VD.TWC.aT*VD.TWC.mT*(VD.TWC.dT^VD.TWC.mT)./((Tcat-VD.TWC.T0).^(VD.TWC.mT+1)).*NOx_eff;
    
    dCO_effdTcat=VD.TWC.aT*VD.TWC.mT*(VD.TWC.dT^VD.TWC.mT)./((Tcat-VD.TWC.T0).^(VD.TWC.mT+1)).*(CO_eff);
    %dCO_effdTcat=(CO_eff-CO_eff_p)./(Tcat-Tcat_p);
    dCO_dTcat=-(debitGaz*1000).*(CO_FG_ppm*1e-6).*dCO_effdTcat;
    
    dHC_effdTcat=VD.TWC.aT*VD.TWC.mT*(VD.TWC.dT^VD.TWC.mT)./((Tcat-VD.TWC.T0).^(VD.TWC.mT+1)).*(HC_eff);
    %dHC_effdTcat=(HC_eff-HC_eff_p)./(Tcat-Tcat_p);   
    dHC_dTcat=-(debitGaz*1000).*(HC_FG_ppm*1e-6).*dHC_effdTcat;
    
    dNOx_effdTcat=VD.TWC.aT*VD.TWC.mT*(VD.TWC.dT^VD.TWC.mT)./((Tcat-VD.TWC.T0).^(VD.TWC.mT+1)).*(NOx_eff);
    %dNOx_effdTcat=(NOx_eff-NOx_eff_p)./(Tcat-Tcat_p);
    dNOx_dTcat=-(debitGaz*1000).*(NOx_FG_ppm*1e-6).*dNOx_effdTcat;
    
    Dt=param.pas_temps;
   % dQdTcat=0;
    dPE_dTcat=dCO_dTcat/VD.TWC.CO_EURO4+dHC_dTcat/VD.TWC.THC_EURO4+dNOx_dTcat/VD.TWC.NOx_EURO4;
    %Lambda2=(Lambda2_p-8e4*param.K*Dt*dPE_dTcat)./(1+dQdTcat*Dt/(VD.TWC.MCata*VD.TWC.CpCata));
     Lambda2=(Lambda2_p)./(1+dQdTcat*Dt/(VD.TWC.MCata*VD.TWC.CpCata));
    
    %Lambda2=(Lambda2_p)./(1+dQdTcat*Dt/(VD.TWC.MCata*VD.TWC.CpCata));
else
    Lambda2=Lambda2_p;
end

%%% Calcul conso et hamiltonien
if isfield(param,'K')
    QCata=dQs+dQin-dQgen;
    ham=VD.MOTH.pci*dcarb+8e4*param.K*(CO/VD.TWC.CO_EURO4+HC/VD.TWC.THC_EURO4+NOx/VD.TWC.NOx_EURO4)+Lambda1.*ibat.*E.*RdF+...
        Lambda2.*1/(VD.TWC.MCata*VD.TWC.CpCata).*QCata;
else
    ham=VD.MOTH.pci*dcarb+Lambda.*RdF.*ibat.*E;
end

%%% Limitations sur le courant batterie et le couple max
ham(cmt>cmt_max | isnan(ibat) | isnan(qacm1)) = param.Ham_hors_limite  ;

%% On elimine les ham a couple <0
%ham(cmt<0)=param.Ham_hors_limite  ;

%%% En rapport de boite libre il faut interdire le Point mort su csec_cpl>0
[Li,Co]=size(ham);

if Li>1 && csec_cpl(2)>0
    ham(1,:)=param.Ham_hors_limite  ;
end
%%% calcul du min et des grandeurs associee
indice_rap_opt=NaN;
if calc_min==1 % Sinon on renvoit les grandeurs vectotielles pour trace hamilt notamment
    if Li>1
        [ham,j_min]=min(ham,[],2);
        [ham,i_min]=min(ham); % le cas point mort n'as pas de sens en mode hybride
        indice_rap_opt=i_min;
        j_min=j_min(i_min);
        cmt=cmt(i_min,j_min);
        wmt=wmt(i_min,j_min);
        ibat=ibat(i_min,j_min);
        ubat=ubat(i_min,j_min);
        soc=soc(i_min,j_min);
        dcarb=dcarb(i_min,j_min);
        cacm1=cacm1(i_min,j_min);
        cprim2_cpl=cprim2_cpl(i_min,j_min);
        qacm1=qacm1(i_min,j_min);
        if strcmp(param.lambda1_cst,'non')
            Lambda1=Lambda1(i_min);
        end
        if strcmp(param.lambda2_cst,'non')
            Lambda2=Lambda2(i_min);
        end
    else
        [ham,i_min]=min(ham);
        cmt=cmt(i_min);
        ibat=ibat(i_min);
        ubat=ubat(i_min);
        soc=soc(i_min);
        dcarb=dcarb(i_min);
        cacm1=cacm1(i_min);
        cprim2_cpl=cprim2_cpl(i_min);
        qacm1=qacm1(i_min);
        indice_rap_opt=NaN;
        Tcat=Tcat(i_min);
        Texh=Texh(i_min);
        CO_eff=CO_eff(i_min);
        HC_eff=HC_eff(i_min);
        NOx_eff=NOx_eff(i_min);
        CO=CO(i_min);
        HC=HC(i_min);
        NOx=NOx(i_min);
        dQin=dQin(i_min);
        dQgen=dQgen(i_min);
        if strcmp(param.lambda1_cst,'non')
            Lambda1=Lambda1(i_min);
        end
        if strcmp(param.lambda2_cst,'non') && length(Lambda2)~=1
            Lambda2=Lambda2(i_min);
        end
    end
end

return


