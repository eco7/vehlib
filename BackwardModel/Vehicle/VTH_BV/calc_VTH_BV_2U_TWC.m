function [ERR, VD, res] = calc_VTH_BV_2U_TWC(ERR, vehlib, param, VD)

name = graphe_VTH_BV_2U_TWC(ERR, vehlib, param, VD);

[res,VD,param] = extraction_chemin_optimal_1X_2U_TWC('./', name);

VD.param = param;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

