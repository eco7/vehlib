function name = graphe_VTH_BV_2U_TWC(ERR, vehlib, param, VD)

cinem = vehlib.CYCL;

% traitement des demandes de modification de champ dans param
if isfield(param,'champ_modif')
    for i=1:length(param.champ_modif)
        eval(['VD.' param.champ_modif{i} ' = param.valeur_modif(i);'])        
    end
end

%% fichier de log

if isfield(param,'fichier_init')
    load(param.fichier_init,'i_fichier')
else
    i_fichier = 1;
end

name_source = name_rec_file(cinem,VD,param);
name = [name_source '_' num2str(i_fichier)];
list_name{i_fichier} = name;

if ~isfield(param,'fichier_init')
    save(name,'list_name');
end
% file = matfile(name,'Writable',true);

%% Profil de vitesse
if isfield(param,'pas_temps') && param.pas_temps ~= 1
    % re-echantillonnage du cycle avec le pas de temps
    t_sample = VD.CYCL.temps(1):param.pas_temps:VD.CYCL.tmax;
    VD.CYCL.vitesse = interp1(VD.CYCL.temps,VD.CYCL.vitesse,t_sample);
    VD.CYCL.vitesse(VD.CYCL.vitesse<1.e-2) = 0;
    VD.CYCL.rappvit = round(interp1(VD.CYCL.temps,VD.CYCL.rappvit,t_sample,'nearest'));
    VD.CYCL.temps = t_sample;
    clearvars t_sample
end
temps = VD.CYCL.temps;
vitesse = VD.CYCL.vitesse;
rapport = VD.CYCL.rappvit';
pas_temps = temps(2) - temps(1);
ntps = size(temps);

% TODO : mettre pas de temps dans les parametres : adapter la boucle

%% Valeurs par defaut des parametres non renseignesès midi,
if ~isfield(param,'lg_calcul')
    param.lg_calcul = ntps(2)-1;
else
    param.lg_calcul = param.lg_calcul-1;
end

if ~isfield(param,'k_cata')
    param.k_cata = 1;
end

if ~isfield(param,'emission_cst')
    param.emission_cst = 0;
end

%% Calcul des couples et regimes
% cinematique
accel = ([diff(vitesse) 0]+[0 diff(vitesse)])/(2*pas_temps);

% bilan de masse et inertie
g = 9.81;
masse = VD.VEHI.Mveh + VD.VEHI.Charge...
    + VD.MOTH.Masse_mth;
Jveh = masse*VD.VEHI.Rpneu^2 + VD.VEHI.Nbroue*VD.VEHI.Inroue;

% calcul des forces
Froul = masse*g*(VD.VEHI.a+VD.VEHI.b*vitesse.^2);
Faero = 0.5*VD.VEHI.Roatm*VD.VEHI.Sveh*VD.VEHI.Cx*vitesse.^2;
Fres = (Froul+Faero).*sign(vitesse);

% bilan a la roue
wroue = vitesse/VD.VEHI.Rpneu;
dwroue = accel/VD.VEHI.Rpneu;
Cres = Fres*VD.VEHI.Rpneu;
Croue = Cres + Jveh*dwroue;

% bilan en amont du pont
i = Croue>=0;
A(i) = 1/(VD.RED.kred*VD.RED.rend);
A(~i) = VD.RED.rend./VD.RED.kred;
Cpont = A.*Croue;
wpont = wroue*VD.RED.kred;
dwpont = dwroue*VD.RED.kred;

% bilan en amont de la boite de vitesse
rapports_bv=[0 VD.BV.k];
rendements_bv=[0 VD.BV.Ro];
k_bv=rapports_bv(rapport+1);
rend_bv=rendements_bv(rapport+1);

i = Cpont>=0;
A(i) = 1./(k_bv(i).*rend_bv(i));
A(~i) = rend_bv(~i)./k_bv(~i);
% On corrige les divisions par zero au point mort.
A(isnan(A))=0; A(isinf(A))=0;
Cbv = A.*Cpont;
wbv = wpont.*k_bv;
dwbv = dwpont.*k_bv;

wmt = wbv;
dwmt = dwbv;

% wmt cannot be < idle speed
% hyp : the clutch that slips is by the gearbox
% Hybrid/thermal modes : engines directly connected (proportional speeds)
% TODO : a l'arret, etudier l'impact de la vitesse choisie sur le comportement

indRal = find(wmt<VD.MOTH.ral);
wmt(indRal)=VD.MOTH.ral;
dwmt(indRal)=0;

Ctot = Cbv + VD.MOTH.J_mt*dwmt ;

%% discretisation en grille
% parametres de la grille : nombre de points en temperature
% sert pour le calcul de l'identifiant unique (multiplicateur de soc)
% hypothese : catalyseur toujours en dessous de 2000K
Tcat_max = 2000;
nb_pts_T = (Tcat_max + 1)/param.pas_T;
nb_pts_T = ceil(nb_pts_T/10000)*10000;

save(name,'param','VD','-append');

if isfield(param,'pas_phi')
    if isfield(param,'phi_cst')
        error('erreur de definition des parametres : il doit y avoir soit un pas soit une constante pour Phi')
    end        
    Phi = param.phi_min:param.pas_phi:param.phi_max;
else
    Phi = param.phi_cst;
end

l_Phi = length(Phi);

if isfield(param,'pas_delta_AA')
    if isfield(param,'delta_AA_cst')
        error('erreur de definition des parametres : il doit y avoir soit un pas soit une constante pour delta_AA')
    end
    delta_AA = param.delta_AA_min:param.pas_delta_AA:param.delta_AA_max;
else
    delta_AA = param.delta_AA_cst;
end
l_AA = length(delta_AA);

Padm = 1; % Arbitraire a ce stade
l_Padm = length(Padm);

commande_Phi = repmat(Phi,1,l_AA);
commande_AA = repmat(delta_AA,l_Phi,1);
commande_AA = commande_AA(:)';
commande_2U = [commande_Phi;commande_AA];
l_commande_2U = length(commande_AA);
commande_Padm = repmat(Padm,l_commande_2U,1);
commande_Padm = commande_Padm(:)';
commande_2U = repmat(commande_2U,1,l_Padm);
commande_3U = [commande_2U;commande_Padm];

Phi = commande_3U(1,:);
delta_AA = commande_3U(2,:);
Padm = commande_3U(3,:);


%% facteurs de normalisation pour somme ponderee
CIN_NEDC_BV_lisse %chargement du NEDC pour le calcul des valeurs moyennes sur cycle normalise
km = sum(CYCL.vitesse(1:end)*(CYCL.temps(2)-CYCL.temps(1)))*1e-3; %distance parcourue
km_s = km./CYCL.temps(end); %distance moyenne parcourue en une seconde
clearvars CYCL

if ~isfield(param,'ValueRef') || strcmp(param.ValueRef,'EURO4') 
    dcarb_norm = VD.VEHI.conso_NEDC/100*VD.MOTH.dens_carb*km_s; %debit carburant moyen sur NEDC
    
    CO_norm = VD.TWC.CO_EURO4*km_s;
    HC_norm = VD.TWC.THC_EURO4*km_s;
    NOx_norm = VD.TWC.NOx_EURO4*km_s;
else
    dcarb_norm = param.consoref/100*VD.MOTH.dens_carb*km_s; %debit carburant moyen sur NEDC
    
    CO_norm = param.COref*km_s;
    HC_norm = param.HCref*km_s;
    NOx_norm = param.NOxref*km_s;
end
%_________________________________________________________________________%
%_________________________________________________________________________%
clearvars -except wmt dwmt Cbv VD param pas_temps ntps nb_pts_T name file dcarb_norm CO_norm HC_norm NOx_norm i_fichier name_source list_name Phi delta_AA Padm indice_commande Pression

%% boucle

% premier point : soc init, T init et cout initial
if isfield(param,'fichier_init')
    load(param.fichier_init)
    i_init = i+1; clearvars i;
else
    T_1 = param.T_init;
    cost2go_1 = param.cost2go_init;
    i_init = 1;
end

tic

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i = i_init:param.lg_calcul
    
% Affectation de la bonne pression d'admission
% Calcul de la pression d'admission du point de fonctionnement moteur
 Cmt = Cbv(i) + VD.MOTH.J_mt*dwmt(i) ;
 [~, Padm] = calc_mt_3U_reverse(Cmt,wmt(i)*30/pi,Phi,delta_AA,VD);
 
 if min(Padm) >1000
     fprintf(1,'%s %f %s %f\n','Pression atmo insuffisante :',min(Padm),' a l indice :',i);
     [~,in] = min(Padm);
     Padm(in)=1000;
     Phi(in)=1.0;
     Padm(~in)=NaN;
 else
    Padm(Padm>1000) = NaN;
 end
 
if mod(i,20)==0 && param.verbose
    fprintf('iteration %.0f : %.1f s\n',i,toc)
end

% nouveau fichier si depassement de la taille maximale (2e9)
if mod(i,10)==0
    info = dir([name '.mat']);
    if info.bytes >= 2e9
        i_fichier = i_fichier + 1;
        name = [name_source '_' num2str(i_fichier)];
        list_name{i_fichier} = name;
        save(list_name{1},'list_name','-append');
        tps_debut = toc;
        save(name,'VD','param','tps_debut');
    end
end

T_0 = T_1;
cost2go_0 = cost2go_1;

szU = size(Phi);
szX = size(T_0);

indice_commande = 1:szU(2);

% le calcul du point de fonctionnement des moteurs depend uniquement
% de la valeur de la commande (hyp : param batterie independants du soc)
% il y a autant d'elements aux resultats que d'arcs accessibles
% Size [1, szU]
[dcarb, Cmt] = calc_mt_3U(wmt(i),Phi,delta_AA,Padm,szU,VD);

% Size [szX, szU]
[nd_CO, nd_HC, nd_NOx, T_1] = calc_exhaust_3U(T_0,Phi,delta_AA,Padm,dcarb,wmt(i),szU,szX,VD,pas_temps,param,i,0);
T_1 = round(T_1/param.pas_T)*param.pas_T;
clear Cmt;
%min(T_1)
%max(T_1)

nd_CO = nd_CO(:);
nd_HC = nd_HC(:);
nd_NOx = nd_NOx(:);
T_1 = T_1(:);

% depend de l'etat initial
ind_0 = (1:length(cost2go_0))';
nd_ind_0 = repmat(ind_0,szU);
nd_ind_0 = nd_ind_0(:); %probleme d'eval
nd_cost2go_0 = repmat(cost2go_0,szU);
nd_cost2go_0 = nd_cost2go_0(:);

% depend de la commande
nd_ind_com = repmat(indice_commande,szX);
nd_ind_com = nd_ind_com(:);
nd_dcarb = repmat(dcarb,szX);
nd_dcarb = nd_dcarb(:); clear dcarb;

% verification des contraintes : soc entre le min et le max
% il peut y avoir des nan si certaines commandes sont en dehors 
% des domaines de fonctionnement moteur machine ou batterie
in_bounds = ~isnan(nd_dcarb) & ~isnan(T_1) ...
    & ~isnan(nd_CO) & ~isnan(nd_HC) & ~isnan(nd_NOx);

% etats necessaire pour le calcul de l'identifiant
T_1 = T_1(in_bounds);

% quantites sauvegardees (eval) pour le post-traitement
nd_ind_0 = nd_ind_0(in_bounds);
nd_ind_com = nd_ind_com(in_bounds);
% expression des performances necessaire pour le calcul du cost to go
nd_cost2go_0 = nd_cost2go_0(in_bounds);
nd_dcarb = nd_dcarb(in_bounds);
nd_CO = nd_CO(in_bounds);
nd_HC = nd_HC(in_bounds);
nd_NOx = nd_NOx(in_bounds);

nd_cout_arc = nd_dcarb/dcarb_norm + param.alpha...
    *(nd_CO/CO_norm + nd_HC/HC_norm + nd_NOx/NOx_norm)/3;
clear nd_dcarb nd_CO nd_HC nd_NOx;
cost2go_1 = nd_cost2go_0 + nd_cout_arc;
clear nd_cost2go_0 nd_cout_arc

% utilisation de uniquetol pour creer un identifiant unique
% pour les elements de meme soc et de meme temperature a tol pres
% tol necessaire pour les questions d'arrondis
identifiant = round((T_1/param.pas_T+1));
% on ordonne sur deux colonnes : identifiant puis cout
% de facon a avoir en premier les couts min pour chaque id
[id_dans_ordre,ordre_cout] = sortrows([identifiant,cost2go_1]); clear identifiant;
% on determine l'indice du premier element avec unique : 
% indice du cout min dans le tableau ordonne
[~,i_min_dans_ordre] = unique(id_dans_ordre(:,1)); clear id_dans_ordre;
% on cherche l'indice du cout min dans le tableau original non ordonne
i_min = ordre_cout(i_min_dans_ordre); clear i_min_dans_ordre ordre_cout;

T_1 = T_1(i_min);
cost2go_1 = cost2go_1(i_min);

% sauvegardes d'etat du code pour pouvoir reprendre si erreur
if isfield(param,'debug') && param.debug
    if mod(i,20)==0
        name_boucle = ['loop_' erase(name_source,'log_') num2str(i_fichier) '_A'];
        save(name_boucle,'i','T_1','cost2go_1','i_fichier','list_name');
    elseif mod(i,20)==10
        name_boucle = ['loop_' erase(name_source,'log_') num2str(i_fichier) '_B'];
        save(name_boucle,'i','T_1','cost2go_1','i_fichier','list_name');
    end
end

% enregistrement progressif du chemin optimal dans un fichier de logs
eval(['ind_precedent_' num2str(i) ' = nd_ind_0(i_min);']);
eval(['commande_opti_' num2str(i) ' = nd_ind_com(i_min);']);
eval(['save(''' name ''', ''ind_precedent_' num2str(i) ''', ''commande_opti_' num2str(i) ''', ''-append'');']);
eval(['clearvars commande_opti_' num2str(i) ' ind_precedent_' num2str(i)])
% file.cost2go_opti(1,i) = {cost2go_1};
% file.ind_precedent(1,i) = {nd_ind_0(i_min)};
% file.commande_opti(1,i) = {nd_Dsoc(i_min)};

clear nd_ind_0 i_min

end

%_________________________________________________________________________%
%_________________________________________________________________________%
clearvars -except wmt dwmt Cbv Ibat_min VD cost2go_1 pas_temps commande_opti ind_precedent ...
    param tps_calcul name i_fichier name_source list_name Phi delta_AA

tps_calcul = toc;
fprintf('temps de calcul : %.0f min\n',tps_calcul/60)

save(name,'cost2go_1','pas_temps','VD','wmt','dwmt','Cbv','param','tps_calcul','Phi','delta_AA','-append')

end
