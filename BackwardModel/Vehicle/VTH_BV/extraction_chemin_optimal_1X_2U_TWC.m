function [res,VD,param] = extraction_chemin_optimal_1X_2U_TWC(pathname,name)

load([pathname filesep name],'list_name');
nb_files = length(list_name);
long_calcul = 1;

if nb_files<4 % possible de tout charger en memoire
    for i = 1:nb_files
    clearvars -except commande_opti ind_precedent long_calcul...
                      pathname name list_name i nb_files verbose_extract
        tic
        load([pathname filesep list_name{i} '.mat'])
        fprintf('chargement log : %.1f s\n',toc)
        fprintf(['log : ' list_name{i} '\n'])
        
        % regarder les instants correspondant au fichier en cours
        % liste de variables commande_opti_nb
        commande_opti_nb = who('commande_opti_*');
        % indices correspondants
        ind = str2double(erase(commande_opti_nb,'commande_opti_'));
        % long_calcul : plus grand des plus grands indices des differents fichiers
        long_calcul = max(long_calcul,max(ind));
        
        for i_extract=1:length(ind)
            eval(['commande_opti{' num2str(ind(i_extract)) '} = commande_opti_' num2str(ind(i_extract)) ';'])
            eval(['ind_precedent{' num2str(ind(i_extract)) '} = ind_precedent_' num2str(ind(i_extract)) ';'])
       end
    end
else % pas possible de tout charger en memoire
    load([pathname filesep list_name{nb_files} '.mat'],'param')
end

clearvars -except...
    verbose_extract pathname name list_name nb_files long_calcul...
    cost2go_1  commande_opti ind_precedent Cbv pas_temps wmt dwmt ...
    tps_calcul VD param Phi delta_AA Padm

T_init = param.T_init;

% soc_1 / cost2go_1 du dernier fichier (fin cycle) pour tester le soc final
load([pathname filesep list_name{nb_files} '.mat'],'cost2go_1')

% Tous les chemins sont valides, on retient celui coute le moins cher
[~,ind_min] = min(cost2go_1);

if nb_files<4 
    % les vecteurs entiers sont en memoire
    % initialisation pour le dernier indice
    try
    ind_opti(long_calcul) = ind_precedent{long_calcul}(ind_min);
    catch
        'rr'
    end
    ind_com_opti(long_calcul) = commande_opti{long_calcul}(ind_min);
    taille_matrice(long_calcul) = length(commande_opti{long_calcul});
    
    % parcours du cycle depuis la fin pour remonter au chemin optimal
    for i_chemn = long_calcul:-1:2
        ind_opti(i_chemn-1) = ind_precedent{i_chemn-1}(ind_opti(i_chemn));       
        ind_com_opti(i_chemn-1) = commande_opti{i_chemn-1}(ind_opti(i_chemn));
        taille_matrice(i_chemn-1) = length(commande_opti{i_chemn-1});
    end
else
    disp('A debugger');
    for ii = nb_files:-1:1
        
        tic
        load([pathname filesep list_name{ii} '.mat'])
        fprintf('chargement log : %.1f s\n',toc)
        fprintf(['log : ' list_name{ii} '\n'])
        
        % regarder les instants correspondant au fichier en cours
        % liste de variables commande_opti_nb
        commande_opti_nb = who('commande_opti_*');
        % indices correspondants
        ind = str2double(erase(commande_opti_nb,'commande_opti_'));
        % long_calcul : plus grand des plus grands indices des differents fichiers
        long_calcul = max(long_calcul,max(ind));
        
        if ii==nb_files
            i_max = max(ind);
        else
            i_max = max(ind)+1;
        end
        i_min = min(ind)+1;
        
        % initialisation pour le dernier indice (seulement dernier fichier)
        if ii==nb_files
            eval(['ind_com_opti(long_calcul) = commande_opti_' num2str(long_calcul) '(ind_min);'])
            eval(['taille_matrice(long_calcul) = length(commande_opti_' num2str(long_calcul) ');'])
        end
        
        % parcours du cycle depuis la fin pour remonter au chemin optimal
        % fichier par fichier
        for i_chemn = i_max:-1:i_min
            eval(['ind_com_opti(i_chemn-1) = commande_opti_' num2str(i_chemn-1) '(ind_opti(i_chemn));'])
            eval(['taille_matrice(i_chemn-1) = length(commande_opti_' num2str(i_chemn-1) ');'])
        end
        
        clearvars -except...
            verbose_extract ...
            pathname list_name nb_files ...
            long_calcul T_init tps_vrtx tps_edg epsilon...
            ind_com_opti taille_matrice...
             Cbv  pas_temps wmt dwmt...
            tps_calcul VD param Phi delta_AA Padm

    end
end

T_1_rnd = T_init;
T_1_fwd = T_init;
T_1_exh = T_init;

tps_vrtx = VD.CYCL.temps;
tps_edg = VD.CYCL.temps(2:end); %TODO: milieu intervalle ?
wmt = wmt(1:long_calcul);

T_rnd = nan(size(tps_vrtx));
T_fwd = nan(size(tps_vrtx));
T_exh = nan(size(tps_vrtx));

Phi_opti = nan(1,long_calcul);
delta_AA_opti = nan(1,long_calcul);
Padm_opti = nan(1,long_calcul);
dcarb = nan(1,long_calcul);



Cmt = nan(1,long_calcul);
nd_CO_rnd = nan(1,long_calcul);
nd_HC_rnd = nan(1,long_calcul);
nd_NOx_rnd = nan(1,long_calcul);
nd_CO_fwd = nan(1,long_calcul);
nd_HC_fwd = nan(1,long_calcul);
nd_NOx_fwd = nan(1,long_calcul);

cost2go_rnd = zeros(1,long_calcul+1);
cost2go_fwd = zeros(1,long_calcul+1);

for i_bcl = 1:long_calcul
    % on ne veut pas faire l'analyse de memoire dans calc_poll_temp
    % pour l'exploitation : param.debug doit valoir 0 
    param.debug = 0;
        
    cost2go_0_rnd = cost2go_rnd(i_bcl);
    cost2go_0_fwd = cost2go_fwd(i_bcl);
    
    T_rnd(i_bcl) = T_1_rnd;
    T_fwd(i_bcl) = T_1_fwd;
    T_exh(i_bcl) = T_1_exh;
    
    Phi_opti(i_bcl) = Phi(ind_com_opti(i_bcl));
    delta_AA_opti(i_bcl) = delta_AA(ind_com_opti(i_bcl));
    % Affectation de la bonne pression d'admission
    % Calcul de la pression d'admission du point de fonctionnement moteur
    Cmt(i_bcl) = Cbv(i_bcl) + VD.MOTH.J_mt*dwmt(i_bcl) ;
    [~, Padm_opti(i_bcl)] = calc_mt_3U_reverse(Cmt(i_bcl),wmt(i_bcl)*30/pi,Phi_opti(i_bcl),delta_AA_opti(i_bcl),VD);

    % Recalcul du couple pour saturation au couple de frottement
    [dcarb(i_bcl), Cmt(i_bcl)] = calc_mt_3U(wmt(i_bcl),Phi_opti(i_bcl),delta_AA_opti(i_bcl),Padm_opti(i_bcl),1,VD);
    [nd_CO_rnd(i_bcl), nd_HC_rnd(i_bcl), nd_NOx_rnd(i_bcl), T_1_rnd, ~, ~, ~, ~, T_1_exh] = calc_exhaust_3U(T_rnd(i_bcl),Phi_opti(i_bcl),delta_AA_opti(i_bcl),Padm_opti(i_bcl),dcarb(i_bcl),wmt(i_bcl),1,1,VD,pas_temps,param,i_bcl,0);
    T_1_rnd = round(T_1_rnd/param.pas_T)*param.pas_T;
    [nd_CO_fwd(i_bcl), nd_HC_fwd(i_bcl), nd_NOx_fwd(i_bcl), T_1_fwd] = calc_exhaust_3U(T_fwd(i_bcl),Phi_opti(i_bcl),delta_AA_opti(i_bcl),Padm_opti(i_bcl),dcarb(i_bcl),wmt(i_bcl),1,1,VD,pas_temps,param,i_bcl,0);
    
    cout_arc_fwd = dcarb(i_bcl) + param.alpha...
        *(nd_CO_fwd(i_bcl)/VD.TWC.CO_EURO4 + nd_HC_fwd(i_bcl)/VD.TWC.THC_EURO4 + nd_NOx_fwd(i_bcl)/VD.TWC.NOx_EURO4);
    cout_arc_rnd = dcarb(i_bcl) + param.alpha...
        *(nd_CO_rnd(i_bcl)/VD.TWC.CO_EURO4 + nd_HC_rnd(i_bcl)/VD.TWC.THC_EURO4 + nd_NOx_rnd(i_bcl)/VD.TWC.NOx_EURO4);

    cost2go_rnd(i_bcl+1) = cost2go_0_rnd + cout_arc_rnd;
    cost2go_fwd(i_bcl+1) = cost2go_0_fwd + cout_arc_fwd;
end

%% post-traitement : calcul des grandeurs pour la structure res
T_fwd(long_calcul+1) = T_1_fwd;
T_rnd(long_calcul+1) = T_1_rnd;
T_exh(long_calcul+1) = T_1_exh;

CO_rnd = cumsum(nd_CO_rnd*pas_temps,'omitnan');
HC_rnd = cumsum(nd_HC_rnd*pas_temps,'omitnan');
NOx_rnd = cumsum(nd_NOx_rnd*pas_temps,'omitnan');

CO_fwd = cumsum(nd_CO_fwd*pas_temps,'omitnan');
HC_fwd = cumsum(nd_HC_fwd*pas_temps,'omitnan');
NOx_fwd = cumsum(nd_NOx_fwd*pas_temps,'omitnan');

% indices de performance
d_km = sum(VD.CYCL.vitesse(2:long_calcul+1)*pas_temps)*1e-3;

conso_lp100km = sum(dcarb*pas_temps,'omitnan')./d_km*100/VD.MOTH.dens_carb;

CO_gpkm_rnd = CO_rnd(end)./d_km;
HC_gpkm_rnd = HC_rnd(end)./d_km;
NOx_gpkm_rnd = NOx_rnd(end)./d_km;

CO_gpkm_fwd = CO_fwd(end)./d_km;
HC_gpkm_fwd = HC_fwd(end)./d_km;
NOx_gpkm_fwd = NOx_fwd(end)./d_km;

% efficacite catalyseur en fonction de la temperature (lambda non inclus)
% cas simple : non ammorce eff_T = 0
eff_T_rnd = zeros(size(tps_vrtx));
eff_T_fwd = zeros(size(tps_vrtx));
% autre cas : calcul
i_chaud = T_rnd>VD.TWC.T0;
eff_T_rnd(i_chaud) = exp(-VD.TWC.aT*(VD.TWC.dT./(T_rnd(i_chaud)-VD.TWC.T0)).^VD.TWC.mT);
i_chaud_fwd = T_fwd>VD.TWC.T0;
eff_T_fwd(i_chaud_fwd) = exp(-VD.TWC.aT*(VD.TWC.dT./(T_fwd(i_chaud_fwd)-VD.TWC.T0)).^VD.TWC.mT);
t_50_fwd =tps_vrtx(find(eff_T_fwd>0.5,1));

res.CO_rnd.x = tps_edg;
res.CO_rnd.y = CO_rnd;
res.CO_rnd.label = 'Cumulative CO';
res.CO_rnd.unit = 'g';

res.HC_rnd.x = tps_edg;
res.HC_rnd.y = HC_rnd;
res.HC_rnd.label = 'Cumulative HC';
res.HC_rnd.unit = 'g';

res.NOx_rnd.x = tps_edg;
res.NOx_rnd.y = NOx_rnd;
res.NOx_rnd.label = 'Cumulative NOx';
res.NOx_rnd.unit = 'g';

res.CO_fwd.x = tps_edg;
res.CO_fwd.y = CO_fwd;
res.CO_fwd.label = 'Cumulative CO';
res.CO_fwd.unit = 'g';

res.HC_fwd.x = tps_edg;
res.HC_fwd.y = HC_fwd;
res.HC_fwd.label = 'Cumulative HC';
res.HC_fwd.unit = 'g';

res.NOx_fwd.x = tps_edg;
res.NOx_fwd.y = NOx_fwd;
res.NOx_fwd.label = 'Cumulative NOx';
res.NOx_fwd.unit = 'g';

res.dcarb.x = tps_edg;
res.dcarb.y = dcarb;
res.dcarb.label = 'fuel mass flow';
res.dcarb.unit = 'g/s';

res.Cmt.x = tps_edg;
res.Cmt.y = Cmt;
res.Cmt.label = 'Engine torque';
res.Cmt.unit = 'Nm';

res.wmt.x = tps_edg;
res.wmt.y = wmt;
res.wmt.label = 'Engine speed';
res.wmt.unit = 'rad/s';

res.Pmt.x = tps_edg;
res.Pmt.y = Cmt.*wmt;
res.Pmt.label = 'Power';
res.Pmt.unit = 'W';

res.Padm.x = tps_edg;
res.Padm.y = Padm_opti;
res.Padm.label = 'Intake pressure';
res.Padm.unit = 'mBar';

res.Phi.x = tps_edg;
res.Phi.y = Phi_opti;
res.Phi.label = 'Equivalent air/fuel ratio';
res.Phi.unit = '(-)';

res.deltaAA.x = tps_edg;
res.deltaAA.y = delta_AA_opti;
res.deltaAA.label = 'Spark advance relative to the optimal';
res.deltaAA.unit = '�BTDC';

res.run = param;
res.tps_calcul = tps_calcul;

res.conso_lp100km = conso_lp100km;
res.CO_gpkm_rnd = CO_gpkm_rnd;
res.HC_gpkm_rnd = HC_gpkm_rnd;
res.NOx_gpkm_rnd = NOx_gpkm_rnd;
res.CO_gpkm_fwd = CO_gpkm_fwd;
res.HC_gpkm_fwd = HC_gpkm_fwd;
res.NOx_gpkm_fwd = NOx_gpkm_fwd;

res.T_fwd.x = tps_vrtx;
res.T_fwd.y = T_fwd;
res.T_fwd.label = 'Temperature';
res.T_fwd.unit = 'K';

res.T_rnd.x = tps_vrtx;
res.T_rnd.y = T_rnd;
res.T_rnd.label = 'Temperature';
res.T_rnd.unit = 'K';

res.T_exh.x = tps_vrtx;
res.T_exh.y = T_exh;
res.T_exh.label = 'Exhaust Temperature';
res.T_exh.unit = 'K';

res.eff_T_fwd.x = tps_vrtx;
res.eff_T_fwd.y = eff_T_fwd;
res.eff_T_fwd.label = 'Efficiency';
res.eff_T_fwd.unit = '-';

res.eff_T_rnd.x = tps_vrtx;
res.eff_T_rnd.y = eff_T_rnd;
res.eff_T_rnd.label = 'Efficiency';
res.eff_T_rnd.unit = '-';

res.cost2go_fwd.x = tps_vrtx;
res.cost2go_fwd.y = cost2go_fwd;
res.cost2go_fwd.label = 'Cost to go';
res.cost2go_fwd.unit = '-';

res.cost2go_rnd.x = tps_vrtx;
res.cost2go_rnd.y = cost2go_rnd;
res.cost2go_rnd.label = 'Cost to go';
res.cost2go_rnd.unit = '-';

%% Figures et displays

% Donnees globales
fprintf('consommation : %.4f l/100km \n',conso_lp100km)
fprintf('CO : %.3f g/km (EURO4 : %.2f) \n',CO_gpkm_rnd,VD.TWC.CO_EURO4)
fprintf('HC : %.3f g/km (EURO4 : %.2f) \n',HC_gpkm_rnd,VD.TWC.THC_EURO4)
fprintf('NOx : %.3f g/km (EURO4 : %.2f) \n',NOx_gpkm_rnd,VD.TWC.NOx_EURO4)
 if ~isfield(param,'ValueRef') || strcmp(param.ValueRef,'EURO4') 
        fprintf('Conso de reference : %.3f; \t Cout conso : %.3f\n',VD.VEHI.conso_NEDC , ...
            res.conso_lp100km/VD.VEHI.conso_NEDC);
        fprintf('Val CO de reference : %.3f; \t Cout CO : %.3f\n',VD.TWC.CO_EURO4, ...
            param.alpha/3*res.CO_gpkm_fwd/VD.TWC.CO_EURO4);
        fprintf('Val HC de reference : %.3f; \t Cout HC : %.3f\n',VD.TWC.THC_EURO4, ...
            param.alpha/3*res.HC_gpkm_fwd/VD.TWC.THC_EURO4);
        fprintf('Val NOx de reference : %.3f; \t Cout NOx : %.3f\n',VD.TWC.NOx_EURO4, ...
            param.alpha/3*res.NOx_gpkm_fwd/VD.TWC.NOx_EURO4);

        fprintf('Critere : %.3f\n',res.conso_lp100km/VD.VEHI.conso_NEDC + ...
            param.alpha/3*(res.CO_gpkm_fwd/VD.TWC.CO_EURO4 + ...
            res.HC_gpkm_fwd/VD.TWC.THC_EURO4 + ...
            res.NOx_gpkm_fwd/VD.TWC.NOx_EURO4));
    else
        fprintf('Conso de reference : %.3f; Cout conso : %.3f\n',param.consoref, ...
            res.conso_lp100km/param.consoref);
        fprintf('Val CO de reference : %.3f; Cout CO : %.3f\n',param.COref, ...
            param.alpha/3*res.CO_gpkm_fwd/param.COref);
        fprintf('Val HC de reference : %.3f; Cout HC : %.3f\n',param.HCref, ...
            param.alpha/3*res.HC_gpkm_fwd/param.HCref);
        fprintf('Val NOx de reference : %.3f; Cout NOx : %.3f\n',param.NOxref, ...
            param.alpha/3*res.NOx_gpkm_fwd/param.NOxref);
            
        fprintf('Critere : %.3f\n',res.conso_lp100km/param.consoref + ...
            param.alpha/3*(res.CO_gpkm_fwd/param.COref + ...
            res.HC_gpkm_fwd/param.HCref + ...
            res.NOx_gpkm_fwd/param.NOxref));
 end

if param.verbose && isempty(javachk('jvm'))
    if ~isfield(param,'MemLog') || param.MemLog ==1
        % Exploitation du log
        [~, name, ~] = fileparts(name);
        newName=regexp(name,'(.*)_1','tokens');
        name_memory = ['mem_' char(newName{1}) '.log'];
        
        [mem, elapsedTime] = parseLog4vehlib(pathname,name_memory);
        
        subplot(2,1,2);
        plot(elapsedTime,mem,'linewidth',2);
        xlabel('CPU time (sec)');
        xlabel('Memory usage');
    end
    
    % Repartition des couples
    figure(1)
    plot(tps_edg,Cmt,'color',[110 70 10]/255,'linewidth',2);
    grid on; xlabel('Time (s)'); ylabel('Torque (Nm)'); legend('T_{ICE}'); title('Torque')
    
    % Temperature et efficacite (arrondies ou non)
    figure(2)
    ax(1) = subplot(2,1,1);
    if param.alpha ~= 0
        %plot(tps_vrtx,T_fwd-273,tps_vrtx,T_rnd-273,'linewidth',2)
        plot(tps_vrtx,T_fwd-273,'linewidth',2)
    else
        plot(tps_vrtx,T_fwd-273,'linewidth',2)
    end
    grid on; ylabel(['Catalyst temperature (',char(176),'C)']);
    hold on;
    plot(t_50_fwd,res.T_fwd.y(find(eff_T_fwd>0.5,1))-273,'*','MarkerSize',10,'linewidth',2);
    text(t_50_fwd+100,res.T_fwd.y(find(eff_T_fwd>0.5,1))-273,'\eta_{T} >50 %');
    title('Catalyst temperature and efficiency');
    ax(2) = subplot(2,1,2);
    if param.alpha  ~= 0
        %plot(tps_vrtx,eff_T_fwd*100,tps_vrtx,eff_T_rnd*100,'linewidth',2)
        plot(tps_vrtx,eff_T_fwd*100,'linewidth',2)
        hold on;
        plot(t_50_fwd,res.eff_T_fwd.y(find(eff_T_fwd>0.5,1))*100,'*','MarkerSize',10,'linewidth',2);
    else
        plot(tps_vrtx,eff_T_fwd*100,'linewidth',2)
        hold on;
        plot(t_50_fwd,res.eff_T_fwd.y(find(eff_T_fwd>0.5,1))*100,'*','MarkerSize',10,'linewidth',2);
    end
    grid on; xlabel('Time (s)'); 
    ylabel('\eta_{T} (%)'); 
%     if param.alpha  ~= 0
%         legend('Unrounded','Rounded'); %title('Impact de l''arrondi de temperature')
%     end
    linkaxes(ax,'x');
    
    % Carto moteur
    fig_pts_fct = figure(3);
    nb_lig = 1; nb_col = 1;
    % seuil d'efficacite pour le catalyseur "chaud"
    seuil = 0.99;
    i_cold = eff_T_fwd<seuil;
    
    N_hot = res.wmt.y*30/pi;
    C_hot = res.Cmt.y;
    N_cold = res.wmt.y(i_cold)*30/pi;
    C_cold = res.Cmt.y(i_cold);
    gr_iso('m_therm_EFF',fig_pts_fct,nb_lig,nb_col,1,N_hot,C_hot,N_cold,C_cold,'s',VD,param,'fr');

        
    % avance allumage
    AA_theo = interp2(VD.MOTH.AA_Padm,VD.MOTH.AA_N,VD.MOTH.AA0_N_Padm',Padm_opti,wmt*30/pi);
    figure(4)
    % Pression admission
    %figure()
    
    subplot(3,1,1)
    plot(tps_edg,Padm_opti/1000,'linewidth',2);
    xlabel('Temps (s)'); ylabel('P_{intake} (B)');
    grid on;
    title('Control parameters');
    
    subplot(3,1,2)
    plot(tps_edg,delta_AA_opti,'linewidth',2);%hold on;plot(tps_edg,AA_theo,'k--','linewidth',2);
    xlabel('Temps (s)'); ylabel(['\Delta SA (',char(176),'BTDC)']); %legend('delta AA')%,'AA opti')
    grid on; %yticks(17:3:29)
    % Richesse
    %figure()
    subplot(3,1,3);
    plot(tps_edg,Phi_opti,'linewidth',2);
    xlabel('Time (s)'); ylabel('\Phi');
    grid on;
    
    
    % Emissions cumulees
    figure(5)
    subplot(3,1,1);
    plot(res.CO_fwd.x,res.CO_fwd.y,'linewidth',2);
    hold on
    plot(t_50_fwd,res.CO_fwd.y(find(eff_T_fwd>0.5,1)),'*','MarkerSize',10,'linewidth',2);
    text(t_50_fwd,2*res.CO_fwd.y(find(eff_T_fwd>0.5,1))/3,'\eta_{T} >50 %');
    grid on;
    %ylabel([res.CO_fwd.label,' (',res.CO_fwd.unit,')']);
    ylabel('CO (g)');
    title('Cumulative emissions')
    subplot(3,1,2);
    plot(res.HC_fwd.x,res.HC_fwd.y,'linewidth',2);
    hold on
    plot(t_50_fwd,res.HC_fwd.y(find(eff_T_fwd>0.5,1)),'*','MarkerSize',10,'linewidth',2);
    grid on;
    %ylabel([res.HC_fwd.label,' (',res.HC_fwd.unit,')']);
    ylabel('HC (g)');
    subplot(3,1,3);
    plot(res.NOx_fwd.x,res.NOx_fwd.y,'linewidth',2);
    hold on
    plot(t_50_fwd,res.NOx_fwd.y(find(eff_T_fwd>0.5,1)),'*','MarkerSize',10,'linewidth',2);
    grid on;
    %ylabel([res.NOx_fwd.label,' (',res.NOx_fwd.unit,')']);
    ylabel('NOx (g)');
    xlabel('Time (s)');
end

end
