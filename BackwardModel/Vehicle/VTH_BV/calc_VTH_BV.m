
% function [ERR, VD, ResXml]=calc_VTH_BV(ERR,vehlib,param,VD)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Calcul d'un vehicule conventionnel dans VEHLIB en mode backward
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (BJ) : version 1
%

function [ERR, VD, ResXml,res]=calc_VTH_BV(ERR,vehlib,param,VD,Xml)

if nargin==4
    Xml=1;
end

% Calcul cinematiques complementaires
% Avec re-echantillonnage eventuel
if ~isfield(param,'pas_temps') | param.pas_temps==0
    param.pas_temps=1; % si param_pas temps n'existe pas ou =0 on reechantillone ? 1s
end

[ERR,VD,dist]=calc_cinemat(ERR,VD,param);

distance=cumtrapz(VD.CYCL.temps,VD.CYCL.vitesse);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,penteFpk,fres,faero,froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Connexion
csec_red=croue;
wsec_red=wroue;
dwsec_red=dwroue;

% Calcul des conditions en amont du reducteur
[ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Connexion
csec_bv=cprim_red;
wsec_bv=wprim_red;
dwsec_bv=dwprim_red;

if VD.CYCL.ntypcin==3 % rapport de boite imposes par la cinematique
    indice=find(csec_bv>0 & VD.CYCL.rappvit ==0);
    if indice
    disp(['calc_VTH_BV: Point Mort impossible aux indices: ', num2str(indice)]);
        VD.CYCL.rappvit(indice)=VD.CYCL.rappvit(indice)+1;
    end
end
    
% Calcul des conditions en amont de la boite de vitesse
[ERR,cprim_bv,wprim_bv,dwprim_bv]=calc_bv(VD,csec_bv,wsec_bv,dwsec_bv,0);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end
if VD.CYCL.ntypcin == 3
    % Gestion des sur regimes si le cycle est mal ecrit/vehicule pas adapte
    indice_surregime=find(wprim_bv>VD.MOTH.wmt_maxi);
    while indice_surregime
        VD.CYCL.rappvit(indice_surregime)=VD.CYCL.rappvit(indice_surregime)+1;
        VD.CYCL.rappvit(indice_surregime)=min(VD.ECU.nbmax_rapbv,VD.CYCL.rappvit(indice_surregime));
        wprim_bv(indice_surregime)=VD.BV.k(VD.CYCL.rappvit(indice_surregime)).*wsec_bv(indice_surregime);
        cprim_bv(indice_surregime)=csec_bv(indice_surregime)./(VD.BV.k(VD.CYCL.rappvit(indice_surregime)).*VD.BV.Ro(VD.CYCL.rappvit(indice_surregime)));
        indice_surregime=find(wprim_bv>VD.MOTH.wmt_maxi);
        if(sum(VD.CYCL.rappvit(indice_surregime))==VD.ECU.nbmax_rapbv*length(VD.CYCL.rappvit(indice_surregime)))
            % over speed even with maximum gear number - No solution
            break
        end
    end
end

% Connexion
csec_emb1=cprim_bv;
wsec_emb1=wprim_bv;
dwsec_emb1=dwprim_bv;
[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,ones(size(cprim_bv)));
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Connexion
wsec_belt1=wprim_emb1;
dwsec_belt1=dwprim_emb1;

% Calcul des conditions sur la courroie d'auxiliaires
[ERR,csec_belt1,csec_belt2]=calc_accm2(VD,wsec_belt1,dwsec_belt1,0);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Connexion
wmt=wprim_emb1;
dwmt=dwprim_emb1;

cmt=cprim_emb1-csec_belt1-csec_belt2+VD.MOTH.J_mt.*dwmt;
cmt_noSat = cmt;
% Calcul des conditions de fonctionnement du moteur thermique
if VD.MOTH.ntypmoth == 2 || VD.MOTH.ntypmoth == 3
    [ERR,dcarb,cmt]=calc_mt_niv0(ERR,VD.MOTH,cmt,wmt,dwmt,ones(size(cmt)),0);
elseif  VD.MOTH.ntypmoth == 10
    if isfield(param,'prec_num_mt')
        [ERR,dcarb,cmt,~,~,cmt_ind]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,ones(size(cmt)),0,0,0,param.prec_num_mt);
    else
        [ERR,dcarb,cmt,~,~,cmt_ind]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,ones(size(cmt)),0,0,0,1e-10);
    end
else
    ERR=MException('BackwardModel:calc_VTH_BV','Type de moteur non reconnu.');
    ResXml=struct([]);
    return;
end

if VD.CYCL.ntypcin==1
    % Choix du rapport de boite de vitesse si necessaire
    if isfield(param,'rappvit_bv_max') && param.rappvit_bv_max==1 % On choisie le rapport max possible
        [~,i_maxi]=max(~isnan(flipud(dcarb)));
        rappvit_bv=size(dcarb,1)-i_maxi;
        [cmt,wmt,dwmt,dcarb]=mat_ind(rappvit_bv+1,2,cmt,wmt,dwmt,dcarb);
    else
        [ERR,rappvit_bv,wmt,cmt,dwmt,elhyb,dcarb]=calc_rappvit_bv(VD,param,cmt,wmt,dwmt,ones(size(cmt)),dcarb,0);
        if ~isempty(ERR)
            ResXml=struct([]);
            return;
        end
    end
    
    VD.CYCL.rappvit=rappvit_bv;
    % Reconstitution des grandeurs pour le bon rapport de boite
    [sx, sy]=size(wprim_emb1);
    ii=0:(sy-1);
    indice=rappvit_bv+1;
    j=indice+ii*sx;
    wprim_emb1=wprim_emb1(j);
    dwprim_emb1=dwprim_emb1(j);
    cprim_emb1=cprim_emb1(j);
    wsec_emb1=wsec_emb1(j);
    dwsec_emb1=dwsec_emb1(j);
    csec_emb1=csec_emb1(j);
    wprim_bv=wprim_bv(j);
    dwprim_bv=dwprim_bv(j);
    cprim_bv=cprim_bv(j);
else
    % Rapports imposes
    rappvit_bv=VD.CYCL.rappvit;
    elhyb=ones(size(cmt));
end

indice_inf=find(isnan(dcarb));
if indice_inf
    if ~isfield(param,'verbose') || param.verbose>=0
        disp(['calc_VTH_BV: points de conso infinie aux indices: ', num2str(indice_inf)]);
    end
    choix=1; % pour saturer les couples et faire les calculs tout de meme
    if isfield(param,'extrap_conso') && param.extrap_conso == 1
        if VD.MOTH.ntypmoth == 10
            [INTP.N1, INTP.C1] = ndgrid(VD.MOTH.Reg_2dconso, VD.MOTH.Cpl_2dconso);
            INTP.F = griddedInterpolant(INTP.N1, INTP.C1, VD.MOTH.Conso_2d, 'linear', 'linear');
            INTP.verbose = 1; % Pour afficher la ligne d'extrapolation des indices
            VD.INTP=INTP;
            [cmt, dcarb] = calc_extrap_conso(VD, param, wmt, cmt, cmt_noSat, dcarb);
        elseif VD.MOTH.ntypmoth == 3
            [cmt, dcarb] = calc_extrap_conso(VD, param, wmt, cmt, cmt_noSat, dcarb);
        end
    elseif choix==0
        ResXml=struct([]);
        return;
    elseif choix==1
        if VD.MOTH.ntypmoth == 10
            cmt(indice_inf)=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt(indice_inf));
            if isfield(param,'prec_num_mt')
                [ERR,dcarb,cmt,~,~,cmt_ind]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,ones(size(cmt)),0,0,0,param.prec_num_mt);
            else
                [ERR,dcarb,cmt,~,~,cmt_ind]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,elhyb,0);
            end
        elseif VD.MOTH.ntypmoth == 3
            N = wmt(indice_inf)*30/pi;
            Cmax = (VD.MOTH.Tmax_N2*N.^2 + VD.MOTH.Tmax_N*N + VD.MOTH.Tmax_cst)*VD.MOTH.Tmax/VD.MOTH.Tmax_Tref;
            % Limitation du couple maxi
            cmt(indice_inf) = Cmax;
            [ERR,dcarb,cmt]=calc_mt_niv0(ERR,VD.MOTH,cmt,wmt,dwmt,ones(size(cmt)),0);           
        end
    end
end

if VD.CYCL.ntypcin == 1
    % En rapport optimises, affectation de la valeur des auxiliaires
    indice=(0:length(VD.CYCL.temps)-1)*length(csec_belt1(:,1));
    caccm1=csec_belt1(rappvit_bv+1+indice);
    caccm2=csec_belt2(rappvit_bv+1+indice);
    waccm1=wsec_belt1(rappvit_bv+1+indice);
    waccm2=waccm1;
else
    caccm1=csec_belt1;
    caccm2=csec_belt2;
    waccm1=wsec_belt1;
    waccm2=waccm1;
end
pacc=(caccm1+caccm2).*waccm1;

% Recalcul du frein meca au niveau de la roue
cprim_emb1=cmt+caccm1+caccm2-VD.MOTH.J_mt.*dwmt;
wprim_emb1=wmt;
dwprim_emb1=dwmt;
csec_emb1=cprim_emb1;
cprim_bv=csec_emb1;
wprim_bv=wsec_emb1;
dwprim_bv=dwsec_emb1;
[ERR,csec_bv,wsec_bv,dwsec_bv,~]=calc_bv_fw(ERR,VD,cprim_bv,wprim_bv,dwprim_bv,0,rappvit_bv);
cprim_red=csec_bv;
wprim_red=wsec_bv;
dwprim_red=dwsec_bv;
[ERR,csec_red,~]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
cfrein_meca=csec_red-croue;

co=zeros(size(wmt))*NaN;
hc=zeros(size(wmt))*NaN;
nox=zeros(size(wmt))*NaN;
if isfield(VD,'TWC')
    if isfield(VD.MOTH,'emissionModel') && VD.MOTH.emissionModel == 1 ...
    && isfield(VD.TWC,'ntypcata') && (VD.TWC.ntypcata == 1 || VD.TWC.ntypcata == 2)
        % Calcul des emissions moteur
        Tcat=zeros(size(wmt))*NaN;
        Tcatg=zeros(size(wmt))*NaN;
        Tcoll=zeros(size(wmt))*NaN;
        Texh=zeros(size(wmt))*NaN;
        co_eff=zeros(size(wmt))*NaN;
        hc_eff=zeros(size(wmt))*NaN;
        nox_eff=zeros(size(wmt))*NaN;
        
        % Initialization
        if isfield(VD.INIT,'Tcat0')
            TcatIni=VD.INIT.Tcat0+273;
        else
            TcatIni=VD.INIT.Tcat0+273;
        end
        Tcoll(1)=TcatIni;
        Tcat(1)=TcatIni;
        Tcatg(1)=TcatIni;
        Texh(1)=TcatIni;
        
        co_eff(1)=0;
        hc_eff(1)=0;
        nox_eff(1)=0;
        co(1)=0;
        hc(1)=0;
        nox(1)=0;
        for itime=2:length(VD.CYCL.temps)
            [ERR, Tcat(itime),  Tcatg(itime), Texh(itime), Tcoll(itime), ...
                co_eff(itime), hc_eff(itime), nox_eff(itime), co(itime), hc(itime), nox(itime)]= ...
                calc_exhaust_emissions(ERR, param, VD, itime, wmt(itime), cmt(itime), dcarb(itime), ...
                Tcat(itime-1),Tcatg(itime-1), Tcoll(itime-1), co_eff(itime-1), hc_eff(itime-1), nox_eff(itime-1), ...
                elhyb(itime));
        end
    end
end

if isfield(param,'calc_vit') && param.calc_vit == 1
    % Calcul Selon EcoDriving - on re affecte la vitesse d'origine a la structure CYCL
    VD.CYCL.vitesse_calc=VD.CYCL.vitesse;
    VD.CYCL.vitesse=VD.CYCL.vitesse_origine;
    VD.CYCL=rmfield(VD.CYCL,'vitesse_origine');
end

% Cycle
tsim=VD.CYCL.temps;
vit=VD.CYCL.vitesse;
acc=VD.CYCL.accel;
vit_dem=vit;
distance=VD.CYCL.distance;
pente=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,distance,'linear',0); % reechantillonage pente fonction distance calculee.     
cinertie=Jveh.*VD.CYCL.accel/VD.VEHI.Rpneu;
pinertie_emb1=VD.MOTH.J_mt.*dwmt.*wmt;
cprim_emb1_sans_inertie=cprim_emb1+VD.MOTH.J_mt.*dwmt; % !!! Pour les flux d'energie du post-traitement
glisst_emb1=zeros(size(VD.CYCL.vitesse));

% Construction de la structure xml de resultats
if Xml==1
    [ResXml]=miseEnForme4VEHLIB(vehlib,'caller');
    
    % Calcul de resultats synthetiques
    [Res]=resultats(VD,param,ResXml);
    conso100=Res.conso100;
    cumcarb=Res.cumcarb;
    co2_eq=Res.co2_eq;
    co2=Res.CO2;
    conso=Res.conso;
    if isfield(VD,'TWC')
        co_gkm=Res.co_gkm;
        hc_gkm=Res.hc_gkm;
        nox_gkm=Res.nox_gkm;
    end
    
    % Bilan de puissance et d'energie
    [bp]= bilanPE(VD,param,ResXml);
    bilanP=bp.bilanP;
    assignin('base','bilanP',bilanP);
    assignin('base','bp',bp);
    
    % Ajout des grandeurs synthétiques dans une nouvelle table de la structure xml
    [ResXml]=miseEnForme4GestionEnergie(ResXml);
    res=[];
else
    ResXml=[];
     %[ResXml]=miseEnForme4VEHLIB(vehlib,'caller');
    %[ERR,res]=affectVehlibStruct2Workspace(ResXml,'caller');
    res.cumcarb=cumsum(VD.CYCL.pas_temps.*dcarb);
    res.conso=res.cumcarb/VD.MOTH.dens_carb;
    res.co2_eq=44*res.cumcarb(end)./(12+VD.MOTH.CO2carb);
    res.CO2_gkm=res.co2_eq/(VD.CYCL.distance(end)/1000);
    res.CO2=44*dcarb./(12+VD.MOTH.CO2carb);
    res.conso100=100.*res.cumcarb(end)./VD.MOTH.dens_carb./(VD.CYCL.distance(end)/1000);
end

%fprintf(1,'%s %.1f %s\n','La Csp moyenne : ',trapz(tsim,dcarb)/bp.Earbremt*1000,'g/k.Wh');

if param.verbose>=10
    trait_vth_bv;
end
