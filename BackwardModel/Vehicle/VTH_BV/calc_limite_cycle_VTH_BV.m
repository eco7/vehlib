% function [ERR, VD, ResXml]=calc_VTH_BV(ERR,vehlib,param,VD)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Calcul d'un vehicule conventionnel dans VEHLIB en mode backward
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (BJ) : version 1
%

function [ERR,VD]=calc_limite_cycle_VTH_BV(ERR,vehlib,param,VD,rl)

if nargin==5 & rl==1 % Cas roue libre
    
    [ERR,VD,dist]=calc_cinemat(ERR,VD,param);
    [ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
    I1=find(VD.CYCL.distance>VD.CYCL.D1,1,'first');
    I1=I1-1;
    vit_dem(1)=VD.CYCL.vitesse(I1);
    dist_rl(1)=VD.CYCL.distance(I1);
    vit_rl(1)=VD.CYCL.vitesse(I1);
    jj=1;
    pas_temps=VD.CYCL.pas_temps(1);
    [~,I_unique]=unique(VD.CYCL.distance);
    while (vit_rl(jj)<vit_dem(1) | jj==1) & dist_rl(jj)<dist
        jj=jj+1;
        [~,vit_rl(jj),~,Fres_rl,Faero_rl,Froul_rl,Fpente_rl]=calc_efforts_vehicule_fw(ERR,VD,dist_rl(jj-1),vit_rl(jj-1),masse(1),Jveh(1),0,pas_temps);
        dist_rl(jj)=dist_rl(jj-1)+(vit_rl(jj))*pas_temps;
        %vit_dem(jj)=interp1(VD.CYCL.distance(I_unique),VD.CYCL.vitesse(I_unique),dist_rl(jj));
    end
    
    if dist_rl(end)>dist
        vit_n=[VD.CYCL.vitesse(1:I1) vit_rl(2:end)];
        dist_n=[VD.CYCL.distance(1:I1) dist_rl(2:end)];
        rappvit_n=[VD.CYCL.rappvit(1:I1) zeros(1,length(vit_rl)-1)];
        temps_n=pas_temps*(0:1:length(dist_n)-1);
        Iend=length(dist_rl);
    else
        % On vient rechercher à quel distance on as rejoint la vitesse de consigne
        Iend=find(VD.CYCL.distance >(VD.CYCL.distance(I1) + dist_rl(end)-dist_rl(1)),1,'first');
        if vit_rl(end)> VD.CYCL.vitesse(Iend)
            vit_n=[VD.CYCL.vitesse(1:I1) vit_rl(2:end-1)  VD.CYCL.vitesse(Iend:end) ];
            dist_n=[VD.CYCL.distance(1:I1) dist_rl(2:end-1)  VD.CYCL.distance(Iend:end)];
            rappvit_n=[VD.CYCL.rappvit(1:I1) zeros(1,length(vit_rl)-2)  VD.CYCL.rappvit(Iend:end)];
            temps_n=pas_temps*(0:1:length(dist_n)-1);
        else
            vit_n=[VD.CYCL.vitesse(1:I1) vit_rl(2:end)  VD.CYCL.vitesse(Iend:end) ];
            dist_n=[VD.CYCL.distance(1:I1) dist_rl(2:end)  VD.CYCL.distance(Iend:end)];
            rappvit_n=[VD.CYCL.rappvit(1:I1) zeros(1,length(vit_rl)-1)  VD.CYCL.rappvit(Iend:end)];
            temps_n=pas_temps*(0:1:length(dist_n)-1);
        end
        
    end
    VD.CYCL.temps=temps_n;
    VD.CYCL.vitesse=vit_n;
    VD.CYCL.distance=dist_n;
    VD.CYCL.tmax=VD.CYCL.temps(end);
    VD.CYCL.rappvit=rappvit_n;
        
    return
end

I=1;
nb_it1=0;
while ~isempty(I) && I(1)~=length(VD.CYCL.temps)
    nb_it1=nb_it1+1;
    if nb_it1>=1000
        'alerte1'
        pause
    end
    if ~isfield(param,'pas_temps') | param.pas_temps==0
        param.pas_temps=1; % si param_pas temps n'existe pas ou =0 on reechantillone ? 1s
    end
    [ERR,VD,dist]=calc_cinemat(ERR,VD,param);
    if param.calc_accel == 2
        VD.CYCL.accel(end)=VD.CYCL.accel(end-1);
    end
    % Calcul masse vehicule et inertie
    [ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
    
    % Calcul des efforts a la roue
    [ERR,croue,wroue,dwroue,penteFpk,fres,faero,froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
    
    % Connexion
    csec_red=croue;
    wsec_red=wroue;
    dwsec_red=dwroue;
    
    % Calcul des conditions en amont du reducteur
    [ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
    
    % Connexion
    csec_bv=cprim_red;
    wsec_bv=wprim_red;
    dwsec_bv=dwprim_red;
    
    
    % Calcul des conditions en amont de la boite de vitesse
    [ERR,cprim_bv,wprim_bv,dwprim_bv]=calc_bv(VD,csec_bv,wsec_bv,dwsec_bv,0);
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
    if VD.CYCL.ntypcin == 3
        % Gestion des sur regimes si le cycle est mal ecrit
        indice_surregime=find(wprim_bv>VD.MOTH.wmt_maxi);
        if indice_surregime
            VD.CYCL.rappvit(indice_surregime)=VD.CYCL.rappvit(indice_surregime)+1;
            wprim_bv(indice_surregime)=VD.BV.k(VD.CYCL.rappvit(indice_surregime)).*wsec_bv(indice_surregime);
            cprim_bv(indice_surregime)=csec_bv(indice_surregime)./(VD.BV.k(VD.CYCL.rappvit(indice_surregime)).*VD.BV.Ro(VD.CYCL.rappvit(indice_surregime)));
        end
    end
    
    % Connexion
    csec_emb1=cprim_bv;
    wsec_emb1=wprim_bv;
    dwsec_emb1=dwprim_bv;
    [ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,ones(size(cprim_bv)));
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
    
    % Connexion
    wsec_belt1=wprim_emb1;
    dwsec_belt1=dwprim_emb1;
    
    % Calcul des conditions sur la courroie d'auxiliaires
    [ERR,csec_belt1,csec_belt2]=calc_accm2(VD,wsec_belt1,dwsec_belt1,0);
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
    
    % Connexion
    wmt=wprim_emb1;
    dwmt=dwprim_emb1;
    
    cmt=cprim_emb1-csec_belt1-csec_belt2+VD.MOTH.J_mt.*dwmt;
    
    % calcul couple max moteur thermique
    cmt_max_mat=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt);
    
    %% recherche des indices ou l'on ne passe pas le cycle
    prec_num=1e-5;
    if isvector(cmt)
         I=find((cmt_max_mat+prec_num>cmt)==0);
    else
        I=find(sum(cmt_max_mat+prec_num>cmt)==0);
    end
    
    if isempty(I) || I(1)==length(cmt)% on passe le cycle si I isempty ou correpond au dernier point du cycle
        temps_n=NaN;
        vit_n=NaN;
        dist_n=NaN;
        return
        
    else % On reppase en fw on vient modifier le cycle
%        warning('Modification du cycle');
        
        I1=I(1);
        % Si raport libre on détermine le rapport de boite à i(1)-1
        %rappvit_fw(1)=16;
        vit_fw=VD.CYCL.vitesse(I1);
        vit_dem(1)=VD.CYCL.vitesse(I1);
        dist_fw=VD.CYCL.distance(I1);
        rappvit_fw=[];
        dvit_fw=[];
        wroue=[];
        wmt_fw=[];
        
        jj=1;
        % Tant que la vitesse n'as pas redépasser la vitesse cible on reste en fw
        nb_it2=0;
        while jj==1 || ( vit_fw(jj)<vit_dem(jj) && dist_fw(jj)<dist ); %% Vitesse fonction de la distance et plus du temps !!!
            % On choisit le point de puissance max pour le moteur
            nb_it2=nb_it2+1;
            if nb_it2>=10000
                'alerte2'
                pause
            end
            if jj==1
                if I1~=1
                    %wmt_vec=wmt(2:end,I1-1);
                     wmt_vec=wmt(2:end,I1);
                else
                    wmt_vec=wmt(2:end,1);
                end
            else
                wmt_vec=wroue(jj)*VD.RED.kred*VD.BV.k';
            end
            
            jj=jj+1;
            
            cmt_max_vec=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt_vec).*(wmt_vec<VD.MOTH.wmt_maxi);
            pas_temps= VD.CYCL.pas_temps(1);
            pmt_max_vec=wmt_vec.*cmt_max_vec;
            [~,icmt_max]=max(pmt_max_vec);
            rappvit_fw(jj)=icmt_max;
            cmt_fw=cmt_max_vec(icmt_max);
            cmt_fw_vec(jj)= cmt_fw;
            icmt_max_vec(jj)=icmt_max;
            % calcul du couple à la roue
            wsec_belt1=wmt_vec;
            dwsec_belt1=0;
            [ERR,csec_belt1,csec_belt2]=calc_accm2(VD,wsec_belt1,dwsec_belt1,0);
            
            
            cprim_bv_fw=cmt_fw+csec_belt1(icmt_max)+csec_belt2(icmt_max);
            csec_belt1_vec(jj)=csec_belt1(icmt_max);
            Jprim_bv_fw=VD.MOTH.J_mt;
            wprim_bv_fw=wmt_vec(icmt_max); % Normalement on s'en tape des vitesses
            [ERR,csec_bv_fw,~,~,Jsec_bv_fw]=calc_bv_fw(ERR,VD,cprim_bv_fw,wprim_bv_fw,zeros(size(cprim_bv_fw)),Jprim_bv_fw,rappvit_fw(jj));
            [ERR,csec_red_fw,~,~,Jsec_red_fw,~]=calc_red_fw(ERR,VD.RED,csec_bv_fw,ones(size(csec_bv_fw)),ones(size(csec_bv_fw)),Jsec_bv_fw);
            
            % Calcul de la vitesse du vehicule
            % connexion
            croue_fw=csec_red_fw;
            Jveh1= Jveh(1) + Jsec_red_fw; % on ramene l'inertie a l'arbre des roues
            [~,vit_fw(jj),~,Fres(jj),Faero(jj),Froul(jj),Fpente(jj)]=calc_efforts_vehicule_fw(ERR,VD,dist_fw(jj-1),vit_fw(jj-1),masse(1),Jveh1,croue_fw,pas_temps);
            
            % Recalcul de la distance parcourue et des vitesses de rotation
            dist_fw(jj)=dist_fw(jj-1)+(vit_fw(jj))*pas_temps;
            wroue(jj)=vit_fw(jj)/VD.VEHI.Rpneu;
            wmt_fw(jj)=wroue(jj)*VD.RED.kred*VD.BV.k(rappvit_fw(jj));
            %croue_vec(jj)=croue_fw-Jsec_red_fw*(wroue(jj)-wroue(jj-1))./pas_temps;
            %croue_vec1(jj)=Fres(jj)*VD.VEHI.Rpneu+Jveh*(wroue(jj)-wroue(jj-1))./pas_temps;
            %dwroue_vec(jj)=(wroue(jj)-wroue(jj-1))/pas_temps;
            %cmt_vec_r(jj)=croue_vec(jj)/VD.RED.kred/VD.RED.rend/VD.BV.k(icmt_max)/VD.BV.Ro(icmt_max)-csec_belt1_vec(jj)+VD.MOTH.J_mt*dwroue_vec(jj)*VD.RED.kred*VD.BV.k(icmt_max);
            
            [~,I_unique]=unique(VD.CYCL.distance);
            vit_dem(jj)=interp1(VD.CYCL.distance(I_unique),VD.CYCL.vitesse(I_unique),dist_fw(jj));
            cmt_vec(jj)=cmt_fw;
            
        end
        
        temps_fw=(1:1:length(vit_fw))*pas_temps+VD.CYCL.temps(I1);
        
        
        if dist_fw(end)>dist
            vit_n=[VD.CYCL.vitesse(1:I1) vit_fw(2:end)];
            dist_n=[VD.CYCL.distance(1:I1) dist_fw(2:end)];
            temps_n=pas_temps*(0:1:length(dist_n)-1);
        else
            % On vient rechercher à quel distance on as rejoint la vitesse de consigne
            Iend=find(VD.CYCL.distance >(VD.CYCL.distance(I1) + dist_fw(end)-dist_fw(1)),1,'first');
            if vit_fw(end)> VD.CYCL.vitesse(Iend)
                vit_n=[VD.CYCL.vitesse(1:I1) vit_fw(2:end-1)  VD.CYCL.vitesse(Iend:end) ];
                dist_n=[VD.CYCL.distance(1:I1) dist_fw(2:end-1)  VD.CYCL.distance(Iend:end)];
                temps_n=pas_temps*(0:1:length(dist_n)-1);
            else
                vit_n=[VD.CYCL.vitesse(1:I1) vit_fw(2:end)  VD.CYCL.vitesse(Iend:end) ];
                dist_n=[VD.CYCL.distance(1:I1) dist_fw(2:end)  VD.CYCL.distance(Iend:end)];
                temps_n=pas_temps*(0:1:length(dist_n)-1);
            end
            
        end
        VD.CYCL.temps=temps_n;
        VD.CYCL.vitesse=vit_n;
        VD.CYCL.distance=dist_n;
        VD.CYCL.tmax=VD.CYCL.temps(end);
       
    end
end















