% Function: calc_VTH_CPLHYD
% Calcul du vehicule conventionnel avec coupleur hydraulique
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Calcul d'un vehicule conventionnel dans VEHLIB en mode backward
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (BJ) : version 1
%

function [ERR,VD,ResXml]=calc_VTH_CPLHYD(ERR,vehlib,param,VD)

% Calcul cinematiques complementaires
% Avec re-echantillonnage eventuel
if ~isfield(param,'pas_temps') | param.pas_temps==0
    param.pas_temps=1; % si param_pas temps n'existe pas ou =0 on reechantillone ? 1s
end
[ERR,VD,dist]=calc_cinemat(ERR,VD,param);
distance=cumtrapz(VD.CYCL.temps,VD.CYCL.vitesse);

% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');

% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,penteFpk,fres,faero,froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Connexion
csec_red=croue;
wsec_red=wroue;
dwsec_red=dwroue;
% Calcul des conditions en amont du reducteur
[ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);

% Connexion
csec_bv=cprim_red;
wsec_bv=wprim_red;
dwsec_bv=dwprim_red;

if VD.CYCL.ntypcin==3 % rapport de boite imposes par la cinematique
    indice=find(csec_bv>0 & VD.CYCL.rappvit ==0);
    if indice
    disp(['calc_VTH_CPLHYD: Point Mort impossible aux indices: ', num2str(indice)]);
        VD.CYCL.rappvit(indice)=VD.CYCL.rappvit(indice)+1;
    end
end

% Calcul des conditions en amont de la boite de vitesse
[ERR,cprim_bv,wprim_bv,dwprim_bv]=calc_bv(VD,csec_bv,wsec_bv,dwsec_bv,0);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Connexion
csec_cplhyd=cprim_bv;
wsec_cplhyd=wprim_bv;
dwsec_cplhyd=dwprim_bv;
% Calcul des conditions de fonctionnement du coupleur hydraulique en premiere vitesse
    % Calcul des conditions de fonctionnement du coupleur hydraulique en premiere vitesse
    [ERR,cprim_cplhyd,wprim_cplhyd,dwprim_cplhyd,etat_cplhyd]=calc_cplhyd(ERR,VD,csec_cplhyd,wsec_cplhyd,dwsec_cplhyd,ones(size(cprim_bv)));
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Connexion
wsec_belt1=wprim_cplhyd;
dwsec_belt1=dwprim_cplhyd;
% Calcul des conditions sur la courroie d'auxiliaires
[ERR,csec_belt1,csec_belt2]=calc_accm3(ERR,VD,wsec_belt1,dwsec_belt1,0);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Connexion
% les inerties moteur et etage entree (pompe) sont prises en compte maintenant
cmt=cprim_cplhyd-csec_belt1-csec_belt2+(VD.CONV.Js+VD.CONV.Je+VD.MOTH.J_mt).*dwprim_cplhyd;
wmt=wprim_cplhyd;
dwmt=dwprim_cplhyd;
% Calcul des conditions de fonctionnement du moteur thermique
[ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,ones(size(wmt)),0);

if VD.CYCL.ntypcin==1
    % Choix du rapport de boite de vitesse si necessaire
    [ERR,rappvit,wmt,cmt,dwmt,dcarb,elhyb,etat_cplhyd]=calc_rappvit_cplhyd(ERR,VD,param,cmt,wmt,dwmt,dcarb,ones(size(cprim_bv)),etat_cplhyd,0);
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
    if sum(isnan(etat_cplhyd))
        indice_NaN_cplhyd=find(isnan(etat_cplhyd));
        disp(['Probleme de calcul des conditions du coupleur hydraulique aux indices: ',num2str(indice_NaN_cplhyd)]);
    end
else
    % Rapports imposes
    rappvit=VD.CYCL.rappvit;
    elhyb=ones(size(cmt));
end

%if ~isempty(ERR) calc_mt ne renvoie plus d'erreur si un couple est > au couple maxi, mais des NaN
if sum(isnan(dcarb))
    % Normalement un probleme de dynamique vehicule
    choix=1; % Choix optionnel
    if choix==0
        % On sort en erreur
        ResXml=struct([]);
        return;
    elseif choix==1
        % On sature le couple au maxi de la carto
        indice=find(isnan(dcarb));
        disp(['calc_VTH_CPLHYD: Probleme de calcul du moteur thermique aux indices: ',num2str(indice)]);
        wmt(indice)=max(min(wmt(indice),VD.MOTH.wmt_maxi),VD.MOTH.ral);
        cmt(indice)=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt(indice));
        [ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,elhyb,0);
    elseif choix==2
        % On retrograde si possible pour avoir plus de puissance
        indice=find(cmt>interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt));
        cmt(indice)=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt(indice));
        [ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,elhyb,0);
    end
end

% Cycle
tsim=VD.CYCL.temps;
vit=VD.CYCL.vitesse;
acc=VD.CYCL.accel;
vit_dem=vit;
rappvit_bv=rappvit;
distance=VD.CYCL.distance;
pente=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,distance,'linear',0); % reechantillonage pente fonction distance claculee.     

if VD.CYCL.ntypcin == 1
    % En rapport optimises, affectation de la valeur des auxiliaires
    indice=(0:length(VD.CYCL.temps)-1)*length(csec_belt1(:,1));
    caccm1=csec_belt1(rappvit_bv+1+indice);
    caccm2=csec_belt2(rappvit_bv+1+indice);
    waccm1=wsec_belt1(rappvit_bv+1+indice);
else
    caccm1=csec_belt1;
    caccm2=csec_belt2;
    waccm1=wsec_belt1;
end
pacc=(caccm1+caccm2).*waccm1;

% Renseignement de la structure xml
[ResXml]=miseEnForme4VEHLIB(vehlib,'caller');

% Calcul de resultats synthetiques
[Res]=resultats(VD,param,ResXml);
conso100=Res.conso100;
cumcarb=Res.cumcarb;
co2_eq=Res.co2_eq;
CO2_gkm=Res.CO2_gkm;

% Ajout des grandeurs synthétiques dans une nouvelle table de la structure xml
[ResXml]=miseEnForme4GestionEnergie(ResXml);


