% function [ERR, VD, ResXml]=calc_HP_BV_1EMB_TWC(ERR,vehlib,param,VD)
%
% Copyright IFSTTAR LTE 1999-2011
%
% Objet:
% Calcul d'un vehicule hybride parallele simple arbre
% dans VEHLIB en mode backward
%
% Arguments appel :
% Arguments de sortie :
%
function [ERR, VD, ResXml]=calc_HP_BV_1EMB(ERR,vehlib,param,VD)
format long

% Initialisation
ResXml=struct([]);

% Test des parametres de lagrange
if strcmpi(param.optim,'lagrange')
    if isfield(VD,'TWC')
        % We can run the model with TWC but with only lambda1
        if (isfield(param,'lambda2') &&  ~isfield(param,'K')) || ...
                (~isfield(param,'lambda2') &&  isfield(param,'K'))
            ERR=MException('BackwardModel:calc_HP_BV_1EMB', ...
                'parametres de lagrange incoherent');
            return;
            
        end
    end
    if ~isfield(param,'lambda1')
        ERR=MException('BackwardModel:calc_HP_BV_1EMB', ...
            'parametres de lagrange incoherent');
        return;
    end
end

% Calcul cinematiques complementaires
% Avec re-echantillonnage eventuel
if ~isfield(param,'pas_temps') | param.pas_temps==0
    param.pas_temps=1;
end
[ERR,VD,dist]=calc_cinemat(ERR,VD,param);


if ~isempty(ERR)
    return;
end

% Initialisation
soc=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));
E=zeros(size(VD.CYCL.temps));
R=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
pbat=zeros(size(VD.CYCL.temps));

% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
if ~isempty(ERR)
    return;
end

% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,penteFpk,fres,faero,froul,fpente,force1,force2,force3]= ...
    calc_efforts_vehicule(ERR,VD,masse,Jveh);

% Dans certain cycle CIN_ARTURB_BV_classe3 par exemple, il peut arriver
%que croue>0 alors que on est au point mort, impossible avec des archi avec
%BV on met alors le couple a zero

if VD.CYCL.ntypcin==3
    ind_croue_pm=find(croue>0 & VD.CYCL.rappvit==0);
    if ~isempty(ind_croue_pm)
        chaine=strcat('in some point, a positive wheel torque exist with neutral gear box position \n corresponding time :',num2str(ind_croue_pm),'\n wheel torque fixe to zero in these cases');
        warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine);
        croue(ind_croue_pm)=0;
    end
end

if ~isempty(ERR)
    return;
end

% Connexion
csec_red=croue;
wsec_red=wroue;
dwsec_red=dwroue;

% Calcul des conditions en amont du reducteur
[ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);
if ~isempty(ERR)
    return;
end

% Connexion
csec_bv=cprim_red;
wsec_bv=wprim_red;
dwsec_bv=dwprim_red;

% Calcul des conditions en amont de la boite de vitesse
% NB si on est en rapport libre ou optimaux on a des matrices
% avec en nb de ligne le nb de rapport de boite plus pm
[ERR,cprim_bv,wprim_bv,dwprim_bv]=calc_bv(VD,csec_bv,wsec_bv,dwsec_bv,0);
if ~isempty(ERR)
    return;
end

% Connexion
csec_emb1=cprim_bv;
wsec_emb1=wprim_bv;
dwsec_emb1=dwprim_bv;
elhyb=ones(size(csec_emb1)); % On suppose etre en hybride -le cas hamiltonien electrique est traite a part
[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,~]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,elhyb);
if VD.CYCL.ntypcin == 1
    % On sature la vitesse du primaire au ralenti moteur dans calc_emb...
    % A priori, on suppose ne patiner qu'en premiere pour cette architecture.
    [ind]=find(wsec_emb1(3:end,:)<VD.MOTH.ral);
    if ~isempty(ind)        
        c1=cprim_emb1(1:2,:);
        c2=cprim_emb1(3:end,:);
        c2(ind)=Inf;
        cprim_emb1=[c1; c2];
    end
end

% Connexion - Les calculs commencent au niveau du coupleur
csec_cpl=cprim_emb1;
wsec_cpl=wprim_emb1;
dwsec_cpl=dwprim_emb1;

% Strategie selon les cas
% les fonctions retournent les conditions de fonctionnement acm et mth, elhyb ... et le rapport optimal dans VD.CYCL.rappvit 
% Les variables renvoyees sont de taille CYCL.temps
if strcmpi(param.optim,'lagrange')
    [ERR,VD,cacm1,wacm1,dwacm1,cmt,wmt,dwmt,elhyb,lambda1,lambda2,Tcat,Texh,Tcoll,co_eff,hc_eff,nox_eff,co,hc,nox]=calc_lagrange_HP_BV_1EMB(ERR,param,VD,csec_cpl,wsec_cpl,dwsec_cpl,wsec_emb1,elhyb);
    if ~isfield(VD,'TWC')
        clear Tcat Texh co hc nox co_eff hc_eff nox_eff;
    end
else
    disp(['Strategie inexistante']);
    return;
end

if ~isempty(ERR)
    return
end

%-------------------------------------------------%
% Recalcul de la chaine de traction en mode forward
%-------------------------------------------------%

% Connexion
cprim1_cpl=cmt-VD.MOTH.J_mt.*dwmt;
wprim1_cpl=wmt;
dwprim1_cpl=dwmt;

cprim2_cpl=cacm1-VD.ACM1.J_mg.*dwacm1;
wprim2_cpl=wacm1;
dwprim2_cpl=dwacm1;

wsec_cpl=wprim1_cpl;
dwsec_cpl=dwprim1_cpl;
[ERR,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl,csec_cpl,~]=calc_adcpl_fw(ERR,VD.ADCPL,cprim1_cpl,0,cprim2_cpl,0,wsec_cpl,dwsec_cpl);

% Connexion
cprim_emb1=csec_cpl;
wprim_emb1=wsec_cpl;
dwprim_emb1=dwsec_cpl;

csec_emb1=cprim_emb1;
if VD.CYCL.ntypcin == 1
    wsec_emb1=wsec_emb1(sub2ind(size(wsec_emb1),VD.CYCL.rappvit+1,1:length(VD.CYCL.rappvit)));
    dwsec_emb1=dwsec_emb1(sub2ind(size(dwsec_emb1),VD.CYCL.rappvit+1,1:length(VD.CYCL.rappvit)));
else
    % Mode electrique
    wsec_emb1(elhyb==0)=0;
    dwsec_emb1(elhyb==0)=0;
end

% Connexion
cprim_bv=csec_emb1;
wprim_bv=wsec_emb1;
dwprim_bv=dwsec_emb1;
[ERR,csec_bv,wsec_bv,dwsec_bv,~]=calc_bv_fw(ERR,VD,cprim_bv,wprim_bv,dwprim_bv,0,VD.CYCL.rappvit);

% Connexion
cprim_red=csec_bv;
wprim_red=wsec_bv;
dwprim_red=dwsec_bv;
[ERR,csec_red,wsec_red,dwsec_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
% Calcul du frein mecanique !
cfrein_meca=csec_red-croue;

%-----------------------------------%
% Fin des recacluls en mode forward %
%-----------------------------------%

% Calculs moteur
[ERR,dcarb]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,elhyb,0);
if ~isempty(ERR)
    return;
end

% Calcul des conditions en amont de la machine electrique
[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
if ~isempty(ERR)
    return;
end

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);
if ~isempty(ERR)
    return;
end

% la machine acm1 est-elle alimentee ?
alim_acm1=ones(size(VD.CYCL.temps));
alim_acm1(elhyb==2)=0;
qacm1=qacm1.*alim_acm1;

pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc;

% calcul batteries
soc(1)=100-VD.INIT.Dod0;
ah(1)=0;
[ERR,ibat(1),ubat(1),~,~,u0bat(1),Rbat(1),RdF(1)]=calc_batt(ERR,VD.BATT,param.pas_temps,pres(1),soc(1),ah(1),pres,0);

for j=2:length(VD.CYCL.temps)
    [ERR,ibat(j),ubat(j),soc(j),ah(j),u0bat(j),Rbat(j),RdF(j)]=calc_batt(ERR,VD.BATT,param.pas_temps,pres(j),soc(j-1),ah(j-1),pres(j),0);
end
pbat=ibat.*ubat;

% Cycle
tsim=VD.CYCL.temps;
vit=VD.CYCL.vitesse;
rappvit_bv=VD.CYCL.rappvit;
distance=cumtrapz(VD.CYCL.temps,VD.CYCL.vitesse);
acc=VD.CYCL.accel;
vit_dem=vit;
pente=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,distance,'linear',0); % reechantillonage pente fonction distance calculee.
cinertie=Jveh.*VD.CYCL.accel/VD.VEHI.Rpneu;
pinertie_emb1=VD.MOTH.J_mt.*dwmt.*wmt;
cprim_emb1_sans_inertie=cprim_emb1+VD.MOTH.J_mt.*dwmt; % !!! Pour les flux d'energie du post-traitement
glisst_emb1=zeros(size(VD.CYCL.vitesse));
cacm1_sans_inertie=cacm1-VD.ACM1.J_mg.*dwacm1; % Sert a boucler les flux d'energie comme dans simulink
pcacm1=cacm1.*wacm1+qacm1;
uacm1=ubat;
iacm1=pcacm1./uacm1;
dod=100-soc;
pertes_bat=Rbat.*ibat.^2;
cahbat=ah;
iacc=pacc./ubat;
uacc=ubat;

[ResXml]=miseEnForme4VEHLIB(vehlib,'caller');

% Calcul de resultats synthetiques
[Res]=resultats(VD,param,ResXml);
conso100=Res.conso100;
cumcarb=Res.cumcarb;
co2_eq=Res.co2_eq;
CO2_gkm=Res.CO2_gkm;
ibat_eff=Res.ibat_eff;
ibat_moy=Res.ibat_moy;
co2=Res.CO2;
conso=Res.conso;
if isfield(VD,'TWC')
    co_gkm=Res.co_gkm;
    hc_gkm=Res.hc_gkm;
    nox_gkm=Res.nox_gkm;
end

% Bilan de puissance et d'energie
[bp]= bilanPE(VD,param,ResXml);
bilanP=bp.bilanP;

% Ajout des grandeurs synth??tiques dans une nouvelle table de la structure xml
[ResXml]=miseEnForme4GestionEnergie(ResXml);

if param.verbose>=10
    trait_hp_bv_1emb;
end

return


