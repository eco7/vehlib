% function [...]=calc_lagrange_HP_BV_1EMB(ERR,param,VD,csec_cpl,wsec_cpl,dwsec_cpl)
%
% Copyright IFSTTAR LTE 1999-2011
%
% Objet:
% Calcul d'un vehicule hybride parallele simple arbre
% dans VEHLIB en mode backward avec methode de lagrange
%
% Arguments appel :
% A l'appel on connait les conditions de fonctionnement en sortie
% (secondaire du coupleur), vecteur de la taille du cycle ou matrice si les
% rapports de boite sont libres
% On passe en arg la vitesse du secondaire de l'embrayage pour imposer le patinage
% Arguments de sortie :
% On connait le couple sur l'arbre d'entree (primaire, ACM1) du cxoupleur,
% et le mode de fonctionnement (elhyb)
%
function [ERR,VD,cacm1,wacm1,dwacm1,cmt,wmt,dwmt,elhyb,lambda1,lambda2,Tcat,Texh,Tcoll,co_eff,hc_eff,nox_eff,co,hc,nox]=calc_lagrange_HP_BV_1EMB(ERR,param,VD,csec_cpl,wsec_cpl,dwsec_cpl,wsec_emb1,elhyb)

% Initialisation
cacm1=NaN*zeros(size(VD.CYCL.temps));
cacm1_hyb=NaN*zeros(size(VD.CYCL.temps));
cacm1_disc=NaN*zeros(size(VD.CYCL.temps));
wacm1=NaN*zeros(size(VD.CYCL.temps));
wacm1_hyb=NaN*zeros(size(VD.CYCL.temps));
wacm1_disc=NaN*zeros(size(VD.CYCL.temps));
dwacm1=NaN*zeros(size(VD.CYCL.temps));
dwacm1_hyb=NaN*zeros(size(VD.CYCL.temps));
dwacm1_disc=NaN*zeros(size(VD.CYCL.temps));

cmt=NaN*zeros(size(VD.CYCL.temps));
cmt_hyb=NaN*zeros(size(VD.CYCL.temps));
cmt_disc=NaN*zeros(size(VD.CYCL.temps));
wmt=NaN*zeros(size(VD.CYCL.temps));
wmt_hyb=NaN*zeros(size(VD.CYCL.temps));
wmt_disc=NaN*zeros(size(VD.CYCL.temps));
dwmt=NaN*zeros(size(VD.CYCL.temps));
dwmt_hyb=NaN*zeros(size(VD.CYCL.temps));
dwmt_disc=NaN*zeros(size(VD.CYCL.temps));

lambda1=NaN*zeros(size(VD.CYCL.temps));
lambda1_hyb=NaN*zeros(size(VD.CYCL.temps));
lambda1_disc=NaN*zeros(size(VD.CYCL.temps));
lambda2=NaN*zeros(size(VD.CYCL.temps));
lambda2_hyb=NaN*zeros(size(VD.CYCL.temps));
lambda2_disc=NaN*zeros(size(VD.CYCL.temps));

ham=NaN*zeros(size(VD.CYCL.temps));
ham_hyb=NaN*zeros(size(VD.CYCL.temps));
ham_disc=NaN*zeros(size(VD.CYCL.temps));
soc=NaN*zeros(size(VD.CYCL.temps));
soc_hyb=NaN*zeros(size(VD.CYCL.temps));
soc_disc=NaN*zeros(size(VD.CYCL.temps));

Tcat=NaN*zeros(size(VD.CYCL.temps));
Tcat_hyb=NaN*zeros(size(VD.CYCL.temps));
Tcat_disc=NaN*zeros(size(VD.CYCL.temps));

Tcatg=NaN*zeros(size(VD.CYCL.temps));
Tcatg_hyb=NaN*zeros(size(VD.CYCL.temps));
Tcatg_disc=NaN*zeros(size(VD.CYCL.temps));

Texh=NaN*zeros(size(VD.CYCL.temps));
Texh_hyb=NaN*zeros(size(VD.CYCL.temps));
Texh_disc=NaN*zeros(size(VD.CYCL.temps));

Tcoll=NaN*zeros(size(VD.CYCL.temps));
Tcoll_hyb=NaN*zeros(size(VD.CYCL.temps));
Tcoll_disc=NaN*zeros(size(VD.CYCL.temps));

co_eff=NaN*zeros(size(VD.CYCL.temps));
co_eff_hyb=NaN*zeros(size(VD.CYCL.temps));
co_eff_disc=NaN*zeros(size(VD.CYCL.temps));
hc_eff=NaN*zeros(size(VD.CYCL.temps));
hc_eff_hyb=NaN*zeros(size(VD.CYCL.temps));
hc_eff_disc=NaN*zeros(size(VD.CYCL.temps));
nox_eff=NaN*zeros(size(VD.CYCL.temps));
nox_eff_hyb=NaN*zeros(size(VD.CYCL.temps));
nox_eff_disc=NaN*zeros(size(VD.CYCL.temps));

co=NaN*zeros(size(VD.CYCL.temps));
co_hyb=NaN*zeros(size(VD.CYCL.temps));
co_disc=NaN*zeros(size(VD.CYCL.temps));
hc=NaN*zeros(size(VD.CYCL.temps));
hc_hyb=NaN*zeros(size(VD.CYCL.temps));
hc_disc=NaN*zeros(size(VD.CYCL.temps));
nox=NaN*zeros(size(VD.CYCL.temps));
nox_hyb=NaN*zeros(size(VD.CYCL.temps));
nox_disc=NaN*zeros(size(VD.CYCL.temps));

indice_rap_opt=NaN*zeros(size(VD.CYCL.temps));
indice_rap_opt_hyb=NaN*zeros(size(VD.CYCL.temps));
indice_rap_opt_disc=NaN*zeros(size(VD.CYCL.temps));


[~,Co]=size(csec_cpl);
elhyb=ones(1,Co);
lambda1(1)=param.lambda1;
if isfield(param,'lambda2')
    lambda2(1)=param.lambda2;
else
    lambda2(1)=NaN;
end
soc(1)=100-VD.INIT.Dod0;
if isfield(VD.INIT,'Tcat0')
    Tcat(1)=VD.INIT.Tcat0+273;
    Tcatg(1)=VD.INIT.Tcat0+273;
    Tcoll(1)=VD.INIT.Tcat0+273;
else
    Tcat(1)=VD.INIT.Tamb+273;
    Tcatg(1)=VD.INIT.Tamb+273;
    Tcoll(1)=VD.INIT.Tamb+273;
end

co_eff(1)=0;
hc_eff(1)=0;
nox_eff(1)=0;

if ~isfield(param,'lambda1_cst')
    param.lambda1_cst='oui';
end
if ~isfield(param,'lambda2_cst')
    param.lambda2_cst='oui';
end

% Boucle sur le temps et la resolution de l'Hamiltonien a chaque pas
for it=1:length(VD.CYCL.temps)
    if it==1
        lambda1_p=lambda1(1);
        lambda2_p=lambda2(1);
        soc_p=soc(1);
        Tcat_p=Tcat(1);
        Tcatg_p=Tcatg(1);
        Tcoll_p=Tcoll(1);
        co_eff_p=co_eff(1);
        hc_eff_p=hc_eff(1);
        nox_eff_p=nox_eff(1);
    else
        lambda1_p=lambda1(it-1);
        lambda2_p=lambda2(it-1);
        soc_p=soc(it-1);
        Tcat_p=Tcat(it-1);
        Tcatg_p=Tcatg(it-1);
        Tcoll_p=Tcoll(it-1);
        co_eff_p=co_eff(it-1);
        hc_eff_p=hc_eff(it-1);
        nox_eff_p= nox_eff(it-1);
    end
    
    % Hamiltonien hybride
    [ERR,ham_hyb(it),lambda1_hyb(it),lambda2_hyb(it),cacm1_hyb(it),wacm1_hyb(it),dwacm1_hyb(it), ...
        cmt_hyb(it),wmt_hyb(it),dwmt_hyb(it),soc_hyb(it),Tcat_hyb(it),Tcatg_hyb(it),Texh_hyb(it), Tcoll_hyb(it),...
        co_eff_hyb(it), hc_eff_hyb(it), nox_eff_hyb(it), co_hyb(it), hc_hyb(it), nox_hyb(it), ...
        indice_rap_opt_hyb(:,it)]=...
        calc_hamilt_hyb_HP_BV_1EMB(ERR,VD,param,csec_cpl(:,it),wsec_cpl(:,it),dwsec_cpl(:,it),wsec_emb1(:,it), ...
        lambda1_p,lambda2_p,it,soc_p,Tcat_p, Tcatg_p, Tcoll_p,co_eff_p, hc_eff_p, nox_eff_p);
    
    % Hamiltonien pour les modes discrets
    [ERR,ham_disc(it),lambda1_disc(it),lambda2_disc(it),cacm1_disc(it),wacm1_disc(it),dwacm1_disc(it), ...
        cmt_disc(it),wmt_disc(it),dwmt_disc(it),soc_disc(it),Tcat_disc(it),Tcatg_disc(it),Texh_disc(it),Tcoll_disc(it)...
        co_eff_disc(it), hc_eff_disc(it), nox_eff_disc(it), co_disc(it), hc_disc(it), nox_disc(it), ...
        indice_rap_opt_disc(it),elhyb_disc(it)]=...
        calc_hamilt_ModesDiscret_HP_BV_1EMB(ERR,VD,param,csec_cpl(:,it),wsec_cpl(:,it),dwsec_cpl(:,it),wsec_emb1(:,it), ...
        lambda1_p,lambda2_p,it,soc_p,Tcat_p, Tcatg_p, Tcoll_p,co_eff_p, hc_eff_p, nox_eff_p);
    %ham_disc(it)=Inf;
    
    if isinf(ham_hyb(it)) && isinf(ham_disc(it))
        % si tous les hamilotniens sont infinis, il n y a pas de solution.
        chaine=strcat(' ham_disc et hybride NaN at iteration : ',num2str(it));
        ERR=MException('BackwardModel:calc_HP_BV_1EMB',chaine);
        [cacm1,wacm1,dwacm1,cmt,wmt,dwmt,elhyb,lambda1,lambda2,Tcat,Texh,co_eff,hc_eff,nox_eff,co,hc,nox]=backward_erreur(length(VD.CYCL.temps));
        return
    else
        % Il y a au moins une solution, on reinitialise la structure ERR
        ERR=[];
    end
    
    
    if ham_hyb(it)<ham_disc(it)
        ham(it)=ham_hyb(it);
        cacm1(it)=cacm1_hyb(it);
        wacm1(it)=wacm1_hyb(it);
        dwacm1(it)=dwacm1_hyb(it);
        cmt(it)=cmt_hyb(it);
        wmt(it)=wmt_hyb(it);
        dwmt(it)=dwmt_hyb(it);
        %if j~=1
            lambda1(it)=lambda1_hyb(it);
            lambda2(it)=lambda2_hyb(it);
            soc(it)=soc_hyb(it);
            Tcat(it)=Tcat_hyb(it); 
            Tcatg(it)=Tcatg_hyb(it);
            Tcoll(it)=Tcoll_hyb(it);
            co_eff(it)=co_eff_hyb(it);
            hc_eff(it)=hc_eff_hyb(it);
            nox_eff(it)= nox_eff_hyb(it);
        %end
        Texh(it)=Texh_hyb(it);
        co(it)=co_hyb(it);
        hc(it)=hc_hyb(it);
        nox(it)=nox_hyb(it);
        elhyb(it)=1;
        indice_rap_opt(it)=indice_rap_opt_hyb(it);
        
    elseif ham_disc(it)<=ham_hyb(it)
        ham(it)=ham_disc(it);
        cacm1(it)=cacm1_disc(it);
        wacm1(it)=wacm1_disc(it);
        dwacm1(it)=dwacm1_disc(it);
        cmt(it)=cmt_disc(it);
        wmt(it)=wmt_disc(it);
        dwmt(it)=dwmt_disc(it);
        %if j~=1
            lambda1(it)=lambda1_disc(it);
            lambda2(it)=lambda2_disc(it);
            soc(it)=soc_disc(it);
            Tcat(it)=Tcat_disc(it);
            Tcatg(it)=Tcatg_disc(it);
            Tcoll(it)=Tcoll_disc(it);
            co_eff(it)=co_eff_disc(it);
            hc_eff(it)=hc_eff_disc(it);
            nox_eff(it)= nox_eff_disc(it);
       % end
        Texh(it)=Texh_disc(it);
        co(it)=co_disc(it);
        hc(it)=hc_disc(it);
        nox(it)=nox_disc(it);
        elhyb(it)=elhyb_disc(it);
        indice_rap_opt(it)=indice_rap_opt_disc(it);
    end
    
    if ham==param.Ham_hors_limite
        chaine=strcat(' ham = Ham_hors_limite, indice',it);
        ERR=MException('BackwardModel:calc_HP_BV_1EMB',chaine);
    end
    
    if param.verbose>=1
        if rem(it,100)==0 | it==1 | it == length(VD.CYCL.temps)
            if isfield(VD,'TWC')
            fprintf(1,'%s %.1f %s %.2f %s %.2f%s%.1f%s\n','calc_HP_BV_1EMB - Indice: ',it,'elhyb:',elhyb(it), ...
                ' / - Temp. catalyseur: ',Tcat(it),' Variation de soc: ',soc(it)-soc(1),' %');
            else
            fprintf(1,'%s %.1f %s %.2f %s %.1f%s\n','calc_HP_BV_1EMB - Indice: ',it,'elhyb:',elhyb(it), ...
                ' Variation de soc: ',soc(it)-soc(1),' %');
            end
        end
    end
end % Fin de boucle sur le temps du cycle

% Rapport de boite optimise : on recree les vecteurs a partir des tableaux
if VD.CYCL.ntypcin==1
    VD.CYCL.rappvit=indice_rap_opt;
end

%----------------------------------------------------------------------------------------%
% (C) Copyright IFSTTAR LTE 1999-2011
%
% Objet: Calcul de l'hamiltonien hybride
%
% Arguments appel :
% Arguments de sortie :
%
function [ERR,ham,Lambda1,Lambda2,cacm1,wacm1,dwacm1,cmt,wmt,dwmt,soc,Tcat,Tcatg,Texh,Tcoll,co_eff,hc_eff,nox_eff,co,hc,nox,indice_rap_opt]=calc_hamilt_hyb_HP_BV_1EMB(ERR,VD,param,csec_cpl,wsec_cpl,dwsec_cpl,wsec_emb1,Lambda1_p,Lambda2_p,it,soc_p,Tcat_p,Tcatg_p,Tcoll_p,co_eff_p,hc_eff_p,nox_eff_p)

% Initialisation
ham=NaN; Lambda1=NaN; Lambda2=NaN;
cacm1=NaN; wacm1=NaN; dwacm1=NaN; cmt=NaN; wmt=NaN; dwmt=NaN; soc=NaN; 
Tcat=NaN; Texh=NaN; co_eff=NaN; hc_eff=NaN; nox_eff=NaN; co=NaN; hc=NaN; nox=NaN;
indice_rap_opt=NaN;

% Discretisation du couple ACM1
borne_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wsec_cpl*VD.ADCPL.kred);
borne_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wsec_cpl*VD.ADCPL.kred);
cprim2_cpl=borne_min:param.pas_cprim2_cpl:borne_max;

ERR=[];

if length(wsec_cpl)~=1 % cas rapport de boite libre
    cprim2_cpl=repmat(cprim2_cpl,length(wsec_cpl),1);
end
elhyb=ones(size(cprim2_cpl));

% Calcul du couple du moteur thermique
[ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,repmat(csec_cpl,1,length(cprim2_cpl)),repmat(wsec_cpl,1,length(cprim2_cpl)),repmat(dwsec_cpl,1,length(cprim2_cpl)),cprim2_cpl,0);
if ~isempty(ERR)
    return;
end

% Connexion
wmt=wprim1_cpl;
dwmt=dwprim1_cpl;
cmt=cprim1_cpl+VD.MOTH.J_mt.*dwmt;
[ERR,dcarb]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,1,0);
if ~isempty(ERR)
    return;
end

% Connexion
wacm1=wprim2_cpl;
dwacm1=dwprim2_cpl;
cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;

% Calcul des conditions en amont de la machine electrique
[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
if ~isempty(ERR)
    disp(ERR.message)
    [ham,Lambda1,Lambda2,cprim2_cpl,ibat,soc,dcarb,ubat,Tcat,Tcatg,Texh, co_eff, hc_eff, nox_eff, co, hc, nox,indice_rap_opt,dQs,dQin,dQgen]=backward_erreur(1);
    return;
end

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);
if ~isempty(ERR)
    return;
end
pacc=pacc(it);

pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc;

[ERR,ibat,ubat,soc,~,E,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,soc_p,0,pres,0);

if ~isempty(ERR)
    return;
end

if isfield(VD,'TWC')
    % Calcul des emissions moteur
    [ERR, Tcat,  Tcatg, Texh, Tcoll, co_eff, hc_eff, nox_eff, co, hc, nox,dQs,dQin,dQgen]= ...
        calc_exhaust_emissions(ERR, param, VD, it, wmt, cmt, dcarb, ...
        Tcat_p,Tcatg_p, Tcoll_p, co_eff_p, hc_eff_p, nox_eff_p, elhyb);
end

% Lambda 1 constant
Lambda1=Lambda1_p;

% Lambda 2 constant
Lambda2=Lambda2_p;

% Calcul conso et hamiltonien
if isfield(VD,'TWC') && isfield(param,'lambda2') && isfield(param,'K')
    % We have to test the existance of param.lambda2 because lambda2 is
    % always initialized to NaN (even if not exist).
    QCata=dQs+dQin-dQgen;
    ham=VD.MOTH.pci*dcarb+param.K*(co/VD.TWC.CO_EURO4+hc/VD.TWC.THC_EURO4+nox/VD.TWC.NOx_EURO4)+ ...
        Lambda1.*RdF.*ibat.*E+ ...
        Lambda2.*1/(VD.TWC.MCata*VD.TWC.CpCata).*QCata;
    t1=VD.MOTH.pci*dcarb;
    t2=param.K*(co/VD.TWC.CO_EURO4+hc/VD.TWC.THC_EURO4+nox/VD.TWC.NOx_EURO4);
    t3=Lambda1.*RdF.*ibat.*E;
    t4=Lambda2.*1/(VD.TWC.MCata*VD.TWC.CpCata).*QCata;
else
    ham=VD.MOTH.pci*dcarb+Lambda1.*RdF.*ibat.*E;
end

% Limitation sur condition de patinage
% Si en premiere le secondaire de l'embrayage est inferieure au patinage
% la premiere est imposee
if VD.CYCL.ntypcin == 1 && VD.CYCL.vitesse(it)~=0 && wsec_emb1(2)<VD.ECU.wmt_patinage
    ham(3:end,:)=param.Ham_hors_limite;
end

% Limitations sur le courant batterie et le couple max MOTH
cmt_max=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt);
ham(cmt>cmt_max | isnan(ibat) | isnan(qacm1)) = param.Ham_hors_limite  ;

[Li,Co]=size(ham);

if it == param.tdebug 
    if findobj('tag','debug')
        close(findobj('tag','debug'));
    end
    figure('tag','debug');
%     subplot(3,1,1)
%     plot(cprim2_cpl,dcarb,'-*');
%     ylabel('dcarb');
%     grid on
%     hold on
%     subplot(3,1,2)
%     plot(cprim2_cpl,ibat,'-*');
%     ylabel('ibat');
%     grid on
%     hold on
%     subplot(3,1,3)
    plot(cprim2_cpl,ham,'-*',cprim2_cpl,t1,'-*',cprim2_cpl,t2,'-*',cprim2_cpl,t3,'-*',cprim2_cpl,t4,'-*');
    legend('ham','dcarb','pol','ibat','qcat');
    xlabel('cprim2\_cpl');
    ylabel('ham');
    grid on
    hold on    
end

% Calcul du min et des grandeurs associee
if Li>1
    % I optimze both gears and operating point
    [ham,j_min]=min(ham,[],2);
    [ham,i_min]=min(ham);
    j_min=j_min(i_min);
    soc=soc(i_min,j_min);
    cacm1=cacm1(i_min,j_min);
    wacm1=wacm1(i_min,j_min);
    dwacm1=dwacm1(i_min,j_min);
    cmt=cmt(i_min,j_min);
    wmt=wmt(i_min,j_min);
    dwmt=dwmt(i_min,j_min);
    if isfield(VD,'TWC')
        Tcat=Tcat(i_min,j_min);
        Tcatg=Tcatg(i_min,j_min);
        Tcoll=Tcoll(i_min,j_min);
        Texh=Texh(i_min,j_min);
        co_eff=co_eff(i_min,j_min);
        hc_eff=hc_eff(i_min,j_min);
        nox_eff=nox_eff(i_min,j_min);
        co=co(i_min,j_min);
        hc=hc(i_min,j_min);
        nox=nox(i_min,j_min);
    else
        Tcat=NaN;
        Tcatg=NaN;
        Tcoll=NaN;
        Texh=NaN;
        co_eff=NaN;
        hc_eff=NaN;
        nox_eff=NaN;
        co=NaN;
        hc=NaN;
        nox=NaN;
    end
    
    indice_rap_opt=i_min-1;
    if strcmp(param.lambda1_cst,'non')
        Lambda1=Lambda1(i_min);
    end
    if strcmp(param.lambda2_cst,'non')
        Lambda2=Lambda2(i_min);
    end
else
    % I optimze operating point only
    [ham,i_min]=min(ham);
    soc=soc(i_min);
    cacm1=cacm1(i_min);
    wacm1=wacm1(i_min);
    dwacm1=dwacm1(i_min);
    cmt=cmt(i_min);
    wmt=wmt(i_min);
    dwmt=dwmt(i_min);
    if isfield(VD,'TWC')
        Tcat=Tcat(i_min);
        Tcatg=Tcatg(i_min);
        Tcoll=Tcoll(i_min);
        Texh=Texh(i_min);
        co_eff=co_eff(i_min);
        hc_eff=hc_eff(i_min);
        nox_eff=nox_eff(i_min);
        co=co(i_min);
        hc=hc(i_min);
        nox=nox(i_min);
    else
        Tcat=NaN;
        Tcatg=NaN;
        Tcoll=NaN;
        Texh=NaN;
        co_eff=NaN;
        hc_eff=NaN;
        nox_eff=NaN;
        co=NaN;
        hc=NaN;
        nox=NaN;
    end
    indice_rap_opt=VD.CYCL.rappvit(it);

    if strcmp(param.lambda1_cst,'non')
        Lambda1=Lambda1(i_min);
    end
    if strcmp(param.lambda2_cst,'non') && length(Lambda2)~=1
        Lambda2=Lambda2(i_min);
    end
end

return

%----------------------------------------------------------------------------------------%
% (C) Copyright IFSTTAR LTE 1999-2011
%
% Objet: Calcul de l'hamiltonien pour les modes "discrets":
% - Mode tout electrique : pour cette architecture, le seul cas possible est vehicule a l arret
%       (Le mode coupure d'injection est un mode hybride)
% - Coupure d'injection
% - Mode tout thermique, ACM1 non alimenté (pertes mecaniques seulement)
%
% Arguments appel :
% Arguments de sortie :
%
function [ERR,ham,Lambda1,Lambda2,cacm1,wacm1,dwacm1,cmt,wmt,dwmt,soc,Tcat,Tcatg,Texh,Tcoll,co_eff,hc_eff,nox_eff,co,hc,nox,indice_rap_opt,elhyb]=calc_hamilt_ModesDiscret_HP_BV_1EMB(ERR,VD,param,csec_cpl,wsec_cpl,dwsec_cpl,wsec_emb1,Lambda1_p,Lambda2_p,it,soc_p,Tcat_p,Tcatg_p,Tcoll_p,co_eff_p,hc_eff_p,nox_eff_p)

% Initialisation
ham=NaN; Lambda1=NaN; Lambda2=NaN;
cacm1=NaN; wacm1=NaN; dwacm1=NaN; cmt=NaN; wmt=NaN; dwmt=NaN; soc=NaN; 
Tcat=NaN; Texh=NaN; Tcoll=NaN; co_eff=NaN; hc_eff=NaN; nox_eff=NaN; co=NaN; hc=NaN; nox=NaN;
indice_rap_opt=NaN; elhyb=NaN;

ERR= [];

csec_cpl_vect=[csec_cpl];
wsec_cpl_vect=[wsec_cpl];
dwsec_cpl_vect=[dwsec_cpl];

% Mode coupure d'injection
cmt_min=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_min,wsec_cpl);
wprim1_cpl=[wsec_cpl];
dwprim1_cpl=[dwsec_cpl];
cprim1_cpl=[cmt_min-VD.MOTH.J_mt.*dwprim1_cpl];
acm1_alim=ones(size(csec_cpl)); % la machine est alimentee
elhyb=ones(size(csec_cpl));

% Mode tout electrique
if (VD.CYCL.vitesse(it)==0)
    % A l'arret du vehicule, le mode electrique est possible
    cprim1_cpl=[cprim1_cpl zeros(size(csec_cpl))];
    wprim1_cpl=[wprim1_cpl zeros(size(csec_cpl))];
    dwprim1_cpl=[dwprim1_cpl zeros(size(csec_cpl))];
    acm1_alim=[acm1_alim zeros(size(csec_cpl))]; % la machine n'est pas alimentee
    elhyb=[elhyb zeros(size(csec_cpl))];
    csec_cpl_vect=[csec_cpl_vect zeros(size(csec_cpl))];
    wsec_cpl_vect=[wsec_cpl_vect zeros(size(csec_cpl))];
    dwsec_cpl_vect=[dwsec_cpl_vect zeros(size(csec_cpl))];
end

% Calcul des conditions sur le secondaire du coupleur (ACM1)
[ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl,cprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl_vect,wsec_cpl_vect,dwsec_cpl_vect,cprim1_cpl,0,2);

% Mode tout thermique
wacm1=wsec_cpl*VD.ADCPL.kred;
dwacm1=dwsec_cpl*VD.ADCPL.kred;
cacm1_frot=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,abs(wacm1));
wprim2_cpl=[wprim2_cpl wacm1];
dwprim2_cpl=[dwprim2_cpl dwacm1];
cprim2_cpl=[cprim2_cpl cacm1_frot];
acm1_alim=[acm1_alim zeros(size(csec_cpl))]; % la machine n'est pas alimentee
elhyb=[elhyb ones(size(csec_cpl))*2];
csec_cpl_vect=[csec_cpl_vect csec_cpl];
wsec_cpl_vect=[wsec_cpl_vect wsec_cpl];
dwsec_cpl_vect=[dwsec_cpl_vect dwsec_cpl];

% Calcul des conditions sur le primaire du coupleur (MOTH)
[ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl_vect,wsec_cpl_vect,dwsec_cpl_vect,cprim2_cpl,0);
cprim1_cpl(isnan(cprim1_cpl))=Inf;

% Connexion
wmt=wprim1_cpl;
dwmt=dwprim1_cpl;
cmt=cprim1_cpl+VD.MOTH.J_mt.*dwmt;
[ERR,dcarb]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,elhyb,0);

%Connexion
wacm1=wprim2_cpl;
dwacm1=dwprim2_cpl;
cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;

% Calcul des conditions en amont de la machine electrique
%%% limitations ME
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);

% Si cacm1<cacm1_min on fait de la recup et on vas faire le max possible
% le reste en frein meca, donc on sature cacm1 ?? cacm1_min
cacm1(cacm1<cacm1_min)=cacm1_min(cacm1<cacm1_min);

% Si cacm1>cacm1_max, le point n'est pas faisable en tout electrique
cacm1(cacm1>cacm1_max)=NaN;

[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');

% Si la machine n'est pas alimentee, les pertes qacm1 sont nulles
qacm1=qacm1.*acm1_alim;

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);
if ~isempty(ERR)
    return;
end
pacc=pacc(it);

% Puissance reseau electrique
pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc;

[ERR,ibat,ubat,soc,ah,E,R,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,soc_p,0,pres,0);

Lambda1=Lambda1_p;

Lambda2=Lambda2_p;

if isfield(VD,'TWC')
    % Calcul des emissions moteur
    [ERR, Tcat, Tcatg, Texh, Tcoll, co_eff, hc_eff, nox_eff, co, hc, nox,dQs,dQin,dQgen]= ...
        calc_exhaust_emissions(ERR, param, VD, it, wmt, cmt, dcarb, ...
        Tcat_p, Tcatg_p, Tcoll_p, co_eff_p, hc_eff_p, nox_eff_p, elhyb);
end

%%% Calcul conso et hamiltonien
if isfield(VD,'TWC') && isfield(param,'lambda2') && isfield(param,'K')
    % We have to test the existance of param.lambda2 because lambda2 is
    % always initialized to NaN (even if not exist).
    QCata=dQs+dQin-dQgen;
    ham=VD.MOTH.pci*dcarb+param.K*(co/VD.TWC.CO_EURO4+hc/VD.TWC.THC_EURO4+nox/VD.TWC.NOx_EURO4)+ ...
        Lambda1.*RdF.*ibat.*E+ ...
        Lambda2.*1/(VD.TWC.MCata*VD.TWC.CpCata).*QCata;
    t1=VD.MOTH.pci*dcarb;
    t2=param.K*(co/VD.TWC.CO_EURO4+hc/VD.TWC.THC_EURO4+nox/VD.TWC.NOx_EURO4);
    t3=Lambda1.*RdF.*ibat.*E;
    t4=Lambda2.*1/(VD.TWC.MCata*VD.TWC.CpCata).*QCata;
else
    ham=VD.MOTH.pci*dcarb+Lambda1.*RdF.*ibat.*E;
end

% Limitation sur condition de patinage
% Si en premiere le secondaire de l'embrayage est inferieure au patinage
% la premiere est imposee
if VD.CYCL.ntypcin == 1 && VD.CYCL.vitesse(it)~=0 && wsec_emb1(2)<VD.ECU.wmt_patinage
    ham(3:end,:)=param.Ham_hors_limite;
end

%%% Limitations sur le courant batterie et le couple max MOTH
cmt_max=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt);
ham(isnan(dcarb) | cmt>cmt_max | isnan(ibat) | isnan(qacm1) | cacm1>cacm1_max | cacm1<cacm1_min) = param.Ham_hors_limite;

[Li,Co]=size(ham);

if it == param.tdebug
    set(0,'CurrentFigure',findobj('tag','debug'));
    plot(cprim2_cpl,ham,'-d',cprim2_cpl,t1,'-d',cprim2_cpl,t2,'-d',cprim2_cpl,t3,'-d',cprim2_cpl,t4,'-d');
    pause
end

% calcul du min et des grandeurs associee
if Li>1
    [ham,j_min]=min(ham,[],2);
    [ham,i_min]=min(ham); % le cas point mort n'as pas de sens en mode hybride
    j_min=j_min(i_min);
    soc=soc(i_min,j_min);
    cacm1=cacm1(i_min,j_min);
    wacm1=wacm1(i_min,j_min);
    dwacm1=dwacm1(i_min,j_min);
    cmt=cmt(i_min,j_min);
    wmt=wmt(i_min,j_min);
    dwmt=dwmt(i_min,j_min);
    if isfield(VD,'TWC')
        Tcat=Tcat(i_min,j_min);
        Tcatg=Tcatg(i_min,j_min);
        Tcoll=Tcoll(i_min,j_min);
        Texh=Texh(i_min,j_min);
        co_eff=co_eff(i_min,j_min);
        hc_eff=hc_eff(i_min,j_min);
        nox_eff=nox_eff(i_min,j_min);
        co=co(i_min,j_min);
        hc=hc(i_min,j_min);
        nox=nox(i_min,j_min);
    else
        Tcat=NaN;
        Tcatg=NaN;
        Tcoll=NaN;
        Texh=NaN;
        co_eff=NaN;
        hc_eff=NaN;
        nox_eff=NaN;
        co=NaN;
        hc=NaN;
        nox=NaN;
    end
    elhyb=elhyb(i_min,j_min);
    indice_rap_opt=i_min-1;
    if strcmp(param.lambda1_cst,'non')
        Lambda1=Lambda1(i_min);
    end
    if strcmp(param.lambda2_cst,'non')
        Lambda2=Lambda2(i_min);
    end
else
    [ham,i_min]=min(ham);
    soc=soc(i_min);
    cacm1=cacm1(i_min);
    wacm1=wacm1(i_min);
    dwacm1=dwacm1(i_min);
    cmt=cmt(i_min);
    wmt=wmt(i_min);
    dwmt=dwmt(i_min);
    if isfield(VD,'TWC')
        Tcat=Tcat(i_min);
        Tcatg=Tcatg(i_min);
        Tcoll=Tcoll(i_min);
        Texh=Texh(i_min);
        co_eff=co_eff(i_min);
        hc_eff=hc_eff(i_min);
        nox_eff=nox_eff(i_min);
        co=co(i_min);
        hc=hc(i_min);
        nox=nox(i_min);
    else
        Tcat=NaN;
        Tcatg=NaN;
        Tcoll=NaN;
        Texh=NaN;
        co_eff=NaN;
        hc_eff=NaN;
        nox_eff=NaN;
        co=NaN;
        hc=NaN;
        nox=NaN;
    end
    elhyb=elhyb(i_min);
    indice_rap_opt=VD.CYCL.rappvit(it);
    if strcmp(param.lambda1_cst,'non')
        Lambda1=Lambda1(i_min);
    end
    if strcmp(param.lambda2_cst,'non') && length(Lambda2)~=1
        Lambda2=Lambda2(i_min);
    end
end

return




