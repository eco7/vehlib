% function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,cacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,dwacm2_elec,qacm2_elec,cprim_red,cfrott_acm1]=  calc_limites_HSP_2EMB(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul des limites du graphes.
% On ne calcule ici que les soc min et max sans se preocuper
% de la remonte du graphe ou de soc <0 ou >100%.
% La limite min doit être calculé avec précision (mode tout élec)
% la mlimite max peut être calcule "grossièrment" d'eventuel arc impossible
% dans le graphe seront elimine par la suite
%
% interface entrée :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discreetisation etc)
% CYLC,VD.ADCPL...:structures de description vehicule et composants
% cprim_red  :   Couple sur le primaire du reducteur (vecteur sur le cycle)
% wprim_red  :   vitesse sur le primaire du reducteur (vecteur sur le cycle)
% dwprim_red :   dérivée de la vitesse sur le primaire du reducteur (vecteur sur le cycle)
% 
% interface de sortie
% ibat_max :    courant max mode tout elec (en recup c'est le courant de recup en tout elec)
% ibat_min :    courant min recharge max ( en recup courant recup plus recharge par moth)
% elec_pos :    vecteur indiquant si mode elec possible , ou zero si impossible
% soc_min :     soc correspondant a ibat_max
% soc_max :     soc correspondant a ibat_min
% Dsoc_min :    variation soc entre deux instant correspondant a ibat_max
%               (atention a intégre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% Dsoc_max :    variation soc entre deux instant correspondant a ibat_min
%               (atention a intégre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% cacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,dwacm2_elec,qacm2_elec,cprim_red,cfrott_acm1: utilisee ensuite en
%               recalcul pour les mode tout elec (evite de refaire les optimisation en
%               recalcul
%
% 23-07-12(EV): Creation 


function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,cacm1_elec,wacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,qacm2_elec,cprim_red,cfrott_acm1]=  calc_limites_HSDP_EPI(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
ERR=[];
ii_max=size(VD.CYCL.temps);
soc_min=zeros(ii_max);
soc_max=zeros(ii_max);
Dsoc_min=zeros(ii_max);
Dsoc_max=zeros(ii_max);
elec_pos=ones(ii_max);
elec1_pos=ones(ii_max);
elec2_pos=ones(ii_max);
elec2_rec=ones(ii_max);
cprim_red_elec1=cprim_red;
cprim_red_elec2=cprim_red;
sc=length(VD.CYCL.temps);
cfrott_acm1=zeros(ii_max);
ibat_max_elec2=NaN*ones(ii_max);


% Calcul de la vitesse min possible du moteur thermique pour vérifier que le mode tout élec est possible
wmt_min=0*ones(size(wprim_red)); % limitation sur vit max du moth et vit max de la generatrice

if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
    wmt_min(wprim_red>VD.ECU.vmaxele/VD.VEHI.Rpneu*VD.RED.kred)=VD.MOTH.ral;
end

elec1_pos(wmt_min~=0)=0.5;
elec2_pos(wmt_min~=0)=1.5;

% Calcul des vitesses max des machines
I_wacm1_max=min(find(VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:)))~=0,1,'last'),length(VD.ACM1.Regmot_pert));
wacm1_max= min(VD.ACM1.Regmot_cmax(I_wacm1_max),VD.ECU.wmaxge);

I_wacm1_min=min(find(VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:)))~=0,1,'last'),length(VD.ACM1.Regmot_pert));
wacm1_min= -min(VD.ACM1.Regmot_cmax(I_wacm1_min),VD.ECU.wmaxge);

I_wacm2_max=min(find(VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:)))~=0,1,'last'),length(VD.ACM1.Regmot_pert));
wacm2_max= min(VD.ACM2.Regmot_cmax(I_wacm2_max),VD.ECU.wmaxge);

I_wacm2_min=min(find(VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmin_mot(1,:)))~=0,1,'last'),length(VD.ACM2.Regmot_pert));
wacm2_min= -min(VD.ACM2.Regmot_cmax(I_wacm2_min),VD.ECU.wmaxge);

[ERR,~,~,~,~,~,~,Ibat_max_calc,Ubat_max_calc,Ibat_min_calc,Ubat_min_calc]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,0,(100-VD.INIT.Dod0),0);

%% recherche du mode tout electrique à chaque pas de temps cas ou acm1 seul Emb1 fermée
% calcul de la vitesse des machines en tout elec avec acm1 alimenté seul
wacm2_elec1=zeros(ii_max);
dwacm2_elec1=zeros(ii_max);
qacm2_elec1=zeros(ii_max);
cacm2_elec1=zeros(ii_max);
dwacm1_elec1=zeros(ii_max);

wacm1_elec2=NaN*ones(ii_max);
wacm2_elec2=NaN*ones(ii_max);
cacm1_elec2=NaN*ones(ii_max);
cacm2_elec2=NaN*ones(ii_max);

% connexion
cps=cprim_red;
wps=wprim_red;


[ERR,cacm1_elec1,ccour_elec1,wacm1_elec1]=calc_train_epi(ERR,VD.EPI,cps,wps,wacm2_elec1,2);



%[ERR,qacm1_elec1]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec1,wacm1_elec1,0);

% limitation vitesse couple acm1:
cacm1_elec1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec1);
cacm1_elec1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec1);
wps_s_elec1=(wprim_red-wacm2_elec1)*VD.EPI.Nsat/VD.EPI.Ncour;

% Si on ne peut pas faire de mode tout elec cprim>cacm1_max
% On prend les couples max
ind_max=find(cacm1_elec1>cacm1_elec1_max);

cacm1_elec1(ind_max)=cacm1_elec1_max(ind_max);

[ERR,qacm1_elec1]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec1,wacm1_elec1,0);
elec1_pos(ind_max)=0;

% Si recup max
ind_min=find(cacm1_elec1<cacm1_elec1_min);
cacm1_elec1(ind_min)=cacm1_elec1_min(ind_min);
[ERR,qacm1_elec1(ind_min)]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec1_min(ind_min),wacm1_elec1(ind_min),0);
cprim_red_elec1(ind_min)=(1-VD.EPI.kb)/VD.EPI.rend_train*cacm1_elec1(ind_min);       


% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);

% calcul puissance motelec
pacm1_elec1=VD.VEHI.nbacm1*(cacm1_elec1.*wacm1_elec1+qacm1_elec1);


pres_elec1=pacm1_elec1+pacc;
% 
[ERR,ibat_max_elec1,~,~,~,~,~,RdF_elec1]=calc_batt(ERR,VD.BATT,param.pas_temps,pres_elec1,100-VD.INIT.Dod0,0,-1,0,param);

% Cas ou l'on as explose les limites en recup
% On doit imposer que Ibat_max = VD.BATT.Ibat_min
ind_rec_max_bat_elec1=find(isnan(ibat_max_elec1)&cprim_red<0);
ibat_max_elec1(ind_rec_max_bat_elec1)=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension

if ~isempty(ind_rec_max_bat_elec1)
    pbat_min=Ibat_min_calc*Ubat_min_calc;
    %[ERR,qacm1_max_elec1]=calc_pertes_acm(ERR,VD.ACM1,cacm1_max(ind_rec_max_bat_elec1),wacm1_elec1(ind_rec_max_bat_elec1),dwacm1_elec1(ind_rec_max_bat_elec1));
    
    pacm1_elec1(ind_rec_max_bat_elec1)=pbat_min-pacc(ind_rec_max_bat_elec1);
    [ERR,qacm1_elec1(ind_rec_max_bat_elec1)]=calc_pertes_acm_fw(ERR,VD.ACM1,pacm1_elec1(ind_rec_max_bat_elec1),wacm1_elec1(ind_rec_max_bat_elec1),dwacm1_elec1(ind_rec_max_bat_elec1));
    cacm1_elec1(ind_rec_max_bat_elec1)=(pacm1_elec1(ind_rec_max_bat_elec1)-qacm1_elec1(ind_rec_max_bat_elec1))./wacm1_elec1(ind_rec_max_bat_elec1);   
    
    cprim_red_elec1(ind_rec_max_bat_elec1)=(1-VD.EPI.kb)/VD.EPI.rend_train*cacm1_elec1(ind_rec_max_bat_elec1); 
    ibat_max_elec1(ind_rec_max_bat_elec1)=Ibat_min_calc;
end

% cas ou l'on explose ibat_max en traction mode elec impossoble
elec1_pos(isnan(ibat_max_elec1))=0; % la batterie ne peut fournir toute la puissance mode elec impossible

ibat_max_elec1(isnan(ibat_max_elec1))=interp1(VD.BATT.DoD_Ibat_max,VD.BATT.Ibat_max,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension

% Sur certain point de recup ont peut arriver a plus de pertes dans acm1
% que l'on ne recupere de puissance sur le primaire reducteur, on regarde
% alors ce qu'ils se passe si on alimente pas la machine
pres_elec1_acm1_na=pres_elec1;
pres_elec1_acm1_na(cprim_red<0)=pacc(cprim_red<0);
[ERR,ibat_max_elec1_acm1_na]=calc_batt(ERR,VD.BATT,param.pas_temps,pres_elec1_acm1_na,100-VD.INIT.Dod0,0,-1,0,param);
I=find(cprim_red<0 & qacm1_elec1>(-cacm1_elec1.*wacm1_elec1) & elec1_pos==1);

ibat_max_elec1(I)=ibat_max_elec1_acm1_na(I);

% calcul des pertes par frottement des machines 
cfrott_acm1=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,abs(wacm1_elec1));
cfrott_acm1_elec1=zeros(1,length(VD.CYCL.temps));

cfrott_acm1_elec1(I)=max(cfrott_acm1(I),cprim_red(I)*VD.EPI.rend_train/(1-VD.EPI.kb));
cacm1_elec1(I)=cfrott_acm1_elec1(I);
qacm1_elec1(I)=0;
cprim_red_elec1(I)=(1-VD.EPI.kb)/VD.EPI.rend_train*cacm1_elec1(I);  
 
% Cas ou ce mode serait impossible pour des raisons de vitesse max de acm1
ind_elec1_np=find( wacm1_elec1>wacm1_max | ...
                   wps_s_elec1>VD.ECU.wmaxps);
ibat_max_elec1(ind_elec1_np)=Inf;

[ERR,ubat_max_elec1,~,~,~,~,RdF_elec1]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat_max_elec1,100-VD.INIT.Dod0,0);

if param.verbose>=3
    
    assignin('base','cprim_red_elec1',cprim_red_elec1);
    assignin('base','ibat_max_elec1',ibat_max_elec1);
    assignin('base','ubat_max_elec1',ubat_max_elec1);
    assignin('base','cfrott_acm1_elec1',cfrott_acm1_elec1);
    assignin('base','cacm1_elec1',cacm1_elec1);
    assignin('base','cacm2_elec1',cacm2_elec1);
    assignin('base','wacm1_elec1',wacm1_elec1);
    assignin('base','wacm2_elec1',wacm2_elec1);
    assignin('base','qacm1_elec1',qacm1_elec1);
    assignin('base','qacm2_elec1',qacm2_elec1);
    assignin('base','pacm1_elec1',pacm1_elec1);
    assignin('base','ind_elec1_np',ind_elec1_np);
    assignin('base','wacm1_max',wacm1_max);
    assignin('base','wmt_min',wmt_min);
end

%% recherche du mode tout electrique à chaque pas de temps cas ou acm1 et acm2 fonctionnent Emb2 fermée Emb3 ouvert

% On recherche pour quel vitesse
% on obtiend le meilleur point de fonctionnement
% On discrétise la vitesse de acm2 entre -vit_max et vit_max
wacm2_elec2_vec=[wacm2_min fliplr(-param.pas_wacm2:-param.pas_wacm2:wacm2_min)  0:param.pas_wacm2:wacm2_max wacm2_max];
wacm2_elec2_mat=repmat(wacm2_elec2_vec',1,sc);
[sx,sy]=size(wacm2_elec2_mat);
wprim_red_mat=repmat(wprim_red,sx,1);
cprim_red_mat=repmat(cprim_red,sx,1);
cps_mat=repmat(cps,sx,1);
elec2_pos_mat=ones(size(wacm2_elec2_mat));
elec2_rec_mat=zeros(size(wacm2_elec2_mat));

[ERR,cacm1_elec2_mat,cacm2_elec2_mat,wacm1_elec2_mat,wsat_ps_elec2]=calc_train_epi(ERR,VD.EPI,cps_mat,wprim_red_mat,wacm2_elec2_mat,2);

% calcul des couples max des machines
cacm1_min_elec2_mat=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec2_mat);
cacm1_max_elec2_mat=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec2_mat);

cacm2_min_elec2_mat=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2_elec2_mat);
cacm2_max_elec2_mat=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2_elec2_mat);

% On met a Inf les valeur qui entraine un depassement de vitesse de acm1 ou
% de rotation des sattelittes
ind_vit=find(abs(wacm1_elec2_mat)>VD.ECU.wmaxge | abs(wsat_ps_elec2)>VD.ECU.wmaxps);
cacm1_elec2_mat(ind_vit)=Inf;
cacm2_elec2_mat(ind_vit)=Inf;
wacm1_elec2_mat(ind_vit)=Inf;
wacm2_elec2_mat(ind_vit)=Inf;

%% limitation sur les couples
% Cas cprim_red>0
% si l'un ou l'autre des couples max est atteind alors c'est lui le plus
% limitant et il impose son couple (via le train à l'autre machine et au PS)
elec2_pos_mat=ones(size(cacm1_elec2_mat));
elec2_rec_mat=zeros(size(cacm1_elec2_mat));

ind_max_elec2_acm1=find( cacm1_elec2_mat>cacm1_max_elec2_mat);
cacm1_elec2_mat(ind_max_elec2_acm1)=cacm1_max_elec2_mat(ind_max_elec2_acm1);
cacm2_elec2_mat(ind_max_elec2_acm1)=min(-VD.EPI.kb*cacm1_elec2_mat(ind_max_elec2_acm1),cacm2_max_elec2_mat(ind_max_elec2_acm1));
elec2_pos_mat(ind_max_elec2_acm1)=0;

ind_max_elec2_acm2=find( cacm2_elec2_mat>cacm2_max_elec2_mat);
cacm2_elec2_mat(ind_max_elec2_acm2)=cacm2_max_elec2_mat(ind_max_elec2_acm2);
cacm1_elec2_mat(ind_max_elec2_acm2)=min(-1/VD.EPI.kb*cacm2_elec2_mat(ind_max_elec2_acm2),cacm1_max_elec2_mat(ind_max_elec2_acm2));
elec2_pos_mat(ind_max_elec2_acm2)=0;



% Cas cprim_red<0
% si l'un ou l'autre des couples min est atteind alors c'est lui le plus
% limitant et il impose son couple (via le train à l'autre machine et au PS)
% On a alors du frein meca
ind_min_elec2_acm1=find(cacm1_elec2_mat<cacm1_min_elec2_mat);
cacm1_elec2_mat(ind_min_elec2_acm1)=cacm1_min_elec2_mat(ind_min_elec2_acm1);

%cacm2_elec2_mat(ind_min_elec2_acm1)=max(-VD.EPI.kb*cacm1_elec2_mat(ind_min_elec2_acm1),cacm2_min_elec2_mat(ind_min_elec2_acm1));
cacm2_elec2_mat(ind_min_elec2_acm1)=-VD.EPI.kb*cacm1_elec2_mat(ind_min_elec2_acm1);

cps_mat(ind_min_elec2_acm1)=(1-VD.EPI.kb)/VD.EPI.rend_train*cacm1_elec2_mat(ind_min_elec2_acm1);
elec2_rec_mat(ind_min_elec2_acm1)=1;

ind_min_elec2_acm2=find(cacm2_elec2_mat<cacm2_min_elec2_mat);
cacm2_elec2_mat(ind_min_elec2_acm2)=cacm2_min_elec2_mat(ind_min_elec2_acm2);
%cacm1_elec2_mat(ind_min_elec2_acm2)=max(-1/VD.EPI.kb*cacm2_elec2_mat(ind_min_elec2_acm2),cacm1_min_elec2_mat(ind_min_elec2_acm2));
cacm1_elec2_mat(ind_min_elec2_acm2)=-1/VD.EPI.kb*cacm2_elec2_mat(ind_min_elec2_acm2);

cps_mat(ind_min_elec2_acm2)=(1-VD.EPI.kb)/VD.EPI.rend_train*cacm1_elec2_mat(ind_min_elec2_acm2);
elec2_rec_mat(ind_min_elec2_acm2)=1;


% On recalcul le couple sur le primaire reducteur
cprim_red_mat=cps_mat;


% Pertes dans les machines
[ERR,qacm1_elec2_mat]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec2_mat,wacm1_elec2_mat,0);
[ERR,qacm2_elec2_mat]=calc_pertes_acm(ERR,VD.ACM2,cacm2_elec2_mat,wacm2_elec2_mat,0);
qacm_elec2_mat=qacm1_elec2_mat+qacm2_elec2_mat;
% On calcule les puissance max min possible de la batterie

pbat_min=Ibat_min_calc.*Ubat_min_calc;
pbat_max=Ibat_max_calc.*Ubat_max_calc;


% On calcule la puissance elec du aux machines
Pelec_mat=cacm1_elec2_mat.*wacm1_elec2_mat+qacm1_elec2_mat+cacm2_elec2_mat.*wacm2_elec2_mat+qacm2_elec2_mat+repmat(pacc,sx,1);

[ERR,ibat_max_elec2_mat,~,~,~,~,~,RdF_elec2]=calc_batt(ERR,VD.BATT,param.pas_temps,Pelec_mat,100-VD.INIT.Dod0,0,-1,0,param);

% Si Pelec_mat > pbat_max on met Ibat à Ibat_max et elec2_pos_mat à 0
ibat_max_elec2_mat(Pelec_mat>pbat_max)=Ibat_max_calc;
elec2_pos_mat(Pelec_mat>pbat_max)=0;
qacm_elec2_mat(elec2_pos_mat==0)=Inf; % On met les pertes à Inf pour ne pas converger dessus, si il n'y as aucune
% valeur valable on remettre Ibat a son max on ne pourra pas faire de tout elec donc on se fout des autres grandeurs
elec2_pos(sum(~isinf(qacm_elec2_mat))==0)=0;

% Si Pelec_mat < pbat_min
ibat_max_elec2_mat(Pelec_mat<pbat_min)=Ibat_min_calc;
I=find(sum(Pelec_mat<pbat_min)>0 & cprim_red<0); % recherche des colonnes ou on peut atteindre Ibat_min

for k=1:length(I)
    ii=I(k);
    jj=find(Pelec_mat(:,ii)<pbat_min); % elements qui atteignent pbat_min dans cette colonne
    [~,jmin]=min(max(wacm2_elec2_mat(jj,ii),wacm1_elec2_mat(jj,ii)));
    jj(jmin);
    % indice choisie il faut venir resoudre le système pour cas vitesses
    wacm1_rec=wacm1_elec2_mat(jj(jmin),ii);
    wacm2_rec=wacm2_elec2_mat(jj(jmin),ii);
    % on discrerise cacm1 entre sa valeur actuel (qui explose ibat) et zero
    cacm1_vec=cacm1_elec2_mat(jj(jmin),ii):param.pas_cacm1:0;
    if isscalar(cacm1_vec)
         cacm1_vec=cacm1_elec2_mat(jj(jmin),ii):-cacm1_elec2_mat(jj(jmin),ii)/5:0;
    end
    cacm2_vec=-VD.EPI.kb*cacm1_vec;
    cacm2_vec(1)=cacm2_elec2_mat(jj(jmin),ii);% pour des pb de prec numérique cette valeur peut eventeullement dépasser cacm2_max à eps prêt et entrainer ensuite despeortes NaN
    % On recalcul les pertes correspondante
    [ERR,qacm1_vec]=calc_pertes_acm(ERR,VD.ACM1,cacm1_vec,wacm1_rec*ones(1,length(cacm1_vec)),0);   
    [ERR,qacm2_vec]=calc_pertes_acm(ERR,VD.ACM2,cacm2_vec,wacm2_rec*ones(1,length(cacm2_vec)),0);
   
    pelec_vec=cacm1_vec.*wacm1_rec+qacm1_vec+cacm2_vec.*wacm2_rec+qacm2_vec+pacc(ii);
    bilan_vec=pelec_vec-pbat_min;
    
    i_pos=find(bilan_vec>0,1); % indices de la première valeur positive
    i_neg=find(bilan_vec<0,1,'last'); % indices de la dernière valeur negative
    if ~isempty(i_pos) && ~isempty(i_neg) && i_pos~=1 % sinon ca doit pas arriver !!!
        if i_neg~=i_pos-1
            if param.verbose>=2
                chaine='bilan de puissance non monotone croissant \n ii: %d \n jj(jmin): %d \n bilan_vec : %s' ;
                warning('BackwardModel:calc_limites_HSDP_EPI',chaine,ii,jj(jmin),num2str(bilan_vec));
            end
            i_neg=i_pos-1;
        end
        cacm1_rec=cacm1_vec(i_pos)-(cacm1_vec(i_pos)-cacm1_vec(i_neg))/(bilan_vec(i_pos)-bilan_vec(i_neg)).*bilan_vec(i_pos);
    end
    wacm1_elec2(ii)=wacm1_elec2_mat(jj(jmin),ii);
    wacm2_elec2(ii)=wacm2_elec2_mat(jj(jmin),ii);
    cacm1_elec2(ii)=cacm1_rec;
    cacm2_elec2(ii)=-VD.EPI.kb*cacm1_rec;
    [ERR,qacm1_elec2(ii)]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec2(ii),wacm1_elec2(ii),0);
    [ERR,qacm2_elec2(ii)]=calc_pertes_acm(ERR,VD.ACM2,cacm2_elec2(ii),wacm2_elec2(ii),0);
    %cprim_red_elec2(ii)=cprim_red_mat(jj(jmin),ii);
    cprim_red_elec2(ii)=(1-VD.EPI.kb)/VD.EPI.rend_train*cacm1_rec;
    ibat_max_elec2(ii)=Ibat_min_calc;
end

% Cas des points de recup ou ibat>ibat_min, on minimize le courant de recup
% idem en traction si on explose pas ibat_max
Irec=find(sum(Pelec_mat<pbat_min)==0 & cprim_red<0);
[min_ibat_max_elec2_mat,ind_min_elec2]=min(ibat_max_elec2_mat);
ind_lin_elec2=(sx)*((1:sy)-1)+ind_min_elec2;

ibat_max_elec2(Irec)=min_ibat_max_elec2_mat(Irec);

wacm1_elec2(Irec)=wacm1_elec2_mat(ind_lin_elec2(Irec));
wacm2_elec2(Irec)=wacm2_elec2_mat(ind_lin_elec2(Irec));
cacm1_elec2(Irec)=cacm1_elec2_mat(ind_lin_elec2(Irec));
cacm2_elec2(Irec)=cacm2_elec2_mat(ind_lin_elec2(Irec));
qacm1_elec2(Irec)=qacm1_elec2_mat(ind_lin_elec2(Irec));
qacm2_elec2(Irec)=qacm2_elec2_mat(ind_lin_elec2(Irec));


% Cas des colonne ou il existe des points qui n'explose pas pbat
% et ou mode elec posssible (majorité des cas) on prend le courant min

Itrac=find(sum(Pelec_mat<=pbat_max & elec2_pos_mat==1)>0 & cprim_red>=0);

ibat_max_elec2_mat(elec2_pos_mat==0)=Inf;
[min_ibat_max_elec2_mat_trac,ind_min_elec2_trac]=min(ibat_max_elec2_mat);
ibat_max_elec2(Itrac)=min_ibat_max_elec2_mat_trac(Itrac);
ind_lin_elec2_trac=(sx)*((1:sy)-1)+ind_min_elec2_trac;

wacm1_elec2(Itrac)=wacm1_elec2_mat(ind_lin_elec2_trac(Itrac));
wacm2_elec2(Itrac)=wacm2_elec2_mat(ind_lin_elec2_trac(Itrac));
cacm1_elec2(Itrac)=cacm1_elec2_mat(ind_lin_elec2_trac(Itrac));
cacm2_elec2(Itrac)=cacm2_elec2_mat(ind_lin_elec2_trac(Itrac));
qacm1_elec2(Itrac)=qacm1_elec2_mat(ind_lin_elec2_trac(Itrac));
qacm2_elec2(Itrac)=qacm2_elec2_mat(ind_lin_elec2_trac(Itrac));


% Cas des colonne ou il n'y as que des points qui explose le courant batterie
% et ou les couples max ne seraient pas dépasses
Itrac_np=find(sum(Pelec_mat<=pbat_max & elec2_pos_mat==1)==0 & cprim_red>=0);
elec2_pos(Itrac_np)=0;
ibat_max_elec2(Itrac_np)=Ibat_max_calc;

% on verifie qu'il n'y as pas de ibat_elec2_mat a NaN
if sum(isnan(ibat_max_elec2))>0
    'ca chie'
    find(isnan(ibat_max_elec2))
    pause
end

% On met a NaN les cas ou elec2_pos=0 = mode elec impossible dans ce mode

I_elec2_np=find(elec2_pos==0);
wacm1_elec2(I_elec2_np)=NaN;
wacm2_elec2(I_elec2_np)=NaN;
cacm1_elec2(I_elec2_np)=NaN;
cacm2_elec2(I_elec2_np)=NaN;
qacm1_elec2(I_elec2_np)=NaN;
qacm2_elec2(I_elec2_np)=NaN;

% Cas ou lon fait plus de pertes dans les machines que de recup
[ERR,ibat_acc]=calc_batt(ERR,VD.BATT,param.pas_temps,pacc,100-VD.INIT.Dod0,0,-1,0,param);
I=find(cprim_red<0 & ibat_max_elec2>ibat_acc & elec1_pos==1);

ibat_max_elec2(I)=ibat_acc(I);

% calcul des pertes par frottement des machines
% Dans ce cas ou considere que on est en mode 1 avec frottement sur acm1


cfrott_acm1=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,abs(wacm1_elec1));
cfrott_acm1_elec2=zeros(1,length(VD.CYCL.temps));

cfrott_acm1_elec2(I)=max(cfrott_acm1(I),cprim_red(I)*VD.EPI.rend_train/(1-VD.EPI.kb));
cacm1_elec2(I)=cfrott_acm1_elec2(I);
qacm1_elec2(I)=0;
wacm1_elec2(I)=wacm1_elec1(I);
cacm2_elec2(I)=0;
qacm2_elec2(I)=0;
wacm2_elec2(I)=0;
cprim_red_elec2(I)=(1-VD.EPI.kb)/VD.EPI.rend_train*cacm1_elec2(I);
cfrott_acm1_elec2(I)=cacm1_elec2(I);

[ERR,ubat_max_elec2,~,~,~,~,RdF_elec1]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat_max_elec2,100-VD.INIT.Dod0,0);

if param.verbose>=3
    assignin('base','Pelec_mat',Pelec_mat);
    assignin('base','pbat_max',pbat_max);
    assignin('base','pbat_min',pbat_min);
    assignin('base','Ibat_min_calc',Ibat_min_calc);
    assignin('base','Ubat_min_calc',Ubat_min_calc);
    assignin('base','cprim_red_mat',cprim_red_mat);
    assignin('base','cps_mat',cps_mat);
    assignin('base','wprim_red_mat',wprim_red_mat);
    assignin('base','cprim_red',cprim_red);
    assignin('base','wprim_red',wprim_red);
    assignin('base','ind_min_elec2',ind_min_elec2);
    
    assignin('base','cprim_red_elec2',cprim_red_elec2);
    
    assignin('base','cfrott_acm1_elec2',cfrott_acm1_elec2);
    
    assignin('base','ibat_max_elec2_mat',ibat_max_elec2_mat);
    assignin('base','ibat_max_elec2',ibat_max_elec2);
    assignin('base','ubat_max_elec2',ubat_max_elec2);
    assignin('base','cacm1_elec2_mat',cacm1_elec2_mat);
    assignin('base','cacm2_elec2_mat',cacm2_elec2_mat);
    assignin('base','wacm1_elec2_mat',wacm1_elec2_mat);
    assignin('base','wacm2_elec2_mat',wacm2_elec2_mat);
    assignin('base','qacm1_elec2_mat',qacm1_elec2_mat);
    assignin('base','qacm2_elec2_mat',qacm2_elec2_mat);
    
    assignin('base','cacm1_elec2',cacm1_elec2);
    assignin('base','cacm2_elec2',cacm2_elec2);
    assignin('base','wacm1_elec2',wacm1_elec2);
    assignin('base','wacm2_elec2',wacm2_elec2);
    assignin('base','qacm1_elec2',qacm1_elec2);
    assignin('base','qacm2_elec2',qacm2_elec2);
    
    assignin('base','elec2_pos_mat',elec2_pos_mat);
    assignin('base','elec1_pos',elec1_pos);
    assignin('base','elec2_pos',elec2_pos);
    
    bilan_pelec1=ibat_max_elec1.*ubat_max_elec1-(cacm1_elec1-cfrott_acm1_elec1).*wacm1_elec1-qacm1_elec1-pacc;
    bilan_pelec2=ibat_max_elec2.*ubat_max_elec2-((cacm1_elec2-cfrott_acm1_elec2).*wacm1_elec2+qacm1_elec2+cacm2_elec2.*wacm2_elec2+qacm2_elec2+pacc);
     
    bilan_pelec1(elec1_pos==0)=NaN;
    
    figure(150)
    clf
    plot(VD.CYCL.temps,bilan_pelec1,VD.CYCL.temps,bilan_pelec2)
    legend('bilan pelec1','bilan pelec2')
    title('bilan de puissance elec pendant le calcul des limites')
    
    assignin('base','bilan_pelec1',bilan_pelec1);
    assignin('base','bilan_pelec2',bilan_pelec2);
    
end

% On fait maintenant le choix du mode elec
    
if isfield(param,'elec_split') & param.elec_split==1   
    [ibat_max,ind_ibat_max]=min([ibat_max_elec1; ibat_max_elec2]);
    
    I1=find(ind_ibat_max==1);
    I2=find(ind_ibat_max==2);
    
else
    I1=1:1:length(VD.CYCL.temps);
    I2=ind_elec1_np;
    I1(ind_elec1_np)=[];
    ibat_max(I1)=ibat_max_elec1(I1);
    ibat_max(I2)=ibat_max_elec2(I2);
end

elec_pos(I1)=elec1_pos(I1);
elec_pos(I2)=elec2_pos(I2);

ubat_max(I1)=ubat_max_elec1(I1);
ubat_max(I2)=ubat_max_elec2(I2);

qacm1_elec(I1)=qacm1_elec1(I1);
qacm1_elec(I2)=qacm1_elec2(I2);

qacm2_elec(I1)=qacm2_elec1(I1);
qacm2_elec(I2)=qacm2_elec2(I2);

cacm1_elec(I1)=cacm1_elec1(I1);
cacm1_elec(I2)=cacm1_elec2(I2);

cacm2_elec(I1)=cacm2_elec1(I1);
cacm2_elec(I2)=cacm2_elec2(I2);

wacm1_elec(I1)=wacm1_elec1(I1);
wacm1_elec(I2)=wacm1_elec2(I2);

wacm2_elec(I1)=wacm2_elec1(I1);
wacm2_elec(I2)=wacm2_elec2(I2);

RdF(I1)=RdF_elec1(I1);
RdF(I2)=RdF_elec1(I2);

cfrott_acm1(I1)=cfrott_acm1_elec1(I1);
cfrott_acm1(I2)=cfrott_acm1_elec2(I2);

cprim_red(I1)=cprim_red_elec1(I1);
cprim_red(I2)=cprim_red_elec2(I2);



soc_min(1)=100-VD.INIT.Dod0;
Dsoc_min(1)=0;
for ii=2:length(VD.CYCL.temps)
    soc_min(ii)=soc_min(ii-1)-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_min(ii)=-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

if param.verbose>=3
     assignin('base','ubat_max',ubat_max)
     assignin('base','wmt_min',wmt_min)
     assignin('base','elec_pos',elec_pos)
     if exist('I1'),assignin('base','I1',I1),end
     if exist('I2'),assignin('base','I2',I2),end
end

if param.verbose>=2
    assignin('base','ibat_max',ibat_max)
    assignin('base','cprim_red',cprim_red)
    assignin('base','wprim_red',wprim_red)
    
    figure(3)
    clf
    hold on
    plot(VD.CYCL.temps,ibat_max,'b',VD.CYCL.temps,VD.CYCL.vitesse,'k')
    grid
    
    figure(4)
    clf
    hold on
    plot(VD.CYCL.temps,soc_min,'b',VD.CYCL.temps,100-VD.INIT.Dod0+Dsoc_min,'g')
    legend('soc min','Dsoc min')
    grid
end


%% recherche de la pbat_min (mode hybride)la

% Calcul des vitesse max possible du moth

% Calcul de la vitesse max possible du moteur en mode split
wmt_max=min(VD.MOTH.wmt_maxi, -((1-VD.EPI.kb)*wprim_red-wacm1_min)/VD.EPI.kb); % limitation sur vit max du moth et vit max de acm1
wmt_max=min(wmt_max,wprim_red*(1-VD.EPI.Nsat/VD.EPI.Ncour)+VD.ECU.wmaxps*VD.EPI.Nsat/VD.EPI.Ncour); % limitation sur la vitesse max au niveau satellite / porte satellite 
wmt_max=min(wmt_max,wacm2_max); %

% calcul du couple max moteur thermique
% il faut verifier la possibilité de faire de l'hybride serie, ou de
% l'hybride split
% Cas hybride séris il faut que cacm1 suffisent à fournir le couple au
% reducteur
cacm1_max=max(VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:)))); % on prend le max absolue même si en fait on devrait regarder les vitesses)
int_hs=find(cacm1_max>=cprim_red./((1-VD.EPI.kb)*VD.EPI.rend_train.^sign(cprim_red)));
int_hs_n=find(cacm1_max<cprim_red./((1-VD.EPI.kb)*VD.EPI.rend_train.^sign(cprim_red)));

% on recherche la puissance max possible du moteur thermique
[~,i_pmt_max]=max(VD.MOTH.pmt_max);
wmt_pmax=VD.MOTH.wmt_max(i_pmt_max);

% Cas hybride serie
pmax_mt=interp1(VD.MOTH.wmt_max,VD.MOTH.pmt_max,wmt_pmax);
cmt_max=pmax_mt./wmt_pmax;
cacm2_max=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wmt_pmax);
cmt_max_hs=min(cmt_max,cacm2_max);
%cmt_max_hs(int_hs_n)=0;


% connexion
cacm2_hs=-cmt_max_hs;
wacm2_hs=wmt_pmax;

[ERR,qacm2_hs]=calc_pertes_acm(ERR,VD.ACM2,cacm2_hs,wacm2_hs,0);
pelecacm2_hs=cacm2_hs.*wacm2_hs+qacm2_hs;

[ERR,cacm1_hs,~,wacm1_hs]=calc_train_epi(ERR,VD.EPI,cprim_red,wprim_red,zeros(ii_max),2);

[ERR,qacm1_hs]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hs,wacm1_hs,zeros(ii_max));
pelecacm1_hs=cacm1_hs.*wacm1_hs+qacm1_hs;

% calcul de ibatmin
pres_hs=pelecacm1_hs+pelecacm2_hs+pacc; % Normalement il y as un survolteur entre pbat et pres !!
pbat_hs=pres_hs;
[ERR,ibat_min_hs,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_hs,100-VD.INIT.Dod0,0,-1,0,param); 
ibat_min_hs(isnan(ibat_min_hs))=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0);

ibat_min_hs(int_hs_n)=NaN;

% Cas hybrid split
wmt_sp=min(wmt_max,wmt_pmax);
wacm2_sp=wmt_sp;
[ERR,cacm1_sp,ccour_sp,wacm1_sp]=calc_train_epi(ERR,VD.EPI,cprim_red,wprim_red,wacm2_sp,2);

% cmt est limité par les couples max de acm2 et acm1
cacm2_max_sp=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wmt_sp);

% On suppose ici que acm1 pas limitant on en tiendra compte edans le calcul
% du coup des arcs

cmt_max_sp=max(cmt_max,ccour_sp-cacm2_max_sp);

cacm2_sp=ccour_sp-cmt_max_sp;
[ERR,qacm2_sp]=calc_pertes_acm(ERR,VD.ACM2,cacm2_sp,wacm2_sp,0);
pelecacm2_sp=cacm2_sp.*wacm2_sp+qacm2_sp;

% On limite cacm1 a sa valeur max (acm1 pas limitant dans ce calcul)
cacm1_max_sp=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_sp);
cacm1_sp=min(cacm1_sp,cacm1_max_sp);
[ERR,qacm1_sp]=calc_pertes_acm(ERR,VD.ACM1,cacm1_sp,wacm1_sp,0);
pelecacm1_sp=cacm1_sp.*wacm1_sp+qacm1_sp;

% calcul de ibatmin
pres_sp=pelecacm1_sp+pelecacm2_sp+pacc; % Normalement il y as un survolteur entre pbat et pres !!
pbat_sp=pres_sp;
[ERR,ibat_min_sp,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_sp,100-VD.INIT.Dod0,0,-1,0,param); 
ibat_min_sp(isnan(ibat_min_sp))=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0);

if isfield(param,'hybrid_serie') & param.hybrid_serie==1
    ibat_min=min(ibat_min_sp,ibat_min_hs);
else
    ibat_min=ibat_min_sp;
end

if param.verbose>=3
    assignin('base','ibat_min_sp',ibat_min_sp)
    assignin('base','ibat_min_hs',ibat_min_hs)
end

% On verifie quel'on passe le cycle
% On ne verifie pas que les puissances suffisent il peut rester des cas 
% ou avec les pertes on ne passe pas
ind=find(Ibat_max_calc*Ubat_max_calc+pmax_mt<cprim_red.*wprim_red);

% indice ou hybride  impossible 
if ~isempty(ind)
    chaine=' Les performances des machines ne permettent pas de passer le cycle aux instant suivants : \n %s \n';
    ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine,num2str(ind));
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
end


% Si on arrive à ibat_min>ibat_max (peut arriver en recup max si les
% limites sont atteintes) on impose ibat_min au min <=ibat_max
ibat_min(ibat_min>ibat_max)=ibat_max(ibat_min>ibat_max);

soc_max(1)=100-VD.INIT.Dod0;
Dsoc_max(1)=0;

for ii=2:length(VD.CYCL.temps)
    soc_max(ii)=soc_max(ii-1)-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_max(ii)=-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

if sum(ibat_max<ibat_min)>0 ||  sum(soc_max<soc_min)>0 || sum(Dsoc_max<Dsoc_min)>0
    chaine='error in calc_limites_HPDP_EPI_SURV' ;
    ERR=MException('BackwardModel:calc_HPDP_EPI_SURV',chaine);
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    return

end

if param.verbose>=2
    assignin('base','ibat_min',ibat_min)

    figure(3)
    plot(VD.CYCL.temps,ibat_min,'g')
    legend('ibat max','vitesse','ibat min')
    title('courant min max fin calc limite')
    
    figure(4)
    plot(VD.CYCL.temps,soc_max,VD.CYCL.temps,Dsoc_max+60)
    legend('soc min','Dsoc min','soc max','Dsoc max')
    title('soc min max fin calc limite')

end
