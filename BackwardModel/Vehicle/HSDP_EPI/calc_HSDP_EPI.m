% function [ERR, VD, ResXml]=calc_HSP_2EMB(ERR,vehlib,param,VD)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul d'un vehicule electrique de type hybrid parallele
% a derivation de puissance
%
% Arguments appel :
% Arguments de sortie :
%
% 23-07-12(EV): Creation 

function [ERR, VD, ResXml]=calc_HSDP_EPI(ERR,vehlib,param,VD)
global Ham_hors_limite;
Ham_hors_limite=1e6;

% Verif sur les rapport de redution et les vitesses max
Vmax_veh=160;
wroue_max=Vmax_veh/3.6/VD.VEHI.Rpneu;
wprim_red_max=wroue_max*VD.RED.kred;
wacm1_max_elec1=(1-VD.EPI.kb)*wprim_red_max; % cas mode elec avec acm1 seul
wmoth_min_vmax=2500;
wacm1_max=(1-VD.EPI.kb)*wprim_red_max+VD.EPI.kb*wmoth_min_vmax*pi/30;


if wacm1_max>VD.ECU.wmaxge
    chaine='les rapport de rductions ne permettent pas d''atteindre la vit max veh  \n Vmax veh : %f' ;
    warning('BackwardModel:calc_cout_arc_HPDP_EVT_SURV',chaine,Vmax_veh);
    pause
end
% Calcul cinematiques complementaires
% Avec re-echantillonnage eventuel
if ~isfield(param,'pas_temps') | param.pas_temps==0
    param.pas_temps=1; % si param_pas temps n'existe pas ou =0 on reechantillone ? 1s
end
[ERR,VD,dist]=calc_cinemat(ERR,VD,param);

if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Initialisation
soc=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));
E=zeros(size(VD.CYCL.temps));
R=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
pbat=zeros(size(VD.CYCL.temps));

% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,penteFpk,fres,faero,froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Connexion
csec_red=croue;
wsec_red=wroue;
dwsec_red=dwroue;

% Calcul des conditions en amont du reducteur
[ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);
if ~isempty(ERR)
    ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Prise en compte du frein meca quand Croue <0, 
% Les relations sur les couples au niveau du train epi sont les
% m??mes que emb1 soit ouvert ou ferm??e.

% %connexion 
% cps=cprim_red;
% wps=wprim_red;
% wcour=zeros(1,length(VD.CYCL.temps)); %si EMB1 ferm??
% 
% [ERR,cacm1,ccour,wacm1]=calc_train_epi(ERR,VD.EPI,cps,wps,wcour);
% 
% dwacm1=zeros(1,length(VD.CYCL.temps));
% 
% wacm2=wprim_red;
% dwacm2=dwprim_red;
% ctotal=cprim_red+VD.ACM1.J_mg*dwacm1+VD.ACM2.J_mg*dwacm2;
% 
% 
% 
% % On vient limiter ctotal a sa valeur min
% ctotal=max(ctotal,interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1)+interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmin_mot(1,:))),wacm2));
% 
% if isfield('Vit_Cmin')
%     ctotal=max(ctotal,interp1(VD.ECU.Vit_Cmin,VD.ECU.Cpl_min,wacm1));
% end
% 
% % On limite le couple red en consequence
% cprim_red=ctotal-VD.ACM1.J_mg*dwacm1-VD.ACM2.J_mg*dwacm2;



%% strategie selon les cas (doivent renvoyer des vecteurs cprim2_cpl et
%% elhyb de taille VD.CYCL.temps)
if strcmp(lower(param.optim),'ligne')  
    %[ERR,cmt,wmt,elhyb]=calc_ligne_HPDP_EVT_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
elseif strcmp(lower(param.optim),'lagrange')
    %[ERR,cmt,wmt,elhyb]=calc_lagrange_HPDP_EVT_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
elseif strcmp(lower(param.optim),'prog_dyn')
     [ERR,VD,r]=calc_prog_dyn_HSDP_EPI(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
end

r_var_sup={'croue','wroue','wprim_red','dwprim_red','csec_red','wsec_red','dwsec_red'};
[ERR,r]=affect_struct(r_var_sup,r);  

if ~isempty(ERR)
    ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2,NaN);
    return;
end
   

% Recalcul de la partie frein meca au niveau de la roue
[ERR,r.csec_red]=calc_red_fw(ERR,VD.RED,r.cprim_red,wprim_red,dwprim_red,0);
r.cfrein_meca=r.csec_red-r.croue;

% Calcul des auxiliaires electriques
[ERR,r.pacc]=calc_acc(VD);
if ~isempty(ERR)
    ResXml=struct([]); %     [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end


%% recalcul des couples du aux freinages meca si on a choisit
% de ne pas aliment?? ma machines en fin de freinage
% (cfrein_meca_pelec_mot=1)
r.cprim_red(r.cfrein_meca_pelec_mot==1)=r.cfrott_acm1(r.cfrein_meca_pelec_mot==1)-VD.ACM1.J_mg*r.dwacm1(r.cfrein_meca_pelec_mot==1);
[ERR,r.csec_red(r.cfrein_meca_pelec_mot~=0)]=calc_red_fw(ERR,VD.RED,r.cprim_red(r.cfrein_meca_pelec_mot~=0),wprim_red(r.cfrein_meca_pelec_mot~=0),dwprim_red(r.cfrein_meca_pelec_mot~=0),0);
r.cfrein_meca(r.cfrein_meca_pelec_mot~=0)=r.csec_red(r.cfrein_meca_pelec_mot~=0)-croue(r.cfrein_meca_pelec_mot~=0);

r.qacm1_frott=-r.cfrott_acm1.*r.wacm1;
r.qacm1=r.qacm1+r.qacm1_frott;

% Cycle
r.tsim=VD.CYCL.temps;
r.vit=VD.CYCL.vitesse;
r.acc=VD.CYCL.accel;
r.vit_dem=r.vit;
r.pente=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,VD.CYCL.distance,'linear',0); % reechantillonage pente fonction distance claculee.  
r.masse=masse;

VD.EPI.sens='hsdp';
r.wcour_train(r.elhyb==0)=r.wacm2(r.elhyb==0);
r.wcour_train(r.elhyb==0.05)=r.wacm2(r.elhyb==0.05);
r.wcour_train(r.elhyb==1)=0;
r.wcour_train(r.elhyb==2)=r.wmt(r.elhyb==2);
r.cps_train=r.cprim_red;
r.wps_train=r.wprim_red;
r.ccour_train=-r.cprim_red.*VD.EPI.kb./((1-VD.EPI.kb).*(VD.EPI.rend_train.^sign(r.cprim_red.*r.wprim_red)));
r.csoleil_train=r.cacm1;
r.wsoleil_train=r.wacm1;

[ResXml]=miseEnForme4VEHLIB(vehlib,'caller','r');

% Calcul de resultats synthetiques
[Res]=resultats(VD,param,ResXml);
conso100=Res.conso100;
cumcarb=Res.cumcarb;
co2_eq=Res.co2_eq;
CO2_gkm=Res.CO2_gkm;
ibat_eff=Res.ibat_eff;
ibat_moy=Res.ibat_moy;

% Bilan de puissance et d'energie
[bp]= bilanPE(VD,param,ResXml);
bilanP=bp.bilanP;

% Ajout des grandeurs synthétiques dans une nouvelle table de la structure xml
[ResXml]=miseEnForme4GestionEnergie(ResXml);


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [nrj]=critere_cvgce(ud,uf)

nrj=0.5*VD.SC.C.*(uf.^2-ud.^2).*VD.SC.Nblocser.*VD.SC.Nbranchepar/3600;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,cmt,wmt,elhyb]=calc_ligne_HPDP_EVT_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,cmt,wmt,elhyb]=calc_lagrange_HPDP_EVT_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);


return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%??
function [ERR,VD,r]=calc_prog_dyn_HSDP_EPI(ERR,param,VD,cprim_red,wprim_red,dwprim_red)
    
    
%elhyb,ibat,soc,ubat,R,E,RdF,dcarb,cprim_red,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,cfrott_acm1,cfrein_meca_pelec_mot]=...
      
r_var={'elhyb','soc','ibat_max','ibat_min','elec_pos','soc_min','soc_max','Dsoc_min','Dsoc_max','cprim_red'...
    'lim_inf','lim_sup','lim_elec','soc_inf','soc_sup'...
    'poids_min','meilleur_chemin'};   

[ERR,VD.ACM2]=inverse_carto_melec(VD.ACM2,0);
%assignin('base','VD.ACM2');

[ERR,VD.ACM1]=inverse_carto_melec(VD.ACM1,0);
%assignin('base','VD.ACM1');

tdeb_calc_limites=cputime;

[ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,cacm1_elec,wacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,qacm2_elec,cprim_red,cfrott_acm1]=...
    calc_limites_HSDP_EPI(ERR,param,VD,cprim_red,wprim_red,dwprim_red);

% verification de la precision du graph pour forcer le nombre
% d'arc sur un pas de temps a une valeur min (sinon peut devenir 0)

if isfield(param,'Nb_arc_min') & param.Nb_arc_min~=0 & abs(Dsoc_min(end)-Dsoc_max(end))~=0
    if (abs(Dsoc_min(end)-Dsoc_max(end))/param.Nb_arc_min < -param.prec_graphe) | (param.prec_graphe==0) | ~isfield(param,'prec_graphe')
        val_prec=-abs(Dsoc_min(end)-Dsoc_max(end))/param.Nb_arc_min;
        param.prec_graphe=val_prec;
        assignin('base','val_prec',val_prec);
        evalin('base','param.prec_graphe=val_prec;');
        if isfield(param,'prec_graphe') & param.prec_graphe~=0
              chaine='Graph precision automatically reduce by param.Nb_arc_min \ngraph precision : %f' ;
              warning('BackwardModel:calc_cout_arc_HPDP_EVT_SURV',chaine,param.prec_graphe);
        end
    end
end

[ ERR,lim_inf,lim_sup,lim_elec,soc_inf,soc_sup] =limite2indice_soc(VD,soc_min,soc_max,param);

tfin_calc_limites=cputime;

if ~isempty(ERR)
    chaine='error in limite2indice_soc' ;
    ERR=MException('BackwardModel:calc_HSP_2EMB',chaine);
    r=[];
    return
end

% Information sur le graphe
if param.verbose>=1
    info_graph(VD,param,Dsoc_min,Dsoc_max,lim_sup,lim_inf);
end


var_com_inf=Dsoc_min;
var_com_sup=Dsoc_max;

tdeb_resolution=cputime;

if isfield(param,'calc_mat') & param.calc_mat==1
    [ERR,poids_min,meilleur_chemin]= ...
        solve_graph_mat(ERR,@(ERR,Dsoc) calc_cout_arc_HSDP_EPI_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,100-VD.INIT.Dod0),...
        param,lim_sup,lim_inf,var_com_inf,var_com_sup,0,lim_elec);
else
    
end

tfin_resolution=cputime;

% cas ou les meilleurs chemin sont inf, on sort en erreur
if sum(poids_min)==Inf
   chaine=' Attention les poids min sont infinis %6.3f%6.3f \n Essayer de diminuer la precision du graph';
   ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine,poids_min(1),poids_min(2));
   r=[];
   return;
end

if isnan(poids_min) | ~isempty(ERR)
    chaine='min weight equal to NaN or ERR non empty after solve_graph ' ;
    warning('BackwardModel:calc_cout_arc_HPDP_EVT',chaine);
    r=[];
    return
end


assignin('base','meilleur_chemin',meilleur_chemin)
assignin('base','poids_min',poids_min)

tdeb_recalcul=cputime;

% passage chemin aux var de commande
% NB il faut appeler calc_cout_arc pour des soucis d'accrochage sur arc tout thermique
if isfield(param,'calc_mat') & param.calc_mat==1
    [ERR,Dsoc,elhyb]=chemin2varcomBATT_mat(ERR,@(ERR,Dsoc) calc_cout_arc_HSDP_EPI_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,100-VD.INIT.Dod0),...
        param,lim_sup,lim_inf,lim_elec,var_com_inf,var_com_sup,meilleur_chemin(1,:),elec_pos);
else
end
assignin('base','Dsoc',Dsoc)
assignin('base','elhyb',elhyb)
assignin('base','elec_pos',elec_pos)

% Recalcul de toutes les grandeurs du vehicule.
if param.verbose>=1
    if isfield(param,'calc_mat') & param.calc_mat==1
        h = msgbox('recalcul des grandeurs','wait');
    else
        h = waitbar(0,'recalcul des grandeurs');
    end
end


if isfield(param,'calc_mat') & param.calc_mat==1
    [ERR,~,r]=calc_cout_arc_HSDP_EPI_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,100-VD.INIT.Dod0,1,elhyb,cacm1_elec,wacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,qacm2_elec,cfrott_acm1);
else

end

if param.verbose>=1
    close(h); % fermeture waitbar
end

tfin_recalcul=cputime;
if param.verbose>=1 | param.verbose==-1
    fprintf(1,'%s %.2f %s \n','temps de calcul des limites :',tfin_calc_limites-tdeb_calc_limites,'s');
    fprintf(1,'%s %.2f %s \n','temps de resolution du graph calc arc compris :',tfin_resolution-tdeb_resolution,'s');
    fprintf(1,'%s %.2f %s \n','temps de recalcul:',tfin_recalcul-tdeb_recalcul,'s');
    fprintf(1,'%s %.2f %s \n','temps global:',tfin_recalcul-tdeb_calc_limites,'s');
end

% Ne pas calculer les soc par trapz mais par des sommes.
r.soc(1)=100-VD.INIT.Dod0;

for i=2:1:length(Dsoc)
    r.soc(i)=r.soc(i-1)+r.Dsoc(i);
end

[ERR,r]=affect_struct(r_var,r);

if param.verbose>=2
    figure(1)
    clf
    plot(VD.CYCL.temps,r.ibat_max,VD.CYCL.temps,r.ibat_min,VD.CYCL.temps,r.ibat,VD.CYCL.temps,VD.CYCL.vitesse*3.6)
    grid
    ylabel('courant en A')
    legend('ibat max','ibat min','ibat','vitesse')
    
    figure(2)
    clf
    hold on
    plot(VD.CYCL.temps,r.soc_min,VD.CYCL.temps,r.soc_max,VD.CYCL.temps,r.soc_inf,VD.CYCL.temps,soc_sup)
    legend('soc min','soc max','soc inf','soc sup')
    title('limites sur le soc')
    
    figure(3)
    clf
    hold on
    plot(VD.CYCL.temps+1,r.meilleur_chemin,VD.CYCL.temps+1,lim_inf,VD.CYCL.temps+1,lim_sup,VD.CYCL.temps+1,r.lim_elec,VD.CYCL.temps+1,VD.CYCL.vitesse*36)
    legend('meilleur chemin 1','meilleur chemin 2','lim inf','lim sup','lim elec','vitesse')
    title('meilleur chemin en indice')
    grid
end  

if param.verbose>=1   
    figure(4)
    clf
    hold on            
    plot(VD.CYCL.temps,r.soc_inf,'k',VD.CYCL.temps,r.soc_sup,'k',VD.CYCL.temps,r.soc,'b',VD.CYCL.temps,VD.CYCL.vitesse*3.6,'c')  
    legend('soc inf','soc sup''soc','vit')
    title('meilleur chemin en soc');
    grid
   
    figure(5)
    clf
    hold on
    plot(VD.CYCL.temps,r.ibat,VD.CYCL.temps,r.ubat,VD.CYCL.temps,r.dcarb)
    legend('ibat','ubat','dcarb')
    grid
end

return

