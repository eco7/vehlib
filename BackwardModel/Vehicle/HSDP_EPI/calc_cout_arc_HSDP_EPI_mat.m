%function [ERR,cout,ibat,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,Dsoc,elhyb,cfrott_acm1,cfrein_meca_pelec_mot]=...
%    calc_cout_arc_HSP_2EMB_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,soc_p,recalcul,elhyb,cacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,dwacm2_elec,qacm2_elec,cfrott_acm1)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Calcul des cout des arcs cas HSp_2EMB
%
% interface entree :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discretisation etc)
% CYLC,VD.ADCPL...:structures de description vehicule et composants
% cprim_red :   Couple sur le primaire du reducteur (vecteur sur le cycle)
% wprim_red :   vitesse sur le primaire du reducteur  (vecteur sur le cycle)
% dprim_red :   derivee de la vitesse sur le primaire du reducteur (vecteur sur le cycle)
% Dsoc :        variation de soc sur l'eventail des arcs possible
% elec_pos :    mode electrique possible (vecteur sur le cycle)
% soc_p :       soc a l'instant precedent
% i :           instant dans le cycle
% recalcul :    si on est en phase de recalcul (grahe deja resolu) recalcul =1 sinon 0 ou pas passer dans l'interface
% elhyb :       mode elec/hybride/thermique en phase recalcul uniquement
% cacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,dwacm2_elec,qacm2_elec,cfrott_acm1 : en recalcul on utilise les valeurs
%
% interface de sortie
% cout :        cout des arcs de l'eventail
% ibat,ubat ...:variable vehicule utile uniquement en recalcul sinon on ne renvoie que le cout
%
% 23-07-12(EV): Creation 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ERR,cout,r]=calc_cout_arc_HSDP_EPI_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,soc_p,recalcul,elhyb,...
    cacm1_elec,wacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,qacm2_elec,cfrott_acm1)


%cout,ibat,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,Dsoc,elhyb,cfrott_acm1,cfrein_meca_pelec_mot]=...  
r_var={'ibat','ubat','Rbat','u0bat','RdF','dcarb','cmt','wmt','dwmt','cacm1','wacm1','dwacm1','qacm1',...
    'cacm2','wacm2','dwacm2','qacm2','Dsoc','elhyb','cfrott_acm1','cfrein_meca_pelec_mot'};


[Li,Co]=size(soc_p);
[sx,sc]=size(Dsoc);

if sum(sum(isnan(soc_p)))==Li*Co
    ERR=MException('BackwardModel:calc_cout_arc_HSDP_EPI', 'appel a calc_cout_arc_HSDP_EPI : tout les elements d''un des vecteur precedent a NaN');
    [cout,ibat,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,Dsoc,elhyb,cfrott_acm1,cfrein_meca_pelec_mot]=backward_erreur(size(Dsoc));
    return
end

ii_max=size(VD.CYCL.temps);

% Calcul de la vitesse min possible du moteur thermique pour verifier que le mode tout elec est possible
wmt_min=0*ones(size(wprim_red)); % limitation sur vit max du moth et vit max de la generatrice

% Calcul des vitesses max des machines
I_wacm1_max=min(find(VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:)))~=0,1,'last'),length(VD.ACM1.Regmot_pert));
wacm1_max= min(VD.ACM1.Regmot_cmax(I_wacm1_max),VD.ECU.wmaxge);

I_wacm1_min=min(find(VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:)))~=0,1,'last'),length(VD.ACM1.Regmot_pert));
wacm1_min= -min(VD.ACM1.Regmot_cmax(I_wacm1_min),VD.ECU.wmaxge);

I_wacm2_max=min(find(VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:)))~=0,1,'last'),length(VD.ACM1.Regmot_pert));
wacm2_max= min(VD.ACM2.Regmot_cmax(I_wacm2_max),VD.ECU.wmaxge);

I_wacm2_min=min(find(VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmin_mot(1,:)))~=0,1,'last'),length(VD.ACM2.Regmot_pert));
wacm2_min= -min(VD.ACM2.Regmot_cmax(I_wacm2_min),VD.ECU.wmaxge);

if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
    wmt_min(wprim_red>VD.ECU.vmaxele/VD.VEHI.Rpneu*VD.RED.kred)=VD.MOTH.ral;
end

% calcul batterie
% calcul ibat ?? partir de Dsoc
RdF=ones(size(Dsoc));
RdF(Dsoc>0)=VD.BATT.RdFarad;
ibat=-Dsoc./RdF*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100/param.pas_temps;


[ERR,ubat,~,~,E,R]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat,soc_p,0);

pbat=ubat.*ibat;

[ERR,pacc]=calc_acc(VD);
pacc_mat=pacc(1);


if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
    wmt_min(wprim_red>VD.ECU.vmaxele/VD.VEHI.Rpneu*VD.RED.kred)=VD.MOTH.ral;
end


if (isfield(param,'fit_pertes') & param.fit_pertes==1)
elseif ~isfield(param,'fit_pertes') | param.fit_pertes~=1
    
    %% calcul du cout des arcs dans le mode hybride serie
    % connexion
    cps=cprim_red;
    wps=wprim_red;
    wcour=zeros(ii_max);
    [ERR,cacm1_hs,ccour_hs,wacm1_hs]=calc_train_epi(ERR,VD.EPI,cps,wps,wcour,2);
    
    cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_hs);
    cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1_hs);
    cfrott_acm1_hp=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,abs(wacm1_hs));
    
    [VD]=ge_optimum_perte(VD,25,25,0); % recherche courbe optimal du groupe
    [ERR,qacm1_hs]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hs,wacm1_hs,zeros(ii_max));
    pelecacm1_hs=cacm1_hs.*wacm1_hs+qacm1_hs;
    pge=repmat(pelecacm1_hs,sx,1)-pbat+pacc_mat;
    
    wacm2_hs=interp1(VD.GE.pge_opti,VD.GE.wmt_opti,pge);
    qacm2_hs=interp1(VD.GE.pge_opti,VD.GE.qacm2_opti,pge);
    cacm2_hs=-(pge+qacm2_hs)./wacm2_hs;
    cmt_hs=-cacm2_hs;
    wmt_hs=wacm2_hs;
    [ERR,dcarb_hs]=calc_mt(ERR,VD.MOTH,cmt_hs,wmt_hs,0,1,0);
    
    
    % On met ?? inf les cas ou la puissance du groupe serait <0 ou > a pge.max
    ind_ge_n=find(pge<0 & pge>VD.GE.pge_opti(end));
    dcarb_hs(ind_ge_n)=Inf;
    
    
    % On met ?? inf les cas ou le mode hybride serie est impossible car
    % cacm1>cacm1_max
    ind_hs_n=find(cacm1_hs>cacm1_max);
    dcarb_hs(ind_hs_n)=Inf;
    
    if param.verbose>=3
        assignin('base','pelecacm1_hs',pelecacm1_hs);
        assignin('base','pge',pge);
        assignin('base','cmt_hs',cmt_hs);
        assignin('base','wmt_hs',wmt_hs);
        assignin('base','wacm2_hs',wacm2_hs);
        assignin('base','wacm1_hs',wacm1_hs);
        
        assignin('base','cacm1_hs',cacm1_hs);
        assignin('base','cacm1_max_hs',cacm1_max);
        
        assignin('base','qacm1_hs',qacm1_hs);
        assignin('base','cacm2_hs',cacm2_hs);
        assignin('base','qacm2_hs',qacm2_hs);
        
        
        assignin('base','dcarb_hs',dcarb_hs);
    end
    
    %% calcul du cout des arcs dans le cas hybride split
    
    % Calcul des vitesses max et min de rotation du moteur thermique
    % Grace aux lignes suivantes les vitesses max de la g??n?? et des portes
    % satellites ne peuvent normalement pas ??tre atteinte
    
    % Calcul de la vitesse max possible du moteur en mode split
    wmt_max=min(VD.MOTH.wmt_maxi,-((1-VD.EPI.kb)*wprim_red-wacm1_min)/VD.EPI.kb); % limitation sur vit max du moth et vit max de acm1
    wmt_max=min(wmt_max,wprim_red*(1-VD.EPI.Nsat/VD.EPI.Ncour)+VD.ECU.wmaxps*VD.EPI.Nsat/VD.EPI.Ncour); % limitation sur la vitesse max au niveau satellite / porte satellite
    wmt_max=min(wmt_max,wacm2_max);
    
    wmt_min=max(0,-((1-VD.EPI.kb)*wprim_red-wacm1_max)/VD.EPI.kb); % limitation sur vit max du moth et vit max de acm1
    wmt_min=max(wmt_min,wprim_red*(1-VD.EPI.Nsat/VD.EPI.Ncour)-VD.ECU.wmaxps*VD.EPI.Nsat/VD.EPI.Ncour); % limitation sur la vitesse max au niveau satellite / porte satellite
    wmt_min=max(wmt_min,wacm2_min);
    
    % Le moteur ne peut pas tourner en dessous de sa vitesse de ral
    wmt_min(wmt_min~=0)=max(VD.MOTH.ral,wmt_min(wmt_min~=0));
    
    if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
        wmt_min(wprim_red>VD.ECU.vmaxele/VD.VEHI.Rpneu*VD.RED.kred)=VD.MOTH.ral;
    end
   

    % creation des tables sur les vitesses et couple moteur
    wmt_mat=NaN*ones(floor(max((wmt_max-wmt_min)/param.pas_wmt))+2,sc);
    for ii=1:sc
        wmt_vec=max(VD.MOTH.ral,wmt_min(ii)):param.pas_wmt:wmt_max(ii);
        wmt_vec=[wmt_vec wmt_max(ii)]';
        wmt_mat(1:length(wmt_vec),ii)=wmt_vec;
    end
    cmt_vec(1,1,:)=[0:param.pas_cmt:max(max(VD.MOTH.cmt_max))  max(max(VD.MOTH.cmt_max))];
    [ERR,pacc]=calc_acc(VD);
    
    % a partir d'ici tout est des tableaux de taille
    % (nb_arc,length(wmt_vec),length(cmt_vec) ou des scalaires
    [sy,~]=size(wmt_mat);
    
    sz=length(cmt_vec);
    pbat=reshape(pbat,[1 sx 1 sc]);
    pbat=repmat(pbat,[sy 1 sz]);
   

    wmt_tab=repmat(reshape(wmt_mat,[sy 1 1 sc]),[1 1 sz]);
    cmt_tab=repmat(cmt_vec,[sy 1 1 sc]);
    I=find(~isnan(wmt_tab)&~isnan(cmt_tab));

    [ERR,~,cmt_tab(I)]=calc_mt(ERR,VD.MOTH,cmt_tab(I),wmt_tab(I),0,1,0,1);

    cprim_red_tab=repmat(reshape(cprim_red,[1 1 1 sc]),[sy 1 sz]);
    wprim_red_tab=repmat(reshape(wprim_red,[1 1 1 sc]),[sy 1 sz]);
    % calcul des pertes dans les machines
    [ERR,cacm1_tab,ccour_tab,wacm1_tab]=calc_train_epi(ERR,VD.EPI,cprim_red_tab,wprim_red_tab,wmt_tab,2);
    wacm2_tab= wmt_tab;
    cacm2_tab=ccour_tab-cmt_tab;
  
    % verification du couple max/min de acm2
    cacm2_max=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2_tab);
    cacm2_min=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmin_mot(1,:))),wacm2_tab);
    cacm2_tab(cacm2_tab>cacm2_max | cacm2_tab<cacm2_min)=NaN;
    
    % verification du couple max/min de acm1
    cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_tab);
    cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1_tab);
    cacm1_tab(cacm1_tab>cacm1_max | cacm1_tab<cacm1_min)=NaN;
   
    % calcul puissance elec acm1
    [ERR,qacm1_tab]=calc_pertes_acm(ERR,VD.ACM1,cacm1_tab,wacm1_tab,0,'1');
    
    pelecacm1_tab=cacm1_tab.*wacm1_tab+qacm1_tab;
    
    % calcul puissance elec acm2
    [ERR,qacm2_tab]=calc_pertes_acm(ERR,VD.ACM2,cacm2_tab,wacm2_tab,0,'1');
    pelecacm2_tab=cacm2_tab.*wacm2_tab+qacm2_tab;

    % calcul de la puisance batterie
    pres=pelecacm1_tab+pelecacm2_tab+repmat(reshape(pacc,[1 1 1 sc]),[sy 1 sz]);
    pbat_syst=pres; % puissance batterie calculee cote systeme
    
    pbat_syst=repmat(pbat_syst,[1 sx 1]);
    bilan_pbat=pbat-pbat_syst; %% Probl??me si toute une ligne est NaN et ou toute une ligne est <0 ou >0

    if param.verbose>=3
        assignin('base','bilan_pbat',bilan_pbat)
        assignin('base','qacm1_tab',qacm1_tab)
        assignin('base','cacm1_tab',cacm1_tab)
        assignin('base','wacm1_tab',wacm1_tab)
        
        assignin('base','qacm2_tab',qacm2_tab)
        assignin('base','cacm2_tab',cacm2_tab)
        assignin('base','wacm2_tab',wacm2_tab)
        assignin('base','pbat',pbat)
        assignin('base','pbat_syst',pbat_syst)
        assignin('base','pelecacm2_tab',pelecacm2_tab)
        assignin('base','pelecacm1_tab',pelecacm1_tab)
        assignin('base','sx',sx)
        assignin('base','sy',sy)
        assignin('base','sz',sz)
        assignin('base','wmt_mat',wmt_mat)
        assignin('base','wmt_tab',wmt_tab)
        assignin('base','cmt_tab',cmt_tab)
        assignin('base','wprim_red_tab',wprim_red_tab)
        assignin('base','cprim_red_tab',cprim_red_tab)
        
        assignin('base','ccour_tab',ccour_tab)
        assignin('base','ibat',ibat)
        assignin('base','ubat',ubat)
        assignin('base','Dsoc',Dsoc)
    end

    %% recherche du couple moteur qui boucle le bilan de puissance
    % pour chaque vitesse moteur et chaque courant (Dsoc) batterie
    % cf ancien fichier pour un ebauche de r??solution matricielle
    
    cmt_mat=NaN*ones(sy,sx,sc);
    bilan_vec=ones(1,sz);
    
    %% Il faut essayer de ne travailler a priori que sur les cas ou l'on a pas que des NaN

    sum_bilan_pbat=sum(~isnan(bilan_pbat),3);
    for hh=2:sc
        for ii=1:sy
            i_pos_vec=[];
            jj_vec=[];
            for jj=1:sx
                if (sum_bilan_pbat(ii,jj,hh))>0
                    bilan_vec(:)=bilan_pbat(ii,jj,:,hh);
                    i_pos=find(bilan_vec>0,1); % indices de la premi??re valeur positive
                    %i_neg=find(bilan_vec<0,1,'last'); % indices de la derni??re valeur negative
                    %if ~isempty(i_pos) && ~isempty(i_neg) && i_pos~=1
                    if ~isempty(i_pos) && i_pos~=1
                        % i_pos_vec(jj)=find(bilan_vec>0,1); % indices de la premi??re valeur positive
                        i_pos_vec=[i_pos_vec i_pos];
                        %i_neg_vec=[i_neg_vec i_neg];
                        jj_vec=[jj_vec jj];
                    end
                    
                end
            end
            if ~isempty(jj_vec)
                bilan_pbat_i_pos=bilan_pbat((hh-1)*sx*sy*sz+(i_pos_vec-1)*sx*sy+(jj_vec-1)*sy+ii);
                bilan_pbat_i_neg=bilan_pbat((hh-1)*sx*sy*sz+(i_pos_vec-2)*sx*sy+(jj_vec-1)*sy+ii);
                
                cmt_i_pos=cmt_tab((hh-1)*sy*sz+(i_pos_vec-1)*sy+ii);
                cmt_i_neg=cmt_tab((hh-1)*sy*sz+(i_pos_vec-2)*sy+ii);
                
                cmt_mat(ii,jj_vec,hh)=cmt_i_pos-(cmt_i_pos-cmt_i_neg)./(bilan_pbat_i_pos-bilan_pbat_i_neg).*bilan_pbat_i_pos;
            end
        end
    end
end   

%% calcul des cout
if param.verbose>=3
    assignin('base','cmt_mat',cmt_mat);
end

if (isfield(param,'fit_pertes') & param.fit_pertes==1)
    % [ERR,dcarb,cmt_mat]=calc_mt(ERR,VD.MOTH,cmt_mat,wmt_tab,0,1,0);
else
    wmt_mat=repmat(wmt_tab(:,1,1,:),[1 sx]);
    wmt_mat=reshape(wmt_mat,[sy sx sc ]);
    [ERR,dcarb_sp,cmt_mat]=calc_mt(ERR,VD.MOTH,cmt_mat,wmt_mat,0,1,0);
end

% On met a inf les dcarb des cmt impossible : d??j?? fait par la ligne du
% dessus
dcarb_sp(isnan(cmt_mat))=Inf;

[dcarb_sp,Imin]=min(dcarb_sp);
dcarb_sp=reshape(dcarb_sp,[sx sc]);

if param.verbose>=3
    assignin('base','dcarb_sp',dcarb_sp);
    assignin('base','Imin',Imin);
end

% On fait le choix du mode qui mnimize dcarb
if isfield(param,'hybrid_serie') & param.hybrid_serie==1
    dcarb=min(dcarb_hs,dcarb_sp);
else
    dcarb=dcarb_sp;
end
   
% Critere de cout des arcs
if nargin ==9 || nargin==18 && recalcul==0
    % mode tout elec moteur thermique arrete
    dcarb(1,find(elec_pos==1))=0;
    % on est en mode "tout elec" cmt=0 mais le moteur thermique doit tourner ?? wmt_min
    % a priori on as d??ja trouver wmt=wmt_min mais on est pas sur d'avoir
    % converg?? exactement vers cmt=0
    [ERR,dcarb(1,find(elec_pos==0.5))]=calc_mt(ERR,VD.MOTH,0*ones(1,sum(elec_pos==0.5)),wmt_min(find(elec_pos==0.5)),0,1,0);
   
    
elseif nargin==18 && recalcul==1  
    % cas hybride et generale (ont pourrais limiter le calcul au cas hybride si besoin
    % cas hs elhyb=1
    % cas sp elhyb =2
    I_e0=find(elhyb==0);
    I_e05=find(elhyb==0.5);
    %I_e1=find(elhyb==1);

    
    dwmt=zeros(1,sc);
    wmt=zeros(1,sc);
    cmt=zeros(1,sc);
    
    dcarb(I_e0)=0;
    wmt(I_e05)=wmt_min(I_e05);
    [ERR,dcarb(I_e05)]=calc_mt(ERR,VD.MOTH,cmt(I_e05),wmt(I_e05),0,1,0);
    
    %elhyb(dcarb_hs==dcarb)=1; d??ja a 1 si hybride
    elhyb(dcarb==dcarb_sp & elhyb==1)=2;
    I_e2=find(elhyb==2);  
    I_e1=find(elhyb==1);
    
    cmt(I_e1)=cmt_hs(I_e1);
    wmt(I_e1)=wmt_hs(I_e1);
    %dcarb(I_e1)=dcarb_hs(I_e1);
    
    
    
    cmt_sp=cmt_mat(sub2ind([sy 1 sc],reshape(Imin,[sc 1 1])',1:sc));
    wmt_sp=wmt_mat(sub2ind([sy 1 sc],reshape(Imin,[sc 1 1])',1:sc));
    
    cmt(I_e2)=cmt_sp(I_e2);
    wmt(I_e2)=wmt_sp(I_e2);
    dwmt(I_e2)=0;
    %dcarb(I_e2)=dcarb_hp(I_e2);
    if param.verbose>=3
        assignin('base','elhyb',elhyb);
        assignin('base','cmt',cmt);
        assignin('base','wmt',wmt);
    end
    
end  

cout=dcarb*param.pas_temps;
cout(isnan(cout))=Inf;

if param.verbose>=3  
    assignin('base','dcarb',dcarb);   
    assignin('base','cout',cout);  
end

if nargin==18 && recalcul==1 % cas recalcul   
    % recalcul cas sp
    [ERR,cacm1_sp,ccour_sp,wacm1_sp]=calc_train_epi(ERR,VD.EPI,cprim_red,wprim_red,wmt_sp,2);
    cacm2_sp=ccour_sp-cmt_sp;
    
   
    cacm1(I_e0)=cacm1_elec(I_e0);
    cacm1(I_e05)=cacm1_elec(I_e05);  
    cacm1(I_e1)=cacm1_hs(I_e1);
    cacm1(I_e2)=cacm1_sp(I_e2);
    
    cacm2=zeros(1,sc);
    wacm2=zeros(1,sc);
    dwacm2=zeros(1,sc);
    dwacm1=zeros(1,sc);

    
    cacm2(I_e0)=cacm2_elec(I_e0);
    cacm2(I_e05)=cacm2_elec(I_e05);
    cacm2(I_e1)=cacm2_hs(I_e1);
    cacm2(I_e2)=cacm2_sp(I_e2);
    
    wacm2(I_e05)=wacm2_elec(I_e05);
    wacm2(I_e0)=wacm2_elec(I_e0);
    wacm2(I_e1)=wacm2_hs(I_e1);
    wacm2(I_e2)=wmt_sp(I_e2);

    wacm1(I_e05)=wacm1_elec(I_e05);
    wacm1(I_e0)=wacm1_elec(I_e0);
    wacm1(I_e1)=wacm1_hs(I_e1);
    wacm1(I_e2)=wacm1_sp(I_e2);
     
    cfrott_acm1(elhyb>0.7)=0;
    
    % calcul puissance elec acm1
    if (isfield(param,'fit_pertes') & param.fit_pertes==1)
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,0,1);
    else
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
    end
    % pour les cas tt elec ou acm1 est non aliment??

    qacm1(I_e0)=qacm1_elec(I_e0); 
    qacm1(I_e05)=qacm1_elec(I_e05);
    
    % Si on est en hybride parall??le et que acm1 non aliment?? alors il faut remettre qacm1 ?? zero
    %cond_acm1_na=find(elhyb==2 & ind_qmin==sz);
    %qacm1(cond_acm1_na)=0;
    %cfrott_acm1(cond_acm1_na)=cacm1(cond_acm1_na);
     
    % calcul puissance elec acm2
    if (isfield(param,'fit_pertes') & param.fit_pertes==1)
         [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,0,1);
    else
        [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0);
    end
    
    qacm2(I_e0)=qacm2_elec(I_e0); 
    qacm2(I_e05)=qacm2_elec(I_e05);
    %qacm2_sp=qacm2_tab(ind_qmin_lin);
    
    %qacm2(I_e2)=qacm2_sp(I_e2); % en hybride serie on remet les pertes calculr fw

    qacm2(I_e1)=qacm2_hs(I_e1); % en hybride // on remet les pertes calculees par l'interpolation pour le choix du pts de fonctinnement
    % si on est en recup et que acm1 non aliment??
    cfrein_meca_pelec_mot=zeros(1,sc);

    Rbat=R;
    u0bat=E*ones(size(cprim_red));
    [ERR,r]=affect_struct(r_var);

end

if ~isempty(find(sum(cout==Inf)==sx))
    i=find(sum(cout==Inf)==sx);
    %chaine=' all edge are impossible, the systems possibilites does not allow to fullfil the required systems operating point ! \n time : %d \n Dsoc : %s  \n cost : %s \n ibat: %s \n ubat: %s \n cmt: %s \n wmt: %s';
    %ERR=MException('BackwardModel:calc_cout_arc_HSDP_EPI',chaine,i,num2str(Dsoc),num2str(cout),num2str(ibat),num2str(ubat),num2str(cmt),num2str(wmt));
    
    chaine=' all edge are impossible, the systems possibilites does not allow to fullfil the required systems operating point ! \n time : %s \n Dsoc : %s  \n cprim_red: %s \n wprim_red: %s';
    ERR=MException('BackwardModel:calc_cout_arc_HSDP_EPI',chaine,num2str(i),num2str(Dsoc(i)),num2str(cprim_red(i)),num2str(wprim_red(i)));
    
    [cout,ibat,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,Dsoc,elhyb,cfrott_acm1,cfrein_meca_pelec_mot]=backward_erreur(length(Dsoc));
    return
end

if sum(isnan(cout))
    chaine='some edge cost are NaN, to avoid problems in solve graph, prefer to fixe them to Inf \n time : %d' ;
    warning('BackwardModel:calc_cout_arc_HPDP_EPI_SURV',chaine,i);
end

return

