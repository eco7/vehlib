% function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=calc_limites_HPDP_EVT_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul des limites du graphes.
% On ne calcule ici que les soc min et max sans se preocuper
% de la remonte du graphe ou de soc <0 ou >100%.
% La limite min doit ??tre calcul?? avec pr??cision (mode tout ??lec)
% la mlimite max peut ??tre calcule "grossi??rment" d'eventuel arc impossible
% dans le graphe seront elimine par la suite
%
% interface entr??e :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discreetisation etc)
% CYLC,VD.ADCPL...:structures de description vehicule et composants
% cprim_red  :   Couple sur le primaire du reducteur (vecteur sur le cycle)
% wprim_red  :   vitesse sur le primaire du reducteur (vecteur sur le cycle)
% dwprim_red :   d??riv??e de la vitesse sur le primaire du reducteur (vecteur sur le cycle)
% 
% interface de sortie
% ibat_max :    courant max mode tout elec (en recup c'est le courant de recup en tout elec)
% ibat_min :    courant min recharge max ( en recup courant recup plus recharge par moth)
% elec_pos :    vecteur indiquant si mode elec possible , ou zero si impossible
% soc_min :     soc correspondant a ibat_max
% soc_max :     soc correspondant a ibat_min
% Dsoc_min :    variation soc entre deux instant correspondant a ibat_max
%               (atention a int??gre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% Dsoc_max :    variation soc entre deux instant correspondant a ibat_min
%               (atention a int??gre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% 21-11-11(EV): Creation 


function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=  calc_limites_HPDP_EVT_SURV_RED(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
ERR=[];
ii_max=size(VD.CYCL.temps);
soc_min=zeros(ii_max);
soc_max=zeros(ii_max);
Dsoc_min=zeros(ii_max);
Dsoc_max=zeros(ii_max);



%% recherche du mode tout electrique ?? chaque pas de temps
  
% ici doit venir s'ajouter les pertes  acm2 qui tourne pour
% ??quilibrer le train on suppose dans les cas ou le moteur
% thermique tourne qand m??me aux ralenti, acm2 sera quand m??me 
% aliment?? pour garantir cacm2 = 0. C'est ce qui est fait dans VEHLIB
% un autre choix est de ne pas alimenter la gene et de compenser les pertes
% meca.

% Calcul de la vitesse min possible du moteur thermique pour v??rifier que l emode tout ??lec est possible
wmt_min=0*ones(size(wprim_red)); % limitation sur vit max du moth et vit max de la generatrice

if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
    wmt_min(wprim_red>VD.ECU.vmaxele/VD.VEHI.Rpneu*VD.RED.kred)=VD.MOTH.ral;
end

% calcul de la vitesse de acm2
if isfield(param,'red2') && param.red2==1
    [ERR,~,wacm2_rot]=calc_red_fw(ERR,VD.RED2,0,wmt_min,0,0);
else
    wacm2_rot=wmt_min;
end
wacm2_elec=wacm2_rot-wprim_red;


% calcul des pertes par frottement dans acm2 
cfrott_acm2=interp1(VD.ACM2.Regmot_pert,VD.ACM2.Cpl_frot,abs(wacm2_elec));

% connexion
wacm1_elec=wprim_red;
dwacm1_elec=dwprim_red;
cacm1_elec=cprim_red+VD.ACM1.J_mg*dwacm1_elec-cfrott_acm2;

% Calcul des conditions en amont de la machine electrique
%%% limitations ME
cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec);

% Si cacm1<cacm1_min on fait de la recup et on vas faire le max possible
% le reste en frein meca, donc on sature cacm1 ?? cacm1_min 
% normalement cela est d??ja fait dans calc_HPDP_EPI_surv mais on assurera
% le coup !

cacm1_elec(cacm1_elec<cacm1_min)=cacm1_min(cacm1_elec<cacm1_min);

if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
    [ERR,qacm1_elec]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec,0,1);
else
    if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
        if isfield(param,'Ubus') & strcmp(param.Ubus,'var')
            [ERR,qacm1_elec,Udcdc_ht]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec);
        else
            [ERR,qacm1_elec]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec);        
            Udcdc_ht=VD.ACM1.Tension_cont(end)*ones(size(cacm1_elec));
        end
    else
        [ERR,qacm1_elec]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec);
    end
end

%[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec,'1');

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);

% calcul puissance motelec
pacm1_elec=VD.VEHI.nbacm1*(cacm1_elec.*wacm1_elec+qacm1_elec);

if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
    [ERR,~,~,~,~,Ebat]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,10,100-VD.INIT.Dod0,0,-1,0,param);
    PHT=pacm1_elec;
    [ERR,pertes_dcdc]=calc_dcdc_2(ERR,VD.SURV,PHT,Ebat*ones(size(pacm1_elec)),Udcdc_ht); % normalement on devrait prendre Ubat et pas UO mais implicite dans ce cas
    pres= pertes_dcdc + PHT +pacc;
else
    pres=pacm1_elec+pacc;
end

% Puissance reseau electrique
%pres=pacm1_elec+pacc+pfrott_acm2; % Normalement il y as un survolteur entre pbat et pres !!
%pres=pacm1_elec+pacc;
% prise en compte limitation batterie 
% on range dans elec_pos les indices des points ou le mode tout elec est
% possible

[ERR,ibat_max,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,100-VD.INIT.Dod0,0,-1,0,param);

% Cas ou l'on as explose les limites en recup
% On doit imposer que Ibat_max = VD.BATT.Ibat_min
ibat_max(isnan(ibat_max)&cprim_red<0)=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension


elec_pos=ones(size(cprim_red));
elec_pos(wmt_min~=0)=2; % moth au ralenti mais cmt =0, la batterie fournie toute la puissance
elec_pos(isnan(ibat_max))=0; % la batterie ne peut fournir toute la puissance mode elec impossible
% calcul de la pbat_max
ibat_max(isnan(ibat_max))=interp1(VD.BATT.DoD_Ibat_max,VD.BATT.Ibat_max,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension

% Sur certain point de recup ont peut arriver a faire plus de pertes dans acm1
% que l'on ne recupere de puissance sur le primaire reducteur, on regarde
% alors ce qu'ils se passe si on alimente pas la machine
pres_acm1_na=pres;
pres_acm1_na(cprim_red<0)=pacc(cprim_red<0);
[ERR,ibat_max_acm1_na]=calc_batt(ERR,VD.BATT,param.pas_temps,pres_acm1_na,100-VD.INIT.Dod0,0,-1,0,param);

ibat_max(ibat_max>ibat_max_acm1_na)=ibat_max_acm1_na(ibat_max>ibat_max_acm1_na);

soc_min(1)=100-VD.INIT.Dod0;
Dsoc_min(1)=0;
for ii=2:length(VD.CYCL.temps)
    soc_min(ii)=soc_min(ii-1)-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_min(ii)=-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

assignin('base','wmt_min',wmt_min)

if param.verbose>=2
    assignin('base','ibat_max',ibat_max)
    assignin('base','cprim_red',cprim_red)
    assignin('base','wprim_red',wprim_red)
    
    figure(3)
    clf
    hold on
    plot(VD.CYCL.temps,ibat_max,'b',VD.CYCL.temps,VD.CYCL.vitesse,'k')
    grid
    
    figure(4)
    clf
    hold on
    plot(VD.CYCL.temps,soc_min,'b',VD.CYCL.temps,60+Dsoc_min,'g')
    grid
end



%% recherche de la pbat_min (mode hybride)la
% calcul du couple max moteur thermique


% on recherche la puissance max possible du moteur thermique
[~,i_pmt_max]=max(VD.MOTH.pmt_max);
wmt_pmax=VD.MOTH.wmt_max(i_pmt_max);

pmax_mt=interp1(VD.MOTH.wmt_max,VD.MOTH.pmt_max,wmt_pmax);
cmt_max=pmax_mt./wmt_pmax;

% connexion
wacm1=wprim_red;
dwacm1=dwprim_red;

if isfield(param,'red2') && param.red2==1
    [ERR,cacm2_rot,wacm2_rot]=calc_red_fw(ERR,VD.RED2,cmt_max,wmt_pmax,0,0);
else
    cacm2_rot=cmt_max;
    wacm2_rot=wmt_pmax;
end
cacm2=-cacm2_rot;
wacm2=wacm2_rot-wprim_red;


cacm1=cprim_red+cacm2+VD.ACM1.J_mg*dwacm1;

% calcul des pertes dans acm1
if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
    [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,0,1);
else
    if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
        if isfield(param,'Ubus') & strcmp(param.Ubus,'var')
            [ERR,qacm1,Udcdc_ht_acm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
        else
            Udcdc_ht_acm1=VD.ACM1.Tension_cont(end)*ones(size(cacm1));
            [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
        end
    else
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
    end
end


 % calcul des pertes dans acm1
[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');
pelecacm1=cacm1.*wacm1+qacm1;
% 
% calcul des pertes dans acm2 

if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
    [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2*ones(size(wacm2)),wacm2,0,0,1);
else
    if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
        if isfield(param,'Ubus') & strcmp(param.Ubus,'var')
            [ERR,qacm2,Udcdc_ht_acm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2*ones(size(wacm2)),wacm2,0);
        else
            Udcdc_ht_acm2=VD.ACM2.Tension_cont(end)*ones(size(cacm2));
            [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2*ones(size(wacm2)),wacm2,0);
        end
    else
        [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2*ones(size(wacm2)),wacm2,0);
    end
end

pelecacm2=cacm2.*wacm2+qacm2;

% calcul des pertes dans acm2
% [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2*ones(size(wacm2)),wacm2,0,'1');
% pelecacm2=cacm2.*wacm2+qacm2;


% Pour le calcul des pertes dans le dcdc on prendra la tension
% max données par les deux machines


if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
    Udcdc_ht=max(Udcdc_ht_acm1,Udcdc_ht_acm2);
    [ERR,~,~,~,~,Ebat]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,10,100-VD.INIT.Dod0,0,-1,0,param);
    PHT=pelecacm1+pelecacm2;
    [ERR,pertes_dcdc]=calc_dcdc_2(ERR,VD.SURV,PHT,Ebat*ones(size(pelecacm1)),Udcdc_ht); % normalement on devrait prendre Ubat et pas UO mais implicite dans ce cas
    pres=PHT + pacc + pertes_dcdc; %
else
    pres=pelecacm1+pelecacm2+pacc; %
end

% calcul de ibatmin
%pres=pelecacm1+pelecacm2+pacc; % Normalement il y as un survolteur entre pbat et pres !!
pbat=pres;

[ERR,ibat_min,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat,100-VD.INIT.Dod0,0,-1,0,param); 
ibat_min(isnan(ibat_min))=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0);


% Si on arrive ?? ibat_min>ibat_max (peut arriver en recup max si les
% limites sont atteintes) on impose ibat_min au min <=ibat_max
ibat_min(ibat_min>ibat_max)=ibat_max(ibat_min>ibat_max);

soc_max(1)=100-VD.INIT.Dod0;
Dsoc_max(1)=0;

for ii=2:length(VD.CYCL.temps)
    soc_max(ii)=soc_max(ii-1)-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_max(ii)=-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

if sum(ibat_max<ibat_min)>0 ||  sum(soc_max<soc_min)>0 || sum(Dsoc_max<Dsoc_min)>0
    chaine='error in calc_limites_HPDP_EPI_SURV' ;
    ERR=MException('BackwardModel:calc_HPDP_EPI_SURV',chaine);
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    return

end

if param.verbose>=2
    assignin('base','ibat_min',ibat_min)

    figure(3)
    plot(VD.CYCL.temps,ibat_min,'g')
    legend('ibat max','vitesse','ibat min')
    title('courant min max fin calc limite')
    
    figure(4)
    plot(VD.CYCL.temps,soc_max,VD.CYCL.temps,Dsoc_max+60)
    legend('soc min','Dsoc min','soc max','Dsoc max')
    title('soc min max fin calc limite')

end


