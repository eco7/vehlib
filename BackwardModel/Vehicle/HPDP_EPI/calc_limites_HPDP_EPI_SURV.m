% function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=calc_limites_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul des limites du graphes.
% On ne calcule ici que les soc min et max sans se preocuper
% de la remonte du graphe ou de soc <0 ou >100%.
% La limite min doit ??tre calcul?? avec pr??cision (mode tout ??lec)
% la mlimite max peut ??tre calcule "grossi??rment" d'eventuel arc impossible
% dans le graphe seront elimine par la suite
%
% interface entr??e :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discreetisation etc)
% CYLC,VD.ADCPL...:structures de description vehicule et composants
% cprim_red  :   Couple sur le primaire du reducteur (vecteur sur le cycle)
% wprim_red  :   vitesse sur le primaire du reducteur (vecteur sur le cycle)
% dwprim_red :   d??riv??e de la vitesse sur le primaire du reducteur (vecteur sur le cycle)
% 
% interface de sortie
% ibat_max :    courant max mode tout elec (en recup c'est le courant de recup en tout elec)
% ibat_min :    courant min recharge max ( en recup courant recup plus recharge par moth)
% elec_pos :    vecteur indiquant si mode elec possible , ou zero si impossible
% soc_min :     soc correspondant a ibat_max
% soc_max :     soc correspondant a ibat_min
% Dsoc_min :    variation soc entre deux instant correspondant a ibat_max
%               (atention a int??gre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% Dsoc_max :    variation soc entre deux instant correspondant a ibat_min
%               (atention a int??gre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% 21-11-11(EV): Creation 

function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=  calc_limites_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
ERR=[];
ii_max=size(VD.CYCL.temps);
soc_min=zeros(ii_max);
soc_max=zeros(ii_max);
Dsoc_min=zeros(ii_max);
Dsoc_max=zeros(ii_max);

%Connexion
wcour=wprim_red;

%% recherche du mode tout electrique ?? chaque pas de temps
  
% ici doit venir s'ajouter les pertes  acm2 qui tourne pour ??quilibrer le train. On suppose dans les cas ou le moteur
% thermique tourne qand avec cmt=0), que acm2 sera quand m??me  aliment?? pour garantir cacm2 = 0. C'est ce qui est fait dans VEHLIB
% un autre choix est de ne pas alimenter la gene et de compenser les pertes meca.

% Calcul de la vitesse min possible du moteur thermique pour v??rifier que l emode tout ??lec est possible
wmt_min=max(0,(-VD.ECU.wmaxge-VD.EPI.kb*wcour)./(1-VD.EPI.kb)); % limitation sur vit max du moth et vit max de la generatrice
wmt_min=max(wmt_min,wcour-VD.ECU.wmaxps*VD.EPI.Nsat/VD.EPI.Ncour ); % limitation sur la vitesse max au niveau satellite / porte satellite 

% Le moteur ne peut pas tourner en dessous de sa vitesse de ral
wmt_min(wmt_min~=0)=max(VD.MOTH.ral,wmt_min(wmt_min~=0));


if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
    wmt_min(wprim_red>VD.ECU.vmaxele/VD.VEHI.Rpneu*VD.RED.kred)=VD.MOTH.ral;
end

% calcul de la vitesse de acm2
[ERR,~,~,wacm2]=calc_train_epi(ERR,VD.EPI,0,wmt_min,wcour);

% calcul des pertes dans acm2 ; en toute rigueur on devrait etudier aussi
% le cas ou acm2 non alimente et donc pertes par frottement et hysteresis !!!
if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
    [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,0*ones(size(wacm2)),wacm2,0,0,1);
else
    [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,0*ones(size(wacm2)),wacm2,0);
end

% Cas ou l'on imagine un embrayage sur la prius pour d??coupler le train 
% en tout elec
if isfield(param,'emb') && param.emb==1
    wacm2=zeros(ii_max);
    cacm2=zeros(ii_max);
    qacm2=zeros(ii_max);
end

pacm2_elec=qacm2;

% connexion
wacm1_elec=wprim_red;
dwacm1_elec=dwprim_red;
cacm1_elec=cprim_red+VD.ACM1.J_mg*dwacm1_elec;

% Calcul des conditions en amont de la machine electrique
%%% limitations ME
cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec);

% Si cacm1<cacm1_min on fait de la recup et on vas faire le max possible
% le reste en frein meca, donc on sature cacm1 a cacm1_min 
% normalement cela est deja fait dans calc_HPDP_EPI_surv mais on assurera
% le coup !

cacm1_elec(cacm1_elec<cacm1_min)=cacm1_min(cacm1_elec<cacm1_min);


if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
    [ERR,qacm1_elec]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec,0,1);
else
    if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
        if isfield(param,'Ubus') & strcmp(param.Ubus,'var')
            [ERR,qacm1_elec,Udcdc_ht]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec);
        else
            [ERR,qacm1_elec]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec);        
            Udcdc_ht=VD.ACM1.Tension_cont(end)*ones(size(cacm1_elec));
        end
    else
        [ERR,qacm1_elec]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec);
    end
end

% calcul puissance motelec
pacm1_elec=VD.VEHI.nbacm1*(cacm1_elec.*wacm1_elec+qacm1_elec);
[ERR,pacc]=calc_acc(VD);

if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
    [ERR,~,~,~,~,Ebat]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,10,100-VD.INIT.Dod0,0,-1,0,param);
    PHT=pacm1_elec+pacm2_elec;
    [ERR,pertes_dcdc]=calc_dcdc_2(ERR,VD.SURV,PHT,Ebat*ones(size(pacm1_elec)),Udcdc_ht); % normalement on devrait prendre Ubat et pas UO mais implicite dans ce cas
    pres= pertes_dcdc + PHT +pacc;
else
    pres=pacm1_elec+pacc+pacm2_elec;
end




% if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
%     [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec,0,1);
% else
%     [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec);
% end
% % Calcul des auxiliaires electriques
% [ERR,pacc]=calc_acc(VD);
% 
% % calcul puissance motelec
% pacm1_elec=VD.VEHI.nbacm1*(cacm1_elec.*wacm1_elec+qacm1);
% 
% % Puissance reseau electrique
% pres=pacm1_elec+pacc+pacm2_elec; % Normalement il y as un survolteur entre pbat et pres !!

% prise en compte limitation batterie 
% on range dans elec_pos les indices des points ou le mode tout elec est
% possible

[ERR,ibat_max,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,pres,100-VD.INIT.Dod0,0,-1,0,param);
% Cas ou l'on as explose les limites en recup
% On doit imposer que Ibat_max = VD.BATT.Ibat_min
ibat_max(isnan(ibat_max)&cprim_red<0)=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension


elec_pos=ones(size(cprim_red));
elec_pos(wmt_min~=0)=0.5; % moth au ralenti mais cmt =0, la batterie fournie toute la puissance
elec_pos(isnan(ibat_max))=0; % la batterie ne peut fournir toute la puissance mode elec impossible

% calcul de la pbat_max
ibat_max(isnan(ibat_max))=interp1(VD.BATT.DoD_Ibat_max,VD.BATT.Ibat_max,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension

% Sur certain point de recup ont peut arriver a faire plus de pertes dans acm1
% que l'on ne recupere de puissance sur le primaire reducteur, on regarde
% alors ce qu'ils se passe si on alimente pas la machine
pres_acm1_na=pres;
pres_acm1_na(cprim_red<0)=pacc(cprim_red<0)+pacm2_elec(cprim_red<0);
[ERR,ibat_max_acm1_na]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,pres_acm1_na,100-VD.INIT.Dod0,0,-1,0,param);

ibat_max(ibat_max>ibat_max_acm1_na)=ibat_max_acm1_na(ibat_max>ibat_max_acm1_na);

soc_min(1)=100-VD.INIT.Dod0;
Dsoc_min(1)=0;

for ii=2:length(VD.CYCL.temps)
    if param.pas_temps~=0
        soc_min(ii)=soc_min(ii-1)-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
        Dsoc_min(ii)=-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    else
        soc_min(ii)=soc_min(ii-1)-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*VD.CYCL.pas_temps(ii);
        Dsoc_min(ii)=-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*VD.CYCL.pas_temps(ii);
    end
end


%% recherche de la pbat_min (mode hybride)la
% calcul du couple max moteur thermique


% Calcul de la vitesse max possible du moteur
wmt_max=min(VD.MOTH.wmt_maxi,(VD.ECU.wmaxge-VD.EPI.kb*wcour)./(1-VD.EPI.kb)); % limitation sur vit max du moth et vit max de la generatrice
wmt_max=min(wmt_max,wcour+VD.ECU.wmaxps*VD.EPI.Nsat/VD.EPI.Ncour ); % limitation sur la vitesse max au niveau satellite / porte satellite 

% on recherche la puissance max possible du moteur thermique
[~,i_pmt_max]=max(VD.MOTH.pmt_max);
wmt_pmax=min(VD.MOTH.wmt_max(i_pmt_max),wmt_max);

pmax_mt=interp1(VD.MOTH.wmt_max,VD.MOTH.pmt_max,wmt_pmax);
cmt_max=pmax_mt./wmt_pmax;

[ERR,cacm2,ccour,wacm2]=calc_train_epi(ERR,VD.EPI,cmt_max,wmt_pmax,wcour);

% connexion
wacm1=wprim_red;
dwacm1=dwprim_red;

cacm1=cprim_red-ccour+VD.ACM1.J_mg*dwacm1;



% calcul des pertes dans acm1
if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
    [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,0,1);
else
    if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
        if isfield(param,'Ubus') & strcmp(param.Ubus,'var')
            [ERR,qacm1,Udcdc_ht_acm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
        else
            Udcdc_ht_acm1=VD.ACM1.Tension_cont(end)*ones(size(cacm1));
            [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
        end
    else
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
    end
end

% calcul puissance motelec
pelecacm1=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1);
[ERR,pacc]=calc_acc(VD);


% calcul des pertes dans acm2 
if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
    [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,0,1);
else
    if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
        if isfield(param,'Ubus') & strcmp(param.Ubus,'var')
            [ERR,qacm2,Udcdc_ht_acm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0);
        else
            Udcdc_ht_acm2=VD.ACM2.Tension_cont(end)*ones(size(cacm2));
            [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0);
        end
    else
        [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0);
    end
end
pelecacm2=cacm2.*wacm2+qacm2;

% POur la calcul des pertes dans le dcdc on prendra la tension
% max données par les deux machines

if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
    Udcdc_ht=max(Udcdc_ht_acm1,Udcdc_ht_acm2);
    [ERR,~,~,~,~,Ebat]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,10,100-VD.INIT.Dod0,0,-1,0,param);
    PHT=pelecacm1+pelecacm2;
    [ERR,pertes_dcdc]=calc_dcdc_2(ERR,VD.SURV,PHT,Ebat*ones(size(pelecacm1)),Udcdc_ht); % normalement on devrait prendre Ubat et pas UO mais implicite dans ce cas
    pres=PHT + pacc + pertes_dcdc; %
else
    pres=pelecacm1+pelecacm2+pacc; %
end




% % calcul des pertes dans acm1
% if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
%     [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,0,1);
% else
%     [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
% end
% pelecacm1=cacm1.*wacm1+qacm1;
% 
% 
% % calcul des pertes dans acm2
% if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
%     [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,0,1);
% else
%     [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0);
% end
% pelecacm2=cacm2.*wacm2+qacm2;


% calcul de ibatmin
%pres=pelecacm1+pelecacm2+pacc; % Normalement il y as un survolteur entre pbat et pres !!
pbat=pres;

[ERR,ibat_min,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,pbat,100-VD.INIT.Dod0,0,-1,0,param); 
ibat_min(isnan(ibat_min))=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0);


% Si on arrive ?? ibat_min>ibat_max (peut arriver en recup max si les
% limites sont atteintes) on impose ibat_min au min <=ibat_max
ibat_min(ibat_min>ibat_max)=ibat_max(ibat_min>ibat_max);

soc_max(1)=100-VD.INIT.Dod0;
Dsoc_max(1)=0;

for ii=2:length(VD.CYCL.temps)
    if param.pas_temps~=0
        soc_max(ii)=soc_max(ii-1)-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
        Dsoc_max(ii)=-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    else
        soc_max(ii)=soc_max(ii-1)-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*VD.CYCL.pas_temps(ii);
        Dsoc_max(ii)=-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*VD.CYCL.pas_temps(ii);
    end   
end

if sum(ibat_max<ibat_min)>0 ||  sum(soc_max<soc_min)>0 || sum(Dsoc_max<Dsoc_min)>0
    chaine='error in calc_limites_HPDP_EPI_SURV' ;
    ERR=MException('BackwardModel:calc_HPDP_EPI_SURV',chaine);
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    return

end

if param.verbose>=2
    assignin('base','ibat_min',ibat_min)
    assignin('base','ibat_max',ibat_max)
    assignin('base','cprim_red',cprim_red)
    assignin('base','wprim_red',wprim_red)
    
    figure(3)
    clf
    plot(VD.CYCL.temps,ibat_min,VD.CYCL.temps,ibat_max,VD.CYCL.temps,VD.CYCL.vitesse)
    legend('ibat min','ibat max','vitesse')
    title('courant min max fin calc limite')
    grid
    
    figure(4)
    clf
    plot(VD.CYCL.temps,soc_min,VD.CYCL.temps,soc_max,VD.CYCL.temps,60+Dsoc_min,VD.CYCL.temps,Dsoc_max+60)
    legend('soc min','soc max','Dsoc min','Dsoc max')
    title('soc min max fin calc limite')
    grid
end


