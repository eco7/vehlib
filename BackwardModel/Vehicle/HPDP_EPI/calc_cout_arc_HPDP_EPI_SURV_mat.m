% function [ERR,cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=calc_cout_arc_HPDP_EVT_SURV(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,soc_p,i,recalcul,elhyb)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Calcul des cout des arcs cas HPDP_EPI
%
% interface entr??e :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discretisation etc)
% CYLC,VD.ADCPL...:structures de description vehicule et composants
% cprim_red :   Couple sur le primaire du reducteur (vecteur sur le cycle)
% wprim_red :   vitesse sur le primaire du reducteur  (vecteur sur le cycle)
% dprim_red :   deriv??e de la vitesse sur le primaire du reducteur (vecteur sur le cycle)
% Dsoc :        variation de soc sur l'eventail des arcs possible
% elec_pos :    mode electrique possible (vecteur sur le cycle)
% soc_p :       soc ?? l'instant precedent
% i :           instant dans le cycle
% recalcul :    si on est en phase de recalcul (grahe deja resolu) recalcul =1 sinon 0 ou pas passer dans l'interface
% elhyb :       mode elec/hybride/thermique en phase recalcul uniquement
%
% interface de sortie
% cout :        cout des arcs de l'??ventail
% ibat,ubat ...:variable vehicule utilie uniquement en recalcul sinon on ne renvoie que le cout
%
% 21-11-11(EV): Creation 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ERR,cout,r]=calc_cout_arc_HPDP_EPI_SURV_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,soc_p,recalcul,elhyb)

if ~isfield(param,'Ubus')
    param.Ubus='cst';
end

r_var={'ibat','ubat','Rbat','u0bat','RdF','dcarb','cmt','wmt','dwmt','cacm1','wacm1','dwacm1','qacm1',...
    'cacm2','wacm2','dwacm2','qacm2','Dsoc','elhyb','cfrott_acm1','cfrein_meca_pelec_mot','Ubus'};

[Li,Co]=size(soc_p);
[sx,sc]=size(Dsoc);

if sum(sum(isnan(soc_p)))==Li*Co
    ERR=MException('BackwardModel:calc_cout_arc_HPDPEPI', 'appel a calc_cout_arc_HPDP_EPI : tout les elements d''un des vecteur precedent a NaN');
    [ibat,ubat,Rbat,u0bat,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,Dsoc,elhyb,cfrott_acm1,cfrein_meca_pelec_mot]=backward_erreur(size(Dsoc));
    [ERR,r]=affect_struct(r_var);
    return
end

if param.pas_temps~=0
    pas_temps=param.pas_temps;
else
    pas_temps=repmat(VD.CYCL.pas_temps,sx,1);
end

%Connexion
wcour=wprim_red;


% Calcul des vitesses max et min de rotation du moteur thermique
% Grace aux lignes suivantes les vitesses max de la g??n?? et des portes
% satellites ne peuvent normalement pas ??tre atteinte

wmt_max=min(VD.MOTH.wmt_maxi,(VD.ECU.wmaxge-VD.EPI.kb*wcour)./(1-VD.EPI.kb)); % limitation sur vit max du moth et vit max de la generatrice
wmt_max=min(wmt_max,wcour+VD.ECU.wmaxps*VD.EPI.Nsat/VD.EPI.Ncour ); % limitation sur la vitesse max au niveau satellite / porte satellite 

wmt_min=max(0,(-VD.ECU.wmaxge-VD.EPI.kb*wcour)./(1-VD.EPI.kb)); % limitation sur vit max du moth et vit max de la generatrice
wmt_min=max(wmt_min,wcour-VD.ECU.wmaxps*VD.EPI.Nsat/VD.EPI.Ncour ); % limitation sur la vitesse max au niveau satellite / porte satellite 

% Le moteur ne peut pas tourner en dessous de sa vitesse de ral
wmt_min(wmt_min~=0)=max(VD.MOTH.ral,wmt_min(wmt_min~=0));

if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
    wmt_min(wprim_red>VD.ECU.vmaxele/VD.VEHI.Rpneu*VD.RED.kred)=VD.MOTH.ral;
end


if (isfield(param,'fit_pertes_acm') & param.fit_pertes_acm==1)
    wmt_mat=NaN*ones(floor(max((wmt_max-wmt_min)/param.pas_wmt))+1,sc);
    for ii=1:sc
        wmt_vec=max(VD.MOTH.ral,wmt_min(ii)):param.pas_wmt:wmt_max(ii);
        wmt_vec=[wmt_vec wmt_max(ii)]';
        wmt_mat(1:length(wmt_vec),ii)=wmt_vec;
    end
    [ERR,pacc]=calc_acc(VD);
    
    % calcul batterie
    % calcul ibat ?? partir de Dsoc
    RdF=ones(size(Dsoc));
    RdF(Dsoc>0)=VD.BATT.RdFarad;
    ibat=-Dsoc./RdF*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100./pas_temps;
    
    [ERR,ubat,~,~,E,R]=calc_batt_fw(ERR,VD.BATT,pas_temps,ibat,soc_p,0);

    pbat=ubat.*ibat;
    
    % a partir d'ici tout est des tableaux de taille
    % (nb_arc,length(wmt_vec),1,length(cycle)
    [sy,~]=size(wmt_mat);
    pbat=reshape(pbat,[1 sx sc]);
    pbat=repmat(pbat,[sy 1]);
    
    wmt=repmat(reshape(wmt_mat,[sy 1 sc]),[1 sx]);
    
    wacm1=repmat(reshape(wprim_red,[1 1 sc]),[sy sx]);
    dwacm1=repmat(reshape(dwprim_red,[1 1 sc]),[sy sx]);
    cprim_red_mat=repmat(reshape(cprim_red,[1 1 sc]),[sy sx]);
    [ERR,~,~,wacm2]=calc_train_epi(ERR,VD.EPI,0,wmt,wacm1);
    k1=VD.EPI.rend_train*VD.EPI.kb/(1-VD.EPI.kb);
    k2=1/(1-VD.EPI.kb);
    
    %sP1=sign(cacm1.*wacm1); % wacm1 est toujours positif en marche avant
    % et les formules de pertes sont OK pour w>0 et c <0 ou >0
    a1=(VD.ACM1.c1*wacm1.*wacm1+VD.ACM1.c4*wacm1+VD.ACM1.c7);
    b1=(VD.ACM1.c2*wacm1.*wacm1+VD.ACM1.c5*wacm1+VD.ACM1.c8);
    c1=(VD.ACM1.c3*wacm1.*wacm1+VD.ACM1.c6*wacm1);

    sP2=sign(wacm2);
    a2=(VD.ACM2.c1*wacm2.*wacm2+VD.ACM2.c4*abs(wacm2)+VD.ACM2.c7);
    b2=(VD.ACM2.c2*wacm2.*wacm2+VD.ACM2.c5*abs(wacm2)+VD.ACM2.c8);
    c2=(VD.ACM2.c3*wacm2.*wacm2+VD.ACM2.c6*abs(wacm2));
    
    
    a=a1*k1*k1+a2*k2*k2;
    b=k1*wacm1-k2*wacm2+k1*b1-sP2.*k2.*b2+2*a1.*k1.*(cprim_red_mat+VD.ACM1.J_mg*dwacm1);
    %pacc_mat=repmat(reshape(pacc,[1 1 sc]),[sy sx]);
    pacc_mat=pacc(1);
    c=wacm1.*(cprim_red_mat+VD.ACM1.J_mg*dwacm1)+a1.*(cprim_red_mat+VD.ACM1.J_mg*dwacm1).*(cprim_red_mat+VD.ACM1.J_mg*dwacm1)+b1.*(cprim_red_mat+VD.ACM1.J_mg*dwacm1)+c1+c2-(pbat-pacc_mat);
    
    delta=(b.*b-4*a.*c);
   
    %cmt1=(-b+sqrt(delta))./(2*a);
    cmt2=(-b-sqrt(delta))./(2*a);
    
    % il faut ??liminer les cmt complexe (delta<0) et les couples mt
    % impossible et <0 (car pas possible dans prius et alors les rendement
    % du train epi et acm1 sont non connu ?? l'avance (sens des puissance) donc il faut r??soudre autrement
    delta(isnan(delta))=-1;
    I=find(~isnan(wmt)&~isnan(cmt2)&~delta<0);
    assignin('base','cmt2_prem',cmt2)
    [ERR,~,cmt2(I)]=calc_mt(ERR,VD.MOTH,cmt2(I),wmt(I),0,1,0,1);
    I=find(cmt2<0|delta<0);  
    cmt2(I)=NaN;
    cmt_mat=cmt2;
    
    if param.verbose>=3    
        assignin('base','sP2',sP2)
        assignin('base','k2',k2)
        assignin('base','b2',b2)
        assignin('base','a',a)
        assignin('base','b',b)
        assignin('base','c',c)
        assignin('base','delta',delta)
        assignin('base','wacm1',wacm1)
        assignin('base','dwacm1',dwacm1)
        assignin('base','wacm2',wacm2)
        assignin('base','pbat',pbat)
        assignin('base','sx',sx)
        assignin('base','sy',sy)
        assignin('base','wmt',wmt)
        assignin('base','cmt2',cmt2)
        assignin('base','wprim_red',wprim_red)
        assignin('base','cprim_red',cprim_red)
        assignin('base','cprim_red_mat',cprim_red_mat)
        assignin('base','ibat',ibat)
        assignin('base','ubat',ubat)
        assignin('base','Dsoc',Dsoc)
    end
    %% Recalcul du bilan de puissance utile pour developpement voir ce que l'on en fait par la suite ??
    %% peut rester un eetape de validation en mettant _rec ?? chaque variable
    % size(cmt_mat)
    % sy
    % sx
    % sc
    %[ERR,cacm2,ccour,wacm2]=calc_train_epi(ERR,VD.EPI,cmt_mat,wmt(:,:,1),wcour(:,:,1));
    %[ERR,cacm2,ccour,wacm2]=calc_train_epi(ERR,VD.EPI,cmt_mat,wmt,wacm1);
    
    % calcul des pertes dans les machines
    
    % connexion
%     [ERR,cacm2_rec,ccour_rec,wacm2_rec]=calc_train_epi(ERR,VD.EPI,cmt2,wmt,wacm1);
%     
%     cacm1_rec=cprim_red_mat-ccour_rec+VD.ACM1.J_mg*dwacm1;
%     
%     % calcul puissance elec acm1
%     %[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');
%     qacm1_rec=(VD.ACM1.c1*cacm1_rec.*cacm1_rec+VD.ACM1.c2*cacm1_rec+VD.ACM1.c3).*wacm1.*wacm1+...
%               (VD.ACM1.c4*cacm1_rec.*cacm1_rec+VD.ACM1.c5*cacm1_rec+VD.ACM1.c6).*wacm1+...
%               (VD.ACM1.c7*cacm1_rec.*cacm1_rec+VD.ACM1.c8*cacm1_rec);
%     pelecacm1_rec=cacm1_rec.*wacm1+qacm1_rec;
%     
%     
%     % calcul puissance elec acm2
%     %[ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,'1');
%     %cacm2_rec=
%     %wacm2_rec=
%     sP2=sign(wacm2_rec);
%     qacm2_rec=(VD.ACM2.c1*cacm2_rec.*cacm2_rec+VD.ACM2.c2*sP2.*cacm2_rec+VD.ACM2.c3).*wacm2.*wacm2+...
%               (VD.ACM2.c4*cacm2_rec.*cacm2_rec+VD.ACM2.c5*sP2.*cacm2_rec+VD.ACM2.c6).*abs(wacm2)+...
%               (VD.ACM2.c7*cacm2_rec.*cacm2_rec+VD.ACM2.c8*sP2.*cacm2_rec);
%     pelecacm2_rec=cacm2_rec.*wacm2_rec+qacm2_rec;
%     
%     % verification limitations sur les vitesses ??
%     
%     % calcul de la puisance batterie
%     pres_rec=pelecacm1_rec+pelecacm2_rec+pacc(1);
%     pbat_syst_rec=pres_rec; % puissance batteire calculee cote systeme
%     
%     
%     %bilan_pbat_rec=pbat(:,:,1)-pbat_syst;
%     
%     bilan_pbat_rec=pbat-pbat_syst_rec;
%     assignin('base','pbat_syst_rec',pbat_syst_rec);
%     assignin('base','pbat',pbat);
%     assignin('base','bilan_pbat_rec',bilan_pbat_rec);
    % find(bilan_pbat_rec>1e-5)
    % 'recalcul bilan_pbat'
    % pause
    % fin recalcul bilan puissance
    
elseif ~isfield(param,'fit_pertes_acm') | param.fit_pertes_acm~=1
       
    % creation des tables sur les vitesses et couple moteur
    wmt_mat=NaN*ones(floor(max((wmt_max-wmt_min)/param.pas_wmt))+1,sc);
    for ii=1:sc
        wmt_vec=max(VD.MOTH.ral,wmt_min(ii)):param.pas_wmt:wmt_max(ii);
        wmt_vec=[wmt_vec wmt_max(ii)]';
        wmt_mat(1:length(wmt_vec),ii)=wmt_vec;
    end
    cmt_vec(1,1,:)=[0:param.pas_cmt:max(max(VD.MOTH.cmt_max))  max(max(VD.MOTH.cmt_max))];
    [ERR,pacc]=calc_acc(VD);
    
    % calcul batterie
    % calcul ibat ?? partir de Dsoc
    RdF=ones(size(Dsoc));
    RdF(Dsoc>0)=VD.BATT.RdFarad;
    ibat=-Dsoc./RdF*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100./pas_temps;
    
    [ERR,ubat,~,~,E,R]=calc_batt_fw(ERR,VD.BATT,pas_temps,ibat,soc_p,0);
    
    pbat=ubat.*ibat;
    
    if strcmp(param.Ubus,'cst')
        % a partir d'ici tout est des tableaux de taille
        % (nb_arc,length(wmt_vec),length(cmt_vec),length(temps)] ou des scalaires
        [sy,~]=size(wmt_mat);
        
        sz=length(cmt_vec);
        pbat=reshape(pbat,[1 sx 1 sc]);
        pbat=repmat(pbat,[sy 1 sz]);
        
        %wmt=repmat(wmt_vec,[1 1 sz sc]);
        wmt=repmat(reshape(wmt_mat,[sy 1 1 sc]),[1 1 sz]);
        cmt=repmat(cmt_vec,[sy 1 1 sc]);
        I=find(~isnan(wmt)&~isnan(cmt));
        
        %[ERR,~,cmt(I)]=calc_mt(ERR,VD.MOTH,cmt(I),wmt(I),0,1,0);
        [ERR,~,cmt(I)]=calc_mt(ERR,VD.MOTH,cmt(I),wmt(I),0,1,0,1);
        % calcul des pertes dans les machines
        
        % connexion
        [ERR,cacm2,ccour,wacm2]=calc_train_epi(ERR,VD.EPI,cmt,wmt,repmat(reshape(wcour,[1 1 1 sc]),[sy 1 sz]));
        
        wacm1=repmat(reshape(wprim_red,[1 1 1 sc]),[sy 1 sz]);
        dwacm1=dwprim_red;
        
        % verification du couple max/min de acm2
        cacm2_max=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2);
        cacm2_min=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmin_mot(1,:))),wacm2);
        cacm2(cacm2>cacm2_max | cacm2<cacm2_min)=NaN;
        
        cacm1=repmat(reshape(cprim_red,[1 1 1 sc]),[sy 1 sz])-ccour+VD.ACM1.J_mg*repmat(reshape(dwacm1,[1 1 1 sc]),[sy 1 sz]);
        
        % verification du couple max/min de acm1
        cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
        cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
        cacm1(cacm1>cacm1_max | cacm1<cacm1_min)=NaN;
        
        % calcul puissance elec acm1
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');
        
        pelecacm1=cacm1.*wacm1+qacm1;
        
        % calcul puissance elec acm2
        [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,'1');
        pelecacm2=cacm2.*wacm2+qacm2;
        
         if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
            [ERR,~,~,~,~,Ebat]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,10,100-VD.INIT.Dod0,0,-1,0,param);
            PHT=pelecacm1+pelecacm2;
            UHT=max(VD.ACM1.Tension_cont(end),VD.ACM2.Tension_cont(end));
            [ERR,pertes_dcdc]=calc_dcdc_2(ERR,VD.SURV,PHT,Ebat*ones(size(pelecacm1)),UHT*ones(size(pelecacm1))); % normalement on devrait prendre Ubat et pas UO mais implicite dans ce cas
            pres=PHT + pertes_dcdc + repmat(reshape(pacc,[1 1 1 sc]),[sy 1 sz]); %
        else
            pres=pelecacm1+pelecacm2+repmat(reshape(pacc,[1 1 1 sc]),[sy 1 sz]);
        end
        
        % calcul de la puisance batterie
        %pres=pelecacm1+pelecacm2+repmat(reshape(pacc,[1 1 1 sc]),[sy 1 sz]);
        pbat_syst=pres; % puissance batterie calculee cote systeme
        
        pbat_syst=repmat(pbat_syst,[1 sx 1]);
        bilan_pbat=pbat-pbat_syst; %% Probleme si toute une ligne est NaN et ou toute une ligne est <0 ou >0
        
        if param.verbose>=3
            assignin('base','bilan_pbat',bilan_pbat)
            assignin('base','qacm1',qacm1)
            assignin('base','cacm1',cacm1)
            assignin('base','wacm1',wacm1)
            
            assignin('base','qacm2',qacm2)
            assignin('base','cacm2',cacm2)
            assignin('base','wacm2',wacm2)
            assignin('base','pbat',pbat)
            assignin('base','pbat_syst',pbat_syst)
            assignin('base','pelecacm2',pelecacm2)
            assignin('base','pelecacm1',pelecacm1)
            assignin('base','sx',sx)
            assignin('base','sy',sy)
            assignin('base','sz',sz)
            assignin('base','wmt',wmt)
            assignin('base','cmt',cmt)
            assignin('base','wprim_red',wprim_red)
            assignin('base','cprim_red',cprim_red)
            
            assignin('base','ccour',ccour)
            assignin('base','ibat',ibat)
            assignin('base','ubat',ubat)
            assignin('base','Dsoc',Dsoc)
        end
        
        %% recherche du couple moteur qui boucle le bilan de puissance
        % pour chaque vitesse moteur et chaque courant (Dsoc) batterie
        % cf ancien fichier pour un ebauche de r??solution matricielle
        
        %     cmt_mat=NaN*ones(sy,sx,sc);
        %     bilan_vec=ones(1,sz);
        %
        %     %% Il faut essayer de ne travailler a priori que sur les cas ou l'on a pas des NaN
        %
        %     sum_bilan_pbat=sum(~isnan(bilan_pbat),3);
        %     for hh=2:sc
        %         for ii=1:sy
        %             i_pos_vec=[];
        %             jj_vec=[];
        %             for jj=1:sx
        %                 if (sum_bilan_pbat(ii,jj,hh))>0
        %                     bilan_vec(:)=bilan_pbat(ii,jj,:,hh);
        %                     i_pos=find(bilan_vec>0,1); % indices de la premi??re valeur positive
        %                     %i_neg=find(bilan_vec<0,1,'last'); % indices de la derni??re valeur negative
        %                     %if ~isempty(i_pos) && ~isempty(i_neg) && i_pos~=1
        %                     if ~isempty(i_pos) && i_pos~=1
        %                         % i_pos_vec(jj)=find(bilan_vec>0,1); % indices de la premi??re valeur positive
        %                         i_pos_vec=[i_pos_vec i_pos];
        %                         %i_neg_vec=[i_neg_vec i_neg];
        %                         jj_vec=[jj_vec jj];
        %                     end
        %
        %                 end
        %             end
        %              if ~isempty(jj_vec)
        %                 bilan_pbat_i_pos=bilan_pbat((hh-1)*sx*sy*sz+(i_pos_vec-1)*sx*sy+(jj_vec-1)*sy+ii);
        %                 bilan_pbat_i_neg=bilan_pbat((hh-1)*sx*sy*sz+(i_pos_vec-2)*sx*sy+(jj_vec-1)*sy+ii);
        %
        %                 cmt_i_pos=cmt((hh-1)*sy*sz+(i_pos_vec-1)*sy+ii);
        %                 cmt_i_neg=cmt((hh-1)*sy*sz+(i_pos_vec-2)*sy+ii);
        %
        %                 cmt_mat(ii,jj_vec,hh)=cmt_i_pos-(cmt_i_pos-cmt_i_neg)./(bilan_pbat_i_pos-bilan_pbat_i_neg).*bilan_pbat_i_pos;
        %             end
        %         end
        %     end
        
        % Version matricielle des lignes 321 a 356
        
        bilan_pbat_pos=bilan_pbat;
        bilan_pbat_pos(bilan_pbat_pos<=0)=NaN;
        [bilan_pbat_3D_pos,i_bilan_pbat_pos]=min(bilan_pbat_pos,[],3);
        [cmt_pos] = mat_ind(i_bilan_pbat_pos,3,cmt);
        
        i_bilan_pbat_neg=i_bilan_pbat_pos-1;
        i_bilan_pbat_neg(i_bilan_pbat_neg<1)=1;
        [bilan_pbat_3D_neg] = mat_ind(i_bilan_pbat_neg,3,bilan_pbat);
        [cmt_neg] = mat_ind(i_bilan_pbat_neg,3,cmt);
        
        cmt_mat=cmt_pos-(cmt_pos-cmt_neg)./(bilan_pbat_3D_pos-bilan_pbat_3D_neg).*bilan_pbat_3D_pos;
        cmt_mat(i_bilan_pbat_pos==1)=NaN;
        cmt_mat(:,:,:,1)=NaN;
        
        cmt_mat=reshape(cmt_mat,[sy,sx,sc]);
        
    elseif strcmp(param.Ubus,'var')
         % a partir d'ici tout est des tableaux de taille
        % (nb_arc,length(wmt_vec),length(cmt_vec),length(temps),length(Ubus)] ou des scalaires
        [sy,~]=size(wmt_mat);
        sz=length(cmt_vec);
        %Ubus_vec=param.Ubus_min:param.Ubus_disc:param.Ubus_max;
        Ubus_vec=VD.ACM1.Tension_cont;
        su=length(Ubus_vec);
        Ubus_5D=repmat(reshape(Ubus_vec,[1 1 1 1 su]),[sy 1 sz sc 1]);
        
        
        %% la suite a ecrire
        % NB peut etre laisser dasn MCT_MSA_muse_3D
        % Pert_mot en 2D
        % et appeler les pertes 3D pertes_mot_3D (modif calc_pertes_acm 
        pbat=reshape(pbat,[1 sx 1 sc]);
        pbat=repmat(pbat,[sy 1 sz 1 su]);
        
        %wmt=repmat(wmt_vec,[1 1 sz sc]);
        wmt=repmat(reshape(wmt_mat,[sy 1 1 sc]),[1 1 sz 1 su]);
        cmt=repmat(cmt_vec,[sy 1 1 sc su]);
        I=find(~isnan(wmt)&~isnan(cmt));
        
        %[ERR,~,cmt(I)]=calc_mt(ERR,VD.MOTH,cmt(I),wmt(I),0,1,0);
        [ERR,~,cmt(I)]=calc_mt(ERR,VD.MOTH,cmt(I),wmt(I),0,1,0,1);
        % calcul des pertes dans les machines
        
        % connexion
        [ERR,cacm2,ccour,wacm2]=calc_train_epi(ERR,VD.EPI,cmt,wmt,repmat(reshape(wcour,[1 1 1 sc]),[sy 1 sz 1 su]));
        
        wacm1=repmat(reshape(wprim_red,[1 1 1 sc]),[sy 1 sz 1 su]);
        dwacm1=dwprim_red;
        
        % verification du couple max/min de acm2
        cacm2_max=interp2( VD.ACM2.Tension_cont, VD.ACM2.Regmot_cmax , VD.ACM2.Cmax_mot , Ubus_5D, wacm2);
        cacm2_min=interp2( VD.ACM2.Tension_cont, VD.ACM2.Regmot_cmax , VD.ACM2.Cmin_mot , Ubus_5D, wacm2);  
        cacm2(cacm2>cacm2_max | cacm2<cacm2_min)=NaN;
        
        cacm1=repmat(reshape(cprim_red,[1 1 1 sc]),[sy 1 sz 1 su])-ccour+VD.ACM1.J_mg*repmat(reshape(dwacm1,[1 1 1 sc]),[sy 1 sz 1 su]);
        
        % verification du couple max/min de acm1
        cacm1_max=interp2( VD.ACM1.Tension_cont, VD.ACM1.Regmot_cmax , VD.ACM1.Cmax_mot , Ubus_5D, wacm1);
        cacm1_min=interp2( VD.ACM1.Tension_cont, VD.ACM1.Regmot_cmax , VD.ACM1.Cmin_mot , Ubus_5D, wacm1); 
        cacm1(cacm1>cacm1_max | cacm1<cacm1_min)=NaN;
        
        % calcul puissance elec acm1
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,0,0,'3D',Ubus_5D);
        
        pelecacm1=cacm1.*wacm1+qacm1;
        
        % calcul puissance elec acm2
        [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,0,0,'3D',Ubus_5D);
        pelecacm2=cacm2.*wacm2+qacm2;
        
        
        if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
            [ERR,~,~,~,~,Ebat]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,10,100-VD.INIT.Dod0,0,-1,0,param);
            PHT=pelecacm1+pelecacm2;
            [ERR,pertes_dcdc]=calc_dcdc_2(ERR,VD.SURV,PHT,Ebat*ones(size(pelecacm1)),Ubus_5D); % normalement on devrait prendre Ubat et pas UO mais implicite dans ce cas
            pres=PHT + pertes_dcdc + repmat(reshape(pacc,[1 1 1 sc]),[sy 1 sz]);; %
        else
            pres=pelecacm1+pelecacm2+repmat(reshape(pacc,[1 1 1 sc]),[sy 1 sz]);
        end
        
        
        % calcul de la puisance batterie
        %pres=pelecacm1+pelecacm2+repmat(reshape(pacc,[1 1 1 sc]),[sy 1 sz]);
        pbat_syst=pres; % puissance batterie calculee cote systeme
        
        pbat_syst=repmat(pbat_syst,[1 sx 1]);
        bilan_pbat=pbat-pbat_syst; %% Probleme si toute une ligne est NaN et ou toute une ligne est <0 ou >0
        
        %% recherche du couple moteur qui boucle le bilan de puissance
            
        bilan_pbat_pos=bilan_pbat;
        bilan_pbat_pos(bilan_pbat_pos<=0)=NaN;
        [bilan_pbat_3D_pos,i_bilan_pbat_pos]=min(bilan_pbat_pos,[],3);
        [cmt_pos] = mat_ind(i_bilan_pbat_pos,3,cmt);
        
        i_bilan_pbat_neg=i_bilan_pbat_pos-1;
        i_bilan_pbat_neg(i_bilan_pbat_neg<1)=1;
        [bilan_pbat_3D_neg] = mat_ind(i_bilan_pbat_neg,3,bilan_pbat);
        [cmt_neg] = mat_ind(i_bilan_pbat_neg,3,cmt);
        
        cmt_mat=cmt_pos-(cmt_pos-cmt_neg)./(bilan_pbat_3D_pos-bilan_pbat_3D_neg).*bilan_pbat_3D_pos;
        cmt_mat(i_bilan_pbat_pos==1)=NaN;
        cmt_mat(:,:,:,1)=NaN;
        
        cmt_mat=reshape(cmt_mat,[sy,sx,sc,su]);
    end
    

end

%% calcul des cout
% Calcul des conditions de fonctionnement du moteur thermique
if param.verbose>=3
    assignin('base','cmt_mat',cmt_mat);
end

if (isfield(param,'fit_pertes_acm') & param.fit_pertes_acm==1)
    I=find(~isnan(cmt_mat));
    dcarb=NaN*ones(size(cmt_mat));
    [ERR,dcarb(I),cmt_mat(I)]=calc_mt(ERR,VD.MOTH,cmt_mat(I),wmt(I),0,1,0);
    %[ERR,dcarb,cmt_mat]=calc_mt(ERR,VD.MOTH,cmt_mat,wmt,0,1,0);
     [dcarb,Imin]=min(dcarb);
      dcarb=reshape(dcarb,[sx sc]);
else
    if strcmp(param.Ubus,'cst')
        wmt_mat=repmat(wmt(:,1,1,:),[1 sx]);
        wmt_mat=reshape(wmt_mat,[sy sx sc ]);
        [ERR,dcarb,cmt_mat]=calc_mt(ERR,VD.MOTH,cmt_mat,wmt_mat,0,1,0);
        
        % On met a inf les dcarb des cmt impossible : d??j?? fait par la ligne du
        % dessus
        dcarb(isnan(cmt_mat))=Inf;
        [dcarb,Imin]=min(dcarb);
        dcarb=reshape(dcarb,[sx sc]);
    elseif strcmp(param.Ubus,'var')
        wmt_mat=repmat(wmt(:,1,1,:,1),[1 sx 1 1 su]);
        wmt_mat=reshape(wmt_mat,[sy sx sc su]);
        [ERR,dcarb_4D,cmt_mat]=calc_mt(ERR,VD.MOTH,cmt_mat,wmt_mat,0,1,0);
        
        % On met a inf les dcarb des cmt impossible : d??j?? fait par la ligne du
        % dessus
        %% A faire
        dcarb_4D(isnan(cmt_mat))=Inf;
        [dcarb_3D,Imin_dcarb_4D]=min(dcarb_4D,[],4);
        [dcarb_2D,Imin_dcarb_3D]=min(dcarb_3D,[],1);
        dcarb=reshape(dcarb_2D,[sx sc]);
    end
end




if param.verbose>=3
    assignin('base','dcarb',dcarb);
    assignin('base','Imin',Imin);
end

% Calculs batterie il faut supprimer les arcs impossible
% Jusqu'ici ibat est calcul?? ?? partir de Dsoc.
if nargin==10 % en recalcul on a plus qu'un arc et normalment il est valide
    [ERR,Ibat]=calc_batt(ERR,VD.BATT,pas_temps,pbat(1,:,1),soc_p,0,0,0);
    dcarb(isnan(Ibat))=Inf;
end

% Critere de cout des arcs
if nargin ==9 || nargin==11 && recalcul==0
    % mode tout elec moteur thermique arrete
    dcarb(1,find(elec_pos==1))=0;
    % on est en mode "tout elec" cmt=0 mais le moteur thermique doit tourner ?? wmt_min
    % a priori on as d??ja trouver wmt=wmt_min mais on est pas sur d'avoir
    % converg?? exactement vers cmt=0
    [ERR,dcarb(1,find(elec_pos==0.5))]=calc_mt(ERR,VD.MOTH,0*ones(1,sum(elec_pos==0.5)),wmt_min(find(elec_pos==0.5)),0,1,0);

elseif nargin==11 && recalcul==1    
    % cas hybride et generale (ont pourrais limiter le calcul au cas
    % hybride si besoin
    if strcmp(param.Ubus,'cst')
        cmt=cmt_mat(sub2ind([sy 1 sc],reshape(Imin,[sc 1 1])',1:sc));
        wmt=wmt_mat(sub2ind([sy 1 sc],reshape(Imin,[sc 1 1])',1:sc));
        Ubus=ones(size(cmt));
    elseif strcmp(param.Ubus,'var')   
        Ubus_4D=repmat(reshape(Ubus_vec,[1 1 1 su]),[sy 1 sc 1]);
        [cmt_3D,wmt_3D,Ubus_3D]=mat_ind(Imin_dcarb_4D,4,cmt_mat,wmt_mat,Ubus_4D);
        [cmt,wmt,Ubus]=mat_ind(Imin_dcarb_3D,1,cmt_3D,wmt_3D,Ubus_3D);
        cmt=reshape(cmt,[1 sc]);
        wmt=reshape(wmt,[1 sc]);
        Ubus=reshape(Ubus,[1 sc]);
    end
    
   
     
    %mode tout elec
    cmt(elhyb==0)=0;
    wmt(elhyb==0)=0;
    dcarb(elhyb==0)=0;
    
    % cas "tout elec" mais moth tourne sans fournir de couple 
    cmt(elhyb==0.5)=0;
    wmt(elhyb==0.5)=wmt_min(elhyb==0.5);
    [ERR,dcarb(elhyb==0.5)]=calc_mt(ERR,VD.MOTH,cmt(elhyb==0.5),wmt(elhyb==0.5),0,1,0);
end

cout=dcarb.*pas_temps;
cout(isnan(cout))=Inf;

if nargin==11 && recalcul==1 % cas recalcul   
    
    wcour=wprim_red;
    [ERR,cacm2,ccour,wacm2]=calc_train_epi(ERR,VD.EPI,cmt,wmt,wcour);
    
    % calcul des pertes dans les machines
   
    
    % connexion
    wacm1=wprim_red;  
    dwacm1=dwprim_red;
    
    cacm1=cprim_red-ccour+VD.ACM1.J_mg*dwacm1; 
    
    % calcul puissance elec acm1
    if (isfield(param,'fit_pertes_acm') & param.fit_pertes_acm==1)
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,0,1);
    else
        if strcmp(param.Ubus,'cst')
            [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
        elseif strcmp(param.Ubus,'var')
            [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,0,0,'3D',Ubus);
        end
    end

      
    % calcul puissance elec acm2
    if (isfield(param,'fit_pertes_acm') & param.fit_pertes_acm==1)
         [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,0,1);
    else
        if strcmp(param.Ubus,'cst')
            [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0);
        elseif strcmp(param.Ubus,'var')
            [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,0,0,'3D',Ubus);
        end
    end
    dwacm2=zeros(size(wmt)); %inertie gene et motherm pas prisent en compte pour l'instant
    dwmt=zeros(size(wmt));
    
     %mode tout elec avec embrayage
     if isfield(param,'emb') && param.emb==1
         cacm2(elhyb==0)=0;
         wacm2(elhyb==0)=0;
         qacm2(elhyb==0)=0;
     end
     
    % si on est en recup et que acm1 non aliment??
    cfrein_meca_pelec_mot=zeros(1,sc);
    cfrott_acm1=zeros(1,sc);
    I=find(elhyb==0 & cprim_red<0 & qacm1>-cacm1.*wacm1);% recup avec plus de pertes dans acm1 que de puissance surl'arbre
    cfrott_acm1(I)=max(interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,wacm1(I)),cprim_red(I)); %la machine elec ne freinera pas plus que le couple de freinage
    cacm1(I)=0;
    cfrein_meca_pelec_mot(I)=1;
    qacm1(I)=0;
    
    Rbat=R;
    u0bat=E*ones(size(cprim_red));
    [ERR,r]=affect_struct(r_var);
    
end

if sum((sum(isinf(cout(:,2:end))))==size(cout,1))
    chaine=' all edge are impossible, the systems possibilites does not allow to fullfil the required systems operating point ! \n time : %d \n Dsoc : %s  \n cost : %s \n ibat: %s \n ubat: %s \n cmt: %s \n wmt: %s';
    ERR=MException('BackwardModel:calc_cout_arc_HPDP_EPI_SURV',chaine,num2str(Dsoc),num2str(cout),num2str(ibat),num2str(ubat),num2str(cmt),num2str(wmt));
    [cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=backward_erreur(length(Dsoc));
    [ERR,r]=affect_struct(r_var);
    return
end

if sum(isnan(cout))
    chaine='some edge cost are NaN, to avoid problems in solve graph, prefer to fixe them to Inf \n time : %d' ;
    warning('BackwardModel:calc_cout_arc_HPDP_EPI_SURV',chaine,i);
end

return

