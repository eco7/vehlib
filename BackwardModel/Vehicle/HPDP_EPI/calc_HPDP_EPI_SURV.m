
% function [ERR, VD, ResXml]=calc_HPDP_EPI_SURV(ERR,vehlib,param,VD)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul d'un vehicule electrique de type hybrid parallele
% a derivation de puissance
%
% Arguments appel :
% Arguments de sortie :
%
% 21-11-11(EV): Creation 

function [ERR, VD, ResXml,r]=calc_HPDP_EPI_SURV(ERR,vehlib,param,VD)
global Ham_hors_limite;
Ham_hors_limite=1e6;

% Verification cohérence carto et tension Ubus
if (isfield(param,'Ubus') & strcmp(param.Ubus,'var')) & ~isfield(VD.ACM1,'Pert_mot_3D')
    warning('attention Ubus variable et pas de carto fonction de Ubus')
elseif isfield(param,'Ubus') & strcmp(param.Ubus,'cst') & isfield(VD.ACM1,'Pert_mot_3D')
    warning('attention Ubus constante et carto fonction de Ubus ou optimale')
end

% Calcul cinematiques complementaires
% Avec re-echantillonnage eventuel
if ~isfield(param,'pas_temps') | param.pas_temps==0
    param.pas_temps=1; % si param_pas temps n'existe pas ou =0 on reechantillone a 1s
end
[ERR,VD,dist]=calc_cinemat(ERR,VD,param);

if ~isempty(ERR)
    ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

if (isfield(param,'fit_pertes_acm') & param.fit_pertes_acm==1)
    [VD.ACM1] = fitcarto(VD.ACM1,param.verbose);
    [VD.ACM2] = fitcarto(VD.ACM2,param.verbose);
end

% Initialisation
soc=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));
E=zeros(size(VD.CYCL.temps));
R=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
pbat=zeros(size(VD.CYCL.temps));

% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
if ~isempty(ERR)
    ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,penteFpk,fres,faero,froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Connexion
csec_red=croue;
wsec_red=wroue;
dwsec_red=dwroue;

% Calcul des conditions en amont du reducteur
[ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);
if ~isempty(ERR)
    ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Prise en compte du frein meca quand Croue <0, on consid??re que dans ce
% cas on est en tout elec recup max sur acm1 connexion

wacm1=wprim_red;
dwacm1=dwprim_red;
cacm1=cprim_red+VD.ACM1.J_mg*dwacm1;

% On vient limiter cacm1 a sa valeur min
cacm1=max(cacm1,interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1));
if isfield(VD.ECU,'Vit_Cmin')
    cacm1=max(cacm1,interp1(VD.ECU.Vit_Cmin,VD.ECU.Cpl_min,wacm1));
end

% On limite le couple red en consequence
cprim_red=cacm1-VD.ACM1.J_mg*dwacm1;

% On prend en compte la limitation de la batterie
% on recherche les indices ou on vient saturer ibat a ibat min
if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
    [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,0,1);
else
    if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
        if strcmp(param.Ubus,'var')
            [ERR,qacm1,Udcdc_ht]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
        else
            Udcdc_ht=VD.ACM1.Tension_cont(end)*ones(size(cacm1));
            [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
        end
    else
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
    end
end



% calcul puissance motelec
pacm1=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1);
[ERR,pacc]=calc_acc(VD);

if (isfield(param,'conv_dcdc') && param.conv_dcdc==1)
    [ERR,~,~,~,~,Ebat]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,10,100-VD.INIT.Dod0,0,-1,0,param);
    [ERR,pertes_dcdc]=calc_dcdc_2(ERR,VD.SURV,pacm1,Ebat*ones(size(pacm1)),Udcdc_ht); % normalement on devrait prendre Ubat et pas UO mais implicite dans ce cas
    pres=pertes_dcdc+pacm1+pacc;
else
    pres=pacm1+pacc;
end

[ERR,ibat,ubat]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,pres,100-VD.INIT.Dod0,0,-1,0,param);

ind=find((isnan(ibat)&cprim_red<0));


% on vient rechercher le couple max de la machine correspondant a ibat_min
if ~isempty(ind)
    %Connexion
    wcour=wprim_red;
    
    %% recherche du mode tout electrique ?? chaque pas de temps
    
    % ici doit venir s'ajouter les pertes  acm2 qui tourne pour ??quilibrer le train. On suppose dans les cas ou le moteur
    % thermique tourne qand avec cmt=0), que acm2 sera quand m??me  aliment?? pour garantir cacm2 = 0. C'est ce qui est fait dans VEHLIB
    % un autre choix est de ne pas alimenter la gene et de compenser les pertes meca.
    
    % Calcul de la vitesse min possible du moteur thermique pour v??rifier que l emode tout ??lec est possible
    wmt_min=max(0,(-VD.ECU.wmaxge-VD.EPI.kb*wcour)./(1-VD.EPI.kb)); % limitation sur vit max du moth et vit max de la generatrice
    wmt_min=max(wmt_min,wcour-VD.ECU.wmaxps*VD.EPI.Nsat/VD.EPI.Ncour ); % limitation sur la vitesse max au niveau satellite / porte satellite
    
    % Le moteur ne peut pas tourner en dessous de sa vitesse de ral
    wmt_min(wmt_min~=0)=max(VD.MOTH.ral,wmt_min(wmt_min~=0));
    
    
    if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
        wmt_min(wprim_red>VD.ECU.vmaxele/VD.VEHI.Rpneu*VD.RED.kred)=VD.MOTH.ral;
    end

    
    Ibat_min=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,100-VD.INIT.Dod0);
    [ERR,Ubat_min]=calc_batt_fw(ERR,VD.BATT,VD.CYCL.pas_temps,Ibat_min,100-VD.INIT.Dod0,0); % il peut peut ??tre rester un soucis si Ubat_min<VD.BATT.Ubat_min
      
    for i = 1: length(ind)
        ii=ind(i);
        cacm1_vec=0:cacm1(ii)/100:cacm1(ii);
        if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
            [ERR,qacm1_vec]=calc_pertes_acm(ERR,VD.ACM1,cacm1_vec,wacm1(ii)*ones(size(cacm1_vec)),dwacm1(ii)*ones(size(cacm1_vec)),0,1);
        else
            [ERR,qacm1_vec]=calc_pertes_acm(ERR,VD.ACM1,cacm1_vec,wacm1(ii)*ones(size(cacm1_vec)),dwacm1(ii)*ones(size(cacm1_vec)));
        end
        
        [ERR,~,~,wacm2]=calc_train_epi(ERR,VD.EPI,0,wmt_min(ii),wprim_red(ii));
        
        if (isfield(param,'fit_pertes_acm') && param.fit_pertes_acm==1)
            [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,0*ones(size(wacm2)),wacm2,0,0,1);
        else
            [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,0*ones(size(wacm2)),wacm2,0,1);
        end
        
        if isfield(param,'emb') && param.emb==1
            qacm2=0;
        end

        
        bilan_puis=Ibat_min*Ubat_min-pacc(ii)-cacm1_vec*wacm1(ii)-qacm1_vec-qacm2;
        [~,Imin]=min(abs(bilan_puis));
        if bilan_puis(Imin)>=0 % on interpolle avec la valeur precedente on recalcule le couple primaire reducteur
            if Imin-1>=1
                cacm1(ii)=cacm1_vec(Imin)-bilan_puis(Imin)*(cacm1_vec(Imin)-cacm1_vec(Imin-1))/(bilan_puis(Imin)-bilan_puis(Imin-1));
                cprim_red(ii)=cacm1(ii)-VD.ACM1.J_mg*dwacm1(ii);
            end
        else
            if Imin+1<=length(cacm1_vec)
                cacm1(ii)=cacm1_vec(Imin)-bilan_puis(Imin)*(cacm1_vec(Imin+1)-cacm1_vec(Imin))/(bilan_puis(Imin+1)-bilan_puis(Imin));
                cprim_red(ii)=cacm1(ii)-VD.ACM1.J_mg*dwacm1(ii);
            end
        end
    end
end

% Recalcul de la partie frein meca au niveau de la roue
 [ERR,csec_red,wsec_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
 cfrein_meca=csec_red-croue;
 
%% strategie selon les cas (doivent renvoyer des vecteurs cprim2_cpl et
%% elhyb de taille VD.CYCL.temps)
if strcmp(lower(param.optim),'ligne')  
    [ERR,cmt,wmt,elhyb]=calc_ligne_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
elseif strcmp(lower(param.optim),'lagrange')
    [ERR,cmt,wmt,elhyb]=calc_lagrange_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
elseif strcmp(lower(param.optim),'prog_dyn')
    [ERR,VD,r]=calc_prog_dyn_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
end

if ~isempty(ERR)
    ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2,NaN);
    return;
end

% grandeurs a rajouter dans r
r_var_sup={'croue','wroue','cprim_red','wprim_red','dwprim_red','csec_red','wsec_red','dwsec_red'};
[ERR,r]=affect_struct(r_var_sup,r);  

if ~isempty(ERR)
    ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2,NaN);
    return;
end

% Calcul des auxiliaires electriques
[ERR,r.pacc]=calc_acc(VD);
if ~isempty(ERR)
    ResXml=struct([]); %     [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end
  
%% recalcul des couples du aux freinages meca si on a choisit
% de ne pas alimente la machines en fin de freinage
% (cfrein_meca_pelec_mot=1)
r.cfrein_meca=cfrein_meca;
r.cprim_red(r.cfrein_meca_pelec_mot==1)=r.cfrott_acm1(r.cfrein_meca_pelec_mot==1)-VD.ACM1.J_mg*r.dwacm1(r.cfrein_meca_pelec_mot==1);
[ERR,r.csec_red(r.cfrein_meca_pelec_mot~=0)]=calc_red_fw(ERR,VD.RED,r.cprim_red(r.cfrein_meca_pelec_mot~=0),r.wprim_red(r.cfrein_meca_pelec_mot~=0),r.dwprim_red(r.cfrein_meca_pelec_mot~=0),0);
r.cfrein_meca(r.cfrein_meca_pelec_mot~=0)=r.csec_red(r.cfrein_meca_pelec_mot~=0)-r.croue(r.cfrein_meca_pelec_mot~=0);

r.qacm1_frott=-r.cfrott_acm1.*r.wacm1;
r.qacm1=r.qacm1+r.qacm1_frott;

% Cycle
r.tsim=VD.CYCL.temps;
r.vit=VD.CYCL.vitesse;
r.acc=VD.CYCL.accel;
r.vit_dem=r.vit;
r.pente=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,VD.CYCL.distance,'linear',0); % reechantillonage pente fonction distance claculee.     
r.masse=masse;

r.wcour_train=r.wprim_red;
r.cps_train=r.cmt;
r.wps_train=r.wmt;
[ERR,~,r.ccour_train]=calc_train_epi(ERR,VD.EPI,r.cps_train,r.wps_train,r.wcour_train);
r.csoleil_train=r.cacm2;
r.wsoleil_train=r.wacm2;


[ResXml]=miseEnForme4VEHLIB(vehlib,'caller','r');

% Calcul de resultats synthetiques
[Res]=resultats(VD,param,ResXml);

conso100=Res.conso100;
cumcarb=Res.cumcarb;
co2_eq=Res.co2_eq;
CO2_gkm=Res.CO2_gkm;
ibat_eff=Res.ibat_eff;
ibat_moy=Res.ibat_moy;

% Bilan de puissance et d'energie
%[bp]= bilanPE(VD,param,ResXml);
%bilanP=bp.bilanP;

% Ajout des grandeurs synthétiques dans une nouvelle table de la structure xml
[ResXml]=miseEnForme4GestionEnergie(ResXml);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,cmt,wmt,elhyb]=calc_ligne_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,cmt,wmt,elhyb]=calc_lagrange_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);


return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%??
function [ERR,VD,r]=calc_prog_dyn_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red)

r_var={'elhyb','soc','ibat_max','ibat_min','elec_pos','soc_min','soc_max','Dsoc_min','Dsoc_max',...
    'lim_inf','lim_sup','lim_elec','soc_inf','soc_sup'...
    'poids_min','meilleur_chemin'};

[ERR,VD.ACM1]=inverse_carto_melec(VD.ACM1,0);

tdeb_calc_limites=cputime;

[ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=...
    calc_limites_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red);

% verification de la precision du graph pour forcer le nombre
% d'arc sur un pas de temps a une valeur min (sinon peut devenir 0)
if isfield(param,'Nb_arc_min') & param.Nb_arc_min~=0 & abs(Dsoc_min(end)-Dsoc_max(end))~=0
    if (abs(Dsoc_min(end)-Dsoc_max(end))/param.Nb_arc_min < -param.prec_graphe) | (param.prec_graphe==0) | ~isfield(param,'prec_graphe')
        val_prec=-abs(Dsoc_min(end)-Dsoc_max(end))/param.Nb_arc_min;
        param.prec_graphe=val_prec;
        assignin('base','val_prec',val_prec);
        evalin('base','param.prec_graphe=val_prec;');
        if isfield(param,'prec_graphe') & param.prec_graphe~=0
              chaine='Graph precision automatically reduce by param.Nb_arc_min \ngraph precision : %f' ;
              warning('BackwardModel:calc_cout_arc_HPDP_EPI_SURV',chaine,param.prec_graphe);
        end
    end
end

[ ERR,lim_inf,lim_sup,lim_elec,soc_inf,soc_sup] =limite2indice_soc(VD,soc_min,soc_max,param);

tfin_calc_limites=cputime;


if ~isempty(ERR)
    chaine='error in limite2indice_soc' ;
    ERR=MException('BackwardModel:calc_HPDP_EPI_SURV',chaine);
    r=struct([]);
    return
end

% Information sur le graphe
if param.verbose>=1 || param.verbose==-1
    info_graph(VD,param,Dsoc_min,Dsoc_max,lim_sup,lim_inf);
end


var_com_inf=Dsoc_min;
var_com_sup=Dsoc_max;

tdeb_resolution=cputime;


if isfield(param,'calc_mat') && param.calc_mat==1
    [ERR,poids_min,meilleur_chemin]= ...
        solve_graph_mat(ERR,@(ERR,Dsoc) calc_cout_arc_HPDP_EPI_SURV_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,100-VD.INIT.Dod0),...
        param,lim_sup,lim_inf,var_com_inf,var_com_sup,0,lim_elec);
else
    [ERR,poids_min,meilleur_chemin]= ...
        solve_graph(ERR,@(ERR,ii,Dsoc) calc_cout_arc_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,100-VD.INIT.Dod0,ii),...
        param,lim_sup,lim_inf,var_com_inf,var_com_sup,0,lim_elec);
end

tfin_resolution=cputime;

% cas ou les meilleurs chemin sont inf, on sort en erreur
if sum(poids_min)==Inf
   chaine=' Attention les poids min sont infinis %6.3f%6.3f \n Essayer de diminuer la precision du graph';
   ERR=MException('BackwardModel:calc_HPDP_EPI_SURV',chaine,poids_min(1),poids_min(2));
   r=struct([]);
   return;
end

% if isnan(poids_min) | ~isempty(ERR)
%     chaine='min weight equal to NaN or ERR non empty after solve_graph ' ;
%     warning('BackwardModel:calc_cout_arc_HPDP_EPI',chaine);
%    [elhyb,ibat,soc,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,cfrott_acm1,cfrein_meca_pelec_mot]=backward_erreur(length(VD.CYCL.temps),0,0);
%     return
% end

if isnan(poids_min(1)) || ~isempty(ERR)
    chaine='min weight equal to NaN or ERR non empty after solve_graph ';
    if isempty(ERR)
        %warning('BackwardModel:calc_cout_arc_HPDP_EVT',chaine);
        ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
    else
        ERR1=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
        ERR=addCause(ERR1,ERR);
        ERR
        ERR.cause{1}
    end
    r=struct([]);
    return
end

tdeb_recalcul=cputime;

% passage chemin aux var de commande
% NB il faut appeler calc_cout_arc pour des soucis d'accrochage sur arc tout thermique
if isfield(param,'calc_mat') & param.calc_mat==1
    [ERR,Dsoc,elhyb]=chemin2varcomBATT_mat(ERR,@(ERR,Dsoc) calc_cout_arc_HPDP_EPI_SURV_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,100-VD.INIT.Dod0),...
        param,lim_sup,lim_inf,lim_elec,var_com_inf,var_com_sup,meilleur_chemin(1,:),elec_pos);
else
    [ERR,Dsoc,elhyb]=chemin2varcomBATT(ERR,@(ERR,ii,Dsoc) calc_cout_arc_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,100-VD.INIT.Dod0,ii),...
        param,lim_sup,lim_inf,lim_elec,var_com_inf,var_com_sup,meilleur_chemin(1,:),elec_pos);
end

% Recalcul de toutes les grandeurs du vehicule.
if param.verbose>=1
    if isfield(param,'calc_mat') & param.calc_mat==1
        h = msgbox('recalcul des grandeurs','wait');
    else
        h = waitbar(0,'recalcul des grandeurs');
    end
end

if isfield(param,'calc_mat') & param.calc_mat==1
    [ERR,~,r]=calc_cout_arc_HPDP_EPI_SURV_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,100-VD.INIT.Dod0,1,elhyb);
else
    for i=2:1:length(VD.CYCL.temps)
        if param.verbose>=1
            waitbar(i/length(VD.CYCL.temps),h)
        end
        [ERR,~,ibat(i),ubat(i),R(i),E(i),RdF(i),dcarb(i),cmt(i),wmt(i),dwmt(i),cacm1(i),wacm1(i),dwacm1(i),qacm1(i),cacm2(i),wacm2(i),dwacm2(i),qacm2(i),Dsoc(1),elhyb(i),cfrott_acm1(i),cfrein_meca_pelec_mot(i)]=...
            calc_cout_arc_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc(i),elec_pos,100-VD.INIT.Dod0,i,1,elhyb(i));
    end
end

if param.verbose>=1
    close(h); % fermeture waitbar
end

tfin_recalcul=cputime;
if param.verbose>=1 | param.verbose==-1
    fprintf(1,'%s %.2f %s \n','temps de calcul des limites :',tfin_calc_limites-tdeb_calc_limites,'s');
    fprintf(1,'%s %.2f %s \n','temps de resolution du graph calc arc compris :',tfin_resolution-tdeb_resolution,'s');
    fprintf(1,'%s %.2f %s \n','temps de recalcul:',tfin_recalcul-tdeb_recalcul,'s');
    fprintf(1,'%s %.2f %s \n','temps global:',tfin_recalcul-tdeb_calc_limites,'s');
end

% Ne pas calculer les soc par trapz mais par des sommes.
%r.soc=ones(1,length(r.Dsoc));
%r.soc(1)=100-VD.INIT.Dod0;
%r.soc(2:1:%(r.Dsoc))=r.soc(1:1:length(r.Dsoc)-1)+r.Dsoc(2:1:end);

%for i=2:1:length(Dsoc)
 %   r.soc(i)=r.soc(i-1)+r.Dsoc(i);
%    soc2(i)=soc2(i-1)+Dsoc2(i);
%     if param.tout_thermique==1;
%        soc1_tt(i)=soc1_tt(i-1)+Dsoc1_tt(i);
%     end
%end

soc_init=100-VD.INIT.Dod0;
r.soc=cumsum(Dsoc);
r.soc=soc_init+r.soc;



[ERR,r]=affect_struct(r_var,r);

if param.verbose>=2
    figure(1)
    clf
    plot(VD.CYCL.temps,r.ibat_max,VD.CYCL.temps,r.ibat_min,VD.CYCL.temps,r.ibat,VD.CYCL.temps,VD.CYCL.vitesse*3.6)
    grid
    ylabel('courant en A')
    legend('ibat max','ibat min','ibat','vitesse')
    
    figure(2)
    clf
    hold on
    plot(VD.CYCL.temps,r.soc_min,VD.CYCL.temps,r.soc_max,VD.CYCL.temps,r.soc_inf,VD.CYCL.temps,soc_sup)
    legend('soc min','soc max','soc inf','soc sup')
    title('limites sur le soc')
    
    figure(3)
    clf
    hold on
    plot(VD.CYCL.temps+1,r.meilleur_chemin,VD.CYCL.temps+1,lim_inf,VD.CYCL.temps+1,lim_sup,VD.CYCL.temps+1,r.lim_elec,VD.CYCL.temps+1,VD.CYCL.vitesse*36)
    legend('meilleur chemin 1','meilleur chemin 2','lim inf','lim sup','lim elec','vitesse')
    title('meilleur chemin en indice')
    grid
end  

if param.verbose>=1   
    figure(4)
    clf
    hold on            
    plot(VD.CYCL.temps,r.soc_inf,'k',VD.CYCL.temps,r.soc_sup,'k',VD.CYCL.temps,r.soc,'b',VD.CYCL.temps,VD.CYCL.vitesse*3.6,'c')  
    legend('soc inf','soc sup''soc','vit')
    title('meilleur chemin en soc');
    grid
   
    figure(5)
    clf
    hold on
    plot(VD.CYCL.temps,r.ibat,VD.CYCL.temps,r.ubat,VD.CYCL.temps,r.dcarb)
    legend('ibat','ubat','dcarb')
    grid
end

return
