% function [ERR,cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=calc_cout_arc_HPDP_EVT_SURV(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,soc_p,i,recalcul,elhyb)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Calcul des cout des arcs cas HPDP_EPI
%
% interface entrée :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discretisation etc)
% CYLC,VD.ADCPL...:structures de description vehicule et composants
% cprim_red :   Couple sur le primaire du reducteur (vecteur sur le cycle)
% wprim_red :   vitesse sur le primaire du reducteur  (vecteur sur le cycle)
% dprim_red :   derivée de la vitesse sur le primaire du reducteur (vecteur sur le cycle)
% Dsoc :        variation de soc sur l'eventail des arcs possible
% elec_pos :    mode electrique possible (vecteur sur le cycle)
% soc_p :       soc à l'instant precedent
% i :           instant dans le cycle
% recalcul :    si on est en phase de recalcul (grahe deja resolu) recalcul =1 sinon 0 ou pas passer dans l'interface
% elhyb :       mode elec/hybride/thermique en phase recalcul uniquement
%
% interface de sortie
% cout :        cout des arcs de l'éventail
% ibat,ubat ...:variable vehicule utilie uniquement en recalcul sinon on ne renvoie que le cout
%
% 21-11-11(EV): Creation 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ERR,cout,ibat,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,Dsoc,elhyb,cfrott_acm1,cfrein_meca_pelec_mot]=...
    calc_cout_arc_HPDP_EPI_SURV(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,soc_p,i,recalcul,elhyb)

wprim_red=wprim_red(i); 
dwprim_red=dwprim_red(i);
cprim_red=cprim_red(i);

[Li,Co]=size(soc_p);
if sum(sum(isnan(soc_p)))==Li*Co
    ERR=MException('BackwardModel:calc_cout_arc_HP_BV_2EMB', 'appel a calc_cout_arc_HP_BV_2EMB : tout les elements d''un des vecteur precedent a NaN');
    [cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=backward_erreur(size(Dsoc));
    return
end

%Connexion
wcour=wprim_red;


% Calcul des vitesses max et min de rotation du moteur thermique
% Grace aux lignes suivantes les vitesses max de la géné et des portes
% satellites ne peuvent normalement pas être atteinte

wmt_max=min(VD.MOTH.wmt_maxi,(VD.ECU.wmaxge-VD.EPI.kb*wcour)./(1-VD.EPI.kb)); % limitation sur vit max du moth et vit max de la generatrice
wmt_max=min(wmt_max,wcour+VD.ECU.wmaxps*VD.EPI.Nsat/VD.EPI.Ncour ); % limitation sur la vitesse max au niveau satellite / porte satellite 

wmt_min=max(0,(-VD.ECU.wmaxge-VD.EPI.kb*wcour)./(1-VD.EPI.kb)); % limitation sur vit max du moth et vit max de la generatrice
wmt_min=max(wmt_min,wcour-VD.ECU.wmaxps*VD.EPI.Nsat/VD.EPI.Ncour ); % limitation sur la vitesse max au niveau satellite / porte satellite 

% Le moteur ne peut pas tourner en dessous de sa vitesse de ral
wmt_min(wmt_min~=0)=max(VD.MOTH.ral,wmt_min(wmt_min~=0));

if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
    wmt_min(wprim_red>VD.ECU.vmaxele/VD.VEHI.Rpneu*VD.RED.kred)=VD.MOTH.ral;
end

% creation des tables sur les vitesses et couple moteur 
wmt_vec=max(VD.MOTH.ral,wmt_min):param.pas_wmt:wmt_max;
wmt_vec=[wmt_vec wmt_max]';
cmt_vec(1,1,:)=[0:param.pas_cmt:max(max(VD.MOTH.cmt_max))  max(max(VD.MOTH.cmt_max))];
[ERR,pacc]=calc_acc(VD);

% calcul batterie
% calcul ibat à partir de Dsoc
RdF=ones(size(Dsoc));
RdF(Dsoc>0)=VD.BATT.RdFarad;
ibat=-Dsoc./RdF*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100/param.pas_temps;


[ERR,ubat,~,~,E,R]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat,soc_p,0);

pbat=ubat.*ibat;

% a partir d'ici tout est des tableaux de taille
% (nb_arc,length(wmt_vec),length(cmt_vec) ou des scalaires
sx=length(Dsoc);
sy=length(wmt_vec);
sz=length(cmt_vec);
pbat=repmat(pbat,[sy 1 sz]);
wmt=repmat(wmt_vec,[1 1 sz]);
cmt=repmat(cmt_vec,[sy 1 1]);

[ERR,~,cmt]=calc_mt(ERR,VD.MOTH,cmt,wmt,0,1,0);

% calcul des pertes dans les machines

% connexion
[ERR,cacm2,ccour,wacm2]=calc_train_epi(ERR,VD.EPI,cmt,wmt,wcour);

wacm1=repmat(wprim_red,[sy 1 sz]);
dwacm1=dwprim_red;

% verification du couple max/min de acm2
cacm2_max=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2);
cacm2_min=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmin_mot(1,:))),wacm2);
cacm2(cacm2>cacm2_max | cacm2<cacm2_min)=NaN;

cacm1=cprim_red-ccour+VD.ACM1.J_mg*dwacm1;
% verification du couple max/min de acm2
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
cacm1(cacm1>cacm1_max | cacm1<cacm1_min)=NaN;

% calcul puissance elec acm1
[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');
pelecacm1=cacm1.*wacm1+qacm1;


% calcul puissance elec acm2
[ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,'1');
pelecacm2=cacm2.*wacm2+qacm2;

% calcul de la puisance batterie
pres=pelecacm1+pelecacm2+pacc(i);
pbat_syst=pres; % puissance batterie calculee cote systeme

pbat_syst=repmat(pbat_syst,[1 sx 1]);
bilan_pbat=pbat-pbat_syst; %% Problème si toute une ligne est NaN et ou toute une ligne est <0 ou >0

if param.verbose>=3
    assignin('base','bilan_pbat',bilan_pbat)
    assignin('base','qacm1',qacm1)
    assignin('base','cacm1',cacm1)
    assignin('base','wacm1',wacm1)
    
    assignin('base','qacm2',qacm2)
    assignin('base','cacm2',cacm2)
    assignin('base','wacm2',wacm2)
    assignin('base','pbat',pbat)
    assignin('base','pbat_syst',pbat_syst)
    assignin('base','pelecacm2',pelecacm2)
    assignin('base','pelecacm1',pelecacm1)
    assignin('base','sx',sx)
    assignin('base','sy',sy)
    assignin('base','sz',sz)
    assignin('base','wmt',wmt)
    assignin('base','cmt',cmt)
    assignin('base','wprim_red',wprim_red)
    assignin('base','cprim_red',cprim_red)
    
    assignin('base','ccour',ccour)
    assignin('base','ibat',ibat)
    assignin('base','ubat',ubat)
    assignin('base','Dsoc',Dsoc)
end

%% recherche du couple moteur qui boucle le bilan de puissance
% pour chaque vitesse moteur et chaque courant (Dsoc) batterie
% cf ancien fichier pour un ebauche de résolution matricielle

cmt_mat=NaN*ones(sy,sx);
bilan_vec=ones(1,sz);
for ii=1:sy
    for jj=1:sx
        bilan_vec(:)=bilan_pbat(ii,jj,:);
        i_pos=find(bilan_vec>0,1); % indices de la première valeur positive
        i_neg=find(bilan_vec<0,1,'last'); % indices de la dernière valeur negative
        if ~isempty(i_pos) && ~isempty(i_neg) && i_pos~=1 % sinon le système ne permet pas de boucler le bilan de puissance on laissera cmt_mat(i,j) à NaN
            % si i_pos=1 et que i_neg n'est pas vide c'est que bilan de puissance bizard on laisse a NaN pourl'instant
            % on doit vérifier que i_neg est bien ègal à i_pos-1 sinon on est dans un cas "bizard" (bilan non monotone croissant)
            if i_neg~=i_pos-1
                if param.verbose>=2
                    chaine='bilan de puissance non monotone croissant \n i: %d \n j: %d \n bilan_vec : %s' ;
                    warning('BackwardModel:calc_cout_arc_HPDP_EVT_SURV',chaine,i,j,num2str(bilan_vec));
                end
                i_neg=i_pos-1;
            end
            cmt_mat(ii,jj)=cmt(ii,i_pos)-(cmt(ii,i_pos)-cmt(ii,i_neg))/(bilan_pbat(ii,jj,i_pos)-bilan_pbat(ii,jj,i_neg)).*bilan_pbat(ii,jj,i_pos);
        end

    end
end


%% Recalcul du bilan de puissance utile pour developpement voir ce que l'on en fait par la suite ??
%% peut rester un eetape de validation en mettant _rec à chaque variable
% [ERR,cacm2,ccour,wacm2]=calc_train_epi(ERR,VD.EPI,cmt_mat,wmt(:,:,1),wcour(:,:,1));
% 
% % calcul des pertes dans les machines
% 
% % connexion
% wacm1=repmat(wprim_red,[sy sx]);
% dwacm1=dwprim_red;
% 
% cacm1=cprim_red-ccour-VD.ACM1.J_mg*dwacm1;
% 
% % calcul puissance elec acm1
% [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');
% pelecacm1=cacm1.*wacm1+qacm1;
% 
% 
% % calcul puissance elec acm2
% [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,'1');
% pelecacm2=cacm2.*wacm2+qacm2;
% 
% % verification limitations sur les vitesses ??
% 
% % calcul de la puisance batterie
% pres=pelecacm1+pelecacm2+pacc(i);
% pbat_syst=pres; % puissance batteire calculee cote systeme
% 
% 
% bilan_pbat_rec=pbat(:,:,1)-pbat_syst;
%% fin recalcul bilan puissance


%% calcul des cout
% Calcul des conditions de fonctionnement du moteur thermique
if param.verbose>=3
    assignin('base','cmt_mat',cmt_mat);
end

wmt_mat=repmat(wmt(:,:,1),[1 sx]);
[ERR,dcarb,cmt_mat]=calc_mt(ERR,VD.MOTH,cmt_mat,wmt_mat,0,1,0);


% On met à inf les dcarb des cmt impossible : déjà fait par la ligne du
% dessus
dcarb(isnan(cmt_mat))=Inf;


[dcarb,Imin]=min(dcarb);

if param.verbose>=3
    assignin('base','dcarb',dcarb);
    assignin('base','Imin',Imin);
end


%% recalcul des grandeurs
%cmt=cmt_mat(sub2ind([sy sx],Imin,1:sx)); 
%wmt=wmt_mat(sub2ind([sy sx],Imin,1:sx));

% Calculs batterie il faut supprimer les arcs impossible
% Jusqu'ici ibat est calculé à partir de Dsoc.
if nargin==21 % en recalcul on a plus qu'un arc et normalment il est valide
    [ERR,Ibat]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat(1,:,1),soc_p,0,0,0);
    dcarb(isnan(Ibat))=Inf;
end

% Critere de cout des arcs
if nargin ==21 || nargin==23 && recalcul==0
    if elec_pos(i)==1 % on est en mode tout elec moteur thermique arrêté
        dcarb(1)=0;
    elseif elec_pos(i)==2 % on est en mode "tout elec" cmt=0 mais le moteur thermique doit tourner à wmt_min
        % a priori on as déja trouver wmt=wmt_min mais on est pas sur d'avoir
        % convergé exactement vers cmt=0
        [ERR,dcarb(1),cfrein_meca_mt]=calc_mt(ERR,VD.MOTH,0,wmt_min,0,1,0);
    end
elseif nargin==23 && recalcul==1
    if elhyb==0 %mode tout elec
        cmt=0;
        wmt=0;
        dcarb=0;
    elseif elhyb==0.5; % cas "tout elec" mais moth tourne sans fournir de couple 
        cmt=0;
        wmt=wmt_min;
        [ERR,dcarb,cfrein_meca_mt]=calc_mt(ERR,VD.MOTH,cmt,wmt,0,1,0);
        %elhyb=1; % on remet elhyb a 1 pour afficher les resultats
    else
        cmt=cmt_mat(sub2ind([sy sx],Imin,1:sx)); % ici Imin est de taille 1 1 pas besoin de tou ce cirque !!
        wmt=wmt_mat(sub2ind([sy sx],Imin,1:sx));
    end
end

cout=dcarb*param.pas_temps;
cout(isnan(cout))=Inf;

if nargin==23 && recalcul==1 % cas recalcul   
    wcour=wprim_red;
    [ERR,cacm2,ccour,wacm2]=calc_train_epi(ERR,VD.EPI,cmt,wmt,wcour);
    
    % calcul des pertes dans les machines
    
    % connexion
    wacm1=wprim_red;  
    dwacm1=dwprim_red;
    
    cacm1=cprim_red-ccour+VD.ACM1.J_mg*dwacm1; 
    
    % calcul puissance elec acm1
    [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');

      
    % calcul puissance elec acm2
    [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,'1');
     dwacm2=0; %inertie gene et motherm pas prisent en compte pour l'instant 
     dwmt=0;
     
     % si on est en recup et que acm1 non alimenté
     cfrein_meca_pelec_mot=0; 
     cfrott_acm1=0;
     if elhyb==0 && cprim_red<0 && qacm1>-cacm1*wacm1 % recup avec plus de pertes dans acm1 que de puissance surl'arbre

         cfrott_acm1=max(interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,wacm1),cprim_red); %la machine elec ne freinera pas plus que le couple de freinage
         cacm1=0;
         cfrein_meca_pelec_mot=1;
         qacm1=0;   
     end   
end

if sum(isinf(cout))==length(cout)
    chaine=' all edge are impossible, the systems possibilites does not allow to fullfil the required systems operating point ! \n time : %d \n Dsoc : %s  \n cost : %s \n ibat: %s \n ubat: %s \n cmt: %s \n wmt: %s';
    ERR=MException('BackwardModel:calc_cout_arc_HPDP_EPI_SURV',chaine,i,num2str(Dsoc),num2str(cout),num2str(ibat),num2str(ubat),num2str(cmt),num2str(wmt));
    [cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=backward_erreur(length(Dsoc));
    return
end

if sum(isnan(cout))
    chaine='some edge cost are NaN, to avoid problems in solve graph, prefer to fixe them to Inf \n time : %d' ;
    warning('BackwardModel:calc_cout_arc_HPDP_EPI_SURV',chaine,i);
end

return

