% function [ERR,ham,lambda1,lambda2,cprim2_cpl,isc,ibat,soc,ubat,u0sc,ux,usc]=calc_hamilt_elec_HP_BV_2EMB_SC(ERR,VD,param,isc,csec_cpl,wsec_cpl,dwsec_cpl,lambda1_p,lambda2_p,j,soc_p,u0sc_p,ux_p,calc_min,lambda1_cst,lambda2_cst)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

function [ERR,ham,lambda1,lambda2,cprim2_cpl,isc,ibat,soc,ubat,u0sc,ux,usc]=calc_hamilt_elec_HP_BV_2EMB_SC(ERR,VD,param,isc,csec_cpl,wsec_cpl,dwsec_cpl,lambda1_p,lambda2_p,j,soc_p,u0sc_p,ux_p,calc_min,lambda1_cst,lambda2_cst)

global Ham_hors_limite;
%Ham_hors_limite=1e6;
ERR= [];

pas=param.pas_temps;

wsec_cpl(VD.CYCL.rappvit(j)==0)=0; % Si on est en tout ??lec au point mort le motelec ne tourne pas
csec_cpl(VD.CYCL.rappvit(j)==0)=0;
        
[ERR,cprim2_cpl,wprim2_cpl,dwprim2_cpl]=calc_red(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0);

% Connexion
wacm1=wprim2_cpl;
dwacm1=dwprim2_cpl;
cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;


% Calcul des conditions en amont de la machine electrique
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);


% Si cacm1<cacm1_min on fait de la recup et on vas faire le max possible
% le reste en frein meca, donc on sature cacm1 ?? cacm1_min
cacm1(cacm1<cacm1_min)=cacm1_min;

[Li,Co]=size(cacm1);
wacm1=repmat(wacm1,Li,1);
[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');
qacm1=qacm1';

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);
if ~isempty(ERR)
    soc=NaN;
    ah=NaN;
    [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end

% calcul de la puissance SC apr??s DC/DC (si pr??sent)
[ERR,usc,u0sc,ux]=calc_sc(ERR,VD.SC,0,pas,isc,u0sc_p,ux_p);
psc=usc.*isc;
if isfield(VD,'DCDC_2') && ~isempty(fields(VD.DCDC_2))
    % Calcul en aval des super capacites
    [ERR,psc_res]=calc_dcdc(1,ERR,DCDC_2,usc,psc);
else
    psc_res=psc;
end

% Puissance reseau electrique
pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc(j); 
pbat_res=repmat(pres,length(isc),1)-repmat(psc_res',1,length(cacm1));

% Puissance batterie
if isfield(VD,'DCDC_1') && ~isempty(fields(VD.DCDC_2))
    [ERR,pbat]=calc_dcdc(2,ERR,VD.DCDC_1,0,pbat_res);
else
    pbat=pbat_res;
end

[ERR,ibat,ubat,soc,ah,E,R,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat,soc_p,0,pres,0);;

if strcmp(lambda1_cst,'non') 
    if ~isfield(VD.BATT,'dE_dEn')
        if isfield(VD.BATT,'tbat')
            [bidon,Li_T20]=min(abs(VD.BATT.tbat-20));
        elseif isfield(VD.BATT,'Tbat_table')
            [bidon,Li_T20]=min(abs(VD.BATT.Tbat_table-20));
        end
        Ebatt=(trapz(VD.BATT.dod_ocv,VD.BATT.ocv(Li_T20,:))-cumtrapz(VD.BATT.dod_ocv,VD.BATT.ocv(Li_T20,:)))*3600*VD.BATT.Cahbat_nom/100*VD.BATT.Nblocser*VD.BATT.Nbranchepar;
        OCV=VD.BATT.ocv(1,:)*VD.BATT.Nblocser;
        dE_dEn(2:length(Ebatt)-1)=(OCV(3:end)-OCV(1:end-2)) ./ (Ebatt(3:end)-Ebatt(1:end-2));
        dE_dEn(1)=dE_dEn(2);
        dE_dEn(length(Ebatt))=dE_dEn(length(Ebatt)-1);
        VD.BATT.Enbatt=Ebatt;
        VD.BATT.dE_dEn=dE_dEn;
    end

    if j>1
        DOD=100-soc;
        dE_dEn=0*ones(1,length(DOD));
        %dE_dEn(delta>=0)=interp1(VD.BATT.dod_ocv,VD.BATT.dE_dEn,DOD(delta>=0));
        dE_dEn=interp1(VD.BATT.dod_ocv,VD.BATT.dE_dEn,DOD);
        pbat=ubat.*ibat;
        dQdEnsc=-VD.BATT.RdFarad./(2*R).*  ( 2*E.*dE_dEn - dE_dEn.*sqrt(E.^2-4*pbat.*R) - ((E.^2).*dE_dEn)./sqrt(E.^2-4*pbat.*R) );  % si seul la tension ?? vide d??pend de l'??nergie stock??
        %dQdEnsc=0;;
        %    if strcmp(FP.exist,'oui')
        %         Ensc_min=FP.xmin*(1+FP.delta_min);
        %         Ensc_max=FP.xmax*(1-FP.delta_max);
        %         dfonc=0*ones(1,length(Cme));
        %         dfonc(Ensc < Ensc_min)=2*FP.rmin*((Ensc(Ensc<Ensc_min)-(Ensc_min))/(FP.xmax^2));
        %         dfonc(Ensc > Ensc_max)=2*FP.rmax*((Ensc(Ensc>Ensc_max)-(Ensc_max))/(FP.xmax^2));
        %    else
        %       dfonc=0;
        %    end
        dfonc=0;
        lambda1=(lambda1_p-dfonc)./(1+dQdEnsc*pas);
    else
        lambda1=lambda1_p*ones(size(pbat));
    end

else
    lambda1=lambda1_p;
end

if strcmp(lambda2_cst,'non')
    if j>1
        % Recalcul de lambda2
        pas=param.pas_temps;
        %psc=usc.*isc;
        R=(VD.SC.Rs.*VD.SC.Nblocser/VD.SC.Nbranchepar);
        C=(VD.SC.C./VD.SC.Nblocser*VD.SC.Nbranchepar);
        Rx=(VD.SC.Rx.*VD.SC.Nblocser/VD.SC.Nbranchepar);
        Cx=(VD.SC.Cx./VD.SC.Nblocser*VD.SC.Nbranchepar);
        taux=Rx*Cx;
        %ux=u0sc-usc-R*isc;
        Re=R+Rx*exp(-pas/(taux));
        dP0=-1/(Re*C) * ( 1  -  ux.*(exp(-pas/(taux)))./(2*u0sc)  -  ( u0sc.^2-2*u0sc.*ux.*(exp(-pas/(taux)))-2*Re*psc-(ux.^2)/2*exp(-2*pas/taux) ) ./ sqrt(  u0sc.^2.*((ux.*exp(-pas/taux)-u0sc).^2-4*Re*psc)  ) );
        lambda2=lambda2_p./(1+dP0);
    else
        lambda2=lambda2_p*ones(size(u0sc));
    end
else
    lambda2=lambda2_p*ones(size(u0sc));
end
%%% Calcul conso et hamiltonien

lambda2=repmat(lambda2',1,length(cacm1));
if isfield(param,'Kibat')
    ham=5*param.Kibat*ibat.*ibat+lambda1.*E.*RdF.*ibat+lambda2.*repmat((u0sc.*isc)',1,length(cacm1));
else
    ham=lambda1.*E.*RdF.*ibat+lambda2.*repmat((u0sc.*isc)',1,length(cacm1));
end
%%% Limitations sur le courant batterie si on depasse les limites on met le
%%% Hamiltonien a NaN idem sur le moteur thermique
%%% Les courants SC sont limite dans le programme appelant
%%% La tension SC est limit???e ici
u0sc=repmat(u0sc',1,length(cacm1));


%% Les limites sur ibat et u0sc sont normalment d??j?? g??r??es dans
%% calc_sc et calc_batt.
Ibat_max=interp1(VD.BATT.DoD_Ibat_max,VD.BATT.Ibat_max,100-soc_p);
Ibat_min=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,100-soc_p);
ham(ibat/VD.BATT.Nbranchepar>Ibat_max | ibat/VD.BATT.Nbranchepar<Ibat_min | ...
    u0sc>VD.SC.maxTension*VD.SC.Nblocser | u0sc<VD.SC.minTension*VD.SC.Nblocser)= NaN;
 

if calc_min==1 % Sinon on renvoit les grandeurs vectotielles pour trace hamilt notamment
        [ham,i_min]=min(ham);
        ibat=ibat(i_min);
        ubat=ubat(i_min);
        soc=soc(i_min);
        u0sc=u0sc(i_min);
        ux=ux(i_min);
        usc=usc(i_min);
        isc=isc(i_min);
        if strcmp(lambda1_cst,'non')
            lambda1=lambda1(i_min,j_min);
        end
        lambda2=lambda2(i_min);    
end

return


