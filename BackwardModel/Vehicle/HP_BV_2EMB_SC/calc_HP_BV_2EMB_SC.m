% function [ERR, VD, ResXml]=calc_HP_BV_2EMB_SC(ERR,vehlib,param,VD)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul d'un vehicule electrique muni d'une association
% batterie/super-condensateur dans VEHLIB en mode backward
%
% 26-08-2011 (EV) : version 1

function [ERR, VD, ResXml]=calc_HP_BV_2EMB_SC(ERR,vehlib,param,VD)
global Ham_hors_limite;
Ham_hors_limite=1e6;

% Calcul cinematiques complementaires
% Avec re-echantillonnage eventuel
if ~isfield(param,'pas_temps') | param.pas_temps==0
    param.pas_temps=1; % si param_pas temps n'existe pas ou =0 on reechantillone ? 1s
end
[ERR,VD,dist]=calc_cinemat(ERR,VD,param);
if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Initialisation
soc=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));
E=zeros(size(VD.CYCL.temps));
R=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
pbat=zeros(size(VD.CYCL.temps));


% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,penteFpk,fres,faero,froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Connexion
csec_red=croue;
wsec_red=wroue;
dwsec_red=dwroue;

% Calcul des conditions en amont du reducteur
[ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);
if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Connexion
csec_bv=cprim_red;
wsec_bv=wprim_red;
dwsec_bv=dwprim_red;

% Calcul des conditions en amont de la boite de vitesse
[ERR,cprim_bv,wprim_bv,dwprim_bv]=calc_bv(VD,csec_bv,wsec_bv,dwsec_bv,0);
if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    return;
end

% Connexion
csec_emb1=cprim_bv;
wsec_emb1=wprim_bv;
dwsec_emb1=dwprim_bv;

%% Prise en compte du frein meca quand Croue <0, on consid??re que dans ce
%% cas (cprim1_cpl =0) moteur arr??t?? ou ralentie avec C =0.

[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,0);

% connexion
wsec_cpl=wprim_emb1;
dwsec_cpl=dwprim_emb1;
csec_cpl=cprim_emb1;

[ERR,cprim2_cpl,wprim2_cpl,dwprim2_cpl]=calc_red(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0);

% Connexion
wacm1=wprim2_cpl;
dwacm1=dwprim2_cpl;
cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;

cacm1=max(cacm1,interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,1),wacm1));
if isfield(VD.ECU,'Vit_Cmin')
    cacm1=max(cacm1,interp1(VD.ECU.Vit_Cmin,VD.ECU.Cpl_min,wacm1));
end

% Recalcul de la partie frein meca au niveau de la roue
cprim2_cpl=(cacm1-VD.ACM1.J_mg.*dwacm1)*VD.VEHI.nbacm1;

%[ERR,csec_cpl,wsec_cpl,dwsec_cpl]=calc_red_fw(ERR,VD,cprim2_cpl,wprim2_cpl,dwprim2_cpl,0);
[ERR,~,~,~,~,csec_cpl]=calc_adcpl_fw(ERR,VD.ADCPL,0,0,cprim2_cpl,0,wsec_cpl,dwsec_cpl);

% connexion
wsec_emb1=wsec_cpl;
dwsec_emb1=dwsec_cpl;
csec_emb1=csec_cpl;

wprim_bv=wsec_emb1;
dwprim_bv=dwsec_emb1;
cprim_bv=csec_emb1;

[ERR,csec,wsec,dwsec]=calc_bv_fw(ERR,VD,cprim_bv,wprim_bv,dwprim_bv,0,0);

% Connexion
wprim_red=wsec_bv;
dwprim_red=dwsec_bv;
cprim_red=csec_bv;


%[ERR,csec_red,wsec_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
[ERR,csec_red,wsec_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
cfrein_meca=csec_red-croue;

%% strategie selon les cas (doivent renvoyer des vecteurs cprim2_cpl et
%% elhyb de taille VD.CYCL.temps)
if strcmp(lower(param.optim),'ligne')  
    [ERR,cprim2_cpl,elhyb,isc]=calc_ligne_HP_BV_2EMB_SC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
elseif strcmp(lower(param.optim),'lagrange')
    [ERR,cprim2_cpl,elhyb,isc]=calc_lagrange_HP_BV_2EMB_SC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
elseif strcmp(lower(param.optim),'lagrange_cycle')
    [ERR,cprim2_cpl,elhyb,isc]=calc_lagrange_cycle_HP_BV_2EMB_SC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
elseif strcmp(lower(param.optim),'prog_dyn')
    [ERR,cprim2_cpl,elhyb,dcarb,soc,isc,u0sc,usc,ux,ibat,ubat,R,E,RdF,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,cfrein_meca_pelec_mot]=calc_prog_dyn_HP_BV_2EMB_SC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
end
 
if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2,NaN);
    return;
end

% Calcul conditions en amont de l'embrayage (?? refaire une fois fait le
% choix de elhyb)
[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,elhyb);
if ~isempty(ERR)
ResXml=struct([]); %     [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2,NaN);
    return;
end

% Calcul conditions en amont du coupleur
% connexion

wsec_cpl=wprim_emb1;
dwsec_cpl=dwprim_emb1;
csec_cpl=cprim_emb1;

[ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,cprim2_cpl,0);

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);
if ~isempty(ERR)
ResXml=struct([]); %     [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end

if strcmp(lower(param.optim),'ligne')  | strcmp(lower(param.optim),'lagrange')
    
    % Connexion
    wacm1=wprim2_cpl;
    dwacm1=dwprim2_cpl;
    cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;
    
    wsec_emb2=wprim1_cpl;
    dwsec_emb2=dwprim1_cpl;
    csec_emb2=cprim1_cpl;
    
    
    %% Pb revoir calc_emb cas passage elhyb 0 ?? 1, valeur cmt, wmt si elhyb ==0
    %% etc
    [ERR,cprim_emb2,wprim_emb2,dwprim_emb2,etat_emb]=calc_emb2(VD,csec_emb2,wsec_emb2,dwsec_emb2,elhyb);
    
    wmt=wprim_emb2;
    dwmt=dwprim_emb2;
    cmt=cprim_emb2+VD.MOTH.J_mt.*dwmt;
    
    % Calcul des conditions de fonctionnement du moteur thermique
    
    [ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,elhyb,0);
    
    if ~isempty(ERR)
        disp(ERR.message);
        choix=1; % pour saturer les couples et faire les calculs tout de meme
        if choix==0
ResXml=struct([]); %             [conso100,co2_gkm,co2_inst]=backward_erreur(length(VD.CYCL.temps),2);
            return;
        elseif choix==1
            indice=find(cmt>interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt));
            cmt(indice)=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt(indice));
            [ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,elhyb,0);
        end
    end
    
    % Calcul des conditions en amont de la machine electrique
    [Li,Co]=size(cacm1);
    wacm1=repmat(wacm1,Li,1);
    [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
    if ~isempty(ERR)
ResXml=struct([]); %         [ah,soc]=backward_erreur(length(VD.CYCL.temps));
        return;
    end
    
    
    % Puissance reseau electrique
    pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc;
    % EN tout thermique il faut remettre pres ?? pacc
    pres(elhyb==2)=pacc(elhyb==2);
    qacm1(elhyb==2)=0; % On suppose que les pertes sont toutes contenue dans Cpl_frott
    
    % calcul batteries et SC
    soc(1)=100-VD.INIT.Dod0;
    ah(1)=0;
    ux(1)=0;
    u0sc(1)=VD.SC.Vc0*VD.SC.Nblocser;
    
    [ERR,usc(1)]=calc_sc(ERR,VD.SC,0,param.pas_temps,isc(1),u0sc(1),ux(1));
    
    if isfield(VD,'DCDC_2') && ~isempty(fields(VD.DCDC_2))
        [ERR,psc_res(1)]=calc_dcdc(1,ERR,VD.DCDC_2,usc(1),usc(1)*isc(1));
    else
        psc_res(1)=usc(1)*isc(1);
    end
    
    pbat_res(1)=pres(1)-psc_res(1);
    
    if isfield(VD,'DCDC_1') && ~isempty(fields(VD.DCDC_1))
        [ERR,pbat(1)]=calc_dcdc(2,ERR,VD.DCDC_1,0,pbat_res(1));
    else
        pbat(1)=pbat_res(1);
    end
    
    [ERR,ibat(1),ubat(1),bidon1,bidon2,E(1),R(1),RdF(1)]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat(1),soc(1),ah(1),pres,0);
    
    
    for j=2:length(VD.CYCL.temps)
        [ERR,usc(j),u0sc(j),ux(j)]=calc_sc(ERR,VD.SC,j,param.pas_temps,isc(j),u0sc(j-1),ux(j-1));
        if isfield(VD,'DCDC_2') && ~isempty(fields(VD.DCDC_2))
            [ERR,psc_res(j)]=calc_dcdc(1,ERR,VD.DCDC_2,usc(j),usc(j)*isc(j));
        else
            psc_res(j)=usc(j)*isc(j);
        end
        pbat_res(j)=pres(j)-psc_res(j);
        if isfield(VD,'DCDC_1') && ~isempty(fields(VD.DCDC_1))
            [ERR,pbat(j)]=calc_dcdc(2,ERR,VD.DCDC_1,0,pbat_res(j));
        else
            pbat(j)=pbat_res(j);
        end
        [ERR,ibat(j),ubat(j),soc(j),ah(j),E(j),R(j),RdF(j)]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat(j),soc(j-1),ah(j-1),pres(j),0);
        if ~isempty(ERR)
            fprintf(1,'%s \n ','calc_HP_BV_2EMB_SC recalcul fonctionnemnt VD.BATT SC  ');
            fprintf(1,'%s %.3f %s\n','j : ',j,' ');
            fprintf(1,'%s %.3f %s\n','elhyb(j) : ',elhyb(j),' ');
            fprintf(1,'%s %.3f %s\n','pres(j) :',pres(j),'W');
            fprintf(1,'%s %.3f %s\n','psc(j) :',usc(j)*isc(j),'W');
            fprintf(1,'%s %.3f %s\n','usc(j) :',usc(j),'V');
            fprintf(1,'%s %.3f %s\n','u0sc(j) :',u0sc(j),'V');
            fprintf(1,'%s %.3f %s\n','isc(j) :',isc(j),'A');
            fprintf(1,'%s %.3f %s\n','pbat(j) ;',pbat(j),'W');
            fprintf(1,'%s %.3f %s\n','ibat(j) :',ibat(j),'A');
            fprintf(1,'%s %.3f %s\n','ubat(j) ;',ubat(j),'V');
            fprintf(1,'%s %.3f %s\n','soc(j-1) ;',soc(j-1),'%');
            fprintf(1,'%s %.3f %s\n','csec_cpl(j) ;',csec_cpl(j),'N');
            fprintf(1,'%s %.3f %s\n','cacm1(j) ;',cacm1(j),'N');
            assignin('base','soc',soc)
            assignin('base','isc',isc)
            assignin('base','ibat',ibat)
            assignin('base','u0sc',u0sc)
            assignin('base','ux',ux)
            assignin('base','elhyb',elhyb)
            pause
        end
    end
end
if strcmp(lower(param.optim),'prog_dyn')
    pbat=ibat.*ubat;
    psc=isc.*usc;
    pres=pbat+psc+pacc;
    csec_emb2=cprim1_cpl;
    wsec_emb2=wprim1_cpl;
    cprim_emb2=csec_emb2;
    wprim_emb2=wsec_emb2;
    
end
% Quelques resultats

fprintf(1,'%s %.2f %s\n','La consommation du vehicule : ',trapz(VD.CYCL.temps,dcarb)/trapz(VD.CYCL.temps,VD.CYCL.vitesse)/VD.MOTH.dens_carb*1000*100,'l/100km');
fprintf(1,'%s %.3f %s\n','La variation de SOC sur le cycle est de : ',soc(1)-soc(end),'%');
fprintf(1,'%s %.3f %s\n','La variation u0sc sur le cycle est de : ',u0sc(1)-u0sc(end),'V');

fprintf(1,'%s %.1f %s\n','La distance parcourue est de : ',dist,'m');
fprintf(1,'%s %.1f %s\n','La masse du vehicule est de : ',mean(masse),'kg');

cumcarb=cumtrapz(VD.CYCL.temps,dcarb);
co2_eq=44*cumcarb(length(cumcarb))./(12+VD.MOTH.CO2carb);
CO2_gkm=co2_eq/(dist/1000);
conso100=100.*sum(dcarb.*param.pas_temps)./VD.MOTH.dens_carb./trapz(VD.CYCL.temps,VD.CYCL.vitesse./1000);
co2_inst=44*dcarb./(12+VD.MOTH.CO2carb);

fprintf(1,'%s %.3f %s\n','La consommation du vehicule est de : ',conso100,'l/100 km');
fprintf(1,'%s %.3f %s\n','Les emissions de CO2 du vehicule sont de : ',CO2_gkm,'g/km');

%% Bilan de puissance et d'??nergie

if VD.SC.Rx~=0
    Pcx=ux.*(isc-ux/VD.SC.Rx/VD.SC.Nblocser*VD.SC.Nbranchepar);
    Pjsc=VD.SC.Nblocser*VD.SC.Nbranchepar*VD.SC.Rs*(isc./VD.SC.Nbranchepar).^2+ux.*ux/VD.SC.Rx*VD.SC.Nbranchepar/VD.SC.Nblocser;
else
    Pjsc=VD.SC.Nblocser*VD.SC.Nbranchepar*VD.SC.Rs*(isc./VD.SC.Nbranchepar).^2;
    Pcx=0;
end

Pjbat=R.*ibat.*ibat;

p0sc=u0sc.*isc;
p0bat=E.*ibat;
Pertes_red=abs(cprim_red.*wprim_red-csec_red.*wsec_red);
Pertes_bv=abs(cprim_bv.*wprim_bv-csec_bv.*wsec_bv);
Pertes_cpl=abs(cprim1_cpl.*wprim1_cpl+cprim2_cpl.*wprim2_cpl-csec_cpl.*wsec_cpl);
Pertes_emb1=abs(cprim_emb1.*wprim_emb1-csec_emb1.*wsec_emb1);
%Inertie ??
Proue=croue.*wroue;

% Pertes motelec en mode tout thermiques
Pj_motelec=0*ones(1,length(VD.CYCL.temps));
Pj_motelec(elhyb==2)=-cacm1(elhyb==2).*wacm1(elhyb==2);

fprintf(1,'%s %.1f %s\n','Le courant efficace de la batterie : ',sqrt( trapz(VD.CYCL.temps,ibat.*ibat)/(VD.CYCL.temps(end)-VD.CYCL.temps(1)) ),'A');
fprintf(1,'%s %.1f %s\n','Le courant moyen de la batterie : ',trapz(VD.CYCL.temps,ibat)/(VD.CYCL.temps(end)-VD.CYCL.temps(1)),'A');
fprintf(1,'%s %.1f %s\n','Le courant efficace dans les supercapacites : ',sqrt(trapz(VD.CYCL.temps,isc.*isc)/(VD.CYCL.temps(end)-VD.CYCL.temps(1))),'A');
fprintf(1,'%s %.6f %s\n','Les pertes joules dans les supercapacites : ',trapz(VD.CYCL.temps,Pjsc)/3600,'Wh');
fprintf(1,'%s %.6f %s\n','Les pertes joules dans les batteries : ',trapz(VD.CYCL.temps,Pjbat)/3600,'Wh');
fprintf(1,'%s %.1f %s\n','Les pertes dans les machines electrique : ',VD.VEHI.nbacm1*trapz(VD.CYCL.temps,qacm1)/3600,'Wh');
fprintf(1,'%s %.1f %s\n','L''energie fournie par les ucap : ',trapz(VD.CYCL.temps,p0sc)/3600,'Wh');
fprintf(1,'%s %.1f %s\n','L''energie fournie par les ucap : ',0.5*VD.SC.C*VD.SC.Nbranchepar/VD.SC.Nblocser*(u0sc(1)^2-u0sc(end)^2)/3600,'Wh');
fprintf(1,'%s %.1f %s\n','L''energie fournie par la batterie (source parfaite) : ',trapz(VD.CYCL.temps,p0bat)/3600,'Wh');
fprintf(1,'%s %.1f %s\n','L''energie fournie par la batterie : ',trapz(VD.CYCL.temps,ibat.*ubat)/3600,'Wh');
fprintf(1,'%s %.1f %s\n','Les pertes dans le reducteur : ',trapz(VD.CYCL.temps,Pertes_red)/3600,'Wh');
fprintf(1,'%s %.1f %s\n','L''energie consommee par les accessoires : ',trapz(VD.CYCL.temps,pacc)/3600,'Wh');


E_ech_pos=0;
E_ech_neg=0;
for i=1:length(VD.CYCL.temps)
    if ibat(i)*isc(i)<0;
        if p0bat(i)>0
            E_ech_pos=E_ech_pos+min(p0bat(i),-p0sc(i))*param.pas_temps/3600;
        else
            E_ech_neg=E_ech_neg+min(-p0bat(i),p0sc(i))*param.pas_temps/3600;
        end
    end
end
fprintf(1,'%s %.2f %s\n','L''energie positive echange entre batterie et ucap : ',E_ech_pos,'Wh');
fprintf(1,'%s %.2f %s\n','L''energie negative echange entre batterie et ucap : ',E_ech_neg,'Wh');

pelec_acm1=qacm1+cacm1.*wacm1;
E_FS=0;
E_FS_BATT=0;
E_FS_SC=0;
E_BOOST=0;
for i=1:length(VD.CYCL.temps)
    if elhyb(i)==1 & pelec_acm1(i)<0
        E_FS=E_FS+pelec_acm1(i)*param.pas_temps/3600;
        if p0bat(i)<0 & p0sc(i)<0
            E_FS_BATT=E_FS_BATT+p0bat(i)*param.pas_temps/3600;
        elseif p0bat(i)<0 & p0sc(i)>=0
            E_FS_BATT=E_FS_BATT+(p0bat(i)-p0sc(i))*param.pas_temps/3600;
        end
        if p0sc(i)<0 & p0bat(i)<0
            E_FS_SC=E_FS_SC+p0sc(i)*param.pas_temps/3600;
        elseif p0sc(i)<0 & p0bat(i)>=0
            E_FS_SC=E_FS_SC+(p0sc(i)-p0bat(i))*param.pas_temps/3600;
        end
    end
    if elhyb(i)==1 & pelec_acm1(i)>0
        E_BOOST=E_BOOST+pelec_acm1(i)*param.pas_temps/3600;
    end
end
fprintf(1,'%s %.2f %s\n','L''energie generee par flux serie: ',E_FS,'Wh');
fprintf(1,'%s %.2f %s\n','L''energie rechargee par flux serie dans la batterie: ',E_FS_BATT,'Wh');
fprintf(1,'%s %.2f %s\n','L''energie rechargee par flux serie dans les ucap: ',E_FS_SC,'Wh');
fprintf(1,'%s %.2f %s\n','L''energie fournie en mode boost: ',E_BOOST,'Wh');
fprintf(1,'%s %.2f %s\n','Le temps passe en mode tout elec: ',sum((elhyb==0).*param.pas_temps),'s');
fprintf(1,'%s %.2f %s\n','Le temps passe a l''arret: ',sum((VD.CYCL.vitesse<=0.001).*param.pas_temps),'s');

bilanP=cmt.*wmt+p0bat+p0sc-Pjbat-Pjsc-Pcx-VD.VEHI.nbacm1*qacm1-Pj_motelec-pacc-Pertes_red-Pertes_cpl-Pertes_emb1-Proue-Pertes_bv-VD.ACM1.J_mg.*dwacm1.*wacm1-VD.MOTH.J_mt.*dwmt.*wmt-cfrein_meca.*wroue;

if param.verbose>=2
    figure(10)
    clf
    if ~strcmp(lower(param.optim),'prog_dyn')
        plot(VD.CYCL.temps,bilanP)
    else %pb du point origine
        plot(VD.CYCL.temps(2:end),bilanP(2:end))
    end
    grid
    legend('bilan de puissance')
end

prouet=wroue.*croue;
prouet(prouet<0)=0;
prouer=wroue.*croue;
prouer(prouer>0)=0;
fprintf(1,'%s %.1f %s %s %.1f %s %s %.1f %s\n','L''energie fournie aux roues : ',trapz(VD.CYCL.temps,croue.*wroue)/3600,'Wh',...
    'en traction',trapz(VD.CYCL.temps,prouet)/3600,'Wh','en recup',trapz(VD.CYCL.temps,prouer)/3600,'Wh');

%trajectoire optimale
if param.verbose>=2
    figure(20)
    clf
    hold on  
   plotyy(VD.CYCL.temps,soc,VD.CYCL.temps,u0sc)
    grid
    title('trajectoire optimale')
    ylabel('soc en %')
end

if param.verbose>=2
    %trace sollicitation batterie
    Inter=-400:25:500;
    %figure;
    classe_energie(VD,VD.CYCL.temps,(ibat),3,min(ibat),max(ibat),10,0,0,1,{'titre'},1,Inter)
end

% Cycle
tsim=VD.CYCL.temps;
vit=VD.CYCL.vitesse;
acc=VD.CYCL.accel;
vit_dem=vit;
distance=cumtrapz(VD.CYCL.temps,VD.CYCL.vitesse); % distance calculee a partir de la vitesse VD.CYCL.vitesse
pente=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,distance,'linear',0); % reechantillonage pente fonction distance claculee.     

[ResXml]=miseEnForme4VEHLIB(vehlib,'caller');
% Ajout des grandeurs synthétiques dans une nouvelle table de la structure xml
[ResXml]=miseEnForme4GestionEnergie(ResXml);


return;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [nrj]=critere_cvgce(VD,ud,uf)

nrj=0.5*VD.SC.C.*(uf.^2-ud.^2).*VD.SC.Nblocser.*VD.SC.Nbranchepar/3600;

return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,cprim2_cpl,elhyb,isc]=calc_ligne_HP_BV_2EMB_SC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1)

% wc=2*pi*param.fc;
% DT=param.pas_temps;
% al=DT/(DT+1/wc);
        
u0sc=zeros(size(VD.CYCL.temps));
ux=zeros(size(VD.CYCL.temps));
%dod=zeros(size(VD.CYCL.temps));
soc=zeros(size(VD.CYCL.temps));
pdem=(csec_emb1.*wsec_emb1);
elhyb=zeros(size(VD.CYCL.temps));

%dod(1)=VD.INIT.Dod0;
soc(1)=100-VD.INIT.Dod0;
elhyb(1)=0;
isc(1)=0;
ibat(1)=0;
ux(1)=0;
u0sc(1)=VD.SC.Vc0*VD.SC.Nblocser;

temps_elec=0;
temps_hyb=0;
pbat_cible(1)=VD.ACC.Pacc_elec;
pdemande_sc(1)=0;
for j=2:length(VD.CYCL.temps)
    % Choix de elhyb
    dod=100-soc(j-1);
    dod_sc=(1-u0sc(j-1)/(VD.SC.Tension.*VD.SC.Nblocser))*100;
    if ~isfield(param,'pdem_bat')
      pdem_bat=interp1(VD.ECU.DoD,VD.ECU.pdem_bat,dod);
      pdem_sc=0;
      pdem_hyb=interp1(VD.ECU.DoD_demarrage,VD.ECU.P_demarrage,dod);
      parr_hyb=VD.ECU.P_arret_hyb;
    else
      pdem_bat=interp1(param.dod_pdem_bat,param.pdem_bat,dod,'linear',min(param.pdem_bat));
      pdem_sc=interp1(param.dod_pdem_sc,param.pdem_sc,dod_sc);
      pdem_hyb=interp1(param.dod_pdemarrage_hybrid,param.pdemarrage_hybrid,dod);   
      parr_hyb=interp1(param.dod_parret_hybrid,param.parret_hybrid,dod);
    end
    pdemande_sc(j)=pdem_sc;
    
    pdem_mt=pdem(j)-pdem_bat;
    if elhyb(j-1)==0;
        temps_elec=temps_elec+VD.CYCL.temps(j);
        temps_hyb=0;
    elseif elhyb(j-1)==1;
        temps_elec=0;
        temps_hyb=temps_hyb+VD.CYCL.temps(j);
    end
    
    if (dod>VD.ECU.DOD_dec_hyb) | (VD.CYCL.vitesse(j)>VD.ECU.vmaxele) | ((pdem_mt>=pdem_hyb) & (temps_elec>VD.ECU.duree_ele))
        elhyb(j)=1;
    elseif (pdem_mt<VD.ECU.pseuil_decel | pdem_mt<parr_hyb) & (dod<VD.ECU.DOD_arret_hyb) & (VD.CYCL.vitesse(j)<VD.ECU.vmaxele)...
            & ((temps_hyb>VD.ECU.duree_hyb) | pdem_mt<VD.ECU.pseuil_decel)
        elhyb(j)=0;
    else
        elhyb(j)=elhyb(j-1);
    end
    
    if elhyb(j)==0;
        ERR= [];
        wprim_emb1=wsec_emb1(j);
        dwprim_emb1=dwsec_emb1(j);
        cprim_emb1=csec_emb1(j);
        
        if VD.CYCL.rappvit(j)==0
            wprim_emb1=0; % Si on est en tout ??lec au point mort le motelec ne tourne pas
            cprim_emb1=0;
        end
        
        wsec_cpl=wprim_emb1;
        dwsec_cpl=dwprim_emb1;
        csec_cpl=cprim_emb1;
        
        [ERR,cprim2_cpl(j),wprim2_cpl,dwprim2_cpl]=calc_red(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0);
        
        % ********************
        %%% calcul dod et verification du mode tout elec sur cacm1 et ibat
        % si on depasse les limites on impose un mode hybride
        % Connexion
        wacm1=wprim2_cpl;
        dwacm1=dwprim2_cpl;
        cacm1=cprim2_cpl(j)+VD.ACM1.J_mg.*dwacm1;
        
        % Calcul des conditions en amont de la machine electrique
        %%% limitations ME
        cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
        
        % Si cacm1>cacm1_max on ne peut faire du tout elec on passe en hybride
        if cacm1>cacm1_max
            elhyb(j)=1;
        end
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
        
        % Calcul des auxiliaires electriques
        [ERR,pacc]=calc_acc(VD);
            
        % Puissance reseau electrique
        pres(j)=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc(j);
        
        % Strategie gestion VD.BATT+SC fonction pres(j) et pbat(j-1) 
       % pbat_cible(j)=al*pres(j)+(1-al)*pbat_cible(j-1);
        
       if param.strat==0
           psc_cible(j)=0;
           pbat_cible(j)=pres(j);
           isc_cible(j)=0;
           isc(j)=0;
           [ERR,ibat(j),ubat,soc(j)]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_cible(j),soc(j-1),0,pres(j),0);
           u0sc(j)=u0sc(j-1);
           usc=u0sc(j);
       elseif param.strat==2
           [ERR,pbat_cible]=filtre_ordre1(ERR,VD,param,pres,pbat_cible,j);
           psc_cible(j)=pres(j)-pbat_cible(j)+pdem_sc;
           if isfield(param,'K_corr')
               %psc_corr(ind)=( param.K_corr* (u0sc(ind-1)-u0sc(1))^3 ) * (param.P_pond_batt/pbat_cible(ind));
               psc_corr(j)=( param.K_corr* (u0sc(j-1)-u0sc(1))^3 ) /pbat_cible(j);
               psc_cible(j)=pres(j)-pbat_cible(j)+psc_corr(j);
           end
           [ERR,isc_cible(j),ibat(j),u0sc,ux,u0bat,soc,usc,ubat] = calc_isc_psc(VD,param,psc_cible,u0sc,j,ux,soc,pres,0*ones(size(VD.CYCL.temps)));
           isc=isc_cible;
       end
        %%% Que doit on faire si ERR sur ibat (tenter en hybride ou boucle
        %%% d??croissance sur isc).
                 
            
        if ~isempty(ERR)
            elhyb(j)=1;
        end
    
        % On verifie que la tension du DCDC est compatible
        ERR=[];
        if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
            [ERR,pbat_res]=calc_dcdc(1,ERR,VD.DCDC_1,ubat,ubat*ibat(j));
        end
        if ~isempty(ERR)
            fprintf(1,'%s %d %s\n','Instant :',j,'Probleme de calcul du DCDC batterie');
            [ah,soc]=backward_erreur(length(VD.CYCL.temps));
            return;
        end     
    end
    
    if elhyb(j)==1; % le moteur fourni pdem + pdem_batt (pertes motelc pas pris en compte).
        % sauf si pmoth<pdem on fait du boost
        % cela reviens a fournir pdem_batt*rend_cpl sur l'arbre du motelec
        %pdem_bat=interp1(VD.ECU.DoD,VD.ECU.pdem_bat,dod(j-1));
        [ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,1);
        %connexion et passage en scalaire
        wsec_cpl=wprim_emb1(j);
        dwsec_cpl=dwprim_emb1(j);
        csec_cpl=cprim_emb1(j);
        
        %calcul des vitesses primaire du coupleur
        [ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0,0);
        
        % Gestion demande cprim2_cpl
        % calcul cmt max
        wmt=wprim1_cpl;
        dwmt=dwprim1_cpl;
        cmt_dem=csec_cpl+VD.MOTH.J_mt.*dwmt-pdem_bat/wprim2_cpl*VD.ADCPL.kred/VD.ADCPL.rend;
        cmt_max=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt);
       
        % Si le demande sur cmt < cmt_max, on fournira en plus pdem bat sur
        % l'arbre motelec, sinon on ne fournira que ce que l'on peut voir
        % on fera du boost
        if cmt_dem<=cmt_max
            cprim2_cpl(j)=pdem_bat/wprim2_cpl;
        else
            pelec_dem=pdem_bat+(cmt_dem-cmt_max)*wmt;
            cprim2_cpl(j)=pelec_dem/wprim2_cpl;
        end
        % ********************
        %%% calcul dod 
        %Connexion
        wacm1=wprim2_cpl;
        dwacm1=dwprim2_cpl;
        cacm1=cprim2_cpl(j)+VD.ACM1.J_mg.*dwacm1;
        
        % Calcul des conditions en amont de la machine electrique
        %%% limitations ME
        cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
        
        % Si cacm1>cacm1_max 
        if cacm1>cacm1_max
            'cacm1>cacm1_max en hybride'
            cacm1=cacm1_max;
            cprim2_cpl(j)= cacm1-VD.ACM1.J_mg.*dwacm1;
            pause
        end
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
        
        % Calcul des auxiliaires electriques
        [ERR,pacc]=calc_acc(VD);
            
        % Puissance reseau electrique
        pres(j)=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc(j);
        
        % Strategie gestion VD.BATT+SC fonction pres(j) et pbat(j-1) 
        
        %pbat_cible(j)=al*pres(j)+(1-al)*pbat_cible(j-1);
       
        if param.strat==0
            psc_cible(j)=0;
            pbat_cible(j)=pres(j);
            isc_cible(j)=0;
            isc(j)=0;
            [ERR,ibat(j),ubat,soc(j)]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_cible(j),soc(j-1),0,pres(j),0);
            ibat(j)=NaN;
            usc=u0sc(j);
        elseif param.strat==2
            [ERR,pbat_cible]=filtre_ordre1(ERR,VD,param,pres,pbat_cible,j);
            psc_cible(j)=pres(j)-pbat_cible(j)+pdem_sc;
            if isfield(param,'K_corr')
                %psc_corr(ind)=( param.K_corr* (u0sc(ind-1)-u0sc(1))^3 ) * (param.P_pond_batt/pbat_cible(ind));
                psc_corr(j)=( param.K_corr* (u0sc(j-1)-u0sc(1))^3 ) /pbat_cible(j);
                psc_cible(j)=pres(j)-pbat_cible(j)+psc_corr(j);
            end
            [ERR,isc_cible(j),ibat(j),u0sc,ux,u0bat,soc,usc,ubat] = calc_isc_psc(VD,param,psc_cible,u0sc,j,ux,soc,pres,0*ones(size(VD.CYCL.temps)));
            isc=isc_cible;
        end
        
        %dod(j)=100-soc;
        if ~isempty(ERR)
            j
            pbat_cible(j)
            psc_cible(j)
            ibat(j)
            ubat
            soc(j)
            'depassement limites batterie en hybride'          
            pause            
        end
          
        % On verifie que la tension du DCDC est compatible
        if isfield(VD,'DCDC_1') && ~isempty(VD.DCDC_1)
            [ERR,pbat_res]=calc_dcdc(1,ERR,VD.DCDC_1,ubat,ubat*ibat(j));
        end
        if ~isempty(ERR)
            fprintf(1,'%s %d %s\n','Instant :',j,'Probleme de calcul du DCDC batterie');
            [ah,soc]=backward_erreur(length(VD.CYCL.temps),vehlib);
            return;
        end        
    end
     if param.verbose>=1
        if rem(j,100)==0 | j==1 | j == length(VD.CYCL.temps)
            fprintf(1,'%s %.1f %s %.2f  %s %.2f %s %.2f \n','calc_HP_BV_2EMB - Indice: ',j,'cprm2',cprim2_cpl(j),...
                'Ibat',ibat(j),'Ubat',ubat./VD.BATT.Nblocser);
        end
    end
end

% figure(101)
% plot(VD.CYCL.temps,ibat,VD.CYCL.temps,isc)
assignin('base','psc_cible',psc_cible);
assignin('base','pres_gestion',pres);
assignin('base','pbat_cible',pbat_cible);
assignin('base','isc_cible',isc_cible);
assignin('base','isc_gestion',isc);
assignin('base','usc_gestion',usc);
assignin('base','u0sc_gestion',u0sc);
assignin('base','elhyb_gestion',elhyb);
assignin('base','pdemande_sc',pdemande_sc);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,cprim2_cpl,elhyb,isc]=calc_lagrange_HP_BV_2EMB_SC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1)
elhyb=ones(1,length(VD.CYCL.temps));
lambda1(1)=param.lambda1;
lambda2(1)=param.lambda2;
soc(1)=100-VD.INIT.Dod0;
u0sc(1)=VD.SC.Vc0.*VD.SC.Nblocser;
ux(1)=0;
lambda1_cst='oui';
lambda2_cst='oui';
global Ham_hors_limite;

for j=1:length(VD.CYCL.temps)
    if j==1
        lambda1_p=lambda1(1);
        lambda2_p=lambda2(1);
        soc_p=soc(1);
        u0sc_p=u0sc(1);
        ux_p=ux(1);
    else
        lambda1_p=lambda1(j-1);
        lambda2_p=lambda2(j-1);
        soc_p=soc(j-1);
        u0sc_p=u0sc(j-1);
        ux_p=ux(j-1);
    end
   
    [ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,elhyb);
    % connexion
    wsec_cpl=wprim_emb1;
    dwsec_cpl=dwprim_emb1;
    csec_cpl=cprim_emb1; 
    
    if  j==param.tdebug
        borne_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,1),wsec_emb1(j)*VD.ADCPL.kred);
        borne_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,1),wsec_emb1(j)*VD.ADCPL.kred);
        cprim2_cpl_trace=borne_min:0.5:borne_max;
        isc_trace=-VD.SC.maxCourant*VD.SC.Nbranchepar:10:VD.SC.maxCourant*VD.SC.Nbranchepar;
        % Tracee des possibles pour l'instant et la valeur de lambda
        [ERR,ham_hyb_trace,~,bidon2,cprim2_cpl_hyb_trace,isc_hyb_trace,ibat_hyb_trace]=...
            calc_hamilt_hyb_HP_BV_2EMB_SC(ERR,VD,param,cprim2_cpl_trace,isc_trace,csec_cpl(j),wsec_cpl(j),dwsec_cpl(j),lambda1_p,lambda2_p,j,soc_p,u0sc_p,ux_p,0,lambda1_cst,lambda2_cst);
        
        if isfield(param,'tout_thermique') & param.tout_thermique==1;
        [ERR,ham_therm_trace,bidon1,bidon2,cprim2_cpl_therm_trace,isc_therm_trace,ibat_therm_trace]=...
            calc_hamilt_therm_HP_BV_2EMB_SC(ERR,VD,param,isc_trace,csec_cpl(j),wsec_cpl(j),dwsec_cpl(j),lambda1_p,lambda2_p,j,soc_p,u0sc_p,ux_p,0,lambda1_cst,lambda2_cst);
        end
        
        [min_ham,Imin_ham]=min(ham_hyb_trace);
        [min_ham,Jmin_ham]=min(min_ham);
        Imin_ham=Imin_ham(Jmin_ham);
        ham_hyb=min_ham
        cprim2_cpl_hyb=cprim2_cpl_hyb_trace(Jmin_ham)
        isc_hyb=isc_hyb_trace(Imin_ham)
        ibat_hyb=ibat_hyb_trace(Imin_ham,Jmin_ham)
        
        if isfield(param,'tout_thermique') & param.tout_thermique==1;
        [min_ham,Imin_ham]=min(ham_therm_trace)
        ham_therm=min_ham
        isc_therm=isc_therm_trace(Imin_ham)
        ibat_therm=ibat_therm_trace(Imin_ham)
        end
        %ham_therm=ham_therm_trace
    end

    borne_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,1),wsec_emb1(j)*VD.ADCPL.kred);
    borne_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,1),wsec_emb1(j)*VD.ADCPL.kred);
    cprim2_cpl_vect=borne_min:0.5:borne_max;
    isc_vect=-VD.SC.maxCourant*VD.SC.Nbranchepar:20:VD.SC.maxCourant*VD.SC.Nbranchepar;
    
    [ERR,ham_hyb(j),lambda1_hyb(j),lambda2_hyb(j),cprim2_cpl_hyb(j),isc_hyb(j),ibat_hyb(j),soc_hyb(j),dcarb_hyb(j),ubat_hyb(j),u0sc_hyb(j),ux_hyb(j),usc_hyb(j)]=...
        calc_hamilt_hyb_HP_BV_2EMB_SC(ERR,VD,param,cprim2_cpl_vect,isc_vect,csec_cpl(j),wsec_cpl(j),dwsec_cpl(j),lambda1_p,lambda2_p,j,soc_p,u0sc_p,ux_p,1,lambda1_cst,lambda2_cst);
   
    if isfield(param,'tout_thermique') & param.tout_thermique==1;
    [ERR,ham_therm(j),lambda1_therm(j),lambda2_therm(j),cprim2_cpl_therm(j),isc_therm(j),ibat_therm(j),soc_therm(j),dcarb_therm(j),ubat_therm(j),u0sc_therm(j),ux_therm(j),usc_therm(j)]=...
        calc_hamilt_therm_HP_BV_2EMB_SC(ERR,VD,param,isc_vect,csec_cpl(j),wsec_cpl(j),dwsec_cpl(j),lambda1_p,lambda2_p,j,soc_p,u0sc_p,ux_p,1,lambda1_cst,lambda2_cst);
    end
    
    
    %[ERR,ham_hyb(j),lambda_hyb(j),cprim2_cpl_hyb(j),ibat_hyb(j),soc_hyb(j),dcarb_hyb(j),ubat_hyb(j)]=VD.solve_hamilt_HP_BV_2EMB(ERR,VD,csec_cpl(j),wsec_cpl(j),dwsec_cpl(j),lambda_p,j,soc_p,1,lambda_cst);
    %[ERR,ham_therm(j),lambda_therm(j),cprim2_cpl_therm(j),ibat_therm(j),soc_therm(j),dcarb_therm(j),ubat_therm(j)]=calc_hamilt_therm_HP_BV_2EMB(ERR,VD,csec_cpl(j),wsec_cpl(j),dwsec_cpl(j),lambda_p,j,soc_p,1,lambda_cst);
  
    
    [ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,0);
   
    % connexion
    wsec_cpl=wprim_emb1;
    dwsec_cpl=dwprim_emb1;
    csec_cpl=cprim_emb1;
    
   
    if  j==param.tdebug
        
        [ERR,ham_elec_trace,bidon1,bidon2,cprim2_elec_trace,isc_elec_trace,ibat_elec_trace]=...
           calc_hamilt_elec_HP_BV_2EMB_SC(ERR,VD,param,isc_trace,csec_cpl(j),wsec_cpl(j),dwsec_cpl(j),lambda1_p,lambda2_p,j,soc_p,u0sc_p,ux_p,0,lambda1_cst,lambda2_cst);
        [min_ham,Imin_ham]=min(ham_elec_trace)
        ham_elec=min_ham
        isc_elec=isc_elec_trace(Imin_ham)
        ibat_elec=ibat_elec_trace(Imin_ham)
        ham_hyb_trace(ham_hyb_trace==Ham_hors_limite)=max(ham_hyb_trace(ham_hyb_trace~=Ham_hors_limite));
        figure(1)
        hold on
        clf
        %hold on
%         for ii=1:1:length(Cme_hyb_vect)
%             for jj=1:1:length(Isc_vect)
%                 if Ham_hyb(ii,jj)==Ham_hors_limite
%                     Ham_hyb(ii,jj)=NaN;
%                 end
%             end
%         end
        surf(isc_trace,cprim2_cpl_trace,ham_hyb_trace');
        hold on
        plot3(isc_trace,cprim2_elec_trace*ones(length(isc_trace),1),ham_elec_trace,'r','LineWidth',2);
        size(isc_trace)
        size(cprim2_cpl_therm)
        if isfield(param,'tout_thermique') & param.tout_thermique==1;
        size(ham_therm_trace)
        plot3(isc_trace,cprim2_cpl_therm_trace*ones(length(isc_trace),1),ham_therm_trace,'g','LineWidth',2);
        end
        %plot(isc_trace,ham_elec_trace,'r*');
        xlabel('courant super capacit??')
        ylabel('couple motelec')
        zlabel('hamiltonien')
        % plot(cprim2_cpl_trace,ham_hyb_trace,cprim2_elec_trace,ham_elec_trace,'r*',cprim2_cpl_trace(Imin_ham),min_ham,'b*',cprim2_therm_trace,ham_therm_trace,'g*')
        %ham_elec=ham_elec_trace
        clear ham_hyb_trace ham_elec_trace ham_elec_trace cprim2_elec_trace cprim2_cpl_hyb_trace bidon bidon2 isc_hyb_trace ibat_hyb_trace
        pause
    end

    [ERR,ham_elec(j),lambda1_elec(j),lambda2_elec(j),cprim2_cpl_elec(j),isc_elec(j),ibat_elec(j),soc_elec(j),ubat_elec(j),u0sc_elec(j),ux_elec(j),usc_elec(j)]=...
       calc_hamilt_elec_HP_BV_2EMB_SC(ERR,VD,param,isc_vect,csec_cpl(j),wsec_cpl(j),dwsec_cpl(j),lambda1_p,lambda2_p,j,soc_p,u0sc_p,ux_p,1,lambda1_cst,lambda2_cst);
 
   if ~isfield(param,'tout_thermique') | param.tout_thermique==0;
       ham_therm(j)=Inf;
   end
       
   if isnan(ham_elec(j))
       ham_elec(j)=Inf;
   end
       
    if ham_hyb(j)<ham_elec(j) & ham_hyb(j)<ham_therm(j)
        ham(j)=ham_hyb(j);
        if j~=1
            lambda1(j)=lambda1_hyb(j);
            lambda2(j)=lambda2_hyb(j);
            soc(j)=soc_hyb(j);
            u0sc(j)=u0sc_hyb(j);
            ux(j)=ux_hyb(j);
        end
        cprim2_cpl(j)=cprim2_cpl_hyb(j);
        usc(j)=usc_hyb(j);
        ibat(j)=ibat_hyb(j);
        isc(j)=isc_hyb(j);
        ubat(j)=ubat_hyb(j);
        dcarb(j)=dcarb_hyb(j);
        elhyb(j)=1;
    elseif ham_elec(j)<ham_hyb(j) & ham_elec(j)<ham_therm(j)
        ham(j)=ham_elec(j);
        if j~=1
            lambda1(j)=lambda1_elec(j);
            lambda2(j)=lambda2_elec(j);
            soc(j)=soc_elec(j);
            u0sc(j)=u0sc_elec(j);
            ux(j)=ux_elec(j);
        end
        cprim2_cpl(j)=cprim2_cpl_elec(j);
        usc(j)=usc_elec(j);
        ibat(j)=ibat_elec(j);
        isc(j)=isc_elec(j);
        ubat(j)=ubat_elec(j);
        dcarb(j)=0;
        elhyb(j)=0;
    else
        ham(j)=ham_therm(j);
        if j~=1
            lambda1(j)=lambda1_therm(j);
            lambda2(j)=lambda2_therm(j);
            soc(j)=soc_therm(j);
            u0sc(j)=u0sc_therm(j);
            ux(j)=ux_therm(j);
        end
        cprim2_cpl(j)=cprim2_cpl_therm(j);
        usc(j)=usc_therm(j);
        ibat(j)=ibat_therm(j);
        isc(j)=isc_therm(j);
        ubat(j)=ubat_therm(j);
        dcarb(j)=dcarb_therm(j);;
        elhyb(j)=2;
    end
%     
%     if ham==Ham_hors_limite
%         chaine=strcat(' ham = Ham_hors_limite, indice',j);
%         ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
%     end
%     
    if param.verbose>=1
        if rem(j,100)==0 | j==1 | j == length(VD.CYCL.temps)
            fprintf(1,'%s %.1f %s %.2f  %s %.2f %s %.2f \n','calc_HP_BV_2EMB - Indice: ',j,'cprm2',cprim2_cpl(j),...
                'Ibat',ibat(j),'Ubat',ubat(j)./VD.BATT.Nblocser);
        end
    end
end
assignin('base','lambda1',lambda1);
assignin('base','lambda2',lambda2);
assignin('base','soc_hyb',soc_hyb)
assignin('base','soc_calcul',soc)
assignin('base','ibat_calc',ibat)
assignin('base','ubat_calc',ubat)
assignin('base','u0sc_calc',u0sc)
assignin('base','isc_calc',isc)
assignin('base','ux_calc',ux)
assignin('base','usc_calc',usc)
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,cprim2_cpl,elhyb]=calc_lagrange_cycle_HP_BV_2EMB_SC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
 %% calcul avec des matrices et en une seule fois sur tout le cycle
    % Attention aux inerties motelec a remodifier 
    wsec_cpl=wprim_emb1;
    dwsec_cpl=dwprim_emb1;
    csec_cpl=cprim_emb1;
    soc_p=60*ones(size(VD.CYCL.temps));
    lambda_p=2.95;
    cacm1=min(VD.ACM1.Cmin_mot):1:max(VD.ACM1.Cmax_mot);
    %cacm1=[-10 -10];
    [ERR,ham_hyb,lambda_hyb,cacm1_hyb,ibat_hyb,soc_hyb,dcarb,ubat_hyb]=calc_hamilt_hyb_HP_BV_2EMB(ERR,param,cacm1,csec_cpl,wsec_cpl,dwsec_cpl,lambda_p,1,soc_p,0,'oui');
    [ERR,ham_elec,lambda_elec,cacm1_elec,ibat_elec,soc_elec]=calc_hamilt_elec_HP_BV_2EMB(ERR,VD,csec_cpl,wsec_cpl,dwsec_cpl,lambda_p,1,soc_p,0,'oui');


    [Li,Co]=size(ham_hyb);
    [ham_hyb,Imin_ham_hyb]=min(ham_hyb);

    for j=1:1:Co
        ibat_hyb_min(j)=ibat_hyb(Imin_ham_hyb(j),j);
    end
    ibat=ibat_hyb_min;


    ind_min=find(ham_elec==min(ham_hyb,ham_elec));
    ham=ham_hyb;
    ham(ind_min)=ham_elec(ind_min);

    cacm1=cacm1(Imin_ham_hyb);
    cacm1(ind_min)=cacm1_elec(ind_min);
    ibat(ind_min)=ibat_elec(ind_min);

    % ici il fuat recalculer le soc puisque le calcul ?? ??t?? fait en supposant
    % un soc constant

    figure(1)
    clf
    hold on
    plot(VD.CYCL.temps,ham_hyb)

    figure(2)
    clf
    hold on
    ham_elec(ham_elec==1e6)=0;
    plot(VD.CYCL.temps,ham_elec)

    figure(3)
    clf
    hold on
    plot(VD.CYCL.temps,ham)

    figure(4)
    clf
    hold on
    plot(VD.CYCL.temps,cacm1_hyb(Imin_ham_hyb))

    figure(5)
    clf
    hold on
    plot(VD.CYCL.temps,cacm1_elec)

    figure(6)
    clf
    hold on
    plot(VD.CYCL.temps,ibat)


    clear ham_hyb ham_elec ham cacm1_hyb cacm1_elec ibat_hyb ibat_elec ubat_hyb dcarb_hyb
return


function [ERR,cprim2_cpl,elhyb,dcarb,soc,isc,u0sc,usc,ux,ibat,ubat,R,E,RdF,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,cfrein_meca_pelec_mot]=calc_prog_dyn_HP_BV_2EMB_SC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
[ERR,VD.ACM1]=inverse_carto_melec(VD.ACM1,0);

assignin('base','VD.ACM1')
[ERR,ibat_max,ibat_min,elec_pos,pres_elec,soc_min,soc_max,Dsoc_min,Dsoc_max,u0sc_max,u0sc_min]=calc_limites_HP_BV_2EMB_SC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);


[ ERR,lim_inf_soc,lim_sup_soc,lim_elec_soc,soc_inf,soc_sup,Dsoc_min,Dsoc_max] =limite2indice_soc(VD,soc_min,soc_max,param,1);
[ lim_inf_u0sc,lim_sup_u0sc] = limite2indice_u0sc(VD,u0sc_min,u0sc_max,param);

if ~isempty(ERR)
    chaine='error in limite2indice_soc calcul' ;
    ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
    [cprim2_cpl,elhyb,ibat,soc_r,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1]=backward_erreur(length(VD.CYCL.temps),0,0);
    return
end


lim=[lim_sup_soc; lim_inf_soc; lim_sup_u0sc;lim_inf_u0sc];

% NB ler var com pourrais ?? priori ??tre faite sur les min max courant comme
% pour les UC

% var_com_y_sup(1)=0;
% var_com_y_sup(2:length(VD.CYCL.temps))=-diff(lim_sup_soc)*param.prec_graphe_y;
% var_com_y_inf(1)=0;
% var_com_y_inf(2:length(VD.CYCL.temps))=-diff(lim_inf_soc)*param.prec_graphe_y;

var_com_y_sup=Dsoc_max;
var_com_y_inf=Dsoc_min;

% temporaire a remplacer par les lignes du dessus (pas sur)
var_com_y_sup=max(Dsoc_max)*ones(size(VD.CYCL.temps));
var_com_y_inf=min(Dsoc_min)*ones(size(VD.CYCL.temps));

% pour test graphe
% var_com_y_inf=-0.3*ones(size(VD.CYCL.temps));
% var_com_y_sup=0.3*ones(size(VD.CYCL.temps));


var_com_z_inf=VD.SC.maxCourant*VD.SC.Nbranchepar*ones(size(VD.CYCL.temps));
var_com_z_sup=-VD.SC.maxCourant*VD.SC.Nbranchepar*ones(size(VD.CYCL.temps));

if param.verbose>=1
    Nb_arc_soc=ceil((Dsoc_min-Dsoc_max)/param.prec_graphe_y);
    Nb_arc_u0sc=ceil((var_com_z_inf-var_com_z_sup)/param.prec_graphe_z);
    fprintf(1,'%s%d\n','edges on the first spread in SoC: ',Nb_arc_soc(2));
    fprintf(1,'%s%d\n','edges on the first spread in u0sc: ',Nb_arc_u0sc(2));
    fprintf(1,'%s%d\n','maximum edge number in SoC : ',max(Nb_arc_soc));
    fprintf(1,'%s%d\n','maximum edge number in u0sc : ',max(Nb_arc_u0sc));
    fprintf(1,'%s%2.1f\n','mean edge number in SoC: ',mean(Nb_arc_soc(2:end-1)));
    fprintf(1,'%s%2.1f\n','mean edge number in u0sc : ',mean(Nb_arc_u0sc(2:end-1)));
    fprintf(1,'%s%d\n','edges on the latest spreads in SoC: ',Nb_arc_soc(end-1));
    fprintf(1,'%s%d\n','edges on the latest spreads in u0sc: ',Nb_arc_u0sc(end-1));
    fprintf(1,'%s%d\n','X size of the graph : ',length(VD.CYCL.temps));
    fprintf(1,'%s%d\n','Y size of the graph : ',max(lim_sup_soc-lim_inf_soc));
    fprintf(1,'%s%d\n','Z size of the graph : ',max(lim_sup_u0sc-lim_inf_u0sc));
    fprintf(1,'%s%d\t%d\t%d\n','Maximum size of the cost table : ',max(lim_sup_u0sc-lim_inf_u0sc),max(Nb_arc_soc),max(Nb_arc_u0sc));
    %% rajouter ici les delta de puissance correspondant a la precision du graphe
   %  pour comparaison entre batterie et uC et ou nb arcs (d??ja dans cout
   %  table)
end


[ERR,poids_min,meilleur_chemin]=...
      solve_graph_3D(ERR, @(ERR,ii,Dsoc,isc,u0sc_p,ux_p) calc_cout_arc_HP_BV_2EMB_SC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,isc,100-VD.INIT.Dod0,0,0,lim,ii),...
param,lim,var_com_y_inf,var_com_y_sup,var_com_z_inf,var_com_z_sup)
% 
[ERR,Dsoc,isc,elhyb]=...
chemin2varcom2EMB_SC(ERR,@(ERR,ii,Dsoc,isc,u0sc_p,ux_p) calc_cout_arc_HP_BV_2EMB_SC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,isc,100-VD.INIT.Dod0,0,0,lim,ii),...
param,lim,var_com_y_inf,var_com_y_sup,var_com_z_inf,var_com_z_sup,meilleur_chemin)

u0sc(1)=VD.SC.Vc0*VD.SC.Nblocser;
ux(1)=0;
for i=2:1:length(VD.CYCL.temps)
[ERR,dcarb(i),isc(i),u0sc(i),usc(i),ux(i),ibat(i),ubat(i),R(i),E(i),RdF(i),cprim2_cpl(i),cmt(i),wmt(i),dwmt(i),cacm1(i),wacm1(i),dwacm1(i),qacm1(i),cprim1_cpl(i),cfrein_meca_pelec_mot(i)]=recalcul_HP_BV_2EMB_SC(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc(i),isc(i),100-VD.INIT.Dod0,u0sc(i-1),ux(i-1),i,elhyb(i))
end

soc(1)=100-VD.INIT.Dod0;
for i=2:1:length(Dsoc)
    soc(i)=soc(i-1)+Dsoc(i);
end

figure(1)
clf
plot(VD.CYCL.temps,soc_inf,VD.CYCL.temps,soc_sup,VD.CYCL.temps,soc_min,VD.CYCL.temps,soc_max)

figure(3)
    clf
    hold on
    plot(VD.CYCL.temps,lim_inf_soc,'k',VD.CYCL.temps,lim_sup_soc,'k',VD.CYCL.temps,VD.CYCL.vitesse*3.6,'b',VD.CYCL.temps,meilleur_chemin(:,1),'r')
    legend('lim inf','lim sup','lim elec','vitesse')
    title('meilleur chemin soc en indice')
    grid
    
figure(4)
    clf
    hold on
    plot(VD.CYCL.temps,lim_inf_u0sc,VD.CYCL.temps,lim_sup_u0sc,VD.CYCL.temps,meilleur_chemin(:,2))
    legend('lim inf','lim sup')
    title('meilleur chemin SC en indice')
    grid

    assignin('base','meilleur_chemin',meilleur_chemin);
    
    assignin('base','lim_sup_soc',lim_sup_soc);
    assignin('base','lim_inf_soc',lim_inf_soc);
    assignin('base','lim_elec_soc',lim_elec_soc);
    assignin('base','var_com_y_inf',var_com_y_inf);
    assignin('base','var_com_y_sup',var_com_y_sup);
    
    assignin('base','lim_sup_u0sc',lim_sup_u0sc);
    assignin('base','lim_inf_u0sc',lim_inf_u0sc);
    assignin('base','var_com_z_inf',var_com_z_inf);
    assignin('base','var_com_z_sup',var_com_z_sup);
    
    
cprim2_cpl=0;

return
