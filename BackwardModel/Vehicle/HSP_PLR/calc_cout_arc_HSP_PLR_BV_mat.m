% function [ERR,cout,ind_rap_opt,r]=calc_cout_arc_HSP_BV_2EMB_CPL_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,soc_p,recalcul,elhyb,r_elec)
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Simulation architecture HSP BV 2EMB avec coupleur 
% sur les machines et reducteur sur le MOTH.
% La place de la BV par rapport au coupleur de la machine 1 (amont ou aval)
% est parametre (param.bv_acm1). POur inhiber les coupleurs reducteur et BV
% on peut mettre les rapports de reduction et les rendements a 1
% interface entree :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discretisation etc)
% VD:           structure de description vehicule et composants
% cprim_bv :    Couple sur le primaire de la BV (vecteur sur le cycle)
% wprim_bv :    vitesse sur le primaire de la BV  (vecteur sur le cycle)
% dprim_bv :    derivee de la vitesse sur le primaire de la BV (vecteur sur le cycle)
% Dsoc :        variation de soc sur l'eventail des arcs possible
% elec_pos :    mode electrique possible (vecteur sur le cycle)
% soc_p :       soc a l'instant precedent
% i :           instant dans le cycle
% recalcul :    si on est en phase de recalcul (grahe deja resolu) recalcul =1 sinon 0 ou pas passer dans l'interface
% elhyb :       mode elec/hybride/thermique en phase recalcul uniquement
% r_elec:       en recalcul on utilise les valeurs calcule en elec
%
% interface de sortie :
% cout :        cout des arcs de l'eventail
% r    :        structures contenant les variables vehicule utile uniquement en recalcul sinon on ne renvoie que le cout
%
% 10-05-16(EV): Creation 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ERR,cout,ind_rap_opt,r]=calc_cout_arc_HSP_PLR_BV_mat(ERR,param,VD,croue,wroue,dwroue,Dsoc,elec_pos,soc_p,recalcul,elhyb,r_elec)

r_var={'ibat','ubat','Rbat','u0bat','RdF','dcarb','cmt','wmt','dwmt','cacm1','wacm1','dwacm1','qacm1',...
    'cacm2','wacm2','dwacm2','qacm2','Dsoc','elhyb','cfrott_acm1','cfrott_acm2','cfrein_meca_pelec_mot',...
    'csec_cpl1','wsec_cpl1','dwsec_cpl1','cprim1_cpl1','wprim1_cpl1','dwprim1_cpl1','cprim2_cpl1','wprim2_cpl1','dwprim2_cpl1',...
    'cprim_red3','wprim_red3','dwprim_red3','csec_red3','wsec_red3','dwsec_red3',...
    'csec_cpl2','wsec_cpl2','dwsec_cpl2','cprim1_cpl2','wprim1_cpl2','dwprim1_cpl2','cprim2_cpl2','wprim2_cpl2','dwprim2_cpl2',...
    'cprim_red','wprim_red','dwprim_red'};

if nargin==9
    recalcul=0;
end

ind_rap_opt=-1*ones(size(Dsoc));

[Li,Co]=size(soc_p);
[sx,sc]=size(Dsoc);

if sum(sum(isnan(soc_p)))==Li*Co
    ERR=MException('BackwardModel:calc_cout_arc_HSP_2EMB', 'appel a calc_cout_arc_HSP_2EMB : tout les elements d''un des vecteur precedent a NaN');
    [~,r]=affect_struct(r_var);
    %[cout,r]=backward_erreur(size(Dsoc));
    return
end

% calcul batterie
% calcul ibat a partir de Dsoc
RdF=ones(size(Dsoc));
RdF(Dsoc>0)=VD.BATT.RdFarad;

ibat=-Dsoc./RdF*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100/param.pas_temps;

[ERR,ubat,~,~,E,R]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat,soc_p,0);

pbat=ubat.*ibat;

if (isfield(param,'fit_pertes') & param.fit_pertes==1)   
elseif ~isfield(param,'fit_pertes') | param.fit_pertes~=1
  
    % calcul du cout des arcs dans le mode hybride serie
    VD_HS=VD;
    VD_HS.ADCPL2.kred=1;
    VD_HS.ADCPL2.rend=1;
    VD_HS.RED3.kred=1;
    VD_HS.RED3.rend=1;
    VD_HS.RED=VD.RED2;
    nrap_hs=length(VD_HS.ADCPL1_R.kred);
    [dcarb_hs]=declar_var(NaN,[sx sc nrap_hs]);
    for ii=1:nrap_hs
        VD_HS.ADCPL1.rapport=VD.ADCPL1_R.rapport(ii);
        VD_HS.ADCPL1.kred=VD.ADCPL1_R.kred(ii);
        VD_HS.ADCPL1.rend=VD.ADCPL1_R.rend(ii);
        VD_HS.ADCPL1.Csec_pert=VD.ADCPL1_R.Csec_pert(ii);
        VD_HS.RED=VD.RED2;
        [ERR,dcarb_hs(:,:,(ii)),r_hs_v(ii)] = calc_cout_arc_HS(ERR,param,VD_HS,croue,wroue,dwroue,pbat);
    end
   [dcarb_hs,r_hs.I_rap_opt]=min(dcarb_hs,[],3);
  
 
   % calcul du cout des arcs dans le cas hybride // 1EM AR
   VD_HP_1EM_AR=VD;
   nrap_hp_1EM_AR=length(VD.ADCPL1_R.kred)*length(VD.ADCPL2_R.kred);
   [dcarb_hp_1EM_AR]=declar_var(NaN,[sx sc nrap_hp_1EM_AR]);
   
   [VD.ADCPL1_vec.rapport VD.RED3_vec.rapport]=ndgrid(VD.ADCPL1_R.rapport, VD.RED3_R.rapport);
   VD.ADCPL1_vec.rapport = reshape(VD.ADCPL1_vec.rapport',[1 nrap_hp_1EM_AR]);
   VD.RED3_vec.rapport = reshape(VD.RED3_vec.rapport',[1 nrap_hp_1EM_AR]);
   
   [VD.ADCPL1_vec.kred VD.RED3_vec.kred]=ndgrid(VD.ADCPL1_R.kred, VD.RED3_R.kred);
   VD.ADCPL1_vec.kred = reshape(VD.ADCPL1_vec.kred',[1 nrap_hp_1EM_AR]);
   VD.RED3_vec.kred = reshape(VD.RED3_vec.kred',[1 nrap_hp_1EM_AR]);
   
   [VD.ADCPL1_vec.rend VD.RED3_vec.rend]=ndgrid(VD.ADCPL1_R.rend, VD.RED3_R.rend);
   VD.ADCPL1_vec.rend = reshape(VD.ADCPL1_vec.rend',[1 nrap_hp_1EM_AR]);
   VD.RED3_vec.rend = reshape(VD.RED3_vec.rend',[1 nrap_hp_1EM_AR]);
   
   [VD.ADCPL1_vec.Csec_pert VD.RED3_vec.Csec_pert]=ndgrid(VD.ADCPL1_R.Csec_pert, VD.RED3_R.Csec_pert);
   VD.ADCPL1_vec.Csec_pert = reshape(VD.ADCPL1_vec.Csec_pert',[1 nrap_hp_1EM_AR]);
   VD.RED3_vec.Csec_pert = reshape(VD.RED3_vec.Csec_pert',[1 nrap_hp_1EM_AR]);
   
   for ii=1:nrap_hp_1EM_AR
       VD_HP_1EM_AR.ADCPL1.rapport=VD.ADCPL1_vec.rapport(ii);
       VD_HP_1EM_AR.ADCPL1.kred=VD.ADCPL1_vec.kred(ii);
       VD_HP_1EM_AR.ADCPL1.rend=VD.ADCPL1_vec.rend(ii);
       VD_HP_1EM_AR.ADCPL1.Csec_pert=VD.ADCPL1_vec.Csec_pert(ii);
       VD_HP_1EM_AR.RED3.rapport=VD.RED3_vec.rapport(ii);
       VD_HP_1EM_AR.RED3.kred=VD.RED3_vec.kred(ii);
       VD_HP_1EM_AR.RED3.rend=VD.RED3_vec.rend(ii);
       VD_HP_1EM_AR.RED3.Csec_pert=VD.RED3_vec.Csec_pert(ii);
       [ERR,dcarb_hp_1EM_AR(:,:,ii),r_hp_1EM_AR_v(ii)] = calc_cout_arc_HP_1EM(ERR,param,VD_HP_1EM_AR,croue,wroue,dwroue,pbat,recalcul);
   end
   [dcarb_hp_1EM_AR,r_hp_1EM_AR.I_rap_opt]=min(dcarb_hp_1EM_AR,[],3);
  
    % calcul du cout des arcs dans le cas hybride // 1EM AV 
    VD_HP_1EM_AV=VD;
    VD_HP_1EM_AV.ACM1=VD.ACM2;
    VD_HP_1EM_AV.RED2=VD.RED;
    nrap_hp_1EM_AV=length(VD.ADCPL2_R.kred);
    [dcarb_hp_1EM_AV]=declar_var(NaN,[sx sc nrap_hp_1EM_AV]);
    
    for ii=1:nrap_hp_1EM_AV
        VD_HP_1EM_AV.ADCPL1.rapport=VD.ADCPL2_R.rapport(ii);
        VD_HP_1EM_AV.ADCPL1.kred=VD.ADCPL2_R.kred(ii);
        VD_HP_1EM_AV.ADCPL1.rend=VD.ADCPL2_R.rend(ii);
        VD_HP_1EM_AV.ADCPL1.Csec_pert=VD.ADCPL2_R.Csec_pert(ii);
        VD_HP_1EM_AV.RED3.rapport=VD.RED3_R.rapport(ii);
        VD_HP_1EM_AV.RED3.kred=VD.RED3_R.kred(ii);
        VD_HP_1EM_AV.RED3.rend=VD.RED3_R.rend(ii);
        VD_HP_1EM_AV.RED3.Csec_pert=VD.RED3_R.Csec_pert(ii);
        [ERR,dcarb_hp_1EM_AV(:,:,ii),r_hp_1EM_AV_v(ii)] = calc_cout_arc_HP_1EM(ERR,param,VD_HP_1EM_AV,croue,wroue,dwroue,pbat,recalcul);
    end
    [dcarb_hp_1EM_AV,r_hp_1EM_AV.I_rap_opt]=min(dcarb_hp_1EM_AV,[],3);

    % calcul du cout des arcs dans le cas hybride // 2EM
    VD_HP_2EM=VD;
    nrap_hp_2EM=length(VD.ADCPL1_R.kred)*length(VD.ADCPL2_R.kred);
    [dcarb_hp_2EM]=declar_var(NaN,[sx sc nrap_hp_2EM]);
%     
    % Déja fait cas au dessus
    % [VD.ADCPL1_vec.rapport VD.RED3_vec.rapport]=ndgrid(VD.ADCPL1_R.rapport, VD.RED3_R.rapport);
    % VD.ADCPL1_vec.rapport = reshape(VD.ADCPL1_vec.rapport',[1 10]);
    % VD.RED3_vec.rapport = reshape(VD.RED3_vec.rapport',[1 10]);
    %
    % [VD.ADCPL1_vec.kred VD.RED3_vec.kred]=ndgrid(VD.ADCPL1_R.kred, VD.RED3_R.kred);
    % VD.ADCPL1_vec.kred = reshape(VD.ADCPL1_vec.kred',[1 10]);
    % VD.RED3_vec.kred = reshape(VD.RED3_vec.kred',[1 10]);
    %
    % [VD.ADCPL1_vec.rend VD.RED3_vec.rend]=ndgrid(VD.ADCPL1_R.rend, VD.RED3_R.rend);
    % VD.ADCPL1_vec.rend = reshape(VD.ADCPL1_vec.rend',[1 10]);
    % VD.RED3_vec.rend = reshape(VD.RED3_vec.rend',[1 10]);
    %
    % [VD.ADCPL1_vec.Csec_pert VD.RED3_vec.Csec_pert]=ndgrid(VD.ADCPL1_R.Csec_pert, VD.RED3_R.Csec_pert);
    % VD.ADCPL1_vec.Csec_pert = reshape(VD.ADCPL1_vec.Csec_pert',[1 10]);
    % VD.RED3_vec.Csec_pert = reshape(VD.RED3_vec.Csec_pert',[1 10]);
    
     VD.ADCPL2_vec=VD.RED3_vec; % dans ce cas on a une seule BV après ACM2
%     
     for ii=1:nrap_hp_2EM
        VD_HP_2EM.ADCPL1.rapport=VD.ADCPL1_vec.rapport(ii);
        VD_HP_2EM.ADCPL1.kred=VD.ADCPL1_vec.kred(ii);
        VD_HP_2EM.ADCPL1.rend=VD.ADCPL1_vec.rend(ii);
        VD_HP_2EM.ADCPL1.Csec_pert=VD.ADCPL1_vec.Csec_pert(ii);
        VD_HP_2EM.ADCPL2.rapport=VD.ADCPL2_vec.rapport(ii);
        VD_HP_2EM.ADCPL2.kred=VD.ADCPL2_vec.kred(ii);
        VD_HP_2EM.ADCPL2.rend=VD.ADCPL2_vec.rend(ii);
        VD_HP_2EM.ADCPL2.Csec_pert=VD.ADCPL2_vec.Csec_pert(ii);
        VD_HP_2EM.RED3.rapport=VD.RED3_vec.rapport(ii);
        VD_HP_2EM.RED3.kred=VD.RED3_vec.kred(ii);
        VD_HP_2EM.RED3.rend=VD.RED3_vec.rend(ii);
        VD_HP_2EM.RED3.Csec_pert=VD.RED3_vec.Csec_pert(ii);
        [ERR,dcarb_hp_2EM(:,:,ii),r_hp_2EM_v(ii)] = calc_cout_arc_HP_2EM(ERR,param,VD_HP_2EM,croue,wroue,dwroue,pbat,recalcul);
    end
     [dcarb_hp_2EM,r_hp_2EM.I_rap_opt]=min(dcarb_hp_2EM,[],3);


    dcarb_hs_hp(:,:,1)=dcarb_hs;
    dcarb_hs_hp(:,:,2)=dcarb_hp_1EM_AR;
    dcarb_hs_hp(:,:,3)=dcarb_hp_1EM_AV;
    dcarb_hs_hp(:,:,4)=dcarb_hp_2EM;
    
    ind_inf=find(sum(isinf(dcarb_hs_hp(2:end,:,1)))==size(dcarb_hs_hp,1) & sum(isinf(dcarb_hs_hp(2:end,:,2)))==size(dcarb_hs_hp,1)...
            & sum(isinf(dcarb_hs_hp(2:end,:,3)))==size(dcarb_hs_hp,1)    & sum(isinf(dcarb_hs_hp(2:end,:,3)))==size(dcarb_hs_hp,4));
    if ~isempty(ind_inf)
         warning('BackwardModel : calc_cout_arc_HSP_BV_CPL ligne 430 : all edges cost equal to Inf in parallele and serial mode; probably the components don''t allow to fullfill the required power');
    end
    [dcarb,r.mode]=min(dcarb_hs_hp,[],3); 
end


% Calcul de la vitesse min possible du moteur thermique pour verifier que le mode tout elec est possible
wmt_min=zeros(size(wroue)); % limitation sur vit max du moth et vit max de la generatrice
wmt_min(VD.CYCL.vitesse>VD.ECU.vmaxele)=VD.MOTH.ral;

% Critere de cout des arcs
if nargin ==9 || nargin==12 && recalcul==0
    % mode tout elec moteur thermique arrete
    dcarb(1,find(elec_pos==1))=0;
    % on est en mode "tout elec" cmt=0 mais le moteur thermique doit tourner a wmt_min
    % a priori on as deja trouver wmt=wmt_min mais on est pas sur d'avoir converge exactement vers cmt=0
    [ERR,dcarb(1,find(elec_pos==0.5))]=calc_mt(ERR,VD.MOTH,zeros(1,sum(elec_pos==0.5)),wmt_min(1,find(elec_pos==0.5)),0,1,0); 
end  

if param.verbose>=3  
    assignin('base','dcarb',dcarb);   
end

cout=dcarb*param.pas_temps;
cout(isnan(cout))=Inf;

if nargin==12 && recalcul==1 % cas recalcul   
    
    % cas hybride et generale (ont pourrais limiter le calcul au cas hybride si besoin
    % cas hs elhyb=1
    % cas hp elhyb =2
    
    elhyb(elhyb==1)=r.mode(elhyb==1);
       
    I_e0=find(elhyb==0);
    I_e05=find(elhyb==0.5);
    %I_e1=find(elhyb==1); % HS
    %I_e2=find(elhyb==2); % HP_1EM
    %I_e3=find(elhyb==3); % HP_2EM

    [tab_res]=declar_var(NaN,[nrap_hs sc]);
    f_names=fieldnames(r_hs_v);
    for ii=1:length(f_names)
        for jj=1:nrap_hs
            tab_res(ii,:)= r_hs_v(jj).(f_names{ii});
        end
        [r_hs.(f_names{ii})]=mat_ind(r_hs.I_rap_opt,2,tab_res);
    end

    [tab_res]=declar_var(NaN,[nrap_hp_1EM_AR sc]);
    f_names=fieldnames(r_hp_1EM_AR_v);
    for ii=1:length(f_names)
        for jj=1:nrap_hp_1EM_AR
            tab_res(ii,:)= r_hp_1EM_AR_v(jj).(f_names{ii});
        end
        [r_hp_1EM_AR.(f_names{ii})]=mat_ind(r_hp_1EM_AR.I_rap_opt,2,tab_res);
    end

    [tab_res]=declar_var(NaN,[nrap_hp_1EM_AV sc]);
    f_names=fieldnames(r_hp_1EM_AV_v);
    for ii=1:length(f_names)
        for jj=1:nrap_hp_1EM_AV
            tab_res(ii,:)= r_hp_1EM_AV_v(jj).(f_names{ii});
        end
        [r_hp_1EM_AV.(f_names{ii})]=mat_ind(r_hp_1EM_AV.I_rap_opt,2,tab_res);
    end

    [tab_res]=declar_var(NaN,[nrap_hp_2EM sc]);
    f_names=fieldnames(r_hp_2EM_v);
    for ii=1:length(f_names)
        for jj=1:nrap_hp_2EM
            tab_res(ii,:)= r_hp_2EM_v(jj).(f_names{ii});
        end
        [r_hp_2EM.(f_names{ii})]=mat_ind(r_hp_2EM.I_rap_opt,2,tab_res);
    end

    
    f_names=fieldnames(r_hp_2EM);
     for ii=1:length(f_names)
%         if ~strcmp(f_names{ii},'ind_acm1_na')
            if ~isfield(r_hs,(f_names{ii})) % si le champ n'existe pas on le met à zeros
                r_hs.(f_names{ii})=zeros*ones(size(croue));
            end
            if ~isfield(r_hp_1EM_AR,(f_names{ii})) % si le champ n'existe pas on le met à zeros
                r_hp_1EM_AR.(f_names{ii})=zeros*ones(size(croue));
            end
            if ~isfield(r_hp_1EM_AV,(f_names{ii})) % si le champ n'existe pas on le met à zeros
                r_hp_1EM_AV.(f_names{ii})=zeros*ones(size(croue));
            end
            
            r.(f_names{ii})=mat_ind(r.mode,2,[r_hs.(f_names{ii}); r_hp_1EM_AR.(f_names{ii}); r_hp_1EM_AV.(f_names{ii}); r_hp_2EM.(f_names{ii})]);
            if isfield(r_elec,(f_names{ii}))
                r.(f_names{ii})([I_e0 I_e05])=r_elec.(f_names{ii})([I_e0 I_e05]);
            end
      
%         else
%             r_elec.(f_names{ii})= r_elec_2EM.(f_names{ii});
%         end
     end
     
    r.cmt(I_e05)=0;
    r.wmt(I_e05)=wmt_min(I_e05);
    r.cmt(I_e0)=0;
    r.wmt(I_e0)=0;
    dcarb(I_e0)=0;
    [ERR,dcarb(I_e05)]=calc_mt(ERR,VD.MOTH,r.cmt(I_e05),r.wmt(I_e05),0,1,0);
    

    r.mode(I_e05)=0.5;
    r.mode(I_e0)=0;
    if param.verbose>=3
        assignin('base','elhyb',elhyb);
        assignin('base','cmt',cmt);
        assignin('base','wmt',wmt);
    end
    
%     cfrott_acm1=zeros(1,sc);
%     cfrott_acm1(I_e2)=r_hp_2EM.cfrott_acm1(I_e2);
%     % en elec il faut aussi remettre cfrott_acm1 a sa valeur
%     ind_elec_acm1_na=find(elhyb(r_elec.ind_acm1_na)==0 | elhyb(r_elec.ind_acm1_na)==0.5); % valeur ou en mode elec acm1 na
%     cfrott_acm1(r_elec.ind_acm1_na(ind_elec_acm1_na))=r_elec.cfrott_acm1(r_elec.ind_acm1_na(ind_elec_acm1_na));
%       
   
%     cfrott_acm2=zeros(1,sc);
%     %cfrott_acm2(I_e2)=r_hp_1EM.cfrott_acm2(I_e2);
%     cfrott_acm2(I_e3)=r_hp_2EM.cfrott_acm2(I_e3);
    
    % recalcul reducteur 3
    cprim_red3=r.cmt-VD.MOTH.J_mt*r.dwmt;
    wprim_red3=r.wmt;
    dwprim_red3=r.dwmt;

% ici il faudrait reconstituer les rapports en fonction des mode et les
% mettres dans VD.RED3
% En attendant on mlet n'importe quoi 
VD.RED3.kred=1;
VD.RED3.rend=1;
VD.ADCPL1.kred=1;
VD.ADCPL1.rend=1;
VD.ADCPL2.kred=1;
VD.ADCPL2.rend=1;
    [ERR,csec_red3,wsec_red3,dwsec_red3]=calc_red_fw(ERR,VD.RED3,cprim_red3,wprim_red3,dwprim_red3,0);
    
    % recalcul coupleur 2 sur acm2     
    cprim2_cpl2=r.cacm2-VD.ACM2.J_mg*r.dwacm2;     cprim1_cpl2=csec_red3;
    wprim2_cpl2=r.wacm2;                           wprim1_cpl2=wsec_red3;
    dwprim2_cpl2=r.dwacm2;                         dwprim1_cpl2=dwsec_red3;
    
    wsec_cpl2=r.wacm2/VD.ADCPL2.kred; 
    dwsec_cpl2=r.dwacm2/VD.ADCPL2.kred;
    
    [ERR,~,~,~,~,csec_cpl2]=calc_adcpl_fw(ERR,VD.ADCPL2,cprim1_cpl2,0,cprim2_cpl2,0,wsec_cpl2,dwsec_cpl2);
    
    % recalcul reducteur
    [ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,croue,wroue,dwroue,0);
    
    % recalcul coupleur 1
    %csec_cpl1=cprim_red; 
    wsec_cpl1=wprim_red;                          dwsec_cpl1=dwprim_red; 
    cprim2_cpl1=r.cacm1-VD.ACM1.J_mg*r.dwacm1;    cprim1_cpl1=csec_cpl2;
    wprim2_cpl1=r.wacm1;                          wprim1_cpl1=wsec_cpl1;
    dwprim2_cpl1=r.dwacm1;                        dwprim1_cpl1=dwsec_cpl1;
    
    [ERR,~,~,~,~,csec_cpl1]=calc_adcpl_fw(ERR,VD.ADCPL1,cprim1_cpl1,0,cprim2_cpl1,0,wsec_cpl1,dwsec_cpl1);
    
    % si on est en recup et que acm1 non alimente
    cfrein_meca_pelec_mot=zeros(1,sc);

    Rbat=R;
    u0bat=E*ones(size(croue));
    
    [ERR,r]=affect_struct(r_var,r);
    
end

ind_inf=find(sum(cout==Inf)==size(cout,1));
if ~isempty(ind_inf)
    chaine=' all edge are impossible, the systems possibilites does not allow to fullfil the required systems operating point ! : %d \n indice colonne inf : %s  \n';
    ERR=MException('BackwardModel:calc_cout_arc_HPDP_EPI_SURV',chaine,num2str(ind_inf));
    [cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=backward_erreur(length(Dsoc));
    [ERR,r]=affect_struct(r_var);
    return
end

if sum(isnan(cout))
    chaine='some edge cost are NaN, to avoid problems in solve graph, prefer to fixe them to Inf \n time : %d' ;
    warning('BackwardModel:calc_cout_arc_HPDP_EPI_SURV',chaine,i);
end

return

