 % function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,r_elec]=calc_limites_HSP_BV_2EMB(ERR,param,VD,cprim_bv,wprim_bv,dwprim_bv);
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul des limites du graphes.
% On ne calcule ici que les soc min et max sans se preocuper
% de la remonte du graphe ou de soc <0 ou >100%.
% La limite min doit etre calcule avec precision (mode tout elec)
% la limite max peut etre calcule "grossierement" d'eventuels arcs impossibles
% dans le graphe seront elimine par la suite
%
% interface entree :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discreetisation etc)
% VD :          structure de description vehicule et composants
% cprim_bv  :   Couple sur le primaire de la BV (vecteur sur le cycle)
% wprim_bv  :   vitesse sur le primaire de la BV(vecteur sur le cycle)
% dwprim_bv :   derivee de la vitesse sur le primaire de la BV (vecteur sur le cycle)
% 
% interface de sortie
% ibat_max :    courant max mode tout elec (en recup c'est le courant de recup en tout elec)
% ibat_min :    courant min recharge max ( en recup courant recup plus recharge par moth)
% elec_pos :    vecteur indiquant si mode elec possible , ou zero si impossible
% soc_min :     soc correspondant a ibat_max
% soc_max :     soc correspondant a ibat_min
% Dsoc_min :    variation soc entre deux instant correspondant a ibat_max
%               (atention a integre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% Dsoc_max :    variation soc entre deux instant correspondant a ibat_min
%               (atention a integre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% r_elec    :   utilisee ensuite en recalcul pour les mode tout elec (evite de refaire les optimisation en recalcul
%
% 06-06-16: Creation 

function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,r_elec]=calc_limites_HSP_PLR_BV(ERR,param,VD,croue,wroue,dwroue);

ERR=[];
ii_max=size(VD.CYCL.temps,2);
soc_min=zeros(1,ii_max);
soc_max=zeros(1,ii_max);
Dsoc_min=zeros(1,ii_max);
Dsoc_max=zeros(1,ii_max);

% recherche du mode tout elec sur train AR
VD_1EM_AR=VD;
[ibat_max_1EM_AR,ind_wacm_max_1EM_AR]=declar_var(NaN,[length(VD_1EM_AR.ADCPL1_R.kred) ii_max]);
for ii=1:length(VD_1EM_AR.ADCPL1_R.kred)
    VD_1EM_AR.ADCPL1.rapport=VD.ADCPL1_R.rapport(ii);
    VD_1EM_AR.ADCPL1.kred=VD.ADCPL1_R.kred(ii);
    VD_1EM_AR.ADCPL1.rend=VD.ADCPL1_R.rend(ii);
    VD_1EM_AR.RED=VD.RED2;
    [ERR,ibat_max_1EM_AR(ii,:),r_elec_1EM_AR_v(ii)]=calc_mode_elec_1EM(ERR,param,VD_1EM_AR,croue,wroue,dwroue,0);
end

% On verfie que l'on as passé les limites sur wacm_max
for jj=1:length(VD.ADCPL1_R.kred)
    ind_wacm_max_1EM_AR(jj,:)=r_elec_1EM_AR_v(jj).ind_wacm_max;
end
ind_melec_1EM_AR_np_wacm=find(sum(ind_wacm_max_1EM_AR)==length(VD.ADCPL1_R.kred));

[ibat_max_1EM_AR,r_elec_1EM_AR.I_rap_opt_1EM_AR]=min(ibat_max_1EM_AR,[],1);
f_names=fieldnames(r_elec_1EM_AR_v);
for ii=1:length(f_names)
    tab_res=[];
    for jj=1:length(VD.ADCPL1_R.kred)
        tab_res=[tab_res; r_elec_1EM_AR_v(jj).(f_names{ii})];
    end
    r_elec_1EM_AR.(f_names{ii})=mat_ind(r_elec_1EM_AR.I_rap_opt_1EM_AR,2,tab_res);
end


% recherche du mode tout elec sur train AV
VD_1EM_AV=VD;
[ibat_max_1EM_AV,ind_wacm_max_1EM_AV]=declar_var(NaN,[length(VD_1EM_AV.ADCPL2_R.kred) ii_max]);
for ii=1:length(VD_1EM_AV.ADCPL2_R.kred)
    VD_1EM_AV.ADCPL1.rapport=VD.ADCPL2_R.rapport(ii);
    VD_1EM_AV.ADCPL1.kred=VD.ADCPL2_R.kred(ii);
    VD_1EM_AV.ADCPL1.rend=VD.ADCPL2_R.rend(ii);
    [ERR,ibat_max_1EM_AV(ii,:),r_elec_1EM_AV_v(ii)]=calc_mode_elec_1EM(ERR,param,VD_1EM_AV,croue,wroue,dwroue,0);
end

% On verfie que l'on as passé les limites sur wacm_max
for jj=1:length(VD.ADCPL1_R.kred)
    ind_wacm_max_1EM_AV(jj,:)=r_elec_1EM_AR_v(jj).ind_wacm_max;
end
ind_melec_1EM_AV_np_wacm=find(sum(ind_wacm_max_1EM_AV)==length(VD.ADCPL1_R.kred));

[ibat_max_1EM_AV,r_elec_1EM_AV.I_rap_opt_1EM_AV]=min(ibat_max_1EM_AV,[],1);
f_names=fieldnames(r_elec_1EM_AV_v);
for ii=1:length(f_names)
    tab_res=[];
    for jj=1:length(VD.ADCPL2_R.kred)
        tab_res=[tab_res; r_elec_1EM_AV_v(jj).(f_names{ii})];
    end
    r_elec_1EM_AV.(f_names{ii})=mat_ind(r_elec_1EM_AV.I_rap_opt_1EM_AV,2,tab_res);
end


% Recherche du mode tout elec sur les deux machines
VD_2EM=VD;
nrap=length(VD.ADCPL1_R.kred)*length(VD.ADCPL2_R.kred);
[ibat_max_2EM,ind_wacm_max_2EM]=declar_var(NaN,[nrap ii_max]);
[VD.ADCPL1_vec.rapport VD.ADCPL2_vec.rapport]=ndgrid(VD.ADCPL1_R.rapport, VD.ADCPL2_R.rapport);
VD.ADCPL1_vec.rapport = reshape(VD.ADCPL1_vec.rapport',[1 nrap]);
VD.ADCPL2_vec.rapport = reshape(VD.ADCPL2_vec.rapport',[1 nrap]);

[VD.ADCPL1_vec.kred VD.ADCPL2_vec.kred]=ndgrid(VD.ADCPL1_R.kred, VD.ADCPL2_R.kred);
VD.ADCPL1_vec.kred = reshape(VD.ADCPL1_vec.kred',[1 nrap]);
VD.ADCPL2_vec.kred = reshape(VD.ADCPL2_vec.kred',[1 nrap]);

[VD.ADCPL1_vec.rend VD.ADCPL2_vec.rend]=ndgrid(VD.ADCPL1_R.rend, VD.ADCPL2_R.rend);
VD.ADCPL1_vec.rend = reshape(VD.ADCPL1_vec.rend',[1 nrap]);
VD.ADCPL2_vec.rend = reshape(VD.ADCPL2_vec.rend',[1 nrap]);

for ii=1:nrap
        VD_2EM.ADCPL1.rapport=VD.ADCPL1_vec.rapport(ii);
        VD_2EM.ADCPL1.kred=VD.ADCPL1_vec.kred(ii);
        VD_2EM.ADCPL1.rend=VD.ADCPL1_vec.rend(ii);
        VD_2EM.ADCPL2.rapport=VD.ADCPL2_vec.rapport(ii);
        VD_2EM.ADCPL2.kred=VD.ADCPL2_vec.kred(ii);
        VD_2EM.ADCPL2.rend=VD.ADCPL2_vec.rend(ii);
        [ERR,ibat_max_2EM(ii,:),r_elec_2EM_v(ii)]=calc_mode_elec_2EM(ERR,param,VD_2EM,croue,wroue,dwroue,0);
end

for jj=1:nrap
    ind_wacm_max_2EM(jj,:)=r_elec_2EM_v(jj).ind_wacm_max;
end
ind_melec_2EM_np_wacm=find(sum(ind_wacm_max_2EM)==nrap);


[ibat_max_2EM,r_elec_2EM.I_rap_opt_2EM]=min(ibat_max_2EM,[],1);

f_names=fieldnames(r_elec_2EM_v);
for ii=1:length(f_names)
    if ~strcmp(f_names{ii},'ind_acm1_na')
        tab_res=[];
        for jj=1:length(VD.ADCPL1_R.kred)
            tab_res=[tab_res; r_elec_2EM_v(jj).(f_names{ii})];
        end
        r_elec_2EM.(f_names{ii})=mat_ind(r_elec_2EM.I_rap_opt_2EM,2,tab_res);
    end
end

% on regarde sur quel point on ne pourras faire du mode elec a cause de wacm
ind_melec_np_wacm=[ind_melec_1EM_AR_np_wacm; ind_melec_1EM_AR_np_wacm; ind_melec_2EM_np_wacm];
ind_melec_np_wacm=sum(ind_melec_np_wacm )==3;

% on choisit le mode qui minimise ibat_max si le mode elec est possible
% Dans le cas cas ou le mode elec n'est possible ni avec 1EM ni avec 2EM
% on choisira le mode qui minimise les pertes donc ibat_max, idem si les
% deux elec_pos=0.5
% ibat_mode=[ibat_max_1EM; ibat_max_2EM];
[ibat_max,I_mode_elec]=min([ibat_max_1EM_AR; ibat_max_1EM_AV; ibat_max_2EM]);

% Cas ou le mode elec est possible seulement dans un cas on garde ce cas
% attention au cas ou les deux modes = 0.5
elec_pos_mode=[r_elec_1EM_AR.elec_pos; r_elec_1EM_AV.elec_pos; r_elec_2EM.elec_pos];
I_elec_1cas_1EM_AR=find(sum(elec_pos_mode)==1 & r_elec_1EM_AR.elec_pos==1);
I_elec_1cas_1EM_AV=find(sum(elec_pos_mode)==1 & r_elec_1EM_AV.elec_pos==1);
I_elec_1cas_2EM=find(sum(elec_pos_mode)==1 & r_elec_2EM.elec_pos==1);
ibat_max(I_elec_1cas_1EM_AR)=ibat_max_1EM_AR(I_elec_1cas_1EM_AR);
ibat_max(I_elec_1cas_1EM_AV)=ibat_max_1EM_AV(I_elec_1cas_1EM_AV);
ibat_max(I_elec_1cas_2EM)=ibat_max_2EM(I_elec_1cas_2EM);
I_mode_elec(I_elec_1cas_1EM_AR)=1;
I_mode_elec(I_elec_1cas_1EM_AV)=2;
I_mode_elec(I_elec_1cas_2EM)=3;

% On reconstruit elec_pos, RdF_elec_1EM, r_elec avec les cas 1EM ou 2EM
[elec_pos,RdF_elec]=mat_ind(I_mode_elec,2,[r_elec_1EM_AR.elec_pos; r_elec_1EM_AV.elec_pos; r_elec_2EM.elec_pos],[r_elec_1EM_AR.RdF_elec; r_elec_1EM_AV.RdF_elec; r_elec_2EM.RdF_elec]);
f_names=fieldnames(r_elec_2EM);

for ii=1:length(f_names)
    if ~strcmp(f_names{ii},'ind_acm1_na')
        if ~isfield(r_elec_1EM_AR,(f_names{ii})) % si le champ n'existe pas on le met à NaN
            r_elec_1EM_AR.(f_names{ii})=NaN*ones(size(croue));
        end
        if ~isfield(r_elec_1EM_AV,(f_names{ii})) % si le champ n'existe pas on le met à NaN
            r_elec_1EM_AV.(f_names{ii})=NaN*ones(size(croue));
        end
        r_elec.(f_names{ii})=mat_ind(I_mode_elec,2,[r_elec_1EM_AR.(f_names{ii}); r_elec_1EM_AV.(f_names{ii}); r_elec_2EM.(f_names{ii})]);
    else
        r_elec.(f_names{ii})= r_elec_2EM.(f_names{ii});
    end
end
r_elec.mode=I_mode_elec;

%% Attention le cas acm1_na est en fait non traité ci dessous, le champ est dans r_elec_2EM_v a faire

 
soc_min(1)=100-VD.INIT.Dod0;
Dsoc_min(1)=0;
for ii=2:length(VD.CYCL.temps)
    soc_min(ii)=soc_min(ii-1)-(RdF_elec(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_min(ii)=-(RdF_elec(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

r_elec.indice_rap_opt_elec=zeros(ii_max);


if param.verbose>=2
    if isfield(VD.CYCL,'vitesse')
    figure(3)
    clf
    hold on
    plot(VD.CYCL.temps,ibat_max,'b',VD.CYCL.temps,VD.CYCL.vitesse,'k')
    legend('ibat max','vitesse')
    title('courant min max fin calc limite')
    grid
    end
    
    figure(4)
    clf
    hold on
    plot(VD.CYCL.temps,soc_min,'b',VD.CYCL.temps,60+Dsoc_min,'g')
    legend('soc min','Dsoc min')
    title('soc min max fin calc limite')
    grid
       
end



%% recherche de la pbat_min (mode hybride)

% Cas mode HS (cas PSA : c'est une BV après ACM2, donc pas de reduction
% entre ICE et ACM2
VD_HS=VD;
VD_HS.ADCPL2.kred=1;
VD_HS.ADCPL2.rend=1;
VD_HS.RED3.kred=1;
VD_HS.RED3.rend=1;
[ibat_max_HS,RdF_hs,ind_hs_np]=declar_var(NaN,[length(VD_HS.ADCPL1_R.kred) ii_max]);
for ii=1:length(VD.ADCPL1_R.kred)
    VD_HS.ADCPL1.rapport=VD.ADCPL1_R.rapport(ii);
    VD_HS.ADCPL1.kred=VD.ADCPL1_R.kred(ii);
    VD_HS.ADCPL1.rend=VD.ADCPL1_R.rend(ii);
    VD_HS.ADCPL1.Csec_pert=VD.ADCPL1_R.Csec_pert(ii);
    VD_HS.RED=VD.RED2;
    [ERR,ibat_min_hs(ii,:),RdF_hs(ii,:),ind_hs_np(ii,:)]=calc_mode_HS(ERR,param,VD_HS,croue,wroue,dwroue);
end

[ibat_min_HS,I_ibat_min_HS]=min(ibat_min_hs,[],1);
[RdF_hs]=mat_ind(I_ibat_min_HS,2,RdF_hs);
ind_hs_np= sum(ind_hs_np,1)==length(VD_HS.ADCPL1_R.kred);

% Cas mode Hybrid parallel sur l'arriere
VD_HP_1EM_AR=VD;
VD_HP_1EM_AR.RED=VD.RED2;
nrap=length(VD.ADCPL1_R.kred)*length(VD.ADCPL2_R.kred);
[ibat_min_HP_1EM_AR,RdF_hp_1EM_AR,ind_hp_1EM_AR_np]=declar_var(NaN,[nrap ii_max]);

[VD.ADCPL1_vec.rapport VD.RED3_vec.rapport]=ndgrid(VD.ADCPL1_R.rapport, VD.RED3_R.rapport);
VD.ADCPL1_vec.rapport = reshape(VD.ADCPL1_vec.rapport',[1 nrap]);
VD.RED3_vec.rapport = reshape(VD.RED3_vec.rapport',[1 nrap]);

[VD.ADCPL1_vec.kred VD.RED3_vec.kred]=ndgrid(VD.ADCPL1_R.kred, VD.RED3_R.kred);
VD.ADCPL1_vec.kred = reshape(VD.ADCPL1_vec.kred',[1 nrap]);
VD.RED3_vec.kred = reshape(VD.RED3_vec.kred',[1 nrap]);

[VD.ADCPL1_vec.rend VD.RED3_vec.rend]=ndgrid(VD.ADCPL1_R.rend, VD.RED3_R.rend);
VD.ADCPL1_vec.rend = reshape(VD.ADCPL1_vec.rend',[1 nrap]);
VD.RED3_vec.rend = reshape(VD.RED3_vec.rend',[1 nrap]);

[VD.ADCPL1_vec.Csec_pert VD.RED3_vec.Csec_pert]=ndgrid(VD.ADCPL1_R.Csec_pert, VD.RED3_R.Csec_pert);
VD.ADCPL1_vec.Csec_pert = reshape(VD.ADCPL1_vec.Csec_pert',[1 nrap]);
VD.RED3_vec.Csec_pert = reshape(VD.RED3_vec.Csec_pert',[1 nrap]);

for ii=1:nrap
    VD_HP_1EM_AR.ADCPL1.rapport=VD.ADCPL1_vec.rapport(ii);
    VD_HP_1EM_AR.ADCPL1.kred=VD.ADCPL1_vec.kred(ii);
    VD_HP_1EM_AR.ADCPL1.rend=VD.ADCPL1_vec.rend(ii);
    VD_HP_1EM_AR.ADCPL1.Csec_pert=VD.ADCPL1_vec.Csec_pert(ii);
    VD_HP_1EM_AR.RED3.rapport=VD.RED3_vec.rapport(ii);
    VD_HP_1EM_AR.RED3.kred=VD.RED3_vec.kred(ii);
    VD_HP_1EM_AR.RED3.rend=VD.RED3_vec.rend(ii);
    VD_HP_1EM_AR.RED3.Csec_pert=VD.RED3_vec.Csec_pert(ii);
    [ERR,ibat_min_hp_1EM_AR(ii,:),RdF_hp_1EM_AR(ii,:),ind_hp_1EM_AR_np(ii,:)]=calc_mode_HP_1EM(ERR,param,VD_HP_1EM_AR,croue,wroue,dwroue);
end

[ibat_min_hp_1EM_AR,I_ibat_min_hp_1EM_AR]=min(ibat_min_hp_1EM_AR,[],1);
[RdF_hp_1EM_AR]=mat_ind(I_ibat_min_hp_1EM_AR,2,RdF_hp_1EM_AR);
ind_hp_1EM_AR_np= sum(ind_hp_1EM_AR_np,1)==nrap;


% Cas mode Hybrid parallel sur l'avant
VD_HP_1EM_AV=VD;
nrap=length(VD.ADCPL2_R.kred);
[ibat_min_HP_1EM_AV,RdF_hp_1EM_AV,ind_hp_np_1EM_AV]=declar_var(NaN,[nrap ii_max]);

for ii=1:nrap
    VD_HP_1EM_AV.ADCPL1.rapport=VD.ADCPL2_R.rapport(ii);
    VD_HP_1EM_AV.ADCPL1.kred=VD.ADCPL2_R.kred(ii);
    VD_HP_1EM_AV.ADCPL1.rend=VD.ADCPL2_R.rend(ii);
    VD_HP_1EM_AV.ADCPL1.Csec_pert=VD.ADCPL2_R.Csec_pert(ii);
    VD_HP_1EM_AV.RED3.rapport=VD.RED3_R.rapport(ii);
    VD_HP_1EM_AV.RED3.kred=VD.RED3_R.kred(ii);
    VD_HP_1EM_AV.RED3.rend=VD.RED3_R.rend(ii);
    VD_HP_1EM_AV.RED3.Csec_pert=VD.RED3_R.Csec_pert(ii);
    [ERR,ibat_min_hp_1EM_AV(ii,:),RdF_hp_1EM_AV(ii,:),ind_hp_1EM_AV_np(ii,:)]=calc_mode_HP_1EM(ERR,param,VD_HP_1EM_AV,croue,wroue,dwroue);
end

[ibat_min_hp_1EM_AV,I_ibat_min_hp_1EM_AV]=min(ibat_min_hp_1EM_AV,[],1);
[RdF_hp_1EM_AV]=mat_ind(I_ibat_min_hp_1EM_AV,2,RdF_hp_1EM_AV);
ind_hp_1EM_AV_np= sum(ind_hp_1EM_AV_np,1)==nrap;


% cas mode hybride parallele sur les deux machines
VD_HP_2EM=VD;
nrap=length(VD.ADCPL1_R.kred)*length(VD.ADCPL2_R.kred);
[ibat_min_HP_2EM,RdF_hp_2EM,ind_hp_2EM_np]=declar_var(NaN,[nrap ii_max]);

% Déja fait cas au dessus
% [VD.ADCPL1_vec.rapport VD.RED3_vec.rapport]=ndgrid(VD.ADCPL1_R.rapport, VD.RED3_R.rapport);
% VD.ADCPL1_vec.rapport = reshape(VD.ADCPL1_vec.rapport',[1 10]);
% VD.RED3_vec.rapport = reshape(VD.RED3_vec.rapport',[1 10]);
% 
% [VD.ADCPL1_vec.kred VD.RED3_vec.kred]=ndgrid(VD.ADCPL1_R.kred, VD.RED3_R.kred);
% VD.ADCPL1_vec.kred = reshape(VD.ADCPL1_vec.kred',[1 10]);
% VD.RED3_vec.kred = reshape(VD.RED3_vec.kred',[1 10]);
% 
% [VD.ADCPL1_vec.rend VD.RED3_vec.rend]=ndgrid(VD.ADCPL1_R.rend, VD.RED3_R.rend);
% VD.ADCPL1_vec.rend = reshape(VD.ADCPL1_vec.rend',[1 10]);
% VD.RED3_vec.rend = reshape(VD.RED3_vec.rend',[1 10]);
% 
% [VD.ADCPL1_vec.Csec_pert VD.RED3_vec.Csec_pert]=ndgrid(VD.ADCPL1_R.Csec_pert, VD.RED3_R.Csec_pert);
% VD.ADCPL1_vec.Csec_pert = reshape(VD.ADCPL1_vec.Csec_pert',[1 10]);
% VD.RED3_vec.Csec_pert = reshape(VD.RED3_vec.Csec_pert',[1 10]);

VD.ADCPL2_vec=VD.RED3_vec; % dans ce cas on a une seule BV après ACM2

for ii=1:nrap
    VD_HP_2EM.ADCPL1.rapport=VD.ADCPL1_vec.rapport(ii);
    VD_HP_2EM.ADCPL1.kred=VD.ADCPL1_vec.kred(ii);
    VD_HP_2EM.ADCPL1.rend=VD.ADCPL1_vec.rend(ii);
    VD_HP_2EM.ADCPL1.Csec_pert=VD.ADCPL1_vec.Csec_pert(ii);
    VD_HP_2EM.ADCPL2.rapport=VD.ADCPL2_vec.rapport(ii);
    VD_HP_2EM.ADCPL2.kred=VD.ADCPL2_vec.kred(ii);
    VD_HP_2EM.ADCPL2.rend=VD.ADCPL2_vec.rend(ii);
    VD_HP_2EM.ADCPL2.Csec_pert=VD.ADCPL2_vec.Csec_pert(ii);
    VD_HP_2EM.RED3.rapport=VD.RED3_vec.rapport(ii);
    VD_HP_2EM.RED3.kred=VD.RED3_vec.kred(ii);
    VD_HP_2EM.RED3.rend=VD.RED3_vec.rend(ii);
    VD_HP_2EM.RED3.Csec_pert=VD.RED3_vec.Csec_pert(ii);
    [ERR,ibat_min_hp_2EM(ii,:),RdF_hp_2EM(ii,:),ind_hp_2EM_np(ii,:)]=calc_mode_HP_2EM(ERR,param,VD_HP_2EM,croue,wroue,dwroue);
end

[ibat_min_hp_2EM,I_ibat_min_hp_2EM]=min(ibat_min_hp_2EM,[],1);
[RdF_hp_2EM]=mat_ind(I_ibat_min_hp_2EM,2,RdF_hp_2EM);
ind_hp_2EM_np=sum(ind_hp_2EM_np,1)==nrap;

% On regarde si il y as des cas ou aucun mode possible
[ind_h_np]=find( sum([ind_hs_np; ind_hp_1EM_AR_np; ind_hp_1EM_AV_np; ind_hp_2EM_np]) == 4 );

% indice ou hybride impossible 
if ~isempty(ind_h_np)
    chaine=' Les performances des machines ne permettent pas de passer le cycle aux instant suivants : \n %s \n';
    ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine,num2str(ind_h_np));
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    return
    %[ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=backward_erreur(length(VD.CYCL.temps),0,0);
end
 
% On choisit le mode qui minimise le courant min 
ibat_min_hs(ind_hs_np)=Inf;
ibat_min_hp_1EM_AR(ind_hp_1EM_AR_np)=Inf;
ibat_min_hp_1EM_AV(ind_hp_1EM_AV_np)=Inf;
ibat_min_hp_2EM(ind_hp_2EM_np)=Inf;

[ibat_min,I_min_hp_hs]=min([ibat_min_hs; ibat_min_hp_1EM_AR; ibat_min_hp_1EM_AV; ibat_min_hp_2EM ]);
[RdF]=mat_ind(I_min_hp_hs,2,[RdF_hs; RdF_hp_1EM_AR; RdF_hp_1EM_AV; RdF_hp_2EM]);

% Si on arrive a ibat_min>ibat_max (peut arriver en recup max si les
% limites sont atteintes) on impose ibat_min au min <=ibat_max
ibat_min(ibat_min>ibat_max)=ibat_max(ibat_min>ibat_max);

soc_max(1)=100-VD.INIT.Dod0;
Dsoc_max(1)=0;

for ii=2:length(VD.CYCL.temps)
    soc_max(ii)=soc_max(ii-1)-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_max(ii)=-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

if sum(ibat_max<ibat_min)>0 ||  sum(soc_max<soc_min)>0 || sum(Dsoc_max<Dsoc_min)>0
    chaine='error in calc_limites_HPDP_EPI_SURV' ;
    ERR=MException('BackwardModel:calc_HPDP_EPI_SURV',chaine);
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    return

end

if param.verbose>=2
   
    figure(3)
    plot(VD.CYCL.temps,ibat_min,'g')
    legend('ibat max','vitesse','ibat min')
    title('courant min max fin calc limite')
    
    figure(4)
    plot(VD.CYCL.temps,soc_max,'k',VD.CYCL.temps,Dsoc_max+60,'r')
    legend('soc min','Dsoc min','soc max','Dsoc max')
    title('soc min max fin calc limite')

end

