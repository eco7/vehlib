 % function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,r_elec]=calc_limites_HSP_BV_2EMB(ERR,param,VD,cprim_bv,wprim_bv,dwprim_bv);
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul des limites du graphes.
% On ne calcule ici que les soc min et max sans se preocuper
% de la remonte du graphe ou de soc <0 ou >100%.
% La limite min doit etre calcule avec precision (mode tout elec)
% la limite max peut etre calcule "grossierement" d'eventuels arcs impossibles
% dans le graphe seront elimine par la suite
%
% interface entree :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discreetisation etc)
% VD :          structure de description vehicule et composants
% cprim_bv  :   Couple sur le primaire de la BV (vecteur sur le cycle)
% wprim_bv  :   vitesse sur le primaire de la BV(vecteur sur le cycle)
% dwprim_bv :   derivee de la vitesse sur le primaire de la BV (vecteur sur le cycle)
% 
% interface de sortie
% ibat_max :    courant max mode tout elec (en recup c'est le courant de recup en tout elec)
% ibat_min :    courant min recharge max ( en recup courant recup plus recharge par moth)
% elec_pos :    vecteur indiquant si mode elec possible , ou zero si impossible
% soc_min :     soc correspondant a ibat_max
% soc_max :     soc correspondant a ibat_min
% Dsoc_min :    variation soc entre deux instant correspondant a ibat_max
%               (atention a integre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% Dsoc_max :    variation soc entre deux instant correspondant a ibat_min
%               (atention a integre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% r_elec    :   utilisee ensuite en recalcul pour les mode tout elec (evite de refaire les optimisation en recalcul
%
% 06-06-16: Creation 

function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,r_elec]=calc_limites_HSP_PLR_2EMB_CPL(ERR,param,VD,croue,wroue,dwroue);

ERR=[];
ii_max=size(VD.CYCL.temps,2);
soc_min=zeros(1,ii_max);
soc_max=zeros(1,ii_max);
Dsoc_min=zeros(1,ii_max);
Dsoc_max=zeros(1,ii_max);


[ERR,ibat_max_1EM,r_elec_1EM]=calc_mode_elec_1EM(ERR,param,VD,croue,wroue,dwroue);

[ERR,ibat_max_2EM,r_elec_2EM]=calc_mode_elec_2EM(ERR,param,VD,croue,wroue,dwroue);

% on choisit le mode qui minimise ibat_max si le mode elec est possible
% Dans le cas cas ou le mode elec n'est possible ni avec 1EM ni avec 2EM
% on choisira le mode qui minimise les pertes donc ibat_max, idem si les
% deux elec_pos=0.5
% ibat_mode=[ibat_max_1EM; ibat_max_2EM];
[ibat_max,I_mode_elec]=min([ibat_max_1EM; ibat_max_2EM]);

% Cas ou le mode elec est possible seulement dans un cas on garde ce cas
% attention au cas ou les deux modes = 0.5
elec_pos_mode=[r_elec_1EM.elec_pos; r_elec_2EM.elec_pos];
I_elec_1cas_1EM=find(sum(elec_pos_mode)==1 & r_elec_1EM.elec_pos~=0.5 & r_elec_1EM.elec_pos==1);
I_elec_1cas_2EM=find(sum(elec_pos_mode)==1 & r_elec_2EM.elec_pos~=0.5 & r_elec_2EM.elec_pos==1);
ibat_max(I_elec_1cas_1EM)=ibat_max_1EM(I_elec_1cas_1EM);
ibat_max(I_elec_1cas_2EM)=ibat_max_2EM(I_elec_1cas_2EM);
I_mode_elec(I_elec_1cas_1EM)=1;
I_mode_elec(I_elec_1cas_2EM)=2;

% On reconstruit elec_pos, RdF_elec_1EM, r_elec avec les cas 1EM ou 2EM
[elec_pos,RdF_elec]=mat_ind(I_mode_elec,2,[elec_pos_mode],[r_elec_1EM.RdF_elec; r_elec_1EM.RdF_elec]);
f_names=fieldnames(r_elec_2EM);

for ii=1:length(f_names)
    if ~strcmp(f_names{ii},'ind_acm1_na')
        if ~isfield(r_elec_1EM,(f_names{ii})) % si le champ n'existe pas on le met à NaN
            r_elec_1EM.(f_names{ii})=NaN*ones(size(croue));
        end
        r_elec.(f_names{ii})=mat_ind(I_mode_elec,2,[r_elec_1EM.(f_names{ii}); r_elec_2EM.(f_names{ii})]);
    else
        r_elec.(f_names{ii})= r_elec_2EM.(f_names{ii});
    end
end
r_elec.mode=I_mode_elec;

 
soc_min(1)=100-VD.INIT.Dod0;
Dsoc_min(1)=0;
for ii=2:length(VD.CYCL.temps)
    soc_min(ii)=soc_min(ii-1)-(RdF_elec(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_min(ii)=-(RdF_elec(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

r_elec.indice_rap_opt_elec=zeros(ii_max);


if param.verbose>=2
    if isfield(VD.CYCL,'vitesse')
    figure(3)
    clf
    hold on
    plot(VD.CYCL.temps,ibat_max,'b',VD.CYCL.temps,VD.CYCL.vitesse,'k')
    legend('ibat max','vitesse')
    title('courant min max fin calc limite')
    grid
    end
    
    figure(4)
    clf
    hold on
    plot(VD.CYCL.temps,soc_min,'b',VD.CYCL.temps,60+Dsoc_min,'g')
    legend('soc min','Dsoc min')
    title('soc min max fin calc limite')
    grid
       
end



%% recherche de la pbat_min (mode hybride)

[ERR,ibat_min_hs,RdF_hs,ind_hs_np]=calc_mode_HS(ERR,param,VD,croue,wroue,dwroue);

[ERR,ibat_min_hp_1EM,RdF_hp_1EM,ind_hp_np_1EM]=calc_mode_HP_1EM(ERR,param,VD,croue,wroue,dwroue);

[ERR,ibat_min_hp_2EM,RdF_hp_2EM,ind_hp_np_2EM]=calc_mode_HP_2EM(ERR,param,VD,croue,wroue,dwroue);

% On regarde si il y as des cas ou aucun mode possible
[ind_h_np]=find( sum([ind_hs_np; ind_hp_np_1EM; ind_hp_np_2EM]) == 3 );

% indice ou hybride impossible 
if ~isempty(ind_h_np)
    chaine=' Les performances des machines ne permettent pas de passer le cycle aux instant suivants : \n %s \n';
    ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine,num2str(ind));
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    return
    %[ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=backward_erreur(length(VD.CYCL.temps),0,0);
end
 
% On choisit le mode qui minimise le courant min 
ibat_min_hs(ind_hs_np)=Inf;
ibat_min_hp_1EM(ind_hp_np_1EM)=Inf;
ibat_min_hp_2EM(ind_hp_np_2EM)=Inf;

[ibat_min,I_min_hp_hs]=min([ibat_min_hs; ibat_min_hp_1EM; ibat_min_hp_2EM ]);
[RdF]=mat_ind(I_min_hp_hs,2,[RdF_hs; RdF_hp_1EM; RdF_hp_2EM]);

% Si on arrive a ibat_min>ibat_max (peut arriver en recup max si les
% limites sont atteintes) on impose ibat_min au min <=ibat_max
ibat_min(ibat_min>ibat_max)=ibat_max(ibat_min>ibat_max);

soc_max(1)=100-VD.INIT.Dod0;
Dsoc_max(1)=0;

for ii=2:length(VD.CYCL.temps)
    soc_max(ii)=soc_max(ii-1)-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_max(ii)=-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

if sum(ibat_max<ibat_min)>0 ||  sum(soc_max<soc_min)>0 || sum(Dsoc_max<Dsoc_min)>0
    chaine='error in calc_limites_HPDP_EPI_SURV' ;
    ERR=MException('BackwardModel:calc_HPDP_EPI_SURV',chaine);
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    return

end

if param.verbose>=2
   
    figure(3)
    plot(VD.CYCL.temps,ibat_min,'g')
    legend('ibat max','vitesse','ibat min')
    title('courant min max fin calc limite')
    
    figure(4)
    plot(VD.CYCL.temps,soc_max,'k',VD.CYCL.temps,Dsoc_max+60,'r')
    legend('soc min','Dsoc min','soc max','Dsoc max')
    title('soc min max fin calc limite')

end

