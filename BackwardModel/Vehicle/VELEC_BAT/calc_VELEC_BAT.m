% function [ERR, VD, ResXml, res]=calc_VELEC_BAT(ERR,vehlib,param,VD)
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul d'un vehicule electrique dans VEHLIB en mode backward
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (BJ-EV) : version 1


function [ERR, VD, ResXml, res]=calc_VELEC_BAT(ERR,vehlib,param,VD,Xml)

res=[];
ResXml=struct([]);

if nargin ==4
    Xml=1;
end
% Calcul cinematiques complementaires
% Avec re-echantillonnage eventuel
if ~isfield(param,'pas_temps') | param.pas_temps==0
    param.pas_temps=1; % si param_pas temps n'existe pas ou =0 on reechantillone ? 1s
end
[ERR,VD,dist]=calc_cinemat(ERR,VD,param);

if ~isempty(ERR)
    return;
end

% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
if ~isempty(ERR)
    return;
end

% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,penteFpk,fres,faero,froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
if ~isempty(ERR)
    return;
end

% Connexion
csec_red=croue;
wsec_red=wroue;
dwsec_red=dwroue;

% Calcul des conditions en amont du reducteur
[ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);
if ~isempty(ERR)
    return;
end

% Connexion
wacm1=wprim_red;
dwacm1=dwprim_red;
cacm1=cprim_red/VD.VEHI.nbacm1+VD.ACM1.J_mg.*dwacm1;
cacm1_sans_inertie=cprim_red/VD.VEHI.nbacm1; % Sert a boucler les flux d'energie comme dans simulink

% Prise en compte des couples min du moteur et impos??? par le calculateur (alors le reste
% est du frein meca)
cacm1=max(cacm1,interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,1),wacm1));
cacm1=max(cacm1,interp1(VD.ECU.Vit_Cmin,VD.ECU.Cpl_min,wacm1));

% Recalcul de la partie frein meca au niveau de la roue
cprim_red=(cacm1-VD.ACM1.J_mg.*dwacm1)*VD.VEHI.nbacm1;
wprim_red=wacm1;
dwprim_red=dwacm1;
[ERR,csec_red,wsec_red,dwsec_red,~,Pertes_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
cfrein_meca=csec_red-croue;

% Calcul des conditions en amont de la machine electrique
% Ici, on ne peut pas s assurer que la Me satisfait les conditions
% dynamiques demandees (on ne connait pas sa tension d'alimentation).

[Li,Co]=size(cacm1);
wacm1=repmat(wacm1,Li,1);
[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
if ~isempty(ERR)
    return;
end
pcacm1=cacm1.*wacm1+qacm1;

[ERR,pacc]=calc_acc(VD);

% Conditions de fonctionnement batterie
pbat=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc;
    
if ~isfield(param,'modular_pack') || param.modular_pack == 0
    soc=zeros(size(pbat));
    ah=zeros(size(pbat));
    ibat=nan(size(pbat));
    ubat=nan(size(pbat));
    u0bat=nan(size(pbat));
    rbat=nan(size(pbat));
    % initialisation
    soc(1)=100-VD.INIT.Dod0;
else
    icell = nan(length(pbat),VD.BATT.Nblocser);
    ucell = nan(length(pbat),VD.BATT.Nblocser);
    u0cell = nan(length(pbat),VD.BATT.Nblocser);
    tcell = nan(length(pbat),VD.BATT.Nblocser);
    rcell = nan(length(pbat),VD.BATT.Nblocser);
    soc_cell = zeros(length(pbat),VD.BATT.Nblocser);
    ah_cell = zeros(length(pbat),VD.BATT.Nblocser);
    qlcal= zeros(length(pbat),VD.BATT.Nblocser);
    qlhot= zeros(length(pbat),VD.BATT.Nblocser);
    qlcold= zeros(length(pbat),VD.BATT.Nblocser);
    soc=zeros(size(pbat));
    ah=zeros(size(pbat));
    ibat=nan(size(pbat));
    ubat=nan(size(pbat));
    u0bat=nan(size(pbat));
    rbat=nan(size(pbat));
    shunt =zeros(VD.BATT.Nblocser); % shunt not active

    % initialisation
    tcell(1,:) = VD.BATT.tcell_current;
    soc_cell(1,:) = VD.BATT.soc_current;
    ah_cell(1,:) = VD.BATT.ah_current;
    if param.cont_aging
        qlcal(1,:) = VD.BATT.qlcal_current;
        qlhot(1,:) = VD.BATT.qlhot_current;
        qlcold(1,:) = VD.BATT.qlcold_current;
    end
    
    soc(1) = mean(soc_cell(1,:));
end

% Calculs batterie
for t=1:length(VD.CYCL.temps)
    if ~isfield(param,'modular_pack') || param.modular_pack == 0
        [ERR,ibat(t),ubat(t),soc(t+1),ah(t+1),u0bat(t),rbat(t)]= ...
            calc_batt(ERR,VD.BATT,param.pas_temps,pbat(t),soc(t),ah(t),-1,0);
    else
        if param.cont_aging
        [ERR, VD.BATT, icell(t,:), ucell(t,:), soc_cell(t+1,:), ah_cell(t+1,:), u0cell(t,:), rcell(t,:), tcell(t+1,:), ...
            qlcal(t+1,:), qlhot(t+1,:), qlcold(t+1,:)] = ...
            calc_cell_bat(ERR, VD.BATT, VD.INIT, param, pbat(t), [], shunt, soc_cell(t,:), ah_cell(t,:), tcell(t,:), ...
            qlcal(t,:), qlhot(t,:), qlcold(t,:));
        else
        [ERR, VD.BATT, icell(t,:), ucell(t,:), soc_cell(t+1,:), ah_cell(t+1,:), u0cell(t,:), rcell(t,:), tcell(t+1,:)] = ...
            calc_cell_bat(ERR, VD.BATT, VD.INIT, param, pbat(t), [], shunt, soc_cell(t,:), ah_cell(t,:), tcell(t,:));

        end
        rbat(t) =sum(rcell(t,:));
        ubat(t) = sum(ucell(t,:));
        u0bat(t) = sum(u0cell(t,:));
        ibat(t) = icell(t,1);
        soc(t+1)=mean(soc_cell(t+1,:));
        ah(t+1) = mean(ah_cell(t+1,:));

    end
    if ~isempty(ERR)
        return;
    end

    uacm1(t)=ubat(t);
    % La machine permet-elle de satisfaire les conditions demandees
    [ERR,wacm1(t),cacm1(t)]=calc_enveloppe_acm(ERR,VD.ACM1,wacm1(t),cacm1(t),uacm1(t));
end
pertes_bat=rbat.*ibat.^2;
iacm1=pcacm1./uacm1;
iacc=pacc./ubat;
uacc=ubat;

% probleme intervalle integrale
soc=soc(1:end-1);
ah=ah(1:end-1);
dod=100-soc;
if isfield(param,'modular_pack') && param.modular_pack == 1
    soc_cell = soc_cell(1:end-1,:);
    ah_cell = ah_cell(1:end-1,:);
    tcell = tcell(1:end-1,:);
    if param.cont_aging
        qlcal = qlcal(1:end-1,:);
        qlhot = qlhot(1:end-1,:);
        qlcold = qlcold(1:end-1,:);
    end
end

Pjbat=rbat.*ibat.*ibat;
p0bat=u0bat.*ibat;

% Quelques resultats
if param.verbose>=0
    fprintf(1,'%s %.3f %s\n','La consommation du vehicule est de : ',ah(end),'Ah');
    fprintf(1,'%s %.3f %s\n','Soit : ',ah(end)/(dist/1000),'Ah/km');
    fprintf(1,'%s %.3f %s\n','La variation de SOC sur le cycle est de : ',soc(1)-soc(end),'%');
    fprintf(1,'%s %.1f %s\n','La distance parcourue est de : ',dist,'m');
    fprintf(1,'%s %.1f %s\n','La masse du vehicule est de : ',mean(masse),'kg');
end

if param.verbose>=1
    fprintf(1,'%s %.6f %s\n','Les pertes joules dans les batteries : ',trapz(VD.CYCL.temps,Pjbat)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','Les pertes dans les machines electrique : ',trapz(VD.CYCL.temps,VD.VEHI.nbacm1*qacm1)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie fournie par la batterie (source parfaite) : ',trapz(VD.CYCL.temps,p0bat)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie fournie par la batterie : ',trapz(VD.CYCL.temps,ibat.*ubat)/3600,'Wh');
    fprintf(1,'%s %.3f %s\n','Soit : ',(trapz(VD.CYCL.temps,ibat.*ubat)/3600/(dist/1000))./1000,'kWh/km');
    fprintf(1,'%s %.2f %s\n','Les pertes dans le reducteur : ',trapz(VD.CYCL.temps,Pertes_red)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie consommee par les accessoires : ',trapz(VD.CYCL.temps,pacc)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie dissipee dans les freins : ',trapz(VD.CYCL.temps,cfrein_meca.*wroue)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie inertie motelec "perdue" : ',trapz(VD.CYCL.temps,VD.ACM1.J_mg.*dwacm1.*wacm1)/3600,'Wh');
end

% Cycle
tsim=VD.CYCL.temps;
vit=VD.CYCL.vitesse;
acc=VD.CYCL.accel;
vit_dem=vit;
distance=VD.CYCL.distance;
pente=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,distance,'linear',0); % reechantillonage pente fonction distance claculee.     
cinertie=Jveh.*VD.CYCL.accel/VD.VEHI.Rpneu;
cahbat=ah;

if Xml==1
    % Construction de la structure xml de resultats
    [ResXml]=miseEnForme4VEHLIB(vehlib,'caller');
    
    % Calcul de resultats synthetiques
    [Res]=resultats(VD,param,ResXml);
    ibat_eff=Res.ibat_eff;
    ibat_moy=Res.ibat_moy;
    
    % Ajout des grandeurs synthétiques dans une nouvelle table de la structure xml
    [ResXml]=miseEnForme4GestionEnergie(ResXml);
    res=[];
else
    ResXml=[];
    %[ResXml]=miseEnForme4VEHLIB(vehlib,'caller');
    %[ERR,res]=affectVehlibStruct2Workspace(ResXml,'caller');
    res.tsim=tsim;
    res.soc=soc;
    res.ibat=ibat;
    res.ubat=ubat;
    res.u0bat=u0bat;
    res.vit=vit;
    res.rbat=rbat;
end
if param.verbose>=1
    trait_velec;
end
if isfield(param,'modular_pack') && param.modular_pack == 1
   % output results
    res.tsim = tsim';
    res.distance = distance';
    res.vit = vit';
    res.ucell = ucell;
    res.icell = icell;
    res.soc_cell = soc_cell;
    res.ah_cell = ah_cell;
    res.u0cell = u0cell;
    res.rcell = rcell;
    res.tcell = tcell;
    if param.cont_aging
        res.qlcal = qlcal;
        res.qlhot = qlhot;
        res.qlcold = qlcold;
    end
end
end
