function [ERR,VD,ResXml]=calc_HP_BV_2EMB_analytique(ERR,vehlib,param,VD)
ResXml=struct([]);

%% Calcul des conditions en amont de la boite de vitesse %%

% Calcul essieu avant jusqu'au reducteur de pont
% la cinematique, les masses, les efforts et couples
% sont renseignes dans les structures VD et res
[ERR,VD,res] = calc_essieu(ERR,vehlib,param,VD);
if ~isempty(ERR), return, end

% Connexion pont / boite de vitesse
res.csec_bv = res.cprim_red;
res.wsec_bv = res.wprim_red;
res.dwsec_bv = res.dwprim_red;

% Calcul des conditions en amont de la boite de vitesse
% NB si on est en rapport libre ou optimaux on as des matrices 
% avec en nb de ligne le nb de rapport de boite plus pm
[ERR,res.cprim_bv,res.wprim_bv,res.dwprim_bv] = ...
    calc_bv(VD,res.csec_bv,res.wsec_bv,res.dwsec_bv,0);
if ~isempty(ERR), return, end

%% Modele de la batterie : Q0, E, R, iMax, iMin (constantes)
VD.BATT = modele_batt_analytique(VD.BATT,param);

%% Modele des pertes de la machine et de l'electroniques : Pertes = c1*wme + c2*Cme^2
VD.ACM1 = modele_acm_analytique(VD.ACM1,param.E_nom);

%% Modele du moteur thermique
[VD.MOTH,ERR] = calc_mt_analytique(VD.MOTH, param);
if ~isempty(ERR), return, end

%% Calcul de la strategie optimale

if strcmpi(param.optim,'analytique')
    [ERR,VD,res] = calc_lagrange_analytique_HP_BV_2EMB(ERR,param,VD,res);
else
    disp('en cours de redaction')
end

%% Post-traitement
[ResXml]=miseEnForme4VEHLIB(vehlib,'caller','res');

% Calcul de resultats synthetiques
[Res]=resultats(VD,param,ResXml);

conso100=Res.conso100;
cumcarb=Res.cumcarb;
co2_eq=Res.co2_eq;
CO2_gkm=Res.CO2_gkm;
ibat_eff=Res.ibat_eff;
ibat_moy=Res.ibat_moy;

% Bilan de puissance et d'energie
[bp]= bilanPE(VD,param,ResXml);
bilanP=bp.bilanP;

% Ajout des grandeurs synthetiques dans une nouvelle table de la structure xml
[ResXml]=miseEnForme4GestionEnergie(ResXml);

end

function BATT = modele_batt_analytique(BATT,param)
%% hyp : les proprietes de la batterie sont independantes du soc
BATT.Q0 = BATT.Nbranchepar*BATT.Cahbat_nom;
BATT.E = BATT.Nblocser*interp1(BATT.dod_ocv,BATT.ocv,param.dodC);
BATT.R = mean([interp1(BATT.dod_ocv,BATT.rc,param.dodC) interp1(BATT.dod_ocv,BATT.rd,param.dodC)])*BATT.Nblocser/BATT.Nbranchepar;
BATT.iMax = interp1(BATT.DoD_Ibat_max,BATT.Ibat_max,param.dodC)*BATT.Nbranchepar;
BATT.iMin = interp1(BATT.DoD_Ibat_min,BATT.Ibat_min,param.dodC)*BATT.Nbranchepar;
end

function ACM = modele_acm_analytique(ACM,E)
%% Pertes = ACM.coeff(1)*wacm + ACM.coeff(2)*cacm.^2 %%
% Modele des pertes de la machine et de l'electronique
% copie des donnees depuis la structure VD
pertes = ACM.Pert_mot;
reg = ACM.Regmot_pert;
cpl = ACM.Cmot_pert;

% construction de matrices de la meme taille que les pertes
matCpl = repmat(cpl', size(reg))';
matReg = repmat(reg', size(cpl));

% passage des matrices en vecteurs (linear indexing)
vecCpl = matCpl(:);
vecReg = matReg(:);
vecPertes = pertes(:);

% exclusion des valeurs > Cmax ou < Cmin (out of bounds)
% On choisit Cmax pour notre tension d'alimentation
ACM.cmax = interp2(ACM.Regmot_cmax, ACM.Tension_cont, ...
    ACM.Cmax_mot', ACM.Regmot_cmax, E);
ACM.cMaxAcm = max(ACM.cmax);
ACM.pMaxAcm = max(ACM.Regmot_cmax.*ACM.cmax);
ACM.wBaseAcm = ACM.pMaxAcm/ACM.cMaxAcm;

% determination du regime de base (transition entre cmax et pmax)
% dans un modele cmax - pmax pour le domaine de fonctionnement
vecCmax = repmat(ACM.cMaxAcm,size(vecCpl));
vecCmax(vecReg>ACM.wBaseAcm) = ACM.pMaxAcm/vecReg(vecReg>ACM.wBaseAcm);
vecCmin = -vecCmax;

vecInOut = vecCpl<=vecCmax & vecCpl>=vecCmin;
iIn = find(vecInOut == 1);

vecCpl = vecCpl(iIn);
vecReg = vecReg(iIn);
vecPertes = vecPertes(iIn);

% determination des coefficients c1 et c2 (moindres carres)
vecAWillans = [vecReg vecCpl.^2];
ACM.coeff = vecAWillans\vecPertes;
end

