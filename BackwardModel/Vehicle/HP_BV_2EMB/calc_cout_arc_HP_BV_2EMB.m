% function [ERR,cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=calc_cout_arc_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,soc_p,i,recalcul,elhyb)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Calcul des cout des arcs cas 2EMB
%
% interface entr??e :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discretisation etc)
% CYLC,VD.ADCPL...:structures de description vehicule et composants
% csec_emb1 :   Couple sur le secondaire de l'embrayage "principal" (vecteur sur le cycle)
% wsec_emb1 :   vitesse sur le secondaire de l'embrayage "principal" (vecteur sur le cycle)
% dwsec_emb1 :  deriv??e de la vitesse sur le secondaire de l'embrayage "principal" (vecteur sur le cycle)
% Dsoc :        variation de soc sur l'eventail des arcs possible
% elec_pos :    mode electrique possible (vecteur sur le cycle)
% soc_p :       soc ?? l'instant precedent
% i :           instant dans le cycle
% recalcul :    si on est en phase de recalcul (grahe deja resolu) recalcul =1 sinon 0 ou pas passer dans l'interface
% elhyb :       mode elec/hybride/thermique en phase recalcul uniquement
%
% interface de sortie
% cout :        cout des arcs de l'??ventail
% Dsoc_tt :     variation de soc correspondant ?? l'arc sur lequel on s'est accroch?? pour le mode tout thermique, ce n'est pas la variation exacte de
%               soc en tou tthermique qui sera elle calcul?? en phase recalcul
% indice_rap_opt : indice correspondant au rapport de boite optimal (-1 en
%                   rapport de boite impos???s)
% ibat,ubat ...:variable vehicule utilie uniquement en recalcul sinon on ne renvoie que le cout
%
% 16-12-10(EV): Creation 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ERR,cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=calc_cout_arc_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,soc_p,i,recalcul,elhyb)

indice_rap_opt=-1*ones(size(Dsoc));

[Li,Co]=size(soc_p);
if sum(sum(isnan(soc_p)))==Li*Co
    ERR=MException('BackwardModel:calc_cout_arc_HP_BV_2EMB', 'appel a calc_cout_arc_HP_BV_2EMB : tout les elements d''un des vecteur precedent a NaN');
    [cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=backward_erreur(size(Dsoc));
    return
end

[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,1);

% connexion

if VD.CYCL.ntypcin==3
    wsec_cpl=wprim_emb1(i);
    dwsec_cpl=dwprim_emb1(i);
    csec_cpl=cprim_emb1(i); 
else
% dans le cas ntypcin==1 (optimisation des rapports), les entrees sont de
% type matricielles (length(cycl.temps) x length(rappvit))
    wsec_cpl=repmat(wprim_emb1(:,i),1,size(Dsoc,2));
    dwsec_cpl=repmat(dwprim_emb1(:,i),1,size(Dsoc,2));
    csec_cpl=repmat(cprim_emb1(:,i),1,size(Dsoc,2));
end

[ERR,~,wacm1,dwacm1]=calc_red(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0);

[ERR,pacc]=calc_acc(VD);

% calcul batterie
% calcul ibat ?? partir de Dsoc
RdF=ones(size(Dsoc));
RdF(Dsoc>0)=VD.BATT.RdFarad;
ibat=-Dsoc./RdF*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100/param.pas_temps;

if VD.CYCL.ntypcin==1
   ibat=repmat(ibat,size(csec_emb1,1),1);
end   

[ERR,ubat]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat,soc_p,0);

pbat=ubat.*ibat;
pacm1=pbat-pacc(i);

% calcul forward motelec
[Li,Co]=size(pacm1);
if VD.CYCL.ntypcin==3
    wacm1=repmat(wacm1,1,Co);
end

[ERR,qacm1]=calc_pertes_acm_fw(ERR,VD.ACM1,pacm1,wacm1,dwacm1);

cacm1=(pacm1-qacm1)./wacm1;
cacm1(isnan(cacm1))=Inf;

% [Li,Co]=size(cacm1);
% if VD.CYCL.ntypcin==3
%     wacm1=repmat(wacm1,Li,1); % ne comprend pas ??? quoi ???a sert
% end
[ERR,qacm1_verif]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');

% accrochage sur arc tout thermique
Pf_motelec=zeros(size(cacm1)); % pertes par frottement en mode tout-thermique, utiles pour le bilan de puissance
Dsoc_tt=Inf;
if param.tout_thermique==1    
    if nargin==10 | recalcul==0
        if sum(csec_emb1(:,i))>0
            [~,ind_tt]=min(abs(pbat-pacc(i)),[],2); 
            % on choisi ici l'arc le plus proche du mode tout thermique sans choisir de rapportde bo???te.
            % on fixe le couple du moteur electrique a sa valeur de frottement
            % (-1), ses pertes sont nulles. On fait ensuite l'approximation
            % que pour cet arc pbat=pacc,
            % on en d???duit le chemin tout-elec
            cacm1(:,ind_tt)=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,wacm1(:,ind_tt)); 
            qacm1(:,ind_tt)=0;
            pbat(:,ind_tt)=pacc(:,i);
            Dsoc_tt=Dsoc(:,ind_tt);
            Pf_motelec(:,ind_tt)=cacm1(:,ind_tt).*wacm1(:,ind_tt); % pertes par frottements
        end
    end
    if nargin==12 & recalcul==1 & elhyb(i)==2 % cas tout thermique
            cacm1=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,wacm1);
            qacm1=0;
            pbat=pacc(i);
            [ERR,ibat,~,soc]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat,soc_p,0,0,0);
            Dsoc=soc-soc_p;
            Pf_motelec=cacm1*wacm1;
    end
end
% Connexion
cprim2_cpl=cacm1-VD.ACM1.J_mg.*dwacm1;

[ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl,cprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,cprim2_cpl,0);


%% A priori en hybride pas de patinage emb2
%[ERR,cprim_emb2,wprim_emb2,dwprim_emb2,etat_emb]=calc_emb2(VD,csec_emb2,wsec_emb2,dwsec_emb2,elhyb);

wmt=wprim1_cpl;
dwmt=dwprim1_cpl;
cmt=cprim1_cpl+VD.MOTH.J_mt.*dwmt;


if VD.CYCL.ntypcin==3
    wmt=wmt*ones(size(cmt));
end

% Calcul des conditions de fonctionnement du moteur thermique
[ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,1,0);

% On met ?? inf les dcarb des cmt impossible
dcarb(isnan(cmt))=Inf;

% Calculs batterie il faut supprimer les arcs impossible
% Jusqu'ici ibat est calcul?? ?? partir de Dsoc.
[ERR,Ibat,~,~,~,E,R]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat,soc_p,0,0,0);

dcarb(isnan(Ibat))=Inf;

% Critere de cout des arcs
cout=dcarb*param.pas_temps;

if  VD.CYCL.ntypcin==3 || (nargin==12 && recalcul==1)
    if elec_pos(i)==1
        cout(1)=0; % cet arc est en fait l'arc tout ??lec
    end
else
    % si l'arc tout elec est possible alors le premier arc (colonne 1) est
    % l'arc tout elec, on fixe son cout a 0. Si ce n'est pas le cas, on
    % interdit le point mort pour ce premier arc.  
    if sum(elec_pos(:,i))>=1
        indice_cout_possible=find(~isinf(cout(:,1)) & ~isnan(cout(:,1)) & elec_pos(:,i)==1);
        cout(indice_cout_possible,1)=0;
    else
        cout(1,1)=Inf;
    end
    
    if csec_cpl(2,1)>0 % Si le couple est positif, (traction), Il est impossible de choisir le point mort.
       cout(1,:)=Inf;
    end
end

% bilan puissance
rend_cpl=VD.ADCPL.rend*ones(size(cprim2_cpl));
rend_cpl(cprim2_cpl<0)=1/VD.ADCPL.rend;

%pbat_calc=(qacm1+pacc(i)+VD.ACM1.J_mg*dwacm1*wacm1+(cprim_emb1(:,i)*wprim_emb1(:,i)-((cmt+cfrein_meca_mt)*wmt-VD.MOTH.J_mt*dwmt*wmt))./rend_cpl)-Pf_motelec;
pbat_calc=(qacm1+pacc(i)+VD.ACM1.J_mg*dwacm1.*wacm1+(csec_cpl.*wsec_cpl-((cmt+cfrein_meca_mt).*wmt-VD.MOTH.J_mt*dwmt.*wmt))./rend_cpl)-Pf_motelec;

% calcul de la difference sur pertes bat due aux diff??rences
% sur les carto backward et forward  du motelec
diff_pertes_bat=(qacm1-qacm1_verif)./qacm1*100;

%limitation diverse
cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
p_elec_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Pelec_max(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
p_elec_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Pelec_min(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);

prec_num=1e-10;
cout(cacm1<cacm1_min-prec_num | cacm1>cacm1_max+prec_num | pacm1<p_elec_min-prec_num | pacm1>p_elec_max+prec_num)=Inf;

if param.verbose>=10
    assignin('base','cout',cout)
end

diff_pertes_bat(cacm1<cacm1_min | cacm1>cacm1_max | pacm1<p_elec_min | pacm1>p_elec_max)=0;

if max(abs(pbat-pbat_calc))>0.1 & param.verbose>=2
    chaine='Energy balance not fullfill \n time : %d';
    warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine,i);
    fprintf(1,'%s %.6f \n','pbat-pbat_calc : ',pbat-pbat_calc);
    fprintf(1,'%s %.1f \n','cmt : ',cmt);
    fprintf(1,'%s %.1f \n','wmt : ',wmt);
    fprintf(1,'%s %.1f \n','wsec_emb1 : ',wsec_emb1(i));
    fprintf(1,'%s %.1f \n','wprim_emb1 : ',wprim_emb1(i));
    fprintf(1,'%s %.1f \n','wprim1_cpl : ',wprim1_cpl);
    fprintf(1,'%s %.1f \n','cprim1_cpl : ',cprim1_cpl);
    fprintf(1,'%s %.1f \n','cfrein_meca_mt : ',cfrein_meca_mt);
    fprintf(1,'%s %.1f \n',' pbat : ',pbat);
    fprintf(1,'%s %.1f \n',' pbat_calc : ',pbat_calc);
    fprintf(1,'%s %.1f \n',' ibat : ',ibat);
    fprintf(1,'%s %.1f \n',' csec_cpl : ',csec_cpl);
    fprintf(1,'%s %.1f \n',' cout : ',cout);
    fprintf(1,'%s %.1f \n',' cacm1 : ',cacm1);
    fprintf(1,'%s %.1f \n',' wacm1 : ',wacm1);
    fprintf(1,'%s %.1f \n',' qacm1 : ',qacm1);
    fprintf(1,'%s %.1f \n',' qacm1_verif : ',qacm1_verif);
    fprintf(1,'%s %.6f \n',' qacm1-qacm1_verif : ',qacm1-qacm1_verif);
    fprintf(1,'%s %.1f \n',' elhyb : ',elhyb(i));
    pause
end

% Minimisation des cout dans le cas ntycin==1 Optimisation des rapports de
% boite

if VD.CYCL.ntypcin==1
    cout(isnan(cout))=Inf;
    [cout,indice_rap_opt]=min(cout);  
end

cfrein_meca_pelec_mot=0;
if nargin==12 && recalcul==1 && elhyb(i)==0 % cas tout elec en recalcul gerer apres bilan de puissance
    % pour des pb de difference de carto fw bw : sinon on ne le boucle pas parfaitement le bilan
    cout=0;
    dcarb=0;
    cmt=0;
    wmt=0;
    [ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,0);
    % connexion
    wsec_cpl=wprim_emb1(i);
    dwsec_cpl=dwprim_emb1(i);
    csec_cpl=cprim_emb1(i);
    [ERR,cprim2_cpl,wacm1,dwacm1]=calc_red(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0);
    cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;
    [Li,Co]=size(cacm1);
    wacm1=repmat(wacm1,Li,1);
    [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');

    
    % ici cacm1 as ??t?? limit?? a cacm1_min (en bw) donc pas besoin de le
    % refaire par contre si pacm1_elec est superieur a pacm1_elec en fw il
    % faut recalculer le frein meca correspondant pour boucler le bilan
    % de puissance (car ibat as ??t?? calcul?? avec pacm1_elec_min)
    p_elec_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Pelec_min(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
    pacm1_elec=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1);
    
    if pacm1_elec<p_elec_min
       pacm1_elec=p_elec_min;
       [ERR,qacm1_fw]=calc_pertes_acm_fw(ERR,VD.ACM1,pacm1_elec,wacm1,dwacm1,1);
       cacm1_fw=(pacm1_elec-qacm1_fw)./wacm1;
       cfrein_meca_pelec_mot=cacm1_fw-cacm1;
       qacm1=qacm1_fw;
       cacm1=cacm1_fw;
    end
    
    % Cas ou ibat atteind les limites de ibat_min
    pbat=ibat.*ubat;
    [ERR,~,~,~,~,~,~,~,Ibat_min_calc]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat,100-VD.INIT.Dod0,0,-1,0,param);
    prec_num=1e-10;
    if ibat<Ibat_min_calc+prec_num
        %bilanP_loc=ibat.*ubat-pacc(i)-cacm1.*wacm1-qacm1
        % on rappele calc_batt, on regarde si on tape dans ibat_min
        %si oui on recalcul pabt puis cacm1 en fw puis cfrein_cacm1
        pacm1_elec=pbat-pacc(i);
        [ERR,qacm1_fw]=calc_pertes_acm_fw(ERR,VD.ACM1,pacm1_elec,wacm1,dwacm1,1);
        cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;
        cacm1_fw=(pacm1_elec-qacm1_fw)./wacm1;
        cfrein_meca_pelec_mot=cacm1_fw-cacm1;
        qacm1=qacm1_fw;
        cacm1=cacm1_fw;
    end
end

if abs(diff_pertes_bat(1))>5 & param.verbose>=2 % si differences calcul pertes bw fw trop importante on envoi un warning
    chaine='more than 5 percent of error on pbat due to backward/forward maps of motelec \n time : %d';
    warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine,i);
%    wacm1
%    p_elec_min
%    diff_pertes_bat'
%    pacm1
%    qacm1'
%    qacm1_verif'
end

if sum(isinf(cout))==length(cout)
    chaine=' all edge are impossible, the systems possibilites does not allow to fullfil the required systems operating point ! \n time : %d \n Dsoc : %s  \n cost : %s \n ibat: %s \n ubat: %s ';
    ERR=MException('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine,i,num2str(Dsoc),num2str(cout),num2str(ibat),num2str(ubat));
    %if ~isfield(param,'dim') | param.dim==0 % Dans le cas d'un processus de dimensionnement param.dim==1 on ne sort pas en "vrac" on renvoi seulement l'erreur
        [cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=backward_erreur(size(Dsoc));
        return
    %end
end

if sum(isnan(cout))
    chaine='some edge cost are NaN, to avoid problems in solve graph, prefer to fixe them to Inf \n time : %d' ;
    warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine,i);
end

return

