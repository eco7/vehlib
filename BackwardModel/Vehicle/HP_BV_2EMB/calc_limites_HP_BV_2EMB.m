% function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=calc_limites_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul des limites du graphes.
% On ne calcule ici que les soc min et max sans se preocuper
% de la remontes du graphe ou de soc <0 ou >100%.
% La limite min doit etre calcule avec precision (mode tout elec)
% la mlimite max peut etre calcule "grossierement" d'eventuel arc impossible
% dans le graphe seront elimine par la suite
%
% interface entree :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discreetisation etc)
% VD    :       structure de description vehicule et composants
% csec_emb1,wsec_emb1, dwsec_emb :   Couple, vitesse, derivee de la vitesse sur le secondaire de l'embrayage "principal" (vecteur sur le cycle)
%
% interface de sortie
% ibat_max :    courant max mode tout elec (en recup c'est le courant de recup en tout elec)
% ibat_min :    courant min recharge max ( en recup courant recup plus recharge par moth)
% elec_pos :    vecteur indiquant si mode elec possible , ou zero si impossible
% soc_min :     soc correspondant a ibat_max
% soc_max :     soc correspondant a ibat_min
% Dsoc_min :    variation soc entre deux instant correspondant a ibat_max
%               (atention a integre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% Dsoc_max :    variation soc entre deux instant correspondant a ibat_min
%               (atention a integre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% 16-12-10(EV): Creation 

function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec,pres_elec,dcarb_soc_min,cmt_soc_min]=calc_limites_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1)
ERR=[];
ii_max=size(VD.CYCL.temps);
soc_min=zeros(ii_max);
soc_max=zeros(ii_max);
Dsoc_min=zeros(ii_max);
Dsoc_max=zeros(ii_max);
indice_rap_opt_elec=zeros(ii_max);
cmt_soc_min = zeros(ii_max);
wmt_soc_min = zeros(ii_max);
dwmt_soc_min = zeros(ii_max);
dcarb_soc_min = zeros(ii_max);
%% recherche du mode tout electrique a chaque pas de temps
% en tout elec on ne patine pas sur emb1
[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,zeros(size(wsec_emb1)));
   
% connexion
wsec_cpl=wprim_emb1;
dwsec_cpl=dwprim_emb1;
csec_cpl=cprim_emb1;

[ERR,cprim2_cpl,wprim2_cpl,dwprim2_cpl]=calc_red(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0);

%Connexion
wacm1_elec=wprim2_cpl;
dwacm1_elec=dwprim2_cpl;
cacm1_elec=cprim2_cpl+VD.ACM1.J_mg.*dwacm1_elec;

%% On doit venir verifier que wacm1 n'est pas superieure a la limite autorisee
if VD.CYCL.ntypcin==1
    [nrap,~] = size(csec_emb1);
    pas_temps=repmat(VD.CYCL.pas_temps,[nrap 1]);
    %il faut verifier que au min un rapport permet de passer
    [nrap,~] = size(csec_emb1);
    lim_wacm1_elec=wacm1_elec>max(VD.ACM1.Regmot_cmax);
    if sum(sum(wacm1_elec(2:nrap,:)>max(VD.ACM1.Regmot_cmax)) == nrap-1) >=1
        chaine='error in calc_limites_HP_BV_2EMB : motor speed upper than maximum speed allow' ;
        ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
        if param.verbose>=1
            warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine);
            Indices = find(sum(wacm1_elec(2:nrap,:)>max(VD.ACM1.Regmot_cmax)) == nrap-1);
        end
        [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=backward_erreur(length(VD.CYCL.temps),0,0);
        return
    end
else
    pas_temps=VD.CYCL.pas_temps;
    if sum(wacm1_elec>max(VD.ACM1.Regmot_cmax))>=1
        chaine='error in calc_limites_HP_BV_2EMB : motor speed upper than maximum speed allow' ;
        ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
        if param.verbose>=1
            warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine);
            Indices=find(wacm1_elec>max(VD.ACM1.Regmot_cmax))
        end
        [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=backward_erreur(length(VD.CYCL.temps),0,0);
        return
    end
end

% Calcul des conditions en amont de la machine electrique
%%% limitations ME
cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec);
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec);

% Si cacm1<cacm1_min on fait de la recup et on vas faire le max possible
% le reste en frein meca, donc on sature cacm1 a cacm1_min
cacm1_elec(cacm1_elec<cacm1_min)=cacm1_min(cacm1_elec<cacm1_min); % C'est déja fait dans calc_HP_BV_2EMB ?? (EV 00-10-2017)

% Si cacm1>cacm1_max on le met égal à cacm1_max et elec_pos=0
elec_pos=ones(size(csec_emb1));
I_cacm1_max=find(cacm1_elec>cacm1_max);
elec_pos(I_cacm1_max)=0;
cacm1_elec(I_cacm1_max)=cacm1_max(I_cacm1_max);

[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec,0,param.fit_pertes_acm);

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);

if VD.CYCL.ntypcin==1
    pacc=repmat(pacc,size(csec_emb1,1),1);
end

% calcul puissance motelec
% On recherche les cas ou en recup on fait plus de pertes dans acm1
% que l'on as de puissance meca
I=find(csec_emb1<0 & qacm1>-cacm1_elec.*wacm1_elec); 
cacm1_elec(I)=0;
wacm1_elec(I)=0;
qacm1(I)=0;

pacm1_elec=VD.VEHI.nbacm1*(cacm1_elec.*wacm1_elec+qacm1);

%%% Pour des pb numeriques d'inversion de carto
% on vient verifier que les calcul fw se passeront bien dans calc_cout arc
% on remet les eventuelles limitations necessaire
eps=1e-10;
p_elec_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Pelec_min(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1_elec);

%[ERR,qacm1_fw]=calc_pertes_acm_fw(ERR,VD.ACM1,pacm1_elec,wacm1_elec,dwacm1_elec,0,param.fit_pertes_acm);

pacm1_elec(pacm1_elec<p_elec_min)=p_elec_min(pacm1_elec<p_elec_min)+eps;

% Puissance reseau electrique
pres=pacm1_elec+pacc;

% prise en compte limitation batterie 
% on range dans elec_pos les indices des points ou le mode tout elec est possible
% ATT : en rapport de boite optimises on as un tableau ( elec peut etre
% possible sur certain rapport et pas d'autres).

[ERR,ibat_max,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,pas_temps,pres,100-VD.INIT.Dod0,0,-1,0,param);

%[ERR,~,~,~,~,~,~,~,ibat_min_calc]=calc_batt(ERR,VD.BATT,pas_temps,-100*ones(size(csec_emb1)),100-VD.INIT.Dod0,0,-1,0,param);

[ERR,~,~,~,~,~,~,~,ibat_min_calc,Ubat_ibat_min_calc,ibat_max_calc,Ubat_ibat_max_calc]=calc_batt(ERR,VD.BATT,pas_temps,-100*ones(size(csec_emb1)),100-VD.INIT.Dod0,0,-1,0,param);
% Cas ou l'on as explose les limites en recup
% On doit imposer que Ibat_max = VD.BATT.Ibat_min

if VD.CYCL.ntypcin==1
  %  ibat_max(isnan(ibat_max)&cprim_emb1<0&lim_wacm1_elec==0)=ibat_min_calc(isnan(ibat_max)&cprim_emb1<0&lim_wacm1_elec==0); % non ! pas si on eclate les limites en tension

    ind=find(isnan(ibat_max)&cprim_emb1<0&lim_wacm1_elec==0);
    ibat_max(ind)=ibat_min_calc(ind); % non ! pas si on eclate les limites en tension
    pres(ind)=ibat_max(ind).*Ubat_ibat_min_calc(ind);
else 
    ind=find(isnan(ibat_max)&cprim_emb1<0);
    ibat_max(ind)=ibat_min_calc(ind);
    pres(ind)=ibat_max(ind).*Ubat_ibat_min_calc(ind);
end
pres_elec=pres;
%elec_pos=ones(size(csec_emb1))
i_hyb = find(isnan(ibat_max));
elec_pos(i_hyb)=0;


% Connexion
cprim2_cpl=cacm1_elec-VD.ACM1.J_mg.*dwacm1_elec;
[ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl,cprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,cprim2_cpl,0);
%cmt_elec(i_hyb) = calc_
wmt_soc_min(i_hyb)=wprim1_cpl(i_hyb);
dwmt_soc_min(i_hyb)=dwprim1_cpl(i_hyb);
cmt_soc_min(i_hyb)=cprim1_cpl(i_hyb)+VD.MOTH.J_mt.*dwmt_soc_min(i_hyb);
[ERR,dcarb_soc_min(i_hyb),cmt_soc_min(i_hyb)]=calc_mt(ERR,VD.MOTH,cmt_soc_min(i_hyb),wmt_soc_min(i_hyb),dwmt_soc_min(i_hyb),1,0,0,param.fit_pertes_moth);

% calcul de la pbat_max

%ibat_max(isnan(ibat_max))=interp1(VD.BATT.DoD_Ibat_max,VD.BATT.Ibat_max,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension et attention Nbranchepar !!
ibat_max(isnan(ibat_max))=ibat_max_calc(isnan(ibat_max));

if VD.CYCL.ntypcin==1 % rapport de boite optimises
    % On vient prendre le rapport de boite qui maximise le courant batterie
    % Si il y as egalite on prend le premier rapport de la colonne
    % Une fois le choix des rapports fait on recree un vecteur rendement faradique
    vitmin=1e-10;
    ind_co=find(sum(csec_emb1)~=0 & abs(sum(wacm1_elec))>vitmin);
    ibat_max(1,ind_co)=Inf;  
    [ibat_max,indice_rap_opt_elec]=min(ibat_max);
    pres_elec=mat_ind(indice_rap_opt_elec,2,pres_elec);
    [ERR,~,~,~,~,~,RdF]=calc_batt_fw(ERR,VD.BATT,pas_temps(1,:),ibat_max,100-VD.INIT.Dod0,0);
    
    indice_neutral=find(VD.CYCL.vitesse<vitmin & indice_rap_opt_elec~=1);
    if ~isempty(indice_neutral)
        warning('problem with gearbox number at neutral')
        indice_neutral
    end    
end

soc_min(1)=100-VD.INIT.Dod0;
Dsoc_min(1)=0;

for ii=2:length(VD.CYCL.temps)
        soc_min(ii)=soc_min(ii-1)-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*VD.CYCL.pas_temps(ii);
        Dsoc_min(ii)=-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*VD.CYCL.pas_temps(ii);
end


%% recherche de la pbat_min (mode hybride)
% calcul du couple max moteur thermique

[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,ones(size(csec_emb1)));
% connexion
wsec_cpl=wprim_emb1;
dwsec_cpl=dwprim_emb1;
csec_cpl=cprim_emb1;
   
wmt=wsec_cpl;
dwmt=dwsec_cpl;
if VD.MOTH.ntypmoth ~=5
    cmt_max=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt);
    cmt_min=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_min,wmt);
else
    N = wmt*30/pi;
    cmt_min = -(VD.MOTH.PMF_cst + VD.MOTH.PMF_N*N + VD.MOTH.PMF_N2*N.^2)*VD.MOTH.Vd/(VD.MOTH.Rev*2*pi);
    % calcul du couple max
    if isfield(param,'pas_phi')  
        Phi = param.phi_min:param.pas_phi:param.phi_max;
    elseif isfield(param,'phi_cst')
        Phi = param.phi_cst;
    else
        Phi = 1;
    end
    l_Phi = length(Phi);
    if isfield(param,'pas_delta_AA')
        delta_AA = param.delta_AA_min:param.pas_delta_AA:param.delta_AA_max;
    elseif isfield(param,'delta_AA_cst')
        delta_AA = param.delta_AA_cst;
    else
        delta_AA = 1;
    end
    l_AA = length(delta_AA);
    Phi = repmat(Phi,1,l_AA);
    delta_AA = repmat(delta_AA,l_Phi,1);
    delta_AA = delta_AA(:)';
    Phi = Phi(:)';
    Padm = interp1(VD.MOTH.p_intake_wmt,VD.MOTH.p_intake,wmt,'linear','extrap');
    if isfield(param,'phi_lut') && param.phi_lut == 1       
        commande_Phi = interp2(VD.ECU.Reg_2dPhi2, VD.ECU.Padm_2dPhi2', VD.ECU.Phi_2d2', wmt, Padm)';
        commande_AA = repmat(delta_AA,length(wmt),1);
        cmt_max=zeros(size(wmt));
        for ii=1:length(wmt)
            [~, cmt_max(ii)] = calc_mt_3U(wmt(ii),commande_Phi(ii),commande_AA(ii),Padm(ii),1,VD);
        end        
    else
        [~, cmt_max] = calc_mt_3U(wmt',Phi,delta_AA,Padm',1,VD);
        cmt_max = max(cmt_max,[],2)'; % On ne garde que les maximums
    end
end

% Connexion
cprim1_cpl=cmt_max-VD.MOTH.J_mt.*dwsec_cpl;
[ERR,cprim1_cpl,wprim_cpl1,dwprim1_cpl,wprim2_cpl,dwprim2_cpl,cprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,cprim1_cpl,0,2);

%Connexion
wacm1_hyb=wprim2_cpl;
dwacm1_hyb=dwprim2_cpl;
cacm1_hyb=cprim2_cpl+VD.ACM1.J_mg.*dwacm1_hyb;

% Si cacm1_hyb > cacm1_max et mode elec impossible = on ne passe pas le cycle
cacm1_hyb_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_hyb);
if VD.CYCL.ntypcin==1 % rapport de boite libre
    sum_elec_pos=sum(elec_pos(2:end,:));
    sum_cacm1_max=sum(cacm1_hyb(2:end,:)<cacm1_hyb_max(2:end,:));
    I=find(sum_elec_pos==0 & sum_cacm1_max==0); % aucun rapport ne permet le mode elec ni le mode hybride
else
    I=find(cacm1_hyb>cacm1_hyb_max & elec_pos==0);
end
if ~isempty(I)
    chaine='error in calc_limites_HP_BV_2EMB : the power train cannot provide the required power' ;
    ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=backward_erreur(length(VD.CYCL.temps),0,0);
    return
end

% Calcul des conditions en amont de la machine electrique
% limitations ME
cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_hyb);

% Si cacm1<cacm1_min on fait de la recup et on vas faire le max possible
% le reste en frein meca, donc on sature cacm1 a cacm1_min
cacm1_hyb(cacm1_hyb<cacm1_min)=cacm1_min(cacm1_hyb<cacm1_min);

[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hyb,wacm1_hyb,dwacm1_hyb,1,param.fit_pertes_acm);

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);
if VD.CYCL.ntypcin==1
    pacc=repmat(pacc,size(csec_emb1,1),1);
end

pacm1_hyb=VD.VEHI.nbacm1*(cacm1_hyb.*wacm1_hyb+qacm1);

% Puissance reseau electrique
pbat=pacm1_hyb+pacc;

% prise en compte limitation batterie 
[ERR,ibat_min,ubat_min,soc,ah,E,R,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat,100-VD.INIT.Dod0,0,-1,0,param);

% calcul de la pbat_min
% non pas si on eclate les limites en tension

if VD.CYCL.ntypcin==1
    % on fait le choix du rapport de boite 
    % qui maximise le courant batterie
    ind_co=find(sum(csec_emb1)~=0);
    ibat_min(1,ind_co)=Inf;
    
    ind_co=find(sum(csec_emb1)==0);
    ibat_min(2:end,ind_co)=Inf;
    [ibat_min,indice_rap_opt_hyb]=min(ibat_min);
    RdF=RdF(sub2ind(size(RdF),indice_rap_opt_hyb,1:length(indice_rap_opt_hyb)));
    ibat_min(isinf(ibat_min))=NaN;
end

ibat_min(isnan(ibat_min))=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0);


% Dans des situation de patinages, on peut arriver a ibat_min>ibat_max
% On a alors plus d'arc dans le graphe 
% pour eviter cela on impose ibat_min au min <=ibat_max
ibat_min(ibat_min>ibat_max)=ibat_max(ibat_min>ibat_max);

soc_max(1)=100-VD.INIT.Dod0;
Dsoc_max(1)=0;
for ii=2:length(VD.CYCL.temps)
        soc_max(ii)=soc_max(ii-1)-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*pas_temps(ii);
        Dsoc_max(ii)=-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*pas_temps(ii); 
end

if sum(ibat_max<ibat_min)>0 ||  sum(soc_max<soc_min)>0 || sum(Dsoc_max<Dsoc_min)>0
    chaine='error in calc_limites_HP_BV_2EMB' ;
    ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=backward_erreur(length(VD.CYCL.temps),0,0);
    return

end

if param.verbose>=2
    assignin('base','ibat_min',ibat_min)
    assignin('base','ibat_max',ibat_max)
    assignin('base','soc_min',soc_min)
    assignin('base','soc_max',soc_max)
    assignin('base','elec_pos',elec_pos)
    
    if isfield(VD.CYCL,'vitesse')
    figure(3)
    clf
    plot(VD.CYCL.temps,ibat_min,VD.CYCL.temps,ibat_max,VD.CYCL.temps,VD.CYCL.vitesse)
    legend('ibat min','ibat max','vitesse')
    title('courant min max fin calc limite')
    grid
    end
    
    figure(4)
    clf
    plot(VD.CYCL.temps,soc_min,VD.CYCL.temps,soc_max,VD.CYCL.temps,60+Dsoc_min,VD.CYCL.temps,Dsoc_max+60)
    legend('soc min','soc max','Dsoc min','Dsoc max')
    title('soc min max fin calc limite')
    grid
    
    figure(5)
    plot(VD.CYCL.temps,indice_rap_opt_elec);
    
end
