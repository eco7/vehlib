% function [ERR,VD,ResXml]=calc_HP_BV_2EMB(ERR,vehlib,param,VD)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul d'un vehicule electrique muni d'une association
% batterie/super-condensateur dans VEHLIB en mode backward
%
% Arguments appel :
% Arguments de sortie :
%
% EV 26/10/09
% En recup si le courant batt + ucap ne permet pas de recuperer toute
% l'energie que se passe t'il ??
% A priori OK quand meme car meme si qacm1 est calcule pour un couple
% moteur sans frein meca, de toute facon si on vient limiter a cause des
% courants on recupere quand meme tout ce que l'on peut. Seul cacm1, qacm1
% et donc l'eventuel frein meca sont faux

function [ERR,VD,ResXml,r]=calc_HP_BV_2EMB(ERR,vehlib,param,VD)
global Ham_hors_limite;
Ham_hors_limite=1e6;

% Calcul cinematiques complementaires
% Avec re-echantillonnage eventuel
if ~isfield(param,'pas_temps') | param.pas_temps==0
    param.pas_temps=1;
end
[ERR,VD,dist]=calc_cinemat(ERR,VD,param);

if ~isempty(ERR)
    ResXml=struct([]); 
    return;
end

if (isfield(param,'fit_pertes_acm') & param.fit_pertes_acm==1)
    [VD.ACM1] = fitcarto(VD.ACM1,param.verbose);
elseif ~isfield(param,'fit_pertes_acm')
    param.fit_pertes_acm=0;    
end

if (isfield(param,'fit_pertes_moth') & param.fit_pertes_moth==1)
    [VD.MOTH] = fitcarto(VD.MOTH,param.verbose);
elseif ~isfield(param,'fit_pertes_moth')
    param.fit_pertes_moth=0;
end

% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
if ~isempty(ERR)
    ResXml=struct([]); 
    return;
end

% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,penteFpk,fres,faero,froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Dans certain cycle CIN_ARTURB_BV_classe3 par exemple, il peut arriver
%que croue>0 alors que on est au point mort, impossible avec des archi avec
%VD.BV on met alors le couple a zero

if VD.CYCL.ntypcin==3
    ind_croue_pm=find(croue>0 & VD.CYCL.rappvit==0);
    if ~isempty(ind_croue_pm)
        chaine=strcat('in some point, a positive wheel torque exist with neutral gear box position \n corresponding time :',num2str(ind_croue_pm),'\n wheel torque fixe to zero in these cases');
        warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine);
        croue(ind_croue_pm)=0;
    end
end

if ~isempty(ERR)
    ResXml=struct([]); 
    return;
end

% Connexion
csec_red=croue;
wsec_red=wroue;
dwsec_red=dwroue;

% Calcul des conditions en amont du reducteur
[ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);
if ~isempty(ERR)
    ResXml=struct([]); 
    return;
end

% Connexion
csec_bv=cprim_red;
wsec_bv=wprim_red;
dwsec_bv=dwprim_red;


% Calcul des conditions en amont de la boite de vitesse
% NB si on est en rapport libre ou optimaux on as des matrices 
% avec en nb de ligne le nb de rapport de boite plus pm
[ERR,cprim_bv,wprim_bv,dwprim_bv]=calc_bv(VD,csec_bv,wsec_bv,dwsec_bv,0);
if ~isempty(ERR)
    ResXml=struct([]); 
    return;
end

% Connexion 
csec_emb1=cprim_bv;
wsec_emb1=wprim_bv;
dwsec_emb1=dwprim_bv;


%% Prise en compte du frein meca quand Croue <0, on considere que dans ce cas cprim1_cpl =0 

[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,zeros(size(wsec_emb1)));

% connexion
wsec_cpl=wprim_emb1;
dwsec_cpl=dwprim_emb1;
csec_cpl=cprim_emb1;
[ERR,cprim2_cpl,wprim2_cpl,dwprim2_cpl]=calc_red(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0);

% Connexion
wacm1=wprim2_cpl;
dwacm1=dwprim2_cpl;
cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;


% On vient limiter cacm1 a sa valeur max
cacm1=max(cacm1,interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1));

if isfield(VD.ECU,'Vit_Cmin')
    cacm1=max(cacm1,interp1(VD.ECU.Vit_Cmin,VD.ECU.Cpl_min,wacm1));
end

% Recalcul de la partie frein meca au niveau de la roue
cprim2_cpl=(cacm1-VD.ACM1.J_mg.*dwacm1)*VD.VEHI.nbacm1;


[ERR,~,~,~,~,csec_cpl]=calc_adcpl_fw(ERR,VD.ADCPL,0,0,cprim2_cpl,0,wsec_cpl,dwsec_cpl);


% connexion grandeurs utile apres calcul
wsec_emb1=wsec_cpl;
dwsec_emb1=dwsec_cpl;
csec_emb1=csec_cpl;

wprim_bv=wsec_emb1;
dwprim_bv=dwsec_emb1;
cprim_bv=csec_emb1;

%[ERR,csec,wsec,dwsec]=calc_bv_fw(ERR,VD,cprim_bv,wprim_bv,dwprim_bv,0,0);


% Connexion
wprim_red=wsec_bv;
dwprim_red=dwsec_bv;
cprim_red=csec_bv;


[ERR,csec_red,wsec_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
cfrein_meca=csec_red-croue;



%% strategie selon les cas (doivent renvoyer des vecteurs cprim2_cpl et
%% elhyb de taille VD.CYCL.temps)
if strcmp(lower(param.optim),'ligne')  
    %[ERR,cprim2_cpl,elhyb]=calc_ligne_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
elseif strcmp(lower(param.optim),'lagrange')
    [ERR,VD,r]=calc_lagrange_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);  
elseif strcmp(lower(param.optim),'lagrange_cycle')
    [ERR,VD,r]=calc_lagrange_cycle_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
elseif strcmp(lower(param.optim),'prog_dyn')
    [ERR,VD,r]=calc_prog_dyn_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
end

if ~isempty(ERR)
    ResXml=struct([]); %         [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2,NaN);
    return;
end
    
if sum(size(csec_emb1)==[1+size(VD.BV.rapport,2) size(VD.CYCL.vitesse,2)])==2 % Cas ou on optimise les rapports de boite
    csec_emb1=csec_emb1(sub2ind(size(csec_emb1),r.indice_rap_opt,1:length(r.indice_rap_opt)));
    wsec_emb1=wsec_emb1(sub2ind(size(wsec_emb1),r.indice_rap_opt,1:length(r.indice_rap_opt)));
    dwsec_emb1=dwsec_emb1(sub2ind(size(dwsec_emb1),r.indice_rap_opt,1:length(r.indice_rap_opt)));
    
    % connexion
    wprim_bv=wsec_emb1;
    dwprim_bv=dwsec_emb1;
    cprim_bv=csec_emb1;
    
    rap_opt=r.indice_rap_opt-1;
else
    rap_opt=VD.CYCL.rappvit;
end
  
% Calcul conditions en amont de l'embrayage (a refaire une fois fait le
% choix de elhyb)

[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,r.elhyb);
if ~isempty(ERR)
    ResXml=struct([]); 
    return;
end

% Calcul conditions en amont du coupleur
% connexion

wsec_cpl=wprim_emb1;
dwsec_cpl=dwprim_emb1;
csec_cpl=cprim_emb1;
[ERR,r.cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,r.cprim2_cpl,0);



if param.verbose>=1
    fprintf(1,'%s %.1f %s\n','La masse du vehicule est de : ',mean(masse),'kg');
end

%trajectoire optimale
if param.verbose>=2 & ~strcmp(lower(param.optim),'prog_dyn')
    figure
    clf
    hold on
    
    if strcmp(lower(param.optim),'prog_dyn')
        % limites du graphe
        plot(VD.CYCL.temps,soc_min,'b',VD.CYCL.temps,soc_max,'b')
        assignin('base','soc_min',soc_min);
        assignin('base','soc_max',soc_max);
        plot(VD.CYCL.temps,soc)
    else
        plot(VD.CYCL.temps,r.soc)
        
    end
    grid
    title('trajectoire optimale')
    ylabel('soc en %')
end

% grandeurs a rajouter dans r

% Calcul des auxiliaires electriques
[ERR,r.pacc]=calc_acc(VD);
if ~isempty(ERR)
    ResXml=struct([]); 
    return;
end

% Cycle
r.tsim=VD.CYCL.temps;
r.vit=VD.CYCL.vitesse;
r.acc=VD.CYCL.accel;
r.vit_dem=r.vit;
r.masse=masse;

r.rappvit_bv=rap_opt;
%r.distance=cumtrapz(VD.CYCL.temps,VD.CYCL.vitesse); % distance calculee a partir de la vitesse VD.CYCL.vitesse
r.pente=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,VD.CYCL.distance,'linear',0); % reechantillonage pente fonction distance calculee.     

% Recalcul du frein meca supplementaires due aux limitations batterie ou motelec :
%cfrein_meca_pelec_mot  represente le frein 
% meca a mettre si c'est la batterie ou la puissance
% elec du motelec qui est limitante et pas le cacm1_min.
% Il est calcule niveau motelec. a recalculer 
% niveau roue et ajouter a cfrein_meca
% NB on ramene le couple sans les rendements sinon il faut aussi
% recalculer les pertes dans chaque organes
if isfield(r,'cfrein_meca_pelec_mot')
    VD.BV.k1=[0 VD.BV.k];
    cfrein_meca_pelec_mot_roue=r.cfrein_meca_pelec_mot*VD.ADCPL.kred.*VD.BV.k1(VD.CYCL.rappvit+1)*VD.RED.kred;
    cfrein_meca=cfrein_meca+cfrein_meca_pelec_mot_roue;
end
  
r_var_sup={'croue','wroue','cprim_red','wprim_red','csec_red','wsec_red','cprim_bv','wprim_bv','csec_bv','wsec_bv',...
    'wprim1_cpl','wprim2_cpl','csec_cpl','wsec_cpl','dwsec_cpl','cprim_emb1','wprim_emb1','dwprim_emb1','csec_emb1','wsec_emb1','dwsec_emb1','cfrein_meca'};
[ERR,r]=affect_struct(r_var_sup,r);

% cas ou en recup on fait plus de pertes dans acm1 que l'on as de puissance meca
I=find(csec_emb1<0 & r.qacm1>-r.cacm1.*r.wacm1 & r.elhyb==0);
r.cacm1(I)=0;
r.wacm1(I)=0;
r.qacm1(I)=0;
r.cprim2_cpl(I)=0;
r.cprim_red(I)=0;
r.csec_red(I)=0;
r.cprim_bv(I)=0;
r.csec_bv(I)=0;
r.csec_cpl(I)=0;
r.cprim_emb1(I)=0;
r.csec_emb1(I)=0;
r.cfrein_meca(I)=-r.croue(I);

ResXml=[];
if ~isfield(param,'ResXml') || param.ResXml~=0
[ResXml]=miseEnForme4VEHLIB(vehlib,'caller','r');

% Calcul de resultats synthetiques
[Res]=resultats(VD,param,ResXml);

conso100=Res.conso100;
cumcarb=Res.cumcarb;
co2_eq=Res.co2_eq;
CO2_gkm=Res.CO2_gkm;
ibat_eff=Res.ibat_eff;
ibat_moy=Res.ibat_moy;

% Ajout des grandeurs synthetiques dans une nouvelle table de la structure xml
[ResXml]=miseEnForme4GestionEnergie(ResXml);
end

bilan_PE=0;
if bilan_PE==1
    % Bilan de puissance et d'energie
    [bp]= bilanPE(VD,param,ResXml);
    bilanP=bp.bilanP;
end

if param.verbose>=2
    assignin('base','r',r)
    assignin('base','bp',bp)
end
% trace sollicitation batterie
% Inter=-100:25:200;
% figure;
% classe_energie(VD,VD.CYCL.temps,(ibat),3,min(ibat),max(ibat),10,0,0,1,{'titre'},1,Inter)
% return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,cprim2_cpl,elhyb]=calc_ligne_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1)
pdem=(csec_emb1.*wsec_emb1);
dod(1)=VD.INIT.Dod0;
elhyb(1)=0;
temps_elec=0;
temps_hyb=0;

for j=2:length(VD.CYCL.temps)
    % Choix de elhyb
    pdem_bat=interp1(VD.ECU.DoD,VD.ECU.pdem_bat,dod(j-1));
    pdem_hyb=interp1(VD.ECU.DoD_demarrage,VD.ECU.P_demarrage,dod(j-1));
    pdem_mt=pdem(j)-pdem_bat;
    if elhyb(j-1)==0;
        temps_elec=temps_elec+VD.CYCL.temps(j);
        temps_hyb=0;
    elseif elhyb(j-1)==1;
        temps_elec=0;
        temps_hyb=temps_hyb+VD.CYCL.temps(j);
    end
    
    if (dod(j-1)>VD.ECU.DOD_dec_hyb) | (VD.CYCL.vitesse(j)>VD.ECU.vmaxele) | ((pdem_mt>=pdem_hyb) & (temps_elec>VD.ECU.duree_ele))
        elhyb(j)=1;
    elseif (pdem_mt<VD.ECU.pseuil_decel | pdem_mt<VD.ECU.P_arret_hyb) & (dod(j-1)<VD.ECU.DOD_arret_hyb) & (VD.CYCL.vitesse(j)<VD.ECU.vmaxele)...
            & ((temps_hyb>VD.ECU.duree_hyb) | pdem_mt<VD.ECU.pseuil_decel)
        elhyb(j)=0;
    else
        elhyb(j)=elhyb(j-1);
    end
    
    if elhyb(j)==0;
        ERR= [];
        wprim_emb1=wsec_emb1(j);
        dwprim_emb1=dwsec_emb1(j);
        cprim_emb1=csec_emb1(j);
        
        if VD.CYCL.rappvit(j)==0
            wprim_emb1=0; % Si on est en tout elec au point mort le motelec ne tourne pas
            cprim_emb1=0;
            cprim_emb1=0;
        end
        
        wsec_cpl=wprim_emb1;
        dwsec_cpl=dwprim_emb1;
        csec_cpl=cprim_emb1;
        
        [ERR,cprim2_cpl(j),wprim2_cpl,dwprim2_cpl]=calc_red(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0);
        
        % ********************
        %%% calcul dod et verification du mode tout elec sur cacm1 et ibat
        % si on depasse les limites on impose un mode hybride
        %Connexion
        wacm1=wprim2_cpl;
        dwacm1=dwprim2_cpl;
        cacm1=cprim2_cpl(j)+VD.ACM1.J_mg.*dwacm1;
        
        % Calcul des conditions en amont de la machine electrique
        %%% limitations ME
        cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
        
        % Si cacm1>cacm1_max on ne peut faire du tout elec on passe en hybride
        if cacm1>cacm1_max
            elhyb(j)=1;
        end
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
        
        % Calcul des auxiliaires electriques
        [ERR,pacc]=calc_acc(VD);
            
        % Puissance reseau electrique
        pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc(j);
        
        [ERR,ibat,ubat,soc,ah,E,R,RdF]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,pres,100-dod(j-1),0,pres,0);
        dod(j)=100-soc;
        if ~isempty(ERR)
            elhyb(j)=1;
        end
    end
    
    if elhyb(j)==1; % le moteur fourni pdem + pdem_batt (pertes motelc pas pris en compte), sauf si pmoth<pdem on fait du boost
        % cela reviens a fournir pdem_batt*rend_cpl sur l'arbre du motelec
        %pdem_bat=interp1(VD.ECU.DoD,VD.ECU.pdem_bat,dod(j-1));
        [ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,1);
        %connexion et passage en scalaire
        wsec_cpl=wprim_emb1(j);
        dwsec_cpl=dwprim_emb1(j);
        csec_cpl=cprim_emb1(j);
        
        %calcul des vitesses primaire du coupleur
        [ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0,0);
        
        % Gestion demande cprim2_cpl
        % calcul cmt max
        wmt=wprim1_cpl;
        dwmt=dwprim1_cpl;
        cmt_dem=csec_cpl+VD.MOTH.J_mt.*dwmt-pdem_bat/wprim2_cpl*VD.ADCPL.kred/VD.ADCPL.rend;
        cmt_max=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt);
       
        % Si le demande sur cmt < cmt_max, on fournira en plus pdem bat sur
        % l'arbre motelec, sinon on ne fournira que ce que l'on peut voir
        % on fera du boost
        if cmt_dem<=cmt_max
            cprim2_cpl(j)=pdem_bat/wprim2_cpl;
        else
            pelec_dem=pdem_bat+(cmt_dem-cmt_max)*wmt;
            cprim2_cpl(j)=pelec_dem/wprim2_cpl;
        end
        
        % ********************
        %%% calcul dod et verification du mode tout elec sur cacm1 et ibat
        % si on depasse les limites on impose un mode hybride
        %Connexion
        wacm1=wprim2_cpl;
        dwacm1=dwprim2_cpl;
        cacm1=cprim2_cpl(j)+VD.ACM1.J_mg.*dwacm1;
        
        % Calcul des conditions en amont de la machine electrique
        %%% limitations ME
        cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
        
        % Si cacm1>cacm1_max 
        if cacm1>cacm1_max
            'cacm1>cacm1_max en hybride'
            cacm1=cacm1_max;
            cprim2_cpl(j)= cacm1-VD.ACM1.J_mg.*dwacm1;
            pause
        end

        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
        
        % Calcul des auxiliaires electriques
        [ERR,pacc]=calc_acc(VD);
            
        % Puissance reseau electrique
        pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc(j);
        
        [ERR,ibat,ubat,soc,ah,E,R,RdF]=calc_batt(ERR,VD.BATT,VD.CYCL.pas_temps,pres,100-dod(j-1),0,pres,0);
        dod(j)=100-soc;
        if ~isempty(ERR)
            j
            pdem_bat
            ibat
            ubat
            soc
            'depassement limites batterie en hybride'          
            pause            
        end
   
    end
end
if strcmp(lower(param.optim),'ligne')  % lignes situe avant en dehors de cette fonction. a reecrire avec une strucutre r en sortie
    %| strcmp(lower(param.optim),'lagrange')
    
    % Connexion
    wacm1=wprim2_cpl;
    dwacm1=dwprim2_cpl;
    cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;
    
    wsec_emb2=wprim1_cpl;
    dwsec_emb2=dwprim1_cpl;
    csec_emb2=cprim1_cpl;
    
    
    %% Pb revoir calc_emb cas passage elhyb 0 a 1, valeur cmt, wmt si elhyb ==0
    %% et
    [ERR,cprim_emb2,wprim_emb2,dwprim_emb2,etat_emb]=calc_emb2(VD,csec_emb2,wsec_emb2,dwsec_emb2,elhyb);
    
    wmt=wprim_emb2;
    dwmt=dwprim_emb2;
    cmt=cprim_emb2+VD.MOTH.J_mt.*dwmt;
    
    % Calcul des conditions de fonctionnement du moteur thermique
    
    [ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,elhyb,0);
    if ~isempty(ERR)
        disp(ERR.message);
        choix=1; % pour saturer les couples et faire les calculs tout de meme
        if choix==0
            ResXml=struct([]); %             [conso100,co2_gkm,co2_inst]=backward_erreur(length(VD.CYCL.temps),2);
            return;
        elseif choix==1
            indice=find(cmt>interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt));
            cmt(indice)=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt(indice));
            [ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,elhyb,0);
        end
    end
    
    % Calcul des conditions en amont de la machine electrique
    [Li,Co]=size(cacm1);
    wacm1=repmat(wacm1,Li,1);
    
    [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,'1');
    if ~isempty(ERR)
        ResXml=struct([]); %         [ah,soc]=backward_erreur(length(VD.CYCL.temps));
        return;
    end
    
    % Puissance reseau electrique
    pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc;
    % EN tout thermique il faut remettre pres a pacc
    pres(elhyb==2)=pacc(elhyb==2);
    qacm1(elhyb==2)=0;
    
    % calcul batteries
    soc(1)=100-VD.INIT.Dod0;
    ah(1)=0;
    [ERR,~,~,~,ah(1),E(1),R(1),RdF(1)]=calc_batt(ERR,VD.BATT,param.pas_temps,pres(1),soc(1),ah(1),pres,0);
    
    for j=2:length(VD.CYCL.temps)
        [ERR,~,~,~,ah(j),E(j),R(j),RdF(j)]=calc_batt(ERR,VD.BATT,param.pas_temps,pres(j),soc(j-1),ah(j-1),pres(j),0);
    end
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,VD,r]=calc_lagrange_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
%cprim2_cpl,elhyb,ibat,soc_r,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cfrein_meca_pelec_mot,indice_rap_opt

r_var={'ham','lambda','cprim2_cpl','ibat','soc','ubat','Rbat','u0bat','RdF','dcarb','cmt','wmt','dwmt','cacm1','wacm1','dwacm1','qacm1','indice_rap_opt'};
[Li,Co]=size(csec_emb1);
elhyb=ones(1,Co);
for ii=1:length(r_var)-2
    eval(['r.' (r_var{ii}) '=' 'NaN*ones(size(VD.CYCL.temps));' ]);
end
r.lambda(1)=param.lambda;
r.soc(1)=100-VD.INIT.Dod0;
global Ham_hors_limite;
indice_rap_opt=NaN;

for j=1:length(VD.CYCL.temps)

    [ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,repmat(elhyb,[Li 1]));
    % connexion
    wsec_cpl=wprim_emb1;
    dwsec_cpl=dwprim_emb1;
    csec_cpl=cprim_emb1; 
    
    [ERR,r]=solve_hamilt_HP_BV_2EMB(ERR,VD,param,csec_cpl(:,j),wsec_cpl(:,j),dwsec_cpl(:,j),r,j,1,param.lambda_cst);
    if isfield(param,'tout_thermique') && param.tout_thermique==1;
       [ERR,r]=calc_hamilt_therm_HP_BV_2EMB(ERR,VD,param,csec_cpl(:,j),wsec_cpl(:,j),dwsec_cpl(:,j),r,j,1,param.lambda_cst);
    else
       r.ham_therm(j)=Inf;
    end
    
    [ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,0);
   
    % connexion
    wsec_cpl=wprim_emb1;
    dwsec_cpl=dwprim_emb1;
    csec_cpl=cprim_emb1;
    
    
    [ERR,r]=calc_hamilt_elec_HP_BV_2EMB(ERR,VD,param,csec_cpl(:,j),wsec_cpl(:,j),dwsec_cpl(:,j),r,j,1,param.lambda_cst);
    
    if r.ham_hyb(j)<=r.ham_elec(j) && r.ham_hyb(j)<=r.ham_therm(j)
        [ERR,r] = affect_struct_lagrange(r_var,r,'hyb',j);
        r.elhyb(j)=1;
        
    elseif r.ham_elec(j)<=r.ham_hyb(j) && r.ham_elec(j)<=r.ham_therm(j)
        [ERR,r] = affect_struct_lagrange(r_var,r,'elec',j);
        r.elhyb(j)=0;
        
    else
        if isfield(param,'tout_thermique') && param.tout_thermique==1;
            [ERR,r] = affect_struct_lagrange(r_var,r,'therm',j);
            r.elhyb(j)=2;
        end
    end
    
    if r.ham==Ham_hors_limite
        chaine=strcat(' ham = Ham_hors_limite, indice',j);
        ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
    end
    
    if param.verbose>=1
        if rem(j,100)==0 | j==1 | j == length(VD.CYCL.temps)
            fprintf(1,'%s %.1f %s %.2f  %s %.2f %s %.2f \n','calc_HP_BV_2EMB - Indice: ',j,'cprm2',r.cprim2_cpl(j),...
                'Ibat',r.ibat(j),'Ubat',r.ubat(j)./VD.BATT.Nblocser);
        end
    end
end

if VD.CYCL.ntypcin==1 % rapport de boite optimise : on recree les vecteurs a partir des tableaux
    %csec_emb1=csec_emb1(sub2ind(size(csec_emb1),indice_rap_opt_1,1:length(indice_rap_opt_1)));
    %wsec_emb1=wsec_emb1(sub2ind(size(wsec_emb1),indice_rap_opt_1,1:length(indice_rap_opt_1)));
    %dwsec_emb1=dwsec_emb1(sub2ind(size(dwsec_emb1),indice_rap_opt_1,1:length(indice_rap_opt_1)));
    %elec_pos=elec_pos(sub2ind(size(elec_pos),indice_rap_opt_1,1:length(indice_rap_opt_1)));
    VD.CYCL.rappvit=indice_rap_opt-1;
    VD.CYCL.ntypcin=3;
    assignin('base','indice_rap_opt',indice_rap_opt)
end


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,VD,r]=calc_lagrange_cycle_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);

r_var={'elhyb','ham','lambda','cprim2_cpl','ibat','soc','ubat','Rbat','u0bat','RdF','dcarb','cmt','wmt','dwmt','cacm1','wacm1','dwacm1','qacm1','indice_rap_opt'};
soc_ini=100-VD.INIT.Dod0;

[Li,Co]=size(csec_emb1);
elhyb_hyb=ones(1,Co);

[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,repmat(elhyb_hyb,[Li 1]));
wsec_cpl=wprim_emb1;
dwsec_cpl=dwprim_emb1;
csec_cpl=cprim_emb1;

wmt_hyb=max(wsec_cpl,VD.MOTH.ral);
wacm1_hyb=wsec_cpl*VD.ADCPL.kred;
dwacm1_hyb=dwsec_cpl*VD.ADCPL.kred;

wacm1_elec=wsec_emb1*VD.ADCPL.kred;
dwacm1_elec=dwsec_emb1*VD.ADCPL.kred;

% calcul des couple max a chaque pas de temps
cmt_max_vec=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt_hyb);
cacm1_max_vec_hyb=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,end),wacm1_hyb);
cacm1_min_vec_hyb=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,end),wacm1_hyb);

cacm1_max_vec_elec=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,end),wacm1_elec);
cacm1_min_vec_elec=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,end),wacm1_elec);

%% Calcul des bimt cimt de la carto MOTH
cmti_mat0=repmat(VD.MOTH.Cpl_2dconso(1:end-1)',[1 Co]); % matrice des couples correspondant a la discretisation de la carto moth
wmti_mat=repmat(VD.MOTH.Reg_2dconso',[1 Co]); % matrice des vitesses correspondant a la discretisation de la carto moth
wmt_mat=repmat(wmt_hyb,[length(VD.MOTH.Reg_2dconso) 1]); % matrice des vitesses MOTH 
[a,ii]=min(wmt_mat>wmti_mat); % la vitesse est entre les indice ii et ii-1 de VD.MOTH.Reg_2dconso
ii=ii-1;
ii(ii<1)=1; % la vitesse est entre les indice ii et ii+1 de VD.MOTH.Reg_2dconso

dwmt=wmt_hyb-VD.MOTH.Reg_2dconso(ii);
Dwmt=VD.MOTH.Reg_2dconso(ii+1)-VD.MOTH.Reg_2dconso(ii);
Dcmt=diff(VD.MOTH.Cpl_2dconso);

cimt=NaN*ones([length(VD.MOTH.Cpl_2dconso)-1 length(wsec_cpl)]);
bimt=NaN*ones([length(VD.MOTH.Cpl_2dconso)-1 length(wsec_cpl)]);

for jj=1:Co
    cimt(:,jj)=((diff(VD.MOTH.Conso_2d(ii(jj),:))).*(1-dwmt(jj)/Dwmt(jj))+(diff(VD.MOTH.Conso_2d(ii(jj)+1,:))).*dwmt(jj)/Dwmt(jj))./Dcmt;
    bimt(:,jj)=VD.MOTH.Conso_2d(ii(jj),1:end-1)*(1-dwmt(jj)/Dwmt(jj))+VD.MOTH.Conso_2d(ii(jj)+1,1:end-1)*(dwmt(jj)/Dwmt(jj));
end

bimt=bimt-cimt.*cmti_mat0;

%% Calcul des bime cime de la carto motelec

cacm1i_mat0=repmat(VD.ACM1.Cmot_pert',[1 Co]);
wacm1i_mat=repmat(VD.ACM1.Regmot_pert',[1 Co]); % matrice des vitesses correspondant a la discretisation de la carto acm1
wacm1_hyb_mat=repmat(wacm1_hyb,[length(VD.ACM1.Regmot_pert) 1]); % matrice des vitesses ACM1
[a,ii]=min(wacm1_hyb_mat>wacm1i_mat); % la vitesse est entre mes indice ii et ii-1 de VD.MOTH.Reg_2dconso
ii=ii-1;
ii(ii<1)=1; % la vitesse est entre les indice ii et ii+1 de VD.MOTH.Reg_2dconso

dwacm1_hyb_c=wacm1_hyb-VD.ACM1.Regmot_pert(ii);
Dwacm1=VD.ACM1.Regmot_pert(ii+1)-VD.ACM1.Regmot_pert(ii);
Dcacm1=diff(VD.ACM1.Cmot_pert);

ciacm1_hyb=NaN*ones([length(VD.ACM1.Cmot_pert)-1 length(wsec_cpl)]);
biacm1_hyb=NaN*ones([length(VD.ACM1.Cmot_pert)-1 length(wsec_cpl)]);

for jj=1:length(wacm1_hyb)
    ciacm1_hyb(:,jj)=((diff(VD.ACM1.Pert_mot(ii(jj),:))).*(1-dwacm1_hyb_c(jj)/Dwacm1(jj))+(diff(VD.ACM1.Pert_mot(ii(jj)+1,:))).*dwacm1_hyb_c(jj)/Dwacm1(jj))./Dcacm1;
    biacm1_hyb(:,jj)=VD.ACM1.Pert_mot(ii(jj),1:end-1)*(1-dwacm1_hyb_c(jj)/Dwacm1(jj))+VD.ACM1.Pert_mot(ii(jj)+1,1:end-1)*(dwacm1_hyb_c(jj)/Dwacm1(jj));
end

biacm1_hyb=biacm1_hyb-ciacm1_hyb.*cacm1i_mat0(1:end-1,:);


wacm1_elec_mat=repmat(wacm1_elec,[length(VD.ACM1.Regmot_pert) 1]); % matrice des vitesses ACM1
[a,ii]=min(wacm1_elec_mat>wacm1i_mat); % la vitesse est entre mes indice ii et ii-1 de VD.MOTH.Reg_2dconso
ii=ii-1;
ii(ii<1)=1; % la vitesse est entre les indice ii et ii+1 de VD.MOTH.Reg_2dconso

dwacm1_elec_c=wacm1_elec-VD.ACM1.Regmot_pert(ii);

ciacm1_elec=NaN*ones([length(VD.ACM1.Cmot_pert)-1 length(wsec_cpl)]);
biacm1_elec=NaN*ones([length(VD.ACM1.Cmot_pert)-1 length(wsec_cpl)]);

for jj=1:length(wacm1_elec)
    ciacm1_elec(:,jj)=((diff(VD.ACM1.Pert_mot(ii(jj),:))).*(1-dwacm1_elec_c(jj)/Dwacm1(jj))+(diff(VD.ACM1.Pert_mot(ii(jj)+1,:))).*dwacm1_elec_c(jj)/Dwacm1(jj))./Dcacm1;
    biacm1_elec(:,jj)=VD.ACM1.Pert_mot(ii(jj),1:end-1)*(1-dwacm1_elec_c(jj)/Dwacm1(jj))+VD.ACM1.Pert_mot(ii(jj)+1,1:end-1)*(dwacm1_elec_c(jj)/Dwacm1(jj));
end

biacm1_elec=biacm1_elec-ciacm1_elec.*cacm1i_mat0(1:end-1,:);

% calcul des couple en mode tout elec
cacm1_elec=csec_cpl./(VD.ADCPL.rend.^sign(csec_cpl))/VD.ADCPL.kred+VD.ACM1.J_mg.*dwacm1_elec;

% Calcul des couple max/min machine elec fonction limitations (rajouter les limitations batteries)
cacm1_min_vec_hyb=max(cacm1_min_vec_hyb,(csec_cpl-cmt_max_vec-VD.MOTH.J_mt.*dwsec_cpl)/VD.ADCPL.rend/VD.ADCPL.kred+VD.ACM1.J_mg.*dwacm1_hyb);

cmt_max_vec=min(cmt_max_vec,csec_cpl-(cacm1_min_vec_hyb-VD.ACM1.J_mg.*dwacm1_hyb)*VD.ADCPL.rend*VD.ADCPL.kred+VD.MOTH.J_mt*dwsec_cpl);
cmt_min_vec=max(0,csec_cpl-(cacm1_max_vec_hyb-VD.ACM1.J_mg.*dwacm1_hyb)*VD.ADCPL.rend*VD.ADCPL.kred+VD.MOTH.J_mt*dwsec_cpl);

cacm1_max_vec_hyb=min(cacm1_max_vec_hyb,cacm1_elec); %% le couple max acm1 ne pourra jamais d?passer sa valeur en electrique, on peut supprimer les points au d?l?

%% calcul des points "d"inflexion" coorespondant a carto moth et carto MELEC ramene a la carto MOTH
cacm1i_mat=cacm1i_mat0;


% On vient limiter ces valeurs par cacm1_max min
[sx,sy]=size(cacm1i_mat);
imax=ones([1 Co]);
imin=ones([1 Co]);

for ii=1:Co
    imax(ii)=find(cacm1i_mat(:,ii)<cacm1_max_vec_hyb(ii),1,'last');
    imin(ii)=find(cacm1i_mat(:,ii)>cacm1_min_vec_hyb(ii),1,'first');
end
Imax_min=find( cacm1i_mat>repmat(cacm1_max_vec_hyb,[sx 1]) | cacm1i_mat<repmat(cacm1_min_vec_hyb,[sx 1]) );
cacm1i_mat(Imax_min)=NaN;

Ico=(1:1:length(VD.CYCL.temps));
imin_lin=(imin-1)+(Ico-1)*sx;
imin_lin(imin==1)=[];
cacm1i_mat(imin_lin)=cacm1_min_vec_hyb(imin~=1);

imax_lin=(imax+1)+(Ico-1)*sx;
imax_lin(imin==sx)=[];
cacm1i_mat(imax_lin)=cacm1_max_vec_hyb(imax~=sx);

%On calcule les couples moth correspondant aux point de carto motelec
csec_cpl_cacm1i=repmat(csec_cpl,[length(VD.ACM1.Cmot_pert) 1]);
wsec_cpl_cacm1i=repmat(wsec_cpl,[length(VD.ACM1.Cmot_pert) 1]);
dwsec_cpl_cacm1i=repmat(dwsec_cpl,[length(VD.ACM1.Cmot_pert) 1]);
cprim2_cpl_cacm1i=cacm1i_mat-VD.ACM1.J_mg.*repmat(dwacm1_hyb,[length(VD.ACM1.Cmot_pert) 1]);

[ERR,cprim1_cpl_cacm1i,wprim1_cpl_cacm1i,dwprim1_cpl_cacm1i,wprim2_cpl_cacm1i,dwprim2_cpl_cacm1i,cprim2_cpl_cacm1i]=...
            calc_adcpl(ERR,VD.ADCPL,csec_cpl_cacm1i,wsec_cpl_cacm1i,dwsec_cpl_cacm1i,cprim2_cpl_cacm1i,0,1);
cmti_cacm1i=cprim1_cpl_cacm1i+VD.MOTH.J_mt.*repmat(dwsec_cpl,[length(VD.ACM1.Cmot_pert) 1]);

I=find(sum(isnan(cacm1i_mat'))~=sy); % on cherche les lignes ou il n'y as que des NaN dans cacm1i_mat
% On les uspprime

cmti=sort([cmti_mat0; cmti_cacm1i(I(1):I(end),:)]);
[a,b]=size(cmti);
cmti=min(cmti,repmat(cmt_max_vec,[a 1]));

for ii=1:length(VD.CYCL.temps)
   ind=find(cmti(:,ii)==cmt_max_vec(ii),1,'first');
   if ind+1<a
   cmti(ind+1:end,ii)=NaN;
   end
end

I=find(sum(isnan(cmti'))~=sy);
cmti=cmti(1:I(end),:);

% Recalcule des couple acm1 corespondant
[sxi,syi]=size(cmti);

csec_cpli=repmat(csec_cpl,[sxi 1]);
wsec_cpli=repmat(wsec_cpl,[sxi 1]);
dwsec_cpli=repmat(dwsec_cpl,[sxi 1]);

cprim1_cpli=cmti-VD.MOTH.J_mt.*dwsec_cpli;

[ERR,cprim1_cpli,wprim1_cpli,dwprim1i_cpli,wprim2_cpli,dwprim2_cpli,cprim2_cpli]=...
    calc_adcpl(ERR,VD.ADCPL,csec_cpli,wsec_cpli,dwsec_cpli,cprim1_cpli,0,2);

cacm1i=cprim2_cpli+VD.ACM1.J_mg.*dwprim2_cpli;
cacm1i=round(cacm1i*10^12)/10^12; % on ecrete les dernieres decimales pour des pb d'arrondie
wacm1i=wprim2_cpli;
dwacm1i=dwprim2_cpli;

% On vient trouver les bi ci correspondant aux cmti 
sz=length(VD.MOTH.Cpl_2dconso);
cmti_3D=repmat(cmti,[1 1 sz]);
cmt_3D=repmat(reshape(VD.MOTH.Cpl_2dconso,[1 1 length(VD.MOTH.Cpl_2dconso)]),[sxi Co 1]);

[~,Ic]=max(cmti_3D<cmt_3D,[],3);
Ic=Ic-1;
Ic(Ic==0)=1;
[bimt_mat,cimt_mat]=mat_ind(Ic,3,repmat(reshape(bimt',[1 sy sz-1]),[sxi 1 1]),repmat(reshape(cimt',[1 sy sz-1]),[sxi 1 1]));

%mfi=bimt_mat+cimt_mat.*cmti;
%[ERR,dcarb]=calc_mt(ERR,VD.MOTH,cmti,repmat(wmt,[sxi 1]),repmat(dwsec_cpl,[sxi 1]),1,0,0,param.fit_pertes_moth);


% On vient trouver les biacm1 ciacm1 correspondant aux cacm1i 
sz=length(VD.ACM1.Cmot_pert);
cacm1i_3D=repmat([cacm1i; cacm1_elec],[1 1 sz]); % On rajoute le mode elec en dernier ligne
cacm1_3D=repmat(reshape(VD.ACM1.Cmot_pert,[1 1 length(VD.ACM1.Cmot_pert)]),[sxi+1 Co 1]);

[~,Ic]=max(cacm1i_3D<=cacm1_3D,[],3);
Ic=Ic-1;
Ic(Ic==0)=1;
[biacm1_mat_hyb,ciacm1_mat_hyb]=mat_ind(Ic(1:end-1,:),3,repmat(reshape(biacm1_hyb',[1 sy sz-1]),[sxi 1 1]),repmat(reshape(ciacm1_hyb',[1 sy sz-1]),[sxi 1 1]));

% on rajoute le cas elec

[biacm1_mat_elec,ciacm1_mat_elec]=mat_ind(Ic(end,:),2,biacm1_elec,ciacm1_elec);

biacm1_mat=[biacm1_mat_hyb; biacm1_mat_elec];
ciacm1_mat=[ciacm1_mat_hyb; ciacm1_mat_elec];
%qacm1_elec=biacm1_mat_elec+ciacm1_mat_elec.*[ cacm1_elec];
%[ERR,qacm1_elec1]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1_elec,dwacm1_elec,0,param.fit_pertes_acm);
qacm1_mat=[biacm1_mat_hyb+ciacm1_mat_hyb.*cacm1i ; biacm1_mat_elec+ciacm1_mat_elec.*cacm1_elec];

%% Calcul des points singulier
[ERR,pacc]=calc_acc(VD);

SPmeca_acm1=((repmat(csec_cpl,[sxi 1])-cmti+VD.MOTH.J_mt.*dwprim1i_cpli)./(VD.ADCPL.kred)+VD.ACM1.J_mg.*dwprim2_cpli);  
SP=sign(SPmeca_acm1);
SP(SP==0)=1;
SP(isnan(SP))=1;

Pbatt=cacm1i.*wacm1i+biacm1_mat(1:end-1,:)+ciacm1_mat(1:end-1,:).*cacm1i+pacc(1);
[ERR,ibat_mat,~,~,~,Ebatt,R_mat,RdF_mat]=calc_batt(ERR,VD.BATT,param.pas_temps,Pbatt,soc_ini,0,-1);
P0batt_hyb=RdF_mat.*ibat_mat.*Ebatt;

Kcpl=VD.ADCPL.kred.*VD.ADCPL.rend.^SP;
A=Ebatt.^2 - 4*R_mat.* (pacc(1) + biacm1_mat(1:end-1,:))- 4*R_mat.*(wacm1i+ciacm1_mat(1:end-1,:)).*((csec_cpli+VD.MOTH.J_mt.*dwsec_cpli)./Kcpl+VD.ACM1.J_mg.*dwacm1i);

% calcul hamiltonien elec

Pbatt_elec=pacc(1)+biacm1_mat(end,:)+(ciacm1_mat(end,:)+wacm1_elec).*cacm1_elec;

[ERR,I_elec,~,~,~,Ebatt,R_elec,RdF_elec]=calc_batt(ERR,VD.BATT,param.pas_temps,Pbatt_elec,soc_ini,0,-1);
P0batt_elec=RdF_elec./(2*R_elec).*(Ebatt^2-Ebatt*sqrt(Ebatt^2-4*R_elec.*Pbatt_elec));


% calcul des p "singulier"
wacm1i_mat=repmat(wacm1_hyb,[sxi 1]);
Telec=VD.BATT.RdFarad*(biacm1_mat(end,:)+ciacm1_mat(end,:).*cacm1_elec+cacm1_elec.*wacm1_elec);

p_mat3=(VD.MOTH.pci*(bimt_mat+cimt_mat.*cmti))./(repmat(P0batt_elec,[sxi +1])-P0batt_hyb);
p_mode=min(p_mat3(3:end-1,:)); % singularit? de mode attention pb sur les indice min max
pelec_hyb=unique(p_mode(p_mode>0)); % ensemble des p qui feront basculer un ou plusieurs points du cycle de elec ? hyb 


%% on calcul les hamiltonien sur cmti et pmode : tableau 3D
mfi=bimt_mat+cimt_mat.*cmti;
T_conso=VD.MOTH.pci*(mfi);
T_pbatt=VD.BATT.RdFarad*(pacc(1)+biacm1_mat+ciacm1_mat.*[cacm1i; cacm1_elec]+[cacm1i; cacm1_elec].*[wacm1i; wacm1_elec]);
T_conso=[T_conso; zeros(size(VD.CYCL.temps))];

sx_elhyb=length(pelec_hyb);
pbatt_3D=repmat([P0batt_hyb; P0batt_elec],[1 1 sx_elhyb]);

[ERR,ibat_mat,ubat_mat,soc_mat,ah_mat,E_mat,R_mat,RdF_mat]=calc_batt(ERR,VD.BATT,param.pas_temps,T_pbatt,soc_ini,0,-1);
dsoc_mat=-soc_ini+soc_mat;

sy=length(VD.CYCL.temps);
Ham_3D=repmat(T_conso,[1 1 sx_elhyb])+repmat(reshape(pelec_hyb,[1 1 sx_elhyb]),[sxi+1 sy 1]).*pbatt_3D;

[m_Ham_3D,i_min]=min(Ham_3D,[],1);
i_min=reshape(i_min,[sy sx_elhyb])';
m_Ham_3D=reshape(m_Ham_3D,[sy sx_elhyb])';

elhyb_mat=repmat(pelec_hyb',[1 sy])>repmat(p_mode,[sx_elhyb 1]) & repmat(p_mode,[sx_elhyb 1])>0;
%si 0 je suis en elec si 1 je suis en hybride donc il me faut cmti etc

dsoc=repmat(dsoc_mat(end,:),[sx_elhyb 1]);
Ihyb=find(elhyb_mat==1);

d2=length(VD.CYCL.temps);
d1=sx_elhyb;
Tco=repmat((1:1:length(VD.CYCL.temps)),[d1 1]);
Ilin=(sxi+1)*(Tco-1)+i_min;

dsoc_mat1=dsoc_mat(Ilin);
dsoc(Ihyb)=dsoc_mat1(Ihyb);

Dsoc=sum(dsoc,2);

cmti_c=[cmti; zeros([1 sy])];
cacm1i_c=[cacm1i; cacm1_elec];

%% recherche du p qui mene au Dsoc_fin que l'on cherche
I=find(Dsoc<param.Dsoc_fin,1,'last');
p1=pelec_hyb(I);
p2=pelec_hyb(I+1);
dsoc_p1=dsoc(I,:);
dsoc_p2=dsoc(I+1,:);

soc_p1=cumsum(dsoc(I,:));
soc_p2=cumsum(dsoc(I+1,:));

prec=0.1;
if abs(Dsoc(I)-param.Dsoc_fin)>prec
   %% On cherche combien de changement de mode pour le p consid?rer et les Dosc correspondant
   Imode_p1=find(p_mode==pelec_hyb(I));
   dsoc_Imode_p1=dsoc(I+1,Imode_p1)-dsoc(I,Imode_p1); % diff entre les Dsoc elec et hyb sur les points ou changement de mode en p1
   
   % on peut calculer le nombre de points qui doivent basculer d'elec ? hyb
   % Nb_electohyb=find(cumsum(dsoc_Imode_p1)<abs(Dsoc(I)-param.Dsoc_fin),1,'last');
   
   [~,Nb_electohyb]=min(abs(cumsum(dsoc_Imode_p1)-abs(Dsoc(I)-param.Dsoc_fin)));
   % et les indices correspondant
   I_electohyb=Imode_p1(1:Nb_electohyb);
   I_electohyb=Imode_p1(end-Nb_electohyb+1:end);
   dsoc_p1_c=dsoc_p1;
   dsoc_p1_c(I_electohyb)=dsoc_p2(I_electohyb); % en fait c'est pas compl?tement juste il faudrait recalculer les dsoc correspondant au prochain p de singularite de carto 
   soc_p1_c=cumsum(dsoc_p1_c);
end
Idiff=find(dsoc_p1~=dsoc_p2 & p_mode~=pelec_hyb(I) );


%% recalcul des grandeurs
if param.cherche_min~=1
    elhyb=double(elhyb_mat(I,:));
    elhyb(I_electohyb)=1;
    ham=m_Ham_3D(I,:);
    ham(I_electohyb)=m_Ham_3D(I+1,I_electohyb);
    lambda=p1*ones([1 sy]);
    
    ibat=repmat(ibat_mat(end,:),[sx_elhyb 1]);
    ibat_mat1=ibat_mat(Ilin);
    ibat(Ihyb)=ibat_mat1(Ihyb);
    ibat=ibat(I,:);
    ibat(I_electohyb)=ibat_mat1(I+1,I_electohyb);
    
    ubat=repmat(ubat_mat(end,:),[sx_elhyb 1]);
    ubat_mat1=ubat_mat(Ilin);
    ubat(Ihyb)=ubat_mat1(Ihyb);
    ubat=ubat(I,:);
    ubat(I_electohyb)=ubat_mat1(I+1,I_electohyb);
    
    Rbat=repmat(R_mat(end,:),[sx_elhyb 1]);
    R_mat1=R_mat(Ilin);
    Rbat(Ihyb)=R_mat1(Ihyb);
    Rbat=Rbat(I,:);
    Rbat(I_electohyb)=R_mat1(I+1,I_electohyb);
    
    RdF=repmat(RdF_mat(end,:),[sx_elhyb 1]);
    RdF_mat1=RdF_mat(Ilin);
    RdF(Ihyb)=RdF_mat1(Ihyb);
    RdF=RdF(I,:);
    RdF(I_electohyb)=RdF_mat1(I+1,I_electohyb);
    
    u0bat=E_mat*ones([1 sy]);
    
    mfi_mat=[mfi; zeros([1 sy])];
    mfi=repmat(mfi_mat(end,:),[sx_elhyb 1]);
    mfi_mat1=mfi_mat(Ilin);
    mfi(Ihyb)=mfi_mat1(Ihyb);
    mfi=mfi(I,:);
    mfi(I_electohyb)=mfi_mat1(I+1,I_electohyb);
    dcarb=mfi;
    
    
    %cmt=cmti_c(Ilin(I,:));
    cmt=repmat(cmti_c(end,:),[sx_elhyb 1]);
    cmt_mat1=cmti_c(Ilin);
    cmt(Ihyb)=cmt_mat1(Ihyb);
    cmt=cmt(I,:);
    cmt(I_electohyb)=cmt_mat1(I+1,I_electohyb);
    
    wmt=wmt_hyb;
    wmt(elhyb==0)=0;
    dwmt=dwsec_cpl;
    dwmt(elhyb==0)=0;
    
    cacm1=repmat(cacm1i_c(end,:),[sx_elhyb 1]);
    cacm1_mat1=cacm1i_c(Ilin);
    cacm1(Ihyb)=cacm1_mat1(Ihyb);
    cacm1=cacm1(I,:);
    cacm1(I_electohyb)=cacm1_mat1(I+1,I_electohyb);
    
    wacm1=wacm1_hyb;
    wacm1(elhyb==0)=wacm1_elec(elhyb==0);
    dwacm1=dwacm1_hyb;
    dwacm1(elhyb==0)=dwacm1_elec(elhyb==0);
    
    qacm1=repmat(qacm1_mat(end,:),[sx_elhyb 1]);
    qacm1_mat1=qacm1_mat(Ilin);
    qacm1(Ihyb)=qacm1_mat1(Ihyb);
    qacm1=qacm1(I,:);
    qacm1(I_electohyb)=qacm1_mat1(I+1,I_electohyb);
    
    cprim2_cpl=cacm1-VD.ACM1.J_mg*dwacm1;
    indice_rap_opt=NaN*ones([1 sy]);
    
    soc=soc_p1_c+soc_ini-soc_p1_c(1);
end
%r_var={'ham','lambda','cprim2_cpl','ibat','soc','ubat','Rbat','Ebat','RdF','dcarb','cmt','wmt','dwmt','cacm1','wacm1','dwacm1','qacm1','indice_rap_opt'}

%% si l'on veut considerer les min possible du aux leger arrondies sur les segment du hamiltonien

if param.cherche_min==1
    %% on cherche p tq H_cmti_min=Ham_elec
    RdF_mat=RdF_mat(1:end-1,:);
    R_mat=R_mat(1:end-1,:);
    %a1=( VD.MOTH.pci*(cimt_mat.*((RdF_mat.*Ebatt.*(wacm1i+ciacm1_mat(1:end-1,:))./(Kcpl*VD.MOTH.pci.*cimt_mat)).^2).*Kcpl./(4*R_mat.*(wacm1i+ciacm1_mat(1:end-1,:))))...
    %    - RdF_mat.^2.*Ebatt^2./(2*R_mat).*(wacm1i+ciacm1_mat(1:end-1,:))./(Kcpl*VD.MOTH.pci.*cimt_mat) );
   
    a1=((RdF_mat.*Ebatt).^2).*(wacm1i+ciacm1_mat(1:end-1,:)) ./ (4*R_mat.*Kcpl*VD.MOTH.pci.*cimt_mat)...
        - RdF_mat.^2.*Ebatt^2./(2*R_mat).*(wacm1i+ciacm1_mat(1:end-1,:))./(Kcpl*VD.MOTH.pci.*cimt_mat) ;
    
    b1=RdF_mat.*Ebatt^2./(2*R_mat)-repmat(P0batt_elec,[sxi 1]);
    c1=VD.MOTH.pci*(bimt_mat-cimt_mat.*A.*Kcpl./(4*R_mat.*(wacm1i+ciacm1_mat(1:end-1,:))));
    
    delta=b1.^2-4*a1.*c1;
    %pm1=(-b1+sqrt(delta))./(2*a1); correspond a des cmti_min<0
    pm2=(-b1-sqrt(delta))./(2*a1);
    
    cmti_min_pm2=((pm2.*RdF_mat.*Ebatt.*(wacm1i+ciacm1_mat(1:end-1,:))./(Kcpl*VD.MOTH.pci.*cimt_mat)).^2-A).*Kcpl./(4*R_mat.*(wacm1i+ciacm1_mat(1:end-1,:)));
    cmti_min_pm2(delta<0)=NaN;
    test=cmti_min_pm2 > cmti & cmti_min_pm2 < [cmti(2:end,:); Inf*ones([1 sy])];
    I_pm2=find(test==1); % On vient trouver les cas ou les couple min est dans l'intervalle cmti cmti+1
    
    pm3=pm2;
    pm3(test~=1)=NaN;
    [p_mode_pm3,Imin_pm3]=min(pm3);
    I_pm3=find(p_mode_pm3<p_mode);
    
    %% pour les instant correpsondant ? I_pm3 le changment de mode se fera entre deux cmti
    % il faut rajouter les couple correpondant dans cmti et remplacer les
    % p_mode correspondant par les p_mode_pm3
    % (franchement c'est de la sodomie de diptere)
    p_mode_sup=p_mode_pm3(I_pm3);
    Li_sup=Imin_pm3(I_pm3);
    Co_sup=I_pm3;
    
    cmti_sup=cmti_min_pm2(Li_sup+(Co_sup-1)*sxi);
    [~,sxisup]=size(cmti_sup);
    
    
    p_modeb=p_mode;
    p_modeb(Co_sup)=p_mode_sup;
    
    pelec_hybb=unique(p_modeb(p_modeb>0));
    
    cmtib=sort([cmti; repmat(unique(cmti_sup)',[1 sy])]);
    [a,b]=size(cmtib);
    cmtib=min(cmtib,repmat(cmt_max_vec,[a 1]));
    
    
    %% on vient recalculer les cacm1 corrspondant et les tableau bimt cimt biacm1 ciacm1
    % On recalcule les cacm1i correspondant
    [sxib,syib]=size(cmtib);
    
    csec_cplib=repmat(csec_cpl,[sxib 1]);
    wsec_cplib=repmat(wsec_cpl,[sxib 1]);
    dwsec_cplib=repmat(dwsec_cpl,[sxib 1]);
    
    cprim1_cplib=cmtib-VD.MOTH.J_mt.*dwsec_cplib;
    
    [ERR,cprim1_cplib,wprim1_cplib,dwprim1i_cplib,wprim2_cplib,dwprim2_cplib,cprim2_cplib]=...
        calc_adcpl(ERR,VD.ADCPL,csec_cplib,wsec_cplib,dwsec_cplib,cprim1_cplib,0,2);
    
    cacm1ib=cprim2_cplib+VD.ACM1.J_mg.*dwprim2_cplib;
    cacm1ib=round(cacm1ib*10^12)/10^12; % on ecrete les dernieres decimales
    wacm1ib=wprim2_cplib;
    dwacm1ib=dwprim2_cplib;
    
    %% On vient trouver les bi ci correspondant aux cmti
    sz=length(VD.MOTH.Cpl_2dconso);
    cmti_3Db=repmat(cmtib,[1 1 sz]);
    cmt_3Db=repmat(reshape(VD.MOTH.Cpl_2dconso,[1 1 length(VD.MOTH.Cpl_2dconso)]),[sxib sy 1]);
    
    [~,Icb]=max(cmti_3Db<cmt_3Db,[],3);
    Icb=Icb-1;
    Icb(Icb==0)=1;
    [bimt_matb,cimt_matb]=mat_ind(Icb,3,repmat(reshape(bimt',[1 sy sz-1]),[sxib 1 1]),repmat(reshape(cimt',[1 sy sz-1]),[sxib 1 1]));
    
    mfib=bimt_matb+cimt_matb.*cmtib;
    %[ERR,dcarbb]=calc_mt(ERR,VD.MOTH,cmtib,repmat(wmt,[sxib 1]),repmat(dwsec_cpl,[sxib 1]),1,0,0,param.fit_pertes_moth);
    
    
    %% On vient trouver les biacm1 ciacm1 correspondant aux cacm1i
    sz=length(VD.ACM1.Cmot_pert);
    cacm1i_3Db=repmat([cacm1ib; cacm1_elec],[1 1 sz]); % On rajoute le mode elec en dernier ligne
    cacm1_3Db=repmat(reshape(VD.ACM1.Cmot_pert,[1 1 length(VD.ACM1.Cmot_pert)]),[sxib+1 Co 1]);
    
    [~,Icb]=max(cacm1i_3Db<=cacm1_3Db,[],3);
    Icb=Icb-1;
    Icb(Icb==0)=1;
    [biacm1_matb_hyb,ciacm1_matb_hyb]=mat_ind(Icb(1:end-1,:),3,repmat(reshape(biacm1_hyb',[1 sy sz-1]),[sxib 1 1]),repmat(reshape(ciacm1_hyb',[1 sy sz-1]),[sxib 1 1]));

    % on rajoute le cas elec
    
    [biacm1_matb_elec,ciacm1_matb_elec]=mat_ind(Icb(end,:),2,biacm1_elec,ciacm1_elec);
    
    biacm1_matb=[biacm1_matb_hyb; biacm1_matb_elec];
    ciacm1_matb=[ciacm1_matb_hyb; ciacm1_matb_elec];
    %qacm1b=biacm1_matb+ciacm1_matb.*[cacm1ib; cacm1_elec];
    %[ERR,qacm1_matb]=calc_pertes_acm(ERR,VD.ACM1,cacm1ib,repmat(wacm1,[sxib 1]),repmat(dwacm1_c,[sxib 1]),0,param.fit_pertes_acm);
    qacm1_mat=[biacm1_matb_hyb+ciacm1_matb_hyb.*cacm1ib ; biacm1_matb_elec+ciacm1_matb_elec.*cacm1_elec];
    
    %% on calcul les hamiltonien sur cmtib et pmodeb : tableau 3D
    T_consob=VD.MOTH.pci*(bimt_matb+cimt_matb.*cmtib);
    T_pbattb=VD.BATT.RdFarad*(pacc(1)+biacm1_matb+ciacm1_matb.*[cacm1ib; cacm1_elec]+[cacm1ib; cacm1_elec].*[wacm1ib; wacm1_elec]);
    T_consob=[T_consob; zeros(size(VD.CYCL.temps))];
    
    SPmeca_acm1=((repmat(csec_cpl,[sxib 1])-cmtib+VD.MOTH.J_mt.*dwprim1i_cplib)./(VD.ADCPL.kred)+VD.ACM1.J_mg.*dwprim2_cplib);
    SP=sign(SPmeca_acm1);
    SP(SP==0)=1;
    SP(isnan(SP))=1;
    
    Pbattb=cacm1ib.*wacm1ib+biacm1_matb(1:end-1,:)+ciacm1_matb(1:end-1,:).*cacm1ib+pacc(1);
    
    
    [ERR,ibat_matb,~,~,~,Ebattb,R_matb,RdF_matb]=calc_batt(ERR,VD.BATT,param.pas_temps,Pbattb,soc_ini,0,-1);
    P0batt_hybb=RdF_matb.*ibat_matb.*Ebattb;
    sx_elhybb=length(pelec_hybb);
    pbatt_3Db=repmat([P0batt_hybb; P0batt_elec],[1 1 sx_elhybb]);
    
    
    [ERR,ibat_matb,ubat_matb,soc_matb,ah_matb,E_matb,R_matb,RdF_matb]=calc_batt(ERR,VD.BATT,param.pas_temps,T_pbattb,soc_ini,0,-1);
    dsoc_matb=-soc_ini+soc_matb;
    
    sy=length(VD.CYCL.temps);
    Ham_3Db=repmat(T_consob,[1 1 sx_elhybb])+repmat(reshape(pelec_hybb,[1 1 sx_elhybb]),[sxib+1 sy 1]).*pbatt_3Db;
    [m_Ham_3Db,i_minb]=min(Ham_3Db,[],1);
    i_minb=reshape(i_minb,[sy sx_elhybb])';
    m_Ham_3Db=reshape(m_Ham_3Db,[sy sx_elhybb])';
    elhyb_matb=repmat(pelec_hybb',[1 sy])>repmat(p_modeb,[sx_elhybb 1]) & repmat(p_modeb,[sx_elhybb 1])>0;
    %si 0 je suis en elec si 1 je suis en hybride donc il me faut cmti etc
    
    dsocb=repmat(dsoc_matb(end,:),[sx_elhybb 1]);
    Ihybb=find(elhyb_matb==1);
    
    d2=length(VD.CYCL.temps);
    d1=sx_elhybb;
    Tco=repmat((1:1:length(VD.CYCL.temps)),[d1 1]);
    Ilin=(sxib+1)*(Tco-1)+i_minb;
    
    dsoc_mat1b=dsoc_matb(Ilin);
    dsocb(Ihybb)=dsoc_mat1b(Ihybb);
    
    %Dsoc=sum(dsoc');
    Dsocb=sum(dsocb,2);
    
    cmti_cb=[cmtib; zeros([1 sy])];
    cacm1i_cb=[cacm1ib; cacm1_elec];

    I=find(Dsoc<param.Dsoc_fin,1,'last');
    p1=pelec_hybb(I);
    p2=pelec_hybb(I+1);
    dsoc_p1b=dsocb(I,:);
    dsoc_p2b=dsocb(I+1,:);
    
    soc_p1b=cumsum(dsocb(I,:));
    soc_p2b=cumsum(dsocb(I+1,:));
    
    prec=0.1;
    if abs(Dsocb(I)-param.Dsoc_fin)>prec
        %% On cherche combien de changement de mode pour le p considerer et les Dosc correspondant
        Imode_p1b=find(p_modeb==pelec_hybb(I));
        dsoc_Imode_p1b=dsocb(I+1,Imode_p1)-dsoc(I,Imode_p1b); % diff entre les Dsoc elec et hyb sur les points ou changement de mode en p1
        
        % on peut calculer le nombre de points qui doivent basculer d'elec ? hyb
        % Nb_electohyb=find(cumsum(dsoc_Imode_p1)<abs(Dsoc(I)-param.Dsoc_fin),1,'last');
        
        [~,Nb_electohybb]=min(abs(cumsum(dsoc_Imode_p1b)-abs(Dsocb(I)-param.Dsoc_fin)));
        % et les indices correspondant
        I_electohybb=Imode_p1b(1:Nb_electohybb);
        dsoc_p1_cb=dsoc_p1b;
        dsoc_p1_cb(I_electohybb)=dsoc_p2b(I_electohybb); % en fait c'est faut il faut recalculer les dsoc correspondant au prochain p de singularit? de carto
        soc_p1_cb=cumsum(dsoc_p1_cb);
    end
    Idiffb=find(dsoc_p1b~=dsoc_p2b & p_modeb~=pelec_hybb(I) );

    %% Recalcul des grandeurs
    
    elhyb=double(elhyb_matb(I,:));
    elhyb(I_electohyb)=1;
    
    ham=m_Ham_3Db(I,:);
    ham(I_electohyb)=m_Ham_3Db(I+1,I_electohyb);
    lambda=p1*ones([1 sy]);
    
    ibat=repmat(ibat_matb(end,:),[sx_elhyb 1]);
    ibat_mat1=ibat_matb(Ilin);
    ibat(Ihyb)=ibat_mat1(Ihyb);
    ibat=ibat(I,:);
    ibat(I_electohyb)=ibat_mat1(I+1,I_electohyb);
    
    ubat=repmat(ubat_matb(end,:),[sx_elhyb 1]);
    ubat_mat1=ubat_matb(Ilin);
    ubat(Ihyb)=ubat_mat1(Ihyb);
    ubat=ubat(I,:);
    ubat(I_electohyb)=ubat_mat1(I+1,I_electohyb);
    
    Rbat=repmat(R_matb(end,:),[sx_elhyb 1]);
    R_mat1=R_matb(Ilin);
    Rbat(Ihyb)=R_mat1(Ihyb);
    Rbat=Rbat(I,:);
    Rbat(I_electohyb)=R_mat1(I+1,I_electohyb);
    
    RdF=repmat(RdF_matb(end,:),[sx_elhyb 1]);
    RdF_mat1=RdF_matb(Ilin);
    RdF(Ihyb)=RdF_mat1(Ihyb);
    RdF=RdF(I,:);
    RdF(I_electohyb)=RdF_mat1(I+1,I_electohyb);
    
    u0bat=E_mat*ones([1 sy]);
    
    mfi_mat=[mfib; zeros([1 sy])];
    mfi=repmat(mfi_mat(end,:),[sx_elhyb 1]);
    mfi_mat1=mfi_mat(Ilin);
    mfi(Ihyb)=mfi_mat1(Ihyb);
    mfi=mfi(I,:);
    mfi(I_electohyb)=mfi_mat1(I+1,I_electohyb);
    dcarb=mfi;
    
    
    %cmt=cmti_c(Ilin(I,:));
    cmt=repmat(cmti_cb(end,:),[sx_elhyb 1]);
    cmt_mat1=cmti_cb(Ilin);
    cmt(Ihyb)=cmt_mat1(Ihyb);
    cmt=cmt(I,:);
    cmt(I_electohyb)=cmt_mat1(I+1,I_electohyb);
    
    wmt=wmt_hyb;
    wmt(elhyb==0)=0;
    dwmt=dwsec_cpl;
    dwmt(elhyb==0)=0;
    
    cacm1=repmat(cacm1i_cb(end,:),[sx_elhyb 1]);
    cacm1_mat1=cacm1i_cb(Ilin);
    cacm1(Ihyb)=cacm1_mat1(Ihyb);
    cacm1=cacm1(I,:);
    cacm1(I_electohyb)=cacm1_mat1(I+1,I_electohyb);
    
    wacm1=wacm1_hyb;
    wacm1(elhyb==0)=wacm1_elec(elhyb==0);
    dwacm1=dwacm1_hyb;
    dwacm1(elhyb==0)=dwacm1_elec(elhyb==0);
    
    qacm1=repmat(qacm1_mat(end,:),[sx_elhyb 1]);
    qacm1_mat1=qacm1_mat(Ilin);
    qacm1(Ihyb)=qacm1_mat1(Ihyb);
    qacm1=qacm1(I,:);
    qacm1(I_electohyb)=qacm1_mat1(I+1,I_electohyb);
    
    cprim2_cpl=cacm1-VD.ACM1.J_mg*dwacm1;
    indice_rap_opt=NaN*ones([1 sy]);
    
    soc=soc_p1_c+soc_ini-soc_p1_c(1);
end
for ii=1:length(r_var)
    if ~exist('r')
        [ERR,r] = affect_struct(r_var);
    else
        [ERR,r] = affect_struct(r_var,r);
    end
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,VD,r]=calc_prog_dyn_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);
%r_var={'cprim2_cpl','elhyb','ibat','soc','ubat','Rbat','Ebat','RdF','dcarb','cmt','wmt','dwmt','cacm1','wacm1','dwacm1','qacm1','cfrein_meca_pelec_mot','indice_rap_opt'};
r_var={'elhyb','soc','indice_rap_opt','ibat_max','ibat_min','elec_pos','soc_min','soc_max','Dsoc_min','Dsoc_max','indice_rap_opt_elec',...
    'lim_inf','lim_sup','lim_elec','soc_inf','soc_sup','pres_elec'...
    'poids_min','meilleur_chemin','dcarb_soc_min','cmt_soc_min'};

[ERR,VD.ACM1]=inverse_carto_melec(VD.ACM1,0);

[ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec,pres_elec,dcarb_soc_min,cmt_soc_min]=calc_limites_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1);


if ~isempty(ERR)
    chaine='error in calc_limites_HP_BV_2EMB' ;
    ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
    %[cprim2_cpl,elhyb,ibat,soc_r,ubat,Rbat,Ebat,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cfrein_meca_pelec_mot,indice_rap_opt]=backward_erreur(length(VD.CYCL.temps),0,0);
    r=struct([]);
    return
end
% verification de la precision du graph pour forcer le nombre
% d'arc sur un pas de temps a une valeur min (sinon peut devenir 0)
if isfield(param,'Nb_arc_min') & param.Nb_arc_min~=0 & abs(Dsoc_min(end)-Dsoc_max(end))~=0
    %if (min(abs(Dsoc_min(2:end)-Dsoc_max(2:end)))/param.Nb_arc_min < -param.prec_graphe) | (param.prec_graphe==0) | ~isfield(param,'prec_graphe')
    if (abs(Dsoc_min(end)-Dsoc_max(end))/param.Nb_arc_min < -param.prec_graphe) | (param.prec_graphe==0) | ~isfield(param,'prec_graphe')
        val_prec=-abs(Dsoc_min(end)-Dsoc_max(end))/param.Nb_arc_min;
        param.prec_graphe=val_prec;
        assignin('base','val_prec',val_prec);
       % evalin('base','param.prec_graphe=val_prec;');
        if isfield(param,'prec_graphe') & param.prec_graphe~=0
              chaine='Graph precision automatically reduce by param.Nb_arc_min \ngraph precision : %f' ;
              warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine,param.prec_graphe);
        end
    end
end


[ ERR,lim_inf,lim_sup,lim_elec,soc_inf,soc_sup] =limite2indice_soc(VD,soc_min,soc_max,param);
if ~isempty(ERR)
    chaine='error in limite2indice_soc calcul' ;
    ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
    r=struct([]);
    return
end

if isfield(param,'limite_chemin') && param.limite_chemin==1
    chemin_p=evalin('base','meilleur_chemin(1,:)');
    lim_inf_p=double(chemin_p-20)-double(chemin_p(1));
    lim_sup_p=double(chemin_p+20)-double(chemin_p(1));
    
    lim_inf=max((lim_inf),lim_inf_p+lim_inf(1));
    lim_sup=min((lim_sup),lim_sup_p+lim_sup(1));
    
    assignin('base','lim_inf',lim_inf)
    assignin('base','lim_sup',lim_sup)
end

% Information sur le graphe
if param.verbose>=1
    info_graph(VD,param,Dsoc_min,Dsoc_max,lim_sup,lim_inf);
end

var_com_inf=Dsoc_min;
var_com_sup=Dsoc_max;

if isfield(param,'calc_mat') & param.calc_mat==1
    [ERR,poids_min,meilleur_chemin]= ...
        solve_graph_mat(ERR,@(ERR,Dsoc) calc_cout_arc_HP_BV_2EMB_mat(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,100-VD.INIT.Dod0),...
        param,lim_sup,lim_inf,var_com_inf,var_com_sup,0,lim_elec); 
else
    [ERR,poids_min,meilleur_chemin]= ...
        solve_graph(ERR,@(ERR,ii,Dsoc) calc_cout_arc_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,100-VD.INIT.Dod0,ii),...
        param,lim_sup,lim_inf,var_com_inf,var_com_sup,0,lim_elec);
end
meilleur_chemin=meilleur_chemin+min(lim_inf)-1;

% cas ou les meilleurs chemin sont inf, on sort en erreur
if sum(poids_min)==Inf
   chaine=' Attention les poids min sont infinis %6.3f%6.3f \n Essayer de diminuer la precision du graph';
   ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine,poids_min(1),poids_min(2));
   r=struct([]);
   return;
end

if isnan(poids_min) | ~isempty(ERR)
    chaine='min weight equal to NaN or ERR non empty after solve_graph ' ;
    %warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine);
    ERR1=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
    ERR=addCause(ERR1,ERR);
    r=struct([]);
    return
end

% passage chemin aux var de commande
% NB il faut appeler calc_cout_arc pour des soucis d'accrochage sur arc tout thermique
if isfield(param,'calc_mat') & param.calc_mat==1
    [ERR,Dsoc,elhyb,indice_rap_opt]=chemin2varcomBATT_mat(ERR,@(ERR,Dsoc) calc_cout_arc_HP_BV_2EMB_mat(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,100-VD.INIT.Dod0),...
        param,lim_sup,lim_inf,lim_elec,var_com_inf,var_com_sup,meilleur_chemin(1,:),elec_pos,indice_rap_opt_elec);
    
    if isfield(param,'calc_2chemin') && param.calc_2chemin==1;
        [ERR,Dsoc_2,elhyb_2,indice_rap_opt_2]=chemin2varcomBATT_mat(ERR,@(ERR,Dsoc) calc_cout_arc_HP_BV_2EMB_mat(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,100-VD.INIT.Dod0),...
            param,lim_sup,lim_inf,lim_elec,var_com_inf,var_com_sup,meilleur_chemin(2,:),elec_pos,indice_rap_opt_elec);
    end
else
    
    [ERR,Dsoc,elhyb,indice_rap_opt]=chemin2varcomBATT(ERR,@(ERR,ii,Dsoc) calc_cout_arc_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,100-VD.INIT.Dod0,ii),...
        param,lim_sup,lim_inf,lim_elec,var_com_inf,var_com_sup,meilleur_chemin(1,:),elec_pos,indice_rap_opt_elec);
    
    if isfield(param,'calc_2chemin') && param.calc_2chemin==1;
        [ERR,Dsoc_2,elhyb_2,indice_rap_opt_2]=chemin2varcomBATT(ERR,@(ERR,ii,Dsoc) calc_cout_arc_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,100-VD.INIT.Dod0,ii),...
            param,lim_sup,lim_inf,lim_elec,var_com_inf,var_com_sup,meilleur_chemin(2,:),elec_pos,indice_rap_opt_elec);
        
    end
end

if VD.CYCL.ntypcin==1 % rapport de boite optimise : on recree les vecteurs a partir des tableaux
    if isfield(param,'calc_2chemin') && param.calc_2chemin==1;
        csec_emb1_2=csec_emb1(sub2ind(size(csec_emb1),indice_rap_opt_2,1:length(indice_rap_opt_2)));
        wsec_emb1_2=wsec_emb1(sub2ind(size(wsec_emb1),indice_rap_opt_2,1:length(indice_rap_opt_2)));
        dwsec_emb1_2=dwsec_emb1(sub2ind(size(dwsec_emb1),indice_rap_opt_2,1:length(indice_rap_opt_2)));
        elec_pos_2=elec_pos(sub2ind(size(elec_pos),indice_rap_opt_2,1:length(indice_rap_opt_2)));
    end
    assignin('base','csec_emb1',csec_emb1)
    assignin('base','indice_rap_opt',indice_rap_opt)
    csec_emb1=csec_emb1(sub2ind(size(csec_emb1),indice_rap_opt,1:length(indice_rap_opt)));
    wsec_emb1=wsec_emb1(sub2ind(size(wsec_emb1),indice_rap_opt,1:length(indice_rap_opt)));   
    dwsec_emb1=dwsec_emb1(sub2ind(size(dwsec_emb1),indice_rap_opt,1:length(indice_rap_opt)));
    elec_pos=elec_pos(sub2ind(size(elec_pos),indice_rap_opt,1:length(indice_rap_opt)));
    
    VD.CYCL.rappvit=indice_rap_opt-1;
    VD.CYCL.ntypcin=3; % on repasse a ntypcin =3 pour le recalcul des grandeurs vehicules puisque 
                    % maintenant on connait les rapports.
else
    if isfield(param,'calc_2chemin') && param.calc_2chemin==1;
        csec_emb1_2=csec_emb1;
        wsec_emb1_2=wsec_emb1;
        dwsec_emb1_2=dwsec_emb1;
        elec_pos_2=elec_pos;
    end
end


% Recalcul de toutes les grandeurs du vehicule.
if isfield(param,'calc_mat') & param.calc_mat==1
    [ERR,~,~,~,r]=...
        calc_cout_arc_HP_BV_2EMB_mat(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,100-VD.INIT.Dod0,1,elhyb);
    
    if isfield(param,'calc_2chemin') && param.calc_2chemin==1;
        [ERR,~,~,~,r2]=...
            calc_cout_arc_HP_BV_2EMB_mat(ERR,param,VD,csec_emb1_2,wsec_emb1_2,dwsec_emb1_2,Dsoc_2,elec_pos_2,100-VD.INIT.Dod0,1,elhyb_2);
        r.dcarb=[r.dcarb; r2.dcarb];
        r.Dsoc=[Dsoc; Dsoc_2];
    end
else
    for i=2:1:length(VD.CYCL.temps)
        [ERR,~,~,~,r.ibat(i),r.ubat(i),r.Rbat(i),r.u0bat(i),r.RdF(i),r.dcarb(i),r.cprim2_cpl(i),r.cmt(i),r.wmt(i),r.dwmt(i),r.cacm1(i),r.wacm1(i),r.dwacm1(i),r.qacm1(i),r.cprim1_cpl(i),r.Dsoc(i),r.cfrein_meca_pelec_mot(i)]=...
            calc_cout_arc_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc_1(i),elec_pos,100-VD.INIT.Dod0,i,1,elhyb);
    end
end

% Ne pas calculer les soc par trapz mais par des sommes.
soc(1)=100-VD.INIT.Dod0;
soc2(1)=100-VD.INIT.Dod0;
if param.tout_thermique==1;
   soc(1)=100-VD.INIT.Dod0;
end
for i=2:1:length(Dsoc)
    soc(i)=soc(i-1)+Dsoc(i);
    if param.tout_thermique==1;
       soc(i)=soc(i-1)+r.Dsoc(i);
    end
    if isfield(param,'calc_2chemin') && param.calc_2chemin==1;
        soc2(i)=soc2(i-1)+Dsoc_2(i);
        if param.tout_thermique==1;
            soc2(i)=soc2(i-1)+r2.Dsoc(i);
        end
    end
end

if isfield(param,'calc_2chemin') && param.calc_2chemin==1;
    r.soc=[soc; soc2];
end

if param.verbose>=2
    figure(1)
    clf
    plot(VD.CYCL.temps,ibat_max,VD.CYCL.temps,ibat_min,VD.CYCL.temps,r.ibat,VD.CYCL.temps,VD.CYCL.vitesse*3.6)
    grid
    ylabel('courant en A')
    legend('ibat max','ibat min','ibat','vitesse')
    
    figure(2)
    clf
    hold on
    plot(VD.CYCL.temps,soc_min,VD.CYCL.temps,soc_max,VD.CYCL.temps,soc_inf,VD.CYCL.temps,soc_sup)
    legend('soc min','soc max','soc inf','soc')
    title('limites sur le soc')
    
    figure(3)
    clf
    hold on
    plot(VD.CYCL.temps+1,meilleur_chemin,VD.CYCL.temps+1,lim_inf,VD.CYCL.temps+1,lim_sup,VD.CYCL.temps+1,lim_elec,VD.CYCL.temps+1,VD.CYCL.vitesse*36)
    legend('meilleur chemin 1','meilleur chemin 2','lim inf','lim sup','lim elec','vitesse')
    title('meilleur chemin en indice')
    grid
end  

if param.verbose>=2   
    figure(4)
    clf
    hold on            
    plot(VD.CYCL.temps,soc_inf,'k',VD.CYCL.temps,soc_sup,'k',VD.CYCL.temps,soc,'b',VD.CYCL.temps,soc2,'g',VD.CYCL.temps,VD.CYCL.vitesse*3.6,'c')
    legend('soc_inf','soc_sup','soc1','soc2')
    title('meilleur chemin en soc');
    grid
   
    figure(5)
    clf
    hold on
    plot(VD.CYCL.temps,r.ibat,VD.CYCL.temps,r.ubat,VD.CYCL.temps,r.dcarb)
    legend('ibat','ubat','dcarb')
    grid
end

[ERR,r]=affect_struct(r_var,r);

assignin('base','ibat_max',ibat_max);
assignin('base','ibat_min',ibat_min);

if param.verbose>=1
    assignin('base','poids_min',poids_min);
    assignin('base','meilleur_chemin',meilleur_chemin);
end
return
