% function [ERR,ham,lambda,cprim2_cpl,ibat,soc,dcarb,ubat]=calc_hamilt_hyb_HP_BV_2EMB(ERR,param,cprim2_cpl,csec_cpl,wsec_cpl,dwsec_cpl,lambda_p,j,soc_p,calc_min,lambda_cst)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

%function [ERR,ham_hyb,lambda,cprim2_cpl,ibat,soc,dcarb,ubat,indice_rap_opt,Dham,i_min]=calc_hamilt_hyb_HP_BV_2EMB(ERR,VD,param,cprim2_cpl,csec_cpl,wsec_cpl,dwsec_cpl,lambda_p,j,soc_p,calc_min,lambda_cst)
function [ERR,r]=calc_hamilt_hyb_HP_BV_2EMB(ERR,VD,param,cprim2_cpl,csec_cpl,wsec_cpl,dwsec_cpl,r,j,calc_min,lambda_cst)

global Ham_hors_limite;
r_var={'ham','lambda','dcarb','cmt','wmt','dwmt','cprim2_cpl','cacm1','wacm1','qacm1','dwacm1','ibat','ubat','Rbat','u0bat','RdF','soc','indice_rap_opt','Dham','i_min'};

if j==1
    soc_p=r.soc(1);
    lambda_p=r.lambda(1);
    for ii=1:length(r_var)-2
        eval(['r.' (r_var{ii}) '_hyb=' 'NaN*ones(size(VD.CYCL.temps));' ]);
    end
else
    soc_p=r.soc(j-1);
    lambda_p=r.lambda(j-1);
end

if length(wsec_cpl)~=1 % cas rapport de boite libre
    cprim2_cpl=repmat(cprim2_cpl,length(wsec_cpl),1);
end

%if length(wsec_cpl)~=1 % cas rapport de boite libre
[ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,repmat(csec_cpl,1,length(cprim2_cpl)),repmat(wsec_cpl,1,length(cprim2_cpl)),repmat(dwsec_cpl,1,length(cprim2_cpl)),cprim2_cpl,0);
% else
%     [ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,cprim2_cpl,0);
% end


% Connexion
wmt=wprim1_cpl;
dwmt=dwprim1_cpl;
cmt=cprim1_cpl+VD.MOTH.J_mt.*dwmt;
wacm1=wprim2_cpl;
dwacm1=dwprim2_cpl;
cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;

% Calcul du couple du moteur thermique
% limitation MTH en recup notamment le freinage fera la reste mais Cth est
% limite a Cth min
cmt_max=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt);
cmt_min=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_min,wmt);


% Calcul des conditions en amont de la machine electrique
%% Remarque : les limitations sur cacm1 max min doivent etre ecrite en
%% dehors de cette fonction
%
%[Li,Co]=size(cacm1);
%wacm1=repmat(wacm1,1,Co);
[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,0,param.fit_pertes_acm);

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);
if ~isempty(ERR)
    soc=NaN;
    ah=NaN;
    [ah,soc]=backward_erreur(length(VD.CYCL.temps));
    return;
end

%if length(wsec_cpl)==1 % sinon on cr??e un vecteur inutilement
    pacc=pacc(j);
%end

pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc;

[ERR,ibat,ubat,soc,ah,E,R,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,soc_p,0,pres,0);

if strcmp(lambda_cst,'non') % ce calcul etant long il est preferable de la faire une fois pour toute 
    % et pas a chaque appel de la fonction
    if ~isfield(VD.BATT,'dE_dEn')
        if isfield(VD.BATT,'tbat')
            [bidon,Li_T20]=min(abs(VD.BATT.tbat-20));
        elseif isfield(VD.BATT,'Tbat_table')
            [bidon,Li_T20]=min(abs(VD.BATT.Tbat_table-20));
        end
        Ebatt=(trapz(VD.BATT.dod_ocv,VD.BATT.ocv(Li_T20,:))-cumtrapz(VD.BATT.dod_ocv,VD.BATT.ocv(Li_T20,:)))*3600*VD.BATT.Cahbat_nom/100*VD.BATT.Nblocser*VD.BATT.Nbranchepar;
        OCV=VD.BATT.ocv(1,:)*VD.BATT.Nblocser;
        dE_dEn(2:length(Ebatt)-1)=(OCV(3:end)-OCV(1:end-2)) ./ (Ebatt(3:end)-Ebatt(1:end-2));
        dE_dEn(1)=dE_dEn(2);
        dE_dEn(length(Ebatt))=dE_dEn(length(Ebatt)-1);
        VD.BATT.Enbatt=Ebatt;
        VD.BATT.dE_dEn=dE_dEn;
    end

    if j>1
        DOD=100-soc;
        %OCV=VD.BATT.ocv(1,:);
        %Enbatt = VD.BATT.Cahbat_nom/100 * VD.BATT.Nblocser *...
        %    ( trapz(VD.BATT.dod_ocv,OCV)-...
        %      trapz( [ VD.BATT.dod_ocv(VD.BATT.dod_ocv<DOD) DOD ] , [ OCV(VD.BATT.dod_ocv<DOD) interp1(VD.BATT.dod_ocv,OCV,DOD)] ));
        %Enbatt=Enbatt+Ibat.*E*Dt; %Energie contenue dans la batterie
        %dQdEnsc=-VD.BATT.RdFarad./R*(VD.BATT.K*Enbatt-(VD.BATT.K*E.*E-2*VD.BATT.K*R*Pbatt)
        %./sqrt(E.*E-4*Pbatt.*R)) ; % D??riv?? de Q (puissance entrante dans la batterie parfaite)
        dE_dEn=0*ones(1,length(DOD));
        %dE_dEn(delta>=0)=interp1(VD.BATT.dod_ocv,VD.BATT.dE_dEn,DOD(delta>=0));
        dE_dEn=interp1(VD.BATT.dod_ocv,VD.BATT.dE_dEn,DOD);
        Pbat=ubat.*ibat;
        dQdEnsc=-VD.BATT.RdFarad./(2*R).*  ( 2*E.*dE_dEn - dE_dEn.*sqrt(E.^2-4*Pbat.*R) - ((E.^2).*dE_dEn)./sqrt(E.^2-4*Pbat.*R) );  % si seul la tension ?? vide d??pend de l'??nergie stock??
        %dQdEnsc=0;;
        %    if strcmp(FP.exist,'oui')
        %         Ensc_min=FP.xmin*(1+FP.delta_min);
        %         Ensc_max=FP.xmax*(1-FP.delta_max);
        %         dfonc=0*ones(1,length(Cme));
        %         dfonc(Ensc < Ensc_min)=2*FP.rmin*((Ensc(Ensc<Ensc_min)-(Ensc_min))/(FP.xmax^2));
        %         dfonc(Ensc > Ensc_max)=2*FP.rmax*((Ensc(Ensc>Ensc_max)-(Ensc_max))/(FP.xmax^2));
        %    else
        %       dfonc=0;
        %    end
        dfonc=0;
        Dt=param.pas_temps;
        lambda=(lambda_p-dfonc)./(1+dQdEnsc*Dt);
    else
        lambda=lambda_p*ones(1,length(cacm1));
    end
    
else
    lambda=lambda_p*ones(1,length(cacm1));
end

%%% Calcul conso et hamiltonien

%[Li_cmt,Co_cmt]=size(cmt)

%wmt=repmat(wmt,Li_cmt,1);
%cmt_min=repmat(cmt_min,Li_cmt,1);
%cmt_max=repmat(cmt_max,Li_cmt,1);
%dcarb(cmt>=0)=interp2(VD.MOTH.Reg_2dconso,VD.MOTH.Cpl_2dconso,VD.MOTH.Conso_2d',wmt(cmt>=0),cmt(cmt>=0),'linear');

%% Pourquoi les ligne qui suivent ne sont pas gerer par calc_mt (EV : 20-12-13)
% dcarb=interp2(VD.MOTH.Reg_2dconso,VD.MOTH.Cpl_2dconso,VD.MOTH.Conso_2d',wmt,cmt,'linear');
%
% if isscalar(wmt)
%     dcarb(cmt<0)=interp2(VD.MOTH.Reg_2dconso,VD.MOTH.Cpl_2dconso,VD.MOTH.Conso_2d',wmt,0,'linear');
% else
%     dcarb(cmt<0)=interp2(VD.MOTH.Reg_2dconso,VD.MOTH.Cpl_2dconso,VD.MOTH.Conso_2d',wmt(cmt<0),0,'linear');
% end
%
% if isscalar(cmt_min)
%     dcarb=dcarb';
%     dcarb(cmt<0)=dcarb(cmt<0).*(1-cmt(cmt<0)./cmt_min);
% else
%     dcarb(cmt<0)=dcarb(cmt<0).*(1-cmt(cmt<0)./cmt_min(cmt<0));
% end
% dcarb(cmt<0)=max(dcarb(cmt<0),0); % par securite en cas de leger depassement negatif

[ERR,dcarb]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,1,0,0,param.fit_pertes_moth);
% if strcmp(type_Ham,'I')==1
%     Hamilt=VD.MOTH.pci*dcarb+lambda*RdF.*Ibat;
% elseif strcmp(type_Ham,'P')==1
if isfield(param,'Kibat')
    ham=5*param.Kibat*ibat.*ibat+lambda.*E.*RdF.*ibat+VD.MOTH.pci*dcarb;
else
    ham=VD.MOTH.pci*dcarb+lambda.*RdF.*ibat.*E;
end

%%% Limitations sur le courant batterie et le couple max
ham(cmt>cmt_max | isnan(ibat) | isnan(qacm1)) = Ham_hors_limite;

%%% En rapport de boite libre il faut interdire le Point mort su csec_cpl>0
[Li,Co]=size(ham);

if Li>1 & csec_cpl(2)>0
    ham(1,:)=Ham_hors_limite;
end
%%% calcul du min et des grandeurs associee
u0bat=E*ones(1,length(cacm1));
Rbat=R;
if calc_min==1 % Sinon on renvoit les grandeurs vectotielles pour trace hamilt notamment
    if Li>1
        [ham,j_min]=min(ham,[],2);
        [ham,i_min]=min(ham); % le cas point mort n'as pas de sens en mode hybride
        indice_rap_opt=i_min;
        j_min=j_min(i_min);
        cmt=cmt(i_min,j_min);
        wmt=wmt(i_min,j_min);
        ibat=ibat(i_min,j_min);
        ubat=ubat(i_min,j_min);
        soc=soc(i_min,j_min);
        dcarb=dcarb(i_min,j_min);
        cacm1=cacm1(i_min,j_min);
        cprim2_cpl=cprim2_cpl(i_min,j_min);
        qacm1=qacm1(i_min,j_min);
        if strcmp(lambda_cst,'non')
            lambda=lambda(i_min,j_min);
        end
    else
        indice_rap_opt=NaN*ones(1,length(cacm1));
        [ham_hyb,i_min]=min(ham);
        if i_min~=length(ham) && i_min~=1
            Dham=min(abs(ham_hyb-ham(i_min+1)),abs(ham_hyb-ham(i_min-1)));
        elseif i_min==1
            Dham=min(abs(ham_hyb-ham(2)));
        elseif i_min==length(ham)
            Dham=min(abs(ham_hyb-ham(end-1)));
        end
        [ERR,r] = affect_struct_lagrange(r_var(1:end-2),r,'hyb',j,' ',i_min);
        r.Dham=Dham;
        r.i_min=i_min;
    end
else
    [ERR,r] = affect_struct_lagrange(r_var(1:end-2),r,'hyb');
end

return


