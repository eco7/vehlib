% function [ERR,ham,lambda,cacm1,ibat,soc,dcarb,ubat]=solve_hamilt_HP_BV_2EMB(ERR,VD,param,csec_cpl,wsec_cpl,dwsec_cpl,Lambda_p,j,soc_p,calc_min,Lambda_cst);
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Minimisation de la courbe des courants BAT/VD.SC admissibles pour la valeur
% de lambda
% Arguments appel :
% Arguments de sortie :
%
% 26-08-2011 (EV) : version 1

function [ERR,r]=solve_hamilt_HP_BV_2EMB(ERR,VD,param,csec_cpl,wsec_cpl,dwsec_cpl,r,j,calc_min,Lambda_cst);
global Ham_hors_limite
% Calcul des limites

borne_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,1),wsec_cpl.*VD.ADCPL.kred);
borne_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,1),wsec_cpl.*VD.ADCPL.kred);

cprim2_cpl_vec=borne_min:param.pas_cprim2_cpl:borne_max;
[ERR,r]=calc_hamilt_hyb_HP_BV_2EMB(ERR,VD,param,cprim2_cpl_vec,csec_cpl,wsec_cpl,dwsec_cpl,r,j,calc_min,Lambda_cst);

if r.ham_hyb~=Ham_hors_limite & isfield(param,'Dham') % on retreci l'intervalle de recherche sur le hamiltonien
    while r.Dham>param.Dham
        if r.i_min~=length(cprim2_cpl_vec) && r.i_min~=1
            cprim2_cpl_vec=cprim2_cpl_vec(r.i_min-1):abs(cprim2_cpl_vec(r.i_min+1)-cprim2_cpl_vec(r.i_min-1))/1000 :cprim2_cpl_vec(r.i_min+1);
        elseif r.i_min==length(cprim2_cpl_vec)
            cprim2_cpl_vec=cprim2_cpl_vec(end-1):abs(cprim2_cpl_vec(end)-cprim2_cpl_vec(end-1))/1000 :cprim2_cpl_vec(end);
        elseif r.i_min==1
            cprim2_cpl_vec=cprim2_cpl_vec(1):abs(cprim2_cpl_vec(2)-cprim2_cpl_vec(1))/1000 :cprim2_cpl_vec(2);
        end
        [ERR,r]=calc_hamilt_hyb_HP_BV_2EMB(ERR,VD,param,cprim2_cpl_vec,csec_cpl,wsec_cpl,dwsec_cpl,r,j,calc_min,Lambda_cst);
    end
end
