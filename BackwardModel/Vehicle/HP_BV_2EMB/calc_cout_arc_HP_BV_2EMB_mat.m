% function [ERR,cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=calc_cout_arc_HP_BV_2EMB(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,soc_p,i,recalcul,elhyb)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Calcul des cout des arcs cas 2EMB
%
% interface entree :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discretisation etc)
% CYLC,VD.ADCPL...:structures de description vehicule et composants
% csec_emb1 :   Couple sur le secondaire de l'embrayage "principal" (vecteur sur le cycle)
% wsec_emb1 :   vitesse sur le secondaire de l'embrayage "principal" (vecteur sur le cycle)
% dwsec_emb1 :  derivee de la vitesse sur le secondaire de l'embrayage "principal" (vecteur sur le cycle)
% Dsoc :        variation de soc sur l'eventail des arcs possible
% elec_pos :    mode electrique possible (vecteur sur le cycle)
% soc_p :       soc a l'instant precedent
% i :           instant dans le cycle
% recalcul :    si on est en phase de recalcul (grahe deja resolu) recalcul =1 sinon 0 ou pas passer dans l'interface
% elhyb :       mode elec/hybride/thermique en phase recalcul uniquement
%
% interface de sortie
% cout :        cout des arcs de l'eventail
% Dsoc_tt :     variation de soc correspondant a l'arc sur lequel on s'est accroche pour le mode tout thermique, ce n'est pas la variation exacte de
%               soc en tou tthermique qui sera elle calcule en phase recalcul
% indice_rap_opt : indice correspondant au rapport de boite optimal (-1 en
%                   rapport de boite imposes)
% ibat,ubat ...:variable vehicule utilie uniquement en recalcul sinon on ne renvoie que le cout
%
% 16-12-10(EV): Creation 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%function [ERR,cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=calc_cout_arc_HP_BV_2EMB_mat(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,soc_p,recalcul,elhyb)
function [ERR,cout,indice_rap_opt,Dsoc_tt,r]=calc_cout_arc_HP_BV_2EMB_mat(ERR,param,VD,csec_emb1,wsec_emb1,dwsec_emb1,Dsoc,elec_pos,soc_p,recalcul,elhyb)

r_var={'ibat','ubat','Rbat','u0bat','RdF','dcarb','cprim2_cpl','cmt','wmt','dwmt','cacm1','wacm1','dwacm1','qacm1','cprim1_cpl','Dsoc','cfrein_meca_pelec_mot'};
indice_rap_opt=-1*ones(size(Dsoc));

[Li,Co]=size(soc_p);
[sx,sy]=size(Dsoc);
sz=size(csec_emb1,1);
elec_pos_mat=0*ones(sx,sy,sz);
if sum(sum(isnan(soc_p)))==Li*Co
    ERR=MException('BackwardModel:calc_cout_arc_HP_BV_2EMB', 'appel a calc_cout_arc_HP_BV_2EMB : tout les elements d''un des vecteur precedent a NaN');
    [cout,Dsoc_tt,indice_rap_opt]=backward_erreur(size(Dsoc));
    [ERR,r]=affect_struct(r_var);
    return
end

%% NB EV : attention calc_emb sature la vitesse du primaire a ral moteur si il y as un raport d ered entre les deux c'est faut
[ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,ones(size(csec_emb1)));

% connexion
if VD.CYCL.ntypcin==3
    wsec_cpl=repmat(wprim_emb1,sx,1);
    dwsec_cpl=repmat(dwprim_emb1,sx,1); 
    csec_cpl=repmat(cprim_emb1,sx,1);   
    if param.pas_temps~=0
        pas_temps=param.pas_temps;
    else
        pas_temps=repmat(VD.CYCL.pas_temps,sx,1);   
    end
else
% dans le cas ntypcin==1 (optimisation des rapports), les entrees cprim_emb1 et  wprim_emb1 sont de
% type matricielles (length(cycl.temps) x length(rappvit)) Dsoc est aussi
% une matrice (length(cycl.temps) x length(Dsocmax)) 

% On recreera quand necessaire des table de taille length(cycl.temps) x
% length(Dsocmax) x length(rappvit)

     wsec_cpl=reshape(wprim_emb1',1,sy,sz);
     wsec_cpl=repmat(wsec_cpl,[sx 1 1]);
     csec_cpl=reshape(cprim_emb1',1,sy,sz);
     csec_cpl=repmat(csec_cpl,[sx 1 1]);
     dwsec_cpl=reshape(dwprim_emb1',1,sy,sz);
     dwsec_cpl=repmat(dwsec_cpl,[sx 1 1]);
     elec_pos=reshape(elec_pos',1,sy,sz); 
     elec_pos_mat(1,:,:)=elec_pos;
     pas_temps=repmat(VD.CYCL.pas_temps,[sx 1 sz]);
end

[ERR,~,wacm1,dwacm1]=calc_red(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,0);


[ERR,pacc]=calc_acc(VD);
pacc=repmat(pacc,[sx 1 sz]);

% calcul batterie
% calcul ibat a partir de Dsoc
RdF=ones(size(Dsoc));
RdF(Dsoc>0)=VD.BATT.RdFarad;

if VD.CYCL.ntypcin==1
   ibat=-repmat(Dsoc./RdF,[1 1 sz])*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100./pas_temps;
else
   ibat=-Dsoc./RdF*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100./pas_temps;
end 

[ERR,ubat]=calc_batt_fw(ERR,VD.BATT,pas_temps,ibat,soc_p,0);

pbat=ubat.*ibat;

pacm1=pbat-pacc;

% calcul forward motelec
[ERR,qacm1]=calc_pertes_acm_fw(ERR,VD.ACM1,pacm1,wacm1,dwacm1,1,param.fit_pertes_acm);

cacm1=(pacm1-qacm1)./wacm1;
cacm1(isnan(cacm1))=Inf;
[ERR,qacm1_verif]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,1,param.fit_pertes_acm);

% accrochage sur arc tout thermique
Pf_motelec=zeros(size(cacm1)); % pertes par frottement en mode tout-thermique, utiles pour le bilan de puissance
Dsoc_tt(sy)=Inf;
if param.tout_thermique==1
    if nargin==9 | recalcul==0
        % recherche des ranges ou csec>0
        if VD.CYCL.ntypcin==3
            Iy_pos=find((csec_emb1)>0);
            pbat(:,Iy_pos)-pacc(:,Iy_pos);
            [~,ind_tt] = min(abs(pbat(:,Iy_pos,1)-pacc(:,Iy_pos,1)));
            ind_lin=sx*(Iy_pos-1)+ind_tt;
            cacm1(ind_lin)=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,wacm1(ind_lin));
            qacm1(ind_lin)=0;
            pbat(ind_lin)=pacc(ind_lin);
            Dsoc_tt(Iy_pos)=Dsoc((Iy_pos-1)*sx+ind_tt);
            Pf_motelec(ind_lin)=cacm1(ind_lin).*wacm1(ind_lin);
        elseif VD.CYCL.ntypcin==1
            % on choisi ici l'arc le plus proche du mode tout thermique sans choisir de rapportde boite.
            % on fixe le couple du moteur electrique a sa valeur de frottement
            % (-1), ses pertes sont nulles. On fait ensuite l'approximation
            % que pour cet arc pbat=pacc,
            % on en deduit le chemin tout thermique
            Iy_pos=find(sum(csec_emb1(2:end,:))>0);
            pbat(:,Iy_pos,1)-pacc(:,Iy_pos,1);
            [~,ind_tt] = min(abs(pbat(:,Iy_pos,1)-pacc(:,Iy_pos,1)));
            Iz=1:sz;
            Iz=repmat(Iz',1,length(Iy_pos));
            Iz=reshape(Iz',1,sz*length(Iy_pos));
            ind_lin=sx*sy*(Iz-1)+sx*(repmat(Iy_pos,1,sz)-1)+repmat(ind_tt,1,sz);
            cacm1(ind_lin)=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,wacm1(ind_lin));
            qacm1(ind_lin)=0;
            pbat(ind_lin)=pacc(ind_lin);
            Dsoc_tt(Iy_pos)=Dsoc((Iy_pos-1)*sx+ind_tt);
            Pf_motelec(ind_lin)=cacm1(ind_lin).*wacm1(ind_lin); % pertes par frottements            
        end
    end
    if nargin==11 & recalcul==1 %& elhyb(i)==2 % cas tout thermique
        I_term=find(elhyb==2);
        cacm1(I_term)=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,wacm1(I_term));
        qacm1(I_term)=0;
        pbat(I_term)=pacc(I_term);
        [ERR,ibat,~,soc]=calc_batt(ERR,VD.BATT,pas_temps,pbat,soc_p,0,0,0);
        Dsoc(I_term)=soc(I_term)-soc_p;
        Pf_motelec(I_term)=cacm1(I_term).*wacm1(I_term);
    end 
end
% Connexion
cprim2_cpl=cacm1-VD.ACM1.J_mg.*dwacm1;
[ERR,cprim1_cpl,wprim1_cpl,dwprim1_cpl,wprim2_cpl,dwprim2_cpl,cprim2_cpl]=calc_adcpl(ERR,VD.ADCPL,csec_cpl,wsec_cpl,dwsec_cpl,cprim2_cpl,0);


%% A priori en hybride pas de patinage emb2
%[ERR,cprim_emb2,wprim_emb2,dwprim_emb2,etat_emb]=calc_emb2(VD,csec_emb2,wsec_emb2,dwsec_emb2,elhyb);

wmt=wprim1_cpl;
dwmt=dwprim1_cpl;
cmt=cprim1_cpl+VD.MOTH.J_mt.*dwmt;

% Calcul des conditions de fonctionnement du moteur thermique
if VD.MOTH.ntypmoth == 2 || VD.MOTH.ntypmoth == 3
    [ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt_niv0(ERR,VD.MOTH,cmt,wmt,dwmt,ones(size(cmt)),0);
elseif VD.MOTH.ntypmoth == 10
    [ERR,dcarb,cmt,cfrein_meca_mt]=calc_mt(ERR,VD.MOTH,cmt,wmt,dwmt,1,0,0,param.fit_pertes_moth);
else
    ERR=MException('BackwardModel:calc_cout_arc_HP_BV_2EMB','Type de moteur non reconnu.');
    ResXml=struct([]);
    return;
end
    
% On met a inf les dcarb des cmt impossible
dcarb(isnan(cmt) | isinf(cmt))=Inf;

% Calculs batterie il faut supprimer les arcs impossible
% Jusqu'ici ibat est calcule a partir de Dsoc.
[ERR,Ibat,~,~,~,E,R]=calc_batt(ERR,VD.BATT,pas_temps,pbat,soc_p,0,0,0);

dcarb(isnan(Ibat))=Inf;

% Critere de cout des arcs
cout=dcarb.*pas_temps;

if  VD.CYCL.ntypcin==3 || (nargin==11 && recalcul==1)
    for ii=1:sy
        if elec_pos(ii)==1
            cout(1,ii)=0;
        end
    end
elseif VD.CYCL.ntypcin==1
    %cout(~isinf(cout) & ~isnan(cout) & elec_pos_mat==1)=0; % attention si on NaN alors que elec_pos_mat possible !!
    % cout(~isinf(cout) & elec_pos_mat==1)=0; % pourquoi seulement si cout est ~inf EV 26 10 2021
    cout(elec_pos_mat==1)=0; 
     
    indice_sy=sum(elec_pos)<1; % comment elec_pos peut etre <1 EV 26 10 2021
    cout(1,indice_sy,1)=Inf;
    
    Ix_pos=find(csec_cpl(1,:,2)>0);
    Iy_pm=1:sx;
    Iy_pm=repmat(Iy_pm',1,length(Ix_pos));
    Iy_pm=reshape(Iy_pm',1,sx*length(Ix_pos));
    ind_lin=sx*(repmat(Ix_pos,1,sx)-1)+Iy_pm;
    cout(ind_lin)=Inf;
end

% bilan puissance
rend_cpl=VD.ADCPL.rend*ones(size(cprim2_cpl));
rend_cpl(cprim2_cpl<0)=1/VD.ADCPL.rend;

pbat_calc=(qacm1+pacc+VD.ACM1.J_mg*dwacm1.*wacm1+(csec_cpl.*wsec_cpl-((cmt+cfrein_meca_mt).*wmt-VD.MOTH.J_mt*dwmt.*wmt))./rend_cpl)-Pf_motelec;

% calcul de la difference sur pertes bat due aux differences
% sur les carto backward et forward  du motelec
diff_pertes_bat=(qacm1-qacm1_verif)./qacm1*100;

%limitation diverse
cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
p_elec_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Pelec_max(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
p_elec_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Pelec_min(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);

prec_num=1e-10;

% On vient verifier les limites mais attention pour eviter
% d'eliminer les cout des arcs tt elec (premier arc et elec_pos=1
% on n'eliminie pas ces cout qui sont deja a zero
%cout(cacm1<cacm1_min-prec_num | cacm1>cacm1_max+prec_num | pacm1<p_elec_min-prec_num | pacm1>p_elec_max+prec_num)=Inf;
cout(cout~=0 & (cacm1<cacm1_min-prec_num | cacm1>cacm1_max+prec_num | pacm1<p_elec_min-prec_num | pacm1>p_elec_max+prec_num))=Inf;

diff_pertes_bat(cacm1<cacm1_min | cacm1>cacm1_max | pacm1<p_elec_min | pacm1>p_elec_max)=0;

if max(abs(pbat-pbat_calc))>0.1 & param.verbose>=2
    chaine='Energy balance not fullfill \n time : %d';
    warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine,i);
    pause
end

% Minimisation des cout dans le cas ntycin==1 Optimisation des rapports de boite

if VD.CYCL.ntypcin==1
    cout(isnan(cout))=Inf;
    [cout,indice_rap_opt]=min(cout,[],3);  
end

if param.verbose>=10
    if nargin==11 && recalcul==1 
          assignin('base','cout',cout)
    else
        assignin('base','cout_mat',cout)
        assignin('base','dcarb_mat',dcarb)
    end
end

cfrein_meca_pelec_mot=0*ones(size(VD.CYCL.rappvit));
if nargin==11 && recalcul==1 % cas tout elec en recalcul gerer apres bilan de puissance
    % pour des pb de difference de carto fw bw : sinon on ne le boucle pas parfaitement le bilan
    ind_elec=find(elhyb==0);
    cout(ind_elec)=0;
    dcarb(ind_elec)=0;
    cmt(ind_elec)=0;
    wmt(ind_elec)=0;
    [ERR,cprim_emb1,wprim_emb1,dwprim_emb1,etat_emb]=calc_emb(VD,csec_emb1,wsec_emb1,dwsec_emb1,0);
    % connexion
    wsec_cpl=wprim_emb1;
    dwsec_cpl=dwprim_emb1;
    csec_cpl=cprim_emb1;
    
    [ERR,cprim2_cpl(ind_elec),wacm1(ind_elec),dwacm1(ind_elec)]=calc_red(ERR,VD.ADCPL,csec_cpl(ind_elec),wsec_cpl(ind_elec),dwsec_cpl(ind_elec),0);
    cacm1(ind_elec)=cprim2_cpl(ind_elec)+VD.ACM1.J_mg.*dwacm1(ind_elec);

    [ERR,qacm1(ind_elec)]=calc_pertes_acm(ERR,VD.ACM1,cacm1(ind_elec),wacm1(ind_elec),dwacm1(ind_elec),0,param.fit_pertes_acm);
    
    
    % ici cacm1 as ete limite a cacm1_min (en bw) donc pas besoin de le
    % refaire par contre si pacm1_elec est superieur a pacm1_elec en fw il
    % faut recalculer le frein meca correspondant pour boucler le bilan
    % de puissance (car ibat as ete calcule avec pacm1_elec_min)
    p_elec_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Pelec_min(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
    pacm1_elec=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1);
    
    ind_p_elec_min=find(pacm1_elec<p_elec_min);
    pacm1_elec(ind_p_elec_min)=p_elec_min(ind_p_elec_min);
    [ERR,qacm1_fw]=calc_pertes_acm_fw(ERR,VD.ACM1,pacm1_elec,wacm1,dwacm1,1,param.fit_pertes_acm);
    
    cacm1_fw=(pacm1_elec-qacm1_fw)./wacm1;

    cfrein_meca_pelec_mot(ind_p_elec_min)=cacm1_fw(ind_p_elec_min)-cacm1(ind_p_elec_min);
    qacm1(ind_p_elec_min)=qacm1_fw(ind_p_elec_min);
    cacm1(ind_p_elec_min)=cacm1_fw(ind_p_elec_min);
    
    % Cas ou il y as eu pb d'inversion de carto
    % Il faut refaire le meme calcul que dans calc_limite pour boucler le
    % bilan de puissance
    cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
    cond=cacm1_fw<cacm1_min & qacm1_fw>qacm1;
    
    % Cas ou ibat atteind les limites de ibat_min
    pbat=ibat.*ubat;
    [ERR,~,~,~,~,~,~,~,Ibat_min_calc]=calc_batt(ERR,VD.BATT,pas_temps,pbat,100-VD.INIT.Dod0,0,-1,0,param);
    prec_num=1e-10;
    
    if ibat<Ibat_min_calc+prec_num % On ne passe pas dans cette condition si on as es vecteurs !!!!
        %bilanP_loc=ibat.*ubat-pacc(i)-cacm1.*wacm1-qacm1
        % on rappele calc_batt, on regarde si on tape dans ibat_min
        %si oui on recalcul pabt puis cacm1 en fw puis cfrein_cacm1
        pacm1_elec=pbat-pacc(i);
        [ERR,qacm1_fw]=calc_pertes_acm_fw(ERR,VD.ACM1,pacm1_elec,wacm1,dwacm1,1,param.fit_pertes_acm);
        cacm1=cprim2_cpl+VD.ACM1.J_mg.*dwacm1;
        cacm1_fw=(pacm1_elec-qacm1_fw)./wacm1;
        cfrein_meca_pelec_mot=cacm1_fw-cacm1;
        qacm1=qacm1_fw;
        cacm1=cacm1_fw;
    end
    
    ind_Ibat_min_calc=find(ibat<Ibat_min_calc+prec_num);
    pacm1_elec=pbat-pacc;
    [ERR,qacm1_fw]=calc_pertes_acm_fw(ERR,VD.ACM1,pacm1_elec,wacm1,dwacm1,1,param.fit_pertes_acm);
    cacm1(ind_Ibat_min_calc)=cprim2_cpl(ind_Ibat_min_calc)+VD.ACM1.J_mg.*dwacm1(ind_Ibat_min_calc);
    cacm1_fw(ind_Ibat_min_calc)=(pacm1_elec(ind_Ibat_min_calc)-qacm1_fw(ind_Ibat_min_calc))./wacm1(ind_Ibat_min_calc);
    cfrein_meca_pelec_mot(ind_Ibat_min_calc)=cacm1_fw(ind_Ibat_min_calc)-cacm1(ind_Ibat_min_calc);
    qacm1(ind_Ibat_min_calc)=qacm1_fw(ind_Ibat_min_calc);
    cacm1(ind_Ibat_min_calc)=cacm1_fw(ind_Ibat_min_calc);
    
    Rbat=R;
    u0bat=E*ones(size(csec_emb1));
    [ERR,r]=affect_struct(r_var);
    
end



if abs(diff_pertes_bat(1))>5 & param.verbose>=2 % si differences calcul pertes bw fw trop importante on envoi un warning
    chaine='more than 5 percent of error on pbat due to backward/forward maps of motelec \n time : %d';
    warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine,i);
end

if sum((sum(isinf(cout(:,2:end))))==size(cout,1))
    I=find((sum(isinf(cout)))==size(cout,1));
    chaine='all edge are impossible on column I, the systems possibilites does not allow to fullfil the required systems operating point ! \n time : %d \n Dsoc : %s  \n cost : %s \n ibat: %s \n ubat: %s ';
    ERR=MException('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine,num2str(I),num2str(Dsoc),num2str(cout),num2str(ibat),num2str(ubat));
    %if ~isfield(param,'dim') | param.dim==0 % Dans le cas d'un processus de dimensionnement param.dim==1 on ne sort pas en "vrac" on renvoi seulement l'erreur
        [cout,Dsoc_tt,indice_rap_opt]=backward_erreur(length(Dsoc));
        [ERR,r]=affect_struct(r_var);
        return
    %end
end

if sum(isnan(cout))
    chaine='some edge cost are NaN, to avoid problems in solve graph, prefer to fixe them to Inf \n time : %d' ;
    warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine,i);
end
return

