function [ERR,VD,res]=calc_lagrange_analytique_HP_BV_2EMB(ERR,param,VD,res)
%% Caracteristiques des composants
% ICE data
jMt = VD.MOTH.J_mt;
% Gear ratio
k = VD.ADCPL.kred;
% Motor data
jMe = VD.ACM1.J_mg;
% Battery data
Q0 = VD.BATT.Q0; E = VD.BATT.E; R = VD.BATT.R; iMax = VD.BATT.iMax; iMin = VD.BATT.iMin;

%% Kinematics (ADCPL)
res.wsec_cpl = res.wprim_bv;
res.dwsec_cpl = res.dwprim_bv;

wmt = res.wprim_bv;
dwmt = res.dwprim_bv;
% wmt cannot be < idle speed
indRal = find(wmt<VD.MOTH.ral);
wmt(indRal)=VD.MOTH.ral;
dwmt(indRal)=0;
% Hybrid/thermal modes : both engines are directly connected : proportional speeds
% hyp : the clutch that slips is by the gearbox
wacm1_hyb = k*wmt;
dwacm1_hyb = k*dwmt;
% Full electric mode : machine directly connected to the wheels, 
% thermal engine is disconnected
wacm1_elec = k*res.wprim_bv;
dwacm1_elec = k*res.dwprim_bv;

%% Torques
res.csec_cpl = res.cprim_bv + jMt*dwmt + k*jMe*dwacm1_hyb;
iBreak = find(res.csec_cpl<0);
res.csec_cpl(iBreak) = res.csec_cpl(iBreak)*param.maxRegen;

cacm1_max = zeros(size(res.csec_cpl));
cacm1_max(wacm1_hyb<=VD.ACM1.wBaseAcm) = VD.ACM1.cMaxAcm;
cacm1_max(wacm1_hyb>VD.ACM1.wBaseAcm) = VD.ACM1.pMaxAcm./wacm1_hyb(wacm1_hyb>VD.ACM1.wBaseAcm);
cacm1_min = -cacm1_max; % hyp : machine domain is symetrical

cmt_max = VD.MOTH.coeffCmax(1)*wmt.^2 + VD.MOTH.coeffCmax(2)*wmt + VD.MOTH.coeffCmax(3);

if strcmpi(param.type_moth,'willans')
    cmt_min = repmat(-VD.MOTH.cf,size(wmt));
elseif strcmpi(param.type_moth,'frot_lin') || strcmpi(param.type_moth,'DRIVE')
    cmt_min = VD.MOTH.cf(1) + VD.MOTH.cf(2)*wmt;
else
    ERR =  'type de moteur (param.type_moth) non defini';
end
if ~isempty(ERR), return, end

cacm1_mt_min = (res.csec_cpl - cmt_max)/k;
cacm1_min = max(cacm1_min, cacm1_mt_min);
cacm1_mt_max = (res.csec_cpl - cmt_min)/k;
cacm1_max = min(cacm1_max, cacm1_mt_max);
%TODO : regarder les bornes avec iMin iMax

%% Expression analytique %%

%%% Hybrid mode %%%
col = size(VD.CYCL.vitesse,2);
deltaT = param.pas_temps;

delta1 = (VD.MOTH.coeff(1)*wmt.*res.csec_cpl + VD.MOTH.coeff(2)*wmt)*VD.MOTH.pci ...
    + repmat(param.lambda*E^2/(2*R),1,col);
delta2 = - VD.MOTH.coeff(1)*wmt*k*VD.MOTH.pci;
delta3 = - param.lambda*E/(2*R); % cst
delta4 = - 4*R*VD.ACM1.coeff(2); % cst
delta5 = - 4*R*wacm1_hyb;
delta6 = E^2 - 4*R*VD.ACM1.coeff(1)*wacm1_hyb;

aCopt = 4*delta4*(delta4 - delta2.^2/delta3^2);
bCopt = 4*delta5.*(delta4 - delta2.^2/delta3^2);
cCopt = delta5.^2 - 4*delta6.*delta2.^2/delta3^2;

cacm1_opt_th = (-bCopt +sqrt(bCopt.^2 - 4*aCopt.*cCopt))./(2*aCopt);

H_opt_th = delta1 + delta2.*cacm1_opt_th + delta3*sqrt(delta4*cacm1_opt_th.^2 + delta5.*cacm1_opt_th + delta6);

cacm1_bornes = [cacm1_min; cacm1_max];
H_bornes = repmat(delta1,2,1) + repmat(delta2,2,1).*cacm1_bornes + delta3*sqrt(delta4*cacm1_bornes.^2 + repmat(delta5,2,1).*cacm1_bornes + repmat(delta6,2,1));
[~,iOpt] = min(H_bornes);
iOpt = (0:1:size(H_bornes,2)-1)*size(H_bornes,1) + iOpt; %linear indexing
cacm1_opt_bornes = cacm1_bornes(iOpt);
H_opt_bornes = H_bornes(iOpt);

out_of_bounds = find(cacm1_opt_th<cacm1_min | cacm1_opt_th>cacm1_max);
cacm1_opt_th(out_of_bounds) = cacm1_opt_bornes(out_of_bounds);
cmt_opt_th = res.csec_cpl - k*cacm1_opt_th;
H_opt_th(out_of_bounds) = H_opt_bornes(out_of_bounds);

%%% Full electric mode %%%
cacm1_elec = res.cprim_bv/k + jMe*dwacm1_elec;
pres_elec = cacm1_elec.*wacm1_elec + VD.ACM1.coeff(1)*wacm1_elec + VD.ACM1.coeff(2)*cacm1_elec.^2;
I_eleec = 0.5*(E - sqrt(E^2 - 4*R*pres_elec))/R;
I_eleec(I_eleec<iMin | I_eleec>iMax) = nan; % current out of bounds
H_elec = param.lambda*I_eleec*E;

%%% Solving %%%
H_tot = [H_opt_th; H_elec];
cacm1_tot = [cacm1_opt_th; cacm1_elec];
wacm1_tot = [wacm1_hyb; wacm1_elec];
cmt_tot = [cmt_opt_th; zeros(size(VD.CYCL.vitesse))];
wmt_tot = [wmt; zeros(size(VD.CYCL.vitesse))];

[~,iOpt] = min(H_tot);
iOpt = (0:1:size(H_tot,2)-1)*size(H_tot,1) + iOpt;

res.cacm1 = cacm1_tot(iOpt);
res.cmt = cmt_tot(iOpt);
res.wacm1 = wacm1_tot(iOpt);
res.wmt = wmt_tot(iOpt);
deltaSocOpt = -deltaT*(E - sqrt(E^2 - ...
    4*R*(res.cacm1.*res.wacm1 + VD.ACM1.coeff(1)*res.wacm1 + VD.ACM1.coeff(2)*res.cacm1.^2)))/(72*Q0*R);
res.dcarb = VD.MOTH.coeff(1)*res.wmt.*res.cmt + VD.MOTH.coeff(2)*res.wmt;
delta_carb = deltaT*res.dcarb;

res.cumcarb = cumsum(delta_carb); %g
res.conso = res.cumcarb/VD.MOTH.dens_carb; %l
res.conso100 = res.cumcarb(end)*100./(VD.MOTH.dens_carb*res.distance(end)); %l/100km

soc0 = 100 - VD.INIT.Dod0;
res.soc = soc0 + cumsum(deltaSocOpt);
res.ah = (res.soc - soc0)*Q0;
end