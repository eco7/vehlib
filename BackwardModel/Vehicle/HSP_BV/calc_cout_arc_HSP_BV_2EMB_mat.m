%function [ERR,cout,ind_rap_opt,r]=...
% calc_cout_arc_HSP_BV_2EMB_mat(ERR,param,VD,cprim_bv,wprim_bv,dwprim_bv,Dsoc,elec_pos,soc_p,recalcul,elhyb,cacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,dwacm2_elec,qacm2_elec,cfrott_acm1_elec)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Calcul des cout des arcs cas HSp_2EMB
%
% interface entree :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discretisation etc)
% VD:           structure de description vehicule et composants
% cprim_bv :     Couple sur le primaire de la BV (vecteur sur le cycle)
% wprim_bv :    vitesse sur le primaire de la BV  (vecteur sur le cycle)
% dprim_bv :    derivee de la vitesse sur le primaire de la BV (vecteur sur le cycle)
% Dsoc :        variation de soc sur l'eventail des arcs possible
% elec_pos :    mode electrique possible (vecteur sur le cycle)
% soc_p :       soc a l'instant precedent
% i :           instant dans le cycle
% recalcul :    si on est en phase de recalcul (grahe deja resolu) recalcul =1 sinon 0 ou pas passer dans l'interface
% elhyb :       mode elec/hybride/thermique en phase recalcul uniquement
% cacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,dwacm2_elec,qacm2_elec,cfrott_acm1
% : en recalcul on utilise les valeurs calcule en elec
%
% interface de sortie
% cout :        cout des arcs de l'eventail
% ibat,ubat ...:variable vehicule utile uniquement en recalcul sinon on ne renvoie que le cout
%
% 10-05-16(EV): Creation 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ERR,cout,ind_rap_opt,r]=calc_cout_arc_HSP_BV_2EMB_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,soc_p,recalcul,elhyb,r_elec)
%ibat,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,Dsoc,elhyb,cfrott_acm1,cfrein_meca_pelec_mot]=...
%function [ERR,cout,indice_rap_opt,r]=calc_cout_arc_HP_BV_2EMB_matcalc_cout_arc_HSP_BV_2EMB_mat(ERR,param,VD,cprim_bv,wprim_bv,dwprim_bv,Dsoc,elec_pos,soc_p,recalcul,elhyb,cacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,dwacm2_elec,qacm2_elec,cfrott_acm1)
 
r_var={'ibat','ubat','Rbat','u0bat','RdF','dcarb','cmt','wmt','dwmt','cacm1','wacm1','dwacm1','qacm1',...
    'cacm2','wacm2','dwacm2','qacm2','Dsoc','elhyb','cfrott_acm1','cfrein_meca_pelec_mot','cprim_bv','wprim_bv','dwprim_bv',...
    'csec_bv','wsec_bv','dwsec_bv'};

ind_rap_opt=-1*ones(size(Dsoc));

[Li,Co]=size(soc_p);
[sx,sc]=size(Dsoc);

if sum(sum(isnan(soc_p)))==Li*Co
    ERR=MException('BackwardModel:calc_cout_arc_HSP_2EMB', 'appel a calc_cout_arc_HSP_2EMB : tout les elements d''un des vecteur precedent a NaN');
    [cout,ibat,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,Dsoc,elhyb,cfrott_acm1,cfrein_meca_pelec_mot]=backward_erreur(size(Dsoc));
     [~,r]=affect_struct(r_var);
    return
end

% Connexion
wsec_bv=wprim_red;
dwsec_bv=dwprim_red;

% Calcul des conditions en amont de la boite de vitesse
% NB si on est en rapport libre ou optimaux on as des matrices 
% avec en nb de ligne le nb de rapport de boite plus pm
if param.bv_acm1==1 % cas acm1 en amont de la BV
    csec_bv=cprim_red;
    [ERR,cprim_bv,wprim_bv,dwprim_bv]=calc_bv(VD,csec_bv,wsec_bv,dwsec_bv,0);
elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
    [ERR,~,wprim_bv,dwprim_bv]=calc_bv(VD,zeros(size(wsec_bv)),wsec_bv,dwsec_bv,0);
end

if ~isempty(ERR)
    ResXml=struct([]); 
    return;
end

nrap=size(wprim_bv,1);

% Calcul de la vitesse min possible du moteur thermique pour verifier que le mode tout elec est possible
wmt_min=zeros(1,size(wprim_bv,2)); % limitation sur vit max du moth et vit max de la generatrice
VD.BV.k1=[0 VD.BV.k];
% choix VD.BV.k(1) car mode tout elec - rechtest_HSP_BV_2EMBv1eck ?
wmt_min(VD.CYCL.vitesse>VD.ECU.vmaxele)=VD.MOTH.ral;
if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
   % if VD.CYCL.ntypcin==3 % cas rapport de boite imposee
        %wmt_min(VD.CYCL.vitesse>VD.ECU.vmaxele)=VD.MOTH.ral;
    %elseif VD.CYCL.ntypcin==1 % cas rapport de boite optimisee
        if VD.CYCL.ntypcin==1 % cas rapport de boite optimisee
        %wmt_min(:,VD.CYCL.vitesse>VD.ECU.vmaxele)=VD.MOTH.ral;
        ind_vit0=find(VD.CYCL.vitesse==0); %On mettra a Inf les cas arret vehicule autre que point mort
        if param.bv_acm1==1 % cas acm1 en amont de la BV
            cprim_bv(2:end,ind_vit0)=Inf;
            cprim_bv=permute(cprim_bv,[3 2 1]);
        end
        wprim_bv=permute(wprim_bv,[3 2 1]);
        dwprim_bv=permute(dwprim_bv,[3 2 1]);
    end
end

% calcul batterie
% calcul ibat a partir de Dsoc
RdF=ones(size(Dsoc));
RdF(Dsoc>0)=VD.BATT.RdFarad;

% if VD.CYCL.ntypcin==1 % cas NEDC
%    ibat=-repmat(Dsoc./RdF,[1 1 sz])*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100./pas_temps;
% else % cas NEDC_BV
%    ibat=-Dsoc./RdF*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100./pas_temps;
% end 

ibat=-Dsoc./RdF*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100/param.pas_temps;

[ERR,ubat,~,~,E,R]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat,soc_p,0);

pbat=ubat.*ibat;

[ERR,pacc]=calc_acc(VD);
% pacc=repmat(pacc,[sx 1 sz]);
pacc_mat=pacc(1);

% connexion
if param.bv_acm1==1 % cas acm1 en amont de la BV
    wacm1=wprim_bv;
    dwacm1=dwprim_bv;
elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
    wacm1=wprim_red;
    dwacm1=dwprim_red;
end
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
cfrott_acm1_hp=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,abs(wacm1));

ind_wacm1_max=wacm1>max(VD.ACM1.Regmot_cmax);

if (isfield(param,'fit_pertes') & param.fit_pertes==1)   
elseif ~isfield(param,'fit_pertes') | param.fit_pertes~=1
  
    %% calcul du cout des arcs dans le mode hybride serie
    [VD]=ge_optimum_perte(VD,25,25,0); % recherche courbe optimal du groupe
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        cacm1_hs=cprim_bv+VD.ACM1.J_mg*dwacm1;
    elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
        cacm1_hs=cprim_red+VD.ACM1.J_mg*dwacm1;
    end
    [ERR,qacm1_hs]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hs,wacm1,dwacm1);
    pelecacm1_hs=cacm1_hs.*wacm1+qacm1_hs;

    if VD.CYCL.ntypcin==3 | (VD.CYCL.ntypcin==1 & param.bv_acm1==2)% cas rapport de boite imposee ou libre et acm1 directement connectee au red final
        pge=repmat(pelecacm1_hs,sx,1)-pbat+pacc_mat;
    elseif VD.CYCL.ntypcin==1 & param.bv_acm1==1 % cas rapport de boite optimisee et acm1 en amont BV
        pge=repmat(pelecacm1_hs,sx,1)-repmat(pbat,[1 1 nrap])+pacc_mat;
    end
    
    wacm2_hs=interp1(VD.GE.pge_opti,VD.GE.wmt_opti,pge);
    qacm2_hs=interp1(VD.GE.pge_opti,VD.GE.qacm2_opti,pge);
    cacm2_hs=-(pge+qacm2_hs)./wacm2_hs;
    
    cmt_hs=-cacm2_hs;    
    wmt_hs=wacm2_hs;
    
    %calc total fuel consumption in series hybrid mode 
    [ERR,dcarb_hs]=calc_mt(ERR,VD.MOTH,cmt_hs,wmt_hs,0,1,0);

    % On met a inf les cas ou la puissance du groupe serait <0 ou > a pge.max
    ind_ge_n=find(pge<0 & pge>VD.GE.pge_opti(end));
    dcarb_hs(ind_ge_n)=Inf;
    
    % On met a inf les cas ou le mode hybride serie est impossible car cacm1_max<cprim_bv
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        ind_hs_n=find(cacm1_max<cprim_bv+VD.ACM1.J_mg*dwacm1);
    elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
        ind_hs_n=find(cacm1_max<cprim_red+VD.ACM1.J_mg*dwacm1);
    end
     
    if VD.CYCL.ntypcin==3 | (VD.CYCL.ntypcin==1 & param.bv_acm1==2)% cas rapport de boite imposee ou libre et acm1 directement connectee au red final
        dcarb_hs(:,ind_hs_n)=Inf;
    elseif VD.CYCL.ntypcin==1 % cas rapport de boite optimisee et acm1 en amont BV
        dcarb_hs(repmat(ind_hs_n,[sx 1 1]))=Inf;
        dcarb_hs(repmat(ind_wacm1_max,[sx 1 1]))=Inf; % On met a inf les cas ou la vitesse acm1 >wacm1_max (important cas rapport optimises)
        
        dcarb_hs(isnan(dcarb_hs))=Inf;
        [dcarb_hs,ind_rap_opt_hs]=min(dcarb_hs,[],3);
        %plus min de dcarb et rapport opt en hs
    end
    
    dcarb_hs(isnan(dcarb_hs))=Inf;
 
       
    %% calcul du cout des arcs dans le cas hybride //
   
    wsec_emb1=wprim_bv;
    dwsec_emb1=dwprim_bv;
    
    if VD.CYCL.ntypcin==3 % cas rapport de boite imposee
        [ERR,~,wprim_emb1,dwprim_emb1]=calc_emb(VD,ones(size(wsec_emb1)),wsec_emb1,dwsec_emb1,ones(size(wsec_emb1))); % calcul des vitesses uniquement
    elseif VD.CYCL.ntypcin==1 % cas rapport de boite optimisee
        [ERR,~,wprim_emb1,dwprim_emb1]=calc_emb(VD,ones(size(wsec_emb1)),wsec_emb1,dwsec_emb1,ones(size(wsec_emb1)),3); % calcul des vitesses uniquement
    end
    
    % connexions
    wacm2_hp=wprim_emb1;
    dwacm2_hp=diff(wacm2_hp,1,2);
    
    dwacm2_hp=[zeros(1,1,nrap) dwacm2_hp];
    %dwacm2_hp=dwprim_emb1;
    
    % torque max/min calc
    cacm2_max_hp=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2_hp);
    cacm2_min_hp=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmin_mot(1,:))),wacm2_hp);
    
    % Recherche du mode de fonctionnement qui minimise la conso
    % On discretise cacm1 entre cacm1_min et cacm1_max
    cacm1_mat=Inf*ones(floor(max(max((cacm1_max-cacm1_min)/param.pas_cacm1)))+2,sc);
    for ii=1:sc
        cacm1_vec=unique([cacm1_min(ii) fliplr(-param.pas_cacm1:-param.pas_cacm1:cacm1_min(ii))  0:param.pas_cacm1:cacm1_max(ii) cacm1_max(ii)]');
        cacm1_mat(1:length(cacm1_vec),ii)=cacm1_vec;
    end
    % cas EM1 non alimente il faudrait aussi prevoir cas EM2 non alimente
    % et calculer couple EM1 a partir inversion carto EM1.
    
    if VD.CYCL.ntypcin==3   % cas rapport de boite imposee
        cacm1_mat_ri=[cacm1_mat ; cfrott_acm1_hp];
        [sz,~]=size(cacm1_mat_ri);
        wacm1_mat=repmat(wacm1,sz,1);
        [ERR,qacm1_mat]=calc_pertes_acm(ERR,VD.ACM1,cacm1_mat_ri,wacm1_mat,0);
        qacm1_mat(sz,:)=0;
        pelec_acm1_mat=cacm1_mat_ri.*wacm1_mat+qacm1_mat;
        pelec_acm1_mat(sz,:)=0;
        
        
        % On passe a des tableau 3D %creation of 3D tables Dsoc*temps*cacm1
        %qacm1_tab=repmat(reshape(qacm1_mat',[1 sc sz]),sx,1);
        pelec_acm1_tab=repmat(reshape(pelec_acm1_mat',[1 sc sz]),sx,1);
        %wacm1_tab=repmat(reshape(wacm1_mat',[1 sc sz]),sx,1);
        
        cacm1_tab=repmat(reshape(cacm1_mat_ri',[1 sc sz]),sx,1);
        pbat_tab=repmat(pbat,[1 1 sz ]);
        wacm2_tab=repmat(wacm2_hp,[sx 1 sz]);
        pelec_acm2_tab=pbat_tab-pelec_acm1_tab-pacc(1);
        %losses for ACM2
        [ERR,qacm2_tab]=calc_pertes_acm_fw(ERR,VD.ACM2,pelec_acm2_tab,wacm2_tab,repmat(dwacm2_hp,[sx 1 sz]));
        cacm2_tab=(pelec_acm2_tab-qacm2_tab)./wacm2_tab;
        cacm2_tab(isnan(cacm2_tab))=Inf;
        
        cacm2_tab( cacm2_tab>repmat(cacm2_max_hp,[sx 1 sz]) | cacm2_tab<repmat(cacm2_min_hp,[sx 1 sz]))= Inf;
        qacm2_tab( cacm2_tab>repmat(cacm2_max_hp,[sx 1 sz]) | cacm2_tab<repmat(cacm2_min_hp,[sx 1 sz]))= Inf;
        
        cacm2_tab=(pelec_acm2_tab-qacm2_tab)./wacm2_tab;
        cacm2_tab(isnan(cacm2_tab))=Inf;
        
        % connexions
        if param.bv_acm1==1 % cas acm1 en amont de la BV
            csec_emb1_hp_tab=repmat(cprim_bv,[sx 1 sz])-cacm1_tab+VD.ACM1.J_mg*repmat(dwacm1,[sx 1 sz]);
        elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
            csec_bv_tab=repmat(cprim_red+VD.ACM1.J_mg*dwacm1,[sx 1 sz])-cacm1_tab;
            [ERR,cprim_bv_tab]=calc_bv(VD,csec_bv_tab,repmat(wsec_bv,[sx 1 sz]),repmat(dwsec_bv,[sx 1 sz]),0);
            csec_emb1_hp_tab=cprim_bv_tab;
        end
        
        cprim_emb1_hp_tab=csec_emb1_hp_tab;
        
        wmt_hp_tab=repmat(wprim_emb1,[sx 1 sz]);
        dwmt_hp_tab=diff(wmt_hp_tab,1,2);
        dwmt_hp_tab=[zeros(sx,1,sz) dwmt_hp_tab];
       
        cmt_hp_tab=cprim_emb1_hp_tab-cacm2_tab+VD.ACM2.J_mg*repmat(dwacm2_hp,[sx 1 sz] )+VD.MOTH.J_mt.*dwmt_hp_tab;
        cmt_hp_tab(isinf(cmt_hp_tab))=Inf; % On passe de valeur -Inf à Inf
        [ERR,dcarb_hp_tab,cmt_hp_tab]=calc_mt(ERR,VD.MOTH,cmt_hp_tab,wmt_hp_tab,0,1,0);
        
        %% On met a Inf les cas ou le moth fonctionera en frein moteur
        % On met a inf les dcarb des cmt impossible
        
        dcarb_hp_tab(isnan(cmt_hp_tab))=Inf;
        dcarb_hp_tab(cmt_hp_tab<0)=Inf;
        
        % Attention comme on ne passe pas pour l'instant dans calc_emb1 on
        % valide des cas de fonctionnement impossible ou (Csec <0) et wprim > wsec.
        % Il faut imposer dcarb Inf sur ces points
        dcarb_hp_tab (csec_emb1_hp_tab<0 & ( repmat(wprim_emb1,[sx 1 sz]) > repmat(wsec_emb1,[sx 1 sz]) ))=Inf;
        
        [dcarb_hp,ind_min_dcarb_hp_tab]=min(dcarb_hp_tab,[],3);
        
    
    elseif VD.CYCL.ntypcin==1  % cas rapport de boite optimisee
        if param.bv_acm1==1 % cas acm1 en amont de la BV
            cacm1_3D=[repmat(cacm1_mat,[1 1 nrap]) ; cfrott_acm1_hp];
            [sz,~]=size(cacm1_3D);
            wacm1_3D=repmat(wacm1,[sz 1 1]);
        elseif param.bv_acm1==2 % cas acm1 en amont de la BV
            cacm1_2D=[cacm1_mat; cfrott_acm1_hp];
            cacm1_3D=repmat(cacm1_2D,[1 1 nrap]) ;
            [sz,~]=size(cacm1_3D);
            wacm1_3D=repmat(wacm1,[sz 1 nrap]);
        end

        
        [ERR,qacm1_3D]=calc_pertes_acm(ERR,VD.ACM1,cacm1_3D,wacm1_3D,0);
        qacm1_3D(sz,:,:)=0;
        pelec_acm1_3D=cacm1_3D.*wacm1_3D+qacm1_3D;
        pelec_acm1_3D(sz,:)=0;
        
        
        % On passe a des tableau 4D 
        %qacm1_4D=repmat(permute(qacm1_3D,[4 2 3 1]),[sx 1 1 1]);
        pelec_acm1_4D=repmat(permute(pelec_acm1_3D,[4 2 3 1]),[sx 1 1 1]);
        %wacm1_4D=repmat(permute(wacm1_3D,[4 2 3 1]),[sx 1 1 1]);
        cacm1_4D=repmat(permute(cacm1_3D,[4 2 3 1]),[sx 1 1 1]);
        
        pbat_4D=repmat(pbat,[1 1 nrap sz ]);
        wacm2_4D=repmat(wacm2_hp,[sx 1 1 sz]);
        pelec_acm2_4D=pbat_4D-pelec_acm1_4D-pacc(1);
        %losses for ACM2
        [ERR,qacm2_4D]=calc_pertes_acm_fw(ERR,VD.ACM2,pelec_acm2_4D,wacm2_4D,repmat(dwacm2_hp,[sx 1 1 sz]));
        cacm2_4D=(pelec_acm2_4D-qacm2_4D)./wacm2_4D;
        cacm2_4D(isnan(cacm2_4D))=Inf;
        
        cacm2_4D( cacm2_4D>repmat(cacm2_max_hp,[sx 1 1 sz]) | cacm2_4D<repmat(cacm2_min_hp,[sx 1 1 sz]))= Inf;
        qacm2_4D( cacm2_4D>repmat(cacm2_max_hp,[sx 1 1 sz]) | cacm2_4D<repmat(cacm2_min_hp,[sx 1 1 sz]))= Inf;
        
        cacm2_4D=(pelec_acm2_4D-qacm2_4D)./wacm2_4D;
        cacm2_4D(isnan(cacm2_4D))=Inf;
        
         % connexions

         if param.bv_acm1==1 % cas acm1 en amont de la BV
             csec_emb1_hp_4D=repmat(cprim_bv,[sx 1 1 sz])-cacm1_4D+VD.ACM1.J_mg*repmat(dwacm1,[sx 1 1 sz]);
         elseif param.bv_acm1==2 % cas acm1 en amont de la BV
            csec_bv_2D=repmat(cprim_red+VD.ACM1.J_mg*dwacm1,[sz 1])-cacm1_2D;
            [ERR,cprim_bv_3D]=calc_bv(VD,csec_bv_2D,repmat(wsec_bv,[sz 1]),repmat(dwsec_bv,[sz 1]),0,3);
            cprim_bv_4D=repmat(permute(cprim_bv_3D,[4 2 3 1]),[sx 1 1 1]);
            csec_emb1_hp_4D=cprim_bv_4D;
         end
        
        %[ERR,cprim_emb1,~,~]=calc_emb(VD,csec_emb1_hp,repmat(wsec_emb1,[sx 1 sz]),repmat(dwsec_emb1,[sx 1 sz]),ones(size(csec_emb1_hp)));
        cprim_emb1_hp_4D=csec_emb1_hp_4D;
        
        %csec_emb2_hp=cprim_emb1-cacm2_hp+VD.ACM2.J_mg*repmat(dwacm2_hp,[sx 1]);
        
        wmt_hp_4D=repmat(wprim_emb1,[sx 1 1 sz]);
        dwmt_hp_4D=diff(wmt_hp_4D,1,2);
        dwmt_hp_4D=[zeros([sx 1 nrap sz]) dwmt_hp_4D];
        %% Attention : si on passe en recalcul avec des rapports de boites imposees pour les pas de temps ou il y as eu changement de rapport,
        % on ne calcule pas le meme dwmt que dans le premier passage avant optimisation des rapports.
        
        cmt_hp_4D=cprim_emb1_hp_4D-cacm2_4D+VD.ACM2.J_mg*repmat(dwacm2_hp,[sx 1 1 sz] )+VD.MOTH.J_mt.*dwmt_hp_4D;
        cmt_hp_4D(isinf(cmt_hp_4D))=Inf; % On passe de valeur -Inf à Inf
        [ERR,dcarb_hp_4D,cmt_hp_4D]=calc_mt(ERR,VD.MOTH,cmt_hp_4D,wmt_hp_4D,0,1,0);
        
        %% On met a Inf les cas ou le moth fonctionera en frein moteur
        % On met a inf les dcarb des cmt impossible
        
        dcarb_hp_4D(isnan(cmt_hp_4D))=Inf;
        dcarb_hp_4D(cmt_hp_4D<0)=Inf;
        
        % Attention comme on ne passe pas pour l'instant dans calc_emb1 on
        % valide des cas de fonctionnement impossible ou (Csec <0) et wprim > wsec.
        % Il faut imposer dcarb Inf sur ces points
        dcarb_hp_4D (csec_emb1_hp_4D<0 & ( repmat(wprim_emb1,[sx 1 1 sz]) > repmat(wsec_emb1,[sx 1 1 sz]) ))=Inf;
        
        [dcarb_hp_3D,ind_min_dcarb_hp_4D]=min(dcarb_hp_4D,[],4);
        [dcarb_hp,ind_rap_opt_hp]=min(dcarb_hp_3D,[],3);
        
    end
    
    dcarb_hs_hp(:,:,1)=dcarb_hs;
    dcarb_hs_hp(:,:,2)=dcarb_hp;
    [dcarb,ind_hs_hp]=min(dcarb_hs_hp,[],3);
    if VD.CYCL.ntypcin==1
        if param.bv_acm1==1 % cas acm1 en amont de la BV
            indice_rap_opt_hs_hp(:,:,1)=ind_rap_opt_hs;
        elseif param.bv_acm1==2 % cas acm1 en amont de la BV
            indice_rap_opt_hs_hp(:,:,1)=ones(size(ind_rap_opt_hp)); % dans ce cas en HS on est au PM
        end
        indice_rap_opt_hs_hp(:,:,2)=ind_rap_opt_hp;
        ind_rap_opt=mat_ind(ind_hs_hp,3,indice_rap_opt_hs_hp);
    end
   
end


% Critere de cout des arcs
if nargin ==9 || nargin==12 && recalcul==0
    % mode tout elec moteur thermique arrete
    dcarb(1,find(elec_pos==1))=0;
    % on est en mode "tout elec" cmt=0 mais le moteur thermique doit tourner a wmt_min
    % a priori on as deja trouver wmt=wmt_min mais on est pas sur d'avoir
    % converge exactement vers cmt=0
    [ERR,dcarb(1,find(elec_pos==0.5))]=calc_mt(ERR,VD.MOTH,zeros(1,sum(elec_pos==0.5)),wmt_min(1,find(elec_pos==0.5)),0,1,0);
   
    
elseif nargin==12 && recalcul==1 
    if VD.CYCL.ntypcin==3   % cas rapport de boite imposee
        [cmt_hp,wmt_hp,dwmt_hp,cacm2_hp,qacm2_hp,cacm1_hp]=...
            mat_ind(ind_min_dcarb_hp_tab,3,cmt_hp_tab,wmt_hp_tab,dwmt_hp_tab,cacm2_tab,qacm2_tab,cacm1_tab);
        ind_cacm1=ind_min_dcarb_hp_tab;
        if param.bv_acm1==2 % cas acm1 directement connectee au red final
            [cprim_bv_hp]=mat_ind(ind_min_dcarb_hp_tab,3,cprim_bv_tab);
        end
    elseif VD.CYCL.ntypcin==1
        [cmt_hp_3D,wmt_hp_3D,dwmt_hp_3D,cacm1_hp_3D,cacm2_hp_3D,wacm2_hp_3D,qacm2_hp_3D]=mat_ind(ind_min_dcarb_hp_4D,4,cmt_hp_4D,wmt_hp_4D,dwmt_hp_4D,cacm1_4D,cacm2_4D,wacm2_4D,qacm2_4D);
        [cmt_hp,wmt_hp,dwmt_hp,cacm1_hp,cacm2_hp,wacm2_hp,dwacm2_hp,ind_cacm1,qacm2_hp]=...
            mat_ind(ind_rap_opt_hp,3,cmt_hp_3D,wmt_hp_3D,dwmt_hp_3D,cacm1_hp_3D,cacm2_hp_3D,wacm2_hp_3D,dwacm2_hp,ind_min_dcarb_hp_4D,qacm2_hp_3D);
        if param.bv_acm1==1 % cas acm1 en amont du reducteur final
           [cmt_hs,wmt_hs]=mat_ind(ind_rap_opt_hs,3,cmt_hs,wmt_hs);
        end
        if param.bv_acm1==2 % cas acm1 directement connectee au red final
            [cprim_bv_tab]=mat_ind(ind_min_dcarb_hp_4D,4,cprim_bv_4D);
            [cprim_bv_hp]=mat_ind(ind_rap_opt_hp,3,cprim_bv_tab);
           % [cprim_bv]=mat_ind(,3,cprim_bv_tab);
        end
    end
    
    % cas hybride et generale (ont pourrais limiter le calcul au cas hybride si besoin
    % cas hs elhyb=1
    % cas hp elhyb =2
    I_e0=find(elhyb==0);
    I_e05=find(elhyb==0.5);
    %I_e1=find(elhyb==1);

    
    dwmt=zeros(1,sc);
    wmt=zeros(1,sc);
    cmt=zeros(1,sc);
    
    dcarb(I_e0)=0;
    wmt(I_e05)=wmt_min(I_e05);
    [ERR,dcarb(I_e05)]=calc_mt(ERR,VD.MOTH,cmt(I_e05),wmt(I_e05),0,1,0);
    
    %elhyb(dcarb_hs==dcarb)=1; deja a 1 si hybride
    elhyb(dcarb==dcarb_hp & elhyb==1)=2;
    I_e2=find(elhyb==2);  
    I_e1=find(elhyb==1);
    
    cmt(I_e1)=cmt_hs(I_e1);
    wmt(I_e1)=wmt_hs(I_e1);
    
    cmt(I_e2)=cmt_hp(I_e2);
    wmt(I_e2)=wmt_hp(I_e2);
    dwmt(I_e2)=dwmt_hp(I_e2);
    %dcarb(I_e2)=dcarb_hp(I_e2);
    if param.verbose>=3
        assignin('base','elhyb',elhyb);
        assignin('base','cmt',cmt);
        assignin('base','wmt',wmt);
    end
    
end  

if param.verbose>=3  
    assignin('base','dcarb',dcarb);   
end

cout=dcarb*param.pas_temps;
cout(isnan(cout))=Inf;

if nargin==12 && recalcul==1 % cas recalcul   
    if VD.CYCL.ntypcin==1 % rapport de boite libre
            [wprim_bv,dwprim_bv]=mat_ind(ind_rap_opt,3,wprim_bv,dwprim_bv);
            if param.bv_acm1==1 % cas acm1 en amont du reducteur final
                [cprim_bv,wacm1,dwacm1,cfrott_acm1]=mat_ind(ind_rap_opt,3,cprim_bv,wacm1,dwacm1,cfrott_acm1_hp);
                [cacm1_hs,cacm2_hs,wacm2_hs,qacm2_hs]=mat_ind(ind_rap_opt_hs,3,cacm1_hs,cacm2_hs,wacm2_hs,qacm2_hs);
            else
                cprim_bv=NaN*ones(size(wprim_red));
            end
            
    end
    
    cacm1=NaN*ones(size(wprim_bv));
    cacm1(I_e0)=r_elec.cacm1_elec(I_e0);
    cacm1(I_e05)=r_elec.cacm1_elec(I_e05);  
    cacm1(I_e1)=cacm1_hs(I_e1);
    cacm1(I_e2)=cacm1_hp(I_e2);
    
    wacm1(I_e05)=r_elec.wacm1_elec(I_e05);
    wacm1(I_e0)=r_elec.wacm1_elec(I_e0);
    dwacm1(I_e05)=r_elec.dwacm1_elec(I_e05);
    dwacm1(I_e0)=r_elec.dwacm1_elec(I_e0);
    
    cprim_bv(I_e0)=r_elec.cprim_bv_elec(I_e0);
    cprim_bv(I_e05)=r_elec.cprim_bv_elec(I_e05);
    if param.bv_acm1==2 % cas acm1 directement connectee au red final, en hybride serie bv ne sert a rien
        cprim_bv(I_e1)=0;
        cprim_bv(I_e2)=cprim_bv_hp(I_e2);
    end
    
    wprim_bv(I_e0)=r_elec.wprim_bv_elec(I_e0);
    wprim_bv(I_e05)=r_elec.wprim_bv_elec(I_e05);
    dwprim_bv(I_e0)=r_elec.dwprim_bv_elec(I_e0);
    dwprim_bv(I_e05)=r_elec.dwprim_bv_elec(I_e05);
    
    ind_rap_opt(I_e0)=r_elec.indice_rap_opt_elec(I_e0);
    ind_rap_opt(I_e05)=r_elec.indice_rap_opt_elec(I_e05);
    
    if VD.CYCL.ntypcin==1 % rapport de boite optimise
        VD.CYCL.rappvit=ind_rap_opt-1;
        VD.CYCL.ntypcin=3;
    end
    
    [ERR,csec_bv]=calc_bv_fw(ERR,VD,cprim_bv,wprim_bv,dwprim_bv,0,0); % Pour le calcul du frein meca on repart en fw
    wsec_bv=wprim_red;
    dwsec_bv=dwprim_red;
    
    % Recalcul cacm2
    cacm2=zeros(1,sc);
    wacm2=zeros(1,sc);
    dwacm2=zeros(1,sc);
    
    %dwacm2(I_e0)=dwacm1(I_e0);
    %dwacm2(I_e05)=dwacm1(I_e05);
    
    dwacm2(I_e2)=dwacm2_hp(I_e2); % et la cas HS ???
    
    cacm2(I_e0)=r_elec.cacm2_elec(I_e0);
    cacm2(I_e05)=r_elec.cacm2_elec(I_e05);
    cacm2(I_e1)=cacm2_hs(I_e1);
    cacm2(I_e2)=cacm2_hp(I_e2);
    
    wacm2(I_e05)=r_elec.wacm2_elec(I_e05);
    wacm2(I_e0)=r_elec.wacm2_elec(I_e0);
    wacm2(I_e1)=wacm2_hs(I_e1);
    wacm2(I_e2)=wacm2_hp(I_e2);
    dwacm2(I_e05)=r_elec.dwacm2_elec(I_e05);
    dwacm2(I_e0)=r_elec.dwacm2_elec(I_e0);
     
    % calcul puissance elec acm1
    if (isfield(param,'fit_pertes') & param.fit_pertes==1)
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,0,1);
    else
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
    end

    qacm1(I_e0)=r_elec.qacm1_elec(I_e0); 
    qacm1(I_e05)=r_elec.qacm1_elec(I_e05);
    
    % Si on est en hybride parallele et que acm1 non alimente alors il faut remettre qacm1 a zero
    cond_acm1_na=find(elhyb==2 & ind_cacm1==sz);
    qacm1(cond_acm1_na)=0;
    cfrott_acm1=zeros(1,sc);
    cfrott_acm1(elhyb>0.7)=0;
    cfrott_acm1(cond_acm1_na)=cacm1(cond_acm1_na);
    
    % en elec il faut aussi remettre cfrott_acm1 a sa valeur
    ind_elec_acm1_na=find(elhyb(r_elec.ind_acm1_na)==0); % valeur ou en mode elec acm1 na
    cfrott_acm1(r_elec.ind_acm1_na(ind_elec_acm1_na))=r_elec.cfrott_acm1(r_elec.ind_acm1_na(ind_elec_acm1_na));
     
    % calcul puissance elec acm2
    if (isfield(param,'fit_pertes') & param.fit_pertes==1)
        [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,0,1);
    else
        [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0);
    end
    
    
    % Pour boulcer le bilan de puissance on remet les pertes calculees en fw
    % A quoi sert le calcul bw du dessus alors !!!
    qacm2(I_e0)=r_elec.qacm2_elec(I_e0); 
    qacm2(I_e05)=r_elec.qacm2_elec(I_e05);
    qacm2(I_e2)=qacm2_hp(I_e2); 
    qacm2(I_e1)=qacm2_hs(I_e1); 
    
    
    % si on est en recup et que acm1 non alimente
    cfrein_meca_pelec_mot=zeros(1,sc);

    Rbat=R;
    u0bat=E*ones(size(cprim_bv));
    
    [ERR,r]=affect_struct(r_var);
    
   % I=find(elhyb==0 & cprim_bv<0 & qacm1+qacm2>(-cacm1.*wacm1-cacm2.*wacm2))
    %I=find(elhyb==0 & cprim_bv<0 & qacm1>-cacm1.*wacm1);% recup avec plus de ppelec_acm1(56)ertes dans acm1 que de puissance surl'arbre
   % cfrott_acm1(I)=max(interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,wacm1(I)),cprim_bv(I)); %la machine elec ne freinera pas plus que le couple de freinage
    %cacm1(I)=0;
   % cacm2(I)=0;
    %cfrein_meca_pelec_mot(I)=1;
    %qacm1(I)=0;
    %qacm2(I)=0;

end

ind_inf=find(sum(cout==Inf)==size(cout,1));
if ~isempty(ind_inf)
    chaine=' all edge are impossible, the systems possibilites does not allow to fullfil the required systems operating point ! : %d \n indice colonne inf : %s  \n';
    ERR=MException('BackwardModel:calc_cout_arc_HPDP_EPI_SURV',chaine,num2str(ind_inf));
    [cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=backward_erreur(length(Dsoc));
    [ERR,r]=affect_struct(r_var);
    return
end

if sum(isnan(cout))
    chaine='some edge cost are NaN, to avoid problems in solve graph, prefer to fixe them to Inf \n time : %d' ;
    warning('BackwardModel:calc_cout_arc_HPDP_EPI_SURV',chaine,i);
end

return

