% function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,r_elec]=calc_limites_HSP_BV_2EMB(ERR,param,VD,cprim_bv,wprim_bv,dwprim_bv);
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul des limites du graphes.
% On ne calcule ici que les soc min et max sans se preocuper
% de la remonte du graphe ou de soc <0 ou >100%.
% La limite min doit etre calcule avec precision (mode tout elec)
% la limite max peut etre calcule "grossierement" d'eventuels arcs impossibles
% dans le graphe seront elimine par la suite
%
% interface entree :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discreetisation etc)
% VD :          structure de description vehicule et composants
% cprim_bv  :   Couple sur le primaire de la BV (vecteur sur le cycle)
% wprim_bv  :   vitesse sur le primaire de la BV(vecteur sur le cycle)
% dwprim_bv :   derivee de la vitesse sur le primaire de la BV (vecteur sur le cycle)
% 
% interface de sortie
% ibat_max :    courant max mode tout elec (en recup c'est le courant de recup en tout elec)
% ibat_min :    courant min recharge max ( en recup courant recup plus recharge par moth)
% elec_pos :    vecteur indiquant si mode elec possible , ou zero si impossible
% soc_min :     soc correspondant a ibat_max
% soc_max :     soc correspondant a ibat_min
% Dsoc_min :    variation soc entre deux instant correspondant a ibat_max
%               (atention a integre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% Dsoc_max :    variation soc entre deux instant correspondant a ibat_min
%               (atention a integre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% r_elec    :   utilisee ensuite en recalcul pour les mode tout elec (evite de refaire les optimisation en recalcul
%
% 06-06-16: Creation 

function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,r_elec]=calc_limites_HSP_BV_2EMB(~,param,VD,cprim_red,wprim_red,dwprim_red);

r_var_elec={'cacm1_elec','wacm1_elec','dwacm1_elec','qacm1_elec','cacm2_elec','wacm2_elec','dwacm2_elec',...
    'qacm2_elec','cprim_bv_elec','wprim_bv_elec','dwprim_bv_elec','cfrott_acm1','indice_rap_opt_elec','ind_acm1_na'};

ERR=[];
ii_max=size(VD.CYCL.temps);
soc_min=zeros(ii_max);
soc_max=zeros(ii_max);
Dsoc_min=zeros(ii_max);
Dsoc_max=zeros(ii_max);


indice_rap_opt_elec=zeros(ii_max);

% Connexion
wsec_bv=wprim_red;
dwsec_bv=dwprim_red;

% Calcul des conditions en amont de la boite de vitesse
% NB si on est en rapport libre ou optimaux on as des matrices 
% avec en nb de ligne le nb de rapport de boite plus pm
if param.bv_acm1==1 % cas acm1 en amont de la BV
    csec_bv=cprim_red;
    [ERR,cprim_bv,wprim_bv,dwprim_bv]=calc_bv(VD,csec_bv,wsec_bv,dwsec_bv,0);
elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
    [ERR,~,wprim_bv,dwprim_bv]=calc_bv(VD,zeros(size(wsec_bv)),wsec_bv,dwsec_bv,0);
end

if ~isempty(ERR)
    ResXml=struct([]); 
    return;
end

[nrap,sc]=size(wprim_bv);

%% recherche du mode tout electrique a chaque pas de temps
  
% Calcul de la vitesse min possible du moteur thermique pour verifier que l emode tout elec est possible
wmt_min=0*ones(1,size(wprim_bv,2)); % limitation sur vit max du moth et vit max de la generatrice
if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
        wmt_min(VD.CYCL.vitesse>VD.ECU.vmaxele)=VD.MOTH.ral;
end

% calcul de la vitesse des machines en tout elec 
wacm2_elec=wprim_bv;
dwacm2_elec=dwprim_bv;

if param.bv_acm1==1 % cas acm1 en amont de la BV
    wacm1_elec=wprim_bv;
    dwacm1_elec=dwprim_bv;
elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
    wacm1_elec=wprim_red;
    dwacm1_elec=dwprim_red;
end

% calcul des pertes par frottement des machines 
cfrott_acm2=interp1(VD.ACM2.Regmot_pert,VD.ACM2.Cpl_frot,abs(wacm2_elec));
cfrott_acm1_elec=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,abs(wacm1_elec));
cfrott_acm1=zeros(1,size(wprim_bv,2));
% Recherche du mode de fonctionnement qui minimise les pertes
% On discretise cacm1 entre cacm1_min et cacm1_max

% On doit venir verifier que wacm1 n'est pas superieure a la limite autorisee
ind_wacm_max=wacm1_elec>max(VD.ACM1.Regmot_cmax);
if (VD.CYCL.ntypcin==1 & param.bv_acm1==1 &  sum(sum(ind_wacm_max(2:nrap,:)) == (nrap-1)) >=1) |...% rapport de boite libre
        sum(ind_wacm_max)>=1 % cas rapport de boite imposee
    
    chaine='error in calc_limites_HP_BV_2EMB : motor speed upper than maximum speed allow' ;
    ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
    if param.verbose>=1
        warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine);
        Indices = find(sum(wacm1_elec(2:nrap,:)>max(VD.ACM1.Regmot_cmax)) == nrap-1);
    end
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=backward_erreur(length(VD.CYCL.temps),0,0);
    return
end

cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec);
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec);

cacm2_min=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2_elec);
cacm2_max=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2_elec);


if VD.CYCL.ntypcin==3 % cas rapport de boite imposee
    cacm1_mat=Inf*ones(floor(max((cacm1_max-cacm1_min)/param.pas_cacm1))+2,sc);
    for ii=1:sc
        cacm1_vec=[cacm1_min(ii) fliplr(-param.pas_cacm1:-param.pas_cacm1:cacm1_min(ii))  0:param.pas_cacm1:cacm1_max(ii) cacm1_max(ii)]';
        cacm1_mat(1:length(cacm1_vec),ii)=cacm1_vec;
    end
    % On rajoute en derniere ligne les couples de frottement cas EM1 non
    % alimente et une ligne avec cprim_bv cas EM2 non alimente (donc emb1 ouvert)
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        cacm1_mat=[cacm1_mat;  cfrott_acm1_elec+VD.ACM1.J_mg*dwacm1_elec;  cprim_bv+VD.ACM1.J_mg*dwacm1_elec];
    elseif param.bv_acm1==2 % cas acm1 en amont de la BV
        cacm1_mat=[cacm1_mat;  cfrott_acm1_elec+VD.ACM1.J_mg*dwacm1_elec;  cprim_red+VD.ACM1.J_mg*dwacm1_elec];
    end
elseif VD.CYCL.ntypcin==1 % cas rapport de boite libre
    % cas du point mort
    % NB la premiere ligne de cprim_bv a deja ete mis a Inf si couple secondaire >0 par calc_bv
    % On mettra a Inf les cas arret vehicule autre que point mort
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        ind_vit0=find(VD.CYCL.vitesse==0);
        cprim_bv(2:end,ind_vit0)=Inf;
        
        cacm1_3D=Inf*ones(floor(max(max((cacm1_max-cacm1_min)/param.pas_cacm1)))+3,sc,nrap);
        for jj=1:nrap
            for ii=1:sc
                cacm1_vec=[cacm1_min(jj,ii) fliplr(-param.pas_cacm1:-param.pas_cacm1:cacm1_min(jj,ii))  0:param.pas_cacm1:cacm1_max(jj,ii) cacm1_max(jj,ii)]';
                cacm1_3D(1:length(cacm1_vec),ii,jj)=cacm1_vec;
            end
        end
        % On rajoute en derniere ligne les couples de frottement cas EM1 non
        % alimente et une ligne avec cprim_bv cas EM2 non alimente (donc emb1 ouvert)
        cacm1_3D=[cacm1_3D;  permute((cfrott_acm1_elec+VD.ACM1.J_mg*dwacm1_elec),[3 2 1]);  permute((cprim_bv+VD.ACM1.J_mg*dwacm1_elec),[3 2 1])];
    elseif param.bv_acm1==2 % cas acm1 en amont de la BV
        cacm1_3D=Inf*ones(floor(max((cacm1_max-cacm1_min)/param.pas_cacm1))+3,sc,nrap);
        cacm1_mat=Inf*ones(floor(max((cacm1_max-cacm1_min)/param.pas_cacm1))+2,sc);
        for ii=1:sc
            cacm1_vec=[cacm1_min(ii) fliplr(-param.pas_cacm1:-param.pas_cacm1:cacm1_min(ii))  0:param.pas_cacm1:cacm1_max(ii) cacm1_max(ii)]';
            cacm1_mat(1:length(cacm1_vec),ii)=cacm1_vec;
        end
        
        cacm1_3D=repmat([cacm1_mat;  cfrott_acm1_elec+VD.ACM1.J_mg*dwacm1_elec;  cprim_red+VD.ACM1.J_mg*dwacm1_elec],[1 1 nrap]);
    end
end


% Calcul des pertes dans les machines
if VD.CYCL.ntypcin==3 % cas rapport de boite imposee
    [sx,~]=size(cacm1_mat);
    wacm1_mat=repmat(wacm1_elec,sx,1);
    wacm2_mat=repmat(wacm2_elec,sx,1);
    
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        cacm2_mat=repmat(cprim_bv+VD.ACM1.J_mg*dwacm1_elec+VD.ACM2.J_mg*dwacm2_elec,[sx 1])-cacm1_mat;
    elseif param.bv_acm1==2 % cas acm1 en amont de la BV
        csec_bv_mat=repmat(cprim_red+VD.ACM1.J_mg*dwacm1_elec,[sx 1])-cacm1_mat;
        [ERR,cprim_bv_mat]=calc_bv(VD,csec_bv_mat,repmat(wsec_bv,[sx 1]),repmat(dwsec_bv,[sx 1]),0);
        cacm2_mat=cprim_bv_mat+repmat(VD.ACM2.J_mg*dwacm2_elec,[sx 1]);
        % indice des points du cycle ou cprim_red<0 et PM
        % seule le cas acm2 non alimente est possible (derniere ligne)
        I_pm=find(cprim_red<0 & VD.CYCL.rappvit==0);
        cacm2_mat(1:end-1,I_pm)=Inf;
        cacm1_mat(1:end-1,I_pm)=Inf;
    end
    
    % Cas EM2 non alimente emb1 ouvert
    cacm2_mat(sx,:)=0;
    wacm2_mat(sx,:)=0;
    
    % calcul des pertes dans les machines
    [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1_mat,wacm1_mat,0);
    [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2_mat,wacm2_mat,0);
    
    % Derniere ligne cas frottement acm1 ou acm2 non alimente
    qacm2(sx,:)=0;
    qacm1(sx-1,:)=0;
    
     % Calcul des auxiliaires electriques
    [ERR,pacc]=calc_acc(VD);
    
    % calcul puissance motelec
    pacm1_elec=VD.VEHI.nbacm1*(cacm1_mat.*repmat(wacm1_elec,sx,1)+qacm1);
    pacm1_elec(sx-1,:)=0;
    pacm2_elec=(cacm2_mat.*repmat(wacm2_elec,sx,1)+qacm2);
    pacm2_elec(sx)=0;
    %
    pres=pacm1_elec+pacm2_elec+pacc(1);
    %
    [ERR,ibat_max,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,100-VD.INIT.Dod0,0,-1,0,param);
    
    % minimisation du courant batterie
    % NB attention la minimisation des pertes conduit a une erreur dans le
    % cas ou l'on considère acm1 na et couple de frottement (recip). On
    % diminue alors le couple de recup donc les pertes mais on augmente ibat !!!
    [~,ind_min]=min(ibat_max);
    [cacm1_elec,cacm2_elec,wacm2_elec,qacm1_elec,qacm2_elec]=mat_ind(ind_min,2,cacm1_mat,cacm2_mat,wacm2_mat,qacm1,qacm2);
    
    
    %Dans les cas ou l'on a converger vers des points ou acm1 non alimente
    %On remet dans qacm1 les pertes dues aux frottement + inertie
    %Detection des indices ou acm1 non alimente
    ind_acm1_na=find(ind_min==sx-1);
    %cfrott_acm1(ind_acm1_na)=cacm1_elec(ind_acm1_na);
     
    if param.bv_acm1==2 % cas acm1 en amont de la BV
        [cprim_bv]=mat_ind(ind_min,2,cprim_bv_mat);
    end
    
    % Si on ne peut pas faire de mode tout elec cprim>cacm1_max+cacm2_max
    % On prend les couples max
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        ind_melec_np=find(cacm1_max+cacm2_max<cprim_bv+VD.ACM1.J_mg*dwacm1_elec+VD.ACM2.J_mg*dwacm2_elec);
    elseif param.bv_acm1==2 % cas acm1 en amont de la BV
        cprim_bv_max=cacm2_max-VD.ACM2.J_mg*dwacm2_elec;
        [ERR,csec_bv_max]=calc_bv_fw([],VD,cprim_bv_max,wprim_bv,dwprim_bv,0);
        ind_melec_np=find(cacm1_max+csec_bv_max-VD.ACM1.J_mg*dwacm1_elec<cprim_red);
    end
    
    cacm1_elec(ind_melec_np)=cacm1_max(ind_melec_np);
    cacm2_elec(ind_melec_np)=cacm2_max(ind_melec_np);
    [ERR,qacm1_elec(ind_melec_np)]=calc_pertes_acm(ERR,VD.ACM1,cacm1_max(ind_melec_np),wacm1_elec(ind_melec_np),0);
    [ERR,qacm2_elec(ind_melec_np)]=calc_pertes_acm(ERR,VD.ACM2,cacm2_max(ind_melec_np),wacm2_elec(ind_melec_np),0);
    
    % Si recup max on met les couples a leur valeur min
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        ind_min=find(cacm1_min+cacm2_min>cprim_bv+VD.ACM1.J_mg*dwacm1_elec+VD.ACM2.J_mg*dwacm2_elec);
    elseif param.bv_acm1==2 % cas acm1 en amont de la BV
        cprim_bv_min=cacm2_min-VD.ACM2.J_mg*dwacm2_elec;
        [ERR,csec_bv_min]=calc_bv_fw([],VD,cprim_bv_min,wprim_bv,dwprim_bv,0);
        ind_min=find(cacm1_min+csec_bv_min-VD.ACM1.J_mg*dwacm1_elec>cprim_red);
    end
    
    cacm1_elec(ind_min)=cacm1_min(ind_min);
    cacm2_elec(ind_min)=cacm2_min(ind_min);
    [ERR,qacm1_elec(ind_min)]=calc_pertes_acm(ERR,VD.ACM1,cacm1_min(ind_min),wacm1_elec(ind_min),0);
    [ERR,qacm2_elec(ind_min)]=calc_pertes_acm(ERR,VD.ACM2,cacm2_min(ind_min),wacm2_elec(ind_min),0);
    
    % Pour etre coherent avec le cas rapport de boite libre on ressort les
    % grandeurs suivantes.
    cprim_bv_elec=cprim_bv;
    wprim_bv_elec=wprim_bv;
    dwprim_bv_elec=dwprim_bv;
    
    cacm1_max_elec=cacm1_max;
    cacm2_max_elec=cacm2_max;
    
    % On recalul ibat_max une fois les limites prise en compte
    % calcul puissance motelec
    pacm1_elec=VD.VEHI.nbacm1*(cacm1_elec.*wacm1_elec+qacm1_elec);
    pacm2_elec=(cacm2_elec.*wacm2_elec+qacm2_elec);
    
    pres=pacm1_elec+pacm2_elec+pacc;
    
    [ERR,ibat_max,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,100-VD.INIT.Dod0,0,-1,0,param);
    
    % Cas ou l'on a explose les limites en recup
    % On doit imposer que Ibat_max = VD.BATT.Ibat_min
    ind_rec_max_bat=find(isnan(ibat_max) & cprim_red<0);
    ibat_max(ind_rec_max_bat)=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension
    
    elec_pos=ones(size(cprim_bv_elec));
    elec_pos(wmt_min~=0)=0.5; % moth au ralenti mais cmt =0, la batterie fournie toute la puissance
    elec_pos(ind_melec_np)=0; % les deux moteurs ne suffisent pas
    elec_pos(isnan(ibat_max))=0; % la batterie ne peut fournir toute la puissance mode elec impossible
    elec_pos_elec=elec_pos;

elseif VD.CYCL.ntypcin==1 % cas rapport de boite libre
    [sx,~]=size(cacm1_3D );
  
    wacm2_3D=repmat(permute(wacm2_elec,[3 2 1]),[sx 1 1]);
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        wacm1_3D=repmat(permute(wacm1_elec,[3 2 1]),[sx 1 1]);
        
        cacm2_3D=repmat(permute((cprim_bv+VD.ACM1.J_mg*dwacm1_elec+VD.ACM2.J_mg*dwacm2_elec),[3 2 1]),[sx 1 1])-cacm1_3D;
    elseif param.bv_acm1==2 % cas acm1 en amont de la BV
        wacm1_3D=repmat(permute(wacm1_elec,[3 2 1]),[sx 1 nrap]);
        
        csec_bv_2D=repmat(cprim_red+VD.ACM1.J_mg*dwacm1_elec,[sx 1])-cacm1_3D(:,:,1);
        [ERR,cprim_bv_3D]=calc_bv(VD,csec_bv_2D,repmat(wsec_bv,[sx 1]),repmat(dwsec_bv,[sx 1]),0,3);
        cacm2_3D=cprim_bv_3D+VD.ACM2.J_mg*repmat(permute(dwacm2_elec,[3 2 1]),[sx 1 1]);
    end
    
    % cas ou acm2 non connecte emb1 ouvert
    cacm2_3D(sx,:,:)=0;
    wacm2_3D(sx,:,:)=0;
    
     % calcul des pertes dans les machines
    [ERR,qacm1_3D]=calc_pertes_acm(ERR,VD.ACM1,cacm1_3D,wacm1_3D,0);
    [ERR,qacm2_3D]=calc_pertes_acm(ERR,VD.ACM2,cacm2_3D,wacm2_3D,0);
    
    % Derniere ligne cas frottement acm1 ou acm2 non alimente
    qacm2_3D(sx,:,:)=0;
    qacm1_3D(sx-1,:,:)=0;
    
    %qacm_3D=qacm1_3D+qacm2_3D;
    
    % Cas ou l'on ne peut pas faire de mode tout elec cprim>cacm1_max+cacm2_max
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        ind_max_acm=((cacm1_max+cacm2_max)<cprim_bv+VD.ACM1.J_mg*dwacm1_elec+VD.ACM2.J_mg*dwacm2_elec) | ind_wacm_max; % indices des points ou mode elec impossible
    elseif param.bv_acm1==2 % cas acm1 en amont de la BV
        cprim_bv_max=cacm2_max-VD.ACM2.J_mg*dwacm2_elec;
        [ERR,csec_bv_max]=calc_bv_fw([],VD,cprim_bv_max,wprim_bv,dwprim_bv,0);
        ind_max_acm=(repmat(cacm1_max,[nrap 1])+csec_bv_max-VD.ACM1.J_mg*repmat(dwacm1_elec,[nrap 1]))<repmat(cprim_red,[nrap 1]) | repmat(ind_wacm_max,[nrap 1]) ;
    end
   
    % On met les pertes a Inf si mode elec impossible
    qacm1_3D(repmat(permute(ind_max_acm,[3 2 1]),[sx 1 1]))=Inf;
    qacm2_3D(repmat(permute(ind_max_acm,[3 2 1]),[sx 1 1]))=Inf;
    
    % pas de temps ou le mode elec n'est jamais possible
    ind_melec_np = find(sum(ind_max_acm(2:end,:))==2 & VD.CYCL.vitesse~=0);
    
    % Calcul des auxiliaires electriques
    [ERR,pacc]=calc_acc(VD);
    
    % calcul puissance motelec
    pacm1_3D=VD.VEHI.nbacm1*(cacm1_3D.*wacm1_3D+qacm1_3D);
    pacm1_3D(sx-1,:,:)=0;
    pacm2_3D=(cacm2_3D.*wacm2_3D+qacm2_3D);
    pacm2_3D(sx,:,:)=0;
    
    pres_3D=pacm1_3D+pacm2_3D+pacc(1);
    %
    [ERR,ibat_max_3D,~,~,~,~,~,RdF_3D]=calc_batt(ERR,VD.BATT,param.pas_temps,pres_3D,100-VD.INIT.Dod0,0,-1,0,param);
    
    % Cas ou aucun courant batterie n'est possible
    %ind_ibat_nan=permute(sum(isnan(ibat_max_3D))==sx,[3 2 1]);
    
    % En recup max on doit imposer ibat=Ibat_min
    ind_rec_max_bat=(isnan(ibat_max_3D) & pres_3D<0 & repmat(permute(cprim_red,[3 2 1]),[sx 1 nrap])<0);
    ibat_max_3D(ind_rec_max_bat)=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension
    
    %% Verifier l'inversion avec ligne dessus
    % Cas ou aucun courant batterie n'est possible
    ind_ibat_nan=permute(sum(isnan(ibat_max_3D))==sx,[3 2 1]);
    
    [ibat_max_2D,ind_min_ibat_max_3D]=min(ibat_max_3D,[],3);
    [ibat_max,ind_min_ibat_max_2D]=min(ibat_max_2D);
    [cacm1_2D,cacm2_2D,wacm1_2D,wacm2_2D,qacm1_2D,qacm2_2D,RdF_2D]=...
        mat_ind(ind_min_ibat_max_3D,3,cacm1_3D,cacm2_3D,wacm1_3D,wacm2_3D,qacm1_3D,qacm2_3D,RdF_3D);
    [cacm1_elec,cacm2_elec,wacm1_elec,wacm2_elec,qacm1_elec,qacm2_elec,RdF,indice_rap_opt_elec]=...
        mat_ind(ind_min_ibat_max_2D,2,cacm1_2D,cacm2_2D,wacm1_2D,wacm2_2D,qacm1_2D,qacm2_2D,RdF_2D,ind_min_ibat_max_3D);
    
    % Dans les cas ou l'on a converger vers des points ou acm1 non alimente
    % On remet dans qacm1 les pertes dues aux frottement + inertie
    % Detection des indices ou acm1 non alimente
    ind_acm1_na=find(ind_min_ibat_max_2D==sx-1);
    cfrott_acm1(ind_acm1_na)=cacm1_elec(ind_acm1_na);
     
    % pour les cas ou mode elec impossible quelquesoit rapvit, on prend les
    % couple max
    cacm1_elec(ind_melec_np)=cacm1_max(ind_melec_np);
    cacm2_elec(ind_melec_np)=cacm2_max(ind_melec_np);
    cacm1_max_elec=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec);
    cacm2_max_elec=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2_elec);
    [ERR,qacm1_elec(ind_melec_np)]=calc_pertes_acm(ERR,VD.ACM1,cacm1_max_elec(ind_melec_np),wacm1_elec(ind_melec_np),0);
    [ERR,qacm2_elec(ind_melec_np)]=calc_pertes_acm(ERR,VD.ACM2,cacm2_max_elec(ind_melec_np),wacm2_elec(ind_melec_np),0);
    
     % Si recup max on prend les couple smin
    cacm1_min_elec=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1_elec);
    cacm2_min_elec=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmin_mot(1,:))),wacm2_elec);
    
    
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        [cprim_bv_elec,wprim_bv_elec,dwprim_bv_elec,dwacm1_elec,dwacm2_elec,cfrott_acm1_elec]=...
            mat_ind(indice_rap_opt_elec,2,cprim_bv,wprim_bv,dwprim_bv,dwacm1_elec,dwacm2_elec,cfrott_acm1_elec);
        ind_min=find(cacm1_min_elec+cacm2_min_elec>cprim_bv_elec+VD.ACM1.J_mg*dwacm1_elec+VD.ACM2.J_mg*dwacm2_elec);
    elseif param.bv_acm1==2 % cas acm1 en amont de la BV
        [wprim_bv_elec,dwprim_bv_elec,dwacm2_elec]=...
            mat_ind(indice_rap_opt_elec,2,wprim_bv,dwprim_bv,dwacm2_elec);
        cprim_bv_elec=cacm2_elec-VD.ACM2.J_mg*dwacm2_elec;
        cprim_bv_min=cacm2_min_elec-VD.ACM2.J_mg*dwacm2_elec;
        [ERR,csec_bv_min]=calc_bv_fw([],VD,cprim_bv_min,wprim_bv_elec,dwprim_bv_elec,0,indice_rap_opt_elec-1);
        ind_min=find(cacm1_min+csec_bv_min-VD.ACM1.J_mg*dwacm1_elec>cprim_red);
    end
   
    
    cacm1_elec(ind_min)=cacm1_min(ind_min);
    cacm2_elec(ind_min)=cacm2_min(ind_min);
    [ERR,qacm1_elec(ind_min)]=calc_pertes_acm(ERR,VD.ACM1,cacm1_min_elec(ind_min),wacm1_elec(ind_min),0);
    [ERR,qacm2_elec(ind_min)]=calc_pertes_acm(ERR,VD.ACM2,cacm2_min_elec(ind_min),wacm2_elec(ind_min),0);
    
    % On doit recalculer ibat_max une fois les limtes prise en compte sur acm1 et acm2
    % Calcul des auxiliaires electriques
   
    % calcul puissance motelec
    pacm1_elec=VD.VEHI.nbacm1*(cacm1_elec.*wacm1_elec+qacm1_elec);
    pacm2_elec=(cacm2_elec.*wacm2_elec+qacm2_elec);
    
    pres=pacm1_elec+pacm2_elec+pacc;
    
    [ERR,ibat_max(ind_melec_np),~,~,~,~,~,RdF(ind_melec_np)]=calc_batt(ERR,VD.BATT,param.pas_temps,pres(ind_melec_np),100-VD.INIT.Dod0,0,-1,0,param);
    
    elec_pos=ones(size(wprim_bv));
    elec_pos(2:end,wmt_min~=0)=0.5; % moth au ralenti mais cmt =0, la batterie fournie toute la puissance
    elec_pos(ind_max_acm)=0; % les deux moteurs ne suffisent pas
    elec_pos(ind_ibat_nan)=0; % la batterie ne peut fournir toute la puissance mode elec impossible
    [elec_pos]=mat_ind(indice_rap_opt_elec,2,elec_pos);
end


% il faut repartir en fw pour recalculer le frein meca et le fonctionnement
% des machines quand on est dans les cas de recup max Ibat==Ibat_min
%% 02-05-16 Attention en rapport de boite libre pas sur que ca marche tout ca
Ibat_min=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,100-VD.INIT.Dod0);
ind_recalcul=find(ibat_max==Ibat_min);
if ~isempty(ind_recalcul)
    [ERR,Ubat_min]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,Ibat_min,100-VD.INIT.Dod0,0);
    pbat_min=Ibat_min*Ubat_min;
    [ERR,qacm1_max]=calc_pertes_acm(ERR,VD.ACM1,cacm1_max_elec(ind_recalcul),wacm1_elec(ind_recalcul),dwacm1_elec(ind_recalcul));
    [ERR,qacm2_max]=calc_pertes_acm(ERR,VD.ACM2,cacm2_max_elec(ind_recalcul),wacm2_elec(ind_recalcul),dwacm2_elec(ind_recalcul));
    for i = 1: length(ind_recalcul)
        ii=ind_recalcul(i);
        %      Si on tape dans les limites on se fout de savoir qui fait quoi (ibat sera limite a son min) donc acm1 fera tout ce qu'elle peut puis acm2 si insuffisant
        if cacm1_max_elec(ii)*wacm1_elec(ii)+qacm1_max(i)+pacc(1)>pbat_min % la machine em1 fera tout le boulot on recalcule son coupl en fw
            pacm1_elec(ii)=pbat_min-pacc(ii);
            [ERR,qacm1_elec(ii)]=calc_pertes_acm_fw(ERR,VD.ACM1,pacm1_elec(ii),wacm1_elec(ii),dwacm1_elec(ii));
            cacm1_elec(ii)=(pacm1_elec(ii)-qacm1_elec(ii))./wacm1_elec(ii);
            qacm2_elec(ii)=0;
            cacm2_elec(ii)=0;
            wacm2_elec(ii)=0;
            if param.bv_acm1==1 % cas acm1 en amont de la BV
                cprim_bv_elec(ii)= cacm1_elec(ii) - VD.ACM1.J_mg*dwacm1_elec(ii) ;
            elseif param.bv_acm1==2 % cas acm1 en amont de la BV, si seul acm1 fonctionne cprim_bv_elec=0
                cprim_bv_elec(ii)= 0;
            end
        elseif cacm1_max_elec(ii)*wacm1_elec(ii)+qacm1_max(i)+pacc(1)<pbat_min & cacm1_max_elec(ii)*wacm1_elec(ii)+qacm1_max(i)+cacm2_max_elec(ii)*wacm2_elec(ii)+qacm2_max(i)+pacc(1)<pbat_min% em1 et em2 font tout le boulot
            cacm1_elec(ii)=cacm1_max_elec(ii);
            qacm1_elec(ii)=qacm1_max(i);
            pacm1_elec(ii)=cacm1_max_elec(ii)*wacm1_elec(ii)+qacm1_max(i);
            pacm2_elec(ii)=pbat_min-pacc(ii)-pacm1_elec(ii);
            [ERR,qacm2_elec(ii)]=calc_pertes_acm_fw(ERR,VD.ACM2,pacm2_elec(ii),wacm2_elec(ii),dwacm2_elec(ii));
            if param.bv_acm1==1 % cas acm1 en amont de la BV
                cprim_bv_elec(ii)= cacm1_elec(ii) +  cacm2_elec(ii) - VD.ACM1.J_mg*dwacm1_elec(ii) - VD.ACM1.J_mg*dwacm2_elec(ii);
            elseif param.bv_acm1==2 % cas acm1 en amont de la BV, si seul acm1 fonctionne cprim_bv_elec=0
                cprim_bv_elec(ii)= cacm2_elec(ii) - VD.ACM1.J_mg*dwacm2_elec(ii);
            end
        else % ce cas ne peut normalement pas se produire car deja limite avant calcul
        end
    end
end

% calcul de la pbat_max
ibat_max(isnan(ibat_max))=interp1(VD.BATT.DoD_Ibat_max,VD.BATT.Ibat_max,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension

% Sur certain point de recup ont peut arriver a faire plus de pertes dans acm1
% que l'on ne recupere de puissance sur le primaire reducteur, on regarde
% alors ce qu'ils se passe si on alimente pas la machine
% Dans le cas rapp_vit optimise et acm1 en amont BV clea ne doit plus se produire 
% Att : Si  on arrive dans des cas ou le couple de frottement de la machine
% est plus faible que la couple ramene du reducteur ce fontionnement est
% impossible il faut donc quand meme alimté la machine pour suivre le
% cycle.
pres_acm_na=pres;
pres_acm_na(cprim_red<0)=pacc(cprim_red<0);
[ERR,ibat_max_acm1_na]=calc_batt(ERR,VD.BATT,param.pas_temps,pres_acm_na,100-VD.INIT.Dod0,0,-1,0,param);

%I=find(cprim_red<0 & qacm1_elec+qacm2_elec>(-cacm1_elec.*wacm1_elec-cacm2_elec.*wacm2_elec) & elec_pos==1);
%I=find(cprim_red<0 & qacm1_elec+qacm2_elec>(-cacm1_elec.*wacm1_elec-cacm2_elec.*wacm2_elec) & elec_pos==1 &...
%    cfrott_acm1_elec>cprim_red-VD.ACM1.J_mg*dwacm1_elec);

if param.bv_acm1==1 % cas acm1 en amont de la BV
    
    I=find(cprim_red<0 & qacm1_elec+qacm2_elec>(-cacm1_elec.*wacm1_elec-cacm2_elec.*wacm2_elec) & elec_pos==1 &...
    cfrott_acm1_elec>cprim_bv_elec);
    %cfrott_acm1(I)=max(cfrott_acm1_elec(I),cprim_bv_elec(I));
    cacm1_elec(I)=cfrott_acm1(I);
    cprim_bv_elec(I)=cacm1_elec(I)-VD.ACM1.J_mg*dwacm1_elec(I);
    
elseif param.bv_acm1==2 % cas acm1 en aval de la BV
    
    I=find(cprim_red<0 & qacm1_elec+qacm2_elec>(-cacm1_elec.*wacm1_elec-cacm2_elec.*wacm2_elec) & elec_pos==1 &...
    cfrott_acm1_elec>cprim_red+VD.ACM1.J_mg*dwacm1_elec);
    %cfrott_acm1(I)=max(cfrott_acm1_elec(I),cprim_red(I)-VD.ACM1.J_mg*dwacm1_elec(I));
    cfrott_acm1(I)=cfrott_acm1_elec(I);
    cacm1_elec(I)=cfrott_acm1(I);
    cprim_bv_elec(I)=0;
    cprim_red_elec(I)=cacm1_elec(I)-VD.ACM1.J_mg*dwacm1_elec(I);
    ind_acm1_na=sort([I ind_acm1_na]); % on doit rajouter ces cas ou acm1 na
end

ibat_max(I)=ibat_max_acm1_na(I);

cacm2_elec(I)=0;
qacm1_elec(I)=0;
qacm2_elec(I)=0;
wacm2_elec(I)=0;
dwacm2_elec(I)=0;


[ERR,r_elec]=affect_struct(r_var_elec);

soc_min(1)=100-VD.INIT.Dod0;
Dsoc_min(1)=0;
for ii=2:length(VD.CYCL.temps)
    soc_min(ii)=soc_min(ii-1)-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_min(ii)=-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

if param.verbose>=2
    if isfield(VD.CYCL,'vitesse')
    figure(3)
    clf
    hold on
    plot(VD.CYCL.temps,ibat_max,'b',VD.CYCL.temps,VD.CYCL.vitesse,'k')
    legend('ibat max','vitesse')
    title('courant min max fin calc limite')
    grid
    end
    
    figure(4)
    clf
    hold on
    plot(VD.CYCL.temps,soc_min,'b',VD.CYCL.temps,60+Dsoc_min,'g')
    legend('soc min','Dsoc min')
    title('soc min max fin calc limite')
    grid
       
end



%% recherche de la pbat_min (mode hybride)
% calcul du couple max moteur thermique
% il faut verifier la possibilite de faire de l'hybride serie, ou de l'hybride //
% Cas hybride serie il faut que cacm1 suffisent a fournir le couple au reducteur

if param.bv_acm1==1 % cas acm1 en amont de la BV
    wacm1=wprim_bv;
    dwacm1=dwprim_bv;
elseif param.bv_acm1==2 % cas acm1 directement connecee au red final
    wacm1=wprim_red;
    dwacm1=dwprim_red;
end

% recherche des indices ou les modes hybrides series sont possible et impossible (limitation sur cacm1)
if param.bv_acm1==1 % cas acm1 en amont de la BV
    ind_hs=find(cacm1_max>=cprim_bv+VD.ACM1.J_mg*dwacm1);
    ind_hs_n=find(cacm1_max<cprim_bv+VD.ACM1.J_mg*dwacm1);
elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
    ind_hs=find(cacm1_max>=cprim_red+VD.ACM1.J_mg*dwacm1);
    ind_hs_n=find(cacm1_max<cprim_red+VD.ACM1.J_mg*dwacm1);
end

% cas hybride // il faut que wmt_max soit possible
ind_hp=find(VD.MOTH.wmt_maxi>=wprim_bv);
ind_hp_n=find(VD.MOTH.wmt_maxi<wprim_bv);

% on recherche la puissance max possible du moteur thermique 
% Si l'on est en mode hybride serie
[~,i_pmt_max]=max(VD.MOTH.pmt_max);
wmt_pmax_hs=VD.MOTH.wmt_max(i_pmt_max);

pmax_mt_hs=interp1(VD.MOTH.wmt_max,VD.MOTH.pmt_max,wmt_pmax_hs);
cmt_max_hs=pmax_mt_hs./wmt_pmax_hs;
cacm2_max_hs=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wmt_pmax_hs);

if VD.CYCL.ntypcin==1 && param.bv_acm1==2 % cas rapport de boite libre et acm1 directement connectee au red final
    cmt_max_hs=min(cmt_max_hs,cacm2_max_hs)*ones(size(wprim_red));
else
    cmt_max_hs=min(cmt_max_hs,cacm2_max_hs)*ones(size(wprim_bv));
end
cmt_max_hs(ind_hs_n)=0;
cmt_max_hs(ind_hs_n)=0;

% calcul du couple max du moteur en hp
% si l'on est en hybride // (vitesse imposee)
[ERR,~,wmt_hp,dwmt_hp]=calc_emb(VD,zeros(size(wprim_bv)),wprim_bv,dwprim_bv,ones(size(wprim_bv)));

wacm2_hp=wmt_hp;
dwacm2_hp=dwmt_hp;

cmt_max_hp=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt_hp);
cmt_max_hp(ind_hp_n)=0;

%cmt_max=max(cmt_max_hs,cmt_max_hp);

% connexion en hybride serie
cacm2_hs=-cmt_max_hs;
wacm2_hs=wmt_pmax_hs;

% On verifie quel'on passe le cycle
% indice ou hybride serie impossible et hybride parallele impossible 
if VD.CYCL.ntypcin==3 % cas rapport de boite imposee
    ind_wacm_max_hp = wacm1>max(VD.ACM1.Regmot_cmax) | wacm2_hp>max(VD.ACM1.Regmot_cmax);
    ind_wacm_max_hs = wacm1>max(VD.ACM1.Regmot_cmax) | wacm2_hs>max(VD.ACM1.Regmot_cmax);
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        ind=find( (cacm1_max<cprim_bv+VD.ACM1.J_mg*dwacm1 | ind_wacm_max_hs) & ...%hybride serie
            (VD.MOTH.wmt_maxi<wprim_bv | ( cacm1_max+cacm2_max+cmt_max_hp<cprim_bv+VD.ACM1.J_mg*dwacm1+VD.ACM2.J_mg*dwacm2_hp+VD.MOTH.J_mt*dwmt_hp ) | ind_wacm_max_hp));
    elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
        cprim_bv_max=cacm2_max-VD.ACM2.J_mg*dwacm2_hp+cmt_max_hp-VD.MOTH.J_mt*dwmt_hp;
        [ERR,csec_bv_max]=calc_bv_fw([],VD,cprim_bv_max,wprim_bv,dwprim_bv,0);
        ind=find( (cacm1_max<cprim_red+VD.ACM1.J_mg*dwacm1 | ind_wacm_max_hs) & ...%hybride serie
            ( VD.MOTH.wmt_maxi<wprim_bv | ( cacm1_max+csec_bv_max<cprim_red+VD.ACM1.J_mg*dwacm1 ) | ind_wacm_max_hp));
    end
elseif VD.CYCL.ntypcin==1 % cas rapport de boite libre
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        ind_wacm_max_hp = wacm1>max(VD.ACM1.Regmot_cmax) | wacm2_hp>max(VD.ACM1.Regmot_cmax);
        ind_wacm_max_hs = wacm1>max(VD.ACM1.Regmot_cmax) | wacm2_hs>max(VD.ACM1.Regmot_cmax);
        ind_2D= (cacm1_max<cprim_bv+VD.ACM1.J_mg*dwacm1 | ind_wacm_max_hs) & ... % hybride serie
            ( VD.MOTH.wmt_maxi<wprim_bv | ( cacm1_max+cacm2_max+cmt_max_hp<cprim_bv+VD.ACM1.J_mg*dwacm1+VD.ACM2.J_mg*dwacm2_hp ) | ind_wacm_max_hp ); % hybride //
        ind=find( sum(ind_2D(2:end,:))==(nrap-1) & VD.CYCL.vitesse~=0 );
    elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
        ind_wacm_max_hp = repmat(wacm1,[nrap 1])>max(VD.ACM1.Regmot_cmax) | wacm2_hp>max(VD.ACM1.Regmot_cmax);
        ind_wacm_max_hs = wacm1>max(VD.ACM1.Regmot_cmax) | wacm2_hs>max(VD.ACM1.Regmot_cmax);
        cprim_bv_max=cacm2_max-VD.ACM2.J_mg*dwacm2_hp+cmt_max_hp-VD.MOTH.J_mt*dwmt_hp;
        [ERR,csec_bv_max]=calc_bv_fw([],VD,cprim_bv_max,wprim_bv,dwprim_bv,0);
        ind_2D= repmat((cacm1_max<cprim_red+VD.ACM1.J_mg*dwacm1 | ind_wacm_max_hs),[nrap 1]) & ... % hybride serie
            ( VD.MOTH.wmt_maxi<wprim_bv | ( repmat(cacm1_max,[nrap 1])+csec_bv_max<repmat(cprim_red,[nrap 1])+VD.ACM1.J_mg*repmat(dwacm1,[nrap 1])) | ind_wacm_max_hp ); % hybride //
        ind=find( sum(ind_2D(2:end,:))==(nrap-1) & VD.CYCL.vitesse~=0 );
    end
end

% indice ou hybride impossible 
if ~isempty(ind)
    chaine=' Les performances des machines ne permettent pas de passer le cycle aux instant suivants : \n %s \n';
    ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine,num2str(ind));
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    %[ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=backward_erreur(length(VD.CYCL.temps),0,0);
end


% calcul du courant min en hs
[ERR,qacm2_hs]=calc_pertes_acm(ERR,VD.ACM2,cacm2_hs,wacm2_hs*ones(size(cacm2_hs)),0);
pelecacm2_hs=cacm2_hs.*wacm2_hs+qacm2_hs;

if param.bv_acm1==1 % cas acm1 en amont de la BV
    cacm1_hs=cprim_bv+VD.ACM1.J_mg*dwacm1;
elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
    cacm1_hs=cprim_red+VD.ACM1.J_mg*dwacm1;
end

[ERR,qacm1_hs]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hs,wacm1,dwacm1);
pelecacm1_hs=cacm1_hs.*wacm1+qacm1_hs;

pres_hs=pelecacm1_hs+pelecacm2_hs+pacc(1); % Normalement il y as un survolteur entre pbat et pres !!  

pbat_hs=pres_hs;
[ERR,ibat_min_hs,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_hs,100-VD.INIT.Dod0,0,-1,0,param); 
ibat_min_hs(isnan(ibat_min_hs))=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0);


if VD.CYCL.ntypcin==1 % cas rapport de boite libre on choisit le courant min
    % On met a inf les points impossible (point mort en traction, vitesse engagé à l'arrêt)
    ibat_min_hs(ind_wacm_max_hs)=Inf;
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        ibat_min_hs(cprim_bv==Inf)=Inf;
        ibat_min_hs=min(ibat_min_hs);
    end

end

% en hybride // en recup, on suppose que cacm1 marcherai au max en gene puis si ca suffit pas cacm2
if param.bv_acm1==1 % cas acm1 en amont de la BV
    cacm1_hp=cprim_bv+VD.ACM1.J_mg*dwacm1+VD.ACM2.J_mg*dwacm2_hp+VD.MOTH.J_mt*dwmt_hp-cmt_max_hp;
elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
    cprim_bv_max=cmt_max_hp-VD.MOTH.J_mt*dwmt_hp-VD.ACM2.J_mg*dwacm2_hp;
    [ERR,csec_bv_max]=calc_bv_fw([],VD,cprim_bv_max,wprim_bv,dwprim_bv,0);
    cacm1_hp=repmat(cprim_red+VD.ACM1.J_mg*dwacm1,[nrap 1])-csec_bv_max; % NB en rapp vit impose nrap vaut 1
end

if VD.CYCL.ntypcin==1 & param.bv_acm1==2 % cas rapport de boite libre et acm1 directement connectee au red final
    cacm1_max_2D=repmat(cacm1_max,[nrap 1]);
    cond=(cacm1_hp>cacm1_max_2D);
    cacm1_hp(cond)=cacm1_max_2D(cond);
    [ERR,qacm1_hp]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hp,repmat(wacm1,[nrap 1]),0);
    pelecacm1_hp=cacm1_hp.*repmat(wacm1,[nrap 1])+qacm1_hp;

else
    cond=(cacm1_hp>cacm1_max);
    cacm1_hp(cond)=cacm1_max(cond);
    [ERR,qacm1_hp]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hp,wacm1,0);
    pelecacm1_hp=cacm1_hp.*wacm1+qacm1_hp;

end

cacm2_hp=zeros(size(wprim_bv));


if param.bv_acm1==1 % cas acm1 en amont de la BV
    cacm2_hp(cond)=cprim_bv(cond)+VD.ACM1.J_mg*dwacm1(cond)+VD.ACM2.J_mg*dwacm2_hp(cond)-cmt_max_hp(cond)-cacm1_hp(cond)+VD.MOTH.J_mt*dwmt_hp(cond);
elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
    if VD.CYCL.ntypcin==3 % rapport de boite imposee
        csec_bv_max=cprim_red+VD.ACM1.J_mg*dwacm1-cacm1_hp;
        [ERR,cprim_bv_max]=calc_bv(VD,csec_bv_max,wsec_bv,dwsec_bv,0);
        cacm2_hp(cond)=cprim_bv_max(cond)+VD.ACM2.J_mg*dwacm2_hp(cond)-cmt_max_hp(cond)+VD.MOTH.J_mt*dwmt_hp(cond);
    elseif VD.CYCL.ntypcin==1 % rapport de boite libre
        csec_bv_max=repmat(cprim_red+VD.ACM1.J_mg*dwacm1,[nrap 1])-cacm1_hp;
        [ERR,cprim_bv_max]=calc_bv(VD,csec_bv_max,repmat(wsec_bv,[nrap 1]),repmat(dwsec_bv,[nrap 1]),0,4);
        cacm2_hp(cond)=cprim_bv_max(cond)+VD.ACM2.J_mg*dwacm2_hp(cond)-cmt_max_hp(cond)+VD.MOTH.J_mt*dwmt_hp(cond);
    end
end

cond2=(cacm2_hp>cacm2_max)&~isinf(cacm2_hp);
cacm2_hp(cond2)=cacm2_max(cond2);

[ERR,qacm2_hp]=calc_pertes_acm(ERR,VD.ACM2,cacm2_hp,wacm2_hp,0);
pelecacm2_hp=cacm2_hp.*wacm2_hp+qacm2_hp;

% calcul de ibatmin
pres_hp=pelecacm1_hp+pelecacm2_hp+pacc(1); % Normalement il y as un survolteur entre pbat et pres !!
pbat_hp=pres_hp;
[ERR,ibat_min_hp,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_hp,100-VD.INIT.Dod0,0,-1,0,param); 

% calcul de la pbat_min
% non pas si on eclate les limites en tension
% 
% if VD.CYCL.ntypcin==1
%     % on fait le choix du rapport de boite 
%     % qui maximise le courant batterie
%     ind_co=find(sum(csec_emb1)~=0);
%     ibat_min(1,ind_co)=Inf;
%     
%     ind_co=find(sum(csec_emb1)==0);
%     ibat_min(2:end,ind_co)=Inf;
%     [ibat_min,indice_rap_opt_hyb]=min(ibat_min);
%     RdF=RdF(sub2ind(size(RdF),indice_rap_opt_hyb,1:length(indice_rap_opt_hyb)));
%     ibat_min(isinf(ibat_min))=NaN;
% end


ibat_min_hp(isnan(ibat_min_hp))=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0);

if VD.CYCL.ntypcin==1 % on doit choisir le courant min
    % On met a inf les points impossibles (point mort en traction, vitesse engage à l'arret)
    ibat_min_hp(ind_wacm_max)=Inf;
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        ibat_min_hp(cprim_bv==Inf)=Inf;
    end
    ibat_min_hp=min(ibat_min_hp);
end

ibat_min=min(ibat_min_hp,ibat_min_hs);

% Si on arrive a ibat_min>ibat_max (peut arriver en recup max si les
% limites sont atteintes) on impose ibat_min au min <=ibat_max
ibat_min(ibat_min>ibat_max)=ibat_max(ibat_min>ibat_max);

soc_max(1)=100-VD.INIT.Dod0;
Dsoc_max(1)=0;

for ii=2:length(VD.CYCL.temps)
    soc_max(ii)=soc_max(ii-1)-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_max(ii)=-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

if sum(ibat_max<ibat_min)>0 ||  sum(soc_max<soc_min)>0 || sum(Dsoc_max<Dsoc_min)>0
    chaine='error in calc_limites_HPDP_EPI_SURV' ;
    ERR=MException('BackwardModel:calc_HPDP_EPI_SURV',chaine);
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    %[ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=backward_erreur(length(VD.CYCL.temps),0,0);
    return

end

if param.verbose>=2
   
    figure(3)
    plot(VD.CYCL.temps,ibat_min,'g')
    legend('ibat max','vitesse','ibat min')
    title('courant min max fin calc limite')
    
    figure(4)
    plot(VD.CYCL.temps,soc_max,'k',VD.CYCL.temps,Dsoc_max+60,'r')
    legend('soc min','Dsoc min','soc max','Dsoc max')
    title('soc min max fin calc limite')

end

