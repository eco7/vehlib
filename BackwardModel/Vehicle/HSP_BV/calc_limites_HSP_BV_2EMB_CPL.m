% function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,r_elec]=calc_limites_HSP_BV_2EMB(ERR,param,VD,cprim_bv,wprim_bv,dwprim_bv);
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul des limites du graphes.
% On ne calcule ici que les soc min et max sans se preocuper
% de la remonte du graphe ou de soc <0 ou >100%.
% La limite min doit etre calcule avec precision (mode tout elec)
% la limite max peut etre calcule "grossierement" d'eventuels arcs impossibles
% dans le graphe seront elimine par la suite
%
% interface entree :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discreetisation etc)
% VD :          structure de description vehicule et composants
% cprim_bv  :   Couple sur le primaire de la BV (vecteur sur le cycle)
% wprim_bv  :   vitesse sur le primaire de la BV(vecteur sur le cycle)
% dwprim_bv :   derivee de la vitesse sur le primaire de la BV (vecteur sur le cycle)
% 
% interface de sortie
% ibat_max :    courant max mode tout elec (en recup c'est le courant de recup en tout elec)
% ibat_min :    courant min recharge max ( en recup courant recup plus recharge par moth)
% elec_pos :    vecteur indiquant si mode elec possible , ou zero si impossible
% soc_min :     soc correspondant a ibat_max
% soc_max :     soc correspondant a ibat_min
% Dsoc_min :    variation soc entre deux instant correspondant a ibat_max
%               (atention a integre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% Dsoc_max :    variation soc entre deux instant correspondant a ibat_min
%               (atention a integre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% r_elec    :   utilisee ensuite en recalcul pour les mode tout elec (evite de refaire les optimisation en recalcul
%
% 06-06-16: Creation 

function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,r_elec]=calc_limites_HSP_BV_2EMB_CPL(ERR,param,VD,cprim_red,wprim_red,dwprim_red,croue,wroue,dwroue);

r_var_elec={'cacm1_elec','wacm1_elec','dwacm1_elec','qacm1_elec','cacm2_elec','wacm2_elec','dwacm2_elec',...
    'qacm2_elec','cprim_bv_elec','wprim_bv_elec','dwprim_bv_elec',...
    'cfrott_acm1','indice_rap_opt_elec','ind_acm1_na'};

ERR=[];
ii_max=size(VD.CYCL.temps);
soc_min=zeros(ii_max);
soc_max=zeros(ii_max);
Dsoc_min=zeros(ii_max);
Dsoc_max=zeros(ii_max);


indice_rap_opt_elec=zeros(ii_max);

% Connexion
wsec_bv=wprim_red;
dwsec_bv=dwprim_red;

% Calcul des conditions en amont de la boite de vitesse
% NB si on est en rapport libre ou optimaux on as des matrices 
% avec en nb de ligne le nb de rapport de boite plus pm
if param.bv_acm1==1 % cas acm1 en amont de la BV
    csec_bv=cprim_red;
    [ERR,cprim_bv,wprim_bv,dwprim_bv]=calc_bv(VD,csec_bv,wsec_bv,dwsec_bv,0);
elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
    [ERR,~,wprim_bv,dwprim_bv]=calc_bv(VD,zeros(size(wsec_bv)),wsec_bv,dwsec_bv,0);
end

if ~isempty(ERR)
    r_elec=[]; 
    return;
end

[nrap,sc]=size(wprim_bv);

%% recherche du mode tout electrique a chaque pas de temps
  
% Calcul de la vitesse min possible du moteur thermique pour verifier que l emode tout elec est possible
wmt_min=0*ones(1,size(wprim_bv,2)); % limitation sur vit max du moth et vit max de la generatrice
if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
        wmt_min(VD.CYCL.vitesse>VD.ECU.vmaxele)=VD.MOTH.ral;
end

% calcul de la vitesse des machines en tout elec 
[ERR,~,wacm2_elec_nrap,dwacm2_elec_nrap]=calc_red([],VD.ADCPL2,zeros(size(wprim_bv)),wprim_bv,dwprim_bv,0);

if param.bv_acm1==1 % cas acm1 en amont de la BV
    [ERR,~,wacm1_elec_nrap,dwacm1_elec_nrap]=calc_red([],VD.ADCPL1,zeros(size(wprim_bv)),wprim_bv,dwprim_bv,0);
elseif param.bv_acm1==2 % cas acm1 en aval de la BV
    [ERR,~,wacm1_elec_nrap,dwacm1_elec_nrap]=calc_red([],VD.ADCPL1,zeros(size(wprim_red)),wprim_red,dwprim_red,0);
end

% calcul des pertes par frottement des machines 
cfrott_acm2=interp1(VD.ACM2.Regmot_pert,VD.ACM2.Cpl_frot,abs(wacm2_elec_nrap));
cfrott_acm1_elec=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,abs(wacm1_elec_nrap));
cfrott_acm1=zeros(1,size(wprim_bv,2));
% Recherche du mode de fonctionnement qui minimise les pertes
% On discretise cacm1 entre cacm1_min et cacm1_max

% On doit venir verifier que wacm1 n'est pas superieure a la limite autorisee
%I_wacm1_max=find(VD.ACM1.Cmax_mot(:,1)==0,1,'first');
%wacm1_max=min( (VD.ACM1.Regmot_cmax(I_wacm1_max)), max(VD.ACM1.Regmot_pert) );
%ind_wacm_max=wacm1_elec_nrap>wacm1_max;
%ind_wacm_max=wacm1_elec_nrap>max(VD.ACM1.Regmot_cmax);

% On doit venir verifier que wacm1 et wacm2 ne sont pas superieure a la limite autorisee
%BK problem speed
I_wacm1_max=find(VD.ACM1.Cmax_mot(:,1)==0,1,'first');
wacm1_max=min( (VD.ACM1.Regmot_cmax(I_wacm1_max)), max(VD.ACM1.Regmot_pert) );
%wacm1_max= max(VD.ACM1.Regmot_cmax);
I_wacm2_max=find(VD.ACM2.Cmax_mot(:,1)==0,1,'first');
wacm2_max=min( VD.ACM2.Regmot_cmax(I_wacm2_max), max(VD.ACM2.Regmot_pert) );
%wacm2_max= max(VD.ACM2.Regmot_cmax);
ind_wacm1_max= wacm1_elec_nrap>wacm1_max ;
ind_wacm_max= wacm1_elec_nrap>wacm1_max | wacm2_elec_nrap>wacm2_max;


if (VD.CYCL.ntypcin==1 & param.bv_acm1==1 &  sum(sum(ind_wacm_max(2:nrap,:),1) == (nrap-1)) >=1) |...% rapport de boite libre
        sum(ind_wacm_max)>=1 % cas rapport de boite imposee
    
    chaine='error in calc_limites_HP_BV_2EMB : motor speed upper than maximum speed allow' ;
    ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine);
    if param.verbose>=1
        warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine);
        Indices = find(sum(wacm1_elec_nrap(2:nrap,:)>max(VD.ACM1.Regmot_cmax)) == nrap-1);
    end
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=backward_erreur(length(VD.CYCL.temps),0,0);
    r_elec=[];
    return
end

cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec_nrap);
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec_nrap);

cacm2_min=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2_elec_nrap);
cacm2_max=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2_elec_nrap);

if param.bv_acm1==1 % cas acm1 en amont de la BV
    [ERR,cprim2_adcpl1_acm2_na]=calc_red([],VD.ADCPL1,cprim_bv,wprim_bv,dwprim_bv,0);
elseif param.bv_acm1==2 % cas acm1 en aval de la BV connecte au red final
    [ERR,cprim2_adcpl1_acm2_na]=calc_red([],VD.ADCPL1,cprim_red,wprim_red,dwprim_red,0);
end
    
if VD.CYCL.ntypcin==3 % cas rapport de boite imposee
    
    if param.emb2==1
        cacm1_mat=Inf*ones(floor(max((cacm1_max-cacm1_min)/param.pas_cacm1))+2,sc);
        for ii=1:sc
            cacm1_vec=[cacm1_min(ii) fliplr(-param.pas_cacm1:-param.pas_cacm1:cacm1_min(ii))  0:param.pas_cacm1:cacm1_max(ii) cacm1_max(ii)]';
            cacm1_mat(1:length(cacm1_vec),ii)=cacm1_vec;
        end
        
        % On rajoute en derniere ligne les couples de frottement cas EM1 non
        % alimente et une ligne avec cprim_bv cas EM2 non alimente (donc emb1 ouvert)
        cacm1_mat=[cacm1_mat;  cfrott_acm1_elec;  cprim2_adcpl1_acm2_na+VD.ACM1.J_mg*dwacm1_elec_nrap];
        
    else
        cacm1_mat=cprim2_adcpl1_acm2_na+VD.ACM1.J_mg*dwacm1_elec_nrap;      
    end
    
    
elseif VD.CYCL.ntypcin==1 % cas rapport de boite libre
    % cas du point mort
    % NB la premiere ligne de cprim_bv a deja ete mis a Inf si couple secondaire >0 par calc_bv
    % On mettra a Inf les cas arret vehicule autre que point mort
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        
        ind_vit0=find(VD.CYCL.vitesse==0);
        cprim_bv(2:end,ind_vit0)=Inf;
        
        if param.emb2==1
            cacm1_3D=Inf*ones(floor(max(max((cacm1_max-cacm1_min)/param.pas_cacm1)))+3,sc,nrap);
            for jj=1:nrap
                for ii=1:sc
                    cacm1_vec=[cacm1_min(jj,ii) fliplr(-param.pas_cacm1:-param.pas_cacm1:cacm1_min(jj,ii))  0:param.pas_cacm1:cacm1_max(jj,ii) cacm1_max(jj,ii)]';
                    cacm1_3D(1:length(cacm1_vec),ii,jj)=cacm1_vec;
                end
            end
            % On rajoute en derniere ligne les couples de frottement cas EM1 non
            % alimente et une ligne avec cprim_bv cas EM2 non alimente (donc emb1 ouvert)
            cacm1_acm2_na=min(cprim2_adcpl1_acm2_na+VD.ACM1.J_mg*dwacm1_elec_nrap,cacm1_max);
            cacm1_acm2_na=max( cacm1_acm2_na,cacm1_min);
            cacm1_3D=[cacm1_3D;  permute(cfrott_acm1_elec,[3 2 1]);  permute((cacm1_acm2_na),[3 2 1])];
        else
            cacm1_3D= permute((cprim2_adcpl1_acm2_na+VD.ACM1.J_mg*dwacm1_elec_nrap),[3 2 1]);
        end
    elseif param.bv_acm1==2 % cas acm1 en aval de la BV connecte au red final
        if param.emb2==1
            cacm1_mat=Inf*ones(floor(max((cacm1_max-cacm1_min)/param.pas_cacm1))+2,sc);
            for ii=1:sc
                cacm1_vec=[cacm1_min(ii) fliplr(-param.pas_cacm1:-param.pas_cacm1:cacm1_min(ii))  0:param.pas_cacm1:cacm1_max(ii) cacm1_max(ii)]';
                cacm1_mat(1:length(cacm1_vec),ii)=cacm1_vec;
            end
            cacm1_acm2_na=min(cprim2_adcpl1_acm2_na+VD.ACM1.J_mg*dwacm1_elec_nrap,cacm1_max);
            cacm1_acm2_na=max( cacm1_acm2_na,cacm1_min);
            cacm1_3D=repmat([cacm1_mat;  cfrott_acm1_elec;  cacm1_acm2_na],[1 1 nrap]);
        else
            cacm1_3D=repmat(cprim2_adcpl1_acm2_na+VD.ACM1.J_mg*dwacm1_elec_nrap,[1 1 nrap]);
        end
    end
end


% Calcul des pertes dans les machines
if VD.CYCL.ntypcin==3 % cas rapport de boite imposee
    [sx,~]=size(cacm1_mat);
    wacm1_mat=repmat(wacm1_elec_nrap,sx,1);
    wacm2_mat=repmat(wacm2_elec_nrap,sx,1);
    dwacm2_mat=repmat(dwacm2_elec_nrap,sx,1);
    
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        
        cprim2_adcpl1_mat=cacm1_mat-repmat(VD.ACM2.J_mg*dwacm1_elec_nrap,[sx 1]);
        [ERR,cprim1_adcpl1_mat]=calc_adcpl(ERR,VD.ADCPL1,repmat(cprim_bv,[sx 1]),repmat(wprim_bv,[sx 1]),repmat(dwprim_bv,[sx 1]),cprim2_adcpl1_mat,0,1);
        
        csec_adcpl2_mat=cprim1_adcpl1_mat;
        [ERR,cprim1_adcpl2_mat]=calc_red([],VD.ADCPL2,csec_adcpl2_mat,repmat(wprim_bv,[sx 1]),repmat(dwprim_bv,[sx 1]),0);
        cacm2_mat=cprim1_adcpl2_mat+VD.ACM2.J_mg*dwacm2_mat;
        
    elseif param.bv_acm1==2 % cas acm1 en amont de la BV
        
        cprim2_adcpl1_mat=cacm1_mat-repmat(VD.ACM2.J_mg*dwacm1_elec_nrap,[sx 1]);
        [ERR,cprim1_adcpl1_mat]=calc_adcpl(ERR,VD.ADCPL1,repmat(cprim_red,[sx 1]),repmat(wprim_red,[sx 1]),repmat(dwprim_red,[sx 1]),cprim2_adcpl1_mat,0,1);
        csec_bv_mat=cprim1_adcpl1_mat;
        [ERR,cprim_bv_mat]=calc_bv(VD,csec_bv_mat,repmat(wsec_bv,[sx 1]),repmat(dwsec_bv,[sx 1]),0);
        
        csec_adcpl2_mat=cprim_bv_mat;
        [ERR,cprim1_adcpl2_mat]=calc_red([],VD.ADCPL2,csec_adcpl2_mat,repmat(wprim_bv,[sx 1]),repmat(dwprim_bv,[sx 1]),0);
        cacm2_mat=cprim1_adcpl2_mat+VD.ACM2.J_mg*dwacm2_mat;
        
        % indice des points du cycle ou cprim_red<0 et PM
        % seule le cas acm2 non alimente est possible (derniere ligne)
        I_pm=find(cprim_red<0 & VD.CYCL.rappvit==0);
        cacm2_mat(1:end-1,I_pm)=Inf;
        cacm1_mat(1:end-1,I_pm)=Inf;
        
    end
    
    % Cas EM2 non alimente emb1 ouvert
    cacm2_mat(sx,:)=0;
    wacm2_mat(sx,:)=0;
    dwacm2_mat(sx,:)=0;
    
    % calcul des pertes dans les machines
    [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1_mat,wacm1_mat,0);
    [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2_mat,wacm2_mat,0);
    
    % Derniere ligne cas frottement acm1 ou acm2 non alimente
    qacm2(sx,:)=0;
    if param.emb2==1
        qacm1(sx-1,:)=0;
    end
     % Calcul des auxiliaires electriques
    [ERR,pacc]=calc_acc(VD);
    
    % calcul puissance motelec
    pacm1_elec=VD.VEHI.nbacm1*(cacm1_mat.*repmat(wacm1_elec_nrap,sx,1)+qacm1);
    if param.emb2==1
        pacm1_elec(sx-1,:)=0;
        pacm2_elec=(cacm2_mat.*repmat(wacm2_elec_nrap,sx,1)+qacm2);
    end
    pacm2_elec(sx,:)=0;
    %
    pres=pacm1_elec+pacm2_elec+pacc(1);
    %
    [ERR,ibat_max,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,100-VD.INIT.Dod0,0,-1,0,param);
    
    % minimisation du courant batterie
    % NB attention la minimisation des pertes conduit a une erreur dans le
    % cas ou l'on considère acm1 na et couple de frottement (recip). On
    % diminue alors le couple de recup donc les pertes mais on augmente ibat !!!
    if param.emb2==1
        [~,ind_min]=min(ibat_max);
        %[cacm1_elec,cacm2_elec,wacm2_elec_nrap,dwacm2_elec_nrap,qacm1_elec,qacm2_elec]=mat_ind(ind_min,2,cacm1_mat,cacm2_mat,wacm2_mat,dwacm2_mat,qacm1,qacm2);
        [cacm1_elec,cacm2_elec,wacm2_elec,dwacm2_elec,qacm1_elec,qacm2_elec]=mat_ind(ind_min,2,cacm1_mat,cacm2_mat,wacm2_mat,dwacm2_mat,qacm1,qacm2);
         wacm1_elec=wacm1_elec_nrap;
         dwacm1_elec=dwacm1_elec_nrap;
        ind_acm1_na=find(ind_min==sx-1); %acm1 non alimente
        if param.bv_acm1==2 % cas acm1 en amont de la BV
            [cprim_bv]=mat_ind(ind_min,2,cprim_bv_mat);
        end
    else
        cacm1_elec=cacm1_mat; cacm2_elec=cacm2_mat; wacm2_elec_nrap=wacm2_mat; dwacm2_elec_nrap=dwacm2_mat;
        qacm1_elec=qacm1; qacm2_elec=qacm2;
        if param.bv_acm1==2 % cas acm1 en amont de la BV
            cprim_bv=cprim_bv_mat;
        end
        
    end

    
    % Si on ne peut pas faire de mode tout elec cprim>cacm1_max+cacm2_max
    % On prend les couples max
    if param.emb2==1
        if param.bv_acm1==1 % cas acm1 en amont de la BV
            
            cprim2_adcpl2_max=cacm2_max-VD.ACM2.J_mg*dwacm2_elec;
            [ERR,csec_adcpl2_max]=calc_red_fw([],VD.ADCPL2,cprim2_adcpl2_max,wacm2_elec,dwacm2_elec,0);
            
            cprim2_adcpl1_max=cacm1_max-VD.ACM1.J_mg*dwacm1_elec;
            
            [ERR,~,~,~,~,csec_adcpl1_max]=calc_adcpl_fw([],VD.ADCPL1,csec_adcpl2_max,0,cprim2_adcpl1_max,0,wprim_bv,dwprim_bv);
            ind_melec_np=find(csec_adcpl1_max<cprim_bv);
            
        elseif param.bv_acm1==2 % cas acm1 aval de la BV
            
            cprim2_adcpl2_max=cacm2_max-VD.ACM2.J_mg*dwacm2_elec_nrap;
            [ERR,csec_adcpl2_max]=calc_red_fw([],VD.ADCPL2,cprim2_adcpl2_max,wacm2_elec_nrap,dwacm2_elec_nrap,0);
            cprim_bv_max=csec_adcpl2_max;
            
            [ERR,csec_bv_max]=calc_bv_fw([],VD,cprim_bv_max,wprim_bv,dwprim_bv,0);
            cprim2_adcpl1_max=cacm1_max-VD.ACM1.J_mg*dwacm1_elec_nrap;
            
            cprim1_adcpl1_max=csec_bv_max;
            
            [ERR,~,~,~,~,csec_adcpl1_max]=calc_adcpl_fw([],VD.ADCPL1,cprim1_adcpl1_max,0,cprim2_adcpl1_max,0,wprim_bv,dwprim_bv);
            ind_melec_np=find(csec_adcpl1_max<cprim_red);
            
        end
    else
        ind_melec_np=find(cacm1_elec>cacm1_max);   
    end
    
    cacm1_elec(ind_melec_np)=cacm1_max(ind_melec_np);
    [ERR,qacm1_elec(ind_melec_np)]=calc_pertes_acm(ERR,VD.ACM1,cacm1_max(ind_melec_np),wacm1_elec(ind_melec_np),0);
    
    if param.emb2==1
        cacm2_elec(ind_melec_np)=cacm2_max(ind_melec_np);
        [ERR,qacm2_elec(ind_melec_np)]=calc_pertes_acm(ERR,VD.ACM2,cacm2_max(ind_melec_np),wacm2_elec(ind_melec_np),0);
    end
    
    % Si recup max on met les couples a leur valeur min
    if param.emb2==1
        if param.bv_acm1==1 % cas acm1 en amont de la BV
            
            cprim2_adcpl2_min=cacm2_min-VD.ACM2.J_mg*dwacm2_elec;
            [ERR,csec_adcpl2_min]=calc_red_fw([],VD.ADCPL2,cprim2_adcpl2_min,wacm2_elec,dwacm2_elec,0);
            cprim1_adcpl1_min=csec_adcpl2_min;
            
            cprim2_adcpl1_min=cacm1_min-VD.ACM1.J_mg*dwacm1_elec;
            [ERR,~,~,~,~,csec_adcpl1_min]=calc_adcpl_fw([],VD.ADCPL1,cprim1_adcpl1_min,0,cprim2_adcpl1_min,0,wprim_bv,dwprim_bv);
            ind_min=find(csec_adcpl1_min>cprim_bv);
            
        elseif param.bv_acm1==2 % cas acm1 en aval de la BV
            
            cprim2_adcpl2_min=cacm2_min-VD.ACM2.J_mg*dwacm2_elec_nrap;
            [ERR,csec_adcpl2_min]=calc_red_fw([],VD.ADCPL2,cprim2_adcpl2_min,wacm2_elec_nrap,dwacm2_elec_nrap,0);
            cprim_bv_min=csec_adcpl2_min;
            
            [ERR,csec_bv_min]=calc_bv_fw([],VD,cprim_bv_min,wprim_bv,dwprim_bv,0);
            cprim2_adcpl1_min=cacm1_min-VD.ACM1.J_mg*dwacm1_elec_nrap;
            cprim1_adcpl1_min=csec_bv_min;
            [ERR,~,~,~,~,csec_adcpl1_min]=calc_adcpl_fw([],VD.ADCPL1,cprim1_adcpl1_min,0,cprim2_adcpl1_min,0,wprim_bv,dwprim_bv);
            ind_min=find(csec_adcpl1_min>cprim_red);
            
        end
    else
        ind_min=find(cacm1_elec<cacm1_min);    
    end
    
    cacm1_elec(ind_min)=cacm1_min(ind_min);
    [ERR,qacm1_elec(ind_min)]=calc_pertes_acm(ERR,VD.ACM1,cacm1_min(ind_min),wacm1_elec(ind_min),0);
    
    if param.emb2==1
        cacm2_elec(ind_min)=cacm2_min(ind_min);
        [ERR,qacm2_elec(ind_min)]=calc_pertes_acm(ERR,VD.ACM2,cacm2_min(ind_min),wacm2_elec(ind_min),0);
    end
    % Pour etre coherent avec le cas rapport de boite libre on ressort les
    % grandeurs suivantes.
    cprim_bv_elec=cprim_bv;
    wprim_bv_elec=wprim_bv;
    dwprim_bv_elec=dwprim_bv;
    
    cacm1_max_elec=cacm1_max;
    cacm2_max_elec=cacm2_max;
    
    % On recalul ibat_max une fois les limites prise en compte
    % calcul puissance motelec
    pacm1_elec=VD.VEHI.nbacm1*(cacm1_elec.*wacm1_elec+qacm1_elec);
    pacm2_elec=(cacm2_elec.*wacm2_elec+qacm2_elec);
            
    pres=pacm1_elec+pacm2_elec+pacc;
    
    [ERR,ibat_max,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,100-VD.INIT.Dod0,0,-1,0,param);
    
    % Cas ou l'on a explose les limites en recup
    % On doit imposer que Ibat_max = VD.BATT.Ibat_min
    ind_rec_max_bat=find(isnan(ibat_max) & cprim_red<0 & pres<0); % a verifier le pres<0 EV 15-05-2019
    ibat_max(ind_rec_max_bat)=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension
    
    elec_pos=ones(size(cprim_bv_elec));
    elec_pos(wmt_min~=0)=0.5; % moth au ralenti mais cmt =0, la batterie fournie toute la puissance
    elec_pos(ind_melec_np)=0; % les deux moteurs ne suffisent pas
    elec_pos(isnan(ibat_max))=0; % la batterie ne peut fournir toute la puissance mode elec impossible
    elec_pos_elec=elec_pos;

elseif VD.CYCL.ntypcin==1 % cas rapport de boite libre
    [sx,~]=size(cacm1_3D );
  
    wacm2_3D=repmat(permute(wacm2_elec_nrap,[3 2 1]),[sx 1 1]);
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        
        wacm1_3D=repmat(permute(wacm1_elec_nrap,[3 2 1]),[sx 1 1]);
        dwacm1_elec_3D=repmat(permute(dwacm1_elec_nrap,[3 2 1]),[sx 1 1]);
        dwacm2_elec_3D=repmat(permute(dwacm2_elec_nrap,[3 2 1]),[sx 1 1]);
        cprim_bv_3D=repmat(permute(cprim_bv,[3 2 1]),[sx 1 1]);
        wprim_bv_3D=repmat(permute(wprim_bv,[3 2 1]),[sx 1 1]);
        dwprim_bv_3D=repmat(permute(dwprim_bv,[3 2 1]),[sx 1 1]);
        
        cprim2_adcpl1_3D=cacm1_3D-VD.ACM1.J_mg*dwacm1_elec_3D;
        [ERR,cprim1_adcpl1_3D]=calc_adcpl(ERR,VD.ADCPL1,cprim_bv_3D,wprim_bv_3D,dwprim_bv_3D,cprim2_adcpl1_3D,0,1);
        
        csec_adcpl2_3D=cprim1_adcpl1_3D;
        [ERR,cprim1_adcpl2_3D]=calc_red([],VD.ADCPL2,csec_adcpl2_3D,wprim_bv_3D,wprim_bv_3D,0);
        cacm2_3D=cprim1_adcpl2_3D+VD.ACM2.J_mg*dwacm2_elec_3D;
        
        
        %cacm2_3D1=repmat(permute((cprim_bv+VD.ACM1.J_mg*dwacm1_elec_nrap+VD.ACM2.J_mg*dwacm2_elec_nrap),[3 2 1]),[sx 1 1])-cacm1_3D;
    elseif param.bv_acm1==2 % cas acm1 en amont de la BV
        
        wacm1_3D=repmat(permute(wacm1_elec_nrap,[3 2 1]),[sx 1 nrap]);
        dwacm2_elec_3D=repmat(permute(dwacm2_elec_nrap,[3 2 1]),[sx 1 1]);
       
        cprim2_adcpl1_2D=cacm1_3D(:,:,1)-repmat(VD.ACM1.J_mg*dwacm1_elec_nrap,[sx 1]);
        [ERR,cprim1_adcpl1_2D]=calc_adcpl(ERR,VD.ADCPL1,repmat(cprim_red,[sx 1]),repmat(wprim_red,[sx 1]),repmat(dwprim_red,[sx 1]),cprim2_adcpl1_2D,0,1);
        csec_bv_2D=cprim1_adcpl1_2D;
        [ERR,cprim_bv_3D]=calc_bv(VD,csec_bv_2D,repmat(wsec_bv,[sx 1]),repmat(dwsec_bv,[sx 1]),0,3);
        
        csec_adcpl2_3D=cprim_bv_3D;
        [ERR,cprim1_adcpl2_3D]=calc_red([],VD.ADCPL2,csec_adcpl2_3D,repmat(permute(wprim_bv,[3 2 1]),[sx 1 1]),repmat(permute(dwprim_bv,[3 2 1]),[sx 1 1]),0);
        cacm2_3D=cprim1_adcpl2_3D+VD.ACM2.J_mg*dwacm2_elec_3D;
        
    end
  
    
    % cas ou acm2 non connecte emb1 ouvert
    cacm2_3D(sx,:,:)=0;
    wacm2_3D(sx,:,:)=0;
    dwacm2_elec_3D(sx,:,:)=0;
    
    % Si on atteind les limites en recup sur acm2
    % On doit limiter acm2 � cacm2_min   
    cacm2_min_3D=repmat(permute(cacm2_min,[3 2 1]),[sx 1 1]);
    I_lim_acm2_n=find(cacm2_3D<cacm2_min_3D);
    cacm2_3D(I_lim_acm2_n)=cacm2_min_3D(I_lim_acm2_n);
        
     % calcul des pertes dans les machines
    [ERR,qacm1_3D]=calc_pertes_acm(ERR,VD.ACM1,cacm1_3D,wacm1_3D,0);
    [ERR,qacm2_3D]=calc_pertes_acm(ERR,VD.ACM2,cacm2_3D,wacm2_3D,0);
    
    % Derniere ligne cas frottement acm1 ou acm2 non alimente
    qacm2_3D(sx,:,:)=0;
    
    % On met � inf les pertes si le limites de cacm2 sont depasse
      
    % Cas ou l'on atteind les limites sur acm2
    % si cacm2>0 traction donc mode elec impossible
    % si cacm2<0 recup donc il faut limiter cacm2 � cacm2_min cd ci dessus
    
   % cacm2_min_3D=repmat(permute(cacm2_min,[3 2 1]),[sx 1 1]);
    cacm2_max_3D=repmat(permute(cacm2_max,[3 2 1]),[sx 1 1]);
   
    %I_lim_acm2=find(cacm2_3D<cacm2_min_3D | cacm2_3D>cacm2_max_3D);
    I_lim_acm2_p=find(cacm2_3D>cacm2_max_3D);
    qacm1_3D(I_lim_acm2_p)=Inf;
    qacm2_3D(I_lim_acm2_p)=Inf;
   
 
    
    if param.emb2==1
        qacm1_3D(sx-1,:,:)=0;
    end
  
    % Cas ou l'on ne peut pas faire de mode tout elec cprim>cacm1_max+cacm2_max
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        
        cprim2_adcpl2_max=cacm2_max-VD.ACM2.J_mg*dwacm2_elec_nrap;
        [ERR,csec_adcpl2_max]=calc_red_fw([],VD.ADCPL2,cprim2_adcpl2_max,wacm2_elec_nrap,dwacm2_elec_nrap,0);
        
        cprim2_adcpl1_max=cacm1_max-VD.ACM1.J_mg*dwacm1_elec_nrap;
        
        [ERR,~,~,~,~,csec_adcpl1_max]=calc_adcpl_fw([],VD.ADCPL1,csec_adcpl2_max,0,cprim2_adcpl1_max,0,wprim_bv,dwprim_bv);
        ind_max_acm= csec_adcpl1_max<cprim_bv | ind_wacm_max; % indices des points ou mode elec impossible;
        
        [ERR,~,~,~,~,csec_adcpl1_max_acm2_na]=calc_adcpl_fw([],VD.ADCPL1,0,0,cprim2_adcpl1_max,0,wprim_bv,dwprim_bv);
        ind_max_acm1_acm2_na=csec_adcpl1_max_acm2_na<cprim_bv | ind_wacm1_max ; % mode elec impossible avec seulement acm1
        
        cprim2_adcpl1_max_acm1_na=-VD.ACM1.J_mg*dwacm1_elec_nrap;
        
        [ERR,~,~,~,~,csec_adcpl1_max_acm1_na]=calc_adcpl_fw([],VD.ADCPL1,csec_adcpl2_max,0,cprim2_adcpl1_max_acm1_na,0,wprim_bv,dwprim_bv);
        ind_max_acm2_acm1_na= csec_adcpl1_max_acm1_na<cprim_bv | ind_wacm_max; % indices des points ou mode elec impossible avec seulement acm2;
       
    elseif param.bv_acm1==2 % cas acm1 en amont de la BV
        
        cprim2_adcpl2_max=cacm2_max-VD.ACM2.J_mg*dwacm2_elec_nrap;
        [ERR,csec_adcpl2_max]=calc_red_fw([],VD.ADCPL2,cprim2_adcpl2_max,wacm2_elec_nrap,dwacm2_elec_nrap,0);
        cprim_bv_max=csec_adcpl2_max;
        
        [ERR,csec_bv_max]=calc_bv_fw([],VD,cprim_bv_max,wprim_bv,dwprim_bv,0);
        
        cprim2_adcpl1_max=repmat(cacm1_max,[nrap 1])-VD.ACM1.J_mg*repmat(dwacm1_elec_nrap,[nrap 1]);
        cprim1_adcpl1_max=csec_bv_max;   
        [ERR,~,~,~,~,csec_adcpl1_max]=calc_adcpl_fw([],VD.ADCPL1,cprim1_adcpl1_max,0,cprim2_adcpl1_max,0,wprim_bv,dwprim_bv);
        
        %Bk removed repmat for ind_wacm_max because it is alreay nrap*1081
        ind_max_acm=csec_adcpl1_max<repmat(cprim_red,[nrap 1]) | ind_wacm_max;
        
        
        [ERR,~,~,~,~,csec_adcpl1_max_acm2_na]=calc_adcpl_fw([],VD.ADCPL1,0,0,cprim2_adcpl1_max,0,wprim_bv,dwprim_bv);
        ind_max_acm1_acm2_na=csec_adcpl1_max_acm2_na<repmat(cprim_red,[nrap 1]) | ind_wacm_max; % mode elec impossible avec seulement acm1
        
        cprim2_adcpl1_max_acm1_na=repmat(-VD.ACM1.J_mg*dwacm1_elec_nrap,[nrap 1]);
        
        [ERR,~,~,~,~,csec_adcpl1_max_acm1_na]=calc_adcpl_fw([],VD.ADCPL1,csec_bv_max,0,cprim2_adcpl1_max_acm1_na,0,wprim_bv,dwprim_bv);
        ind_max_acm2_acm1_na=csec_adcpl1_max_acm1_na<repmat(cprim_red,[nrap 1]) | ind_wacm_max;  % indices des points ou mode elec impossible avec seulement acm2;
        
        %cprim_bv_max=cacm2_max-VD.ACM2.J_mg*dwacm2_elec_nrap;
        %[ERR,csec_bv_max]=calc_bv_fw([],VD,cprim_bv_max,wprim_bv,dwprim_bv,0);
        %ind_max_acm=(repmat(cacm1_max,[nrap 1])+csec_bv_max-VD.ACM1.J_mg*repmat(dwacm1_elec_nrap,[nrap 1]))<repmat(cprim_red,[nrap 1]) | repmat(ind_wacm_max,[nrap 1]) ;
        
    end
    
   
    % On met les pertes a Inf si mode elec impossible
    qacm1_3D(repmat(permute(ind_max_acm,[3 2 1]),[sx 1 1]))=Inf;
    qacm2_3D(repmat(permute(ind_max_acm,[3 2 1]),[sx 1 1]))=Inf;
    
    % pas de temps ou le mode elec n'est jamais possible a cause des
    % deux machines
    ind_melec_np = find(sum(ind_max_acm(2:end,:),1)==nrap-1 & VD.CYCL.vitesse~=0);
    % cas ou mode elec impossible avec acm1 ou acm2
    ind_melec_np_acm1 = find(sum(ind_max_acm1_acm2_na(2:end,:),1)==nrap-1 & VD.CYCL.vitesse~=0);
    ind_melec_np_acm2 = find(sum(ind_max_acm2_acm1_na(2:end,:),1)==nrap-1 & VD.CYCL.vitesse~=0);
    % Calcul des auxiliaires electriques
    [ERR,pacc]=calc_acc(VD);
    
    % calcul puissance motelec
    pacm1_3D=VD.VEHI.nbacm1*(cacm1_3D.*wacm1_3D+qacm1_3D);
    if param.emb2==1
        pacm1_3D(sx-1,:,:)=0;
    end
    
    pacm2_3D=(cacm2_3D.*wacm2_3D+qacm2_3D);
    pacm2_3D(sx,:,:)=0;
    
    pres_3D=pacm1_3D+pacm2_3D+pacc(1);
    
    % Si on passe pas avec une machine on elimine ce cas
    pres_3D(end,ind_melec_np_acm1,2:end)=NaN; % cas acm2 na
    pres_3D(end-1,ind_melec_np_acm2,2:end)=NaN; % cas acm1 na
    %
    [ERR,ibat_max_3D,~,~,~,~,~,RdF_3D]=calc_batt(ERR,VD.BATT,param.pas_temps,pres_3D,100-VD.INIT.Dod0,0);
   
    % Cas ou aucun courant batterie n'est possible
    %ind_ibat_nan_pbatt_max=permute(sum(isnan(ibat_max_3D))==sx,[3 2 1]);
    
    % En recup max on doit imposer ibat=Ibat_min
    % calcul pbat_min
    [ERR,~,~,~,~,~,~,~,ibat_min_calc,Ubat_min_calc]=calc_batt(ERR,VD.BATT,param.pas_temps,-10,100-VD.INIT.Dod0,0,-1,0,param);
    [~,~,~,~,~,~,~,~,~,~,ibat_max_calc,Ubat_max_calc]=calc_batt(ERR,VD.BATT,param.pas_temps,10,100-VD.INIT.Dod0,0,-1,0,param);
    pbatt_min_calc=ibat_min_calc*Ubat_min_calc;
    pbatt_max_calc=ibat_max_calc*Ubat_max_calc;
    ind_rec_max_bat=(isnan(ibat_max_3D) & pres_3D<pbatt_min_calc & repmat(permute(cprim_red,[3 2 1]),[sx 1 nrap])<0);
    ibat_max_3D(ind_rec_max_bat)=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension
    
    %% Verifier l'inversion avec ligne dessus
    % Cas ou aucun courant batterie n'est possible car pres_3D trop grand
    if param.emb2==1
        %ind_ibat_nan=permute(sum(isnan(ibat_max_3D)& pres_3D>pbatt_max_calc)>=(sx-2),[3 2 1]); % on regarde pour chaque rapport
        ind_ibat_nan_pbatt_max=permute(sum(isnan(ibat_max_3D))==(sx),[3 2 1]); % on regarde pour chaque rapport
        %ind_ibat_nan_pbatt_max=sum(ind_ibat_nan(2:end,:),1)==(nrap-1); % impossible quelquesoit les rapports
    else
        ind_ibat_nan_pbatt_max=permute(isnan(ibat_max_3D),[3 2 1]);
    end
    
     % On supprime les cas "idiots" ou croue<0 et cacm1>0 avec cacm2<0 (flux
    % entre les  maachines).
    cprim_red_3D=repmat(cprim_red,[sx 1 nrap]); % EV 08-02020 remplacement croue cprim_red
   % ibat_max_3D(croue_3D<0 & cacm1_3D>0)=Inf;
    ibat_max_3D(cprim_red_3D<0 & cacm1_3D>0)=NaN;
    
    % Si cacm1<0 et croue >0 on met � inf sauf le cas cacm1=cfrott (derniere
    % ligne de la matrice)
    ibat_max_3D_sxm2=ibat_max_3D(1:end-2,:,:);  
   % ibat_max_3D_sxm2(croue_3D(1:end-2,:,:)>0 & cacm1_3D(1:end-2,:,:)<0)=Inf;
    ibat_max_3D_sxm2(cprim_red_3D(1:end-2,:,:)>0 & cacm1_3D(1:end-2,:,:)<0)=NaN;
    ibat_max_3D=[ibat_max_3D_sxm2; ibat_max_3D(end-1:end,:,:)];
    
    
    
    [ibat_max_2D,ind_min_ibat_max_3D]=min(ibat_max_3D,[],3); % min selon les rapport de boite
    [cacm1_2D,cacm2_2D,wacm1_2D,wacm2_2D,dwacm2_2D,qacm1_2D,qacm2_2D,RdF_2D]=...
        mat_ind(ind_min_ibat_max_3D,3,cacm1_3D,cacm2_3D,wacm1_3D,wacm2_3D,dwacm2_elec_3D,qacm1_3D,qacm2_3D,RdF_3D);
    
    if param.emb2==1
        [ibat_max,ind_min_ibat_max_2D]=min(ibat_max_2D); % min 
        [cacm1_elec,cacm2_elec,wacm1_elec,wacm2_elec,dwacm2_elec,qacm1_elec,qacm2_elec,RdF,indice_rap_opt_elec]=...
            mat_ind(ind_min_ibat_max_2D,2,cacm1_2D,cacm2_2D,wacm1_2D,wacm2_2D,dwacm2_2D,qacm1_2D,qacm2_2D,RdF_2D,ind_min_ibat_max_3D);
        
         % Dans les cas ou l'on a converger vers des points ou acm1 non alimente
        ind_acm1_na=find(ind_min_ibat_max_2D==sx-1);
        cfrott_acm1(ind_acm1_na)=cacm1_elec(ind_acm1_na);
    else
        ibat_max=ibat_max_2D;
        cacm1_elec=cacm1_2D; cacm2_elec=cacm2_2D; wacm1_elec=wacm1_2D; wacm2_elec=wacm2_2D; dwacm2_elec=dwacm2_2D;
        qacm1_elec=qacm1_2D; qacm2_elec=qacm2_2D; RdF=RdF_2D; indice_rap_opt_elec=ind_min_ibat_max_3D;
    end
    
    % pour les cas ou mode elec impossible quelquesoit rapvit, on prend les
    % couple max
     % Si recup max on prend les couples min
    
    cacm1_max_elec=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1_elec_nrap);    
    cacm2_max_elec=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2_elec_nrap);
    
    if ~isempty(ind_melec_np)
        % cacm1_elec(ind_melec_np)=cacm1_max(ind_melec_np);
        
        [ERR,qacm1_elec_melec_np]=calc_pertes_acm(ERR,VD.ACM1,cacm1_max_elec,wacm1_elec_nrap,0);
        pacm1_elec_melec_np=qacm1_elec_melec_np+cacm1_max_elec.*wacm1_elec_nrap;
        
        %qacm1_elec(ind_melec_np)
        %[ERR,qacm1_elec(ind_melec_np)]=calc_pertes_acm(ERR,VD.ACM1,cacm1_max_elec(ind_melec_np),wacm1_elec(ind_melec_np),0);
        
        if param.emb2==1
            %cacm2_elec(ind_melec_np)=cacm2_max(ind_melec_np);
            %[ERR,qacm2_elec(ind_melec_np)]=calc_pertes_acm(ERR,VD.ACM2,cacm2_max_elec(ind_melec_np),wacm2_elec(ind_melec_np),0);  
            [ERR,qacm2_elec_melec_np]=calc_pertes_acm(ERR,VD.ACM2,cacm2_max_elec,wacm2_elec_nrap,0);  
            pacm2_elec_melec_np=qacm2_elec_melec_np+cacm2_max_elec.*wacm2_elec_nrap;
        end
        pres_melec_np=pacm1_elec_melec_np+pacm2_elec_melec_np+pacc(1);
        [ERR,ibat_max_melec_np,~,~,~,~,~,RdF_3D_melec_np]=calc_batt(ERR,VD.BATT,param.pas_temps,pres_melec_np,100-VD.INIT.Dod0,0);
        [ibat_max_melec_np,ind_ibat_max_melec_np]=min(ibat_max_melec_np(2:end,:),[],1);
        ibat_max(ind_melec_np)=ibat_max_melec_np(ind_melec_np);
        indice_rap_opt_elec(ind_melec_np)=ind_ibat_max_melec_np(ind_melec_np);
        indice_rap_opt_elec(ind_melec_np)=ind_ibat_max_melec_np(ind_melec_np);
        [cacm1_elec(ind_melec_np),cacm2_elec(ind_melec_np),wacm1_elec(ind_melec_np),wacm2_elec(ind_melec_np),dwacm2_elec(ind_melec_np),...
               qacm1_elec(ind_melec_np),qacm2_elec(ind_melec_np),RdF(ind_melec_np)]=...
            mat_ind(ind_ibat_max_melec_np(ind_melec_np),2,cacm1_max_elec(2:end,ind_melec_np),cacm2_max_elec(2:end,ind_melec_np),wacm1_elec_nrap(2:end,ind_melec_np),wacm2_elec_nrap(2:end,ind_melec_np),dwacm2_elec_nrap(2:end,ind_melec_np),...
               qacm1_elec_melec_np(2:end,ind_melec_np),qacm2_elec_melec_np(2:end,ind_melec_np),RdF_3D_melec_np(2:end,ind_melec_np));
       
       % [cacm1_elec(ind_melec_np)]=mat_ind(ind_ibat_max_melec_np(ind_melec_np),2,cacm1_max_elec(2:end,ind_melec_np));
    end
    
    cacm1_min_elec=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1_elec);
    cacm2_min_elec=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmin_mot(1,:))),wacm2_elec);
     
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        
        [cprim_bv_elec,wprim_bv_elec,dwprim_bv_elec,dwacm1_elec,cfrott_acm1_elec]=...
            mat_ind(indice_rap_opt_elec,2,cprim_bv,wprim_bv,dwprim_bv,dwacm1_elec_nrap,cfrott_acm1_elec);
        
        cprim2_adcpl2_min=cacm2_min_elec-VD.ACM2.J_mg*dwacm2_elec;
        [ERR,csec_adcpl2_min]=calc_red_fw([],VD.ADCPL2,cprim2_adcpl2_min,wacm2_elec,dwacm2_elec,0);
        cprim1_adcpl1_min=csec_adcpl2_min;
        
        cprim2_adcpl1_min=cacm1_min_elec-VD.ACM1.J_mg*dwacm1_elec;
        [ERR,~,~,~,~,csec_adcpl1_min_elec]=calc_adcpl_fw([],VD.ADCPL1,cprim1_adcpl1_min,0,cprim2_adcpl1_min,0,wprim_bv_elec,dwprim_bv_elec);
        
        ind_min=find(csec_adcpl1_min_elec>cprim_bv_elec);
        
    elseif param.bv_acm1==2 % cas acm1 en aval de la BV
        
        [wprim_bv_elec,dwprim_bv_elec]=mat_ind(indice_rap_opt_elec,2,wprim_bv,dwprim_bv);
        cprim2_adcpl2_elec=cacm2_elec-VD.ACM2.J_mg*dwacm2_elec;
        [ERR,cprim_bv_elec]=calc_red_fw([],VD.ADCPL2,cprim2_adcpl2_elec,wacm2_elec,dwacm2_elec,0);
        
        cprim2_adcpl2_min=cacm2_min_elec-VD.ACM2.J_mg*dwacm2_elec;
        [ERR,csec_adcpl2_min]=calc_red_fw([],VD.ADCPL2,cprim2_adcpl2_min,wacm2_elec,dwacm2_elec,0);
        cprim_bv_min=csec_adcpl2_min;
        
        [ERR,csec_bv_min]=calc_bv_fw([],VD,cprim_bv_min,wprim_bv_elec,dwprim_bv_elec,0,indice_rap_opt_elec-1);
        dwacm1_elec=dwacm1_elec_nrap;
        cprim2_adcpl1_min=cacm1_min_elec-VD.ACM1.J_mg*dwacm1_elec;
        cprim1_adcpl1_min=csec_bv_min;
        [ERR,~,~,~,~,csec_adcpl1_min_elec]=calc_adcpl_fw([],VD.ADCPL1,cprim1_adcpl1_min,0,cprim2_adcpl1_min,0,wprim_bv_elec,dwprim_bv_elec);
        
        ind_min=find(csec_adcpl1_min_elec>cprim_red);
     
    end
   
    
    cacm1_elec(ind_min)=cacm1_min(ind_min);
    if ~isempty(ind_min)
       [ERR,qacm1_elec(ind_min)]=calc_pertes_acm(ERR,VD.ACM1,cacm1_min_elec(ind_min),wacm1_elec(ind_min),0);
    end
    
    if param.emb2==1
        cacm2_elec(ind_min)=cacm2_min(ind_min);
        if ~isempty(ind_min)
            [ERR,qacm2_elec(ind_min)]=calc_pertes_acm(ERR,VD.ACM2,cacm2_min_elec(ind_min),wacm2_elec(ind_min),0);
        end
    end
    % On doit recalculer ibat_max une fois les limtes prise en compte sur acm1 et acm2
    % Calcul des auxiliaires electriques
   
    % calcul puissance motelec
    pacm1_elec=VD.VEHI.nbacm1*(cacm1_elec.*wacm1_elec+qacm1_elec);
    pacm2_elec=(cacm2_elec.*wacm2_elec+qacm2_elec);
    
    pres=pacm1_elec+pacm2_elec+pacc;
    
    if~isempty(ind_melec_np)
       [ERR,ibat_max(ind_melec_np),~,~,~,~,~,RdF(ind_melec_np)]=calc_batt(ERR,VD.BATT,param.pas_temps,pres(ind_melec_np),100-VD.INIT.Dod0,0,-1,0,param);
    end
    
    elec_pos=ones(size(wprim_bv));
    elec_pos(2:end,wmt_min~=0)=0.5; % moth au ralenti mais cmt =0, la batterie fournie toute la puissance
    elec_pos(ind_max_acm)=0; % les deux moteurs ne suffisent pas
    elec_pos(ind_ibat_nan_pbatt_max)=0; % la batterie ne peut fournir toute la puissance mode elec impossible
    [elec_pos]=mat_ind(indice_rap_opt_elec,2,elec_pos);
end

%% Il faut ici avoir les couple et vitesse et derive vitesse des coupleurs en mode elec ??
%% OU alors on laisse ca pour le recalul ??


% il faut repartir en fw pour recalculer le frein meca et le fonctionnement
% des machines quand on est dans les cas de recup max Ibat==Ibat_min
%% 02-05-16 Attention en rapport de boite libre pas sur que ca marche tout ca
Ibat_min=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,100-VD.INIT.Dod0);
ind_recalcul=find(ibat_max==Ibat_min);
if ~isempty(ind_recalcul)
    [ERR,Ubat_min]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,Ibat_min,100-VD.INIT.Dod0,0);
    pbat_min=Ibat_min*Ubat_min;
    [ERR,qacm1_max]=calc_pertes_acm(ERR,VD.ACM1,cacm1_max_elec(ind_recalcul),wacm1_elec(ind_recalcul),dwacm1_elec(ind_recalcul));
    [ERR,qacm2_max]=calc_pertes_acm(ERR,VD.ACM2,cacm2_max_elec(ind_recalcul),wacm2_elec(ind_recalcul),dwacm2_elec(ind_recalcul));
    for i = 1: length(ind_recalcul)
        ii=ind_recalcul(i);
        %      Si on tape dans les limites on se fout de savoir qui fait quoi (ibat sera limite a son min) donc acm1 fera tout ce qu'elle peut puis acm2 si insuffisant
        if cacm1_max_elec(ii)*wacm1_elec(ii)+qacm1_max(i)+pacc(1)>pbat_min % la machine em1 fera tout le boulot on recalcule son coupl en fw
            pacm1_elec(ii)=pbat_min-pacc(ii);
            [ERR,qacm1_elec(ii)]=calc_pertes_acm_fw(ERR,VD.ACM1,pacm1_elec(ii),wacm1_elec(ii),dwacm1_elec(ii));
            cacm1_elec(ii)=(pacm1_elec(ii)-qacm1_elec(ii))./wacm1_elec(ii);
            qacm2_elec(ii)=0;
            cacm2_elec(ii)=0;
            wacm2_elec(ii)=0;
            dwacm2_elec(ii)=0;
            if param.bv_acm1==1 % cas acm1 en amont de la BV
                
                cprim2_adcpl1_elec(ii)= cacm1_elec(ii) - VD.ACM1.J_mg*dwacm1_elec(ii) ;
                [ERR,csec_adcpl1_elec(ii)]=calc_red_fw([],VD.ADCPL1,cprim2_adcpl1_elec(ii),wacm1_elec(ii),dwacm1_elec(ii),0);
                cprim_bv_elec(ii)=csec_adcpl1_elec(ii);
                %cprim_bv_elec(ii)= cacm1_elec(ii) - VD.ACM1.J_mg*dwacm1_elec(ii) ;
            elseif param.bv_acm1==2 % cas acm1 en amval de la BV, si seul acm1 fonctionne cprim_bv_elec=0
                
                cprim_bv_elec(ii)= 0;
            end
        elseif cacm1_max_elec(ii)*wacm1_elec(ii)+qacm1_max(i)+pacc(1)<pbat_min & ...
               cacm1_max_elec(ii)*wacm1_elec(ii)+qacm1_max(i)+cacm2_max_elec(ii)*wacm2_elec(ii)+qacm2_max(i)+pacc(1)>pbat_min & ...% em1 et em2 font tout le boulot
               param.emb2==1
            cacm1_elec(ii)=cacm1_max_elec(ii);
            qacm1_elec(ii)=qacm1_max(i);
            pacm1_elec(ii)=cacm1_max_elec(ii)*wacm1_elec(ii)+qacm1_max(i);
            pacm2_elec(ii)=pbat_min-pacc(ii)-pacm1_elec(ii);
            [ERR,qacm2_elec(ii)]=calc_pertes_acm_fw(ERR,VD.ACM2,pacm2_elec(ii),wacm2_elec(ii),dwacm2_elec(ii));
            cacm2_elec(ii)=(pacm2_elec(ii)-qacm2_elec(ii))./wacm2_elec(ii);
            
            if param.bv_acm1==1 % cas acm1 en amont de la BV
                cprim2_adcpl2_elec(ii)=cacm2_elec(ii) - VD.ACM2.J_mg*dwacm2_elec(ii) ;
                [ERR,csec_adcpl2_elec(ii)]=calc_red_fw([],VD.ADCPL2,cprim2_adcpl2_elec(ii),wacm2_elec(ii),dwacm2_elec(ii),0);
                
                cprim1_adcpl1_elec(ii)=csec_adcpl2_elec(ii);
                
                cprim2_adcpl1_elec(ii)=cacm1_elec(ii) - VD.ACM1.J_mg*dwacm1_elec(ii) ;
                [ERR,~,~,~,~,csec_adcpl1_elec(ii)]=calc_adcpl_fw([],VD.ADCPL1,cprim1_adcpl1_elec(ii),0,cprim2_adcp1_elec(ii),0,wprim_bv_elec(ii),dwprim_bv_elec(ii));
                cprim_bv_elec(ii)=csec_adcpl1_elec(ii);
                %cprim_bv_elec(ii)= cacm1_elec(ii) +  cacm2_elec(ii) - VD.ACM1.J_mg*dwacm1_elec(ii) - VD.ACM1.J_mg*dwacm2_elec(ii);
                
            elseif param.bv_acm1==2 % cas acm1 en aval de la BV, 
                cprim2_adcpl2_elec(ii)=cacm2_elec(ii) - VD.ACM2.J_mg*dwacm2_elec(ii) ;
                [ERR,csec_adcpl2_elec(ii)]=calc_red_fw([],VD.ADCPL2,cprim2_adcpl2_elec(ii),wacm2_elec(ii),dwacm2_elec(ii),0);
                
                cprim1_adcpl1_elec(ii)=csec_adcpl2_elec(ii);
                
                cprim_bv_elec(ii)= csec_adcpl2_elec(ii);
            end
        else % ce cas ne peut normalement pas se produire car deja limite avant calcul
        end
    end
end

% calcul de la pbat_max

% A priori si il reste des NaN c'est du � une pres trop importante donc on
% met ibat_max � BATT.Ibat_max et il fau taussi remettre le rendement
% faradique � sa bonne valeur !!!! 
I_nan=find(isnan(ibat_max));
ibat_max(I_nan)=interp1(VD.BATT.DoD_Ibat_max,VD.BATT.Ibat_max,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension
RdF(I_nan)=1;
% Cela doit aussi etre le cas si mode elec possible avec 2 machines mais
% pres trop importante donc ibatt doit etre � ibatt max (on risque sinon de
% choisir le cas 1 machine
%I_elec_pos_2em_ibatt_max=find(ind_melec_np==0 & ind_melec_np_acm1==1);

% Sur certain point de recup ont peut arriver a faire plus de pertes dans acm1
% que l'on ne recupere de puissance sur le primaire reducteur, on regarde
% alors ce qu'ils se passe si on alimente pas la machine
% Dans le cas rapp_vit optimise et acm1 en amont BV cela ne doit plus se produire 
% Att : Si  on arrive dans des cas ou le couple de frottement de la machine
% est plus faible que la couple ramenee du reducteur ce fontionnement est
% impossible il faut donc quand meme alimté la machine pour suivre le cycle.
pres_acm_na=pres;
pres_acm_na(cprim_red<0)=pacc(cprim_red<0);
[ERR,ibat_max_acm1_na]=calc_batt(ERR,VD.BATT,param.pas_temps,pres_acm_na,100-VD.INIT.Dod0,0,-1,0,param);

if param.bv_acm1==1 % cas acm1 en amont de la BV
    
    I=find(cprim_red<0 & qacm1_elec+qacm2_elec>(-cacm1_elec.*wacm1_elec-cacm2_elec.*wacm2_elec) & elec_pos==1 &...
    cfrott_acm1_elec>cprim_bv_elec);
    
    cacm1_elec(I)=cfrott_acm1(I);
    cprim2_adcpl1_elec(I)=cacm1_elec(I)-VD.ACM1.J_mg*dwacm1_elec(I);
    [ERR,cprim_bv_elec(I)]=calc_red_fw(ERR,VD.ADCPL1,cprim2_adcpl1_elec(I),wprim_bv_elec(I),dwprim_bv_elec(I),0);
    
    
elseif param.bv_acm1==2 % cas acm1 en aval de la BV
    
    I=find(cprim_red<0 & qacm1_elec+qacm2_elec>(-cacm1_elec.*wacm1_elec-cacm2_elec.*wacm2_elec) & elec_pos==1 &...
    cfrott_acm1_elec>cprim_red+VD.ACM1.J_mg*dwacm1_elec);
    
    cfrott_acm1(I)=cfrott_acm1_elec(I);
    cacm1_elec(I)=cfrott_acm1(I);
    cprim_bv_elec(I)=0;
    cprim2_adcpl1_elec(I)=cacm1_elec(I)-VD.ACM1.J_mg*dwacm1_elec(I);
    [ERR,cprim_red_elec(I)]=calc_red_fw(ERR,VD.ADCPL1,cprim2_adcpl1_elec(I),wprim_bv_elec(I),dwprim_bv_elec(I),0);
    
end

if param.emb2==1
    ind_acm1_na=sort([I ind_acm1_na]); % on doit rajouter ces cas ou acm1 na
else
    ind_acm1_na=I;
end


ibat_max(I)=ibat_max_acm1_na(I);

cacm2_elec(I)=0;
qacm1_elec(I)=0;
qacm2_elec(I)=0;
wacm2_elec(I)=0;
dwacm2_elec(I)=0;

%  TO COMMENT / UNCOMMENT
% %% Calcul des limites avec fonctions BK
% % Electric 1 EM
% I_elec1em=find(contains(fieldnames(VD)',"elec1em"));
% nb_mode_1em=sum(I_elec1em~=0);
% if nb_mode_1em>=1
%     for ii=1:nb_mode_1em
%         [ERR,ibat_max_elec1em(ii,:),r_elec1em(ii)]=calc_mode_elec_1EM_GENERAL(ERR,param,VD.(strcat('elec1em_',num2str(ii))),croue,wroue,dwroue,0);
%     end
%     for jj=1:nb_mode_1em
%         ind_wacm_max_1EM(jj,:)=r_elec1em(jj).ind_wacm_max;
%     end
% end
% % On regarde les points sur lesquels on depassera toujours wacm quelquesoit
% % les rapports de reduction
% ind_melec_1EM_np_wacm=(sum(ind_wacm_max_1EM,1)==nb_mode_1em);
% 
% 
% % Electric 2 EM
% I_elec2em=find(contains(fieldnames(VD)',"elec2em"));
% nb_mode_2em=sum(I_elec2em~=0);
% if nb_mode_2em>=1
%     for ii=1:nb_mode_2em
%         [ERR,ibat_max_elec2em(ii,:),r_elec2em(ii)]=calc_mode_elec_2EM_GENERAL(ERR,param,VD.(strcat('elec2em_',num2str(ii))),croue,wroue,dwroue,0);
%     end
%     for jj=1:nb_mode_2em
%         ind_wacm_max_2EM(jj,:)=r_elec2em(jj).ind_wacm_max;
%     end
% end
% % On regarde les points sur lesquels on depassera toujours wacm quelquesoit
% % les rapports de reduction
% ind_melec_2EM_np_wacm=(sum(ind_wacm_max_2EM,1)==nb_mode_2em);
% 
% % on regarde sur quel point on ne pourras faire du mode elec a cause de wacm
% ind_melec_np_wacm =[ind_melec_1EM_np_wacm; ind_melec_2EM_np_wacm];
% ind_melec_np_wacm = sum(ind_melec_np_wacm,1)== nb_mode_1em + nb_mode_2em;
% 
% %Si on a des indices a 1 dans ind_melec_np_wacm la seul solution possible est
% % de passer en tout termique si possibleibat_max
% 
% 
% % Nutral
% I_nutral=find(contains(fieldnames(VD)',"nutral"));
% nb_mode_nutral=sum(I_nutral~=0);
% if nb_mode_nutral>=1
%     for ii=1:nb_mode_nutral
%         [ERR,ibat_max_nutral(ii,:),r_nutral(ii)]=calc_mode_nutral_GENERAL(ERR,param,VD.(strcat('nutral_',num2str(ii))),croue,wroue,dwroue,0);
%     end
% 
% end
% 
% I_mode_tab=[ibat_max_elec1em; ibat_max_elec2em;ibat_max_nutral];
% elec_pos_gen_tab=[r_elec1em.elec_pos; r_elec2em.elec_pos; r_nutral.elec_pos];
% RdF_tab=[r_elec1em.RdF_elec; r_elec2em.RdF_elec; r_nutral.RdF_elec];
% [ibat_max_gen,I_mode_elec_gen]=min(I_mode_tab); 
% [elec_pos_gen,RdF_gen]=mat_ind(I_mode_elec_gen,2,elec_pos_gen_tab,RdF_tab);
% 
% 
% I_elec_pos_2EM=r_elec1em.elec_pos==0 &  (r_elec2em.elec_pos>0) ; % Mode elec possible que avec les deux machines �lectrique
% 
% % Mode elec impossible m�me avec les deux machines on doit prendre le courant max donc avec deux machines
% % sauf si croue <0 (cas idiot ou l'on a plus  pertes que en neutre donc on
% % restera en neutre, on laisse ibat � ibat_max avec 1EM et 2EM
% I_elec_np=r_elec1em.elec_pos==0 &  r_elec2em.elec_pos==0 & croue>0 ; 
% 
% I_2EM=find(I_elec_pos_2EM | I_elec_np);
% ibat_max_gen(I_2EM)=ibat_max_elec2em(I_2EM);
% elec_pos_gen(I_2EM)=r_elec2em.elec_pos(I_2EM);
% RdF_gen(I_2EM)=r_elec2em.RdF_elec(I_2EM);
% 
% 
%      
% if max(abs(RdF-RdF_gen))>1e-8 
%     'pb ref'
% end
% if max(abs(ibat_max-ibat_max_gen))>1e-8 
%      'pb ibat_max'
% end
% if ~isempty(find(isinf(ibat_max-ibat_max_gen)))
%     'pb ibat_max'
% end
% if ~isempty(find(isnan(ibat_max-ibat_max_gen)))
%     'pb ibat_max'
% end
% if max(abs(elec_pos-elec_pos_gen))>1e-8
%     'pb elec_pos'
% end

%%
[ERR,r_elec]=affect_struct(r_var_elec);

soc_min(1)=100-VD.INIT.Dod0;
Dsoc_min(1)=0;
for ii=2:length(VD.CYCL.temps)
    soc_min(ii)=soc_min(ii-1)-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_min(ii)=-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

if param.verbose>=2
    if isfield(VD.CYCL,'vitesse')
    figure(3)
    clf
    hold on
    plot(VD.CYCL.temps,ibat_max,'b',VD.CYCL.temps,VD.CYCL.vitesse,'k')
    legend('ibat max','vitesse')
    title('courant min max fin calc limite')
    grid
    end
    
    figure(4)
    clf
    hold on
    plot(VD.CYCL.temps,soc_min,'b',VD.CYCL.temps,60+Dsoc_min,'g')
    legend('soc min','Dsoc min')
    title('soc min max fin calc limite')
    grid
       
end



%% recherche de la pbat_min (mode hybride)
% calcul du couple max moteur thermique
% il faut verifier la possibilite de faire de l'hybride serie, ou de l'hybride //
% Cas hybride serie il faut que cacm1 suffisent a fournir le couple au reducteur

% recherche des indices ou les modes hybrides series sont possible et impossible (limitation sur cacm1)
if param.bv_acm1==1 % cas acm1 en amont de la BV
    [ERR,~,~,~,wacm1,dwacm1,cprim2_adcpl1_hs]=calc_adcpl([],VD.ADCPL1,cprim_bv,wprim_bv,dwprim_bv,zeros(size(cprim_bv)),0,2);
elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
    [ERR,~,~,~,wacm1,dwacm1,cprim2_adcpl1_hs]=calc_adcpl([],VD.ADCPL1,cprim_red,wprim_red,dwprim_red,zeros(size(cprim_red)),0,2);
end
ind_hs=find(cacm1_max>=cprim2_adcpl1_hs+VD.ACM1.J_mg*dwacm1);
ind_hs_n=find(cacm1_max<cprim2_adcpl1_hs+VD.ACM1.J_mg*dwacm1);

% cas hybride // il faut que wmt_max soit possible
%[ERR,~,wprim_red2,dwprim_red2]=calc_red(ERR,VD.RED2,ones(size(wprim_bv)),wprim_bv,zeros(size(wprim_bv)),0);
[ERR,~,wprim_red2,dwprim_red2]=calc_red(ERR,VD.RED2,ones(size(wprim_bv)),wprim_bv,dwprim_bv,0);
ind_hp=find(VD.MOTH.wmt_maxi>=wprim_red2);
ind_hp_n=find(VD.MOTH.wmt_maxi<wprim_red2);

% on recherche la puissance max possible du moteur thermique 
% Si l'on est en mode hybride serie
[~,i_pmt_max]=max(VD.MOTH.pmt_max);
wmt_pmax_hs=VD.MOTH.wmt_max(i_pmt_max);

pmax_mt_hs=interp1(VD.MOTH.wmt_max,VD.MOTH.pmt_max,wmt_pmax_hs);
cmt_max_hs=pmax_mt_hs./wmt_pmax_hs;

[ERR,~,wprim1_adcpl2_pmax_hs]=calc_red_fw(ERR,VD.RED2,ones(size(wmt_pmax_hs)),wmt_pmax_hs,zeros(size(wmt_pmax_hs)),0);
[ERR,~,wacm2_pmax_hs]=calc_red([],VD.ADCPL2,ones(size(wmt_pmax_hs)),wprim1_adcpl2_pmax_hs,zeros(size(wmt_pmax_hs)),0);

cacm2_max_hs=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2_pmax_hs);


%[ERR,cprim1_adcpl2_max_hs]=calc_red_fw(ERR,VD.ADCPL2,cacm2_max_hs,wacm2_pmax_hs,0,0);
% Attention signe du rendement on doit ecrire :
cprim1_adcpl2_max_hs=cacm2_max_hs*VD.ADCPL2.kred/VD.ADCPL2.rend;
[ERR,cprim_red2_max]=calc_red(ERR,VD.RED2,cprim1_adcpl2_max_hs,wprim1_adcpl2_pmax_hs,0,0);

if VD.CYCL.ntypcin==1 && param.bv_acm1==2 % cas rapport de boite libre et acm1 directement connectee au red final
    cmt_max_hs=min(cmt_max_hs,cprim_red2_max)*ones(size(wprim_red));
else
    cmt_max_hs=min(cmt_max_hs,cprim_red2_max)*ones(size(wprim_bv));
end
cmt_max_hs(ind_hs_n)=0;
cmt_max_hs(ind_hs_n)=0;

% calcul du couple max du moteur en hp
% si l'on est en hybride // (vitesse imposee)
% On sature la vitesse MOTH à VD.ECU.wmt_patinage
[ERR,~,wmt_hp,dwmt_hp]=calc_emb(VD,zeros(size(wprim_red2)),wprim_red2,dwprim_red2,ones(size(wprim_red2)));

[ERR,~,wsec_red2_hp,dwsec_red2_hp]=calc_red_fw(ERR,VD.RED2,ones(size(wmt_hp)),wmt_hp,dwmt_hp,0);
[ERR,~,wprim2_adcpl2_hp,dwprim2_adcpl2_hp]=calc_red([],VD.ADCPL2,ones(size(wmt_hp)),wsec_red2_hp,dwsec_red2_hp,0);

wacm2_hp=wprim2_adcpl2_hp;
dwacm2_hp=dwprim2_adcpl2_hp;


cmt_max_hp=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt_hp);
cmt_max_hp(ind_hp_n)=0;

%cmt_max=max(cmt_max_hs,cmt_max_hp);

% connexion en hybride serie
%[ERR,csec_red2_hs,wsec_red2_hs]=calc_red_fw(ERR,VD.RED2,-cmt_max_hs,wmt_pmax_hs*ones(size(cmt_max_hs)),zeros(size(cmt_max_hs)),0);
csec_red2_hs=-cmt_max_hs*VD.RED2.kred*VD.RED2.rend;
wsec_red2_hs=wmt_pmax_hs*ones(size(cmt_max_hs))/VD.RED2.kred;


[ERR,cacm2_hs,wacm2_hs,dwacm2_hs]=calc_red([],VD.ADCPL2,csec_red2_hs,wsec_red2_hs,zeros(size(cmt_max_hs)),0);

%cacm2_hs=-cmt_max_hs;
%wacm2_hs=wmt_pmax_hs;

cprim_red2_max_hp=cmt_max_hp-VD.MOTH.J_mt*dwmt_hp;
[ERR,csec_red2_max_hp]=calc_red_fw(ERR,VD.RED2,cprim_red2_max_hp,wmt_hp,dwmt_hp,0);
%cprim2_adcpl2_max_hp=cacm2_max+VD.ACM2.J_mg*dwacm2_hp;
cprim2_adcpl2_max_hp=cacm2_max-VD.ACM2.J_mg*dwacm2_hp; % EV 13-06-2019 changment signe + en -
cprim1_adcpl2_max_hp=csec_red2_max_hp;
[ERR,~,~,~,~,csec_adcpl2_max_hp]=calc_adcpl_fw(ERR,VD.ADCPL2,cprim1_adcpl2_max_hp,0,cprim2_adcpl2_max_hp,0,wprim_bv,dwprim_bv);

% On verifie quel'on passe le cycle
% indice ou hybride serie impossible et hybride parallele impossible 
if VD.CYCL.ntypcin==3 % cas rapport de boite imposee
    ind_wacm_max_hp = wacm1>max(VD.ACM1.Regmot_cmax) | wacm2_hp>max(VD.ACM2.Regmot_cmax);
    ind_wacm_max_hs = wacm1>max(VD.ACM1.Regmot_cmax) | wacm2_hs>max(VD.ACM2.Regmot_cmax);
    
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        
        cprim1_adcpl1_max=csec_adcpl2_max_hp;
        [ERR,~,~,~,~,csec_adcpl1_max]=calc_adcpl_fw(ERR,VD.ADCPL1,cprim1_adcpl1_max,0,cacm1_max-VD.ACM1.J_mg*dwacm1,0,wprim_bv,dwprim_bv);
        ind=find( (cacm1_max<cprim2_adcpl1_hs+VD.ACM1.J_mg*dwacm1 | ind_wacm_max_hs) & ...%hybride serie
            ( VD.MOTH.wmt_maxi<wprim_bv | csec_adcpl1_max<cprim_bv | ind_wacm_max_hp ));
        
    elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
      
        cprim_bv_max_hp=csec_adcpl2_max_hp;
        [ERR,csec_bv_max_hp]=calc_bv_fw([],VD,cprim_bv_max_hp,wprim_bv,dwprim_bv,0);
        [ERR,~,~,~,~,csec_adcpl1_max]=calc_adcpl_fw(ERR,VD.ADCPL1,csec_bv_max_hp,0,cacm1_max-VD.ACM1.J_mg*dwacm1,0,wprim_bv,dwprim_bv);
        ind=find( (cacm1_max<cprim2_adcpl1_hs+VD.ACM1.J_mg*dwacm1 | ind_wacm_max_hs) & ...%hybride serie
            (VD.MOTH.wmt_maxi<wprim_bv | csec_adcpl1_max<cprim_red | ind_wacm_max_hp) );
        
    end
    
elseif VD.CYCL.ntypcin==1 % cas rapport de boite libre
    
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        
        ind_wacm_max_hp = wacm1>max(VD.ACM1.Regmot_cmax) | wacm2_hp>max(VD.ACM2.Regmot_cmax);
        ind_wacm_max_hs = wacm1>max(VD.ACM1.Regmot_cmax) | wacm2_hs>max(VD.ACM2.Regmot_cmax);
        
        cprim1_adcpl1_max=csec_adcpl2_max_hp;
        [ERR,~,~,~,~,csec_adcpl1_max]=calc_adcpl_fw(ERR,VD.ADCPL1,cprim1_adcpl1_max,0,cacm1_max-VD.ACM1.J_mg*dwacm1,0,wprim_bv,dwprim_bv);
        ind_2D= (cacm1_max<cprim2_adcpl1_hs+VD.ACM1.J_mg*dwacm1 | ind_wacm_max_hs) & ... % hybride serie
            (VD.MOTH.wmt_maxi<wprim_bv | csec_adcpl1_max<cprim_bv  | ind_wacm_max_hp); % hybride //
        ind=find( sum(ind_2D(2:end,:))==(nrap-1) & VD.CYCL.vitesse~=0 );
        
    elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
        
        ind_wacm_max_hp = repmat(wacm1,[nrap 1])>max(VD.ACM1.Regmot_cmax) | wacm2_hp>max(VD.ACM1.Regmot_cmax);
        ind_wacm_max_hs = wacm1>max(VD.ACM1.Regmot_cmax) | wacm2_hs>max(VD.ACM1.Regmot_cmax);
        
        cprim_bv_max=csec_adcpl2_max_hp;
        [ERR,csec_bv_max_hp]=calc_bv_fw([],VD,cprim_bv_max,wprim_bv,dwprim_bv,0);
        [ERR,~,~,~,~,csec_adcpl1_max]=calc_adcpl_fw(ERR,VD.ADCPL1,csec_bv_max_hp,0,repmat(cacm1_max-VD.ACM1.J_mg*dwacm1,[nrap 1]),0,wprim_bv,dwprim_bv); 
        ind_2D= repmat((cacm1_max<cprim2_adcpl1_hs+VD.ACM1.J_mg*dwacm1 | ind_wacm_max_hs),[nrap 1]) & ... % hybride serie
            ( VD.MOTH.wmt_maxi<wprim_bv | csec_adcpl1_max<repmat(cprim_red,[nrap 1]) | ind_wacm_max_hp ); % hybride //
        ind=find( sum(ind_2D(2:end,:))==(nrap-1) & VD.CYCL.vitesse~=0 );
        
    end
end

% indice ou hybride impossible 
if ~isempty(ind)
    chaine=' Les performances des machines ne permettent pas de passer le cycle aux instant suivants : \n %s \n';
    ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine,num2str(ind));
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    return
    %[ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,indice_rap_opt_elec]=backward_erreur(length(VD.CYCL.temps),0,0);
end


% calcul du courant min en hs
[ERR,qacm2_hs]=calc_pertes_acm(ERR,VD.ACM2,cacm2_hs,wacm2_hs,0);
pelecacm2_hs=cacm2_hs.*wacm2_hs+qacm2_hs;

cacm1_hs=cprim2_adcpl1_hs+VD.ACM1.J_mg*dwacm1;

[ERR,qacm1_hs]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hs,wacm1,dwacm1);
pelecacm1_hs=cacm1_hs.*wacm1+qacm1_hs;


pres_hs=pelecacm1_hs+pelecacm2_hs+pacc(1); % Normalement il y as un survolteur entre pbat et pres !!  

pbat_hs=pres_hs;
[ERR,ibat_min_hs,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_hs,100-VD.INIT.Dod0,0,-1,0,param); 
ibat_min_hs(isnan(ibat_min_hs))=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0);


if VD.CYCL.ntypcin==1 % cas rapport de boite libre on choisit le courant min
    % On met a inf les points impossible (point mort en traction, vitesse engagé à l'arrêt)
    ibat_min_hs(ind_wacm_max_hs)=Inf;
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        ibat_min_hs(cprim_bv==Inf)=Inf;
        ibat_min_hs=min(ibat_min_hs);
    end

end

%% TO COMMENT / UNCOMMENT
% [ERR,ibat_min_hs_emb,RdF_hs_emb,ind_hs_np_emb]=calc_mode_HS_GENERAL(ERR,param,VD.serie_1,croue,wroue,dwroue);
% [ERR,ibat_min_hs_rech1em,RdF_rech1em,ind_rech1em_np]=calc_mode_rech1em_GENERAL(ERR,param,VD.rech1em_1,croue,wroue,dwroue);
% 
% [ibat_min_hs_gen,I_min_ibat_min_hs_gen]=min([ibat_min_hs_rech1em; ibat_min_hs_emb],[],1);
% % continue here : choose the minimum
% 
% if max(abs(ibat_min_hs-ibat_min_hs_gen))>1e-8 
%      'pb ibat_min_hs'
% end
% if find(isnan(ibat_min_hs) & ~isnan(ibat_min_hs_gen)) 
%     'pb_ibat_hs_nan'
% end
% if find(~isnan(ibat_min_hs) & isnan(ibat_min_hs_gen)) 
%     'pb_ibat_hs_nan2'
% end

%%
% en hybride // en recup, on suppose que cacm1 marcherai au max en gene puis si ca suffit pas cacm2
wacm1_hp=wacm1;
dwacm1_hp=dwacm1;
% Dans un premier temps on suppose que seul acm1 est alimentée
if param.bv_acm1==1 % cas acm1 en amont de la BV

    cprim1_adcpl1_hp=csec_red2_max_hp-VD.ACM2.J_mg*dwacm2_hp*VD.ADCPL2.kred./(VD.ADCPL2.rend.^sign(dwacm2_hp));
    [ERR,~,~,~,~,~,cprim2_adcpl1_hp]=calc_adcpl([],VD.ADCPL1,cprim_bv,wsec_red2_hp,dwsec_red2_hp,cprim1_adcpl1_hp,0,2);% Attention vitesse wsec_red2_hp devrait etre prim1_adcpl1
    cacm1_hp=cprim2_adcpl1_hp+VD.ACM1.J_mg*dwacm1_hp;
    
elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
    
    cprim_bv_max=csec_red2_max_hp-VD.ACM2.J_mg*dwacm2_hp*VD.ADCPL2.kred./(VD.ADCPL2.rend.^sign(dwacm2_hp));
    [ERR,csec_bv_max_hp]=calc_bv_fw([],VD,cprim_bv_max,wprim_bv,dwprim_bv,0);
    [ERR,~,~,~,~,~,cprim2_adcpl1_hp]=calc_adcpl([],VD.ADCPL1,repmat(cprim_red,[nrap 1]),repmat(wprim_red,[nrap 1]),repmat(dwprim_red,[nrap 1]),csec_bv_max_hp,0,2); % NB en rapp vit impose nrap vaut 1
    cacm1_hp=cprim2_adcpl1_hp+VD.ACM1.J_mg*repmat(dwacm1_hp,[nrap 1]);% NB en rapp vit impose nrap vaut 1

end

% Si acm1 ne suffit pas on sature cacm1 a son max et on alimente acm2
if VD.CYCL.ntypcin==1 & param.bv_acm1==2 % cas rapport de boite libre et acm1 directement connectee au red final 
    cacm1_max_2D=repmat(cacm1_max,[nrap 1]);
    cond=(cacm1_hp>cacm1_max_2D);
    cacm1_hp(cond)=cacm1_max_2D(cond);
    
    %BK added below:--------------------------------------
    cacm1_min_2D=repmat(cacm1_min,[nrap 1]);
    cond_min=(cacm1_hp<cacm1_min_2D);
    cacm1_hp(cond_min)=cacm1_min_2D(cond_min);
    %-------------------------------------------------------
    
    [ERR,qacm1_hp]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hp,repmat(wacm1_hp,[nrap 1]),0);
    pelecacm1_hp=cacm1_hp.*repmat(wacm1_hp,[nrap 1])+qacm1_hp;

else
     % au point mort wacm1 dopt etre sature fonction de ralenti moteur
    [ERR,~,wacm1_hp_pm,dwacm1_hp_pm]=calc_red([],VD.ADCPL1,ones(size(wmt_hp(1,:))),wsec_red2_hp(1,:),dwsec_red2_hp(1,:),0);
    wacm1_hp(1,:)=wacm1_hp_pm(1,:);
    cond=(cacm1_hp>cacm1_max);
    cacm1_hp(cond)=cacm1_max(cond);
    
    %BK added below:--------------------------------------
    cond_min=(cacm1_hp<cacm1_min);
    cacm1_hp(cond_min)=cacm1_min(cond_min);
    %-------------------------------------------------------
        
    [ERR,qacm1_hp]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hp,wacm1_hp,0);
    pelecacm1_hp=cacm1_hp.*wacm1_hp+qacm1_hp;

end

cacm2_hp=zeros(size(wprim_bv));

%% A verifier
if param.bv_acm1==1 % cas acm1 en amont de la BV
    
    cprim2_adcpl1_hp=cacm1_hp-VD.ACM1.J_mg*dwacm1_hp;
    [ERR,cprim1_adcpl1_hp]=calc_adcpl([],VD.ADCPL1,cprim_bv,wprim_bv,dwprim_bv,cprim2_adcpl1_hp,0,1);  
    
    csec_adcpl2_hp=cprim1_adcpl1_hp;
    [ERR,~,~,~,~,~,cprim2_adcpl2_hp]=calc_adcpl(ERR,VD.ADCPL2,csec_adcpl2_hp,wprim_bv,dwprim_bv,csec_red2_max_hp,0,2);
    % BK : csec_red2_max_hp?? or cprim_bv_max in my case
    
    cacm2_hp(cond)=cprim2_adcpl2_hp(cond)+VD.ACM2.J_mg*dwacm2_hp(cond);
        
    %BK added below:--------------------------------------
    cacm2_hp(cond_min)=cprim2_adcpl2_hp(cond_min)+VD.ACM2.J_mg*dwacm2_hp(cond_min);
    %-------------------------------------------------------
   
elseif param.bv_acm1==2 % cas acm1 directement connectee au red final
    
    if VD.CYCL.ntypcin==3 % rapport de boite imposee
        
        cprim2_adcpl1_hp=cacm1_hp-VD.ACM1.J_mg*dwacm1_hp;
        [ERR,cprim1_adcpl1_hp]=calc_adcpl([],VD.ADCPL1,cprim_red,wprim_red,dwprim_red,cprim2_adcpl1_hp,0,1);
        csec_bv_hp=cprim1_adcpl1_hp;
        [ERR,cprim_bv_hp]=calc_bv(VD,csec_bv_hp,wsec_bv,dwsec_bv,0);
        
        csec_adcpl2_hp=cprim_bv_hp;
        [ERR,~,~,~,~,~,cprim2_adcpl2_hp]=calc_adcpl(ERR,VD.ADCPL2,csec_adcpl2_hp,wprim_bv,dwprim_bv,csec_red2_max_hp,0,2);
        
        cacm2_hp(cond)=cprim2_adcpl2_hp(cond)+VD.ACM2.J_mg*dwacm2_hp(cond);
        
        %BK added below:--------------------------------------
        cacm2_hp(cond_min)=cprim2_adcpl2_hp(cond_min)+VD.ACM2.J_mg*dwacm2_hp(cond_min);
        %-------------------------------------------------------    
    elseif VD.CYCL.ntypcin==1 % rapport de boite libre
        
        cprim2_adcpl1_hp=cacm1_hp-VD.ACM1.J_mg*repmat(dwacm1_hp,[nrap 1]);
        [ERR,cprim1_adcpl1_hp]=calc_adcpl([],VD.ADCPL1,repmat(cprim_red,[nrap 1]),repmat(wprim_red,[nrap 1]),repmat(dwprim_red,[nrap 1]),cprim2_adcpl1_hp,0,1);
        csec_bv_hp=cprim1_adcpl1_hp;
        [ERR,cprim_bv_hp]=calc_bv(VD,csec_bv_hp,repmat(wsec_bv,[nrap 1]),repmat(dwsec_bv,[nrap 1]),0,4);
        
        csec_adcpl2_hp=cprim_bv_hp;
        [ERR,~,~,~,~,~,cprim2_adcpl2_hp]=calc_adcpl(ERR,VD.ADCPL2,csec_adcpl2_hp,wprim_bv,dwprim_bv,csec_red2_max_hp,0,2);
        
        cacm2_hp(cond)=cprim2_adcpl2_hp(cond)+VD.ACM2.J_mg*dwacm2_hp(cond);
        
        %BK added below:--------------------------------------
        cacm2_hp(cond_min)=cprim2_adcpl2_hp(cond_min)+VD.ACM2.J_mg*dwacm2_hp(cond_min);
        %-------------------------------------------------------    
        
    end
end

% on sature cacm2 a son max/min si necessaire
cond2=(cacm2_hp>cacm2_max)&~isinf(cacm2_hp);
cacm2_hp(cond2)=cacm2_max(cond2);

cond3=(cacm2_hp<cacm2_min)&~isinf(cacm2_hp);
cacm2_hp(cond3)=cacm2_min(cond3);

[ERR,qacm2_hp]=calc_pertes_acm(ERR,VD.ACM2,cacm2_hp,wacm2_hp,0);
pelecacm2_hp=cacm2_hp.*wacm2_hp+qacm2_hp;

% calcul de ibatmin
pres_hp=pelecacm1_hp+pelecacm2_hp+pacc(1); % Normalement il y as un survolteur entre pbat et pres !!
pbat_hp=pres_hp;
% A l'arret et au point mort on ne fera rien donc pbatt=pacc
% Le cas recharg batt par acm2 est trait� dansle cas hs, le cas recharge
% sur les deux machines n'est pas trait�
%I_vit_zero=find(wroue==0);
%pbat_hp(1,I_vit_zero)=pacc(1);
[ERR,ibat_min_hp,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_hp,100-VD.INIT.Dod0,0,-1,0,param); 

ibat_min_hp(isnan(ibat_min_hp))=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0);

if VD.CYCL.ntypcin==1 % on doit choisir le courant min
    % On met a inf les points impossibles (point mort en traction, vitesse engage à l'arret)
    ibat_min_hp(ind_wacm_max)=Inf;
    if param.bv_acm1==1 % cas acm1 en amont de la BV
        ibat_min_hp(cprim_bv==Inf)=Inf;
    end
    ibat_min_hp=min(ibat_min_hp);
    %BK
end

% %%  TO COMMENT / UNCOMMENT
% [ERR,ibat_min_hp_2em,RdF_hp_2em,ind_hp_np_2em]=calc_mode_HP_2EM_GENERAL(ERR,param,VD.parallel2em_1,croue,wroue,dwroue);
% [ERR,ibat_min_hp_rech2em,RdF_rech2em,ind_rech2em_np]=calc_mode_rech2em_GENERAL(ERR,param,VD.parallel2em_1,croue,wroue,dwroue);
% 
% [ERR,ibat_min_hp_1em,RdF_hp_1em,ind_hp_np_1em]=calc_mode_HP_1EM_GENERAL(ERR,param,VD.parallel1em_1,croue,wroue,dwroue);
% [ERR,ibat_min_hp_rech1em,RdF_rech1em,ind_rech1em_np]=calc_mode_rech1em_ral_GENERAL(ERR,param,VD.rech1em_2,croue,wroue,dwroue);
% 
% [ibat_min_hp_1em_gen,I_min_ibat_min_hp_1em_gen]=min([ibat_min_hp_rech1em; ibat_min_hp_1em],[],1);
% 
% VD.ADCPL=VD.ADCPL1;
% param.fit_pertes_acm=0;
% 
% [ERR,c1,w1,dw1]=calc_red(ERR,VD.RED,croue,wroue,dwroue,0);
% [ERR,c,w,dw]=calc_bv(VD,c1,w1,dw1,0);
% [ERR,ibat_max_test_1hp,ibat_min_test_1hp,elec_pos_test_1hp,soc_min_test_1hp,soc_max_test_1hp,Dsoc_min_test_1hp,Dsoc_max_test_1hp,indice_rap_opt_elec_test_1hp]=...
%      calc_limites_HP_BV_2EMB(ERR,param,VD,c,w,dw);
% 
% if max(abs(ibat_min_hp_1em_gen-ibat_min_test_1hp))>1e-8
%      'pb ibat_min_hp_1em'
% end
% 
% % figure
% % plot(ibat_min_test_1hp,'r')
% % hold on
% % plot(ibat_min_hp_1em_gen)
% % figure
% % plot(ibat_min_hp_1em_gen-ibat_min_test_1hp)
% % need to add also the rech2em instead of rech1em
% 
% [ibat_min_hp_gen,I_min_ibat_min_hp_gen]=min([ibat_min_hp_rech2em; ibat_min_hp_2em],[],1);
% 
% %ibat_min_hp_gen=ibat_min_hp_2em; 
% 
% %if max(abs(ibat_min_hp-ibat_min_hp_gen))>1e-8
% if max(abs(ibat_min_hp-ibat_min_hp_gen))>1e-8
%      'pb ibat_min_hp'
% end

%%
ibat_min=min(ibat_min_hp,ibat_min_hs);

% Si on arrive a ibat_min>ibat_max (peut arriver en recup max si les
% limites sont atteintes) on impose ibat_min au min <=ibat_max
ibat_min(ibat_min>ibat_max)=ibat_max(ibat_min>ibat_max);

soc_max(1)=100-VD.INIT.Dod0;
Dsoc_max(1)=0;

for ii=2:length(VD.CYCL.temps)
    soc_max(ii)=soc_max(ii-1)-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_max(ii)=-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

if sum(ibat_max<ibat_min)>0 ||  sum(soc_max<soc_min)>0 || sum(Dsoc_max<Dsoc_min)>0
    chaine='error in calc_limites_HPDP_EPI_SURV' ;
    ERR=MException('BackwardModel:calc_HPDP_EPI_SURV',chaine);
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    return

end

if param.verbose>=2
   
    figure(3)
    plot(VD.CYCL.temps,ibat_min,'g')
    legend('ibat max','vitesse','ibat min')
    title('courant min max fin calc limite')
    
    figure(4)
    plot(VD.CYCL.temps,soc_max,'k',VD.CYCL.temps,Dsoc_max+60,'r')
    legend('soc min','Dsoc min','soc max','Dsoc max')
    title('soc min max fin calc limite')

end

