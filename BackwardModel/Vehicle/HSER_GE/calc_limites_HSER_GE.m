% Calcul des limites du graphes.
% On ne calcule ici que les soc min et max sans se preocuper
% de la remonte du graphe ou de soc <0 ou >100%.
% La limite min doit etre calcule avec precision (mode tout elec)
% la limite max peut etre calcule "grossierement" d'eventuel arc impossible
% dans le graphe seront elimine par la suite
%
% interface entree :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discreetisation etc)
% CYLC,VD.ADCPL...:structures de description vehicule et composants
% pres :   la puissance electrique a fournir pour le cycle et les
% auxiliaires
% 
% interface de sortie
% ibat_max :    courant max mode tout elec (en recup c'est le courant de recup en tout elec)
% ibat_min :    courant min recharge max ( en recup courant recup plus recharge par moth)
% elec_pos :    vecteur indiquant si mode elec possible , ou zero si impossible
% soc_min :     soc correspondant a ibat_max
% soc_max :     soc correspondant a ibat_min
% Dsoc_min :    variation soc entre deux instant correspondant a ibat_max
%               (atention a integre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% Dsoc_max :    variation soc entre deux instant correspondant a ibat_min
%               (atention a integre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=calc_limites_HSER_GE(ERR,param,VD,pres)

ERR=[];
ii_max=size(VD.CYCL.temps);
soc_min=zeros(ii_max);
soc_max=zeros(ii_max);
pbat_max=zeros(ii_max);
pbat_min=zeros(ii_max);

%% recherche du mode tout electrique a chaque pas de temps
% Decharge maximum

% prise en compte limitation batterie 
% on range dans elec_pos les indices des points ou le mode tout elec est
% possible

[ERR,ibat_max,ubat,soc,ah,E,R,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,100-VD.INIT.Dod0,0,-1,0);


if param.mode_elect==2
    elec_pos=ones(1,length(VD.CYCL.temps));
    elec_pos(VD.CYCL.vitesse~=0)=0;
elseif param.mode_elect==1
    elec_pos=ones(1,length(VD.CYCL.temps));
else
    elec_pos=zeros(1,length(VD.CYCL.temps));
end
% elec_pos(VD.CYCL.vitesse==0)=1;

if sum(isnan(ibat_max))~=0
    % Le mode electrique n'est pas possible
    elec_pos(isnan(ibat_max))=0;
    % Peux t'on faire le cycle ?
    % Calcul de pbat_max
    ibat_max(isnan(ibat_max))=interp1(VD.BATT.DoD_Ibat_max,VD.BATT.Ibat_max,VD.INIT.Dod0)*VD.BATT.Nbranchepar;
    [ERR,ubat]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat_max,100-VD.INIT.Dod0,0);
    pbat_max=ubat.*ibat_max;
    % Calcul de pge_max
    pge_max=max(VD.GE.pge_opti)*ones(size(VD.CYCL.temps));
    if sum((pbat_max+pge_max)<pres)
        % Pas de solution
        chaine='calc_limites_HSER_GE: Les caracteristiques du systeme ne permettent pas de satisfaire les conditions demandees !';
        ERR=MException('BackwardModel:calc_limites_HSER_GE',chaine);
        [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
        return;
    end
end

soc_min(1)=100-VD.INIT.Dod0;
Dsoc_min(1)=0;
for ii=2:length(VD.CYCL.temps)
    soc_min(ii)=soc_min(ii-1)-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_min(ii)=-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% recherche de la pbat_min (mode hybride, recharge maximum)
elhyb=ones(1,length(VD.CYCL.temps));

% Calcul de la puissance maxi du GE
pge=max(VD.GE.pge_opti)*ones(size(VD.CYCL.temps));

% Puissance reseau electrique
pbat=pres-pge;


% prise en compte limitation batterie 
[ERR,ibat_min,ubat_min,soc,ah,E,R,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat,100-VD.INIT.Dod0,0,-1,0);

% bricolage pour batterie toshiba
ibat_min(pbat==pbat_max & isnan(ibat_min))=ibat_max(pbat==pbat_max & isnan(ibat_min));
ibat_min(ibat_min>ibat_max)=ibat_max(ibat_min>ibat_max);

% calcul de la pbat_min
ibat_min(isnan(ibat_min))=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0)*VD.BATT.Nbranchepar; % non pas si on eclate les limites en tension

soc_max(1)=100-VD.INIT.Dod0;
Dsoc_max(1)=0;
for ii=2:length(VD.CYCL.temps)
    soc_max(ii)=soc_max(ii-1)-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_max(ii)=-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

% % % 
%  figure(1)
%  clf
%  plot(VD.CYCL.temps,ibat_min,VD.CYCL.temps,ibat_max,VD.CYCL.temps,VD.CYCL.vitesse)
% 
%  figure(2)
%  clf
%  plot(VD.CYCL.temps,soc_min,VD.CYCL.temps,soc_max,VD.CYCL.temps,60+Dsoc_min,VD.CYCL.temps,Dsoc_max+60)
%  pause

if param.verbose>1
    assignin('base','soc_elec',soc_min)
end
