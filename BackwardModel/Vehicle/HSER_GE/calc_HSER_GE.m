%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calcul d'un vehicule hybride serie avec groupe electrogene
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,VD,ResXml]=calc_HSER_GE(ERR,vehlib,param,VD)


% Calcul cinematiques complementaires
% Avec re-echantillonnage eventuel
if ~isfield(param,'pas_temps') | param.pas_temps==0
    param.pas_temps=1; % si param_pas temps n'existe pas ou =0 on reechantillone ? 1s
end
[ERR,VD,dist]=calc_cinemat(ERR,VD,param);

if ~isempty(ERR)
ResXml=struct([]);   %    [conso100,co2_gkm,co2_inst,ah,soc]=backward_erreur(length(VD.CYCL.temps),2);
    
    return;
end

% Initialisation
soc=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
pbat=zeros(size(VD.CYCL.temps));
cretenue=zeros(size(VD.CYCL.temps)); % effort de retenue a l'arret (effort de frein par rapport a la pente)

% Calcul masse vehicule et inertie
[ERR,masse,Jveh]=bilan_masse_inertie(ERR,VD,vehlib,'');
if ~isempty(ERR)
    ResXml=struct([]);   
    return;
end

% Calcul des efforts a la roue
[ERR,croue,wroue,dwroue,penteFpk,fres,faero,froul,fpente,force1,force2,force3]=calc_efforts_vehicule(ERR,VD,masse,Jveh);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

cveh=croue;
Proue=croue.*wroue;

% separation effort en mouvement et effort de retenue a l'arret
cretenue(wroue==0)=croue(wroue==0);
ctraction=croue-cretenue;

% couple de traction/freinage sur l'essieu moteur, freinage suivant repartition
%[ctraction]=calc_ctraction_frein_reparti(ctraction);

% Connexion
csec_red=ctraction;
wsec_red=wroue;
dwsec_red=dwroue;

% Calcul des conditions en amont du reducteur
[ERR,cprim_red,wprim_red,dwprim_red]=calc_red(ERR,VD.RED,csec_red,wsec_red,dwsec_red,0);
if ~isempty(ERR)
    ResXml=struct([]); 
    return;
end

% Connexion
wacm1=wprim_red;
dwacm1=dwprim_red;
cacm1=cprim_red/VD.VEHI.nbacm1+VD.ACM1.J_mg.*dwacm1;

% Prise en compte des couples min du moteur et impos?? par le calculateur (alors le reste
% est du frein meca)
cacm1=max(cacm1,interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,1),wacm1));
%cacm1=max(cacm1,interp1(VD.ECU.Vit_Cmin,VD.ECU.Cpl_min,wacm1));

% Recalcul de la partie frein meca au niveau de la roue
cprim_red=(cacm1-VD.ACM1.J_mg.*dwacm1)*VD.VEHI.nbacm1;
wprim_red=wacm1;
dwprim_red=dwacm1;
%[ERR,csec_red,wsec_red,dwsec_red,Pertes_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
[ERR,csec_red,wsec_red,dwsec_red,Jsec,Pertes_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
cfrein_meca=csec_red-croue;

% Calcul des conditions en amont de la machine electrique
% Ici, on ne peut pas s assurer que la Me satisfait les conditions
% dynamiques demandees (on ne connait pas sa tension d'alimentation).
% [Li,Co]=size(cacm1);
% wacm1=repmat(wacm1,Li,1);
[ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% detection cacm1>Cmax
WAR=[];
if ~isempty(find(cacm1>max(max(VD.ACM1.Cmax_mot)), 1))
    ind=find(cacm1>max(max(VD.ACM1.Cmax_mot)));
    chaine=' Probleme de surcouple Machine electrique !\n ind : %s \n cacm1 : %s \n wacm1 : %s \n qacm1 : %s \n ';
    WAR=warning('BackwardModel:calc_HSER_GE',chaine,num2str(ind),num2str(cacm1(ind)),num2str(wacm1(ind)),num2str(qacm1(ind)));
%     return;
end

% detection survitesse
if ~isempty(find(wacm1>VD.ACM1.Regmot_cmax( find(VD.ACM1.Cmax_mot(:,find(max(VD.ACM1.Tension_cont)))==0,1)), 1))
    ind=find(wacm1>VD.ACM1.Regmot_cmax( find(VD.ACM1.Cmax_mot(:,find(max(VD.ACM1.Tension_cont)))==0,1)), 1);
    chaine=' Probleme de survitesse Machine electrique !\n ind : %s \n cacm1 : %s \n wacm1 : %s \n qacm1 : %s \n ';
    WAR=warning('BackwardModel:calc_HSER_GE',chaine,num2str(ind),num2str(cacm1(ind)),num2str(wacm1(ind)),num2str(qacm1(ind)));
%     return;
end

if ~isempty(WAR) 
    plot(wacm1*30/pi,cacm1,'+',VD.ACM1.Regmot_cmax*30/pi,VD.ACM1.Cmax_mot)
    ResXml=struct([]);
%     return;
end


% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);
if ~isempty(ERR)  
    ResXml=struct([]);
    return;
end

% Puissance reseau electrique
pres=VD.VEHI.nbacm1*(cacm1.*wacm1+qacm1)+pacc;

% Il faut tester si la batterie accepte cette puissance en freinage
% Calcul de la puissance batterie minimum
Ibmin=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0);
[ERR,Vbat,~,~,~,~,~]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,Ibmin*VD.BATT.Nbranchepar,100-VD.INIT.Dod0,0);
pbat_min=Vbat.*Ibmin*VD.BATT.Nbranchepar;
indice=find(pres<pbat_min);
if ~isempty(indice)
    % on sature la puissance reseau au maxi accepte par le recepteur
    pres(indice)=pbat_min;
    verbose=0;
    [ERR,VD.ACM1]=inverse_carto_melec(VD.ACM1,verbose);
    extrap=1;
    [ERR,qacm1_new]=calc_pertes_acm_fw(ERR,VD.ACM1,pres-pacc,wacm1,dwacm1,extrap);
    qacm1(indice)=qacm1_new(indice);
    cacm1(indice)=((pres(indice)-pacc(indice))./VD.VEHI.nbacm1-qacm1(indice))./wacm1(indice);

    % Recalcul de la partie frein meca au niveau de la roue
%     cprim_red=(cacm1-VD.ACM1.J_mg.*dwacm1)*VD.VEHI.nbacm1;
%     wprim_red=wacm1;
%     dwprim_red=dwacm1;
%     [ERR,csec_red,wsec_red,dwsec_red,Jsec,Pertes_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
%     cfrein_meca=csec_red-croue;
    %return
end

soc(1)=100-VD.INIT.Dod0;

% Courbe optimum GE
nb_iso=25;
nb_pt_iso=100;
if param.verbose>=2
    trace=1;
else
    trace=0; % 1 trace / 0 pas de trace de la courbe optimum
end

[VD]=ge_optimum_perte(VD,nb_iso,nb_pt_iso,trace);

% il faut tester si le groupe et la batterie peuvent fournir la puissance demandee
Ibmax=interp1(VD.BATT.DoD_Ibat_max,VD.BATT.Ibat_max,VD.INIT.Dod0);
[ERR,Vbat,~,~,~,~,~]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,Ibmax*VD.BATT.Nbranchepar,100-VD.INIT.Dod0,0);
pbat_max=Vbat.*Ibmax*VD.BATT.Nbranchepar;
pmax=pbat_max+max(VD.GE.pge_opti);
indice=find(pres>pmax, 1);

if ~isempty(indice)
    chaine='le groupe et la batterie ne peuvent pas fournir la puissance demandee'
    ERR=MException('BackwardModel:calc_HSER_GE',chaine);
    ResXml=struct([]);
    return;
end

% Cas fonctionnement tout electrique
if strcmpi(param.optim,'tout_elec')
    [ERR,elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]= ...
        calc_tout_elec_HSER_GE(ERR,param,vehlib,VD,pres);
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
end

% Cas GE a Pmax
if strcmpi(param.optim,'re_max')
    [ERR,elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]= ...
        calc_re_max_HSER_GE(ERR,param,vehlib,VD,pres);
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
end

% Cas prog dynamique
if strcmpi(param.optim,'prog_dyn')
    [ERR,elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]= ...
        calc_prog_dyn_HSER_GE(ERR,param,vehlib,VD,pres);
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
end

% Cas calcul variationnel
if strcmpi(param.optim,'lagrange')
    [ERR,elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah,lambda]= ...
        calc_lagrange_HSER_GE(ERR,param,vehlib,VD,pres);
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
end


% Cas calcul en ligne
if strcmpi(param.optim,'ligne')
    [ERR,elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]= ...
        calc_ligne_HSER_GE(ERR,param,vehlib,VD,pres);
    if ~isempty(ERR)
        ResXml=struct([]);
        return;
    end
end

% Recalcul de la partie frein meca au niveau de la roue
cprim_red=(cacm1-VD.ACM1.J_mg.*dwacm1)*VD.VEHI.nbacm1;
wprim_red=wacm1;
dwprim_red=dwacm1;
[ERR,csec_red,wsec_red,dwsec_red,Jsec,Pertes_red]=calc_red_fw(ERR,VD.RED,cprim_red,wprim_red,dwprim_red,0);
cfrein_meca=csec_red-croue;   

% Post-calculs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Pjbat=rbat.*ibat.*ibat;
p0bat=u0bat.*ibat;


bilanP=p0bat-Pjbat-wacm2.*cacm2-qacm2-VD.VEHI.nbacm1*qacm1-pacc-Pertes_red-Proue-cfrein_meca.*wroue-VD.ACM1.J_mg.*dwacm1.*wacm1*VD.VEHI.nbacm1;

% Post-calculs(Fin)  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pge=-wacm2.*cacm2-qacm2;
rendement_ge=pge./(dcarb.*VD.MOTH.pci);

% La machine permet-elle de satisfaire les conditions demandees
uacm1=ubat;
[ERR,wacm1,cacm1]=calc_enveloppe_acm(ERR,VD.ACM1,wacm1,cacm1,uacm1);
if ~isempty(ERR)
    ResXml=struct([]);
    return;
end

% Ecriture de grandeurs instantanees

assignin('base','bilanP',bilanP);

% Cycle
tsim=VD.CYCL.temps;
vit=VD.CYCL.vitesse;
acc=VD.CYCL.accel;
vit_dem=vit;
distance=VD.CYCL.distance; % distance calculee a partir de la vitesse VD.CYCL.vitesse
pente=interp1(VD.CYCL.PKpente,VD.CYCL.penteFpk,distance,'linear',0); % reechantillonage pente fonction distance claculee.     
% assignin('base','tsim',VD.CYCL.temps);
% assignin('base','vit',VD.CYCL.vitesse);
% assignin('base','distance',distance);
% assignin('base','pente',pente);

% calculateur
pdem=cacm1.*wacm1.*VD.VEHI.nbacm1;
cons_totale=cacm1.*VD.VEHI.nbacm1+cfrein_meca;
cons_trac=cacm1.*VD.VEHI.nbacm1;
cons_acm1=cacm1;
% alim_acm1=NaN.*zeros(size(VD.CYCL.temps));
cons_acm2=cacm2;
% alim_acm2=NaN.*zeros(size(VD.CYCL.temps));
cons_mt=cmt;
% alim_mt=NaN.*zeros(size(VD.CYCL.temps));
cacm1_max_bat=interp2(VD.ACM1.Regmot_cmax,VD.ACM1.Tension_cont,VD.ACM1.Cmax_mot',wacm1,uacm1);
cacm1_min_bat=-cacm1_max_bat;
Pdem_bat=(cacm2.*wacm2+qacm2)-pres;
Pdem_bat(Pdem_bat<0)=0;
Pdem_bat(cons_totale<0)=0;
wmtdes=wmt;

% Vehicule
cinertie=Jveh.*VD.CYCL.accel/VD.VEHI.Rpneu;
% F_vehi=cveh./VD.VEHI.Rpneu;
% P_vehi=F_vehi.*vit./1000;
pveh=croue.*wroue;
cfrein=cfrein_meca;
cfrein_tot=croue;
cfrein_tot(croue>0)=0;
% frein_av=abs(cfrein_tot./2./VD.VEHI.Rpneu);
% frein_ar=abs(cfrein_tot./2./VD.VEHI.Rpneu);

% ME
iacm1=(cacm1.*wacm1+qacm1)./ubat;
uacm1=ubat;

% Batterie
dod=100-soc;
if ~strcmpi(param.optim,'prog_dyn')
    ibat_min=Ibmin*VD.BATT.Nbranchepar*ones(size(VD.CYCL.temps));
    ibat_max=Ibmax*VD.BATT.Nbranchepar*ones(size(VD.CYCL.temps));
end
% flag_recharge=NaN.*zeros(size(VD.CYCL.temps)); 
% flag_stop_bat=NaN.*zeros(size(VD.CYCL.temps)); 

% accessoires
uacc=ubat;
iacc=pacc./uacc;

% GE
uacm2=ubat;
iacm2=pge./ubat;

% moteur thermique
cmt_opti=interp1(VD.MOTH.wmt_opti,VD.MOTH.cmt_opti,wmt);
conso=cumtrapz(VD.CYCL.temps,dcarb)./VD.MOTH.dens_carb;
% teau_mth=NaN.*zeros(size(VD.CYCL.temps));

% if strcmpi(param.optim,'lagrange')
%     assignin('base','lambda',lambda);
% end

% if strcmpi(param.optim,'ligne')
%     assignin('base','pbat_cible',pbat_cible)
% end

% Construction de la structure xml
if isscalar(u0bat)
    u0bat=u0bat*ones(size(VD.CYCL.temps)); % En prog. dyn le modele batterie est a soc constant donc u0bat scalaire
end
[ResXml]=miseEnForme4VEHLIB(vehlib,'caller');

% Calcul de resultats synthetiques
[Res]=resultats(VD,param,ResXml);
conso100=Res.conso100;
cumcarb=Res.cumcarb;
co2_eq=Res.co2_eq;
CO2_gkm=Res.CO2_gkm;
ibat_eff=Res.ibat_eff;
ibat_moy=Res.ibat_moy;

% Ajout des grandeurs synthétiques dans une nouvelle table de la structure xml
[ResXml]=miseEnForme4GestionEnergie(ResXml);

if param.verbose>=1
    % Quelques resultats
    fprintf(1,'%s %i \n','Le nombre de demarrage est de : ',sum((diff(elhyb)>0.5)));
    fprintf(1,'%s %.1f %s\n','Le rendement moyen du groupe est de : ', mean(rendement_ge(~isnan(rendement_ge)))*100,'%');
    fprintf(1,'%s %.1f %s\n','La distance parcourue est de : ',distance(end),'m');
    fprintf(1,'%s %.1f %s\n','La masse du vehicule est de : ',mean(masse),'kg');

    fprintf(1,'%s %.6f %s\n','Les pertes joules dans les batteries : ',trapz(VD.CYCL.temps,Pjbat)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','Les pertes dans les machines electrique : ',VD.VEHI.nbacm1*trapz(VD.CYCL.temps,VD.VEHI.nbacm1*qacm1)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie fournie par la batterie (source parfaite) : ',trapz(VD.CYCL.temps,p0bat)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie fournie par la batterie : ',trapz(VD.CYCL.temps,ibat.*ubat)/3600,'Wh');
    fprintf(1,'%s %.3f %s\n','Soit : ',(trapz(VD.CYCL.temps,ibat.*ubat)/3600/(dist/1000))./1000,'kWh/km');
    fprintf(1,'%s %.2f %s\n','Les pertes dans le reducteur : ',trapz(VD.CYCL.temps,Pertes_red)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie consommee par les accessoires : ',trapz(VD.CYCL.temps,pacc)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie dissipee dans les freins : ',trapz(VD.CYCL.temps,cfrein_meca.*wroue)/3600,'Wh');
    fprintf(1,'%s %.2f %s\n','L''energie inertie motelec "perdue" : ',trapz(VD.CYCL.temps,VD.ACM1.J_mg.*dwacm1.*wacm1)/3600,'Wh');

    if param.verbose>=2
        figure
        clf
        if strcmpi(param.optim,'prog_dyn')
            %pb du point origine
            plot(VD.CYCL.temps(2:end),bilanP(2:end))
        else
            plot(VD.CYCL.temps,bilanP)
        end
        grid
        legend('bilan de puissance')
    end
    
    prouet=wroue.*croue;
    prouet(croue<0)=0;
    prouer=wroue.*croue;
    prouer(croue>0)=0;
    fprintf(1,'%s %.1f %s %s %.1f %s %s %.1f %s\n','L''energie fournie aux roues motrices : ',trapz(VD.CYCL.temps,Proue)/3600,'Wh',...
        'en traction',trapz(VD.CYCL.temps,prouet)/3600,'Wh','en recup',trapz(VD.CYCL.temps,prouer)/3600,'Wh');
    
    % trajectoire optimale
%     figure
%     clf
%     hold on
    
%     % trace sollicitation batterie
%     Inter=-125:25:350;
%     classe_energie(VD,VD.CYCL.temps,(ibat),3,min(ibat),max(ibat),10,0,0,1,{'titre'},1,Inter)


end


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Pas d'optimisation, tout en mode electrique
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ERR,elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]=calc_tout_elec_HSER_GE(ERR,param,vehlib,VD,pres)

soc(1)=100-VD.INIT.Dod0;
ah(1)=0;

for t=1:length(VD.CYCL.temps)
    % la batterie peut elle fournir seule la puissance demandee
    Ibmax=interp1(VD.BATT.DoD_Ibat_max,VD.BATT.Ibat_max,100-soc(t));
    [ERR,Vbat,~,~,~,~,~]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,Ibmax*VD.BATT.Nbranchepar,soc(t),ah(t));
    pbat_max=Vbat.*Ibmax*VD.BATT.Nbranchepar;
    
    if pres(t)>pbat_max
        chaine='la batterie ne peut pas fournir la puissance demandee';
        ERR=MException('BackwardModel:calc_HSER_GE',chaine);
        [elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]=backward_erreur(length(VD.CYCL.temps));
        return;
    end
    
    % Calculs batterie
    [ERR,ibat(t),ubat(t),soc(t+1),ah(t+1),u0bat(t),rbat(t)]=calc_batt(ERR,VD.BATT,param.pas_temps,pres(t),soc(t),ah(t),-1,0);
    if ~isempty(ERR)
        [elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]=backward_erreur(length(VD.CYCL.temps));
        return;
    end
end

% probleme intervalle integrale
soc=soc(1:end-1);
ah=ah(1:end-1);
pbat = ibat.*ubat;

elhyb=zeros(size(VD.CYCL.temps));
cmt=zeros(size(VD.CYCL.temps));
wmt=zeros(size(VD.CYCL.temps));
wacm2=zeros(size(VD.CYCL.temps));
cacm2=zeros(size(VD.CYCL.temps));
qacm2=zeros(size(VD.CYCL.temps));
dcarb=zeros(size(VD.CYCL.temps));

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Pas d'optimisation, le GE fonctionne a Pmax
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ERR,elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]=calc_re_max_HSER_GE(ERR,param,vehlib,VD,pres);

% Calcul de la puissance maxi du GE
pge_max=max(VD.GE.pge_opti)*ones(size(VD.CYCL.temps));
pge=pge_max;

% Puissance reseau electrique
pbat=pres-pge;

soc(1)=100-VD.INIT.Dod0;
ah(1)=0;

for t=1:length(VD.CYCL.temps)
    % La batterie accepte elle la recharge
    Ibmin=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,100-soc(t));
    [ERR,Vbat,~,~,~,~,~]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,Ibmin*VD.BATT.Nbranchepar,soc(t),ah(t));
    
    pbat_min=Vbat.*Ibmin*VD.BATT.Nbranchepar;
    if pbat(t)<pbat_min
        pbat(t)=pres(t);
        pge(t)=0;
    end
    
    % Calculs batterie
    [ERR,ibat(t),ubat(t),soc(t+1),ah(t+1),u0bat(t),rbat(t)]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat(t),soc(t),ah(t),-1,0);
    if ~isempty(ERR)
       [elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]=backward_erreur(length(VD.CYCL.temps));
        return;
    end
    
    if soc(t+1)>param.soc_max_re_max
        pbat(t)=pres(t);
        pge(t)=0;
        [ERR,ibat(t),ubat(t),soc(t+1),ah(t+1),u0bat(t),rbat(t)]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat(t),soc(t),ah(t),-1,0);
        if ~isempty(ERR)
            [elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]=backward_erreur(length(VD.CYCL.temps));
            return;
        end
    end
end

% probleme intervalle integrale
soc=soc(1:end-1);
ah=ah(1:end-1);

wacm2=interp1(VD.GE.pge_opti,VD.GE.wmt_opti,pge,'linear',max(VD.GE.wmt_opti));
qacm2=interp1(VD.GE.pge_opti,VD.GE.qacm2_opti,pge,'linear',VD.GE.qacm2_opti(end));
cacm2=-(pge+qacm2)./wacm2;
wmt=wacm2;
cmt=-cacm2;
dcarb=interp2(VD.MOTH.Cpl_2dconso,VD.MOTH.Reg_2dconso,VD.MOTH.Conso_2d,cmt,wmt,'linear',max(max(VD.MOTH.Conso_2d)));

elhyb=ones(1,length(VD.CYCL.temps));

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Optimisation de la gestion de l energie dans une vehicule hybride serie
% avec batterie 
%
% Methode des multiplicateurs de lagrange
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah,lambda]=calc_lagrange_HSER_GE(ERR,param,vehlib,VD,pres)

global max_ham;
max_ham=1e6;

% Initialisation
u0bat=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
pbat=zeros(size(VD.CYCL.temps));
rbat=zeros(size(VD.CYCL.temps));
soc=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));
elhyb=zeros(size(VD.CYCL.temps))*NaN; 
soc(1)=100-VD.INIT.Dod0;
wmt=zeros(size(VD.CYCL.temps));
cmt=zeros(size(VD.CYCL.temps));
dcarb=zeros(size(VD.CYCL.temps));
wacm2=zeros(size(VD.CYCL.temps));
cacm2=zeros(size(VD.CYCL.temps));
qacm2=zeros(size(VD.CYCL.temps));

lambda(1)=param.lambda;

fprintf(1,'%s %f\n','Valeur de lambda : ',lambda(1));

%% boucle sur le temps du cycle
for t=1:length(VD.CYCL.temps)
    if param.mode==2 % stop start autorise
        if VD.CYCL.vitesse(t)==0
            param.stopstart=1;
        else
            param.stopstart=0;
        end
    else
        param.stopstart=param.mode;
    end
    % Minimisation des possibles pour l'instant courant et la
    % valeur de lambda
    [ERR,wmt(t),cmt(t),dcarb(t),wacm2(t),cacm2(t),qacm2(t),ibat(t),ubat(t),soc(t),ah(t),u0bat(t),rbat(t),elhyb(t),hamilt,lambda]=calc_hamilt_HSER_GE(ERR,param,VD,lambda,t,pres,soc,ah);
    
    if t==param.tdebug
        fprintf(1,'%s %d \n','hamiltonien min :',hamilt);
        fprintf(1,'%s %d \n','puissance mth correspondante :',wmt(t)*cmt(t));
    end
    
    %         pacm2(t)=wacm2(t).*cacm2(t);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %         pbat(t)=pres(t)+pacm2(t)+qacm2(t);
    
    if ~isempty(ERR)
        ERR
        [elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah,lambda]=backward_erreur(length(VD.CYCL.temps));
        return;
    end
    
    if param.verbose>1
        fprintf(1,'%s %.1f %s %.2f  %s %.2f %s %.2f   %s %.2f %s %.2f\n','calc_HSER_GE - Temps: ',t,'Pres:',pres(t),'wmt',wmt(t), ...
            'cmt',cmt(t),'Ibat',ibat(t),'Ubat',ubat(t)./VD.BATT.Nblocser);
    end
    
end % fin de la boucle sur le temps du cycle

pacm2=wacm2.*cacm2;
pbat=pres+pacm2+qacm2;

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Calcul du vehicule hybride serie avec groupe electrogene
% Methode de la programmation dynamique
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function[ERR,elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]=calc_prog_dyn_HSER_GE(ERR,param,vehlib,VD,pres)

% initialisation
cout=zeros(size(VD.CYCL.temps));
wacm2=zeros(size(VD.CYCL.temps));
cacm2=zeros(size(VD.CYCL.temps));
qacm2=zeros(size(VD.CYCL.temps));
dcarb=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
rbat=zeros(size(VD.CYCL.temps));
u0bat=zeros(size(VD.CYCL.temps));
RdF=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));
pppbat=zeros(size(VD.CYCL.temps));
pge_calc=zeros(size(VD.CYCL.temps));


% Calcul des limites du graphe
[ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=calc_limites_HSER_GE(ERR,param,VD,pres);
if ~isempty(ERR)
    ERR.message
    [elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]=backward_erreur(length(VD.CYCL.temps));
    return;
end

% verification de la precision du graph pour forcer le nombre
% d'arc sur un pas de temps a une valeur min (sinon peut devenir 0)
if isfield(param,'Nb_arc_min') & param.Nb_arc_min~=0 & abs(Dsoc_min(end)-Dsoc_max(end))~=0
    %if (min(abs(Dsoc_min(2:end)-Dsoc_max(2:end)))/param.Nb_arc_min < -param.prec_graphe) | (param.prec_graphe==0) | ~isfield(param,'prec_graphe')
    if (abs(Dsoc_min(end)-Dsoc_max(end))/param.Nb_arc_min < -param.prec_graphe) | (param.prec_graphe==0) | ~isfield(param,'prec_graphe')
        val_prec=-abs(Dsoc_min(end)-Dsoc_max(end))/param.Nb_arc_min;
        param.prec_graphe=val_prec;
        assignin('base','val_prec',val_prec);
        evalin('base','param.prec_graphe=val_prec;');
        if isfield(param,'prec_graphe') & param.prec_graphe~=0
              chaine='Graph precision automatically reduce by param.Nb_arc_min \ngraph precision : %f' ;
              warning('BackwardModel:calc_HSER_GE',chaine,param.prec_graphe);
        end
    end
end

[ERR,lim_inf,lim_sup,lim_elec,soc_inf,soc_sup,Dsoc_inf,Dsoc_sup] =limite2indice_soc(VD,soc_min,soc_max,param,0,Dsoc_min,Dsoc_max );
if ~isempty(ERR)
    ERR.message
    [elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]=backward_erreur(length(VD.CYCL.temps));
    return;
end

% [ERR,lim_inf,lim_sup,lim_elec,soc_inf,soc_sup] =limite2indice_soc(VD,soc_min,soc_max,param);


% Information sur le graphe
if param.verbose>=1
    Nb_arc=ceil((Dsoc_min-Dsoc_max)/param.prec_graphe);
    fprintf(1,'%s%d\n','edges on the first spread : ',Nb_arc(2));
    fprintf(1,'%s%d\n','maximum edge number : ',max(Nb_arc));
    fprintf(1,'%s%d\n','mean edge number : ',mean(Nb_arc(2:end-1)));
    fprintf(1,'%s%d\n','edges on the latest spreads : ',Nb_arc(end-1));
    fprintf(1,'%s%d\n','X size of the graph : ',length(VD.CYCL.temps));
    fprintf(1,'%s%d\n','Y size of the graph : ',max(lim_sup-lim_inf));
    fprintf(1,'%s%d\n','Maximum size of the cost matrix : ',max(Nb_arc));
end


var_com_inf=Dsoc_min;
var_com_sup=Dsoc_max;

% si le mode electrique est possible elhyb est mis a zero
elhyb=ones(size(VD.CYCL.temps))-elec_pos;

if isfield(param,'calc_mat') & param.calc_mat==1
    [ERR,poids_min,meilleur_chemin]= ...
        solve_graph_mat(ERR,@(ERR,Dsoc) calc_cout_arc_HSER_GE_mat(ERR,param,vehlib,VD,pres,Dsoc,100-VD.INIT.Dod0,elhyb),...
        param,lim_sup,lim_inf,var_com_inf,var_com_sup,0,lim_elec);
else
    [ERR,poids_min,meilleur_chemin]= ...
        solve_graph_mat(ERR,@(ERR,ii,Dsoc) calc_cout_arc_HSER_GE(ERR,param,vehlib,VD,pres,Dsoc,100-VD.INIT.Dod0,ii,elhyb),...
        param,lim_sup,lim_inf,var_com_inf,var_com_sup,0,lim_elec);
end

if ~isempty(ERR)
    ERR
    [elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]=backward_erreur(length(VD.CYCL.temps));
    return;
end

% passage chemin aux var de commande
if isfield(param,'calc_mat') & param.calc_mat==1
    [ERR,Dsoc1,elhyb]=chemin2varcomBATT_mat(ERR,@(ERR,Dsoc) calc_cout_arc_HSER_GE_mat(ERR,param,vehlib,VD,pres,Dsoc,100-VD.INIT.Dod0,elhyb),...
        param,lim_sup,lim_inf,lim_elec,var_com_inf,var_com_sup,meilleur_chemin(1,:),elec_pos);
else
    [ERR,Dsoc1,elhyb]=chemin2varcomBATT(ERR,@(ERR,ii,Dsoc) calc_cout_arc_HSER_GE(ERR,param,vehlib,VD,pres,Dsoc,100-VD.INIT.Dod0,ii,elhyb),...
        param,lim_sup,lim_inf,lim_elec,var_com_inf,var_com_sup,meilleur_chemin(1,:),elec_pos);
end

% Recalcul de toutes les grandeurs du vehicule.
if isfield(param,'calc_mat') & param.calc_mat==1
    [ERR,cout,wacm2,cacm2,qacm2,dcarb,ibat,ubat,rbat,u0bat,RdF,~,ah]=...
        calc_cout_arc_HSER_GE_mat(ERR,param,vehlib,VD,pres,Dsoc1,100-VD.INIT.Dod0,elhyb);
else
    for i=2:1:length(VD.CYCL.temps)
        [ERR,cout(i),wacm2(i),cacm2(i),qacm2(i),dcarb(i),ibat(i),ubat(i),rbat(i),u0bat(i),RdF(i),~,ah(i)]=...
            calc_cout_arc_HSER_GE(ERR,param,vehlib,VD,pres,Dsoc1(i),100-VD.INIT.Dod0,i,elhyb);
        
        % calcul batterie
        % calcul ibat ?? partir de Dsoc
        RdF=ones(size(Dsoc1(i)));
        RdF(Dsoc1(i)>0)=VD.BATT.RdFarad;
        iibat=-Dsoc1(i)./RdF*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100/param.pas_temps;
        %
        [ERR,uubat,~,aah,EE,RR,RdF]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,iibat,100-VD.INIT.Dod0,0);
        
        ppbat=uubat.*iibat;
        pppbat(i)=ppbat;
        pge_calc(i)=pres(i)-ppbat;
        if i==VD.CYCL.temps(end)
            assignin('base','pbat_calc',pppbat);
            assignin('base','pge_calc',pge_calc);
        end
    end
end
ubat(1)=ubat(2); %on remet ubat(1) a ubat(2) sinon on a zero

wmt=wacm2;
cmt=-cacm2;

pbat=ubat.*ibat;

% Ne pas calculer les soc par trapz mais par des sommes.
soc1(1)=100-VD.INIT.Dod0;
for i=2:1:length(Dsoc1)
    soc1(i)=soc1(i-1)+Dsoc1(i);
end
soc=soc1;

if param.verbose >= 2
    figure(200)
    clf
    hold on
    plot(VD.CYCL.temps,ibat,VD.CYCL.temps,ubat,VD.CYCL.temps,dcarb)
    legend('ibat','ubat','dcarb')
    
    figure(3)
    clf
    hold on
    plot(VD.CYCL.temps+1,meilleur_chemin,VD.CYCL.temps+1,lim_inf,VD.CYCL.temps+1,lim_sup,VD.CYCL.temps+1,VD.CYCL.vitesse*36)
    title('meilleur chemin en indice')
    grid
    
    figure(4)
    clf
    hold on
    subplot(2,1,1)
    plot(VD.CYCL.temps,soc_inf,'k',VD.CYCL.temps,soc_sup,'k',VD.CYCL.temps,soc1,'b')%,VD.CYCL.temps,soc2,'g')
    subplot(2,1,2)
    plot(VD.CYCL.temps,VD.CYCL.vitesse*3.6,'c')
    if isfield(param,'tout_thermique') & param.tout_thermique==1;
        plot(VD.CYCL.temps,soc1_tt,'b--')
    end
    
    title('meilleur chemin en soc');
    grid
    
    figure(5)
    clf
    plot(VD.CYCL.temps,ibat_max,VD.CYCL.temps,ibat_min,VD.CYCL.temps,ibat,VD.CYCL.temps,VD.CYCL.vitesse*3.6)
    grid
    ylabel('courant en A')
    legend('ibat max','ibat min','ibat','vitesse')
end

assignin('base','Dsoc1',Dsoc1);
assignin('base','RdF',RdF);
% assignin('base','ibat_max',ibat_max);
% assignin('base','ibat_min',ibat_min);
assignin('caller','ibat_max',ibat_max);
assignin('caller','ibat_min',ibat_min);
% 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Optimisation de la gestion de l energie dans une vehicule hybride serie
% avec batterie 
%
% Methode en ligne
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ERR,elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]= calc_ligne_HSER_GE(ERR,param,vehlib,VD,pres);

ERR=[];

% Initialisation
pge_cible=zeros(size(VD.CYCL.temps));
soc=zeros(size(VD.CYCL.temps));
ah=zeros(size(VD.CYCL.temps));
elhyb=zeros(size(VD.CYCL.temps));
pfiltre=zeros(size(VD.CYCL.temps));
wmt=zeros(size(VD.CYCL.temps));
cmt=zeros(size(VD.CYCL.temps));
dcarb=zeros(size(VD.CYCL.temps));
wacm2=zeros(size(VD.CYCL.temps));
cacm2=zeros(size(VD.CYCL.temps));
qacm2=zeros(size(VD.CYCL.temps));
pacm2=zeros(size(VD.CYCL.temps));
pbat=zeros(size(VD.CYCL.temps));
ibat=zeros(size(VD.CYCL.temps));
ubat=zeros(size(VD.CYCL.temps));
u0bat=zeros(size(VD.CYCL.temps));
rbat=zeros(size(VD.CYCL.temps));
lambda=zeros(size(VD.CYCL.temps));
cout_elec=zeros(size(VD.CYCL.temps));
cout_hybr=zeros(size(VD.CYCL.temps));

% Soc initial
soc(1)=100-VD.INIT.Dod0;

fact_int=0;

for ind=1:length(VD.CYCL.temps)
    
    if ind>1
        soc_p=soc(ind-1);
        ah_p=ah(ind-1);
    else
        soc_p=soc(1);
        ah_p=0;
    end
    %        Ibat=-10;
    Ibat=VD.BATT.Nbranchepar.*interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,100-soc(ind));
    [ERR,~,~,~,~,~,~,~,~,Ibat_min_calc,Ubat_min_calc]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,Ibat,soc_p,ah_p);
    Pbat_min_calc=Ibat_min_calc.*Ubat_min_calc+1;
    %        Ibat=10;
    Ibat=VD.BATT.Nbranchepar.*interp1(VD.BATT.DoD_Ibat_max,VD.BATT.Ibat_max,100-soc(ind));
    [ERR,~,~,~,~,~,~,Ibat_max_calc,Ubat_max_calc,~,~]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,Ibat,soc_p,ah_p);
    Pbat_max_calc=Ibat_max_calc.*Ubat_max_calc-1;
    if pres(ind)<Pbat_min_calc
        % il faut mettre des freins mecaniques
        pres(ind)=Pbat_min_calc;
    end
    if pres(ind)>Pbat_max_calc+VD.GE.pge_opti(end)
        chaine='Le vehicule ne peut pas satisfaire les conditions dynaliques demandees';
        ERR=MException('BackwardModel:calc_HSER_GE',chaine);
        [wmt,cmt,dcarb,wacm2,cacm2,qacm2,ibat,ubat,soc,ah,u0bat,rbat,elhyb]=backward_erreur;
        return;
    end
    
    % Implentation strategie en ligne
    if param.strat==0
        if param.mode_elect==0
            pge_cible(ind)=pres(ind);
            elhyb(ind)=1;
        elseif param.mode_elect==1
            if pres(ind)>param.pmin_ge
                pge_cible(ind)=pres(ind);
                elhyb(ind)=1;
            else
                pge_cible(ind)=0;
            end
        elseif param.mode_elect==2
            if VD.CYCL.vitesse(ind)~=0
                if ind>1
                    fact_int=fact_int+(soc(ind-1)-param.soc_cible)/param.pas_temps;
                    pdem=pres(ind)*(1-param.kp*(soc(ind-1)-param.soc_cible)-param.ki*fact_int);
                else
                    pdem=pres(1);
                end
                pge_cible(ind)=pdem;
                elhyb(ind)=1;
            else
                pge_cible(ind)=0;
            end
        end
    elseif param.strat==1
        % Strategie valeur moyenne
        if param.mode_elect==0
            [ERR,pge_cible(ind)]=moyenne_glissante(ERR,VD,param,pres,ind);
            elhyb(ind)=1;
        end
    elseif param.strat==2
        % Strategie filtre premier ordre
        if ind~=1
            [ERR,pfiltre]=filtre_ordre1(ERR,VD,param,pres,pfiltre,ind);
        else
            pfiltre(1)=param.pini;
        end
        pdem=pfiltre(ind);
%         pfiltre(ind)=param.pini+param.kp*(pfiltre(ind)-param.pini);
        if param.mode_elect==0
                if ind>1
                    fact_int=fact_int+(soc(ind-1)-param.soc_cible)/param.pas_temps;
                    pdem=pfiltre(ind)*(1-param.kp*(soc(ind-1)-param.soc_cible)-param.ki*fact_int);
                else
                    pdem=pfiltre(1);
                end
                elhyb(ind)=1;
        elseif param.mode_elect==2
            if VD.CYCL.vitesse(ind)~=0
                if ind>1
                    fact_int=fact_int+(soc(ind-1)-param.soc_cible)/param.pas_temps;
                    pdem=pfiltre(ind)*(1-param.kp*(soc(ind-1)-param.soc_cible)-param.ki*fact_int);
                else
                    pdem=pfiltre(1);
                end
                elhyb(ind)=1;
            else
                pdem=0;
            end
        end
        pge_cible(ind)=max(pdem,param.pmin_ge);
    elseif param.strat==3
        % Strategie resolution hamiltonien
        % calcul de la puissance cible
        if ind==1
            lambda(ind)=VD.ECU.lambda_ini;
        else
            fact_int=fact_int+(soc(ind-1)-param.soc_cible)/param.pas_temps;
            lambda(ind)=lambda(ind-1)*(1-param.kp*(soc(ind-1)-param.soc_cible)-param.ki*fact_int);
            %         lambda(ind)=param.lambda_init+param.kp*(param.soc_cible-soc(ind-1));
        end
        
        pge_cible(ind)=(lambda(ind)-VD.ECU.b)./(2*VD.ECU.a);
        pge_cible(ind)=min(pge_cible(ind),max(VD.GE.pge_opti));
        pge_cible(ind)=max(pge_cible(ind),min(VD.GE.pge_opti));
        wmt_cible=interp1(VD.GE.pge_opti,VD.GE.wmt_opti,pge_cible(ind));
        qacm2_cible=interp1(VD.GE.pge_opti,VD.GE.qacm2_opti,pge_cible(ind));
        cmt_cible=(pge_cible(ind)+qacm2_cible)./wmt_cible;
        dcarb_cible=interp2(VD.MOTH.Cpl_2dconso,VD.MOTH.Reg_2dconso,VD.MOTH.Conso_2d,cmt_cible,wmt_cible);
%         dcarb_cible=(pge_cible(ind).*pge_cible(ind).*VD.ECU.a+pge_cible(ind).*VD.ECU.b+VD.ECU.c)./VD.MOTH.pci;
        
        if param.mode_elect==0
            % mode tout elec interdit
            elhyb(ind)=1;
        elseif param.mode_elect==1
            % mode tout elec autorise
            % cout electrique
            pbat_e=pres(ind);
            ERR=[];
            if ind==1
                [ERR,ibat_e,ubat_e,soc_e,ah_e,u0bat_e,rbat_e,~]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_e,soc(1),ah(1),pres(1),0);
            else
                [ERR,ibat_e,ubat_e,soc_e,ah_e,u0bat_e,rbat_e,~]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_e,soc(ind-1),ah(ind-1),pres(ind),0);
            end
            if isempty(ERR)
                % le mode tout electrique est possible
                cout_elec(ind)=ham_fonct(lambda(ind),0,ibat_e,soc_e);
            else
                cout_elec(ind)=inf;
            end
            
            %cout hybride
            pbat_h=pres(ind)-pge_cible(ind);
            ERR=[];
            if ind==1
                [ERR,ibat_h,ubat_h,soc_h,ah_h,u0bat_h,rbat_h,~]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_h,soc(1),ah(1),pres(1),pge_cible(ind));
            else
                [ERR,ibat_h,ubat_h,soc_h,ah_h,u0bat_h,rbat_h,~]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_h,soc(ind-1),ah(ind-1),pres(ind),pge_cible(ind));
            end
            if isempty(ERR)
                % pge_cible(ind) est possible
                cout_hybr(ind)=ham_fonct(lambda(ind),dcarb_cible,ibat_h,soc_h);
            else
                % pge cible impossible
                if pres(ind)<pge_cible(ind)
                    cout_hybr(ind)=inf;
                elseif pres(ind)>pge_cible(ind)
                    cout_hybr(ind)=-inf;
                end
            end
            ERR=[];
            if cout_elec(ind)<=cout_hybr(ind)
                elhyb(ind)=0;
            else
                elhyb(ind)=1;
            end
            
        elseif param.mode_elect==2
            % mode stop start
            if VD.CYCL.vitesse(ind)~=0
                elhyb(ind)=1;
            else
                elhyb(ind)=0;
            end
        end
    end % fin de boucle sur calcul de pge_cible en ligne
    
    % Verification: il faut que la batterie puisse accepter cette puissance
    % Et qu'elle soit dans les bornes mini et maxi de ce que peut faire le GE
    if elhyb(ind)==1
        
        pge_cible(ind)=min(pge_cible(ind),(-1)*(Pbat_min_calc-pres(ind)));
        pge_cible(ind)=max(pge_cible(ind),(-1)*(Pbat_max_calc-pres(ind)));
        pge_cible(ind)=min(max(pge_cible(ind),VD.GE.pge_opti(1)),VD.GE.pge_opti(end));
        wmt(ind)=interp1(VD.GE.pge_opti,VD.GE.wmt_opti,pge_cible(ind));
        qacm2(ind)=interp1(VD.GE.pge_opti,VD.GE.qacm2_opti,pge_cible(ind));
        cmt(ind)=(pge_cible(ind)+qacm2(ind))./wmt(ind);
        dcarb(ind)=interp2(VD.MOTH.Cpl_2dconso,VD.MOTH.Reg_2dconso,VD.MOTH.Conso_2d,cmt(ind),wmt(ind));
%         dcarb(ind)=(pge_cible(ind)^2*VD.ECU.a+pge_cible(ind)*VD.ECU.b+VD.ECU.c)/VD.MOTH.pci;
    else
        pge_cible(ind)=0;
        wmt(ind)=0;
        qacm2(ind)=0;
        cmt(ind)=0;
        dcarb(ind)=0;
    end
    % Calcul des conditions en amont de la generatrice electrique
    wacm2(ind)=wmt(ind);
    cacm2(ind)=-cmt(ind);
    pacm2(ind)=wacm2(ind).*cacm2(ind);
    % Ici, on ne peut pas s assurer que la Me satisfait les conditions
    % dynamiques demandees (on ne connait pas sa tension d'alimentation).
    %    [ERR,qacm2(ind)]=calc_pertes_acm(ERR,VD.ACM2,(-1)*cacm2(ind),wacm2(ind),0);
    %    if ~isempty(ERR)
    %        ERR
    %        [wmt,cmt,dcarb,wacm2,cacm2,qacm2,ibat,ubat,soc,ah,u0bat,rbat,elhyb]=backward_erreur;
    %        return;
    %    end
    %    % Probleme de precision numerique autour de pge=0
    %    if pge_cible(ind)<1e-10
    %        qacm2(ind)=-pacm2(ind);
    %    end
    
    % Conditions de fonctionnement batterie
    %pbat(ind)=pres(ind)+pacm2(ind)+qacm2(ind);
    % Pour des problemes de precision numerique (les pertes ne sont pas lin?aires entre 2 points de VD.GE.pgmt_opti)
    % le bilan est fait avec pge_cible et non pacm2(ind)+qacm2(ind)
    
    pbat(ind)=pres(ind)-pge_cible(ind);
    
    % Calculs batterie
    % Il peut se trouver des cas ou, lorsque le dimensionnement est juste et
    % que l'on decharge la batterie sur le cycle, que calc_batt ne trouve plus
    % de solution, alors que le test realise dans calc_HSER_GE
    % (cf tester si le groupe et la batterie peuvent fournir la puissance demandee)
    % soit passe !
    if ind==1
        [ERR,ibat(1),ubat(1),soc(1),ah(1),u0bat(1),rbat(1),~]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat(1),soc(1),ah(1),pres(1),pacm2(1)+qacm2(1));
    else
        [ERR,ibat(ind),ubat(ind),soc(ind),ah(ind),u0bat(ind),rbat(ind),~]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat(ind),soc(ind-1),ah(ind-1),pres(ind),pacm2(ind)+qacm2(ind));
    end
    
    if ~isempty(ERR)
        ind
        soc(ind-1)
        pge_erreur=pge_cible(ind)
        pres_erreur=pres(ind)
        pbat_erreur=pbat(ind)
        pause
        [wmt,cmt,dcarb,wacm2,cacm2,qacm2,ibat,ubat,soc,ah,u0bat,rbat,elhyb]=backward_erreur;
        ERR
        return;
    end
    pbat=ibat.*ubat;
    
    ibat(isnan(pbat))=NaN;
    
end

if ~isempty(ERR)
    ERR
    [elhyb,wmt,cmt,dcarb,wacm2,cacm2,qacm2,u0bat,ibat,ubat,pbat,rbat,soc,ah]=backward_erreur(length(VD.CYCL.temps));
    return;
end

if param.strat==3
    
    assignin('base','lambda',lambda);
    assignin('base','pge_cible',pge_cible);
    assignin('base','cout_elec',cout_elec);
    assignin('base','cout_hybr',cout_hybr);
elseif param.strat==1
    assignin('base','pge_cible',pge_cible);
    assignin('base','pres',pres);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fonction a minimiser
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function [ham]=ham_fonct(lambda,dcarb,ibat,soc)
        % Minimisation de la consommation du groupe
        if ibat>=0
            RdF=1;
        else
            RdF=VD.BATT.RdFarad;
        end
        E=interp1(VD.BATT.dod_ocv,VD.BATT.ocv(1,:),100-soc,'linear','extrap')*VD.BATT.Nblocser;
        
        ham=VD.MOTH.pci*dcarb+lambda*RdF.*ibat.*E;
        
    end
end
