function [ERR,cout,wacm2,cacm2,qacm2,dcarb,ibat,ubat,R,E,RdF,Dsoc,ah]=calc_cout_arc_HSER_GE(ERR,param,vehlib,VD,pres,Dsoc,soc_p,i,elhyb)

[Li,Co]=size(soc_p);
if sum(sum(isnan(soc_p)))==Li*Co
    ERR=MException('BackwardModel:calc_arc', 'appel a calc_arc_HSER_GE : tout les elements d''un des vecteur precedent a NaN');
    return;
end

% calcul batterie
% calcul ibat a partir de Dsoc
RdF=ones(size(Dsoc));
RdF(Dsoc>0)=VD.BATT.RdFarad;
ibat=-Dsoc./RdF*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100/param.pas_temps;

[ERR,ubat,~,ah,E,R,RdF]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat,soc_p,0);

pbat=ubat.*ibat;
pge=pres(i)-pbat;


if isnan(pbat(1)) && isfield(param,'modulo_prec_graph') && param.modulo_prec_graph==1
    ibat(1)=ibat(1)-eps(ibat(1));
    [ERR,ubat]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat,soc_p,0);
    pbat=ubat.*ibat;
    pge=pres(i)-pbat;
    if isnan(pbat(1))
        chaine=strcat('Erreur dans le calcul de l arc tout electrique a l instant :',num2str(i));
        ERR=MException('BackwardModel:calc_cout_arc_HSER_GE',chaine);
        [cout,wacm2,cacm2,qacm2,dcarb,ibat,ubat,R,E,RdF,Dsoc,ah]=backward_erreur(length(Dsoc));
        return;
    end
end

indice=find(pge<0 & pge>param.pge_satur);
if ~isempty(indice)
    pge(indice)=0;
end

wacm2=interp1(VD.GE.pge_opti,VD.GE.wmt_opti,pge,'linear',max(VD.GE.wmt_opti));
qacm2=interp1(VD.GE.pge_opti,VD.GE.qacm2_opti,pge,'linear',VD.GE.qacm2_opti(end));
cacm2=-(pge+qacm2)./wacm2;

dcarb=interp2(VD.MOTH.Cpl_2dconso,VD.MOTH.Reg_2dconso,VD.MOTH.Conso_2d,-cacm2,wacm2,'linear',max(max(VD.MOTH.Conso_2d)));
 

% if ~isempty(ERR)
%     dcarb=NaN;
%     chaine=strcat('Erreur dans le calcul des pertes acm2 au temps :',num2str(i));
%     ERR=MException('BackwardModel:calc_cout_arc_HSER_GE',chaine);
%     [cout,wacm2,cacm2,qacm2,dcarb,ibat,ubat,R,E,RdF,Dsoc,ah]=backward_erreur(length(Dsoc));
%     return;
% end

if ~isfield(param,'modulo_prec_graph') || param.modulo_prec_graph==0
    % Calculs batterie il faut supprimer les arcs impossible (recharge maximum
    % a priori !)
    % Jusqu'ici ibat est calcule a partir de Dsoc.
    [ERR,Ibat,~,~,ah,E,R]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat,soc_p,0,0,0);
    if isnan(Ibat(1))
        % A priori, si Ibat(1) est NaN, c est un probleme de precision du
        % graphe ...
        if param.verbose>=1
            warning(strcat('calc_cout_arc_HSER_GE: Ibat(1) est NaN a l instant :',num2str(i)));
        end
        Ibat(1)=ibat(1)-eps(ibat(1));
        ERR=[ ];
        [ERR,ubat]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,Ibat,soc_p,0);
        pbat=ubat.*Ibat;
        pge=pres(i)-pbat;
        if isnan(pbat(1))
            ubat
            Ibat
            chaine=strcat('Erreur dans le calcul de l arc tout electrique a l instant :',num2str(i));
            ERR=MException('BackwardModel:calc_cout_arc_HSER_GE',chaine);
            [cout,wacm2,cacm2,qacm2,dcarb,ibat,ubat,R,E,RdF,Dsoc,ah]=backward_erreur(length(Dsoc));
            return;
        end
        wacm2=interp1(VD.GE.pge_opti,VD.GE.wmt_opti,pge,'linear',max(VD.GE.wmt_opti));
        qacm2=interp1(VD.GE.pge_opti,VD.GE.qacm2_opti,pge,'linear',VD.GE.qacm2_opti(end));
        cacm2=-(pge+qacm2)./wacm2;
        dcarb=interp2(VD.MOTH.Cpl_2dconso,VD.MOTH.Reg_2dconso,VD.MOTH.Conso_2d,-cacm2,wacm2,'linear',max(max(VD.MOTH.Conso_2d)));

    end
    
    % on supprime les arcs impossibles
    dcarb(isnan(Ibat))=inf;
end

%%%%%% MODE TOUT ELECTRIQUE %%%%%%%%%%%
% lors de la recherche du chemin le premier arc represente l'arc tout
% electrique
% lors du recalcul il n'y a qu'un arc a chaque fois
if elhyb(i)==0
    wacm2(1)=0;
    cacm2(1)=0;
    dcarb(1)=0;
    qacm2(1)=0;   
end

% Critere de cout des arcs
cout=dcarb*param.pas_temps;


% ZEV : mode elec obligatoire
% On met les cout de tout les arcs non elec a l'infini lorsque l'on se
% trouve dans un intervalle de temps ou l'on veut imposer le mode electrique
if isfield(param,'t_zev')
    if ~isempty(param.t_zev)
        for c = 1:size(param.t_zev,1)
            if i>=param.t_zev(c,1) && i<=param.t_zev(c,2)
                cout(1:end) = Inf;
                cout(1) = 0;
            end
        end
    end
end

if~isempty(ERR)
    ERR=MException('BackwardModel:calc_cout_arc', 'tous les arcs sont impossibles, les caracteristiques du systeme ne permettent pas de satisfaire les conditions demandees !');
    %return;
end

if sum(isnan(cout))
    chaine='some edge cost are NaN, to avoid problems in solve graph, prefer to fixe them to Inf \n time : %d' ;
    warning('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine,i);
end

if sum(isinf(cout))==length(cout)
    chaine=' all edge are impossible, the systems possibilites does not allow to fullfil the required systems operating point ! \n time : %d \n Dsoc : %s  \n cost : %s \n ibat: %s \n ubat: %s ';
    ERR=MException('BackwardModel:calc_cout_arc_HP_BV_2EMB',chaine,i,num2str(Dsoc),num2str(cout),num2str(ibat),num2str(ubat));
    if ~isfield(param,'dim') | param.dim==0 % Dans le cas d'un processus de dimensionnement param.dim==1 on ne sort pas en "vrac" on renvoi seulement l'erreur
        return
    end
end

return
