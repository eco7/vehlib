%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Minimisation de la courbe des courants BAT/PAC admissibles pour la valeur
% de lambda
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ERR,wmt_cible,cmt_cible,dcarb_cible,wacm2_cible,cacm2_cible,qacm2_cible,ibat_cible,ubat_cible,soc_cible,ah_cible,u0bat_cible,rbat_cible,elhyb,hamilt,lambda]=calc_hamilt_HSER_GE(ERR,param,VD,lambda,t,pres,soc,ah)

%#eml
eml.extrinsic('fprintf')

% wmt_s=VD.GE.wmt_opti;
% cmt_s=VD.GE.cmt_opti;
% dcarb_s=interp2(VD.MOTH.Cpl_2dconso,VD.MOTH.Reg_2dconso,VD.MOTH.Conso_2d,cmt_s,wmt_s);

pge_s=(min(VD.GE.pge_opti):(max(VD.GE.pge_opti)-min(VD.GE.pge_opti))/20 :max(VD.GE.pge_opti));
wmt_s=interp1(VD.GE.pge_opti,VD.GE.wmt_opti,pge_s);
qacm2_s=interp1(VD.GE.pge_opti,VD.GE.qacm2_opti,pge_s);

cmt_s=(pge_s+qacm2_s)./wmt_s;


dcarb_s=interp2(VD.MOTH.Cpl_2dconso,VD.MOTH.Reg_2dconso,VD.MOTH.Conso_2d,cmt_s,wmt_s);

% Calcul des conditions en amont de la generatrice electrique
wacm2_s=wmt_s;
cacm2_s=-cmt_s;
pacm2_s=wacm2_s.*cacm2_s;
% Ici, on ne peut pas s assurer que la Me satisfait les conditions
% dynamiques demandees (on ne connait pas sa tension d'alimentation).

% Conditions de fonctionnement batterie

pbat_s=pres(t)+pacm2_s+qacm2_s;
% pbat_s=pres(t)-pge_s;
% assignin('base','pbat_s',pbat_s);


% Calculs batterie
% Il peut se trouver des cas ou, lorsque le dimensionnement est juste et
% que l'on decharge la batterie sur le cycle, que calc_batt ne trouve plus
% de solution, alors que le test realise dans calc_HSER_GE 
% (cf tester si le groupe et la batterie peuvent fournir la puissance demandee)
% soit passe !

if t==1
    [ERR,ibat_s,ubat_s,soc_s,ah_s,u0bat_s,rbat_s,~]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_s,soc(1),ah(1),pres(t),pacm2_s+qacm2_s);
else
    [ERR,ibat_s,ubat_s,soc_s,ah_s,u0bat_s,rbat_s,~]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_s,soc(t-1),ah(t-1),pres(t),pacm2_s+qacm2_s);
end

if ~isempty(ERR)
    [wmt_cible,cmt_cible,dcarb_cible,wacm2_cible,cacm2_cible,qacm2_cible,ibat_cible,ubat_cible,soc_cible,ah_cible,u0bat_cible,rbat_cible,elhyb,hamilt,lambda]=backward_erreur;
    ERR
    return;
end
pbat_s=ibat_s.*ubat_s;

ibat_s(isnan(pbat_s))=NaN;

lambda_s=ones(size(wmt_s)).*lambda(1);

% Appel de la fonction ad hoc suivant critere
ham=ham_fonct(VD,lambda_s,dcarb_s,ibat_s,soc_s); 

% Recherche du minimum de la courbe
indice=find(ham==min(ham));

if param.stopstart==1
    % Mode tout electrique autorise
    % Conditions de fonctionnement batterie
    pbat_selec=pres(t);
    
    % Calculs batterie
    if t==1
        [ERR,ibat_selec,ubat_selec,soc_selec,ah_selec,u0bat_selec,rbat_selec,~]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_selec,soc(1),ah(1),pres(t),0);
    else
        [ERR,ibat_selec,ubat_selec,soc_selec,ah_selec,u0bat_selec,rbat_selec,~]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_selec,soc(t-1),ah(t-1),pres(t),0);
    end
    
    if isempty(ERR)
        % le mode tout electrique est possible
        pbat_selec=ibat_selec.*ubat_selec;    
        ibat_selec(isnan(pbat_selec))=NaN;
        % Appel de la fonction ad hoc suivant critere
        ham_elec=ham_fonct(VD,lambda(1),0,ibat_selec,soc_selec);
    else
        % le mode tout elec n'est pas possible
        ham_elec=inf;
    end
    ERR=[];
else
    % le mode tout elec n'est pas possible 
    ham_elec=inf;
end

if t==param.tdebug
    figure;
    plot(pacm2_s+qacm2_s,ham,pacm2_s(indice)+qacm2_s(indice),ham(indice),'*')
    grid on
    xlabel('- puissance groupe')
    ylabel('hamiltonien')
    pause
end

% Affectation des grandeurs avant sortie

if ham_elec>min(ham)
    % le mode electrique coute plus cher
    wmt_cible=wmt_s(indice(1));
    cmt_cible=cmt_s(indice(1));
    wacm2_cible=wacm2_s(indice(1));
    cacm2_cible=cacm2_s(indice(1));
    qacm2_cible=qacm2_s(indice(1));
    dcarb_cible=dcarb_s(indice(1));
    ibat_cible=ibat_s(indice(1));
    ubat_cible=ubat_s(indice(1));
    soc_cible=soc_s(indice(1));
    ah_cible=ah_s(indice(1));
    u0bat_cible=u0bat_s;
    rbat_cible=rbat_s(indice(1));
    elhyb=1;
    lambda(t)=lambda_s(indice(1));
    hamilt=ham(indice(1));
else
    wmt_cible=0;
    cmt_cible=0;
    wacm2_cible=0;
    cacm2_cible=0;
    qacm2_cible=0;
    dcarb_cible=0;
    ibat_cible=ibat_selec;
    ubat_cible=ubat_selec;
    soc_cible=soc_selec;
    ah_cible=ah_selec;
    u0bat_cible=u0bat_selec;
    rbat_cible=rbat_selec;
    elhyb=0;
    lambda(t)=lambda(1);
    hamilt=ham_elec;
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fonction a minimiser
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ham]=ham_fonct(VD,lambda,dcarb,ibat,soc)
% Minimisation de la consommation du groupe
if ibat>=0
    RdF=1;
else
    RdF=VD.BATT.RdFarad;
end
E=interp1(VD.BATT.dod_ocv,VD.BATT.ocv(1,:),100-soc,'linear','extrap')*VD.BATT.Nblocser;
% E=interp1(VD.BATT.dod_ocv,VD.BATT.ocv(1,:),100-soc,'linear')*VD.BATT.Nblocser;

ham=VD.MOTH.pci*dcarb+lambda*RdF.*ibat.*E;

return;

