% function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,cacm1_elec,qacm1_elec,cprim_red,cfrott_acm1]=  calc_limites_HSP_1EMB(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Calcul des limites du graphes.
% On ne calcule ici que les soc min et max sans se preocuper
% de la remonte du graphe ou de soc <0 ou >100%.
% La limite min doit ??tre calcul?? avec pr??cision (mode tout ??lec)
% la mlimite max peut ??tre calcule "grossi??rment" d'eventuel arc impossible
% dans le graphe seront elimine par la suite
%
% interface entr??e :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discreetisation etc)
% CYLC,VD.ADCPL...:structures de description vehicule et composants
% cprim_red  :   Couple sur le primaire du reducteur (vecteur sur le cycle)
% wprim_red  :   vitesse sur le primaire du reducteur (vecteur sur le cycle)
% dwprim_red :   d??riv??e de la vitesse sur le primaire du reducteur (vecteur sur le cycle)
% 
% interface de sortie
% ibat_max :    courant max mode tout elec (en recup c'est le courant de recup en tout elec)
% ibat_min :    courant min recharge max ( en recup courant recup plus recharge par moth)
% elec_pos :    vecteur indiquant si mode elec possible , ou zero si impossible
% soc_min :     soc correspondant a ibat_max
% soc_max :     soc correspondant a ibat_min
% Dsoc_min :    variation soc entre deux instant correspondant a ibat_max
%               (atention a int??gre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% Dsoc_max :    variation soc entre deux instant correspondant a ibat_min
%               (atention a int??gre ibat par des sommes et pas des trapz cf ligne 70 et suivante)
% cacm1_elec,qacm1_elec,cprim_red,cfrott_acm1 : utilisee ensuite en
%               recalcul pour les mode tout elec (evite de refaire les optimisation en
%               recalcul
%
% 23-07-12(EV): Creation 


function [ERR,ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max,cacm1_elec,qacm1_elec,cprim_red,cfrott_acm1]=  calc_limites_HSP_1EMB(ERR,param,VD,cprim_red,wprim_red,dwprim_red);
ERR=[];
ii_max=size(VD.CYCL.temps);
soc_min=zeros(ii_max);
soc_max=zeros(ii_max);
Dsoc_min=zeros(ii_max);
Dsoc_max=zeros(ii_max);

sc=length(VD.CYCL.temps);


%% recherche du mode tout electrique ?? chaque pas de temps
  
% Calcul de la vitesse min possible du moteur thermique pour v??rifier que l emode tout ??lec est possible
wmt_min=0*ones(size(wprim_red)); % limitation sur vit max du moth et vit max de la generatrice

if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
    wmt_min(wprim_red>VD.ECU.vmaxele/VD.VEHI.Rpneu*VD.RED.kred)=VD.MOTH.ral;
end

% calcul de la vitesse des machines en tout elec (hybride parralele)

wacm1=wprim_red;
dwacm1=dwprim_red;

% calcul des pertes par frottement des machines 
%cfrott_acm2=interp1(VD.ACM2.Regmot_pert,VD.ACM2.Cpl_frot,abs(wacm2));
cfrott_acm1_elec=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,abs(wacm1));
cfrott_acm1=zeros(1,length(VD.CYCL.temps));
% Recherche du mode de fonctionnement qui minimize les pertes
% On discr??tise cacm1 entre cacm1_min et cacm1_max

cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);

%cacm2_min=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2);
%cacm2_max=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2);

cacm1_elec=cprim_red+VD.ACM1.J_mg*dwacm1;
%[sx,sy]=size(cacm1_mat);

% calcul des pertes dans les machines
[ERR,qacm1_elec]=calc_pertes_acm(ERR,VD.ACM1,cacm1_elec,wacm1,0); 


% Si on ne peut pas faire de mode tout elec cprim>cacm1_max+cacm2_max
% On prend les couples max
ind_max=find(cacm1_max<cprim_red+VD.ACM1.J_mg*dwacm1);

% Si recup max
ind_min=find(cacm1_min>cprim_red+VD.ACM1.J_mg*dwacm1);
cacm1_elec(ind_min)=cacm1_min(ind_min);
[ERR,qacm1_elec(ind_min)]=calc_pertes_acm(ERR,VD.ACM1,cacm1_min(ind_min),wacm1(ind_min),0);

% Calcul des auxiliaires electriques
[ERR,pacc]=calc_acc(VD);

% calcul puissance motelec
pacm1_elec=VD.VEHI.nbacm1*(cacm1_elec.*wacm1+qacm1_elec);


pres=pacm1_elec+pacc;
% 
[ERR,ibat_max,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,100-VD.INIT.Dod0,0,-1,0,param);


% Cas ou l'on as explose les limites en recup
% On doit imposer que Ibat_max = VD.BATT.Ibat_min
ind_rec_max_bat=find(isnan(ibat_max)&cprim_red<0);
ibat_max(ind_rec_max_bat)=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension

% il faut repartir en fw pour recalculer le frein meca et le fonctionnement
% des machines
if ~isempty(ind_rec_max_bat)
    Ibat_min=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,100-VD.INIT.Dod0);
    [ERR,Ubat_min]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,Ibat_min,100-VD.INIT.Dod0,0);
    pbat_min=Ibat_min*Ubat_min;
    [ERR,qacm1_max]=calc_pertes_acm(ERR,VD.ACM1,cacm1_max(ind_rec_max_bat),wacm1(ind_rec_max_bat),dwacm1(ind_rec_max_bat));
    for i = 1: length(ind_rec_max_bat)
        ii=ind_rec_max_bat(i);       
        pacm1_elec(ii)=pbat_min-pacc(ii);
        [ERR,qacm1_elec(ii)]=calc_pertes_acm_fw(ERR,VD.ACM1,pacm1_elec(ii),wacm1(ii),dwacm1(ii));
        cacm1_elec(ii)=(pacm1_elec(ii)-qacm1_elec(ii))./wacm1(ii);
        cprim_red(ii)=cacm1_elec(ii) - VD.ACM1.J_mg*dwacm1(ii) ;
        
    end
end

elec_pos=ones(size(cprim_red));
elec_pos(wmt_min~=0)=0.5; % moth au ralenti mais cmt =0, la batterie fournie toute la puissance
elec_pos(ind_max)=0; % acm1 ne suffit pas
elec_pos(isnan(ibat_max))=0; % la batterie ne peut fournir toute la puissance mode elec impossible

% calcul de la pbat_max
ibat_max(isnan(ibat_max))=interp1(VD.BATT.DoD_Ibat_max,VD.BATT.Ibat_max,VD.INIT.Dod0); % non ! pas si on eclate les limites en tension

% Sur certain point de recup ont peut arriver a faire plus de pertes dans acm1
% que l'on ne recupere d-VD.ACM2.J_mg*dwacm2(7)e puissance sur le primaire reducteur, on regarde
% alors ce qu'ils se passe si on alimente pas la machine
pres_acm_na=pres;
pres_acm_na(cprim_red<0)=pacc(cprim_red<0);
[ERR,ibat_max_acm1_na]=calc_batt(ERR,VD.BATT,param.pas_temps,pres_acm_na,100-VD.INIT.Dod0,0,-1,0,param);
I=find(cprim_red<0 & qacm1_elec>(-cacm1_elec.*wacm1) & elec_pos==1);
ibat_max(I)=ibat_max_acm1_na(I);
cfrott_acm1(I)=max(cfrott_acm1_elec(I),cprim_red(I));
cacm1_elec(I)=cfrott_acm1(I);
qacm1_elec(I)=0;
cprim_red(I)=cacm1_elec(I)- VD.ACM1.J_mg*dwacm1(I);


soc_min(1)=100-VD.INIT.Dod0;
Dsoc_min(1)=0;
for ii=2:length(VD.CYCL.temps)
    soc_min(ii)=soc_min(ii-1)-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_min(ii)=-(RdF(ii).*ibat_max(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

if param.verbose>=3
    [ERR,~,ubat_max,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pres,100-VD.INIT.Dod0,0,-1,0);
     assignin('base','ubat_max',ubat_max)
     assignin('base','wmt_min',wmt_min)
     assignin('base','elec_pos',elec_pos)
end

if param.verbose>=2
    assignin('base','ibat_max',ibat_max)
    assignin('base','cprim_red',cprim_red)
    assignin('base','wprim_red',wprim_red)
    
    figure(3)
    clf
    hold on
    plot(VD.CYCL.temps,ibat_max,'b',VD.CYCL.temps,VD.CYCL.vitesse,'k')
    grid
    
    figure(4)
    clf
    hold on
    plot(VD.CYCL.temps,soc_min,'b',VD.CYCL.temps,60+Dsoc_min,'g')
    legend('soc min','soc max','Dsoc min','Dsoc max')
    grid
end



%% recherche de la pbat_min (mode hybride)la
% calcul du couple max moteur thermique
% il faut verifier la possibilit?? de faire de l'hybride serie, ou de
% l'hybride //
% Cas hybride s??ris il faut que cacm1 suffisent ?? fournir le couple au
% reducteur
int_hs=find(cacm1_max>=cprim_red+VD.ACM1.J_mg*dwacm1);
int_hs_n=find(cacm1_max<cprim_red+VD.ACM1.J_mg*dwacm1);
% cas hybride // il faut que wmt_max soit possible
ind_hp=find(VD.MOTH.wmt_maxi>=wprim_red);
ind_hp_n=find(VD.MOTH.wmt_maxi<wprim_red);

% on recherche la puissance max possible du moteur thermique
% Si l'on est en mode hybride s??rie
[~,i_pmt_max]=max(VD.MOTH.pmt_max);
wmt_pmax_hs=VD.MOTH.wmt_max(i_pmt_max);

pmax_mt_hs=interp1(VD.MOTH.wmt_max,VD.MOTH.pmt_max,wmt_pmax_hs);
cmt_max_hs=pmax_mt_hs./wmt_pmax_hs;
cacm2_max=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wmt_pmax_hs);

cmt_max_hs=min(cmt_max_hs,cacm2_max)*ones(size(cprim_red));
cmt_max_hs(int_hs_n)=0;

% si l'on est en hybride // (vitesse impos??e)
[ERR,~,wmt,dwmt,etat_emb]=calc_emb(VD,cprim_red,wprim_red,dwprim_red,1);

cmt_max_hp=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt);
cmt_max_hp(ind_hp_n)=0;

cmt_max=max(cmt_max_hs,cmt_max_hp);

% On verifie quel'on passe le cycle
%ind=find(cacm1_max+cacm2_max+cmt_max<cprim_red+VD.ACM1.J_mg*dwacm1_elec+VD.ACM2.J_mg*dwacm2_elec);
% indice ou hybride s??rie impossible et hybride parall??le impossible (vitesse MOT) ou
% possible mais couple insuffisant
ind=find( cacm1_max<cprim_red+VD.ACM1.J_mg*dwacm1 & ...
        (VD.MOTH.wmt_maxi<wprim_red | ( VD.MOTH.wmt_maxi>=wprim_red & cacm1_max+cacm2_max+cmt_max<cprim_red+VD.ACM1.J_mg*dwacm1+VD.ACM2.J_mg*dwacm1 )));

% indice ou hybride serie impossible (cacm1 trop petit)
if ~isempty(ind)
    chaine=' Les performances des machines ne permettent pas de passer le cycle aux instant suivants : \n %s \n';
    ERR=MException('BackwardModel:calc_HP_BV_1EMB',chaine,num2str(ind));
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
end

% en hybride s??rie
% connexion
cacm2_hs=-cmt_max_hs;
wacm2_hs=wmt_pmax_hs;
[ERR,qacm2_hs]=calc_pertes_acm(ERR,VD.ACM2,cacm2_hs,wacm2_hs,0);
pelecacm2_hs=cacm2_hs.*wacm2_hs+qacm2_hs;

cacm1_hs=cprim_red+VD.ACM1.J_mg*dwacm1;

[ERR,qacm1_hs]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hs,wacm1,dwacm1);
pelecacm1_hs=cacm1_hs.*wacm1+qacm1_hs;

% calcul de ibatmin
pres_hs=pelecacm1_hs+pelecacm2_hs+pacc; % Normalement il y as un survolteur entre pbat et pres !!
pbat_hs=pres_hs;
[ERR,ibat_min_hs,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_hs,100-VD.INIT.Dod0,0,-1,0,param); 
ibat_min_hs(isnan(ibat_min_hs))=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0);

% en hybride // 
% On suppose que cacm1 marcherai au max en g??n?? 
% puis si ca suffit pas cacm2
% connexion
wacm1=wprim_red;
dwacm1=dwprim_red;

wacm2=wprim_red;
dwacm2=dwprim_red;

cacm1_hp=cprim_red+VD.ACM1.J_mg*dwacm1+VD.ACM2.J_mg*dwacm2+VD.MOTH.J_mt*dwmt-cmt_max_hp;

cond=find(cacm1_hp>cacm1_max);
cacm1_hp(cond)=cacm1_max(cond);

cacm2_hp=zeros(1,length(VD.CYCL.temps));
cacm2_hp(cond)=cprim_red(cond)+VD.ACM1.J_mg*dwacm1(cond)+VD.ACM2.J_mg*dwacm2(cond)-cmt_max_hp(cond)-cacm1_hp(cond)+VD.MOTH.J_mt*dwmt(cond);
cond2=(cacm2_hp>cacm2_max);
cacm2_hp(cond2)=cacm2_max(cond2);

[ERR,qacm1_hp]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hp,wacm1,0);
pelecacm1_hp=cacm1_hp.*wacm1+qacm1_hp;
 
qacm2_hp=zeros(1,length(VD.CYCL.temps));
[ERR,qacm2_hp(cond)]=calc_pertes_acm(ERR,VD.ACM2,cacm2_hp(cond),wacm2(cond),0);
pelecacm2_hp=cacm2_hp.*wacm2+qacm2_hp;

% calcul de ibatmin
pres_hp=pelecacm1_hp+pelecacm2_hp+pacc; % Normalement il y as un survolteur entre pbat et pres !!
pbat_hp=pres_hp;
[ERR,ibat_min_hp,~,~,~,~,~,RdF]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat_hp,100-VD.INIT.Dod0,0,-1,0,param); 
ibat_min_hp(isnan(ibat_min_hp))=interp1(VD.BATT.DoD_Ibat_min,VD.BATT.Ibat_min,VD.INIT.Dod0);


ibat_min=min(ibat_min_hp,ibat_min_hs);

% Si on arrive ?? ibat_min>ibat_max (peut arriver en recup max si les
% limites sont atteintes) on impose ibat_min au min <=ibat_max
ibat_min(ibat_min>ibat_max)=ibat_max(ibat_min>ibat_max);

soc_max(1)=100-VD.INIT.Dod0;
Dsoc_max(1)=0;

for ii=2:length(VD.CYCL.temps)
    soc_max(ii)=soc_max(ii-1)-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
    Dsoc_max(ii)=-(RdF(ii).*ibat_min(ii)/VD.BATT.Nbranchepar/3600/VD.BATT.Cahbat_nom*100).*param.pas_temps;
end

if sum(ibat_max<ibat_min)>0 ||  sum(soc_max<soc_min)>0 || sum(Dsoc_max<Dsoc_min)>0
    chaine='error in calc_limites_HPDP_EPI_SURV' ;
    ERR=MException('BackwardModel:calc_HPDP_EPI_SURV',chaine);
    [ibat_max,ibat_min,elec_pos,soc_min,soc_max,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    return

end

if param.verbose>=2
    assignin('base','ibat_min',ibat_min)

    figure(3)
    plot(VD.CYCL.temps,ibat_min,'g')
    legend('ibat max','vitesse','ibat min')
    title('courant min max fin calc limite')
    
    figure(4)
    plot(VD.CYCL.temps,soc_max,VD.CYCL.temps,Dsoc_max+60)
    legend('soc min','Dsoc min','soc max','Dsoc max')
    title('soc min max fin calc limite')

end

