%function [ERR,cout,ibat,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,Dsoc,elhyb,cfrott_acm1,cfrein_meca_pelec_mot]=...
%    calc_cout_arc_HSP_2EMB_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,soc_p,recalcul,elhyb,cacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,dwacm2_elec,qacm2_elec,cfrott_acm1)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% Calcul des cout des arcs cas HSp_2EMB
%
% interface entree :
% ERR :         message erreur
% param :       strcuture des parametres de simu (discretisation etc)
% CYLC,VD.ADCPL...:structures de description vehicule et composants
% cprim_red :   Couple sur le primaire du reducteur (vecteur sur le cycle)
% wprim_red :   vitesse sur le primaire du reducteur  (vecteur sur le cycle)
% dprim_red :   derivee de la vitesse sur le primaire du reducteur (vecteur sur le cycle)
% Dsoc :        variation de soc sur l'eventail des arcs possible
% elec_pos :    mode electrique possible (vecteur sur le cycle)
% soc_p :       soc ?? l'instant precedent
% i :           instant dans le cycle
% recalcul :    si on est en phase de recalcul (grahe deja resolu) recalcul =1 sinon 0 ou pas passer dans l'interface
% elhyb :       mode elec/hybride/thermique en phase recalcul uniquement
% cacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,dwacm2_elec,qacm2_elec,cfrott_acm1 : en recalcul on utilise les valeurs
%
% interface de sortie
% cout :        cout des arcs de l'eventail
% ibat,ubat ...:variable vehicule utilie uniquement en recalcul sinon on ne renvoie que le cout
%
% 23-07-12(EV): Creation 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ERR,cout,r]=calc_cout_arc_HSP_2EMB_mat(ERR,param,VD,cprim_red,wprim_red,dwprim_red,Dsoc,elec_pos,soc_p,recalcul,elhyb,cacm1_elec,qacm1_elec,cacm2_elec,wacm2_elec,dwacm2_elec,qacm2_elec,cfrott_acm1)
%ibat,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,Dsoc,elhyb,cfrott_acm1,cfrein_meca_pelec_mot]=...
 
r_var={'ibat','ubat','Rbat','u0bat','RdF','dcarb','cmt','wmt','dwmt','cacm1','wacm1','dwacm1','qacm1',...
    'cacm2','wacm2','dwacm2','qacm2','Dsoc','elhyb','cfrott_acm1','cfrein_meca_pelec_mot'};


[Li,Co]=size(soc_p);
[sx,sc]=size(Dsoc);

if sum(sum(isnan(soc_p)))==Li*Co
    ERR=MException('BackwardModel:calc_cout_arc_HSP_2EMB', 'appel a calc_cout_arc_HSP_2EMB : tout les elements d''un des vecteur precedent a NaN');
    [cout,ibat,ubat,R,E,RdF,dcarb,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cacm2,wacm2,dwacm2,qacm2,Dsoc,elhyb,cfrott_acm1,cfrein_meca_pelec_mot]=backward_erreur(size(Dsoc));
    [~,r]=affect_struct(r_var);
    return
end


% Calcul de la vitesse min possible du moteur thermique pour v??rifier que l emode tout ??lec est possible
wmt_min=0*ones(size(wprim_red)); % limitation sur vit max du moth et vit max de la generatrice

if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
    wmt_min(wprim_red>VD.ECU.vmaxele/VD.VEHI.Rpneu*VD.RED.kred)=VD.MOTH.ral;
end

% calcul batterie
% calcul ibat ?? partir de Dsoc
RdF=ones(size(Dsoc));
RdF(Dsoc>0)=VD.BATT.RdFarad;
ibat=-Dsoc./RdF*VD.BATT.Nbranchepar*3600*VD.BATT.Cahbat_nom/100/param.pas_temps;


[ERR,ubat,~,~,E,R]=calc_batt_fw(ERR,VD.BATT,param.pas_temps,ibat,soc_p,0);

pbat=ubat.*ibat;

[ERR,pacc]=calc_acc(VD);
pacc_mat=pacc(1);

% connexion
wacm1=wprim_red;
dwacm1=dwprim_red;
cacm1_max=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmax_mot(:,length(VD.ACM1.Cmax_mot(1,:))),wacm1);
cacm1_min=interp1(VD.ACM1.Regmot_cmax,VD.ACM1.Cmin_mot(:,length(VD.ACM1.Cmin_mot(1,:))),wacm1);
cfrott_acm1_hp=interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,abs(wacm1));

if isfield(param,'exist_vit_max_elec') & param.exist_vit_max_elec==1
    wmt_min(wprim_red>VD.ECU.vmaxele/VD.VEHI.Rpneu*VD.RED.kred)=VD.MOTH.ral;
end


if (isfield(param,'fit_pertes') & param.fit_pertes==1)   
elseif ~isfield(param,'fit_pertes') | param.fit_pertes~=1
  
    %% calcul du cout des arcs dans le mode hybride serie
    [VD]=ge_optimum_perte(VD,25,25,0); % recherche courbe optimal du groupe
    cacm1_hs=cprim_red+VD.ACM1.J_mg*dwacm1;
    [ERR,qacm1_hs]=calc_pertes_acm(ERR,VD.ACM1,cacm1_hs,wacm1,dwacm1);
    pelecacm1_hs=cacm1_hs.*wacm1+qacm1_hs;
    pge=repmat(pelecacm1_hs,sx,1)-pbat+pacc_mat;
    
    wacm2_hs=interp1(VD.GE.pge_opti,VD.GE.wmt_opti,pge);
    qacm2_hs=interp1(VD.GE.pge_opti,VD.GE.qacm2_opti,pge);
    cacm2_hs=-(pge+qacm2_hs)./wacm2_hs;
    cmt_hs=-cacm2_hs;    
    wmt_hs=wacm2_hs;
   [ERR,dcarb_hs]=calc_mt(ERR,VD.MOTH,cmt_hs,wmt_hs,0,1,0);

    % On met a inf les cas ou la puissance du groupe serait <0 ou > a pge.max
    ind_ge_n=find(pge<0 & pge>VD.GE.pge_opti(end));
    dcarb_hs(ind_ge_n)=Inf;
    
    
    % On met a inf les cas ou le mode hybride serie est impossible car cacm1_max<cprim_red
    ind_hs_n=find(cacm1_max<cprim_red+VD.ACM1.J_mg*dwacm1);
    dcarb_hs(:,ind_hs_n)=Inf;

    if param.verbose>=3
        assignin('base','pelecacm1_hs',pelecacm1_hs);
        assignin('base','pge',pge);
        assignin('base','cmt_hs',cmt_hs);
        assignin('base','wmt_hs',wmt_hs);
        assignin('base','cacm1_hs',cacm1_hs);
        assignin('base','qacm1_hs',qacm1_hs);
        assignin('base','cacm2_hs',cacm2_hs);
        assignin('base','qacm2_hs',qacm2_hs);
    end
       
    %% calcul du cout des arcs dans le cas hybride //
    wacm2_hp=wprim_red;
    dwacm2_hp=dwprim_red;
    cacm2_max_hp=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmax_mot(:,length(VD.ACM2.Cmax_mot(1,:))),wacm2_hp);
    cacm2_min_hp=interp1(VD.ACM2.Regmot_cmax,VD.ACM2.Cmin_mot(:,length(VD.ACM2.Cmin_mot(1,:))),wacm2_hp);
    
    % Recherche du mode de fonctionnement qui minimize les pertes
    % On discretise cacm1 entre cacm1_min et cacm1_max
    cacm1_mat=Inf*ones(floor(max((cacm1_max-cacm1_min)/param.pas_cacm1))+2,sc);
    for ii=1:sc
        cacm1_vec=[cacm1_min(ii) fliplr(-param.pas_cacm1:-param.pas_cacm1:cacm1_min(ii))  0:param.pas_cacm1:cacm1_max(ii) cacm1_max(ii)]';
        cacm1_mat(1:length(cacm1_vec),ii)=cacm1_vec;
    end
    % cas EM1 non alimente il faudrait aussi prevoir cas EM2 non alimente
    % et calculer couple EM1 a partir inversion carto EM1.
    cacm1_mat=[cacm1_mat ; cfrott_acm1_hp];
    [sz,~]=size(cacm1_mat);
    wacm1_mat=repmat(wacm1,sz,1);
    [ERR,qacm1_mat]=calc_pertes_acm(ERR,VD.ACM1,cacm1_mat,wacm1_mat,0);
    qacm1_mat(sz,:)=0;
    pelec_acm1_mat=cacm1_mat.*wacm1_mat+qacm1_mat;
    pelec_acm1_mat(sz,:)=0;
    
    
    % On passe a des tableaux 3D
    qacm1_tab=repmat(reshape(qacm1_mat',[1 sc sz]),sx,1);
    pelec_acm1_tab=repmat(reshape(pelec_acm1_mat',[1 sc sz]),sx,1);
    wacm1_tab=repmat(reshape(wacm1_mat',[1 sc sz]),sx,1);
    
    cacm1_tab=repmat(reshape(cacm1_mat',[1 sc sz]),sx,1);
    pbat_tab=repmat(pbat,[1 1 sz ]);
    wacm2_tab=repmat(wacm2_hp,[sx 1 sz]);
    pelec_acm2_tab=pbat_tab-pelec_acm1_tab-pacc(1);
    [ERR,qacm2_tab]=calc_pertes_acm_fw(ERR,VD.ACM2,pelec_acm2_tab,wacm2_tab,repmat(dwacm2_hp,[sx 1 sz]));
    cacm2_tab=(pelec_acm2_tab-qacm2_tab)./wacm2_tab;
    cacm2_tab(isnan(cacm2_tab))=Inf;
    cacm2_tab( cacm2_tab>repmat(cacm2_max_hp,[sx 1 sz]) | cacm2_tab<repmat(cacm2_min_hp,[sx 1 sz]))= Inf;
    qacm2_tab( cacm2_tab>repmat(cacm2_max_hp,[sx 1 sz]) | cacm2_tab<repmat(cacm2_min_hp,[sx 1 sz]))= Inf;
    
    % On prend les min des pertes
    [min_q,ind_qmin]=min(qacm1_tab+qacm2_tab,[],3);
    ind_qmin_lin=(ind_qmin-1)*sc*sx+sx*(repmat(1:sc,[sx 1])-1)+repmat((1:sx)',[1 sc]);
    %On recupere les couples correspondand
    cacm1_hp=NaN*ones(sx,sc);
    cacm2_hp=NaN*ones(sx,sc);
    cacm1_hp(:,:)=cacm1_tab(ind_qmin_lin);
    cacm2_hp(:,:)=cacm2_tab(ind_qmin_lin);
    
    ind_nan=find(isnan(min_q) | isinf(min_q));
    cacm1_hp(ind_nan)=NaN;
    cacm2_hp(ind_nan)=NaN;
    
    % indice hybride // impossible
    [ERR,~,wmt_hp,dwmt_hp]=calc_emb(VD,cprim_red,wprim_red,dwprim_red,1*ones(1,sc));
    cmt_max_hp=interp1(VD.MOTH.wmt_max,VD.MOTH.cmt_max,wmt_hp);
   
    cmt_hp=repmat(cprim_red,[sx 1])-cacm1_hp+VD.ACM1.J_mg*repmat(dwacm1,[sx 1])-cacm2_hp+VD.ACM2.J_mg*repmat(dwacm2_hp,[sx 1])+VD.MOTH.J_mt.*repmat(dwmt_hp,[sx 1]);
    [ERR,dcarb_hp,cmt_hp]=calc_mt(ERR,VD.MOTH,cmt_hp,repmat(wmt_hp,[sx 1]),0,1,0);
    
    %% On met a Inf les cas ou le moth fonctionerai en frein moteur
    dcarb_hp(cmt_hp<0)=Inf;
    
    %% On met a inf les cas ou le mode hybride serie est impossible car cacm1_max<cprim_red
    % a priori les cas ou wmt>wmt_max sont deja traite par calc_mt

    I_hs_np=find(cacm1_max<cprim_red+VD.ACM1.J_mg*dwacm1);
    dcarb_hs(:,I_hs_np)=Inf;
    
    dcarb=min(dcarb_hs,dcarb_hp);
    
    
    %% Il faut eliminer les valeurs de couple imposible !!
    if param.verbose>=3
        assignin('base','sc',sc);
        assignin('base','sx',sx);
        assignin('base','ind_qmin_lin',ind_qmin_lin);
        assignin('base','ind_qmin',ind_qmin);
        assignin('base','qacm2_tab',qacm2_tab);
        assignin('base','cacm2_tab',cacm2_tab);
        assignin('base','cacm2_hp',cacm2_hp);
        assignin('base','cacm1_hp',cacm1_hp);
        assignin('base','pelec_acm1_tab',pelec_acm1_tab);
        assignin('base','pelec_acm2_tab',pelec_acm2_tab);
        assignin('base','pbat_tab',pbat_tab);
        assignin('base','cacm1_tab',cacm1_tab);
        assignin('base','qacm1_tab',qacm1_tab);
        assignin('base','cacm2_tab',cacm2_tab);
        assignin('base','qacm2_tab',qacm2_tab);
        assignin('base','cprim_red',cprim_red);
        assignin('base','wacm1_tab',wacm1_tab);
        assignin('base','wacm2_tab',wacm2_tab);
        assignin('base','dcarb_hs',dcarb_hs);
        assignin('base','dcarb_hp',dcarb_hp);
        assignin('base','cmt_hp',cmt_hp);
        assignin('base','wmt_hp',wmt_hp);
    end
end

% Calculs batterie il faut supprimer les arcs impossible
% Jusqu'ici ibat est calcul?? ?? partir de Dsoc.
%% Utilie ou pas ??
% if nargin==21 % en recalcul on a plus qu'un arc et normalment il est valide
%     [ERR,Ibat]=calc_batt(ERR,VD.BATT,param.pas_temps,pbat(1,:,1),soc_p,0,0,0);
%     dcarb(isnan(Ibat))=Inf;
% end

% Critere de cout des arcs
if nargin ==9 || nargin==18 && recalcul==0
    % mode tout elec moteur thermique arrete
    dcarb(1,find(elec_pos==1))=0;

    % on est en mode "tout elec" cmt=0 mais le moteur thermique doit tourner a wmt_min
    % a priori on as deja trouver wmt=wmt_min mais on est pas sur d'avoir
    % converge exactement vers cmt=0

    [ERR,dcarb(1,find(elec_pos==0.5))]=calc_mt(ERR,VD.MOTH,0*ones(1,sum(elec_pos==0.5)),wmt_min(find(elec_pos==0.5)),0,1,0);
   
    
elseif nargin==18 && recalcul==1  
    % cas hybride et generale (ont pourrais limiter le calcul au cas hybride si besoin
    % cas hs elhyb=1
    % cas hp elhyb =2
    I_e0=find(elhyb==0);
    I_e05=find(elhyb==0.5);
    %I_e1=find(elhyb==1);

    
    dwmt=zeros(1,sc);
    wmt=zeros(1,sc);
    cmt=zeros(1,sc);
    
    dcarb(I_e0)=0;
    wmt(I_e05)=wmt_min(I_e05);
    [ERR,dcarb(I_e05)]=calc_mt(ERR,VD.MOTH,cmt(I_e05),wmt(I_e05),0,1,0);
    
    %elhyb(dcarb_hs==dcarb)=1; deja a 1 si hybride
    elhyb(dcarb==dcarb_hp & elhyb==1)=2;
    I_e2=find(elhyb==2);  
    I_e1=find(elhyb==1);
    
    cmt(I_e1)=cmt_hs(I_e1);
    wmt(I_e1)=wmt_hs(I_e1);
    %dcarb(I_e1)=dcarb_hs(I_e1);
    
    cmt(I_e2)=cmt_hp(I_e2);
    wmt(I_e2)=wmt_hp(I_e2);
    dwmt(I_e2)=dwmt_hp(I_e2);
    %dcarb(I_e2)=dcarb_hp(I_e2);
    if param.verbose>=3
        assignin('base','elhyb',elhyb);
        assignin('base','cmt',cmt);
        assignin('base','wmt',wmt);
    end
    
end  

if param.verbose>=3  
    assignin('base','dcarb',dcarb);   
end

cout=dcarb*param.pas_temps;
cout(isnan(cout))=Inf;

if nargin==18 && recalcul==1 % cas recalcul   
 
    cacm1=cprim_red+VD.ACM1.J_mg*dwacm1; 
    cacm1(I_e0)=cacm1_elec(I_e0);
    cacm1(I_e05)=cacm1_elec(I_e05);  
    cacm1(I_e1)=cacm1_hs(I_e1);
    cacm1(I_e2)=cacm1_hp(I_e2);
    
    cacm2=zeros(1,sc);
    wacm2=zeros(1,sc);
    dwacm2=zeros(1,sc);
    
    dwacm2(I_e0)=dwacm1(I_e0);
    dwacm2(I_e2)=dwacm1(I_e2);
    dwacm2(I_e05)=dwacm1(I_e05);
    
    cacm2(I_e0)=cacm2_elec(I_e0);
    cacm2(I_e05)=cacm2_elec(I_e05);
    cacm2(I_e1)=cacm2_hs(I_e1);
    cacm2(I_e2)=cacm2_hp(I_e2);
    
    wacm2(I_e05)=wacm2_elec(I_e05);
    wacm2(I_e0)=wacm2_elec(I_e0);
    wacm2(I_e1)=wacm2_hs(I_e1);
    wacm2(I_e2)=wacm2_hp(I_e2);
    dwacm2(I_e05)=dwacm2_elec(I_e05);
    dwacm2(I_e0)=dwacm2_elec(I_e0);
     
    cfrott_acm1(elhyb>0.7)=0;
    
    % calcul puissance elec acm1
    if (isfield(param,'fit_pertes') & param.fit_pertes==1)
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1,0,1);
    else
        [ERR,qacm1]=calc_pertes_acm(ERR,VD.ACM1,cacm1,wacm1,dwacm1);
    end
    % pour les cas tt elec ou acm1 est non alimente

    qacm1(I_e0)=qacm1_elec(I_e0); 
    qacm1(I_e05)=qacm1_elec(I_e05);
    
    % Si on est en hybride parallele et que acm1 non alimente alors il faut remettre qacm1 a zero
    cond_acm1_na=find(elhyb==2 & ind_qmin==sz);
    qacm1(cond_acm1_na)=0;
    cfrott_acm1(cond_acm1_na)=cacm1(cond_acm1_na);
     
    % calcul puissance elec acm2
    if (isfield(param,'fit_pertes') & param.fit_pertes==1)
         [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0,0,1);
    else
        [ERR,qacm2]=calc_pertes_acm(ERR,VD.ACM2,cacm2,wacm2,0);
    end
    
    qacm2(I_e0)=qacm2_elec(I_e0); 
    qacm2(I_e05)=qacm2_elec(I_e05);
    qacm2_hp=qacm2_tab(ind_qmin_lin);
    qacm2(I_e2)=qacm2_hp(I_e2); % en hybride serie on remet les pertes calculr fw

    qacm2(I_e1)=qacm2_hs(I_e1); % en hybride // on remet les pertes calculer par l'interpolation pour le choix du pts de fonctinnement
    % si on est en recup et que acm1 non alimente
    cfrein_meca_pelec_mot=zeros(1,sc);

    Rbat=R;
    u0bat=E*ones(size(cprim_red));
    [ERR,r]=affect_struct(r_var);
    
    %cfrott_acm1=zeros(1,sc);
    %cfrott_acm1(cond_acm1_na)=cacm1(cond_acm1_na);
   

   % I=find(elhyb==0 & cprim_red<0 & qacm1+qacm2>(-cacm1.*wacm1-cacm2.*wacm2))
    %I=find(elhyb==0 & cprim_red<0 & qacm1>-cacm1.*wacm1);% recup avec plus de ppelec_acm1(56)ertes dans acm1 que de puissance surl'arbre
   % cfrott_acm1(I)=max(interp1(VD.ACM1.Regmot_pert,VD.ACM1.Cpl_frot,wacm1(I)),cprim_red(I)); %la machine elec ne freinera pas plus que le couple de freinage
    %cacm1(I)=0;
   % cacm2(I)=0;
    %cfrein_meca_pelec_mot(I)=1;
    %qacm1(I)=0;
    %qacm2(I)=0;

end

if sum(isinf(cout))==length(cout)
    chaine=' all edge are impossible, the systems possibilites does not allow to fullfil the required systems operating point ! \n time : %d \n Dsoc : %s  \n cost : %s \n ibat: %s \n ubat: %s \n cmt: %s \n wmt: %s';
    ERR=MException('BackwardModel:calc_cout_arc_HPDP_EPI_SURV',chaine,i,num2str(Dsoc),num2str(cout),num2str(ibat),num2str(ubat),num2str(cmt),num2str(wmt));
    [cout,Dsoc_tt,indice_rap_opt,ibat,ubat,R,E,RdF,dcarb,cprim2_cpl,cmt,wmt,dwmt,cacm1,wacm1,dwacm1,qacm1,cprim1_cpl,Dsoc,cfrein_meca_pelec_mot]=backward_erreur(length(Dsoc));
    [~,r]=affect_struct(r_var);
    return
end

if sum(isnan(cout))
    chaine='some edge cost are NaN, to avoid problems in solve graph, prefer to fixe them to Inf \n time : %d' ;
    warning('BackwardModel:calc_cout_arc_HPDP_EPI_SURV',chaine,i);
end

return

