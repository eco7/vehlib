% function [ERR,poids_min,meilleur_chemin]=solve_graph(ERR,fonc_cout_arc,param,lim_sup,lim_inf,var_com_inf,var_com_sup,reg,lim_elec)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Resolution d'un graphe (programmation dynamique).
%
% Interface entree :
% ERR :                 gestion des erreurs
%
% fonc_cout arc :       fonction de calcul du cout des arcs ecrit sous forme de handle cf appel dans calc_VELEC_BATT_SC :
%                       @(ERR,pas,i,isc,u0sc_p,ux_p,soc_p,ah_p) calc_cout_arc(ERR,param,pas,pres,isc,u0sc_p,ux_p,soc_p,ah_p,i)
%                       La fonction de calcul de cout des arcs doit calculer les cout de tout les
%                       arcs d'une colonne a partir des var_p de la colonne precedentes et renvoi
%                       donc une matrice de cout de taille (nombre de point max d'une colonne *nombre d'arc max).
%                       Si des arcs sontimpossible on eur affecte un poids infini.
%                       Elle doit contenir dans son inerface (dans un ordre quelconque) ERR, pas, i et une variable d'etat (ici
%                       isc), un nombre de parametre a l'instant precedent(defini dans param.Nb_par_p) et un certain nombre
%                       de donnees ( DCDC ....)
%
% param :               Strucuture de parametres du graphe definis dans param_backward_prog_dyn (precision, nombre de var a� stocker a� l'instant precedent...)
% lim_sup,lim_inf :     Limites du graphes exprimees en indice
% lim_elec :            Limites tout électrique sans la remonté "permettra de connaitre les arcs tout élec dans le graphes et de leur associé un cout nul
% var_com_inf :         vecteur des valeurs de la variable de commande sur l'arc inférieur de l'éventail (ex: VD.SC.maxcourant si VD.BATT plus VD.SC)
% var_com_sup :         vecteur des valeurs de la variable de commande sur l'arc supérieur de l'éventail (ex: -VD.SC.maxcourant si VD.BATT plus VD.SC)
% param :               structure des paramètres contenant les pas, la précision du graphe (en pas de la variable de commande), le nombres de
%                       grandeurs à stocké à chaque points et leur valeurs initiales.
% reg   :               Indique si le graphe est régulier 1 si oui 0 si non
%
% Interface sortie :
% Poids_min :           Poids minimale possible
% Meilleur chemin       Meilleur chemin en indice (vecteur fonction du temps).
%
% 30_06_10 (EV) : correction probleme ip_min (reservation des tables)
% mais pas de garantie que ca marche si ip_min different de 1
%
% 06_07_10 (EV) : correction pb sur ip_min, modification appel pour rendre
% cette fonction "impersonnel" et independante de l'architecture
%
% 28_10_10 (EV) : modifs pour permettre un graphe (irregulier) et insertion
% des arcs tout élec par exemple
%
% 20_01_12 (EV) : changement "algo" pour tenter une résolution //


function [ERR,poids_min,meilleur_chemin]=solve_graph_2(ERR,fonc_cout_arc,param,lim_sup,lim_inf,var_com_inf,var_com_sup,reg,lim_elec)
% Il faut connaitre taille du graphe, et limite du graphe

%i_max=length(lim_sup);
i_max=length(var_com_sup);

ip_max=max(lim_sup); % indice max des poids dans le graphe
ip_min=min(lim_inf); % indice min des poids dans le graphe

if ip_min~=1
    if param.verbose>=2
    chaine='ip_min ~=1 index shifting \n The best traject will be reference to 1 and not min(lim_inf)' ;
    warning('BackwardModel:solve_graph',chaine);  
    end
    lim_sup=lim_sup-ip_min+1;
    lim_inf=lim_inf-ip_min+1;
    ip_min=min(lim_inf);
    ip_max=max(lim_sup);
end

if lim_sup(1)~=lim_inf(1) 
    chaine=strcat(' Les limites a l''origine  sont differentes graphe mal construit');
    ERR=MException('BackwardModel:solve_graph',chaine);
end

if lim_sup(end)~=lim_inf(end) &  param.verbose>=2
    chaine='The end limits are differents : \n upper end limits: %f \n lower end limits : %f' ;
    warning('BackwardModel:solve_graph',chaine,lim_inf(end),lim_sup(end));
end

% réservation des tables et vecteurs
chemin_opt=repmat(uint32(1:1:ip_max)',1,i_max);

imp=ones(1,ip_max,'int32');
poids=zeros(1,ip_max);
poids_p=zeros(1,ip_max);
%poids_temp=zeros(1,ip_max);

% réservation des vecteurs pour les grandeurs a conserver aux instant
% précedents on réserve des matrices de taille ip_max (indice des poids
% max) * (nbre variable à conserver a l'instant précedent)
if param.Nb_var_p~=0
    var_p=NaN*ones(ip_max,param.Nb_var_p);
end
% réservation table des variables issus de de la fonction cout
% tableau 3D (indice des poids max) * (nombre d'arc dans les éventails)
% * (nbre variable à conserver a l'instant précedent)
k_min=1*ones(size(var_com_inf));
k_max=floor(abs((var_com_sup-var_com_inf)/param.prec_graphe))+1;
k_moy=ceil(var_com_inf/param.prec_graphe)+1;


cout=Inf*ones(ip_max,max(k_max-k_min)+1);
if param.Nb_var_p~=0
    var_mat=NaN*ones(ip_max,max(k_max-k_min)+1,param.Nb_var_p);
end
% initialisation des grandeurs
if param.Nb_var_p~=0
    var_p(lim_sup(1),:)=param.var;
end
% initialisation (var commande)
%var_com=NaN*ones(1,max(k_max-k_min)+1); %   INUTILE et probablement faut car dimension 1 a cause k_max <0 si batt, coriger cf solve_graph 3D

% initialisation chemin
chemin_opt(lim_sup(1),1)=lim_sup(1);

if param.verbose>=1
    h = waitbar(0,'resolution du graphe ...');
end
t_calc_cout_arc=0;
t_res_graph=0;

for i=2:i_max+1
    if param.verbose>=2
        fprintf(1,'%s %d \n','solve_graph : calcul sur la colonne :',i)
    end
    if param.verbose>=1
        waitbar(i/i_max,h);
    end
    var_com=var_com_inf(i-1):-param.prec_graphe:var_com_sup(i-1);
    
    poids_p(1:ip_max)=poids(1:ip_max);
    poids(1:ip_max)=Inf;
    imp(1:ip_max)=1;
    j_max=lim_sup(i);
    j_min=lim_inf(i);
    
    %% Appel fonction cout
    % calcul des couts sur tout les eventails en partant de tout les points
    % de la colonne précédente (y compris si NaN).
    %[ERR,cout,var_mat(:,:,1),var_mat(:,:,2),var_mat(:,:,3),var_mat(:,:,4)]=fonc_cout_arc(ERR,i,var_com,var_p(:,1)',var_p(:,2)',var_p(:,3)',var_p(:,4)');
    % L'appel ci dessus est reecrit dans chaine de facon a pouvoir le reaaliser pour un nombre different de var et var_p
    tdeb_calc_cout_arc=cputime;
    if param.Nb_var_p~=0
        chaine='[ERR,cout';
        for k=1:param.Nb_var_p
            chaine=strcat(chaine,',var_mat(:,1:length(var_com),',num2str(k),')');
        end
        chaine=strcat(chaine,']=fonc_cout_arc(ERR,i,var_com');
        for k=1:param.Nb_var_p
            chaine=strcat(chaine,',var_p(:,',num2str(k),')''');
        end
        chaine=strcat(chaine,');');
        eval(chaine)
    else
        [ERR,cout]=fonc_cout_arc(ERR,i,var_com);
    end
    tfin_calc_cout_arc=cputime;
    t_calc_cout_arc=t_calc_cout_arc+tfin_calc_cout_arc-tdeb_calc_cout_arc;
    
    if ~isempty(ERR)
        [poids_min,meilleur_chemin]=backward_erreur;
        return;
    end
    

    for j=lim_inf(i):1:lim_sup(i)
        %fprintf(1,'%s %d \n','calcul sur la ligne :',j)
        if nargin==7 | (nargin==9 & reg==1)
            ip_inf=max(j-(k_max(i)-k_moy(i)),lim_inf(i-1));
            ip_sup=min(j+k_moy(i)-1,lim_sup(i-1));
            
            k_inf=max(k_min(i),k_moy(i)-(lim_sup(i-1)-j));
            k_sup=min(k_max(i),k_moy(i)-(lim_inf(i-1)-j));
        elseif nargin ==9 & reg==0
            di_elec=lim_elec(i)-lim_elec(i-1);
            Nb_arc=k_max(i-1)-k_min(i-1)+1;
            inc_haut=di_elec+Nb_arc-1;
            ip_sup=min(j-di_elec,lim_sup(i-1)); 
            ip_inf=max(j-inc_haut,lim_inf(i-1));
            
            if j-lim_sup(i-1)>di_elec
                k_inf=1+j-(lim_sup(i-1)+di_elec);
            else
                k_inf=1;
            end
            
            k_sup=k_inf+(ip_sup-ip_inf);
            
        end
      
        if ip_sup>=ip_inf % si ip_inf>ip_sup cas pas d'arc pour atteindre le point
            k=k_sup:-1:k_inf;
            ip=ip_inf:1:ip_sup;
            
            if isvector(cout)
                [poids(j),J_min]=min(poids_p(ip_inf:ip_sup)+cout(k_sup:-1:k_inf));         
                imp(j)=ip(J_min);
            else
                [sx,~]=size(cout);
                ind_lin=(k-1)*sx+ip;
                [poids(j),J_min]=min(poids_p(ip_inf:ip_sup)+cout(ind_lin));
                imp(j)=ip(J_min);     
            end
            
            if param.Nb_var_p~=0
                if param.Nb_var_p==1
                    var_p(j)=var_mat(ind_lin(J_min));
                else
                    x=rem(ind_lin(J_min),sx);
                    y=ceil(ind_lin(J_min)/sx);
                    var_p(j,:)=var_mat(x,y,:);
                end
            end
        end
    end

    
    % mise en "memoire" des meilleurs chemins
    chemin_opt(j_min:j_max,1:i-1) = chemin_opt(imp(j_min:j_max),1:i-1);

    if sum(isnan(poids))+sum(isinf(poids))==length(poids)
        chaine=' all weight are Inf or NaN ! \n i : %d ';
        ERR=MException('BackwardModel:solve_graph',chaine,i);
        if ~isfield(param,'dim') | param.dim==0 % Dans le cas d'un processus de dimensionnement param.dim==1 on ne sort pas en "vrac" on renvoi seulement l'erreur
            [poids_min,meilleur_chemin]=backward_erreur(length(lim_sup),1);
            return
        end
    end
    tfin_res_graph=cputime;
    t_res_graph=t_res_graph+tfin_res_graph-tfin_calc_cout_arc;
end
if param.verbose>=1
    close(h); % fermeture waitbar
end
if param.verbose>=1 | param.verbose==-1
fprintf(1,'%s %.2f %s\n','temps cumule de calcul du cout des arcs :',t_calc_cout_arc,'s');
fprintf(1,'%s %.2f %s\n','temps cumule de resolution du graph :',t_res_graph,'s');
end
poids_min=poids(lim_inf(end):lim_sup(end));

meilleur_chemin=chemin_opt((lim_inf(end):lim_sup(end)),:);
meilleur_chemin(1,i_max+1)=lim_inf(end);
meilleur_chemin(2,i_max+1)=lim_sup(end);


