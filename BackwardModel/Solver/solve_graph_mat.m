% function [ERR,poids_min,meilleur_chemin]=solve_graph(ERR,fonc_cout_arc,param,lim_sup,lim_inf,var_com_inf,var_com_sup,reg,lim_elec)
%
% C Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Resolution d'un graphe (programmation dynamique).
%
% Interface entree :
% ERR :                 gestion des erreurs
%
% fonc_cout arc :       fonction de calcul du cout des arcs ecrit sous forme de handle cf appel dans calc_VELEC_BATT_SC :
%                       @(ERR,pas,i,isc,u0sc_p,ux_p,soc_p,ah_p) calc_cout_arc(ERR,param,pas,pres,isc,u0sc_p,ux_p,soc_p,ah_p,i)
%                       La fonction de calcul de cout des arcs doit calculer les cout de tout les
%                       arcs d'une colonne a partir des var_p de la colonne precedentes et renvoi
%                       donc une matrice de cout de taille (nombre de point max d'une colonne *nombre d'arc max).
%                       Si des arcs sontimpossible on eur affecte un poids infini.
%                       Elle doit contenir dans son inerface (dans un ordre quelconque) ERR, pas, i et une variable d'etat (ici
%                       isc), un nombre de parametre a l'instant precedent(defini dans param.Nb_par_p) et un certain nombre
%                       de donnees ( DCDC ....)
%
% param :               Strucuture de parametres du graphe definis dans param_backward_prog_dyn (precision, nombre de var a stocker a l'instant precedent...)
% lim_sup,lim_inf :     Limites du graphes exprimees en indice
% lim_elec :            Limites tout electrique sans la remonte "permettra de connaitre les arcs tout elec dans le graphes et de leur associe un cout nul
% var_com_inf :         vecteur des valeurs de la variable de commande sur l'arc inferieur de l'eventail (ex: VD.SC.maxcourant si VD.BATT plus VD.SC)
% var_com_sup :         vecteur des valeurs de la variable de commande sur l'arc superieur de l'eventail (ex: -VD.SC.maxcourant si VD.BATT plus VD.SC)
% param :               structure des parametres contenant les pas, la precision du graphe (en pas de la variable de commande), le nombres de
%                       grandeurs a stocke a chaque points et leur valeurs initiales.
% reg   :               Indique si le graphe est regulier 1 si oui 0 si non
%
% Interface sortie :
% Poids_min :           Poids minimale possible
% Meilleur chemin       Meilleur chemin en indice (vecteur fonction du temps).
%
% 30_06_10 (EV) : correction probleme ip_min (reservation des tables)
% mais pas de garantie que ca marche si ip_min different de 1
%
% 06_07_10 (EV) : correction pb sur ip_min, modification appel pour rendre
% cette fonction "impersonnel" et independante de l'architecture
%
% 28_10_10 (EV) : modifs pour permettre un graphe (irregulier) et insertion
% des arcs tout elec par exemple
%
% 20_01_12 (EV) : changement "algo" pour tenter une resolution //
%
% 04_03_13 (EV) : changement calcul meilleur chemin pour eviter recopie de
% ligne dans la matrice chemin_opt


function [ERR,poids_min,meilleur_chemin,Dsoc_mat]=solve_graph_mat(ERR,fonc_cout_arc,param,lim_sup,lim_inf,var_com_inf,var_com_sup,reg,lim_elec)
% Il faut connaitre taille du graphe, et limite du graphe

i_max=length(lim_sup);

ip_max=max(lim_sup); % indice max des poids dans le graphe
ip_min=min(lim_inf); % indice min des poids dans le graphe

if ip_min~=1
    if param.verbose>=2
    chaine='ip_min ~=1 index shifting \n The best traject will be reference to 1 and not min(lim_inf)' ;
    warning('BackwardModel:solve_graph',chaine);  
    end
    lim_sup=lim_sup-ip_min+1;
    lim_inf=lim_inf-ip_min+1;
    ip_min=min(lim_inf);
    ip_max=max(lim_sup);
end

if lim_sup(1)~=lim_inf(1) 
    chaine=strcat(' Les limites a l''origine  sont differentes graphe mal construit');
    ERR=MException('BackwardModel:solve_graph',chaine);
end

if lim_sup(end)~=lim_inf(end) &  param.verbose>=2
    chaine='The end limits are differents : \n upper end limits: %f \n lower end limits : %f' ;
    warning('BackwardModel:solve_graph',chaine,lim_inf(end),lim_sup(end));
end

% reservation des tables et vecteurs
chemin_opt=repmat(uint32(1:1:ip_max)',1,i_max);

imp=ones(1,ip_max,'uint32');

poids=zeros(1,ip_max);
poids_p=zeros(1,ip_max);

% reservation des vecteurs pour les grandeurs a conserver aux instant
% precedents on reserve des matrices de taille ip_max (indice des poids
% max) * (nbre variable a conserver a l'instant precedent)
if param.Nb_var_p~=0
    var_p=NaN*ones(ip_max,param.Nb_var_p);
end
% reservation table des variables issus de de la fonction cout
% tableau 3D (indice des poids max) * (nombre d'arc dans les eventails)
% * (nbre variable a conserver a l'instant precedent)
k_min=1*ones(size(var_com_inf));
k_max=floor(abs((var_com_sup-var_com_inf)/param.prec_graphe))+1;
k_moy=ceil(var_com_inf/param.prec_graphe)+1;

if param.Nb_var_p~=0
    var_mat=NaN*ones(ip_max,max(k_max-k_min)+1,param.Nb_var_p);
end
% initialisation des grandeurs
if param.Nb_var_p~=0
    var_p(lim_sup(1),:)=param.var;
end


% initialisation chemin
chemin_opt(lim_sup(1),1)=lim_sup(1);

if isfield(param,'calc_mat') & param.calc_mat==1
    tdeb_cal_cout_arc=cputime;
    if param.verbose>=1
        h = msgbox('calcul des couts des arcs ...','wait');
    end
    
    %% calcul matricielle des cout
    var_com_mat=NaN*ones(floor(max((var_com_inf-var_com_sup)./param.prec_graphe))+1,i_max);
    for i=2:i_max
        var_com=var_com_inf(i):-param.prec_graphe:var_com_sup(i);
        var_com_mat(1:length(var_com),i)=var_com;
    end
   
    Dsoc_mat=var_com_mat;
    [ERR,cout_mat]=fonc_cout_arc(ERR,var_com_mat);
    %   end
    if ~isempty(ERR)
        [poids_min,meilleur_chemin]=backward_erreur;
        return;
    end
    tfin_calc_cout_arc=cputime;
    if param.verbose>=1
        close(h); % fermeture waitbar
    end
end

if param.verbose>=1
    h = waitbar(0,'resolution du graphe ...');
end
t_calc_cout_arc=0;
tres_graph=0;
for i=2:i_max
    if param.verbose>=2
        fprintf(1,'%s %d \n','solve_graph : calcul sur la colonne :',i);
    end
    if param.verbose>=1
        if i == (round(i/20)*20)
            waitbar(i/i_max,h)
        end
    end
    
    if ~isfield(param,'calc_mat') | param.calc_mat==0
        var_com=var_com_inf(i):-param.prec_graphe:var_com_sup(i);
        tdeb_calc_cout_arc=cputime;
        if param.Nb_var_p~=0
            chaine='[ERR,cout';
            for k=1:param.Nb_elseif isfield(param,'var_s')
                chaine='[ERR,cout';
                for k=1:length(param.var_s)
                    chaine=strcat(chaine,',',param.var_s(k));
                end
                chaine=strcat(chaine,',var_mat(:,1:length(var_com),',num2str(k),')');
            end
            chaine=strcat(chaine,']=fonc_cout_arc(ERR,i,var_com');
            for k=1:param.Nb_var_p
                chaine=strcat(chaine,',var_p(:,',num2str(k),')''');
            end
            chaine=strcat(chaine,');');
            eval(chaine)
        else
            [ERR,cout]=fonc_cout_arc(ERR,i,var_com);
        end
        tfin_calc_cout_arc=cputime;
        t_calc_cout_arc=t_calc_cout_arc+tfin_calc_cout_arc-tdeb_calc_cout_arc;
        
        if ~isempty(ERR)
            [poids_min,meilleur_chemin]=backward_erreur;
            return;
        end
    else
        cout=cout_mat(1:length(var_com_mat(:,i)),i)';
    end
    
    poids_p(1:ip_max)=poids(1:ip_max);
    poids(1:ip_max)=Inf;
    imp(1:ip_max)=1;
   
    j_max=lim_sup(i);
    j_min=lim_inf(i);
    j_max_p=lim_sup(i-1);
    j_min_p=lim_inf(i-1);
    k_maxs=k_max(i);
    k_mins=k_min(i);
    
    j_vec=j_min:1:j_max;
    
    %     if nargin==7 | (nargin==9 & reg==1)
    %         ip_inf=max(j_vec-(k_maxs-k_moys),j_min_p);
    %         ip_sup=min(j_vec+k_moys-1,j_max_p);
    %         k_inf=max(k_mins,k_moys-(j_max_p-j_vec));
    %         k_sup=min(k_maxs,k_moys-(j_min_p-j_vec));
    %
    %     elseif nargin ==9 & reg==0
    j_elec=lim_elec(i);
    j_elec_p=lim_elec(i-1);
    di_elec=j_elec-j_elec_p;
    Nb_arc=k_maxs-k_mins+1;
    inc_haut=di_elec+Nb_arc-1;
    ip_sup=min(j_vec-di_elec,j_max_p);
    ip_inf=max(j_vec-inc_haut,j_min_p);
    k_inf=ones(size(j_vec));
    k_inf(j_vec-j_max_p>di_elec)=1+j_vec(j_vec-j_max_p>di_elec)-(j_max_p+di_elec);
    k_sup=k_inf+(ip_sup-ip_inf);
    %    end
    
    poids_mat=Inf*ones(ip_max,k_max(i));
    ip_mat=NaN*ones(ip_max,k_max(i)); % a priori indice (declarable en entier attention NaN??)
    ind_cout_mat=NaN*ones(ip_max,k_max(i));
    
   
    sym=k_sup-k_inf+1;
    for j=1:1:max(sym)
        cond=find(ip_inf<=ip_sup-j+1);
        ind=j_vec;
        ip_mat(ind(cond),j)=ip_inf(cond)+j-1;
        ind_cout_mat(ind(cond),j)=k_sup(cond)-j+1;
    end

    cond=find(~isnan(ip_mat) & ~isnan(ind_cout_mat));
    
    if iscolumn(cout)
        poids_mat(cond)=poids_p(ip_mat(cond))+cout(ind_cout_mat(cond))';
    else
        poids_mat(cond)=poids_p(ip_mat(cond))+cout(ind_cout_mat(cond));
    end
    
    [poids(:),ind_min_cout]=min(poids_mat,[],2);
    
    [SX,~]=size(poids_mat);
    ind_lin=(ind_min_cout(j_min:j_max)'-1)*SX+(j_min:j_max);
    
    imp(j_min:j_max)=ip_mat(ind_lin);
    imp(imp==0)=1;
    
    % mise en "memoire" des meilleurs chemins
    %chemin_opt(j_min:j_max,1:i-1) = chemin_opt(imp(j_min:j_max),1:i-1);
    chemin_opt(j_min:j_max,i-1) = imp(j_min:j_max);

  
    if sum(isnan(poids))+sum(isinf(poids))==length(poids)
        chaine=' all weight are Inf or NaN ! \n i : %d ';
        ERR=MException('BackwardModel:solve_graph_mat',chaine,i);
        if ~isfield(param,'dim') | param.dim==0 % Dans le cas d'un processus de dimensionnement param.dim==1 on ne sort pas en "vrac" on renvoi seulement l'erreur
            [poids_min,meilleur_chemin]=backward_erreur(length(lim_sup),1);
            return
        end
    end
    tres_graph=tres_graph+cputime-tfin_calc_cout_arc;
end

if param.verbose>=1
    close(h); % fermeture waitbar
end
tfin_res_graphe=cputime;
if param.verbose>=1 | param.verbose==-1
    if isfield(param,'calc_mat') & param.calc_mat==1
        fprintf(1,'%s %.2f %s \n','temps de calcul des arcs matriciel :',tfin_calc_cout_arc-tdeb_cal_cout_arc,'s');
        fprintf(1,'%s %.2f %s \n','temps de resolution du graph :',tfin_res_graphe-tfin_calc_cout_arc,'s');
    else
        fprintf(1,'%s %.2f %s\n','temps cumule de calcul du cout des arcs :',t_calc_cout_arc,'s');
        fprintf(1,'%s %.2f %s \n','temps de resolution du graph :',tres_graph,'s');
    end
end
poids_min=poids(lim_inf(end):lim_sup(end));
%meilleur_chemin=chemin_opt((lim_inf(end):lim_sup(end)),:);

meilleur_chemin=ones(2,i_max);
meilleur_chemin(1,i_max)=lim_inf(end);
meilleur_chemin(2,i_max)=lim_sup(end);

for i=i_max-1:-1:1   
   meilleur_chemin(1,i)=chemin_opt(meilleur_chemin(1,i+1),i);
   meilleur_chemin(2,i)=chemin_opt(meilleur_chemin(2,i+1),i);
end


