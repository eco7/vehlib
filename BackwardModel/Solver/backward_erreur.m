% function [arg_out1,arg_out2,arg_out3,arg_out4,arg_out5,arg_out6,arg_out7,arg_out8,arg_out9,arg_out10,...
%          arg_out11,arg_out12,arg_out13,arg_out14,arg_out15,arg_out16,arg_out17,arg_out18,arg_out19,arg_out20]=backward_erreur(n,nscal,coeff)
%
% ?? Copyright IFSTTAR LTE 1999-2011 
% Objet:
% Error management for function and module of the project gestion_energie
% This function allow the necessary outputs arguments of the calling function and fixed
% them to a certain value NaN by default
%
% Input Arguments:
% n : output vectors length
% nscal : numbers of scalar arguments which will be the first output value by defaults :
% coeff : value of the outut arguments by defaults: NaN
%
% Output Arguments:
% [arg_out1, ....] : outputs arguments
%
% Exemple :
% [arg_out1,arg_out2,arg_out3,arg_out4,arg_out5]=backward_erreur(length(VD.CYCL.temps),2,Inf)
% fixe arg_out1,arg_out2 to a scalar = Inf and arg_out3,arg_out4,arg_out5 to a vector Inf of size VD.CYCL.temps
%
% [arg_out1,arg_out2,arg_out3,arg_out4,arg_out5]=backward_erreur(length(VD.CYCL.temps))
% fixe arg_out1...arg_out5 to a vector of NaN
%
% [arg_out1,arg_out2,arg_out3,arg_out4,arg_out5]=backward_erreur
% fixe arg_out1...arg_out5 to a scalar of NaN
% 01/04/2011 (EV)


function [varargout]=backward_erreur(n,nscal,coeff)

if nargin==0
    for ii=1:1:nargout
        varargout{ii}=NaN;
    end
end

if nargin==1 
    nscal=0;
end

if (nargin==1 ||  nargin==2)
    for ii=1:1:nscal
        varargout{ii}=NaN;
    end
    for ii=nscal+1:1:nargout
        varargout{ii}=NaN*ones(1,n);
    end
elseif nargin==3 
    for ii=1:1:nscal
        varargout{ii}=coeff;
    end
    for ii=nscal+1:1:nargout
        varargout{ii}=coeff*ones(1,n);
    end
end
