function [varargout] = declar_var(val, dims)

for k=1:max(nargout,1)
    varargout{k} = val * ones(dims);
end
