function info_graph(VD,param,Dsoc_min,Dsoc_max,lim_sup,lim_inf)

Nb_arc=ceil((Dsoc_min-Dsoc_max)/param.prec_graphe);
fprintf(1,'%s%d\n','edges on the first spread : ',Nb_arc(2));
fprintf(1,'%s%d\n','maximum edge number : ',max(Nb_arc));
fprintf(1,'%s%d\n','mean edge number : ',mean(Nb_arc(2:end-1)));
fprintf(1,'%s%d\n','edges on the latest spreads : ',Nb_arc(end-1));
fprintf(1,'%s%d\n','X size of the graph : ',length(VD.CYCL.temps));
fprintf(1,'%s%d\n','Y size of the graph : ',max(lim_sup-lim_inf));
fprintf(1,'%s%d\n','max value of Y  : ',max(lim_sup));
fprintf(1,'%s%d\n','min value of Y  : ',min(lim_inf));
fprintf(1,'%s%d\n','Approximate number of edge in the graph : ',max(lim_sup-lim_inf)*length(VD.CYCL.temps)*mean(Nb_arc(2:end-1)));
fprintf(1,'%s%d\n','Maximum size of the cost matrix : ',max(Nb_arc));