% function [ERR,Dsoc,elhyb,indice_rap_opt]=chemin2varcomBATT_mat(ERR,fonc_cout_arc,param,lim_sup,lim_inf,lim_elec,var_com_inf,var_com_sup,meilleur_chemin,elec_pos,indice_rap_opt_elec)
%
% C Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
%
% Interface entree :
% ERR :                 gestion des erreurs
%
% fonc_cout arc :       fonction de calcul du cout des arcs ecrit sous forme de handle cf appel dans calc_VELEC_BATT_SC :
%                       @(ERR,pas,i,isc,u0sc_p,ux_p,soc_p,ah_p) calc_cout_arc(ERR,param,pas,pres,isc,u0sc_p,ux_p,soc_p,ah_p,i)
%                       La fonction de calcul de cout des arcs doit calculer les cout de tout les
%                       arcs d'une colonne a partir des var_p de la colonne precedentes et renvoi
%                       donc une matrice de cout de taille (nombre de point max d'une colonne *nombre d'arc max).
%                       Si des arcs sontimpossible on eur affecte un poids infini.
%                       Elle doit contenir dans son inerface (dans un ordre quelconque) ERR, pas, i et une variable d'etat (ici
%                       isc), un nombre de parametre a l'instant precedent(defini dans param.Nb_par_p) et un certain nombre
%                       de donnees ( DCDC ....)
%
% param :               Strucuture de parametres du graphe definis dans param_backward_prog_dyn (precision, nombre de var a stocker a l'instant precedent...)
% lim_sup,lim_inf :     Limites du graphes exprimees en indice
% lim_elec :            Limites tout electrique sans la remonte "permettra de connaitre les arcs tout elec dans le graphes et de leur associe un cout nul
% var_com_inf :         vecteur des valeurs de la variable de commande sur l'arc inferieur de l'eventail (ex: VD.SC.maxcourant si VD.BATT plus VD.SC)
% var_com_sup :         vecteur des valeurs de la variable de commande sur l'arc superieur de l'eventail (ex: -VD.SC.maxcourant si VD.BATT plus VD.SC)
% param :               structure des parametres contenant les pas, la precision du graphe (en pas de la variable de commande), le nombres de
%                       grandeurs a stocke a chaque points et leur valeurs initiales.
% meilleur chemin :     Indices du meilleur chemin 
%
% Interface sortie :
% Dsoc :                Variation de soc sur le meilleur chemin
% elhyb :               mode electrique =0, hybride =1, tout thermique =
%
% 16_12_10 (EV) :       creation


function [ERR,Dsoc,elhyb,indice_rap_opt]=chemin2varcomBATT_mat_2(ERR,fonc_cout_arc,param,lim_sup,lim_inf,lim_elec,var_com_inf,var_com_sup,meilleur_chemin,elec_pos,indice_rap_opt_elec)
% Il faut connaitre taille du graphe, et limite du graphe

%i_max=length(lim_sup);
i_max=length(var_com_sup);

ip_max=max(lim_sup); % indice max des poids dans le graphe
ip_min=min(lim_inf); % indice min des poids dans le graphe

if ip_min~=1
    if param.verbose>=2
    chaine='ip_min ~=1 index shifting \n The best traject will be reference to 1 and not min(lim_inf)' ;
    warning('BackwardModel:chemin2varcomBATT',chaine);    
    end
    lim_sup=lim_sup-ip_min+1;
    lim_inf=lim_inf-ip_min+1;
    ip_min=min(lim_inf);
    ip_max=max(lim_sup);
end

if lim_sup(1)~=lim_inf(1) 
    chaine=strcat(' Les limites a l''origine  sont differentes graphe mal construit');
    ERR=MException('BackwardModel:chemin2varcomBATT',chaine);
end

if lim_sup(end)~=lim_inf(end) &  param.verbose>=2
    chaine='The end limits are differents : \n upper end limit : %f \n lower end limit : %f' ;
    warning('BackwardModel:chemin2varcom',chaine,lim_inf(end),lim_sup(end));
end

% reservation table des variables issus de de la fonction cout
% tableau 3D (indice des poids max) * (nombre d'arc dans les eventails)
% * (nbre variable a conserver a l'instant precedent)
k_min=1*ones(size(var_com_inf));
k_max=floor(abs((var_com_sup-var_com_inf)/param.prec_graphe))+1;


% initialisation (var commande)
var_com=NaN*ones(1,max(k_max-k_min)+1);
indice_rap_opt=-1*ones(size(meilleur_chemin));
indice_rap_opt(1)=1; % point mort au debut du cycle

Dsoc(1)=0;
elhyb(1)=0;


%if ( isfield(param,'tout_thermique') & param.tout_thermique==1 ) ...  % on recherche l'eventuel arc tout thermique
 %        | ( evalin('caller','VD.CYCL.ntypcin==1') & evalin('caller','isfield(VD,''BV'')')  ) % en rapport optimal on revient recalculer le meilleur rapport pour les archi avec boite de vitesse
   
 if ( isfield(param,'tout_thermique') && param.tout_thermique==1  ...
         || ( evalin('caller','VD.CYCL.ntypcin==1') && evalin('caller','isfield(VD,''BV'')') ) )  && ~evalin('caller','isfield(VD,''general'')')
     
     var_com_mat=NaN*ones(floor(max((var_com_inf-var_com_sup)./param.prec_graphe))+1,i_max);
     for i=2:i_max
         var_com=var_com_inf(i):-param.prec_graphe:var_com_sup(i);
         var_com_mat(1:length(var_com),i)=var_com;
     end
     
     if (isfield(param,'tout_thermique') & param.tout_thermique==1)
         [ERR,~,indice_rap_opt_vec,Dsoc_tt]=fonc_cout_arc(ERR,var_com_mat);
     else
         [ERR,~,indice_rap_opt_vec]=fonc_cout_arc(ERR,var_com_mat);
     end
 end

for i=1:i_max  
    if param.verbose>=2
        fprintf(1,'%s %d \n','chemin2varcomBATT : calcul sur la colonne :',i)
    end
    var_com=var_com_inf(i):-param.prec_graphe:var_com_sup(i);
    
    %j=meilleur_chemin(i-1); 
    %di_elec=lim_elec(i)-lim_elec(i-1);
    %ip_elec=di_elec+double(j);

    j=meilleur_chemin(i); 
    di_elec=lim_elec(i+1)-lim_elec(i);
    ip_elec=di_elec+double(j);
    
    if ip_elec==meilleur_chemin(i)
        if nargin == 10 % rapport imposee
            if elec_pos(i)==1
                elhyb(i)=0;
            elseif elec_pos(i)==0
                elhyb(i)=1;
            elseif elec_pos(i)==0.5 % cas "tout elec mais moth tourne sans fournir de couple prius ou limitation vamaxelec par exemple"
                elhyb(i)=0.5;
            end
            Dsoc(i)=var_com(1);
            
        elseif nargin == 11 % raport libre recalcul avant de repasser en CIN imposee
            if size(elec_pos,1)==1
                if elec_pos(i)==1
                    elhyb(i)=0;
                elseif elec_pos(i)==0
                    elhyb(i)=1;
                elseif elec_pos(i)==0.5 % cas "tout elec mais moth tourne sans fournir de couple prius ou limitation vamaxelec par exemple"
                    elhyb(i)=0.5;
                end
            else
                if sum(elec_pos(:,i))>=1
                    elhyb(i)=0;
                else
                    elhyb(i)=1;
                end
            end
            Dsoc(i)=var_com(1);
            indice_rap_opt(i)=indice_rap_opt_elec(i);
            

         end        
    else   
        elhyb(i)=1;
        Dsoc(i)=var_com(meilleur_chemin(i+1)-ip_elec+1); 
%        if (isfield(param,'tout_thermique') & param.tout_thermique==1) ...  % on recherche l'eventuel arc tout thermique
 %         | (evalin('caller','VD.CYCL.ntypcin==1') & evalin('caller','isfield(VD,''BV'')')) % en rapport optimal on revient recalculer le meilleur rapport pour les archi avec boite de vitesse
      if ( isfield(param,'tout_thermique') && param.tout_thermique==1  ...
         || ( evalin('caller','VD.CYCL.ntypcin==1') && evalin('caller','isfield(VD,''BV'')') ) )  && ~evalin('caller','isfield(VD,''general'')')  
            indice_rap_opt(i)=indice_rap_opt_vec(meilleur_chemin(i)-ip_elec+1,i);  % ici indice_rap_opt est un vecteur de taille Dsoc (arcs dans l'eventail),
                                                                                 % on vient prendre l'indice du rapport de boite correspondant au meilleur chemin
                                                                                 % Si on en en rapport impose on recupere -1 a ce nivea
        end
    end
end

if (isfield(param,'tout_thermique') && param.tout_thermique==1) && ~evalin('caller','isfield(VD,''general'')')
    elhyb(elhyb==1 & Dsoc_tt==Dsoc)=2;
end


