  % function [ERR,r] = affect_struct(r_var,r,jj,mode)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Remplir les champ d'une structure avec des noms de variables definis
% dans r_var.
%
% Arguments appel :
% Arguments de sortie :
%
% EV 02/09/15



function [ERR,r] = affect_struct(r_var,r)
ERR=[];

if nargin==1
    for ii=1:length(r_var)
        if evalin('caller',['exist(''',r_var{ii},''',''var'')'])
            r.(r_var{ii})=evalin('caller',r_var{ii});
        else
            chaine='error in affect_struct, non existent variable in caller \n var : %s' ;
            ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine,r_var{ii});
        end
    end
end 

if nargin==2
    for ii=1:length(r_var)
        if ~isfield(r,r_var{ii}) % si la variable existe deja dans r elle n'est pas remplacee
            if evalin('caller',['exist(''',r_var{ii},''',''var'')'])
                r.(r_var{ii})=evalin('caller',r_var{ii});
            else
                chaine='error in affect_struct, non existent variable in caller \n var : %s' ;
                ERR=MException('BackwardModel:calc_HP_BV_2EMB',chaine,r_var{ii});
            end
        end
    end
end 

end

