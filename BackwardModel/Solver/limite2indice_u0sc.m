% function [ lim_inf,lim_sup] = limite2indice_u0sc(u0sc_min,u0sc_max,param)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% calcul des limites sous forme d'indice a partir des soc min et max
% on recherche le point ou le graphe doit remonter, les limites et soc de
% sortie sont donc celles du domaine du graphe (sauf lim_elec)
% 
% interface entre :
% INIT :    structure avec les donnees initiales (dod notamment)
% VD.CYCL :    structure descriptive du cycle
% VD.SC :    parametres batterie
% u0sc_min : soc min (sans remonte)
% u0sc_max : soc max (sans remonte)
% param :   strcuture des parametres de simu (discretisation etc)
%
% interface sortie :
% lim_inf : indice de la limite inferieure du graphe
% lim_sup : indice de la limite superieure du graphe
% 
% 25/02/11 (EV) : Creation

function [ERR, lim_inf,lim_sup] = limite2indice_u0sc(ERR, VD,u0sc_min,u0sc_max,param)

if isfield(param,'prec_graphe_z')
    param.prec_graphe=param.prec_graphe_z;
end

C=VD.SC.C*VD.SC.Nbranchepar/VD.SC.Nblocser;
format long
lim_inf = (u0sc_min-min(u0sc_min)) / (param.prec_graphe*param.pas_temps/C) +1;
lim_sup = (u0sc_max-min(u0sc_min)) / (param.prec_graphe*param.pas_temps/C) +1;
lim_sup = ceil(lim_sup);
lim_inf = ceil(lim_inf);

if lim_sup(1)~=lim_inf(1) | lim_sup(end)~=lim_inf(end) | min(lim_inf)~=1
    fprintf('probleme calcul des limites indices debut et fin')
    pause
end

if param.verbose>=2
    temps=0:param.pas_temps:param.pas_temps*(length(u0sc_min)-1);
  
    figure(10)
    clf
    hold on
    plot(temps,(lim_inf),'k')
    plot(temps,(lim_sup),'k')
    xlabel('temps')
    ylabel('limites u0sc en indices')
    grid
    
    figure(11)
    clf
    hold on
    plot(temps,(u0sc_min),'k')
    plot(temps,(u0sc_max),'k')
    xlabel('temps')
    ylabel('limites u0sc en V')
    grid
end

% NB : recalcul des limites en indices (pr�voir un "etage d'adaptation" ici
% calcul des limites tombe formc�ment sur le graphe, ca pourrrait ne
% plus �tre le cas, de plus si param.du0 ~=0 et pas un nombre entier de pas
% c'est le cas.

