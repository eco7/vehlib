% function [ERR,r] = affect_struct_lagrange(r_var,r,jj,mode)
%
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% Remplir les champ d'une structure avec des noms de variables definis
% dans r_var.
%
% Arguments appel :
% Arguments de sortie :
%
% EV 02/09/15



function [ERR,r] = affect_struct_lagrange(r_var,r,mode,jj,c,i_min)
ERR=[];

if nargin==3
    var=evalin('caller',r_var{ii});
    eval(['r.' r_var{ii} '_' mode  '=' num2str(var,64) ';']);

elseif nargin==4 || (nargin==5 && c~=1)
    
    for ii=1:length(r_var)
        if isfield(r,[r_var{ii} '_' mode])
            %r.(r_var{ii})(jj)=evalin('caller',['r.' r_var{ii} '_' mode '(j);']);
            r.(r_var{ii})(jj)=eval(['r.' r_var{ii} '_' mode '(jj);']);
        else
            r.(r_var{ii})(jj)=NaN;
        end
    end
    
elseif nargin==5 && c==1
    for ii=1:length(r_var)
        var=evalin('caller',[r_var{ii} ';']);
        eval(['r.' r_var{ii} '_' mode '(' num2str(jj) ')' '=' num2str(var,64) ';']);
    end
    
elseif nargin==6
    for ii=1:length(r_var)
        var=evalin('caller',[r_var{ii} '(' num2str(i_min) ');']);
        eval(['r.' r_var{ii} '_' mode '(' num2str(jj) ')' '=' num2str(var,64) ';']);
    end
end

