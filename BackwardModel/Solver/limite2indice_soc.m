%function [ ERR,lim_inf,lim_sup,lim_elec,soc_inf,soc_sup,Dsoc_min,Dsoc_max] = limite2indice_soc( VD,soc_min,soc_max,param,reg,Dsoc_min,Dsoc_max)
%
%  Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% calcul des limites sous forme d'indice a partir des soc min et max
% on recherche le point ou le graphe doit remonter, les limites et soc de
% sortie sont donc celles du domaine du graphe (sauf lim_elec)
% 
% interface entre :
% INIT :    structure avec les donnees initiales (dod notamment)
% VD.CYCL :    structure descriptive du cycle
% VD.BATT :    parametres batterie
% soc_min : soc min (sans remonte), represente exactement le mode tout elec
% soc_max : soc max (sans remonte)
% param :   strcuture des parametres de simu (discretisation etc)
% reg   :   =1 si graphe regulier 0 si irregulier (avec arc electrique)
% Dsoc_min : variation soc entre deux instant correspondant a ibat_max
% Dsoc_max : variation soc entre deux instant correspondant a ibat_min
%
% interface sortie :
% ERR : erreur
% lim_inf : indice de la limite inferieure du graphe
% lim_sup : indice de la limite superieure du graphe
% lim_elec :indice de la limite electrique : ne tiend ps compte de la
% remontee du graphe et indique quelle serait la limite si on faisait tout 
% le graphe en mode tout elec (necessaire pour connaitre les arcs tt elec dans resolution)
% soc_inf : soc inferieure avec remonte (pour info)
% soc_sup : soc superieure avec remonte (pour info)
% Dsoc_min : variation soc entre deux instant correspondant a ibat_max, recalculer pour correspondre a des arcs le cas echeant
% Dsoc_max : variation soc entre deux instant correspondant a ibat_min, recalculer pour correspondre a des arcs le cas echeant
%
% Utilisation :
% nargin =4 : graphe irregulier avec arc electrique inclus
% nargin =5 & reg =1 : graphe regulier avec arc elec non inclus (il faut recalculer les limites pour quelles soit sur des arcs
% nargin =7 : graphe irregulier avec arc elec inclus et recalcul de la limite sup pour qu'elle soit sur le dernier arc valide 
%
% 16_12_10 (EV] : Creation

function [ ERR,lim_inf,lim_sup,lim_elec,soc_inf,soc_sup,Dsoc_min,Dsoc_max] = limite2indice_soc( VD,soc_min,soc_max,param,reg,Dsoc_min,Dsoc_max)
prec_num=1e-10;
soc0=100-VD.INIT.Dod0;
ERR=[];

if isfield(param,'prec_graphe_y')
    param.prec_graphe=param.prec_graphe_y;
end

if soc_max(end)< (soc_max(1)+param.dsoc)
    chaine='final maximum soc < initial maximum soc+Dsoc' ;
    ERR=MException('BackwardModel:limite2indice_soc',chaine);
    [lim_inf,lim_sup,lim_elec,soc_inf,soc_sup,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    return
end

if soc_min(end)> (soc_min(1)+param.dsoc)
    chaine='final minimum soc > initial minimum soc+Dsoc, all electrique do not allow to reach final desired SOC' ;
    ERR=MException('BackwardModel:limite2indice_soc',chaine);
    [lim_inf,lim_sup,lim_elec,soc_inf,soc_sup,Dsoc_min,Dsoc_max]=backward_erreur(length(VD.CYCL.temps),0,0);
    return
end

if param.verbose>=2
    figure(21)
    clf
    hold on
    plot(VD.CYCL.temps,(soc_min),'b')
    plot(VD.CYCL.temps,(soc_max),'b')   
end

if nargin==7 & (isfield(param,'modulo_prec_graph') & param.modulo_prec_graph==1) %on recalcul les limites correpondant exactement a des arcs du graphes
    for ii=2:length(VD.CYCL.temps)
        kdsoc=-floor( -(Dsoc_max(ii)-Dsoc_min(ii)) /param.prec_graphe );
        Dsoc_max(ii)=Dsoc_min(ii)+kdsoc*param.prec_graphe+prec_num; % On rajoute prec num pour eviter d'eliminer l'arc sup dans solve_graphe en contruisant le vecteur var_com
        soc_max(ii)=soc_max(ii-1)+Dsoc_max(ii);
    end
end


Dsoc=-param.prec_graphe;

% recherche des indices de "croissement" des limites
[~,Isoc_min]=min(abs( soc_min - (soc_max-(soc_max(end)-soc0)) - param.dsoc ));
[~,Isoc_max]=min(abs( soc_max - (soc_min-(soc_min(end)-soc0)) - param.dsoc ));

if Isoc_min==1 | Isoc_max==1
    chaine='graph non possible : crossing limits = 1';
    ERR=MException('BackwardModel:limite2indice_soc',chaine);
    [lim_inf,lim_sup,lim_elec,soc_inf,soc_sup]=backward_erreur(length(VD.CYCL.temps));
    return
end

% Il faut etre sur d'avoir l'indice de gauche de l'intersection 
% Est ce encore utile

if soc_min(Isoc_min)-(soc_max(Isoc_min)-(soc_max(end)-soc0)) - param.dsoc <0
    Isoc_min=Isoc_min-1;
end

if soc_max(Isoc_min)-(soc_min(Isoc_min)-(soc_min(end)-soc0)) - param.dsoc <0
    Isoc_max=Isoc_max-1;
end

if nargin==4 |  reg==0
    delta_soc_sup=rem(soc_min(1)-soc_min(end),Dsoc); % rem : reste apres division
    delta_soc_inf=Dsoc-delta_soc_sup;
    
    ind_soc_min=ceil((soc_min-soc0)/Dsoc); % indice soc_min (ceil arrondi vers plus infini)
    ind_soc_max=ceil((soc_max-soc0)/Dsoc); % indice soc_max
    
    ind_soc_min_rf=ceil((soc_min-soc_min(end)+delta_soc_inf+param.dsoc)/Dsoc); % indice soc_min reporte fin
    ind_soc_max_rf=ceil((soc_max-soc_max(end)-delta_soc_sup+param.dsoc)/Dsoc); % indice soc_max reporte fin
    
    lim_inf=[ind_soc_min(1:Isoc_min) ind_soc_max_rf(Isoc_min+1:end)];
    lim_sup=[ind_soc_max(1:Isoc_max) ind_soc_min_rf(Isoc_max+1:end)];
    
    soc_max_rf=soc_max-soc_max(end)-delta_soc_sup+soc0+param.dsoc;
    soc_min_rf=soc_min-soc_min(end)+delta_soc_inf+soc0+param.dsoc;
    soc_inf=[soc_min(1:Isoc_min) soc_max_rf(Isoc_min+1:end)];
    soc_sup=[soc_max(1:Isoc_max) soc_min_rf(Isoc_max+1:end)];
    
    lim_sup=lim_sup+abs(min(lim_inf))+1; % on met le min des limites a 1
    lim_inf=lim_inf+abs(min(lim_inf))+1; % on met le min des limites a 1
    lim_elec=ind_soc_min+abs(min(lim_inf))+1;
 
    
elseif nargin==5 & reg==1
    
    ind_soc_min=floor((soc_min-soc0)/Dsoc); % indice soc_min (floor arrondi vers moins infini)
    ind_soc_max=ceil((soc_max-soc0)/Dsoc); % indice soc_max (floor arrondi vers plus infini)
    
    ind_soc_min_rf=floor((soc_min-soc_min(end)+param.dsoc)/Dsoc); % indice soc_min reporte fin
    ind_soc_max_rf=ceil((soc_max-soc_max(end)+param.dsoc)/Dsoc); % indice soc_max reporte fin
    
    lim_inf=[ind_soc_min(1:Isoc_min) ind_soc_max_rf(Isoc_min+1:end)];
    lim_sup=[ind_soc_max(1:Isoc_max) ind_soc_min_rf(Isoc_max+1:end)];

    
    % Ici si graphe regulier on renvoi les soc_min max recalcules sur les
    % arcs ainsi que les Dsoc min max recalcules aussi.   
    
    lim_sup=lim_sup+abs(min(lim_inf))+1;% on met le min des limites a 1
    lim_inf=lim_inf+abs(min(lim_inf))+1; % on met le min des limites a 1
    soc_sup=soc0+(lim_sup-lim_sup(1))*Dsoc;
    soc_inf=soc0+(lim_inf-lim_inf(1))*Dsoc;
    
    Dsoc_max(1)=0;
    Dsoc_max(2:length(VD.CYCL.temps))=diff(ind_soc_max);
    Dsoc_max=Dsoc_max*Dsoc;
    
    Dsoc_min(1)=0;
    Dsoc_min(2:length(VD.CYCL.temps))=diff(ind_soc_min);
    Dsoc_min=Dsoc_min*Dsoc;
    
    lim_elec=0;  
end
   
%prise en compte des limites sur soc min max

% On verifie que les param.soc_max param.soc_min
% sont coherent avec param.soc.

borne_soc_max=max(param.soc_max,soc0+param.dsoc);
borne_soc_min=min(param.soc_min,soc0+param.dsoc);

% si param.soc_max < soc0 on envoi un warning
if param.soc_max<soc0
    chaine='The value of param.soc_max is lower to the initial soc value /n the upper graph limit will be the initial soc value';
    warning('BackwardModel:limite2indice_soc',chaine);
end

if param.soc_min>soc0
    chaine='The value of param.soc_min is greater to the initial soc value /n the lower graph limit will be the initial soc value';
    warning('BackwardModel:limite2indice_soc',chaine);
end

Isoc_sup=find(soc_sup>=borne_soc_max);
Isoc_inf=find(soc_inf<=borne_soc_min);

if ~isempty(Isoc_sup)
    lim_sup(Isoc_sup)=lim_sup(Isoc_sup(1));
    soc_sup(Isoc_sup)=soc_sup(Isoc_sup(1));
end


if ~isempty(Isoc_inf)
    lim_inf(Isoc_inf)=lim_inf(Isoc_inf(1));
    soc_inf(Isoc_inf)=soc_inf(Isoc_inf(1));
    % on remet les indices a 1
    lim_inf=lim_inf-min(lim_inf)+1;
    lim_sup=lim_sup-(lim_sup(1)-lim_inf(1));
end
lim_elec=lim_elec-2+lim_inf(1);

% il peut arriver sur certain cas que lim_inf(end)=lim_sup(end) quand
% on part avec un soc_ini = a la limite max
% dans ce cas on lui enleve 1
if lim_inf(end)==lim_sup(end)
    lim_inf(end)=lim_inf(end)-1;
end

% Il peut arriver que apres prise en compte des limites 
% le graphe se "croise"
if Isoc_min==1 | Isoc_max==1
    chaine='graph non possible : limits are crossing ';
    ERR=MException('BackwardModel:limite2indice_soc',chaine);
    [lim_inf,lim_sup,lim_elec,soc_inf,soc_sup]=backward_erreur(length(VD.CYCL.temps));
    
    return
end

if param.verbose>=2
    figure(20)
    clf
    hold on
    plot(VD.CYCL.temps,(lim_inf),'b')
    plot(VD.CYCL.temps,(lim_sup),'r')
    legend('lim_inf','lim_sup')
    if lim_elec~=0
    plot(VD.CYCL.temps,(lim_elec),'r')
     legend('lim_inf','lim_sup','lim_elec')
    end
    xlabel('temps')
    ylabel('limites soc en indices')
    grid
    
    figure(21)
    hold on
    plot(VD.CYCL.temps,(soc_inf),'r')
    plot(VD.CYCL.temps,(soc_sup),'r')
    xlabel('temps')
    ylabel('limites soc')
    legend('soc min','soc max','soc inf','soc sup')
    xlabel('temps')
    grid

    assignin('base','lim_inf',lim_inf);
    assignin('base','lim_sup',lim_sup);
    assignin('base','lim_elec',lim_elec);
    assignin('base','soc_inf',soc_inf);
    assignin('base','soc_sup',soc_sup);
end
% Il peut arriver que apres prise en compte des limites 
% le graphe se "croise"
if ~isempty(find(lim_sup<lim_inf))
    chaine='graph non possible : limits are crossing ';
    ERR=MException('BackwardModel:limite2indice_soc',chaine);
    [lim_inf,lim_sup,lim_elec,soc_inf,soc_sup]=backward_erreur(length(VD.CYCL.temps));
    return
end

end

