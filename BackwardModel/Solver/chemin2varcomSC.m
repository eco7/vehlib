% function [ERR,isc]=chemin2varcomVD.SC(chemin,param,var_com_inf)
%
% © Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% fonction pour passer d'un chemin du graphe en indice
% au variable de commande le long de ce chemin
%
% Interface entree :
%
% Interface sortie :
%
% 16_12_10 (EV) :       creation

function [ERR,isc]=chemin2varcomSC(chemin,param,var_com_inf)

ERR=[];
delta_var_com=diff(double(chemin));
delta_var_com(2:length(delta_var_com)+1)=delta_var_com;
delta_var_com(1)=0;

k_min=1*ones(size(var_com_inf));
k_moy=ceil(var_com_inf/param.prec_graphe)+1;
isc=var_com_inf-(k_moy-k_min+delta_var_com)*param.prec_graphe;