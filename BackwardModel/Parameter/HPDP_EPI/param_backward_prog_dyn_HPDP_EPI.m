% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% Copyright IFSTTAR LTE 1999-2011 
%
% Objet: 
% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ?
% 26-08-2011 (EV-BJ) : version 1

% definition pas de temps
param.pas_temps=1;

% Parametre pour debugger
param.verbose=0;

% Type de calcul ( A choisir parmis prog_dyn, lagrange ou ligne)
param.optim='prog_dyn';

% calcul matricielle
param.calc_mat=1;

% calcul des pertes par formule
param.fit_pertes_acm=0;

% parametre tension bus DC
param.Ubus='var';

% Presence modele convertisseur DC/DC
param.conv_dcdc=1;

% Precision graphe
param.prec_graphe=-0.05; % en soc 

param.pas_wmt=50*pi/30; % precision sur la discretisation des vitesses pour le choix de wmt on tombera 
param.pas_cmt=5;        % precision sur la discretisation des couples piur la resolution du systeme (NB on interpole lineairement entre les valeurs).

%param.dsoc=-0.103; % avec param.prec_graphe=-0.01;
param.dsoc=0; % avec param.prec_graphe=-0.05;
% Recherche mode tout thermique
% param.tout_thermique=0;

% limite sur soc min max
param.soc_max=65;
param.soc_min=35;

% Tension Bus DC
param.Ubus='cst'; % var ou cst

% Autorisation du flux serie en freinage (1: autorise, 0: interdit)
param.freinage_flux_serie=1;

% limitation de vit max elec a CALC.vwaxele
param.exist_vit_max_elec=0;

% param pour supposer qu'il existe un embrayage qui coupe le train en tout elec
param.emb=0;

% Initialisation des grandeurs
% param.var(1)=SC.Vc0*SC.Nblocser; % initialisation u0sc
% param.var(2)=100-INIT.Dod0; % initialisation soc
% param.var(3)=0; % initialisation ux
% 
% % nombre de var a conserver a l'instant precedent
param.Nb_var_p=0;