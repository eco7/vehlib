%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% INRETS - LTE Juin 2010
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% définition pas de temps
param.pas_temps=1;

% Parametre pour debugger
param.verbose=1;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='lagrange';

% Convergence dans un intervalle ou calcul pour un parametre de lagrange fixe ?
param.cvgce=0;

% Valeur de lambda
% param.lambda=3.208691;%3.343750; %pour 308 
param.lambda=3.343750; %pour 308 

% tdebug pour donner l'instant ou l'on veut tracer la fonction (0 sinon)
param.tdebug=0;

% mode : 0 groupe tout le temps en marche 1 arret groupe autorise 2 stop start
param.mode=1;