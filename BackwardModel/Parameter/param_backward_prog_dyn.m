%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% INRETS - LTE Juin 2010
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???

% définition pas de temps
param.pas_temps=1;

% Parametre pour debugger
param.verbose=1;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='prog_dyn';

% Precision graphe
param.prec_graphe=5; % en A dans les Ucap

% Variation tension SC entre debut et fin du cycle
param.du0=0;

% Autorisation du flux serie en freinage (1: autorise, 0: interdit)
param.freinage_flux_serie=1;

% Initialisation des grandeurs
param.var(1)=VD.SC.Vc0*VD.SC.Nblocser; % initialisation u0sc
param.var(2)=100-VD.INIT.Dod0; % initialisation soc
param.var(3)=0; % initialisation ux

% nombre de var à conserver à 'linstant précedent
param.Nb_var_p=3;