%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% INRETS - LTE Juin 2010
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???

% définition pas de temps
param.pas_temps=1;

% définition pas courant
param.pas_isc=5;

% Parametre pour debugger
param.verbose=0;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='lagrange';

% Convergence dans un intervalle ou calcul pour un parametre de lagrange fixe ?
param.cvgce=1;

% Valeur du parametre de lagrange
% param.lambda=0.24; % Ibat^2 bus 1branche par ECE15
% param.lambda=0.164; % Ibat^2 bus 5branche par ECE15
% param.lambda=2.95; % Ibat^2 aixam 1branche par 4ECE15
% param.lambda=0.24; % Ibat^2 mbus 1branche ECE15
% param.lambda=0.42; % Ibat^2 mbus 5branche par ECE15 SC.Vco=0.9*SC.maxTensionAdmissible
% param.lambda=0.425; % Ibat^2 mbus 2branche par ECE15 SC.Vco=0.8*SC.maxTensionAdmissible
% param.lambda=0.48; % Ibat^2 mbus 1branche par ECE15 SC.Vco=0.8*SC.maxTensionAdmissible
%param.lambda=0.423; % Ibat^2 mbus 1branche par ECE15 SC.Vco=0.8*SC.maxTensionAdmissible
%param.lambda=0.14;

%param.int_lambda=[0.9 1.2]; % NRJbat
%param.int_lambda=[0.5 1.5]; % NRJbat
%param.int_lambda=[2.5 3]; % rb*ib^2+rsc*isc^2
param.int_lambda=[0.18 0.25]; % Ibat^2
%param.int_lambda=[0.1 0.2]; % Ibat^2 CIN_LAGRANGE

% tdebug pour donner l'instant ou l'on veut tracer la fonction (0 sinon)
param.tdebug=0;

% Precision pour la boucle de convergence
param.precision=1/100;

% nombre de boucle de convergence maxi autorisee
param.nboucle_max=20;

% Autorisation du flux serie en freinage (1: autorise, 0: interdit) 
% non implemente
%param.freinage_flux_serie=1;

