%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% INRETS - LTE Juin 2010
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calcul de l'acceleration (Voir calc_cinemat.m)
param.calc_accel=1;

% définition pas de temps
param.pas_temps=1;

% Optimisation des rapports de boite pour VTH_BV
param.optim_rapport=1;

% Parametre pour debugger
param.verbose=1;
