% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes

% definition pas de temps
param.pas_temps = 1;

% Parametre pour debugger
param.verbose=1;

% Type de minimisation
param.optim='lagrange';

% definition pas couple 
param.pas_cprim2_cpl=0.1;

param.lambda_cst='oui';

% Valeur du parametre de lagrange
param.lambda= 2.89;

