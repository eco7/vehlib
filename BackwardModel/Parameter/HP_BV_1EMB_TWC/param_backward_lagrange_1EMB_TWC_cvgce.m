% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
%
% (C) Copyright IFSTTAR LTE 1999-2011 
%

% definition pas de temps
param.pas_temps=1;

% definition pas couple 
param.pas_cprim2_cpl=1;

% Parametre pour debugger
param.verbose=0;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='lagrange';

% Valeur maxi du hamiltonien lorsqu'aucune soluton n'est envisageable
param.Ham_hors_limite=Inf;

% Valeur du parametre de lagrange 
param.lambda1_cst='oui';
param.lambda2_cst='oui';

% param.K=40;
% param.lambda1=2.410156250000000; 
% param.lambda2=200;

param.K=0;
param.lambda1=3.5; 
param.lambda2=-0.5;

% Parametre de convergence
param.paramCvgce='param.lambda1';

% Fonction et critere de convergence (la structure s'appelle ResXml)
param.fonctionCvgce='soc=XmlValue(ResXml,''soc'');';
param.critereCvgce='soc(end)';


% tdebug pour donner l'instant ou l'on veut tracer la fonction (0 sinon)
%param.tdebug=[35 36];
%param.tdebug=[110 1170];
param.tdebug=0;

