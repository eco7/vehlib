% Fichier parametres

% type d'optimisation
param.optim = '3U_TWC';

param.verbose = 1;

%% parametres de calcul des composants
% type de calcul de la batterie :
%'VEHLIB' : R correspond a la moyenne de Rc et Rd
%'fit' : recalibre R a partir d'un cycle WLTC sur VE
param.res_batt = 'VEHLIB';
% soc pour le calcul des parametres batterie
param.DoDC = 50;
% temperature pour le calcul des parametres batterie
param.tbat = 20;

% Calcul de l'acceleration centree
param.calc_accel = 3;

% parametre de calcul des courbes de Cmin/Cmax pour la machine electrique
param.Ures = 600;

% parametre de calcul de la courbe de Cmin pour le moteur thermique
% 'frot' : courbe de frottement modele DRIVE
% 'nul' : limite de couple a zero : interdire C<0
param.cmt_min = 'frot';

% puissance de la machine electrique
param.Pacm1 = 25000;

%% parametres de la programmation dynamique
% initialisation du calcul de programmation dynamique
param.DoD_init = 50;
param.T_init = 298;
param.cost2go_init = 0;

% parametres de grille de soc
param.pas_soc = 0.02;
param.soc_max = 80;
param.soc_min = 20;

% parametres de grille de temperature
param.pas_T = 0.4;

% parametres de pas de temps
param.pas_temps = 2;


% poids relatif conso-pollu
param.alpha = 5;

% delta de soc a la fin du calcul :
% soc_fin = soc_init + param.delta_soc;
param.delta_soc = 0;

% parametres de la commande
% choisir soit param.phi_cst soit param.phi_min/max/pas
% param.phi_cst = 1;
param.phi_min = 0.99;
param.phi_max = 1.01;
param.pas_phi = 0.005;

% choisir soit param.delta_AA_cst soit param.delta_AA_min/max/pas
% param.delta_AA_cst = 0;
param.delta_AA_min = -30;
param.delta_AA_max = 10;
param.pas_delta_AA = 10;

% parametres de pas de pression admission
param.Padm_min = 1;
param.Padm_max = 1000;
param.pas_Padm = 50;

% debug : enregistrement de l'utilisation memoire 
% et des etats intermediaires pour pouvoir reprendre le calcul
param.debug = 0;

% Optimisation des ecritures au detriment de la lisibilite pour diminuer
% l'usage de la ram (cf graphe...)
param.optiRam = 1;

% Utilisation de csortrows (csortrows.c, algorithme de tri-merge sort- ecrit en langage C) en lieu et place 
% de sortrows pour gain de rapidite et usage ram (cf graphe...)
param.versionC = 1;

% si on souhaite reprendre depuis un fichier d'etat intermediaire
% sinon commenter la ligne
% param.fichier_init = 'loop_3U_NEDC_acm_a1_ds002_dT4_dphi005_dP50_VEHLIB_T2501_B.mat';

% longueur du calcul si on veut effectuer une partie seulement du cycle
% si non precisee (commenter la ligne) : ntps(2)-1 par defaut
% param.lg_calcul = 60;
