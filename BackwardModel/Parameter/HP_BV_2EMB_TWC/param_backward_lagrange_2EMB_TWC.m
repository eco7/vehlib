% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
%
% ?? Copyright IFSTTAR LTE 1999-2011 
%
% Objet:
% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???
% 26-08-2011 (EV) : version 1
%

% definition pas de temps
param.pas_temps=1;

% definition pas courant
param.pas_isc=5;

% definition pas couple 
param.pas_cprim2_cpl=0.1;

% Parametre pour debugger
param.verbose=10;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='lagrange';

% Valeur maxi du hamiltonien lorsqu'aucune soluton n'est envisageable
param.Ham_hors_limite=Inf;

% Convergence dans un intervalle ou calcul pour un parametre de lagrange fixe ?
param.cvgce=0;


param.Ham_hors_limite=Inf;

% Recherche mode tout thermique
param.tout_thermique=0;

% Valeur du parametre de lagrange 
param.lambda1_cst='oui';
param.lambda2_cst='non';
% rap opt sans tout thermqiue
% param.lambda1=2.62; 
% param.lambda2=-10000;


% Quelques valeurs de parametres dominants issues de la publication VPPC
% suivant la penalite sur les polluants (K) pour lambda2 non constant
% param.K=0;
% param.lambda1=2.890930175781250; 
% param.lambda2=0;
% 
% param.K=0.1;
% param.lambda1=3.2931518554687503;   
% param.lambda2=10;
% 
% param.K=0.2;
% param.lambda1=3.740234375000000;   
% param.lambda2=0;
% 
% param.K=3;
% param.lambda1=16.582031250000000;   
% param.lambda2=10;

param.K=5;
param.lambda1=0.014226585626602;   
param.lambda2=-200;

% tdebug pour donner l'instant ou l'on veut tracer la fonction (0 sinon)
%param.tdebug=[35 36];
%param.tdebug=[110 1170];
param.tdebug=0;

