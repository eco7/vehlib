%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% INRETS - LTE Juin 2010
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???

% définition pas de temps
param.pas_temps=1;

% Parametre pour debugger
param.verbose=1;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='ligne';


% Autorisation du flux serie en freinage (1: autorise, 0: interdit) 
% non implemente
%param.freinage_flux_serie=1;

% stratégie en ligne pour gestion batt_sc
% strat=0; % pas de strategie isc =0
% strat=1; % pbat = valeur moyenne de pres sur les Xdernières valeurs
% strat=2; % pbat= filtre passe bas du premier ordre sur pres
% strat=3; % pbat= filtre passe bas du deuxieme ordre sur pres

param.strat=2;

%param.strat=1;
%param.t_moy=50;

%param.strat=2;
param.fc=0.006;
%param.fc=0.06;
%param.strat=3;
%param.fc2=1/100;

%parametrisation des lois de commandes en ligne
% param.pdem_bat    = [0  0 -8000 -8000 ];
% param.dod_pdem_bat= [0  40   80  100  ];
% 
% param.pdem_sc    = [2000 2000 -8000 -8000 ];
% param.dod_pdem_sc     = [0  2   80  100  ];
% 
% param.pdemarrage_hybrid = [32000  32000 2000 2000 ];
% param.dod_pdemarrage_hybrid= [0  20   80  100  ];
% 
% param.parret_hybrid = [8000  8000 0 0 ];
% param.dod_parret_hybrid = [0  20   80  100  ];

param.pdem_bat     = [ 0     0     0   -3200  -6400    -8000   8000];
param.dod_pdem_bat = [ 0    20    40    50    60    80   100 ];

param.pdem_sc    = [0  0 0 0 ];
param.dod_pdem_sc     = [0  40   80  100  ];

param.pdemarrage_hybrid = [ 32000       32000       24000       12000        6000        2000        2000];
param.dod_pdemarrage_hybrid = [0    20    40    50    60    80   100 ];

param.parret_hybrid = [8000  8000 8000 8000 ];
param.dod_parret_hybrid = [0  20   80  100  ];


% Correction éventuel de la psc_cible pour essayer de ramener u0sc
% a la valeur initiale
%  psc_corr(t)=(K_corr*(u0sc(t-1)-u0sc(1)))*(P_pond_batt/pbat_cible(t));
%param.K_corr=100; % si O pas de correction sur psc_cible
param.K_corr=1400;
%param.P_pond_batt=5000; % Puissance de pondération pour la batterie si correction

