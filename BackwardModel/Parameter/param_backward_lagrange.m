%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fichier de parametres pour les calculs en mode backward
% et outil d'optimisation associes
% 
% INRETS - LTE Juin 2010
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% A voir si les fichiers de donnees vehicule et cinematique doivent etre
% repertories ici.
% Empeche de modifier des parametres dans init_backward (mais est-ce
% judicieux ?) A faire dans auto_backward ou dans le fichier de donnees ???

% définition pas de temps
param.pas_temps=1;

% définition pas courant
param.pas_isc=1;

% definition pas couple 
param.pas_cprim2_cpl=0.1;
% Parametre pour debugger
param.verbose=1;

% Type de calcul ( A choisir parmi prog_dyn, lagrange ou ligne)
param.optim='lagrange';

% Convergence dans un intervalle ou calcul pour un parametre de lagrange fixe ?
param.cvgce=0;

% Valeur du parametre de lagrange
param.lambda_cst='oui';

% param.lambda=0.49; % Ibat^2 bus 1branche par ECE15 (lambda fixe)

% param.lambda=0.60; % Bus ellisup pour ECE15 (lambda fixe)

% param.lambda=0.56; % Ibat^2 bus 1branche par ECE15
% param.lambda=0.164; % Ibat^2 bus 5branche par ECE15
% param.lambda=2.95; % Ibat^2 aixam 1branche par 4ECE15
% param.lambda=3.26; % Ibat^2 aixam 3branche par cin_deb_autonomie_filtre lambda variable
% param.lambda=3.21; % Ibat^2 aixam 3branche par cin_deb_autonomie_filtre lambda fixe
% param.lambda=0.24; % Ibat^2 mbus 1branche ECE15
% param.lambda=0.42; % Ibat^2 mbus 5branche par ECE15 SC.Vco=0.9*SC.maxTensionAdmissible
% param.lambda=0.425; % Ibat^2 mbus 2branche par ECE15 SC.Vco=0.8*SC.maxTensionAdmissible

% param.lambda=0.48; % Ibat^2 mbus 1branche par ECE15 SC.Vco=0.8*SC.maxTensionAdmissible
%param.lambda=0.44; % Ibat^2 mbus 1branche par CIN_LAVAL_MOD SC.Vco=0.8*SC.maxTensionAdmissible

% param.lambda=0.48; % Ibat^2 mbus 1branche par ECE15 SC.Vco=0.8*SC.maxTensionAdmissible
% param.lambda=0.423; % Ibat^2 mbus 1branche par ECE15 SC.Vco=0.8*SC.maxTensionAdmissible
% param.lambda=0.48; % Ibat^2 mbus 1branche par ECE15 SC.Vco=0.8*SC.maxTensionAdmissible

%param.lambda=3.17; % NES_NHP plus deux batt exide plus ral moth à MOTH.ral dans calc_emb
param.lambda=3.245; % NES_NHP plus deux batt exide plus ral moth à CALC.wmt_patinage dans calc_emb

%param.int_lambda=[0.9 1.2]; % NRJbat
%param.int_lambda=[0.5 1.5]; % NRJbat
%param.int_lambda=[2.5 3]; % rb*ib^2+rsc*isc^2
%param.int_lambda=[0.18 0.25]; % Ibat^2
%param.int_lambda=[0.1 0.2]; % Ibat^2 CIN_LAGRANGE


%param.lambda=0.49; %bus ElLisup ECE15

%param.lambda=2.3; % aixam 9*2 SC
%param.lambda=2; % eixam 9*2 SC vehlib

%param.lambda=2.96; % NES_NHP plus trois batt 


param.lambda1=3;
param.lambda2=1.2*param.lambda1;

% tdebug pour donner l'instant ou l'on veut tracer la fonction (0 sinon)
param.tdebug=0;

% Precision pour la boucle de convergence
%param.precision=1/100;

% nombre de boucle de convergence maxi autorisee
%param.nboucle_max=20;

% Autorisation du flux serie en freinage (1: autorise, 0: interdit) 
% non implemente
%param.freinage_flux_serie=1;

