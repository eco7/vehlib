function [VEHLIBStruct, comp, tabl] = StructureType4VEHLIB_templates(VEHLIBStruct, comp, tabl)

% % Noms composants et categories utilises dans le projet
% projComp = {'MON_COMP1'
%     'MON_COMP2'};
% 
% projTabl = {'VARIABLES MON COMP 1'
%     'VARIABLES MON COMP 2'};
% 
% % On verifie qu'ils n'existent pas deja (depuis d'autres projets par ex.)
% for ind = 1:length(projComp)
%     if ~strcmp(projComp{ind},comp)
%         comp = [comp; projComp(ind)];
%         tabl = [tabl; projTabl(ind)];
%     end
% end
% 
% % Nom du champ dans la structure vehlib
% comp = [comp;
%     {'mon_comp1'
%     'mon_comp2'}];
% 
% % Nom de la categorie de variables telle qu'apparaissant dans la liste
% % d'explgr
% tabl = [tabl;
%     {'VARIABLES MON COMP 1'
%     'VARIABLES MON COMP 2'}];
% 
% % 'VARIABLES MON COMP 1'
% 
% if ~isnan(tableId('MON_COMP1',VEHLIBStruct,comp,tabl))
%     n=tableId('MON_COMP1',VEHLIBStruct,comp,tabl);
% else
%     n=length(VEHLIBStruct.table)+1;
%     
%     % id et metatable
%     VEHLIBStruct.table{n}.id=num2str(n);
%     
%     VEHLIBStruct.table{n}.metatable=struct;
%     VEHLIBStruct.table{n}.metatable.name='VARIABLES MON COMP 1';
%     VEHLIBStruct.table{n}.metatable.date='date';
%     VEHLIBStruct.table{n}.metatable.sourcefile='source';
%     VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';
% end
% 
% % variables
% VEHLIBStruct.table{n}.nomvar=struct;
% VEHLIBStruct.table{n}.nomvar.condition='true';
% VEHLIBStruct.table{n}.nomvar.name='nomvar';
% VEHLIBStruct.table{n}.nomvar.longname='Nom de ma variable';
% VEHLIBStruct.table{n}.nomvar.type='double';
% VEHLIBStruct.table{n}.nomvar.unit='Unit';
% VEHLIBStruct.table{n}.nomvar.precision='%f';
% VEHLIBStruct.table{n}.nomvar.vector='nomvar';
% 
% % 'VARIABLES MON COMP 2'
% 
% if ~isnan(tableId('MON_COMP2',VEHLIBStruct,comp,tabl))
%     n=tableId('MON_COMP2',VEHLIBStruct,comp,tabl);
% else
%     n=length(VEHLIBStruct.table)+1;
%     
%     % id et metatable
%     VEHLIBStruct.table{n}.id=num2str(n);
%     
%     VEHLIBStruct.table{n}.metatable=struct;
%     VEHLIBStruct.table{n}.metatable.name='VARIABLES MON COMP 2';
%     VEHLIBStruct.table{n}.metatable.date='date';
%     VEHLIBStruct.table{n}.metatable.sourcefile='source';
%     VEHLIBStruct.table{n}.metatable.comments='[''VEHLIB - VERSION : '',VD.INIT.version ]';
% end
% 
% % variables
% VEHLIBStruct.table{n}.nomvar=struct;
% VEHLIBStruct.table{n}.nomvar.condition='true';
% VEHLIBStruct.table{n}.nomvar.name='nomvar';
% VEHLIBStruct.table{n}.nomvar.longname='Nom de ma variable';
% VEHLIBStruct.table{n}.nomvar.type='double';
% VEHLIBStruct.table{n}.nomvar.unit='Unit';
% VEHLIBStruct.table{n}.nomvar.precision='%f';
% VEHLIBStruct.table{n}.nomvar.vector='nomvar';


