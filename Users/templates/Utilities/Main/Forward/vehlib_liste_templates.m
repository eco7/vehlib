% ---------------------------------------------------------
% function [liste_arch, liste_veh, texte]=vehlib_liste_templatesarchitecture,liste_arch,liste_veh,texte)
%
%  Creation de la liste des architectures disponibles (liste_arch),
%                 liste des vehicule repertories par architecture (liste_veh),
%                 d une zone de texte decriavant l architecture (texte).
%  Appel conditionnel suivant l'architecture vehicule (architecture)
%
% ---------------------------------------------------------
function [liste_arch, liste_veh,texte]=vehlib_liste_templates(architecture,liste_arch,liste_veh,texte)

% Architectures non disponibles dans la version public
narch=length(liste_arch);

%res=regexp(liste_arch,['MON_ARCHITECTURE']);
%if isempty(cell2mat(res))
%    narch=narch+1;
%    liste_arch(narch)={'MON_ARCHITECTURE';};
%    new=1;
%else
%    new=0;
%end

% fin de creation de la liste des architectures disponibles dans vehlib

   
% Modeles non disponibles dans la version publique

%if strcmp(architecture,'MON_ARCHITECTURE')
%    if new==1
%        n=1;
%    else
%        n=length(liste_veh)+1;
%    end
%    liste_veh(n)={'MON_VEHICULE1';};       
%    n=n+1;
%    liste_veh(n)={'MON_VEHICULE2';};       
%end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
