% Function: initpath
% Fonction d'initialisation des path de la bibliotheque VEHLIB
%
%  © Copyright IFSTTAR LTE 1999-2017
%
% -------------------------------------------------------------------------
%
% Objet:
% Initialisation des paths matlab pour les modeles
% de simulation de VEHLIB
%
% Le script initialise egalement les path des sous-dossiers utilisateurs
%
% auteur      : IFSTTAR/LTE
% 
% Date de creation : 
% janvier 99
% Major Revision 08/2016
%
% -------------------------------------------------------------------------
function []=initpath()

% if isunix   
%     bdclose all; set_param(0,'CharacterEncoding', 'windows-1252');
% end

% variable path
cdvehlib=pwd;
P=path();

P=path(P,cdvehlib);

P=path(P,fullfile(cdvehlib,''));

vehlibListDir={ 'BackwardModel'
    'BackwardModel/ComponentModel'
    'BackwardModel/Solver'
    'BackwardModel/Vehicle'
    'BackwardModel/Vehicle/HP_BV_1EMB'
    'BackwardModel/Vehicle/HP_BV_2EMB'
    'BackwardModel/Vehicle/HP_BV_2EMB_SC'
    'BackwardModel/Vehicle/HP_BV_2EMB_TWC'
    'BackwardModel/Vehicle/HPDP_EPI'
    'BackwardModel/Vehicle/HPDP_EVT'
    'BackwardModel/Vehicle/HSDP_EPI'
    'BackwardModel/Vehicle/HSER_GE'
    'BackwardModel/Vehicle/HSP_2EMB'
    'BackwardModel/Vehicle/HSP_BV'
    'BackwardModel/Vehicle/VELEC_BAT'
    'BackwardModel/Vehicle/VELEC_BAT_SC'
    'BackwardModel/Vehicle/VTH_BV'
    'BackwardModel/Vehicle/VTH_CPLHYD'
    'BackwardModel/Vehicle/HSP_PLR'
    'BackwardModel/Parameter'
    'BackwardModel/Parameter/HP_BV_1EMB_TWC'
    'BackwardModel/Parameter/HP_BV_2EMB'
    'BackwardModel/Parameter/HP_BV_2EMB_SC'
    'BackwardModel/Parameter/HP_BV_2EMB_TWC'
    'BackwardModel/Parameter/HPDP_EPI'
    'BackwardModel/Parameter/HPDP_EVT'
    'BackwardModel/Parameter/HSDP_EPI'
    'BackwardModel/Parameter/HSER_GE'
    'BackwardModel/Parameter/HSP_2EMB'
    'BackwardModel/Parameter/HSP_BV'
    'Data'
    'Data/Component'
    'Data/Component/Auxiliary'
    'Data/Component/Battery'
    'Data/Component/Chassis'
    'Data/Component/ControlUnit'
    'Data/Component/ControlUnit/NeuralNet'
    'Data/Component/Converter'
    'Data/Component/ElectricMachine'
    'Data/Component/ElectricMachine/conv'
    'Data/Component/ElectricMachine/mas'
    'Data/Component/ElectricMachine/mcc'
    'Data/Component/ElectricMachine/mct'
    'Data/Component/ElectricMachine/msa'
    'Data/Component/ElectricMachine/msb'
    'Data/Component/ElectricMachine/utilitaire'
    'Data/Component/Engine'
    'Data/Component/SuperCapacitor'
    'Data/Component/Transmission'
    'Data/Driver'
    'Data/Road'
    'Data/Road/DebugCycle'
    'Data/Road/mesures'
    'Data/Road/normalise'
    'Data/Road/perfo'
    'Data/Road/trajet'
    'Data/Road/usage_reel'
    'Data/Vehicle'
    'Examples'
    'Examples/GPS'
    'Examples/figures'
    'Examples/figures/figs'
    'ForwardModel'
    'ForwardModel/Sfunction'
    'Results'
    'Utilities'
    'Utilities/Matlab2tex'
    'Utilities/Calculation'
    'Utilities/Ccode'
    'Utilities/ADAS'
    'Utilities/DataManipulation'
    'Utilities/DateTime'
    'Utilities/FileIO'
    'Utilities/GPS'
    'Utilities/Graphic'
    'Utilities/Initial'
    'Utilities/Logging'
    'Utilities/Physics'
    'Utilities/PostTreatment'
    'Utilities/StringManipulation'
    'Utilities/Main'
    'Utilities/Main/Forward'
    'Utilities/Main/Backward'
    'Utilities/XML'
    'Utilities/Comparison_wks_struct'
    'Utilities/TestCase'
    'Utilities/TestCase/Backward'
    'Utilities/TestCase/Forward'
    'Utilities/TestCase/Utilities'
     };

vehlibListDir=cellfun(@(x) fullfile(cdvehlib,x), vehlibListDir,'UniformOutput',false);
cellfun(@(x) addpath(x), vehlibListDir);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluation du script initdata
eval('initdata');

% Recherche de la version de simulink utilisee
% a=ver('matlab');
% if ~strcmp(a.Release,'(R2010b)')
%     msgbox(strcat('Attention, la version ',INIT.version,' de VEHLIB a ete validee avec Matlab R2010b, vous utilisez la release : ',regexp(a.Release,'(\w*)','match')),['VEHLIB ',INIT.version]);
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Recherche systematique des fichiers initpath_XXX.m dans les
% sous repertoires de utilisateurs (repertoire de nom XXX)
% et initialisation des paths utilisateurs
rep=lsFiles(INIT.UsersFolder,'',true, true);
P=path;
for ind=1:length(rep)
    [~,dossier]=fileparts(rep{ind});
    if ~strcmp(dossier,'templates')
        file=strcat('initpath_',dossier,'.m');
        try
            run(fullfile(rep{ind},file));
        catch ME
            %reponse= questdlg(['Probleme a l execution de : ',file,' Continuer tout de meme ?'], ...
            %    'VEHLIB - Script initpath.m','Oui', 'Non','Oui');
            %if strcmp(reponse,'Non')
                %break;
            %end
        end
    end
end
            

