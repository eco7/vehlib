% =============================================================================
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          IFSTTAR - LTE 
%  Objet:   Ce script initialise la structure VEHI
%           contenant les champs necessaires pour renseigner 
%           le composant VEHICULE de la librairie VEHLIB.
%
%           Donnees C0 electrique pour modele vehicule
%           Une partie des donnees provient du rapport du
%           labo allemand fka :
%           [1] : "Analysis of the Functional Properties of a Series Electric Vehicle"
%           dans LTE_VEH/SOURCES_DOCUMENTAIRES/VEHICULES/
%           
% 13/03/13 : Creation (RD)
% =============================================================================

% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=2;

% Nombre de groupes motopropulseurs
VEHI.nbacm1=1;

% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 		% acceleration de la pesanteur
VEHI.Patm     = 101325 ; 	% Pression atmospherique
VEHI.Tatm     = 298.15 ; 	% Temperature atmospherique
VEHI.Ratm     = 287 ; 		% constante de l'air
VEHI.Roatm    = VEHI.Patm/VEHI.Ratm/VEHI.Tatm ; 	% masse volumique de l'air atmospherique

%..................PARAMETRES PNEUMATIQUES
VEHI.Rpneu     = 0.286; 	% Rayon des pneus
VEHI.Nbroue    = 4;      	% nombre de roue
VEHI.Inroue    = 0.;     	% Inertie d une roue en kgm2


% ................PARAMETRES VEHICULE
% Masse du chassis a nu ou caisse en blanc
VEHI.Mveh     = 1110-...	% Poids a vide (PV)
   165-...                  % Masse de la batterie Pack_Citroen_C0 (a priori sans le BMS)
   48;                      % Masse du moteur electrique MCT_MSA_C0_47kW (Source: [1])
VEHI.Charge	= 90; 			% Charge du vehicule en kg
VEHI.Cx    	= 0.33 ; 		% coef. de trainee du vehicule (Source: [1])
VEHI.Sveh  	= 2.137; 		% surface frontale du vehicule en m2 (Source: [1])
VEHI.frmeca	= 2000;         % Force de freinage maxi en N 
VEHI.a      =  0.011;    	% coef de resistance au roulement (-) (Source: [1])
VEHI.b      =  0;         	% coeff de resistance au roulement en v Att : dans vehlib on as VEHI.b *v*v
                            % Il vaut donc mieux utiliser la loi de route qui elle multipliera bien
                            % VEHI.fvit1=Masse*VEHI.Gpes*VEHI.b par v
VEHI.c	=  0;    	     	% coef de resistance au roulement en v**2 ()

% Loi de route determinee lors des essais au banc a rouleaux a partir des
% coeffs de resistance ci-dessus
VEHI.fcst= 129.4;           % Force constante en N
VEHI.fvit1= 0;              % Force fonction de la vitesse
VEHI.fvit2= 4.23e-1;        % Force fonction de la vitesse**2

% Coefficient d adherance
VEHI.adherance=0.6;		

% Transfert de masse dynamique
VEHI.Hcg=0.56;				% Hauteur du centre de gravite vehicule (Source: [1])
VEHI.Empatt=2.55;			% Empattement du vehicule
VEHI.Masse_frac=0.53;		% Fraction de la masse du vehicule sur l essieu avant (Source: [1])
VEHI.pos_acm1=2;            % position moteur (1:avant; 2:arriere)
VEHI.La=(1-VEHI.Masse_frac).*VEHI.Empatt ; % distance essieu avant plan du centre de gravite
VEHI.Lb=VEHI.Empatt-VEHI.La ;              % distance essieu arriere plan du centre de gravite


display('Fin d''initialisation des parametres vehicule');

%------------------------------------MISES A JOUR-----------------------------------%
