% File: VEH_CLIO_1L5DCI
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
%          Valeur pour clio 1.5 DCI utilise au banc a rouleau
%          fiche chrono 430 banc
%
% ============================================================================

% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=2;

% Nombre de groupes motopropulseurs
VEHI.nbacm1=1;

% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur

%..................PARAMETRES PNEUMATIQUES
% pneumatique 175/65 R14
VEHI.dev=1.781;					% developpement des pneumatiques (m);
VEHI.Rpneu    = VEHI.dev/(2*pi);			% Rayon des pneus
VEHI.Nbroue    = 4;             % nombre de roue
VEHI.Inroue    = 0.7;           % Inertie d une roue en kgm2

% ................PARAMETRES VEHICULE
% Masse du chassis a nu ou caisse en blanc :
VEHI.Mveh=1020-...              %masse de reference banc
140-...								%masse du moteur thermique K9K
100;								% charge pour masse de reference du banc
VEHI.Charge	 = 100; 				   % Charge du vehicule en kg
VEHI.fcst=85; 		% Force constante en N
VEHI.fvit1=1.38;	% Force fonction de la vitesse en N/(m/s)
VEHI.fvit2=0.402;	% Force fonction de la vitesse**2 en N/(m/s)2

% Coefficient d adherance
VEHI.adherance=0.6;			

% Transfert de masse dynamique
VEHI.Hcg=0.6;				% Hauteur du centre de gravite vehicule
VEHI.Empatt=2.472;			% Empattement du vehicule
VEHI.Masse_frac=0.64;		% Fraction de la masse du vehicule sur l essieu avant
VEHI.La=(1-VEHI.Masse_frac).*VEHI.Empatt ; % distance essieu avant plan du centre de gravite
VEHI.Lb=VEHI.Empatt-VEHI.La ;              % distance essieu arriere plan du centre de gravite

VEHI.pos_acm1=1; % position moteur (1:avant; 2:arriere)


display('Fin d''initialisation des parametres vehicule'); 

%------------------------------------MISES A JOUR-----------------------------------%
