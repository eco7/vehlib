% File: VEH_308SW_1L6VTI16V
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
%%             Fichier vehicule      VEH_308SW_1L6VTI16V.m
%            donnees pour modele vehicule
%                        
% =============================================================================

% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=1;

% Nombre de groupes motopropulseurs
VEHI.nbacm1=1;

% Position de la machine electrique pour le freinage recuperatif
VEHI.pos_acm1=1;

% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur
VEHI.Patm     = 101325 ; 			% Pression atmospherique
VEHI.Tatm     = 298.15 ; 			% Temperature atmospherique
VEHI.Ratm     = 287 ; 			% constante de l'air
VEHI.Roatm    = VEHI.Patm/VEHI.Ratm/VEHI.Tatm ; 		% masse volumique de l'air atmopherique

%..................PARAMETRES PNEUMATIQUES
VEHI.dev    = 1.928;
VEHI.Rpneu  = VEHI.dev/(2*pi); 			% Rayon des pneus (205/55 R 16)
VEHI.Inroue = 0.7;
VEHI.Nbroue = 4;

% ................PARAMETRES VEHICULE
VEHI.Mveh     = 1330;
VEHI.Charge	  = 0; 			% Charge du vehicule en kg (comme demande dans directive europeenne)
VEHI.Cx       = 1 ; 			
VEHI.Sveh     = 0.728; 			% S*Cx frontale du vehicule (donnees RTech.)
% coef de resistance au roulement (kg/tonne) (origine ???)
VEHI.a	= 0.008;    		% terme constant (kg/tonne)
VEHI.b	= 0;    		% terme en v*v

% Coefficient d adherance
VEHI.adherance=0.6;			

%%%%%%%%%%%%%% CONTACT PNEU-CHAUSSEE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Identification du modele de contact pneu-chaussee
VEHI.ntyppneu=2;

% Transfert de masse dynamique
VEHI.Hcg=0.56;				% Hauteur du centre de gravite vehicule
VEHI.Empatt=2.612;			% Empattement du vehicule
VEHI.Masse_frac=0.568;		% Fraction de la masse du vehicule sur l essieu avant
VEHI.La=(1-VEHI.Masse_frac).*VEHI.Empatt ; % distance essieu avant plan du centre de gravite
VEHI.Lb=VEHI.Empatt-VEHI.La ;              % distance essieu arriere plan du centre de gravite


% Modèle de pacejka 
VEHI.pCx1 = 1.93171;
VEHI.pDx1 = -1.03889;
VEHI.pDx2 = 0.0177702;
VEHI.pEx1 = 0.881667;
VEHI.pEx2 = -0.0906844;
VEHI.pEx3 = 0.101998;
VEHI.pEx4 = -0.000107697;
VEHI.pKx1 = 22.1937;
VEHI.pKx2 = 0.00670573;
VEHI.pKx3 = 0.0272113;
VEHI.pHx1 = 0;
VEHI.pHx2 = 0;
VEHI.pVx1 = 0;
VEHI.pVx2 = 0;

% coefs Fx couplé : 
VEHI.rBx1 = 18.456;
VEHI.rBx2 = 16.314;
VEHI.rCx1 = 1.091;
VEHI.rHx1 = 0.0058715;

VEHI.Fz0 = 3600;
VEHI.Fpz0=VEHI.Fz0;


display('Fin d''initialisation des parametres vehicule');

%------------------------------------MISES A JOUR-----------------------------------%
