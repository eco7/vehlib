% File: VEH_ALFA_ROMEO_159
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
%          Valeur pour "ALFA ROMEO 159 1,9l JTD150"
%           Expert Automobile N° 466 - Nov 2007
%
% ============================================================================

% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=1;

% ................PARAMETRES CONSTANTS
% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur
VEHI.Patm     = 101325 ; 			% Pression atmospherique
VEHI.Tatm     = 298.15 ; 			% Temperature atmospherique
VEHI.Ratm     = 287 ; 			% constante de l'air
VEHI.Roatm    = VEHI.Patm/VEHI.Ratm/VEHI.Tatm ; 		% masse volumique de l'air atmopherique

%..................PARAMETRES PNEUMATIQUES
% pneumatique de serie 215/55 R16 93V
% hauteur du flanc (mm): 215*55/100
% Diametre de la jante (mm): 16*25.4
VEHI.dev=pi.*(2*215*55/100+16*25.4)/1000;	% Developpement theorique des pneumatiques (m);
VEHI.Rpneu= VEHI.dev/(2*pi);	% Rayon des pneus
VEHI.Nbroue= 4;             % Nombre de roue
VEHI.Inroue= 0.7;           % Inertie d une roue en kgm2

% ................PARAMETRES VEHICULE
% Article R312-1
%   Le poids a vide d'un vehicule s'entend du poids du vehicule en ordre de
%   marche comprenant le châssis avec les accumulateurs et le reservoir
%   d'eau rempli, les reservoirs a carburant ou les gazogènes remplis, la
%   carrosserie, les equipements normaux, les roues et les pneus de rechange
%   et l'outillage courant normalement livres avec le vehicule.
VEHI.Mveh=1535-200;        % Masse a vide du vehicule en kg
VEHI.Charge= 75; 	% Charge du vehicule en kg

% En l'absence de donnees, valeur LTE pour un vehicule equivallent (Laguna)
VEHI.Cx       = .32; 			% coef. de trainee du vehicule
VEHI.Sveh     = 2.27; 			% surface frontale du vehicule
VEHI.a	=  0.01;    		% coef de resistance au roulement (S.U.)
VEHI.b	=  0.;    	     	% coef de resistance au roulement en v**2 

% Coefficient d adherance
VEHI.adherance=0.6;			

% Transfert de masse dynamique
VEHI.Hcg=0.6;				% Hauteur du centre de gravite vehicule
VEHI.Empatt=2.7;			% Empattement du vehicule
VEHI.Masse_frac=0.6;		% Fraction de la masse du vehicule sur l essieu avant
VEHI.La=(1-VEHI.Masse_frac).*VEHI.Empatt ; % distance essieu avant plan du centre de gravite
VEHI.Lb=VEHI.Empatt-VEHI.La ;              % distance essieu arriere plan du centre de gravite


display('Fin d''initialisation des parametres vehicule'); 

%------------------------------------MISES A JOUR-----------------------------------%
