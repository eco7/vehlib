% File: VEH_AXELEC
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
%            donnees AX electrique pour modele vehicule
%                        20/11/98
% =============================================================================

% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=1;

% Nombre de groupes motopropulseurs
VEHI.nbacm1=1;

% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur
VEHI.Patm     = 101325 ; 			% Pression atmospherique
VEHI.Tatm     = 298.15 ; 			% Temperature atmospherique
VEHI.Ratm     = 287 ; 			% constante de l'air
VEHI.Roatm    = VEHI.Patm/VEHI.Ratm/VEHI.Tatm ; 		% masse volumique de l'air atmopherique

%..................PARAMETRES PNEUMATIQUES
VEHI.Rpneu     = 0.2556; 			% Rayon des pneus
VEHI.Nbroue    = 4;             % nombre de roue
VEHI.Inroue    = 0.;           % Inertie d une roue en kgm2


% ................PARAMETRES VEHICULE
% Masse du chassis a nu ou caisse en blanc
VEHI.Mveh     = 1160-...		%Poids a vide (PV)
   260-...				%Masse de la batterie Pack_AXELEC
   85;					% Masse du moteur electrique MCT_MCC_SA13
VEHI.Charge	 = 0; 			% Charge du vehicule en kg
VEHI.Cx       = .34 ; 			% coef. de trainee du vehicule
VEHI.Sveh     = 1.74; 			% surface frontale du vehicule
VEHI.frmeca=2000;				% Force de freinage maxi en N 
VEHI.a	=  0.0135;    		% coef de resistance au roulement (kg/tonne)
VEHI.b	=  0.;    	     	% coef de resistance au roulement en v**2 ()


% Coefficient d adherance
VEHI.adherance=0.6;			

% Transfert de masse dynamique
VEHI.Hcg=0.6;				% Hauteur du centre de gravite vehicule
VEHI.Empatt=2.285;			% Empattement du vehicule
VEHI.Masse_frac=0.6;		% Fraction de la masse du vehicule sur l essieu avant
VEHI.La=(1-VEHI.Masse_frac).*VEHI.Empatt ; % distance essieu avant plan du centre de gravite
VEHI.Lb=VEHI.Empatt-VEHI.La ;              % distance essieu arriere plan du centre de gravite


VEHI.pos_acm1=1; % position moteur (1:avant; 2:arriere)


display('Fin d''initialisation des parametres vehicule');


%------------------------------------MISES A JOUR-----------------------------------%

