% File: VEH_PRIUS_II
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
%         Fichier pour simuler le vehicule Prius II
%                    Creation: 04-12-05
% =============================================================================

% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=2;

% Nombre de groupes motopropulseurs
VEHI.nbacm1=1;

% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 		% acceleration de la pesanteur

%..................PARAMETRES PNEUMATIQUES
VEHI.Rpneu    = 0.3; 		% Rayon des pneus
VEHI.Nbroue    = 4;             % nombre de roue
VEHI.Inroue    = 0.;            % Inertie d une roue en kgm2

% ................PARAMETRES VEHICULE
% Masse du chassis a nu ou caisse en blanc :
VEHI.Mveh     = 1360-...	% Poids a vide (PV)+60kg
   60-...			% Charge VEHI.Charge de 100kg pour norme+banc
   100-...			% Masse du moteur MTHERM_PRIUS
   49-...			% Masse du Pack_prius_japon (batterie)
   55-...			% Masse du moteur de traction MCT_MSA_PRIUS
   35;				% Masse de la generatrice GCT_MSA_PRIUS
VEHI.Charge	 =  60; 	% Charge du vehicule en kg
% coeff loi de route
VEHI.fcst=152.1; 		% Force constante en Nm
VEHI.fvit1=0.0;      	        % Force fonction de la vitesse
VEHI.fvit2=0.4095;              % Force fonction de la vitesse**2

% Coefficient d adherance
VEHI.adherance=0.6;

% Transfert de masse dynamique
VEHI.Hcg=0.65;				% Hauteur du centre de gravite vehicule
VEHI.Empatt=2.7;			% Empattement du vehicule
VEHI.Masse_frac=0.53;		% Fraction de la masse du vehicule sur l essieu avant
VEHI.La=1.28 ; % distance essieu avant plan du centre de gravite
VEHI.Lb=1.42 ;              % distance essieu arriere plan du centre de gravite

VEHI.pos_acm1=1; % position moteur (1:avant; 2:arriere)


display('Fin d''initialisation des parametres vehicule'); 

%------------------------------------MISES A JOUR-----------------------------------%
% 04-12-05 : creation a partir de VEH_PRIUS (EV)
% 10-10-06 : Augmentation Pacc de 250 � 350 W pour prendre en compte
% puissance consommee transitoirement par un organe non identifie pour
% l'instant.(cf puissance batterie � l'arr�t vehicule).
