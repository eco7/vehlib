% =============================================================================
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          IFSTTAR - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
%            donnees AIXAM electrique pour modele vehicule
%                        2011
% =============================================================================

% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=2;

% Nombre de groupes motopropulseurs
VEHI.nbacm1=1;

% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur
VEHI.Patm     = 101325 ; 		% Pression atmospherique
VEHI.Tatm     = 298.15 ;        % Temperature atmospherique
VEHI.Ratm     = 287 ; 			% constante de l'air
VEHI.Roatm    = VEHI.Patm/VEHI.Ratm/VEHI.Tatm ; 		% masse volumique de l'air atmopherique

%..................PARAMETRES PNEUMATIQUES
VEHI.Rpneu     = 1700/(2*pi)/1000; 			% Rayon des pneus
VEHI.Nbroue    = 4;             % nombre de roue
VEHI.Inroue    = 0.;           % Inertie d une roue en kgm2


% ................PARAMETRES VEHICULE
% Masse du chassis a nu ou caisse en blanc
VEHI.Mveh     = 750-...		% Poids a vide (PV)
   336-...                  % Masse de la batterie Pack aixam Pb_AGM_etanche_76Ah
   45;                      % Masse du moteur electrique MCT_MCC_SA13
VEHI.Charge	 = 90; 			% Charge du vehicule en kg
VEHI.Cx       = 0.375 ; 	% coef. de trainee du vehicule
VEHI.Sveh     = 1.6; 		% surface frontale du vehicule en m^2
VEHI.frmeca    =2000;		% Force de freinage maxi en N 
VEHI.a	=  0.0136;    		% coef de resistance au roulement (-)
VEHI.b	=  0.08*1e-3;    	% coeff de resistance au roulement en v Att : dans vehlib on as VEHI.b *v*v
                            % Il vaut donc mieux utiliser la loi de route qui elle multipliera bien VEHI.fvit1=Masse*VEHI.Gpes*VEHI.b par v
VEHI.c	=  0;    	     	% coef de resistance au roulement en v**2 ()

% coeff loi de route (calcules a partir de Scx et CRR (attention si masse 
% vehicule change CRR aussi))
Masse_essai=725+60; % masse lors des essais de caracterisation
Masse_simu=725+60;  % equipe des batteries plomb

% Le terme VEHI.a est issue de la caracterisation realisee par JPR lors
% d'essais a vitesse stabilise sur une eCITY. Il represente la partie constante
% du CRR (r0), mais du fait des approximations realises lors de l'identification,
% il comprend egalement la partie constante des pertes dans la transmission
% (t0). Or, les pertes dans la transmission ne dependent pas de la masse.

VEHI.r0 = 0.0082; % regression lineaire sur donnees Michelin a 2 bars (-), pneu 1700mm
VEHI.t0 = 0.0054; % VEHI.a-VEHI.r0; % (-) terme constant pertes transmission

VEHI.fcst= Masse_simu*VEHI.Gpes*VEHI.r0 + Masse_essai*VEHI.Gpes*VEHI.t0;  % Force constante en N
VEHI.fvit1=Masse_simu*VEHI.Gpes*VEHI.b;        	% Force fonction de la vitesse
VEHI.fvit2=0.5*VEHI.Roatm*VEHI.Sveh*VEHI.Cx;	% Force fonction de la vitesse**2
clear Masse_essai Masse_simu

% Coefficient d adherance
VEHI.adherance=0.6;		

% Transfert de masse dynamique
VEHI.Hcg=0.6;				% Hauteur du centre de gravite vehicule
VEHI.Empatt=2.5;			% Empattement du vehicule
VEHI.Masse_frac=0.6;		% Fraction de la masse du vehicule sur l essieu avant
VEHI.pos_acm1=1; % position moteur (1:avant; 2:arriere)
VEHI.La=(1-VEHI.Masse_frac).*VEHI.Empatt ; % distance essieu avant plan du centre de gravite
VEHI.Lb=VEHI.Empatt-VEHI.La ;              % distance essieu arriere plan du centre de gravite


display('Fin d''initialisation des parametres vehicule');


%------------------------------------MISES A JOUR-----------------------------------%
% 08/06/2012 - RD - Modif calcul de la loi de route. Le coeff VEHI.a
% comprenait la partie constante des pertes de la transmission. Separation
% grace aux donnees Michelin du r0 et du t0, afin de prendre en compte
% l'impact du changement de masse sur le CRR. Prendre la masse simu
% adequate.
