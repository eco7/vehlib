% =============================================================================
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
%%             Fichier vehicule      VEH_CLIO_III_1L2Turbo
%      Donnees temporaire Clio III 1L2Turbo
%                     11/07/11
%                        
% =============================================================================

% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=1;

% Nombre de groupes motopropulseurs
VEHI.nbacm1=1;

% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur
VEHI.Patm     = 101325 ; 		% Pression atmospherique
VEHI.Tatm     = 298.15 ; 		% Temperature atmospherique
VEHI.Ratm     = 287 ; 			% constante de l'air
VEHI.Roatm    = VEHI.Patm/VEHI.Ratm/VEHI.Tatm ; 	% masse volumique de l'air atmopherique

%..................PARAMETRES PNEUMATIQUES
VEHI.dev    = 15/8.25;            % developpement mesure 8,25 tours pour 15 metre
VEHI.Rpneu  = VEHI.dev/(2*pi); 	% Rayon des pneus (185/60 R 15)
% VEHI.Rpneu  = 0.3015; 	% Rayon des pneus (185/60 R 15) calcule
VEHI.Inroue = 0.7;
VEHI.Nbroue = 4;

% ................PARAMETRES VEHICULE
% Masse du chassis a nu ou caisse en blanc :
VEHI.Mveh     = 1195-...	%	Poids en ordre de marche (PV) 3p (1145 kg en 5p)
    120;
VEHI.Charge	  = 100; 			% Charge du vehicule en kg (comme demande dans directive europeenne)
VEHI.Cx       = 1 ; 			
VEHI.Sveh     = 0.725; 			% S*Cx frontale du vehicule (donnees site web Renault)
% coef de resistance au roulement (kg/tonne) (origine ???)
VEHI.a	= 0.008;    		% terme constant (kg/tonne)
VEHI.b	= 0;    		% terme en v*v

% Coefficient d adherance
VEHI.adherance=0.6;			

% Transfert de masse dynamique
VEHI.Hcg=0.6;				% Hauteur du centre de gravite vehicule
VEHI.Empatt=2.575;			% Empattement du vehicule
VEHI.Masse_frac=0.61;		% Fraction de la masse du vehicule sur l essieu avant
VEHI.La=(1-VEHI.Masse_frac).*VEHI.Empatt ; % distance essieu avant plan du centre de gravite
VEHI.Lb=VEHI.Empatt-VEHI.La ;              % distance essieu arriere plan du centre de gravite

VEHI.pos_acm1=1; % position moteur (1:avant; 2:arriere)

display('Fin d''initialisation des parametres vehicule');
%------------------------------------MISES A JOUR-----------------------------------%

