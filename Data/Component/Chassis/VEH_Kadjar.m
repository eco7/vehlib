% File: VEH_308SW_1L6VTI16V
% =============================================================================
%
%  C  Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
%%             Fichier vehicule      Kadjar
%              Donnees types sauf 
%              - pneumatiques : revue automobile 2016
%              - SCx : http://www.media-renault.eu/kadjar/files/213_nl_tf_renault_68846_be_nl.pdf
%                        
% =============================================================================

% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=1;

% Nombre de groupes motopropulseurs
VEHI.nbacm1=1;

% Position de la machine electrique pour le freinage recuperatif (1:avant; 2:arriere)
VEHI.pos_acm1=1;

% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur
VEHI.Patm     = 101325 ; 			% Pression atmospherique
VEHI.Tatm     = 298.15 ; 			% Temperature atmospherique
VEHI.Ratm     = 287 ; 			% constante de l'air
VEHI.Roatm    = VEHI.Patm/VEHI.Ratm/VEHI.Tatm ; 		% masse volumique de l'air atmopherique

%..................PARAMETRES PNEUMATIQUES
% pneu : 215/65 R16
largeur = 0.215;                                % m
hauteur = 0.65;                                 % en pourcentage de la largeur
d_jante = 16*25.4e-3;                           % diametre de jante en pouce => m
VEHI.Rpneu  = largeur*hauteur + d_jante/2; 		% rayon des pneus m
VEHI.Inroue = 0.7;                              % kg/m2
VEHI.Nbroue = 4;

% ................PARAMETRES VEHICULE
VEHI.Mveh     = 1310 + 55*0.7 ...  % Masse revue automobile + volume reservoir * densite
                - 140;             % Masse du moteur (arbitraire)
VEHI.Charge	  = 90; 	% Charge du vehicule en kg (comme demande dans directive europeenne)
VEHI.Cx       = 1 ; 			%
VEHI.Sveh     = 0.79; 			% SCx selon http://www.media-renault.eu/kadjar/files/213_nl_tf_renault_68846_be_nl.pdf
% coef de resistance au roulement (kg/tonne) (origine ???)
VEHI.a	= 0.008;    		% terme constant (kg/tonne)
VEHI.b	= 0;                % terme en v*v

% Coefficient d adherance
VEHI.adherance=0.6;			

% Transfert de masse dynamique
VEHI.Hcg=0.6;				% Hauteur du centre de gravite vehicule
VEHI.Empatt=2.65;			% Empattement du vehicule (revue automobile)
VEHI.Masse_frac=0.569;		% Fraction de la masse du vehicule sur l essieu avant
VEHI.La=(1-VEHI.Masse_frac).*VEHI.Empatt ; % distance essieu avant plan du centre de gravite
VEHI.Lb=VEHI.Empatt-VEHI.La ;              % distance essieu arriere plan du centre de gravite


display('Fin d''initialisation des parametres vehicule');

%------------------------------------MISES A JOUR-----------------------------------%
