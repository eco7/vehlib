% File: VxVy_SimpleModel
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
%%             Fichier vehicule      VEH_308SW_1L6VTI16V.m
%            donnees pour modele simple du vitesse longitudinal et lateral
%                        
% =============================================================================

%vehicle parameters
%L=VEHI.Empatt;
L=2.708;
% wheelbase
% d=1/VEHI.Hcg;
d=1.35/(1.35+1.75);                                                         % weight distribution, L*d= center of gravity from front
lf=L*d;                                                                     % length from front to CG
lr=L*(1-d);                                                                 % length from read to CG
ltw= 2.4;                                                                   % track width
%reff=VEHI.Rpneu;                                                                 % tire radius(usually effective tire radius)
reff=1.928/(2*pi);                                                                 % tire radius(usually effective tire radius)

lf=L*d;
lr=L*(1-d);

%environment
mu=1;  
g=9.81;

%structure to identify type of model (calculation of slip)
VXVY.Tsliplong='noslip';
VXVY.Tsliplat='noslip';

VXVY.lf=lf;
VXVY.lr=lr;

VXVY.pitchAngle=1/9.81*5000;%3.3;         % about 3.3 degree for 1g braking

VXVY.Steering_ratio=16.1;%16.1*1.8;                            % steering ratio 
