% File: VEH_TINO
% ============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%                              SOUS-ENSEMBLE VEHICULE
% ============================================================================

VEHI.ntypveh=2;

VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur

% Masse du chassis a nu ou caisse en blanc :
VEHI.Mveh     = 1590-...		Poids a vide (PV)
   175-...						Masse du moteur MTHERM_TINO
   70-...							Masse du Pack_prius_japon (batterie)
   30-...							Masse du moteur de traction MCT_MSA_TINO
   15;								% Masse de la generatrice GCT_MSA_TINO

VEHI.Charge	 = 100; 			% Charge du vehicule en kg (comme indique dans directive europeenne)

VEHI.Rpneu    = 0.3072; 		% Rayon sous charge des pneus

VEHI.fcst=251.6; 				% Force constante en Nm
VEHI.fvit1=-1.543*3.6;			% Force fonction de la vitesse
VEHI.fvit2=0.0534*3.6*3.6;	% Force fonction de la vitesse**2

VEHI.nbacm1=1;					% nombre de acm

VEHI.Pacc=150;					% Puissance electrique des auxiliaires en W.
VEHI.Nbroue    = 4;             % nombre de roue
VEHI.Inroue    = 0.;           % Inertie d une roue en kgm2


VEHI.dev=2*pi*VEHI.Rpneu;		% developpement des pneumatiques (m);

% Coefficient d adherance
VEHI.adherance=0.6;			

% Transfert de masse dynamique
VEHI.Hcg=0.6;				% Hauteur du centre de gravite vehicule
VEHI.Empatt=2.535;			% Empattement du vehicule
VEHI.Masse_frac=0.6;		% Fraction de la masse du vehicule sur l essieu avant
VEHI.La=(1-VEHI.Masse_frac).*VEHI.Empatt ; % distance essieu avant plan du centre de gravite
VEHI.Lb=VEHI.Empatt-VEHI.La ;              % distance essieu arriere plan du centre de gravite

VEHI.pos_acm1=1; % position moteur (1:avant; 2:arriere)

display('Fin d''initialisation des parametres vehicule'); 

%---------------------------------MISES A JOUR----------------------------------------%
% 01/10/2003. MDR. Ajout VEHI.Inroue, VEHI.Pacc et VEHI.Nbroue