% File: VEH_PRIUS_JAPON
% ============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
%                              SOUS-ENSEMBLE VEHICULE
% ============================================================================

% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=2;

% Nombre de groupes motopropulseurs
VEHI.nbacm1=1;

% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur

%..................PARAMETRES PNEUMATIQUES
VEHI.Rpneu    = 0.285; 			% Rayon des pneus
VEHI.Nbroue    = 4;             % nombre de roue
VEHI.Inroue    = 0.;           % Inertie d une roue en kgm2

% ................PARAMETRES VEHICULE
% Masse du chassis a nu ou caisse en blanc :
VEHI.Mveh     = 1360-...		%Poids a vide (PV)+100kg
   100-...						%Charge VEHI.Charge de 100kg pour norme+banc
   100-...						%Masse du moteur MTHERM_PRIUS
   70-...							%Masse du Pack_prius_japon (batterie)
   50-...							%Masse du moteur de traction MCT_MSA_PRIUS
   30;								% Masse de la generatrice GCT_MSA_PRIUS
VEHI.Charge	 = 100; 				   % Charge du vehicule en kg (comme indique dans directive europeenne)
VEHI.fcst=188; 		% Force constante en Nm
VEHI.fvit1=0.32;	% Force fonction de la vitesse
VEHI.fvit2=0.456;	% Force fonction de la vitesse**2

% Coefficient d adherance
VEHI.adherance=0.6;			

% Transfert de masse dynamique
VEHI.Hcg=0.65;				% Hauteur du centre de gravite vehicule
VEHI.Empatt=2.7;			% Empattement du vehicule
VEHI.Masse_frac=0.53;		% Fraction de la masse du vehicule sur l essieu avant
VEHI.La=1.28 ; % distance essieu avant plan du centre de gravite
VEHI.Lb=1.42 ;              % distance essieu arriere plan du centre de gravite

VEHI.pos_acm1=1; % position moteur (1:avant; 2:arriere)

display('Fin d''initialisation des parametres vehicule'); 

%------------------------------------MISES A JOUR-----------------------------------%
%rajout inertie roue RT 17/06/03
