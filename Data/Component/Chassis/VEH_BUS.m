% File: VEH_BUS
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
% =============================================================================


% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=1;

% Nombre de groupes motopropulseurs
VEHI.nbacm1=1;

% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur
VEHI.Patm     = 101325 ; 			% Pression atmospherique
VEHI.Ratm     = 287 ; 			   % constante de l'air
VEHI.Tatm     = 298.15 ; 			% Temperature atmospherique
VEHI.Roatm    = VEHI.Patm/VEHI.Ratm/VEHI.Tatm ; 		% masse volumique de l'air atmopherique

%..................PARAMETRES PNEUMATIQUES
VEHI.Nbroue=6;				% nombre de roue
VEHI.Rpneu     = 0.475; 			% Rayon sous charge des pneus
VEHI.Inroue=12;				% Inertie d'une roue en kgm2

% ................PARAMETRES VEHICULE
% Masse du chassis a nu ou caisse en blanc :
VEHI.Mveh     = 13520-...		Poids a vide (PV)
   1280; 							% Masse du moteur MTHERM_BUS

VEHI.Charge	 = 0; 				% Charge du vehicule en kg
VEHI.Cx       =0.6 ; 			% coef. de trainee du vehicule
VEHI.Sveh     = 7.0; 			% surface frontale du vehicule

VEHI.gainfrein=25;				% Force de freinage
VEHI.a	=  0.0065;    		% coef de resistance au roulement (tonne/tonne)
VEHI.b	=  0.0;    		% coef de resistance au roulement (tonne/tonne)

% Coefficient d adherance
VEHI.adherance=0.6;			

display('Fin d''initialisation des parametres vehicule');

%------------------------------------MISES A JOUR-----------------------------------%
