% File: VEH_308SW_1L6VTI16V
% =============================================================================
%
%  C  Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
%%             Fichier vehicule      VEH_308SW_1L6VTI16V.m
%            donnees pour modele vehicule
%                        
% =============================================================================

% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=1;

% Nombre de groupes motopropulseurs
VEHI.nbacm1=1;

% Position de la machine electrique pour le freinage recuperatif (1:avant; 2:arriere)
VEHI.pos_acm1=1;

% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur
VEHI.Patm     = 101325 ; 			% Pression atmospherique
VEHI.Tatm     = 298.15 ; 			% Temperature atmospherique
VEHI.Ratm     = 287 ; 			% constante de l'air
VEHI.Roatm    = VEHI.Patm/VEHI.Ratm/VEHI.Tatm ; 		% masse volumique de l'air atmopherique

%..................PARAMETRES PNEUMATIQUES
VEHI.dev    = 1.928;
VEHI.Rpneu  = VEHI.dev/(2*pi); 			% Rayon des pneus (205/55 R 16)
VEHI.Inroue = 0.7;
VEHI.Nbroue = 4;

% ................PARAMETRES VEHICULE
VEHI.Mveh     = 1380-...% Masse consideree banc rouleau
    140;                % Masse du moteur MTHERM_EP6
VEHI.Charge	  = 90; 	% Charge du vehicule en kg (comme demande dans directive europeenne)
VEHI.Cx       = 1 ; 			% Cx = 0.314 et SCx = 0.726 d'apres http://www.caradisiac.com/Gabarit-et-poids-a-la-hausse-24096.htm
VEHI.Sveh     = 0.728; 			% S*Cx frontale du vehicule (donnees RTech.)
% coef de resistance au roulement (kg/tonne) (origine ???)
VEHI.a	= 0.008;    		% terme constant (kg/tonne)
VEHI.b	= 0;    		% terme en v*v

% Coefficient d adherance
VEHI.adherance=0.6;			

% Transfert de masse dynamique
VEHI.Hcg=0.6;				% Hauteur du centre de gravite vehicule
VEHI.Empatt=2.708;			% Empattement du vehicule
VEHI.Masse_frac=0.569;		% Fraction de la masse du vehicule sur l essieu avant
VEHI.La=(1-VEHI.Masse_frac).*VEHI.Empatt ; % distance essieu avant plan du centre de gravite
VEHI.Lb=VEHI.Empatt-VEHI.La ;              % distance essieu arriere plan du centre de gravite

% Consommation de reference sur NEDC
VEHI.conso_NEDC = 6.9; %L/100km

display('Fin d''initialisation des parametres vehicule');

%------------------------------------MISES A JOUR-----------------------------------%
