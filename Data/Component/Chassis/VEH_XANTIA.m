% File: VEH_XANTIA
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier vehicule
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure VEHI
%          contenant les champs necessaires pour renseigner 
%          le composant VEHICULE de la librairie VEHLIB.
%
%             Fichier vehicule      VEH_XANTIA.m
%            donnees pour modele vehicule
% 
% ============================================================================

% Identification du type de modele vehicule pour
% calcul des efforts de resistance a l avancement
VEHI.ntypveh=1;

% ................PARAMETRES CONSTANTS
VEHI.Gpes     = 9.81 ; 			% acceleration de la pesanteur

VEHI.Patm     = 101325 ; 			% Pression atmospherique
VEHI.Tatm     = 298.15 ; 			% Temperature atmospherique
VEHI.Ratm     = 287 ; 			% constante de l'air
VEHI.Roatm    = VEHI.Patm/VEHI.Ratm/VEHI.Tatm ; 		% masse volumique de l'air atmopherique

% ................PARAMETRES VEHICULE
% Masse du chassis a nu ou caisse en blanc :
VEHI.Mveh     = 1234-...		Poids a vide (PV)
   241;							% Masse du moteur MTHERM_XU7JP4 (extrapolation)

VEHI.Charge	 = 100; 			% Charge du vehicule en kg (comme demande dans directive europeenne)
VEHI.Cx       = .30 ; 			% coef. de trainee du vehicule
VEHI.Sveh     = 2.0; 			% surface frontale du vehicule

%..................PARAMETRES PNEUMATIQUES
VEHI.Rpneu     = 0.2889; 			% Rayon des pneus
VEHI.Nbroue    = 4;             % nombre de roue
VEHI.Inroue    = 0.71;           % Inertie d une roue en kgm2

% coef de resistance au roulement (kg/kg)
VEHI.a	= 0.01255;    		% terme constant (kg/kg)
VEHI.b	= 2.035e-6;    		% terme en v*v

VEHI.nbacm1=1;				% nombre de acm

%pre calculs
VEHI.dev=2*pi*VEHI.Rpneu;				% developpement des pneumatiques (m);

% Coefficient d adherance
VEHI.adherance=0.6;			

% Transfert de masse dynamique
VEHI.Hcg=0.6;				% Hauteur du centre de gravite vehicule
VEHI.Empatt=2.740;			% Empattement du vehicule
VEHI.Masse_frac=0.6;		% Fraction de la masse du vehicule sur l essieu avant
VEHI.La=(1-VEHI.Masse_frac).*VEHI.Empatt ; % distance essieu avant plan du centre de gravite
VEHI.Lb=VEHI.Empatt-VEHI.La ;              % distance essieu arriere plan du centre de gravite

VEHI.pos_acm1=1; % position moteur (1:avant; 2:arriere)


display('FIN d''initialisation des parametres vehicule');
