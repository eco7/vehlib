% =============================================================================
% 
%                Initialisation du fichier supercondensateur
%                      Pour librairie VEHLIB
%                    INRETS - LTE dec 2005
%          Element "supercondensateur" issu des donn�es constructeur :
%          pack maxwell BMOD2600-48 48V 144 F (assemblage de 18 unite MC2600 2.7V 2600F) 
%          Pour modele LTN
%  Objet: Ce script initialise la structure SC
%          contenant les champs necessaires pour renseigner 
%          le composant supercondensateur de la librairie VEHLIB.
%
% =============================================================================

% Nom de l element supercapacite
SC.Nom_element='SC_MW_2V7_2600F';

% Nombre des condensateurs en serie :
SC.Nblocser=18;
% Nombre des condensateurs en parallel :
SC.Nbranchepar=1;

% Lecture des donnees de l'element super capa
eval(SC.Nom_element);

% ============================================================================
display('Fin d''initialisation des parametres Pack Super capacite'); 



