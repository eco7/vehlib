% =============================================================================
% 
%                Initialisation du fichier supercondensateur
%                      Pour librairie VEHLIB
%                    INRETS - LTN Mars 2004
%          1. Element "supercondensateur" issu des donn�es constructeur
%          et du model LTN
%  Objet: Ce script initialise la structure SC
%          contenant les champs necessaires pour renseigner 
%          le composant supercondensateur de la librairie VEHLIB.
%
% =============================================================================


% Donn�es necessaires au model LTN (Rs, Rx||Cx, C): ------------------
% Capacite principale C(Farad)
SC.C=4500;
% Capacite secondaire Cx(Farad)
SC.Cx=700;
% Resistence serie (Ohm)
SC.Rs=200*1e-6;
% Resistence secondaire (Ohm)
SC.Rx=180*1e-6;

% 
% Donn�es constructeur -------------------------------------
% Rated Voltage Vr(Volts)
SC.Tension=2.7;
% Rated Current Ic(A) (25�C, 5s discharge rate to 0.5Vr)
SC.Courant=964;
% Max Current Icmax(A) (25�C, 1s discharge rate to 0.5Vr)
SC.maxCourant=500;
% Max Leakage Current Lc(A) (12h, 25�C)
SC.fuiteCourant=22*1e-3;
% Masse d'une cellule (kg)
SC.Masse_cell=0.890;

%-------------------------------------------------------------------
% Condition initiale Fonction de transfert element SC
% Tension initiale C(V)
SC.Vc0=0.8*SC.Tension;
% Tension initale Cx(V)
SC.Vx0=0;

% Surge Voltage Vs(Volts)
SC.maxTension=2.7;
% Min Voltage Vs(Volts)
SC.minTension=SC.Tension/2;
% Temperature maxi 
SC.Tsc_max=55;

% Gestion du courant de recuperation admissible par la abtterie
% parametre pour la regul de la tension maxi
SC.tmax_reset=0.5;
SC.P_lim=5000; % 3500/5000 ?
SC.I_lim=500; % 500/1000 ??
SC.D_lim=0;

% ============================================================================
display('Fin d''initialisation des parametres element Super capacite'); 
