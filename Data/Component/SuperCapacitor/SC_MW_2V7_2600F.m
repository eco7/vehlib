% =============================================================================
% 
%                Initialisation du fichier supercondensateur
%                      Pour librairie VEHLIB
%                    INRETS - LTE dec 2005
%          Element "supercondensateur" issu des donn�es constructeur :
%          pack maxwell BMOD2600-48 48V 144 F (assemblage de 18 unite MC2600 2.7V 2600F) 
%          Pour modele LTN
%  Objet: Ce script initialise la structure SC
%          contenant les champs necessaires pour renseigner 
%          le composant supercondensateur de la librairie VEHLIB.
%
% =============================================================================


%Donnees necessaires au model LTN (Rs, Rx||Cx, C): ------------------
% Rated main Capacitance C(Farad)
SC.C=2600;
% Rated second Capacitance Cx(Farad)
SC.Cx=360;
% Serial resistor (Ohm)
SC.Rs=600*1e-6;
% Rated second resistor (Ohm)
SC.Rx=450*1e-6;

%Donn�es constructeur -------------------------------------
% Rated Voltage Vr(Volts)
SC.Tension=2.7;
% Rated Current Ic(A) (25�C, 5s discharge rate to 0.5Vr)
SC.Courant=964;
% Max Current Icmax(A) (25�C, 1s discharge rate to 0.5Vr)
SC.maxCourant=500;
% Max Leakage Current Lc(A) (12h, 25�C)
SC.fuiteCourant=22*1e-3;
% Masse d'une cellule (kg)
SC.Masse_cell=0.750;
% ESR=0.4 mOhms

%-------------------------------------------------------------------
% Condition initiale Fonction de transfert element SC
SC.Vc0=0.8*SC.Tension;
SC.Vx0=0;

% Surge Voltage Vs(Volts)
SC.maxTension=2.7;
% Tension minime autorisee de la batterie des SC
SC.minTension=0.5*SC.Tension;
% Temperature maxi 
SC.Tsc_max=55;

% Gestion du courant de recuperation admissible par la batterie
% parametre pour la regul de la tension maxi
SC.tmax_reset=0.5;
SC.P_lim=1000; % 3500/5000 ?
SC.I_lim=500; % 500/1000 ??
SC.D_lim=0;

% ============================================================================
display('Fin d''initialisation des parametres element Super capacite'); 



