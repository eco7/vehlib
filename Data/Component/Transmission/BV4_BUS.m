% File: BV4_BUS
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier boite de vitesse
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure BV
%          contenant les champs necessaires pour renseigner 
%          le composant BOITE DE VITESSE de la librairie VEHLIB.
%
%      Fichier boite de vitesse automatique ZF 4HP500
%      (Transmission de puissance du V2G)
%      donnees issues du rapport LEN 9405 JPR
%            05/03/01
% =============================================================================

% 3 boite de vitesse
BV.ntyptra=3;

% nombre maxi de rapports
BV.maxrap=4;

BV.rapport_Jbv   =    [0     1    2    3    4];    % moment d'inertie du primaire boite de vitesse
BV.Jprim_bv   =            [0.01  0.   0.   0.   0.];    % moment d'inertie du primaire boite de vitesse


BV.rapport=[1 2 3 4];      % numero du rapport

% Valeurs des rapports de r?duction
BV.k=[2.81 1.84 1.36 1.];      % coeffcient du rapport 


% Valeurs des rendements de base
BV.Ro=[0.940 0.950 0.940 0.95];      % rendement par rapport 

% Pertes minimum sur le secondaire de la boite (barbottage huile, frottement paliers)
BV.Csec_pert=0;

disp('Fin d''initialisation des parametres boite de vitesse\n')

%------------ MISES A JOUR -------------------------------------

