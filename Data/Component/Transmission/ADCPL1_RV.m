% File: ADDCPL_RENDCST_BAVL
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier addition de couple
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure ADCPL
%          contenant les champs necessaires pour renseigner 
%          le composant ADDITION DE COUPLE de la librairie VEHLIB.
%
%      Fichier pour addition de couple
%      conforme a la configuration du banc moteur dynamique LTE
%
%                  25/03/03
% =============================================================================



ADCPL1_R.rapport= [1 2 ];                         % numero du rapport
%ADCPL1_R.kred= [ 1.280 0.951 ]; 

ADCPL1_R.rend = [ 0.980 0.980 ];  
                % numero du rapport
ADCPL1_R.kred= [ 1.280 0.951 ]; 

% Pertes minimum sur le secondaire (barbottage huile, frottement paliers)
ADCPL1_R.Csec_pert=[0 0];




display('Fin d''initialisation des parametres addition de couple');

%------------------------------------MISES A JOUR-----------------------------------%
