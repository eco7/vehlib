% File: RED_PRIUS_II
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure RED
%          contenant les champs necessaires pour renseigner 
%          le composant REDUCTEUR de la librairie VEHLIB.
%
%   Fichier de donnees du reducteur Prius II
%
% 26-09-05 : EV
% -------------
% Calcul rapport de demultiplication � partir du livret (CH3, page 115)
% Nbre de dents pignon de chaine menant : 36 
% Nbre de dents pignon de chaine men� : 35
% Nbre de dents pignon de renvoi menant : 30
% Nbre de dents pignon de renvoi men� : 44
% Nbre de dents pignon d'entrainement menant : 26
% Nbre de dents pignon d'entrainement men� : 75
% rapport demultiplication = (Zmen�/Zmenant) = 4.113
% ============================================================================



RED2.ntyptra=2;

RED2.kred=1;    % rapport de demultiplication total

%RED2.rend=0.98;
RED2.rend=1;

% Pertes minimum sur le secondaire du train (barbottage huile, frottement paliers)
RED2.Csec_pert=0;

display('Fin d''initialisation des parametres reducteur'); 

%------------------------------------MISES A JOUR-----------------------------------%
