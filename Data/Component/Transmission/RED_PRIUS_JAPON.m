% File: RED_PRIUS_JAPON
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure RED
%          contenant les champs necessaires pour renseigner 
%          le composant REDUCTEUR de la librairie VEHLIB.
%
%   Fichier de donnees du reducteur de la prius version japon
%
% =============================================================================


RED.ntyptra=2;

RED.kred=3.927;    % rapport de demultiplication total

RED.rend=0.97;

% Pertes minimum sur le secondaire du train (barbottage huile, frottement paliers)
RED.Csec_pert=0;

display('Fin d''initialisation des parametres reducteur'); 

%------------------------------------MISES A JOUR-----------------------------------%
