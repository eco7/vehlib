% File: BELT2_TYPE
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier courroie
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure BELT2
%          contenant les champs necessaires pour renseigner 
%          le composant COURROIE de la librairie VEHLIB.
%
%      donnees type rendement parfait; pas de glisst
%
% =============================================================================

% premiere prise de couple (par ex. poulie d'alternateur)
BELT2.kred1=2.6;
BELT2.glisst1=0;
BELT2.rend1=1;

% seconde prise de couple (par ex. compresseur de climatisation)
BELT2.kred2=2.6;
BELT2.glisst2=0;
BELT2.rend2=1;

display('Fin d''initialisation des parametres courroie2');

%------------------------------------MISES A JOUR-----------------------------------%
% 15 05 07: (bj) creation modele dans vehlib et donnes associees