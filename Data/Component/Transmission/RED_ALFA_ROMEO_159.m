% File: RED_ALFA_ROMEO_159
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure RED
%          contenant les champs necessaires pour renseigner 
%          le composant REDUCTEUR de la librairie VEHLIB.
%
%      donnees du reducteur de l'ALFA ROMEO 159 avec Boite C544 et moteur
%      1,9l JTD
%
% =============================================================================

% Rapport de reduction differentiel
RED.kred=1/0.2740;
RED.rend=0.97;

RED.Csec_pert=0; % Pertes minimum sur le secondaire du reducteur

display('Fin d''initialisation des parametres reducteur');

%------------------------------------MISES A JOUR-----------------------------------%
% 28 Juillet 2009 : Creation du fichier


