% File: RED_308
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure RED
%          contenant les champs necessaires pour renseigner 
%          le composant REDUCTEUR de la librairie VEHLIB.
%
%   Donnees recueillies dans le catalogue de la revue automobile 2016
%                              (Kadjar)
% =============================================================================

% Identification du type de modele reducteur
RED.ntypred=1;

% Rapport de reduction et rendement differentiel
RED.kred=4.21; %
RED.rend=0.97; % hypothese

RED.Csec_pert=0; % Pertes minimum sur le secondaire du reducteur

display('Fin d''initialisation des parametres reducteur');

%------------------------------------MISES A JOUR-----------------------------------%
