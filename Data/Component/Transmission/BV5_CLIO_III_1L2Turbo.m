% =============================================================================
% 
%                Initialisation du fichier boite de vitesse
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure BV
%          contenant les champs necessaires pour renseigner 
%          le composant BOITE DE VITESSE de la librairie VEHLIB.
%
%      Donnees temporaire Clio III 1L2Turbo
%                     11/07/11
% =============================================================================

% 3 boite de vitesse
BV.ntyptra=3;

BV.rapport=[1 2 3 4 5];                         % numero du rapport
BV.k=[3.73 2.05 1.39 1.03 0.82];           % rapport de reduction
BV.Ro=[0.900 0.970 0.980 0.980 0.980];          % rendement par rapport 

% Pertes minimum sur le secondaire de la boite (barbottage huile, frottement paliers)
BV.Csec_pert=0;

BV.rapport_Jbv= [1 2 3 4 5];      % numero du rapport
BV.Jprim_bv   = [0 0 0 0 0];      % Inertie (kg/m2)  vu sur l'arbre primaire de boite

display('Fin d''initialisation des parametres boite de vitesse');

%------------------------------------MISES A JOUR-----------------------------------%

