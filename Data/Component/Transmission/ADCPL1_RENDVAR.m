% File: ADDCPL_RENDCST_BAVL
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier addition de couple
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure ADCPL
%          contenant les champs necessaires pour renseigner 
%          le composant ADDITION DE COUPLE de la librairie VEHLIB.
%
%      Fichier pour addition de couple
%      conforme a la configuration du banc moteur dynamique LTE
%
%                  25/03/03
% =============================================================================

ADCPL1.kred=1;       % rapport de reduction

%ADCPL1.C_kred=[1e-3 0.9 1 1.1 100 ];
ADCPL1.C_kred=[1e-3 0.7 1 1.3 100 ];
rend=0.98;
ADCPL1.C_rend=[rend rend 1 rend rend];
ADCPL1.rend=interp1(ADCPL1.C_kred,ADCPL1.C_rend,ADCPL1.kred);

%ADCPL1.rend=1;    % rendement du rapport

% Pertes minimum sur le secondaire (barbottage huile, frottement paliers)
ADCPL1.Csec_pert=0;



display('Fin d''initialisation des parametres addition de couple');

%------------------------------------MISES A JOUR-----------------------------------%
