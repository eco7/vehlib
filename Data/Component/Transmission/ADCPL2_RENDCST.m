% File: ADDCPL_RENDCST_BAVL
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier addition de couple
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure ADCPL
%          contenant les champs necessaires pour renseigner 
%          le composant ADDITION DE COUPLE de la librairie VEHLIB.
%
%      Fichier pour addition de couple
%      conforme a la configuration du banc moteur dynamique LTE
%
%                  25/03/03
% =============================================================================

ADCPL2.kred=1;       % rapport de reduction

ADCPL2.rend=0.98;    % rendement du rapport

% Pertes minimum sur le secondaire (barbottage huile, frottement paliers)
ADCPL2.Csec_pert=0;



display('Fin d''initialisation des parametres addition de couple');

%------------------------------------MISES A JOUR-----------------------------------%
