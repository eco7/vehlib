% File: EMB2_TYPE
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier embrayage
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure EMBR2
%          contenant les champs necessaires pour renseigner 
%          le composant EMBRAYAGE de la librairie VEHLIB.
%
%      donnees type pour un embrayage
%
% =============================================================================

EMBR2.mu=0.28;          % Coefficient de friction statique garniture
EMBR2.Remb=0.1;        % Rayon de l'embrayage 


display('FIN d''initialisation des parametres embrayage 2');

%------------------------------------MISES A JOUR-----------------------------------%
