% File: RED_BUS
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure RED
%          contenant les champs necessaires pour renseigner 
%          le composant REDUCTEUR de la librairie VEHLIB.
%
%     Reducteur, descente de mouvement et renvoi d angle "type"
%      (Transmission de puissance du V2G)
%      donnees issues du rapport LEN 9405 JPR
%            05/03/01
%
% =============================================================================

% Rapport de reduction global aval bv
kr=2.0;	  rr=0.98;	%reducteur
kp=2.860; rp=0.985;	%pont
ka=1.039; ra=0.99;	%renvoi d angle

RED.kred=kr*kp*ka;
RED.rend=rr*rp*ra;
clear kr rr kp rp ka ra;

% Pertes minimum sur le secondaire du reducteur (barbottage huile, frottement paliers)
RED.Csec_pert=0;

display('Fin d''initialisation des parametres reducteur');

%------------------------------------MISES A JOUR-----------------------------------%
