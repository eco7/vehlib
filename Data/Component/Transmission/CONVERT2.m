% File: CONVERT2
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier convertisseur de couple
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure CONV
%          contenant les champs necessaires pour renseigner 
%          le composant CONVERTISSEUR DE COUPLE de la librairie VEHLIB.
%
%      Fichier convertisseur de couple CONVERT2.m
%      donnees du convertisseur de couple ZF W380
%      Rc=2.54
%            29/06/2001
% =============================================================================

% Inertie de l etage d entree
CONV.Je=1.1;

% Inertie de l etage de sortie
CONV.Js=0.8;

% Caracteristique du couple d entree (Ce=lambda*Wm^2) 
CONV.rappvit_Ce=[  0     0.2   0.4   0.6   0.8   0.9 1.0];
CONV.lambda    =[380.7 376.6 373.6 333.7 218.4 120.7 0.0]*(30/(pi.*1000))^2;


% Caracteristique du rapport de couple (Cs=mu*Ce)
CONV.rappvit_Cs=[   0  0.1  0.2  0.3  0.4  0.5  0.6  0.7  0.8 0.9];
CONV.mu        =[2.54 2.33 2.07 1.82 1.58 1.37 1.19 1.05 0.94 0.9];
