% File: BV6_ALFA_ROMEO_159
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier boite de vitesse
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure BV
%          contenant les champs necessaires pour renseigner 
%          le composant BOITE DE VITESSE de la librairie VEHLIB.
%
%      donnees de BV 6 rapports de l'Alfa Romeo 159 
%      Boite C544 (M32) Ref. Expert Automobile N466 Novembre 2007
%
% =============================================================================

% 3 boite de vitesse
BV.ntyptra=3;

BV.rapport=[1 2 3 4 5 6];                         % numero du rapport
BV.k=1./[0.2619 0.4634 0.7680 1.0427 1.3441 1.6287];           % rapport de reduction
BV.Ro=  [0.900  0.970  0.980  0.980  0.980  0.980];          % rendement par rapport 

% Pertes minimum sur le secondaire de la boite (barbottage huile, frottement paliers)
BV.Csec_pert=0;

BV.rapport_Jbv= [1 2 3 4 5 6];      % numero du rapport
BV.Jprim_bv   = [0 0 0 0 0 0];      % Inertie (kg/m2)  vu sur l'arbre primaire de boite

display('Fin d''initialisation des parametres boite de vitesse');

%------------------------------------MISES A JOUR-----------------------------------%
% 28 Juillet 2009 (bj): Creation du fichier
