% File: TRAIN_PRIUS_II
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure EPI
%          contenant les champs necessaires pour renseigner 
%          le composant TRAIN EPICYCLOIDAL de la librairie VEHLIB.
%
%   Fichier de donnees du train epicycloidal de la prius II
%
% 26-09-05 : EV
% -------------
% Nbre de dents couronne : 78
% Nbre de dents pignons : 23
% Nbre de dents roue solaire : 30
% Raison du train (Nbre dents couronne)/(Nbre de dents roue solaire)=2.6
% =============================================================================


EPI.ntyptra=2;

EPI.kb	=	-2.6;	   % raison basique principale du train

EPI.rend_train=0.98;       % rendement du train constant

EPI.Nsat=23;          
EPI.Ncour=78;

display('Fin d''initialisation des parametres train epicycloidal'); 
