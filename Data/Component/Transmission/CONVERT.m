% File: CONVERT
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier convertisseur de couple
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure CONV
%          contenant les champs necessaires pour renseigner 
%          le composant CONVERTISSEUR DE COUPLE de la librairie VEHLIB.
%
%      Fichier convertisseur de couple CONVERT.m
%      donnees du convertisseur de couple ZF300
%      Rc=2.15
%            15/02/2001
% =============================================================================


% Inertie de l etage d entree
CONV.Je=1.1;

% Inertie de l etage de sortie
CONV.Js=0.8;

% Caracteristique du couple d entree (Ce=lambda*Wm^2) 
CONV.rappvit_Ce=[   0  0.2  0.4  0.6  0.8  0.9 1.0];
CONV.lambda    =[400. 385. 355. 300. 225. 130. 0.0]*(30/(pi.*1000))^2;


% Caracteristique du rapport de couple (Cs=mu*Ce)
CONV.rappvit_Cs=[   0 0.1  0.2  0.3 0.4 0.5  0.6  0.7 0.8];
CONV.mu        =[2.15 2.0 1.83 1.66 1.5 1.4 1.28 1.14 1.0];
