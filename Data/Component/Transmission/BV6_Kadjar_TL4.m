% File: BV5_308_20DP42
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier boite de vitesse
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure BV
%          contenant les champs necessaires pour renseigner 
%          le composant BOITE DE VITESSE de la librairie VEHLIB.
%
%      Donnees recueillies dans le catalogue de la revue automobile 2016 et
%      document internet (nombre de dents correspondants) : modele Kadjar
%      TL4 indice 000 (sauf pont)
% =============================================================================

% 3 boite de vitesse
BV.ntyptra=3;

BV.rapport=[1 2 3 4 5 6];                         % numero du rapport
BV.k=[41/11 37/19 41/31 39/40 29/38 30/47];       % rapport de reduction
BV.Ro=[0.950 0.950 0.950 0.950 0.950 0.95];       % rendement par rapport (hypothese) 

% Pertes minimum sur le secondaire de la boite (barbottage huile, frottement paliers)
BV.Csec_pert=0;

BV.rapport_Jbv= [1 2 3 4 5 6];      % numero du rapport
BV.Jprim_bv   = [0 0 0 0 0 0];      % Inertie (kg/m2)  vu sur l'arbre primaire de boite

display('Fin d''initialisation des parametres boite de vitesse');

%------------------------------------MISES A JOUR-----------------------------------%

