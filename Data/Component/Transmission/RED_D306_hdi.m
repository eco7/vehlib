% File: RED_D306_hdi
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure RED
%          contenant les champs necessaires pour renseigner 
%          le composant REDUCTEUR de la librairie VEHLIB.
%
%      donnees du reducteur de la 306 hdi
%      Revue Technique 306 HDI 
%            22/06/00
% =============================================================================

% Rapport de reduction differentiel
RED.kred=70/19;
RED.rend=0.97;


% Pertes minimum sur le secondaire du reducteur (barbottage huile, frottement paliers)
RED.Csec_pert=0;


display('Fin d''initialisation des parametres reducteur');

%------------------------------------MISES A JOUR-----------------------------------%
% 10 mai 2007 (bj): rajout d'un champ pour saturer les pertes du reducteur
