% File: RED_D306
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure RED
%          contenant les champs necessaires pour renseigner 
%          le composant REDUCTEUR de la librairie VEHLIB.
%
%         Donnees homogene avec modele SIMULCO de la 306 D 
%                         22/06/00
% =============================================================================

% Identification du type de modele reducteur
RED.ntypred=1;

% Rapport de reduction et rendement differentiel
RED.kred=3.947;
RED.rend=0.97;

RED.Csec_pert=0; % Pertes minimum sur le secondaire du reducteur

display('Fin d''initialisation des parametres reducteur');

%------------------------------------MISES A JOUR-----------------------------------%
% 10 mai 2007 (bj): rajout d'un champ pour saturer les pertes du reducteur
