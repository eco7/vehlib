% File: BELT1_CLIO_1L5DCI
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier courroie
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure BELT1
%          contenant les champs necessaires pour renseigner 
%          le composant COURROIE de la librairie VEHLIB.
%
%      donnees pour la courroie de la CLIO II Phase 2 Diesel (80 ch)
%
% =============================================================================

% premiere prise de couple (par ex. poulie d'alternateur)
BELT1.kred1=49/18;
BELT1.glisst1=0;
BELT1.rend1=1;

% seconde prise de couple (par ex. compresseur de climatisation)
BELT1.kred2=49/42;
BELT1.glisst2=0;
BELT1.rend2=1;

display('Fin d''initialisation des parametres courroie1');

%------------------------------------MISES A JOUR-----------------------------------%
% 15 05 07: (bj) creation modele dans vehlib et donnes associees