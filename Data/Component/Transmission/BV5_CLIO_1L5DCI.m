% File: BV5_CLIO_1L5DCI
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier boite de vitesse
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure BV
%          contenant les champs necessaires pour renseigner 
%          le composant BOITE DE VITESSE de la librairie VEHLIB.
%
%      donnees d'une BV 5 rapports de la CLIO II Phase 2 Diesel (80 ch)
%      Boite JC5 indice 128
%
% =============================================================================

% 3 boite de vitesse
BV.ntyptra=3;

BV.rapport=[1 2 3 4 5];                         % numero du rapport
BV.k=[41/11 43/21 37/28 34/35 31/41];           % rapport de reduction
BV.Ro=[0.900 0.970 0.980 0.980 0.980];          % rendement par rapport 

% Pertes minimum sur le secondaire de la boite (barbottage huile, frottement paliers)
BV.Csec_pert=0;

BV.rapport_Jbv= [1 2 3 4 5];      % numero du rapport
BV.Jprim_bv   = [0 0 0 0 0];      % Inertie (kg/m2)  vu sur l'arbre primaire de boite

display('Fin d''initialisation des parametres boite de vitesse');

%------------------------------------MISES A JOUR-----------------------------------%
% 10 mai 2007 (bj): dissociation reducteur et boite de vitesse dans VEHLIB
%               Rajout d'un champ pour les inerties de la boite
%               Rajout d'un champ pour saturer les pertes de la boite
