% File: TRAIN_PRIUS_JAPON
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure EPI
%          contenant les champs necessaires pour renseigner 
%          le composant TRAIN EPICYCLOIDAL de la librairie VEHLIB.
%
%   Fichier de donnees du train epicycloidal de la prius japon
%
% =============================================================================

EPI.ntyptra=2;

EPI.kb	=	-2.61;	   % raison basique principale du train

EPI.rend_train=0.97;       % rendement du train constant

display('Fin d''initialisation des parametres train epicycloidal'); 

%------------------------------------MISES A JOUR-----------------------------------%
