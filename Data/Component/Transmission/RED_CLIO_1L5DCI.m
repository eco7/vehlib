% File: RED_CLIO_1L5DCI
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure RED
%          contenant les champs necessaires pour renseigner 
%          le composant REDUCTEUR de la librairie VEHLIB.
%
%      donnees du reducteur de la CLIO II Phase 2 Diesel (80 ch)
%      reducteur de la boite JC5 indice 128
%
% =============================================================================

% Rapport de reduction differentiel
RED.kred=56/17;
RED.rend=0.97;

RED.Csec_pert=0; % Pertes minimum sur le secondaire du reducteur

display('Fin d''initialisation des parametres reducteur');

%------------------------------------MISES A JOUR-----------------------------------%
% 10 mai 2007 (bj): rajout d'un champ pour saturer les pertes du reducteur


