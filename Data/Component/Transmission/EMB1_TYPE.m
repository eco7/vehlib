% File: EMB1_TYPE
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier embrayage
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure EMBR1
%          contenant les champs necessaires pour renseigner 
%          le composant EMBRAYAGE de la librairie VEHLIB.
%
%      donnees type pour un embrayage
%
% =============================================================================

EMBR1.mu=0.28;          % Coefficient de friction statique garniture
EMBR1.Remb=0.1;         % Rayon de l'embrayage

display('FIN d''initialisation des parametres embrayage 1');

%------------------------------------MISES A JOUR-----------------------------------%