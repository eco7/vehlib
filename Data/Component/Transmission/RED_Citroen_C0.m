% RED_Citroen_C0
% =============================================================================
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          IFSTTAR - LTE 
%  Objet:  Ce script initialise la structure RED
%          contenant les champs necessaires pour renseigner 
%          le composant REDUCTEUR de la librairie VEHLIB.
%
%   Fichier de donnees du reducteur de la C0 electrique
%
% 12/03/13 Creation (PK & RD)
% =============================================================================


RED.kred=6.2323;	% rapport de reduction

RED.rend=0.96;      % rendement du rapport de reduction (hyp)

% Pertes minimum sur le secondaire du reducteur (barbottage huile, frottement paliers)
RED.Csec_pert=0;

display('Fin d''initialisation des parametres reducteur'); 

%------------------------------------MISES A JOUR-----------------------------------%

