% File: RED_XANTIA
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure RED
%          contenant les champs necessaires pour renseigner 
%          le composant REDUCTEUR de la librairie VEHLIB.
%
%      Fichier boite de vitesse BV5_XANTIA.m
%      donnees BV 5 rapports boite courte
%            03/06/05
% =============================================================================

% Rapport de reduction differentiel
RED.kred=4.053;
RED.rend=0.97;

RED.Csec_pert=0; % Pertes minimum sur le secondaire du reducteur

display('Fin d''initialisation des parametres reducteur');

%------------------------------------MISES A JOUR-----------------------------------%
