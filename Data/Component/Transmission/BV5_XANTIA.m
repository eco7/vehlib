% File: BV5_XANTIA
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier boite de vitesse
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure BV
%          contenant les champs necessaires pour renseigner 
%          le composant BOITE DE VITESSE de la librairie VEHLIB.
%
%      Fichier boite de vitesse BV5_XANTIA.m
%      donnees BV 5 rapports boite courte
%            03/06/05
% =============================================================================

% 3 boite de vitesse
BV.ntyptra=3;

BV.rapport=[1 2 3 4 5];      % numero du rapport
% Valeurs des rapports de r�duction
BV.k=[3.455 1.870 1.360 1.051 0.795];      % coeffcient du rapport 


% Valeurs des rendements de base (d'apr�s modele SIMULCO Roumegoux)
BV.Ro=[0.950 0.970 0.980 0.990 0.980];      % rendement par rapport 

% Pertes minimum sur le secondaire de la boite (barbottage huile, frottement paliers)
BV.Csec_pert=0;

BV.rapport_Jbv= [1 2 3 4 5];      % numero du rapport
BV.Jprim_bv   = [0 0 0 0 0];      % Inertie (kg/m2)  vu sur l'arbre primaire de boite

display('Fin d''initialisation des parametres boite de vitesse');

