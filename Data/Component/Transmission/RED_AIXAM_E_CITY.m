% =============================================================================
% 
%                Initialisation du fichier reducteur
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure RED
%          contenant les champs necessaires pour renseigner 
%          le composant REDUCTEUR de la librairie VEHLIB.
%
%   Fichier de donnees du reducteur AX electrique
%            20/11/98
% =============================================================================


RED.kred=8.46;       % rapport de reduction

RED.rend=0.94;      % rendement du rapport de reduction

% Pertes minimum sur le secondaire du reducteur (barbottage huile, frottement paliers)
RED.Csec_pert=0;

display('Fin d''initialisation des parametres reducteur'); 

%------------------------------------MISES A JOUR-----------------------------------%

