% ECU_Citroen_C0
% =============================================================================
%
%                Initialisation du fichier calculateur
%                      Pour librairie VEHLIB
%                          IFSTTAR - LTE
%  Objet:  Ce script initialise la structure CALC
%          contenant les champs necessaires pour renseigner 
%          le composant Control Unit de la librairie VEHLIB.
%
%  Fichier ECU pour vehicule C0 electrique
% =============================================================================

% Type de strategie
ECU.ntypstrat=4;

% strategie de freinage (1 recup max, 2 stabilite)
ECU.strat_frein=1;

% Freinage elec
ECU.Vit_Cmin = [0   25   59   81    124   163    216   253   293     366.5   412    458   550   625   730   770   795  815];
ECU.Cpl_min  = [0   0   -50  -50   -83.4  -86   -88  -89.3  -91.2   -93.5   -95.9  -97.1 -87.10  -74.30  -64.40  -60.60  -58.30   0];

% Repartition du freinage avant arriere 
% norme par rapport au freinage maxi vehicule
ECU.Frein_entre=[0 1];
ECU.Frein_avant=[1 1];

% filtrage de la demande
ECU.Trep=0.5;

% vitesse maxi de fonct en mode electrique (m/s);
ECU.vmaxele=0;

%Gestion de la charge batterie en mode hybride
ECU.cpcbat_calc=[0   10   20   30   50  80 100];
ECU.pdembat_calc=[0 500 1000 1500 7000 17000 20000]*0.5;

% Couple de demarreur
ECU.cpl_dem=30;

% Saturation en sortie du pid de la gene
ECU.cpl_min_gen=-400;
ECU.cpl_max_gen=0;

%%% gestion du groupe electrog�ne
ECU.pge_opti = [ 0    0.3771    0.7541    1.1312    1.5082    1.8853    2.2623    2.6394    3.0164    3.3935    3.7705 ...
   4.1476    4.5246    4.9017    5.2787    5.6558    6.0328    6.4099    6.7869    7.1640 ]*1.e3;
ECU.wge_opti = [157.0796  198.9757  198.9757  240.8717  282.7677  324.6637  324.6637  366.5597  408.4557  534.1438  576.0398 ...
     617.9358  617.9358  659.8319  701.7279  701.7279  743.6239  785.5199  827.4159  911.2080];

 % Regulation de la vitesse du groupe
ECU.P_reg_ge=1;
ECU.I_reg_ge=0;
ECU.D_reg_ge=0;

ECU.grad_Pdem_ge_mont=5000;
ECU.grad_Pdem_ge_desc=-20000;

%%% gestion hybride

% 1 Puissance variable GE et charge sustaining :
% Puissance de declenchement et d'arret du mot therm
ECU.P_dec_hyb=3250; 
ECU.P_arret_hyb=3050;

% % 2 Puissance constante a fournir (aussi utilise en mode 3)
ECU.Pcst=4000;

% % 3 thermostat seuil en capacite (%) pour declenchement et arret du mot therm
ECU.cpcbat_mar=41;
ECU.cpcbat_arret=40;
% 
% % 4 stop-start :Vitesse vehicule de declenchement et d'arret du mot therm
ECU.Vit_dec_hyb=1.5;
ECU.Vit_arret_hyb=1;
% 
% % 5 DoDcible : mode 1 & 3
%
% 6 ligne
% la conso de carburant sur la courbe de fonctionnement optimale du groupe est exprimm�e en fonction de la puissance electrique
% MOTH.pci.*dcarb=a*Pge^2+b*Pge+c
% dans ce cas l'hamiltonien (H=MOTH.pci.*dcarb+lambda.*Pbat) est minimum pour Pge=(lambda-b)./(2.*a)
ECU.a=1;
ECU.b=0;
ECU.c=0;
ECU.lambda_ini=0;


display('Fin d''initialisation des parametres calculateur'); 

%------------------------------------MISES A JOUR-----------------------------------%
