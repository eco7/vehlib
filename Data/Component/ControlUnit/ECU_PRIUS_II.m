% File: ECU.PRIUS_II
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier calculateur
%                      Pour librairie VEHLIB
%                          INRETS - LTE
%  Objet: Ce script initialise la structure CALC
%          contenant les champs necessaires pour renseigner 
%          le composant ECU.LATEUR de la librairie VEHLIB.
%
%   Fichier pour commande vehicule Prius II
%             version 12/06 des calculs des consignes mg1
%             validation proche de strategie Toyota
%
%
% =============================================================================


ECU.ral_min=90; % dans bloc alpha to couple, utilit� ???? (EV)
ECU.ral_dec=103; % vitesse ralenti pendant les phases de decell en mode hybride
ECU.alpha_dec=0.15; % consigne min moteur thermique pendant les phases de decell en mode hybride
ECU.ral_dem=90; % Ralenti pour consigne en phase demarrage, au dela de cette vitesse on a demarrer le moteur
ECU.ral_max=90; % A priori pas utilis� pour les hybride 

ECU.Auto_rech_arret=1;

% Courbe enveloppe du moteur thermique pour ces pertes (frottement + pompage)
ECU.Nmt_pertes		= [ 0 90 200 300 420];
ECU.Cpertes_mt 	= [-5 -5 -15 -20 -25];

% Puissance de declenchement et d'arret du mot therm
ECU.P_dec_hyb=5800;

ECU.DoD_demarrage = [ 0        20      30       40     50      60     80    100 ];
ECU.P_demarrage =   [ 15000 15000   12000     10000   7000    4000    0  0 ];

ECU.P_arret_hyb=7500;
ECU.pseuil_decel=-50;

%Couple de demarreur
ECU.cpl_dem=30;

%parametres de regulation de la vitesse du GE
%% Avec inertie 
ECU.P_reg_ge=0.8;
ECU.I_reg_ge=0.025;
ECU.D_reg_ge=0;

% gradient pour choix de vitesse cible moth
ECU.grad_wmt_mont=400;
ECU.grad_wmt_desc=-Inf;

ECU.wmaxge=1047;				% vitesse maximum de la generatrice (rd/s);

ECU.wmaxps=11150;   % vitesse maxi des portes satellites pour limitation vitesse moteur thermique

ECU.vmaxele=60/3.6;
ECU.duree_ele=0;
ECU.duree_hyb=5;

% duree minimum de maintient du mode hybride en sec
ECU.delay_stop=3;

% temperature eau seuil bas pour chauffe moteur
ECU.Teau_mth_seuil=40;

%gestion de la dod
ECU.DoD=      [ 0       20    25      30   35       40       45       55        60         80      100 ];
ECU.pdem_bat= [ 5000  5000   5000   5000   1000   -1000    -1000     -2000     -5000      -5000   -10000 ];

% DOD de declenchement et d'arret du mot therm
ECU.DOD_dec_hyb=62;
ECU.DOD_arret_hyb=58;

% Repartition du freinage avant arriere 
% norme par rapport au freinage maxi vehicule
ECU.Frein_entre=[0 1];
ECU.Frein_avant=[1 1];

% param�tres pour le calcul de la consigne survolteur
ECU.surv_Nmotelec=0:400:4800;
ECU.surv_Wmotelec=ECU.surv_Nmotelec/(30/pi);
ECU.surv_Cmotelec=0:40:400;
ECU.surv_Udem_motelec=[ 220.0000  222.4000  224.8000  227.2000  229.6000  262.5000  341.2500  394.3750  449.2857  505.6000  508.0000 509.0000 509.0000
                    220.0000  224.9412  227.8519  233.4622  313.7622  385.3333  468.7769  505.0558  505.2269  505.7000  508.1000 509.0000 509.0000
                    220.0000  226.6364  229.8824  308.3333  410.0000  500.5789  505.2233  505.4303  505.6086  505.8000  508.2000 509.0000 509.0000
                    220.0000  228.1818  275.0000  388.3333  495.2222  505.3083  505.5852  505.8049  505.9904  506.1746  508.3000 509.0000 509.0000
                    220.0000  229.4857  345.0000  466.5000  505.2364  505.5909  505.9174  506.1675  506.3705  506.5492  508.4000 509.0000 509.0000
                    220.0000  247.3333  412.0000  505.0000  505.4394  505.8636  506.2182  506.5266  506.7451  506.9238  508.5000 509.0000 509.0000
                    220.0000  307.1429  474.2222  505.1848  505.6242  506.0637  506.4909  506.8455  507.1117  507.3043  508.6000 509.0000 509.0000
                    220.0000  357.6744  505.0000  505.3696  505.8090  506.2484  506.6879  507.1182  507.4587  507.6853  508.7000 509.0000 509.0000
                    220.0000  409.7674  505.1149  505.5544  505.9938  506.4332  506.8727  507.3121  507.7455  508.0558  508.8000 509.0000 509.0000
                    220.0000  465.7104  505.2997  505.7391  506.1786  506.6180  507.0575  507.4969  507.9363  508.3727  508.9000 509.0000 509.0000
                    220.0000  505.0450  505.4845  505.9239  506.3634  506.8028  507.2422  507.6817  508.1211  508.5606  509.0000 509.0000 509.0000];

ECU.surv_Ngene=0:840:10080;
ECU.surv_Wgene=ECU.surv_Ngene/(30/pi);
ECU.surv_Cgene=0:13.28:132.8;
ECU.surv_Udem_gene =[ 220.0000  222.4000  224.8000  227.2000  229.6000  262.5000  341.2500  394.3750  449.2857  505.6000  508.0000 509.0000 509.0000 
                  220.0000  224.9412  227.8519  233.4622  313.7622  385.3333  468.7769  505.0558  505.2269  505.7000  508.1000 509.0000 509.0000 
                  220.0000  226.6364  229.8824  308.3333  410.0000  500.5789  505.2233  505.4303  505.6086  505.8000  508.2000 509.0000 509.0000 
                  220.0000  228.1818  275.0000  388.3333  495.2222  505.3083  505.5852  505.8049  505.9904  506.1746  508.3000 509.0000 509.0000 
                  220.0000  229.4857  345.0000  466.5000  505.2364  505.5909  505.9174  506.1675  506.3705  506.5492  508.4000 509.0000 509.0000 
                  220.0000  247.3333  412.0000  505.0000  505.4394  505.8636  506.2182  506.5266  506.7451  506.9238  508.5000 509.0000 509.0000 
                  220.0000  307.1429  474.2222  505.1848  505.6242  506.0637  506.4909  506.8455  507.1117  507.3043  508.6000 509.0000 509.0000 
                  220.0000  357.6744  505.0000  505.3696  505.8090  506.2484  506.6879  507.1182  507.4587  507.6853  508.7000 509.0000 509.0000 
                  220.0000  409.7674  505.1149  505.5544  505.9938  506.4332  506.8727  507.3121  507.7455  508.0558  508.8000 509.0000 509.0000 
                  220.0000  465.7104  505.2997  505.7391  506.1786  506.6180  507.0575  507.4969  507.9363  508.3727  508.9000 509.0000 509.0000 
                  220.0000  505.0450  505.4845  505.9239  506.3634  506.8028  507.2422  507.6817  508.1211  508.5606  509.0000 509.0000 509.0000];


display('Fin d''initialisation des parametres calculateur'); 

%------------------------------------MISES A JOUR-----------------------------------%
