% =============================================================================
%
%                Initialisation du fichier calculateur
%                      Pour librairie VEHLIB
%                          INRETS - LTE
%  Objet: Ce script initialise la structure ECU.
%          contenant les champs necessaires pour renseigner 
%          le composant ECU.LATEUR de la librairie VEHLIB.
%
%  Fichier calculateur pour vehicule AX electrique
% =============================================================================


ECU.Vit_Cmin=[0  800  850   1165 7000]*pi/30;
ECU.Cpl_min= [-1 -1 -1 -1 -1]*10;
%ECU.Cpl_min=[0    0  0    0 0];
% Repartition du freinage avant arriere 
% norme par rapport au freinage maxi vehicule
ECU.Frein_entre=[0 1];
ECU.Frein_avant=[1 1];

ECU.ntypstrat=4;
ECU.tarret=20;
ECU.taux=200;

% Strategie Lagrange embedded
% Pas de calcul
ECU.pas_temps=0.01;
% discretisation en courant
ECU.pas_isc=1;
% Valeur initiale de lambda
ECU.lambda_ini=1.75;

display('Fin d''initialisation des parametres calculateur'); 

%------------------------------------MISES A JOUR-----------------------------------%
