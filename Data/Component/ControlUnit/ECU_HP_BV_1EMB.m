% File: ECU.HP_BV_1EMB
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier calculateur
%                      Pour librairie VEHLIB
%                          INRETS - LTE
%  Objet: Ce script initialise la structure CALC
%          contenant les champs necessaires pour renseigner 
%          le composant ECU.LATEUR de la librairie VEHLIB.
%
%	Calculateur pour veh. Hybride Parallele Simple Arbre 
%	avec Boite Manuelle et 1 seul embrayage place apres le moteur electrique
%
%                                25/11/2002
% =============================================================================


% Changement de rapports de boite automatique
% Montee des rapports
ECU.wmt_chrap_M=[2000 2500 3000 3500 3550]*pi/30;
ECU.ouvert_chrap_M=[0. 0.2 0.4 0.6 1];
% Descente des rapports
ECU.wmt_chrap_D=[950 1000 2000 2050]*pi/30;
ECU.ouvert_chrap_D=[0. 0.2 0.8 1];

ECU.ral_dem    = 900*pi/30;	% Regime de fin d'action du demarreur

ECU.cpl_dem=50;		% Couple d action du demarreur

% nombre maximum de rapport de boite
ECU.nbmax_rapbv=5;

% Duree totale d un changement de rapport
ECU.chrapp_duree=1.2;

% D?lai de passage d un rapport (action sur pedales puis passage vitesse)
ECU.rapp_delay=0.5;

% gradient sur le couple mg1 dans calculateur
ECU.grd_cmg1_mont=70;
ECU.grd_cmg1_desc=-7000;

% Action sur la pedale d embrayage lors d'un chgt de rapport
ECU.emb_temps= [0 .4 0.6 1.2 1.5];
ECU.emb_course=[0. 1 1   0.  0  ];

% Correction sur la pedale d accelerateur lors d'un chgt de rapport
ECU.acc_temps=[0  .2  0.4 0.6  1    1.2 1.5];
ECU.acc_corr= [1. 0.5 0.2 0.3  0.3  1   1  ];

% Valeurs en dessous desquels on fait patiner l'embrayage
ECU.wmt_patinage=120; % vitesse de regulation pour le patinage
%ECU.wemb_patinage=100; % condition de vitesse pour enclencher la regulation de patinage

% Ouverture papillon maximum du conducteur
ECU.alphamot_max=1;

% Definition des conditions pour fonctionnement en tout electrique
ECU.vmaxele=eps;		% vitesse maxi du vehicule en m/s.

% temperature eau seuil bas pour chauffe moteur
ECU.Teau_mth_seuil=80;

%gestion de la dod
ECU.DoD=[     0 20 40   50     60    80   100];
ECU.pdem_bat=[0  0  0 -2000 -4000 -5000 -5000];

% Pdem bat de declenchement et d'arret du mot therm
ECU.Pbat_dec_hyb=-2000;
ECU.Pbat_arret_hyb=-1000;

%interdiction boost en fonction de la dod
ECU.Dod_boost=[0 20 40 50 60 80 100];
ECU.Auto_boost=[1 1 1 1 0 0 0 ];

% pour favoriser le boost a dod faible
ECU.Dod_boost2=[0 20 40 50  80 100];
ECU.Coef_boost2=[0 0.2 0.6 1 1 1];

% Repartition du freinage avant arriere 
% norme par rapport au freinage maxi vehicule
ECU.Frein_entre=[0 1];
ECU.Frein_avant=[1 1];

display('Fin d''initialisation des parametres calculateur'); 

%------------------------------------MISES A JOUR-----------------------------------%
