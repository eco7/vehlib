% File: ECU.BUS_CPLHYD
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier calculateur
%                      Pour librairie VEHLIB
%                          INRETS - LTE
%  Objet: Ce script initialise la structure CALC
%          contenant les champs necessaires pour renseigner 
%          le composant ECU.LATEUR de la librairie VEHLIB.
%
%   Fichier pour commande vehicule conventionnel avec loi pour BV auto
%
%                             16/08/2001
% =============================================================================

% Identification du type de lois de passage de boite de vitesse
ECU.bv_auto=1;

% Condition initiale primaire convertisseur de couple
ECU.regini_prim_conv=600*pi/30;

% Changement de rapports de boite automatique
% Montee rapport >1
ECU.wmt_chrap_M=[1350 1550 1850 1900]*pi/30;
ECU.ouvert_chrap_M=[0.  0.4  0.6    1];
% Montee passage 1ere -> 2eme
ECU.wmt_chrap_M1=ECU.wmt_chrap_M;
ECU.ouvert_chrap_M1=ECU.ouvert_chrap_M;
% Descente
ECU.wmt_chrap_D=[800 900 1000 1050]*pi/30;
ECU.ouvert_chrap_D=ECU.ouvert_chrap_M;

% nombre maximum de rapport de boite
ECU.nbmax_rapbv=4;

% Duree totale d un changement de rapport
ECU.chrapp_duree=1.2*0.6;

% Action sur la pedale d embrayage lors d'un chgt de rapport
ECU.emb_temps= [0 .1 0.3 0.4 0.7];
ECU.emb_course=[0. 1 1   0.  0  ];

% Correction sur la pedale d accelerateur lors d'un chgt de rapport
ECU.acc_temps=[0  .1  0.3  0.4 0.7];
ECU.acc_corr= [1. 0.  0.   1   1  ];

% Ouverture papillon maximum du conducteur
ECU.alphamot_max=1;

% Repartition du freinage avant arriere 
% norme par rapport au freinage maxi vehicule
ECU.Frein_entre=[0 1];
ECU.Frein_avant=[1 1];

display('Fin d''initialisation des parametres calculateur'); 

%------------------------------------MISES A JOUR-----------------------------------%
