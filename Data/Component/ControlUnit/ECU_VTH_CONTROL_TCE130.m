% File: ECU.VTH
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier calculateur
%                      Pour librairie VEHLIB
%                          INRETS - LTE
%  Objet: Ce script initialise la structure CALC
%          contenant les champs necessaires pour renseigner 
%          le composant ECU.LATEUR de la librairie VEHLIB.
%
%   Fichier pour commande vehicule conventionnel avec loi pour BV auto
%
%                             20/10/99
% =============================================================================


% Changement de rapports de boite automatique
% Montee des rapports
ECU.wmt_chrap_M=[2500 3000 3500 3550]*pi/30;
ECU.ouvert_chrap_M=[0. 0.4 0.6 1];
% Descente des rapports
ECU.wmt_chrap_D=[1000 2000 2050]*pi/30;
ECU.ouvert_chrap_D=[0. 0.2 1];
ECU.wmt_chrap_D=ECU.wmt_chrap_M*0.4;
ECU.ouvert_chrap_D=ECU.ouvert_chrap_M;

% nombre maximum de rapport de boite
ECU.nbmax_rapbv=5;

% Duree totale d un changement de rapport
ECU.chrapp_duree=1.2;

% Delai de passage d un rapport
ECU.rapp_delay=0.5;

% Action sur la pedale d embrayage lors d'un chgt de rapport
ECU.emb_temps= [0 .4 0.6 1.2 1.5];
ECU.emb_course=[0. 1 1   0.  0  ];

% Correction sur la pedale d accelerateur lors d'un chgt de rapport
ECU.acc_temps=[0  .2  0.4 0.6  1  1.2 1.5];
ECU.acc_corr= [1. 0.5 0.2 0.0  0. 1   1  ];

% Valeurs en dessous desquels on fait patiner l'embrayage
ECU.wmt_patinage=120; % vitesse de regulation pour le patinage

% Regulation de ralenti
ECU.P_ral = 0.1;
ECU.I_ral = 0.0;
ECU.D_ral = 0.0;

% Ouverture papillon maximum du conducteur
ECU.alphamot_max=1;

% Torque and speed to phi look-up table - measured on bench with Renault ECU (22/03/2022; 24 and 25)
ECU.Speed_2dPhi = [750	850	1000	1500	2000	2500	3000	3500	4000 6000]*pi/30;
ECU.Torque_2dPhi = [	-10.0	0.0	10.0	25.0	50.0	75.0	85.0	100.0	120.0	125.0	150.0	175.0	200.0];
ECU.Phi_2d = [
	0.918	0.749	0.699	0.801	0.876	0.969	1.011	1.077	1.164	1.186	1.295	1.404	1.513
	0.918	0.749	0.699	0.801	0.876	0.969	1.011	1.077	1.164	1.186	1.295	1.404	1.513
	0.832	0.741	0.776	0.805	0.875	1.007	1.005	1.037	1.116	1.136	1.236	1.335	1.433
	0.862	0.837	0.834	0.835	0.918	0.977	0.991	0.994	1.021	1.045	1.111	1.177	1.244
	0.564	0.871	0.862	0.851	0.938	0.970	0.999	0.991	1.029	1.038	1.095	1.145	1.157
	0.545	0.961	0.964	0.960	1.022	1.027	1.032	1.043	1.083	1.088	1.104	1.126	1.406
	0.620	1.039	1.006	1.017	1.031	1.028	1.040	1.070	1.085	1.095	1.165	1.162	1.686
	0.684	1.137	1.109	1.072	1.069	1.068	1.087	1.110	1.121	1.122	1.216	1.128	1.3
	0.823	1.108	1.061	1.060	1.041	1.065	1.084	1.122	1.117	1.131	1.199	1.1	1.1
	0.823	1.108	1.061	1.060	1.041	1.065	1.084	1.122	1.117	1.131	1.199	1.1	1.1
];

% Torque and intake pressure to phi look-up table - measured on bench with Renault ECU (22/03/2022; 24 and 25)
ECU.Reg_2dPhi2 = [750	850	1000	1500	2000	2500	3000	3500	4000 6000]*pi/30;
ECU.Padm_2dPhi2 = [	0.2	0.4	0.6	0.8	1.0	1.2	1.4	1.6	1.8	2.0]*1000;
ECU.Phi_2d2 = [
	0.913	0.723	0.704	0.838	0.905	0.967	0.978	0.997	1.010	1.029
	0.913	0.723	0.704	0.838	0.905	0.967	0.978	0.997	1.010	1.029
	0.880	0.813	0.752	0.869	0.960	1.017	0.985	0.999	1.008	1.021
	0.843	0.850	0.832	0.973	1.001	1.013	1.010	1.005	1.001	0.996
	0.8 	0.8 	0.849	0.984	1.025	1.137	1.070	1.081	1.088	1.099
	0.8 	0.955	0.971	1.028	1.080	1.111	1.129	1.157	1.175	1.202
	0.8 	1.020	1.011	1.031	1.087	1.135	1.189	1.233	1.262	1.306
	0.8 	1.104	1.074	1.069	1.113	1.279	1.249	1.309	1.349	1.409
	0.8 	1.120	1.042	1.086	1.098	1.224	1.297	1.385	1.436	1.500
	0.8 	1.120	1.042	1.086	1.098	1.224	1.297	1.385	1.436	1.500
];



% Couples mini et maxi :
ECU.wmt_max = [750.0 1000.0 1250.0 1500.0 1750.0 2000.0 2250.0 2500.0 2750.0 3000.0 3250.0 3500.0 3750.0 4000.0 4250.0 4500.0 4750.0 5000.0 5250.0 5500.0 5750.0 6000.0]*pi/30; 
ECU.cmt_max = [86.9 113.6 141.0 184.0 199.1 200.6 199.3 199.7 199.4 200.8 199.4 197.5 194.9 191.8 188.1 186.0 181.1 177.8 173.8 167.1 159.8 151.9];
ECU.cmt_min =(-1)*[10.6 11.2 11.8 12.5 13.2 14.0 14.9 15.8 16.8 17.9 19.0 20.1 21.3 22.6 24.0 25.4 26.8 28.4 29.9 31.6 33.3 35.0];
% MTHERM_CONTROL_TCE130;
% VD.MOTH = MOTH;
% Phi = 1;
% Padm = interp1(VD.MOTH.p_intake_wmt,VD.MOTH.p_intake,ECU.wmt_max,'linear','extrap');
% delta_AA = 0;
% szD = 1;
% [dcarb, Cmt] = calc_mt_3U(ECU.wmt_max,Phi,delta_AA,Padm,szD,VD);


% Repartition du freinage avant arriere 
% norme par rapport au freinage maxi vehicule
ECU.Frein_entre=[0 1];
ECU.Frein_avant=[1 1];

display('Fin d''initialisation des parametres calculateur'); 

%------------------------------------MISES A JOUR-----------------------------------%
