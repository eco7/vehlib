% File: ECU.VTH
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier calculateur
%                      Pour librairie VEHLIB
%                          INRETS - LTE
%  Objet: Ce script initialise la structure CALC
%          contenant les champs necessaires pour renseigner 
%          le composant ECU.LATEUR de la librairie VEHLIB.
%
%   Fichier pour commande vehicule conventionnel avec loi pour BV auto
%
%                             20/10/99
% =============================================================================


% Changement de rapports de boite automatique
% Montee des rapports
ECU.wmt_chrap_M=[2500 3000 3500 3550]*pi/30;
ECU.ouvert_chrap_M=[0. 0.4 0.6 1];
% Descente des rapports
ECU.wmt_chrap_D=[1000 2000 2050]*pi/30;
ECU.ouvert_chrap_D=[0. 0.2 1];
ECU.wmt_chrap_D=ECU.wmt_chrap_M*0.4;
ECU.ouvert_chrap_D=ECU.ouvert_chrap_M;

% nombre maximum de rapport de boite
ECU.nbmax_rapbv=5;

% Duree totale d un changement de rapport
ECU.chrapp_duree=1.2;

% Delai de passage d un rapport
ECU.rapp_delay=0.5;

% Action sur la pedale d embrayage lors d'un chgt de rapport
ECU.emb_temps= [0 .4 0.6 1.2 1.5];
ECU.emb_course=[0. 1 1   0.  0  ];

% Correction sur la pedale d accelerateur lors d'un chgt de rapport
ECU.acc_temps=[0  .2  0.4 0.6  1  1.2 1.5];
ECU.acc_corr= [1. 0.5 0.2 0.0  0. 1   1  ];

% Valeurs en dessous desquels on fait patiner l'embrayage
ECU.wmt_patinage=120; % vitesse de regulation pour le patinage

% Ouverture papillon maximum du conducteur
ECU.alphamot_max=1;

% Repartition du freinage avant arriere 
% norme par rapport au freinage maxi vehicule
ECU.Frein_entre=[0 1];
ECU.Frein_avant=[1 1];

display('Fin d''initialisation des parametres calculateur'); 

%------------------------------------MISES A JOUR-----------------------------------%
