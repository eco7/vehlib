% File: ECU.VTH_perfos
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier calculateur
%                      Pour librairie VEHLIB
%                          INRETS - LTE
%  Objet: Ce script initialise la structure CALC
%          contenant les champs necessaires pour renseigner 
%          le composant ECU.LATEUR de la librairie VEHLIB.
%
%   Fichier perfos pour commande vehicule conventionnel
%
%                             20/10/99
% =============================================================================


% Changement de rapports de bo�te
% montee
ECU.wmt_chrap_M=[    3550 4000 ]*pi/30;
ECU.ouvert_chrap_M=[   0    1  ];
%descente
ECU.wmt_chrap_D=ECU.wmt_chrap_M*0.4;
ECU.ouvert_chrap_D=ECU.ouvert_chrap_M;

% nombre maximum de rapport de boite
ECU.nbmax_rapbv=5;

% Duree totale d un changement de rapport
ECU.chrapp_duree=1.0;

% D�lai de passage d un rapport (action sur pedales puis passage vitesse)
ECU.rapp_delay=0.15;

% Action sur la pedale d embrayage lors d'un chgt de rapport
ECU.emb_temps= [0 0.1 0.2 0.3 0.4 0.5];
ECU.emb_course=[0 0.8 0.4 0.2 0.0 0.0];

% Correction sur la pedale d accelerateur lors d'un chgt de rapport
ECU.acc_temps=[0 0.01 0.1 0.2 0.3 0.4];
ECU.acc_corr= [1 0.20 0.8 1.0 1.0 1.0];

% Valeurs en dessous desquels on fait patiner l'embrayage
ECU.wmt_patinage=2000*pi/30; % vitesse de regulation pour le patinage

% Ouverture papillon maximum du conducteur
ECU.alphamot_max=1;

display('Fin d''initialisation des parametres calculateur'); 

% Repartition du freinage avant arriere 
% norme par rapport au freinage maxi vehicule
ECU.Frein_entre=[0 1];
ECU.Frein_avant=[1 1];

%------------------------------MISES A JOUR-------------------------------------%
% 10-04-2003. MDR. Modifications des changements de rapports pour meilleures perfos.
% 11-05-2007. BJ Adaptation des lois de changement de rapport pour Clio 
