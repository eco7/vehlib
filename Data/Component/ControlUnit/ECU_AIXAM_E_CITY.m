% =============================================================================
%
%                Initialisation du fichier calculateur
%                      Pour librairie VEHLIB
%                          INRETS - LTE
%  Objet: Ce script initialise la structure ECU.
%          contenant les champs necessaires pour renseigner 
%          le composant ECU.LATEUR de la librairie VEHLIB.
%
%  Fichier calculateur pour vehicule AIXAM electrique
% =============================================================================


% Valeur statistique JPR; valeur moyenne des couple de retenu utilises dans
% VEHLIB et pendant la generation des cycles
ECU.Vit_Cmin = [0	250  750    1250  1750  2250  2750  3250  3750  4250  4750  5250  6000 7000 7900]*pi/30;
ECU.Cpl_min  = [0  -1.8  -6.3  -6.8  -6.9  -6.8  -6.4  -5.7  -5.0  -5.0  -4.3  -4.0  -3.0 -2.0 -1.0]; % machine asynchrone

% Valeur statistique JPR; valeur max des couples de retenu utilises dans le
% traitement des donnees pour la generation des cycles
ECU.Vit_retenu     =  [ 0	500     2500    4000    6000 ]*pi/30;
ECU.Cpl_max_retenu =  [ 0	-15.8   -17.5   -16.4   -9.3 ]; 

% Repartition du freinage avant arriere 
% norme par rapport au freinage maxi vehicule
ECU.Frein_entre=[0 1];
ECU.Frein_avant=[1 1];

ECU.seuil_frein=850*0.5;

% Type de strategie
% 2: loi empirique
% 3: loi lagrange en ligne
ECU.ntypstrat=4;

% Strategie 2 (empirique)
% Constante de temps pour filtrage de Pdem
ECU.taux=200;
% Duree moyenne d'un arret
ECU.tarret=20;


% Strategie Lagrange embedded
% Pas de calcul
ECU.pas_temps=0.01;
% discretisation en courant
ECU.pas_isc=1;
% Valeur initiale de lambda
ECU.lambda_ini=2.1;

display('Fin d''initialisation des parametres calculateur'); 

%------------------------------------MISES A JOUR-----------------------------------%
