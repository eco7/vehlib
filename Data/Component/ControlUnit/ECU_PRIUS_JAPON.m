% File: ECU.PRIUS_JAPON
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier calculateur
%                      Pour librairie VEHLIB
%                          INRETS - LTE
%  Objet: Ce script initialise la structure CALC
%          contenant les champs necessaires pour renseigner 
%          le composant ECU.LATEUR de la librairie VEHLIB.
%
%   Fichier pour commande vehicule Prius version japon
%
%
% =============================================================================


% Gestion du ralenti moteur
ECU.ral_min=70;
ECU.ral_dem=100;
ECU.ral_max=90;

% Courbe enveloppe du moteur thermique pour ces pertes (frottement + pompage)
ECU.Nmt_pertes		= [ 0 90 200 300 420];
ECU.Cpertes_mt 	= [-5 -5 -15 -20 -25];

% Puissance de declenchement et d'arret du mot therm
ECU.P_dec_hyb=5800;
ECU.P_arret_hyb=3800;

%Couple de demarreur
ECU.cpl_dem=30;

%parametres de regulation de la vitesse du GE
ECU.P_reg_ge=1;
ECU.I_reg_ge=0;
ECU.D_reg_ge=0;

ECU.wmaxge=480;				% vitesse maximum de la generatrice (rd/s);
ECU.wmaxge=600;				% vitesse maximum de la generatrice (rd/s);
%vmaxele=-wmaxge*kred*dev/(kb*2*pi);	% vitesse maxi de fonct en mode electrique (m/s);
ECU.vmaxele=13.6;

ECU.vitminge=[0 19.4 27.8];			% vitesse minimum de la generatrice fonction de la vitesse v?hicule  
ECU.wminge=[-380 -130 -100];

% duree minimum de maintient du mode hybride en sec
ECU.delay_stop=3;

% temperature eau seuil bas pour chauffe moteur
ECU.Teau_mth_seuil=70;

%gestion de la dod
ECU.DoD=[     0 20 40   50     60    80   100];
ECU.pdem_bat=[0  0  0 -2000 -4000 -5000 -5000];

% DOD de declenchement et d'arret du mot therm
ECU.DOD_dec_hyb=150;
ECU.DOD_arret_hyb=150;

% Repartition du freinage avant arriere 
% norme par rapport au freinage maxi vehicule
ECU.Frein_entre=[0 1];
ECU.Frein_avant=[1 1];

display('Fin d''initialisation des parametres calculateur'); 

%------------------------------------MISES A JOUR-----------------------------------%

