% File: ECU.AX
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier calculateur
%                      Pour librairie VEHLIB
%                          INRETS - LTE
%  Objet: Ce script initialise la structure ECU.
%          contenant les champs necessaires pour renseigner 
%          le composant ECU.LATEUR de la librairie VEHLIB.
%
%  Fichier calculateur pour vehicule AX electrique
% =============================================================================


ECU.Vit_Cmin=[0 50  300 1165 7000]*pi/30;
ECU.Cpl_min=[0 0 -40 -40 -40];

% Repartition du freinage avant arriere 
% norme par rapport au freinage maxi vehicule
ECU.Frein_entre=[0 1];
ECU.Frein_avant=[1 1];

display('Fin d''initialisation des parametres calculateur'); 

%------------------------------------MISES A JOUR-----------------------------------%
