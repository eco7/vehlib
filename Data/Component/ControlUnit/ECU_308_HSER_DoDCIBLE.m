% =============================================================================
% 
%                Initialisation du fichier calculateur
%                      Pour librairie VEHLIB    
%                           INRETS - LTE 
%  Objet: Ce script initialise la structure ECU.
%          contenant les champs necessaires pour renseigner 
%          le composant ECU.LATEUR de la librairie VEHLIB.
%
% =============================================================================


%reperage de la strategie du calculateur (5 : DoDcible)
ECU.ntypstrat=5;

% strategie de freinage (1 recup max, 2 stabilite)
ECU.strat_frein=1;

% caracteristiques pour action frein
ECU.Vit_Cmin=[0 50  300 1165 7000]*pi/30*1.46;
ECU.Cpl_min=[0 0 -40 -40 -40]*2.5;

% filtrage de la demande
ECU.Trep=0.5;

% vitesse maxi de fonct en mode electrique (m/s);
ECU.vmaxele=10;

%Gestion de la charge batterie en mode hybride
ECU.cpcbat_calc=[0   10   20   30   50  80 100];
ECU.pdembat_calc=[0 100 300 500 5000 17000 20000]*0.7;

%Couple de demarreur
ECU.cpl_dem=50;

%saturation en sortie du pid de la gene
ECU.cpl_min_gen=-400;
ECU.cpl_max_gen=0;

%%% gestion du groupe electrog�ne
ECU.pge_opti = [  0         0.1250    0.2500    0.3751    0.5001    0.6251    0.7501    0.8752    1.0002    1.1252    1.2502    1.3752    1.5003    1.6253    1.7503    1.8753    2.0003    2.1254    2.2504    2.3754    2.5004 ]* 1.0e+004;
ECU.wge_opti = [ 83.7758   83.7758  120.0258  138.1508  156.2758  210.6508  214.2758  225.1508  236.0258  239.6508  250.5258  261.4008  261.4008  261.4008  261.4008  265.0258  272.2758  290.4008  297.6508  304.9008  348.4008 ];
%ECU.cge_opti = [  0.3063   18.5956   24.8414   32.2403   37.7651   33.5189   39.7869   44.2809   48.2863   53.8618   57.2177   60.2434   66.3232   72.5749   79.0895   85.5567   90.3556   89.9126   93.7122   96.8570   86.4368];

%parametres de regulation de la vitesse du GE
ECU.P_reg_ge=1;
ECU.I_reg_ge=0;
ECU.D_reg_ge=0;

ECU.grad_Pdem_ge_mont=5000;
ECU.grad_Pdem_ge_desc=-20000;

%%% gestion hybride

% 1 Puissance variable GE et charge sustaining :
%   Puissance de declenchement et d'arret du mot therm
ECU.P_dec_hyb=3250+5000; 
ECU.P_arret_hyb=3050+5000;

% 2 Puissance constante a fournir (aussi utilise en mode 3)
ECU.Pcst=5000;

% 3 thermostat seuil en capacite (%) pour declenchement et arret du mot therm
ECU.cpcbat_mar=41;
ECU.cpcbat_arret=40;

% % 4 stop-star :Vitesse vehicule de declenchement et d'arret du mot therm
% ECU.Vit_dec_hyb=1.5;
% ECU.Vit_arret_hyb=1;

% 5 DoDcible : mode 1 & 3

% 6 ligne
% la conso de carburant sur la courbe de fonctionnement optimale du groupe est exprimm�e en fonction de la puissance electrique
% MOTH.pci.*dcarb=a*Pge^2+b*Pge+c
% dans ce cas l'hamiltonien (H=MOTH.pci.*dcarb+lambda.*Pbat) est minimum pour Pge=(lambda-b)./(2.*a)
ECU.a=7.0636e-005;
ECU.b=1.9918;
ECU.c=5.1070e+03;
ECU.lambda_ini=3.208691;%3.343750;


% ============================================================================
display('Fin d''initialisation des parametres calculateur'); 

%-------------------------------------MISE A JOUR-------------------------------------%
