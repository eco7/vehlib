% File: AUX_PRIUS_II
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier auxiliaire
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure ACC
%          contenant les champs necessaires pour renseigner 
%          le composant AUXILIAIRES de la librairie VEHLIB.
%
% ACC.ntypacc :   1: Aux electriques
%                 2: Aux mecaniques type 1 
%                 3: Aux mecaniques type 2
% =============================================================================

% Identification du type de modele auxiliaires 
ACC.ntypacc=1;

% Puissance consommee sur le reseau electrique
ACC.Pacc_elec=250;

display('FIN d''initialisation des parametres auxiliaires');

%------------------------------------MISES A JOUR-----------------------------------%
