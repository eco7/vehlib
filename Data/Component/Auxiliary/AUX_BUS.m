% File: AUX_BUS
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier auxiliaire
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure ACC
%          contenant les champs necessaires pour renseigner 
%          le composant AUXILIAIRES de la librairie VEHLIB.
%
% ACC.ntypacc :   1: Aux electriques
%                 2: Aux mecaniques type 1 
%                 3: Aux mecaniques type 2
% =============================================================================

% Identification du type de modele auxiliaires 
ACC.ntypacc=3;

% Alternateur 
ACC.Pacc_elec=55*24;				% consommation accessoires electriques
ACC.rdmoy_acm2=0.6;     % Rendement moyen de l alternateur

% Ventilateur
ACC.Pventil_wmt=    [500 1000 1200  1500  2000  2500  3000  3500]*pi/30;
%ACC.Pventil_pventil=[980 5980 7980 10980 15980 20980 25980 30980];   % Valeurs avec un taux de 0.8
ACC.Pventil_pventil=[940 3835 4993  6729  9624 12519 15414 18308];   % Valeurs avec un taux de 0.4

% Servo direction
ACC.Pservo=1100;

% Compresseur d'air avec agenouillement
ACC.Pcomp_wmt=    [500 1000 1500  2000  2500  3000  3500]*pi/30;
ACC.Pcomp_pcomp=  [990 2260 3520  4790  6060  7320  8590];


display('FIN d''initialisation des parametres auxiliaires');

%------------------------------------MISES A JOUR-----------------------------------%
