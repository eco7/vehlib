% File: AUX_308SW_1L6VTI16V
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier auxiliaire
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure ACC
%          contenant les champs necessaires pour renseigner 
%          le composant AUXILIAIRES de la librairie VEHLIB.
%
% ACC.ntypacc :   1: Aux electriques
%                 2: Aux mecaniques type 1 
%                 3: Aux mecaniques type 2
% =============================================================================

% valeur arbitraire
% Identification du type de modele auxiliaires 
ACC.ntypacc=2;

% Accessoires electriques
ACC.Pacc_elec=250;		% Consommation moyenne accessoires cote electrique (W)
ACC.rdmoy_acm2=0.6;     % Rendement moyen de l alternateur

% Compresseur de climatisation
ACC.clim=0;                             % controle Marche:1; Arret:0 de la climatisation
ACC.Pcomp_waccm2=[850 2000 4500]*pi/30; % Regime pour abaque compresseur
ACC.Pcomp_pcomp=[   0    0    0];       % puissance consomm�e en W

display('FIN d''initialisation des parametres auxiliaires');

%------------------------------------MISES A JOUR-----------------------------------%
