% File: HACHEX_SA13
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%     initialisation des parametres  Pour le convertisseur DC/DC (HACHEUR)
%                 ici les donnees corresponde au SA13 exit 
%                      composants : Rien
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

display('initialisation des parametres Hacheur excit pour ACM1');

% Caracteristiques composants
ACM1.Id_ex=[0 5 500];            % courant pour chute de tension
ACM1.Vd_ex=[0 0 0];              % Chute de tension Diode en V
ACM1.Rd_ex=0;                    % Resistance de conduction diode en Ohm
ACM1.Ice_ex=[0 5 500];            % courant pour chute de tension trans
ACM1.Vce_ex=[0 0 0];             % Chute de tension transistor en V
ACM1.Rce_ex=0;                   % Resistance de conduction transistor en Ohm

% Fr�quence de commutation;
ACM1.fcom_ex=10000;        % en Hz
ACM1.tcom_ex=0;
ACM1.Comm_ex=[0 ACM1.fcom_ex*ACM1.tcom_ex ACM1.fcom_ex*ACM1.tcom_ex];
% temps mort a la commutation
ACM1.Rhom_ex=0;

display('initialisation termin�e');
