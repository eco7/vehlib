% File: ONDUL_BOB135
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%     initialisation des parametres  Pour le convertisseur  (ONDULEUR)
%                 ici les donnees sont hypothetiques 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

display('initialisation des parametres Onduleur');

% Caracteristiques composants
ACM1.Id_ond=[0 5 500];            % courant pour chute de tension
ACM1.Vd_ond=[0 1.2 1.2];              % Chute de tension Diode en V
ACM1.Rd_ond=3.0e-3;               % Resistance de conduction diode en Ohm
ACM1.Ice_ond=[0 5 500];            % courant pour chute de tension trans
ACM1.Vce_ond=[0 1.2 1.2];       % Chute de tension transistor en V
ACM1.Rce_ond=3.00e-3;           % Resistance de conduction transistor en Ohm

% Fr�quence de commutation;
ACM1.fcom_ond=10000;        % en Hz
ACM1.tcom_ond=5e-7;
ACM1.Comm_ond=[0 ACM1.fcom_ond*ACM1.tcom_ond ACM1.fcom_ond*ACM1.tcom_ond];
% temps mort a la commutation
ACM1.Rhom_ond=0;

display('initialisation termin�e');
