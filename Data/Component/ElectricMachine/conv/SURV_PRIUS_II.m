% File: SURV_PRIUS_II
% =====================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% fichier SURV genere pour VEHLIB pour prendre en compte
% la tension en sortie du survolteur : fix�e par 
% max[U_dem(N_motelc,C_motelec) U_dem(N_gene,C_gene)].
% La tension sortie survolteur est le max de ce que demande
% la gene et le moteur, sachant que la demande de chacun depend
% du couple et de la vitesse = carto Udem(N,C)
% ====================================================

SURV.Umax=507;
SURV.tauS=0.02;
SURV.fh=5e3 ; % frequence de hachage en Hz

% Param�tres IGBT et diode pour calcul des pertes
SURV.Icnom=300; % courant moyen max IGBT (en regime permanent) en A
SURV.tr=90e-9; % temps de mont�e du courant des IGBT � I=Icnom en s
SURV.tf=70e-9; % temps de descente du courant des IGBT � I=Icnom en s
SURV.tc=SURV.tr+SURV.tf; % temps total de commutation des IGBT
SURV.Vce=1.4;  % tension collecteur emeteur IGBT en saturation en V 
SURV.VD=1.1;    % tension diode en conduction
SURV.Idnom=300; % courant moyen max diode (en r�gime permanent) en A
SURV.Qrr=35e-6; % charge de recouvrement inverse en C

display('Fin d''initialisation des parametres du survolteur');
