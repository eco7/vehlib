% File: HACHIND_SA13
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%     initialisation des parametres  Pour le convertisseur DC/DC (HACHEUR)
%                 ici les donnees corresponde au CCC.AX98 
%                      composants : LS SX 120V 200A
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

display('initialisation des parametres Hacheur induit pour ACM1');

% Caracteristiques composants
ACM1.Id_ind=[0 5 500];            % courant pour chute de tension
ACM1.Vd_ind=[0 1.2 1.2];              % Chute de tension Diode en V
ACM1.Rd_ind=2.15e-3;               % Resistance de conduction diode en Ohm
ACM1.Ice_ind=[0 5 500];            % courant pour chute de tension trans
ACM1.Vce_ind=[0 1.2 1.2];       % Chute de tension transistor en V
ACM1.Rce_ind=6.00e-3;           % Resistance de conduction transistor en Ohm

% Fr�quence de commutation;
ACM1.fcom_ind=16000;        % en Hz
ACM1.tcom_ind=3e-7;
ACM1.Comm_ind=[0 ACM1.fcom_ind*ACM1.tcom_ind ACM1.fcom_ind*ACM1.tcom_ind];
% temps mort a la commutation
ACM1.Rhom_ind=0;

display('initialisation termin�e');
