% File: MCT_80kW_TYPE
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier moteur electrique
%                      Pour librairie SIVTHEC
%                    INRETS - LTE Decembre 1999
%          Element moteur electrique cartographie
%  Objet: Ce script initialise la structure ACM1
%          contenant les champs necessaires pour renseigner 
%          le composant Groupe Moto Propulseur 1 de la librairie SIVTHEC.
%
%       Convention moteur regime et couple de m�me signe
%       Convention generateur regime et couple de signes opposes
% =============================================================================

% Reperage du type de modelisation moteur (1: moteur cartographie LTE)
ACM1.ntypmg=10;

% Masse d'un moteur+onduleur+environnement
ACM1.Masse_mg=40;
ACM1.J_mg	=	0.4;	   % inertie moteur electrique

ACM1.Regmot_pert=[ 0.0 10.0  510.0  1010.0  1510.0  2010.0  2510.0  3010.0  3510.0 ...
              4010.0  4510.0  5010.0  5510.0  6010.0  6510.0]*pi/30*1.46;
ACM1.Cmot_pert=[ -130.0  -110.0   -90.0   -70.0   -50.0   -30.0 -20.0 -10.0  0.0 ...
               10.0  20.0  30.0    50.0    70.0    90.0   110.0   130.0]*2.5;
ACM1.Pert_mot =[   0.690   0.690   0.690   0.690   1.300   0.940 0.800  0.690  0.00 ...
            0.690 0.800  0.940   1.300   1.780   2.380   3.100   3.930
              0.690   0.690   0.690   0.690   1.300   0.940 0.800  0.690  0.61 ...
               0.690 0.800  0.940   1.300   1.780   2.380   3.100   3.93
              3.820   3.010   2.330   1.750   1.290   0.940 0.810  0.700  0.61 ...
	         0.710 0.820  0.960   1.320   1.800   2.390   3.100   3.920
              3.880   3.060   2.360   1.780   1.310   0.960  0.830 0.720  0.61 ...
	         0.760  0.870 1.000   1.360   1.840   2.420   3.120   3.940
              3.940   3.110   2.410   1.820   1.340   0.990 0.850  0.750  0.61 ...
	         0.840  0.950 1.090   1.440   1.920   2.500   3.190   4.000
              4.010   3.170   2.460   1.860   1.380   1.140 0.970  0.830  0.61 ...
	         0.660 0.760  0.890   1.310   1.900   2.720   3.790   5.160
              4.900   3.840   2.920   2.100   1.430   0.920  0.720 0.570  0.61 ...
	         0.550  0.710 0.940   1.620   2.650   4.060   5.980   5.980
              6.570   5.120   3.830   2.690   1.770   1.060 0.800  0.600  0.61 ...
	         0.610 0.850  1.180   2.220   3.830   6.180   6.180   6.180
              6.660   6.660   4.960   3.460   2.220   1.280  0.940 0.680  0.61 ...
	         0.720  1.030 1.510   3.030   5.520   5.520   5.520   5.520
              8.440   8.440   6.260   4.350   2.760   1.550 1.110  0.790  0.61 ...
	         0.850 1.280  1.930   4.120   7.870   7.870   7.870   7.870
              7.750   7.750   7.750   5.360   3.370   1.860  1.310 0.930  0.61 ...
	         1.020  1.570 2.450   5.490   5.490   5.490   5.490   5.490
              9.380   9.380   9.380   6.520   4.070   2.220  1.550 1.080  0.61 ...
	         1.220 1.930  3.090   7.320   7.320   7.320   7.320   7.320
              7.750   7.750   7.750   7.750   4.850   2.620  1.820 1.260  0.65 ...
	         1.450  2.360 3.870   9.770   9.770   9.770   9.770   9.770
              9.110   9.110   9.110   9.110   5.700   3.080 2.130  1.470  0.80 ...
	         1.720  2.860 4.840   4.840   4.840   4.840   4.840   4.840
             10.620  10.620  10.620  10.620   6.640   3.570 2.450  1.700  0.98 ...
	         2.050  3.470 6.040   6.040   6.040   6.040   6.040   6.040
]*1000*3;  

ACM1.Cpl_frot=zeros(size(ACM1.Regmot_pert));

ACM1.Tension_cont = [90 110 130 150 160]*2.53;                         % tension cote continu (V) 
% Rajouter une ligne de couple maximum pour une valeur de tension superieure a la tension maxi 
%(pb d extrapolation dans simulink si tension superieure)
ACM1.Regmot_cmax  = [0 1000 1500 2000 3000 4000 5000 6000 ...
                     7000 7100 8000]*pi/30*1.46;                         %regime
ACM1.Cmax_mot     = [ 130 130 100 69.7 47 32.9 25.3 18.5 9 0 0; 
		 130 130 126 89 58 42 33 24.2 11.9 0 0;
		 130 130 130 108 71 52 40 29.9 14.7 0 0;
       130 130 130 126.6 82.9 60.9 47.8 35.5 17.6 0 0;
       130 130 130 126.6 82.9 60.9 47.8 35.5 17.6 0 0;]*2.5 ; %  couple maxi en fonction de la tension et du regime
% i.e. les deux dernieres lignes sont identiques en couple

ACM1.Cmax_mot=ACM1.Cmax_mot';		 
ACM1.Cmin_mot=-ACM1.Cmax_mot;

%gradient de montee et descente du couple en Nm/s
ACM1.grad_cmg_mont=3000*2.5;
ACM1.grad_cmg_desc=-3000*2.5;

% Courbe de performances maxi sur l arbre du moteur de traction pour le calculateur
ACM1.reg_trac_max=ACM1.Regmot_cmax;
ACM1.cpl_trac_max=ACM1.Cmax_mot(:,3)';
