% param_MELEC   : chargelment de la structure MELEC pour le
% modele Paire_pole_v3065_M21.icar, pour l'optimisation SQP du moteur
% electrique (optim_MELEC).
% Meme fichier pour tous les types d'optimisation


% java.lang.System.setProperty('muse.repo.home','Z:\Config\Cades3.2.1\.muse');
% gav= 'org.gu.g2elab:test2.TEST2smlModel:1.0' %GroupId: org.gu.g2elab ArtifactId: test2.TEST2smlModel Version: 1.0
% import org.gu.vesta.cades.scripting.matlab.plugin.MatlabMuseCaller; 
% muse = MatlabMuseCaller();
% muse.loadMuse(gav);

% VR 26/06/2013 creation
% VR 04/02/2014 : mise � jour du composant icar


MELEC.nom = 'Paire\_pole\_v3065\_M21\_jar3\_v2\_nsga.muse';

MELEC.parametres = {'ALPHA',    'BR',   'DELTA','GAP','H3S','H4S','I','LM','MAGWID', 'NSE','PROF','RAD1','RHQ2','T', 'VIT','W3S'};
MELEC.inputs     =  [1.396263   1.12      0       0.6   27.0 16.4   0   5.0  27.0      13   85.0   92.0   10.0  420   1200  7.0];
                  % [1.396263   1.12      0       0.6   27.0 16.4   0   5.0  27.0      13    85.0   92.0   10.0  420  1200  7.0];
                                             
MELEC.ALPHA   = 1.396263;
MELEC.BRA     = 1.12;
MELEC.DELTA   = 0;
MELEC.GAP     = 0.6;
MELEC.H3S     = 27;
MELEC.H4S     = 16.4;
MELEC.I       = 0;
MELEC.LM      = 5.0;
MELEC.MAGWID  = 27;
MELEC.NSE     = 13;
MELEC.PROF    = 85;
MELEC.RAD1    = 92;
MELEC.RHQ2    = 10;
MELEC.T       = 420;
MELEC.W3S     = 7;

MELEC.VIT     = 1200; % vitesse de rotation de la machine
MELEC.p       = 8;


MELEC.Nse   = 13;
MELEC.Nepp  = 2;
MELEC.Ne    = MELEC.Nepp*3*MELEC.p;
MELEC.b1 	= 1.5;
MELEC.ab1 	= 1.5;
MELEC.dd 	= 0.35e-3; 
MELEC.Kh 	= 136;
MELEC.Ke 	= 0.36;
MELEC.sigma	= 1800000;
MELEC.PHI   = pi/MELEC.p;

MELEC.Jmax = 12; % A/mm2
MELEC.regmot_max=15000; % vitesse de rotation max en tr/min pour generer les tables regmot_cmax, etc

MELEC.tet_bob = 2*pi/MELEC.p*(MELEC.RAD1+MELEC.GAP+2*MELEC.H3S/3)*1.2;
MELEC.L_cuivre = MELEC.Nse*MELEC.Ne/3*(MELEC.PROF+MELEC.tet_bob)*1e-3;

MELEC.S_cuivre = muse.getStaticFacetVariableValue('S_cuivre');
MELEC.S_encoche = muse.getStaticFacetVariableValue('S_encoche');
MELEC.R_chaud  = muse.getStaticFacetVariableValue('R_chaud');
MELEC.Dext     = muse.getStaticFacetVariableValue('x_Dext');
MELEC.Imax     = MELEC.Jmax*MELEC.S_cuivre*sqrt(2)*1e6;
MELEC.BRIDGE   = muse.getStaticFacetVariableValue('BRIDGE');


if evalin('base','exist(''OND'')'); % Utilis� avec genere_carto_ACM1_muse
    MELEC.Vmax = OND.Ubus*4/pi*0.8/sqrt(3);
else
    MELEC.Vmax = 500*4/pi*0.8/sqrt(3);
end

MELEC.Masse_mg = muse.getStaticFacetVariableValue('Masse');
MELEC.J_mg     = muse.getStaticFacetVariableValue('Jinertie');

MELEC.Pmax = 54901; % par defaut au d�but

% Carto

MELEC.dI_carto      = 17;   % nombre de ligne en courant 
MELEC.kdI_carto     = 5; 
MELEC.alpha_carto   = 2;    % coude pour la discr�tisation logarithmique

MELEC.ddelta_carto  = 13;
MELEC.Ie            = logsample(0.0001,MELEC.Imax,MELEC.dI_carto,MELEC.alpha_carto);
MELEC.deltae        = [logsample(-90,-40,MELEC.ddelta_carto,2) -35:5:0];
MELEC.deltae        = sort(unique([-180-MELEC.deltae MELEC.deltae]));
MELEC.kCgrid        = 60;   % taile de MELEC.Cmot_pert



MELEC.Regmot_pert  = sort([0 50 MELEC.regmot_max logsample(70,MELEC.regmot_max,30,1.5)])*pi/30;
MELEC.Regmot_cmax  = sort([0 50 MELEC.regmot_max logsample(70,MELEC.regmot_max,30,1.5)])*pi/30;
MELEC.reg_trac_max  = sort([0 50 MELEC.regmot_max logsample(70,MELEC.regmot_max,30,1.5)])*pi/30;

MELEC.grad_cmg_mont = 3000;
MELEC.grad_cmg_desc = -Inf;

clear ans x_Ba

