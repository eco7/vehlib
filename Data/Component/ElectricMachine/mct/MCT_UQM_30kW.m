% File: MCT_UQM_30kW
% fichier acm1 genere pour VEHLIB a partir d un moteur
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% asynchrone Ls18kW
%tension d alimentation 250 V
% courant max 90A bobinage

ACM1.nom='MCT_UQM_30kW';

ACM1.ntypmg=10;

ACM1.Regmot_pert=[     0.0    52.4   105.8   157.1   210.5   261.8   315.2   419.9   524.6   629.4   734.1   837.8];

ACM1.Cmot_pert=[  -160.0   -140.0    -120.0    -100.0     -80.0     -60.0     -40.0     -20.0     -10.0       0.0      10.0      20.0      40.0      60.0      80.0     100.0     120.0     140.0    160.0];

 
ACM1.Pert_mot=[ 1654.74   1884.23   1366.25   1138.64   1076.94    932.29    746.03    331.49     30.77      0.00     76.67    450.34    800.49   1200.67   2125.36   2242.06   2767.71   4054.95   3944.43
                3350.40   2785.02   2198.70   1779.90   1465.80   1099.35    774.78    387.39    198.93    100.00    320.85    500.90    850.81   1300.31   2255.08   2696.82   3382.62   4491.97   5584.00
                5078.40   3703.00   3047.04   2433.40   1862.08   1269.60    804.08    444.36    370.30    200.00    450.69    562.48    900.69   1400.00   2387.28   3160.26   4009.26   4937.33   7254.86
                5026.24   4178.06   3392.71   2670.19   1884.84   1366.51    942.42    628.28    549.74    400.00    600.76    700.35    950.73   1500.26   2217.46   3217.10   4137.45   5158.10   6282.80
                5725.60   4361.56   3662.70   2947.00   2105.00   1578.75   1052.50    799.90    736.75    650.00    750.46    850.53   1000.86   1600.29   2405.71   3426.74   4283.86   5119.20   6898.31
                7120.96   5131.28   4084.08   2748.90   1989.68   1492.26    994.84    785.40    916.30    700.00    800.69    924.00   1050.27   1625.91   2198.54   3071.40   4694.34   5966.60   8579.47
                9329.92   6398.56   4917.12   3467.20   2269.44   1512.96   1008.64    819.52   1008.64    800.00   1000.29   1100.98   1096.35   1644.52   2493.89   3895.73   5651.86   7483.70  11447.75
               15452.32  10581.48   8062.08   5668.65   4199.00   2645.37   1175.72    839.80   1343.68   1100.00   1400.00   1500.11   1264.22   2955.72   4798.86   6553.35   9597.71  12904.24  20067.95
               25180.80  17626.56  12590.40   9180.50   6295.20   4091.88   2518.08   1783.64   1678.72   1500.00   2000.71   2148.96   2861.45   4703.31   7406.12  11127.88  15738.00  23192.84  35972.57
               40281.60  26434.80  18882.00  13846.80   9063.36   6042.24   3524.64   2202.90   2014.08   1800.00   2600.88   2670.18   4098.42   7193.14  11052.88  17752.31  25176.00  37764.00  67136.00
               58728.00  41109.60  29070.36  19820.70  13801.08   8148.51   4845.06   2936.40   2569.35   2300.00   3600.85   3670.50   5802.47   9998.17  18040.63  27151.64  43388.60  68516.00 117456.00
               80424.00  58642.50  42222.60  31834.50  20106.00  12566.25   6702.00   4188.75   3099.68   2800.00   4920.12   5585.00   8377.50  16755.00  28722.86  51345.97  72797.59 117285.00 201060.00];
            
ACM1.Cpl_frot=[  0. -1 -1 -1 -1 -1 -1.2 -1.3 -1.4 -1.6 -1.8 -2 ];
            
ACM1.J_mg=    0.112 ;   %  a revoir unite bizarre sur la plaquette

ACM1.Masse_mg= 39.24+19.39 ;    %moteur+controle

ACM1.Tension_cont=[    120    180    240];

ACM1.Regmot_cmax=[0.0    105.8    210.5   261.8   315.2   419.9   524.6   629.4   734.1 780.0 837.8 900];

%ACM1.Cmax_mot=[   96.6    96.6   96.6    80.0     66.6     50.6    40.0   32.0    26.6   23.3    0   
%                  145     145     145     120     100.     76      60      48      40     35     0   
%                  193.3  193.3    193.3  159.9    133.3    101.3   79.9   63.9    53.3   46.6    0 ];  

ACM1.Cmax_mot=[   145     145   96.6    80.0     66.6     50.6    40.0   32.0    26.6   23.3    0   0
                  145     145     145   120     100.     76      60      48      40     35     0    0
                  145     145     145   120     100.     76      60      48      40     35     0    0];  

ACM1.Cmax_mot=ACM1.Cmax_mot';
ACM1.Cmin_mot=-ACM1.Cmax_mot;

%gradient de montee et descente du couple en Nm/s
ACM1.grad_cmg_mont=    800.0 ;
ACM1.grad_cmg_desc=   -800.0 ;

% Courbe de performances maxi sur l arbre du moteur de traction pour le calculateur
ACM1.reg_trac_max=ACM1.Regmot_cmax;
ACM1.cpl_trac_max=ACM1.Cmax_mot(:,length(ACM1.Tension_cont))';

display('Fin d''initialisation des parametres moteur electrique');


%----------------------------------MISES A JOUR------------------------------------------%
% 09-01-2002. RD, MDR. ACM1.Cmax_mot ajuste pour tenir compte de la limite de courant maxi.
% 06-01-2003 : pertes non nulles a vitesse nulle. Interpollation lin?aire ? partir des deux derniers points.s.
% 10-04-2003 : RT, MDR. Pertes lissee pour plus de coherence a faible couple.
% 06-05-2003 : MDR. Ajout du champ ACM1.nom.