% File: MAS_N1_LS18kW2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%         fichier donnees d'un moteur asynchrone 
%            Moteur INRETS  Leroy Somer 18.5 kW
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
display('initialisation des parametres Moteur ASY LS 18.5kW');


ACM1.ntypmg=2;     % type de moteur (2: asynchrone )

ACM1.TR_ET=sqrt(3);  % type de couplage etoile : 1, triangle : sqrt(3)

% resistances a 20 deg	R(T)=R(20)*(1+alpha*(T-20))
ACM1.Rstat=0.09;     %  resit stator � 20 deg en Ohm
ACM1.alpha_stat=4.0e-4;% coeff de temper stat
ACM1.alpha_rot=4.0e-4;% coeff de temper rot



%nombre de paires de poles
ACM1.nbpolmas=2;

% pour toutes les pertes omega est en tr/mn     
% pertes mecaniques
ACM1.cqfpmgvit=3.57e-2;                %coefficient en omega
ACM1.cqfpmgvit2=-4e-7;		           %coefficient en omega carre
ACM1.cqfpmgvit3=4e-9;              %coefficient en omega cube

%inertie
ACM1.J_mg=0.15;                % moment d'inertie du mcc en kgm2
ACM1.Masse_mg=0;                % Masse mot+conv.

% pertes fer =(cqfpfercons+cqfpfervit*omeg)*Id*omega
ACM1.cqfpfercons=9.5e-3;
ACM1.cqfpfervit=2e-6;           %coefficient en omeg

% Temps de reponse des courants
ACM1.Td=0.05;
ACM1.Tq=0.05;

% Constante de temps rotorique a 20 deg
ACM1.Taur20=0.2;

% decroissance de la const de temps rot avec la pulsation
ACM1.wr=[0 2 5 12 25 50 100];
ACM1.decr_Tr_wr=[0.2 0.2 0.2 0.2 0.2 0.2 0.2];

% Inductance en H en fonction du courant magnetisant
ACM1.Imagn_sat=[0 300];
ACM1.Ls_sat=[0.018 0.018];
%ACM1.sigma=[0.25 0.17 0.11 0.105 0.107 0.125 0.180 0.22 0.24 0.25 0.26 0.27 0.28]*0.6 ;
ACM1.sigma=0.09;

display('initialisation terminee');
