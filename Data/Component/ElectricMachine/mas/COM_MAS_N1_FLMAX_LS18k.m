% File: COM_MAS_N1_FLMAX_LS18k
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%     Fichier commande de moteur asynchrone
%     commande flux maximum pour modele de Niveau1
%     Pour VEHLIB 1.0 Janvier 2001
%     donnees du MAS Leroy Somer 18.5 kW
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ACM1.ntypcommas=1;   % 1 : flux maximum

% commande de desexcitation
%ACM1.omeg_opt=[0.0   104.7   209.4   314.2   418.9   523.6   628.3   733.0];
%ACM1.id_ref=[56 56 30.6 15.0 12.0 10.2 8.9 8];

ACM1.omeg_opt=[0.0   104.7  157 209.4 260  314.2 360  418.9 470  523.6  580 628.3   733.0];

ACM1.id_ref=[42 42 33 23 16 13 11.5 9.5  5.5  3  1.5  1 1];

% tension continue nominale
ACM1.Vcc_nom=240;

   
%limitation du couple maximum en fonction de la tension
ACM1.Tension_cont = [120 150 180 200];                         % tension Bat (V) 
ACM1.Regmot_cmax  = [0 1000 1500 2000 3000 4000 5000 6000 ...
                     7000 7100 8000]*pi/30;                         %regime
ACM1.Cmax_mot     = [ 130 130 130 126.6 82.9 60.9 47.8 35.5 0 0 0; 
		           130 130 130 126.6 82.9 60.9 47.8 35.5 0 0 0;
		           130 130 130 126.6 82.9 60.9 47.8 35.5 0 0 0;
                 130 130 130 126.6 82.9 60.9 47.8 35.5 0 0 0;]*1.2 ; %  couple maxi 

ACM1.Cmax_mot=ACM1.Cmax_mot';		 
ACM1.Cmin_mot=-ACM1.Cmax_mot;

ACM1.rapp_cycl=[0 0.97 0.98 1.0 1.02 1.05];
ACM1.cplmax_rcyc=[200 200 100 30 0 0];

% courant maximum sur l'axe q
ACM1.iq_max=200.;     % courant q maxi
