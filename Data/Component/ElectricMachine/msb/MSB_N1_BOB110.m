% File: MSB_N1_BOB110
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%         fichier d'initialisation d'un moteur a courant
%            continu a excitation separee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
display('initialisation des parametres Moteur MSY BOB110');

ACM1.ntypmg=3;     % type de moteur (3: synchrone bobin�e)


ACM1.TR_ET=1;  % type de couplage etoile : 1, triangle : sqrt(3)

% resistances a 20 deg	R(T)=R(20)*(1+alpha*(T-20))
ACM1.Rstat=0.009;     %  resit stator � 20� en Ohm
ACM1.alpha_stat=4.0e-4;% coeff de temper stat
ACM1.Rexcit=5.06;    %  resit rotor � 20� en Ohm
ACM1.alpha_excit=4.0e-4;% coeff de temper rot

%constantes de temps en s
ACM1.Td=0.05  ;
ACM1.Tq=0.05  ;
ACM1.Tex=0.1  ;

% Mf, sturartion
ACM1.Istat_sat_Mf= [0 250 500];       % courant stator en A
ACM1.Iexcit_sat_Mf=[0 1 2 3 4 5 6 7 8 9 10 20]; % Iex en A
ACM1.Mf_sat=[63.70 63.70 61.85 57.50 47.97 40.70 35.35 31.24 28.02 25.40 23.26 23.0;
        63.70 63.70 61.85 57.50 47.97 40.70 35.35 31.24 28.02 25.40 23.26 23.0;
       63.70 63.70 61.85 57.50 47.97 40.70 35.35 31.24 28.02 25.40 23.26 23.0];
  
ACM1.Mf_sat=ACM1.Mf_sat/1000;   % conversion en Henry

%     Ld, saturation     
ACM1.Istat_sat_Ld= [0 24.9 43.0 63.9 85.0 104.5 126.3 146.8 168.2 185.5 210 1000];% courant stator en A 
ACM1.Iexcit_sat_Ld=[0  10 20]; % Iex en A
ACM1.Ld_sat=[1.800 1.733 1.660 1.558 1.303 1.124 0.969 0.860 0.770 0.712 0.639 0.639;
1.800  1.733 1.660 1.558 1.303 1.124 0.969 0.860 0.770 0.712 0.639 0.639;    
    1.800  1.733 1.660 1.558 1.303 1.124 0.969 0.860 0.770 0.712 0.639 0.639];
ACM1.Ld_sat=ACM1.Ld_sat'/1000;          % transposition et conversion en Henry          
%     Lq, saturation               
ACM1.Istat_sat_Lq= [0 24.9 43.0 63.9 85.0 104.5 126.3 146.8 168.2 185.5 210 1000];% courant stator en A 
ACM1.Iexcit_sat_Lq=[0  10 20]; % Iex en A 
ACM1.Lq_sat=[1.800 1.733 1.660 1.558 1.303 1.124 0.969 0.860 0.770 0.712 0.639 0.639;
1.800 1.733 1.660 1.558 1.303 1.124 0.969 0.860 0.770 0.712 0.639 0.639;        
1.800  1.733 1.660 1.558 1.303 1.124 0.969 0.860 0.770 0.712 0.639 0.639];
ACM1.Lq_sat=ACM1.Lq_sat'/1000;          % transposition et conversion en Henry        
ACM1.Lq_sat=ACM1.Lq_sat/2.2;

ACM1.Lf=3;

%nombre de paires de poles
ACM1.nbpolmsy=2;

% pour toutes les pertes omega est en tr/mn     
% pertes mecaniques
ACM1.cqfpmgvit=2.593e-2;                %coefficient en omega
ACM1.cqfpmgvit2=-7.76e-6;		   %coefficient en omega carr�
ACM1.cqfpmgvit3=1.702e-9;              %coefficient en omega cube

%inertie
ACM1.J_mg=0.15;                % moment d'inertie du mcc en kgm2
ACM1.Masse_mg=0;                % Masse mot+conv en kg

% pertes fer
ACM1.cqfpfercons=8.306e-2;
ACM1.cqfpfervit=-3.688e-5;           %coefficient en omeg
ACM1.cqfpfervit2=4.834e-9;           %coefficient en omeg carr�


% pertes supplementaires
ACM1.cqfpsupcons=-1.681e-2;           %coefficient en I2
ACM1.cqfsupreg=7.704e-6;              %coefficient en omeg*I2
ACM1.regtestsup=2182;


% parametres pour modele lineaire
ACM1.Ld=0.0011;
ACM1.Lq=0.0005;
ACM1.Mf=0.023;

display('initialisation termin�e');
