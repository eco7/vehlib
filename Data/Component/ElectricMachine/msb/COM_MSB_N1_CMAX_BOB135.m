% File: COM_MSB_N1_CMAX_BOB135
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%     Fichier commande de moteur synchrone bobine
%     commande flux max et couple max pour modele de Niveau1
%     Pour VEHLIB 1.0 Janvier 2001
%     donnees du moteur BOB135
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


ACM1.ntypcommsb=1;    % type de commande du moteur msb (1: couple maximum)

ACM1.omeg_tab=[0 500 1000 1500 1800 2100 2400 3000 3600 4200 4800 5400 6000 6500]*pi/30;
%iex_tab=[10 10 10 10 10 8.5 7.5 6 5 4.28 3.75 3.33 3 2.8];
ACM1.iex_tab=[10 10 10 10 10 10 10 6.5 3.4 2.7 2.20 1.8 1.5 1.2];

%limitation du couple maximum en fonction de la tension
ACM1.Tension_cont = [120 150 180 200];                         % tension Bat (V) 
ACM1.Regmot_cmax  = [0 1000 1500 2000 3000 4000 5000 6000 ...
                     7000 7100 8000]*pi/30;                         %regime
ACM1.Cmax_mot     = [ 130 130 130 126.6 82.9 60.9 47.8 35.5 0 0 0; 
		           130 130 130 126.6 82.9 60.9 47.8 35.5 0 0 0;
		           130 130 130 126.6 82.9 60.9 47.8 35.5 0 0 0;
                 130 130 130 126.6 82.9 60.9 47.8 35.5 0 0 0;]*1.2 ; %  couple maxi 

ACM1.Cmax_mot=ACM1.Cmax_mot';		 
ACM1.Cmin_mot=-ACM1.Cmax_mot;

%limite du couple du au rapport cyclique
ACM1.rapp_cycl=[0 0.97 0.98 1.0 1.02];
ACM1.cplmax_rcyc=[200 200 100 30 0];

ACM1.p=2;  %nombre de paires de p�les
