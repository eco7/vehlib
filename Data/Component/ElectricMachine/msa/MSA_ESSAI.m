% File: MSA_ESSAI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%         fichier d'initialisation d'un moteur synchrone a aiment
%            a base du moteur RNRJ BOB135
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
display('initialisation des parametres Moteur MSA RNRJ135');

ACM1.ntypmg=4;     % type de moteur (4: synchrone a aiment)


ACM1.TR_ET=1;  % type de couplage etoile : 1, triangle : sqrt(3)

% resistances a 20 deg	R(T)=R(20)*(1+alpha*(T-20))
ACM1.Rstat=0.009;     %  resit stator � 20� en Ohm
ACM1.alpha_stat=4.0e-4;% coeff de temper stat

%constantes de temps en s
ACM1.Td=0.05  ;
ACM1.Tq=0.05  ;

% parametres pour modele lineaire
ACM1.Ld=0.0011;
ACM1.Lq=0.0011;

% flux au rotor aiment
ACM1.phif=0.5;

%nombre de paires de poles
ACM1.nbpolmsy=2;

% pour toutes les pertes omega est en tr/mn     
% pertes mecaniques
%ACM1.cqfpmgvit=2.593e-2;                %coefficient en omega
%ACM1.cqfpmgvit2=-7.76e-6;		   %coefficient en omega carr�
%ACM1.cqfpmgvit3=1.702e-9;              %coefficient en omega cube

% pertes mecaniques
ACM1.cqfpmgvit=0;                %coefficient en omega
ACM1.cqfpmgvit2=0;		   %coefficient en omega carr�
ACM1.cqfpmgvit3=0;              %coefficient en omega cube

%inertie
ACM1.J_mg=0.15;                % moment d'inertie du mcc en kgm2
ACM1.Masse_mg=0;                % Masse mot+conv en kg

% pertes fer
%ACM1.cqfpfercons=8.306e-2;
%ACM1.cqfpfervit=-3.688e-5;           %coefficient en omeg
%ACM1.cqfpfervit2=4.834e-9;           %coefficient en omeg carr�

% pertes fer
ACM1.cqfpfercons=0;
ACM1.cqfpfervit=7.65e-6*pi/30;           %coefficient en omeg
ACM1.cqfpfervit2=0;           %coefficient en omeg carr�


% pertes supplementaires
ACM1.cqfpsupcons=-1.681e-2;           %coefficient en I2
ACM1.cqfsupreg=7.704e-6;              %coefficient en omeg*I2
ACM1.regtestsup=2182;



display('initialisation termin�e');
