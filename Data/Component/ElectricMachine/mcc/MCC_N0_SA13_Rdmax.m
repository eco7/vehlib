% File: MCC_N0_SA13_Rdmax
% ============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% .......Element moteur electrique
%    Convention moteur regime et couple de meme signe
%    Convention moteur regime et couple de signes opposes
   10.0   610.0  1210.0  1810.0  2410.0  3010.0  3610.0  4210.0  4810.0  5410.0  6010.0  6610.0 
 -130.00  -110.00  -90.00  -70.00  -50.00  -30.00  -10.00   10.00   30.00   50.00   70.00   90.00  110.00 125.00
  0.610    3.420    3.510    3.650    3.490    3.440    3.610    3.890    4.440    4.320    4.600    5.560  
  0.610    2.680    2.740    2.870    3.270    3.440    3.610    3.890    4.440    4.320    4.600    5.560  
  0.610    2.010    2.070    2.190    2.460    3.440    3.610    3.890    4.440    4.320    4.600    5.560  
  0.610    1.450    1.460    1.570    1.770    2.390    3.270    3.890    4.440    4.320    4.600    5.560  
  0.610    0.920    0.960    1.060    1.210    1.550    2.060    2.700    3.450    4.320    4.600    5.560  
  0.610    0.510    0.550    0.630    0.800    0.930    1.180    1.490    1.870    2.310    2.830    3.410  
  0.610    0.180    0.220    0.270    0.550    0.560    0.660    0.790    0.950    1.150    1.390    1.670  
  0.170    0.200    0.230    0.300    0.510    0.560    0.670    0.830    1.040    1.290    1.600    1.980  
  0.500    0.530    0.580    1.030    0.780    1.020    1.390    1.890    2.540    3.390    4.500    4.440  
  0.930    0.950    1.000    1.270    1.310    1.940    2.890    3.470    3.980    4.330    4.500    4.440  
  1.440    1.460    1.510    1.650    2.140    3.460    3.400    3.470    3.980    4.330    4.500    4.440  
  2.060    2.070    2.120    2.210    2.990    3.460    3.400    3.470    3.980    4.330    4.500    4.440  
  2.740    2.740    2.790    2.930    2.990    3.460    3.400    3.470    3.980    4.330    4.500    4.440  
  3.330    3.320    3.360    3.140    2.990    3.460    3.400    3.470    3.980    4.330    4.500    4.440  
J_mg	=	0.05;	   % inertie moteur electrique
ACM1.Masse_mg	=	50.;	   % Masse moteur electrique+convertisseur

Tension_cont = [90 110 130 150];                         % tension Bat (V) 
Regmot_cmax  = [0 1000 1500 2000 3000 4000 5000 6000...
                     7000]*pi/30;                         %regime
Cmax_mot     = [ 130 130 100 69.7 47 32.9 25.3 20 16.1; 
		 130 130 126 89 58 42 33 26 21.5;
		 130 130 130 108 71 52 40 32.7 27;
		 130 130 130 126.6 82.9 60.9 47.8 38.8 32.3;] ; %  couple maxi 
		 
Cmin_mot=-Cmax_mot;
