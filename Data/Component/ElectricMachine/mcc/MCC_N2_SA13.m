% File: MCC_N2_SA13
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%         fichier d'initialisation d'un moteur a courant
%            continu a excitation separee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
display('initialisation des parametres Moteur MCC');

ACM1.ntypmg=1;       % type de moteur (1 : courant continu)

% resistances a 20 deg
ACM1.Rind=0.057;     %  induit en Ohm
ACM1.Rexcit=5.54;    %  inducteur en Ohm

% Temps de r�ponse de mont�e des courant 
ACM1.trind=0.1;      % induit en s
ACM1.trex=0.5;       % inducteur en s

%    version VERT
ACM1.Iind_sat= [-250 250];       % courant induit en A
ACM1.Iexcit_sat=[0 0.5 1 1.66 2 2.5 2.9 3.33 3.6 3.85  4.08 4.4 4.83 5 5.2 ...
            5.41 5.8 6.16 6.8 7.3 7.9 8.33 9 9.75 10.5 11.41]; % Iex en A
ACM1.Mutuel_sat=[1.928 1.9300 1.9355 1.9181 1.9061 1.8799 1.8588 1.8235 1.8038 ...
            1.7807 1.7590 1.7253 1.6707 1.6458 1.6159 1.5867 1.5335 1.4860 ...
            1.4083 1.3470 1.2814 1.2311 1.1649 1.1029 1.0426 0.9789;
            1.928 1.9300 1.9355 1.9181 1.9061 1.8799 1.8588 1.8235 1.8038 ...
            1.7807 1.7590 1.7253 1.6707 1.6458 1.6159 1.5867 1.5335 1.4860 ...
            1.4083 1.3470 1.2814 1.2311 1.1649 1.1029 1.0426 0.9789];
                        
ACM1.Mutuel_sat=ACM1.Mutuel_sat/1000;      % conversion en Henry            
ACM1.consflux=59.2056;

% chute de tension dans les balais
ACM1.Idbal=[-500 -5 -1 0 1 5 500];          % courant pour chute de tension balais
ACM1.Dubalais=[-2 -2 0 0 0 2 2];           % Chute de tension dans les balais en V


% pertes mecaniques
ACM1.cqfpmgvit=0.035;                %coefficient en omega
ACM1.cqfpmgvit3=3.6e-9;              %coefficient en omega cube
%inertie
ACM1.J_mg	=0.103;             % moment d'inertie du mcc en kgm2
ACM1.Masse_mg	=	0.;	  				 % Masse moteur electrique+convertisseur

% pertes fer
ACM1.cqfpferreg=-6.88e-10;           %coefficient en omeg2
ACM1.cqfpferregvolt=5.834e-6;        %coefficient en omeg*V2


% pertes supplementaires
ACM1.cqfpsupcons=-1.681e-2;           %coefficient en I2
ACM1.cqfsupreg=7.704e-6;              %coefficient en omeg*I2
ACM1.regtestsup=2182;

display('initialisation termin�e');
