% File: COM_MCC_N2_FLMAX_SA13
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%     Fichier commande de moteur MCC � excitation separee
%     commande flux maximum pour modele de Niveau2
%     Pour VEHLIB 1.0 Janvier 2001
%     donnees du moteur SA13
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

display('initialisation des parametres Commande MCC1');

ACM1.ntypcomcc=1;       % type de commande (1 : flux maximum)

% resistances a 20 deg
ACM1.Rind=0.057;     %  induit en Ohm
ACM1.Rexcit=5.54;    %  inducteur en Ohm

% Temps de r�ponse de mont�e des courants 
ACM1.trind=0.1;      % induit en s
ACM1.trex=0.5;       % inducteur en s

% Inductance mutuelle en mH
%    version VERT
ACM1.Iind_sat= [-250 250];       % courant induit en A
ACM1.Iexcit_sat=[0 0.5 1 1.66 2 2.5 2.9 3.33 3.6 3.85  4.08 4.4 4.83 5 5.2 ...
            5.41 5.8 6.16 6.8 7.3 7.9 8.33 9 9.75 10.5 11.41]; % Iex en A
ACM1.Mutuel_sat=[1.928 1.9300 1.9355 1.9181 1.9061 1.8799 1.8588 1.8235 1.8038 ...
            1.7807 1.7590 1.7253 1.6707 1.6458 1.6159 1.5867 1.5335 1.4860 ...
            1.4083 1.3470 1.2814 1.2311 1.1649 1.1029 1.0426 0.9789;
            1.928 1.9300 1.9355 1.9181 1.9061 1.8799 1.8588 1.8235 1.8038 ...
            1.7807 1.7590 1.7253 1.6707 1.6458 1.6159 1.5867 1.5335 1.4860 ...
            1.4083 1.3470 1.2814 1.2311 1.1649 1.1029 1.0426 0.9789];
                        
ACM1.Mutuel_sat=ACM1.Mutuel_sat/1000;      % conversion en Henry            
ACM1.consflux=59.2056;

% chute de tension dans les balais
ACM1.Idbal=[-500 -5 -1 0 1 5 500];          % courant pour chute de tension balais
ACM1.Dubalais=[-2 -2 0 0 0 2 2];           % Chute de tension dans les balais en V

%  limitation de courant en fonction de la vitesse mpteur
ACM1.Vit_Imax=[0 5750 7000];       % vitesse rot mot en tr/mn
ACM1.Iind_max=[200 200 107];

%  limitation de la vitesse maxi par l'interrmediaire du couple
ACM1.Vit_Cmax=[0  6990 7010];       % vitesse rot mot en tr/mn
ACM1.Cpl_max=[200 200  0];

%  limitation courant maxi par l'interrmediaire du couple
ACM1.Iind_Cmax=[0  200 210];       % courant mot en A
ACM1.Cind_max=[200 200  0];

ACM1.Vit_Imin=[0 50 300 1165 2800 5200 7000];
ACM1.Iind_min=[0 0 -72 -72 -72 -123 -123];

ACM1.Iex_max=10.5;            % valeur max pour l'excit en A
ACM1.sec_iex=0.2;             % marge de securit� pour assurer le defluxage

% Caracteristiques Hacheur pour l'estimation de la tension induit
ACM1.Id_ind=[0 5 500];            % courant pour chute de tension
ACM1.Vd_ind=[0 1.2 1.2];              % Chute de tension Diode en V
ACM1.Rd_ind=2.15e-3;               % Resistance de conduction diode en Ohm
ACM1.Ice_ind=[0 5 500];            % courant pour chute de tension trans
ACM1.Vce_ind=[0 1.2 1.2];       % Chute de tension transistor en V
ACM1.Rce_ind=6.00e-3;           % Resistance de conduction transistor en Ohm

% Fr�quence de commutation;
ACM1.fcom_ind=16000;        % en Hz
ACM1.tcom_ind=3e-7;
ACM1.Comm_ind=[0 ACM1.fcom_ind*ACM1.tcom_ind ACM1.fcom_ind*ACM1.tcom_ind];
% temps mort a la commutation
ACM1.Rhom_ind=0;


display('initialisation termin�e');
