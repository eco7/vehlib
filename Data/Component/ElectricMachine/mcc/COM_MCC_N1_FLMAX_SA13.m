% File: COM_MCC_N1_FLMAX_SA13
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%     Fichier commande de moteur MCC � excitation separee
%     commande flux maximum pour modele de Niveau1
%     Pour VEHLIB 1.0 Janvier 2001
%     donnees du moteur SA13
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

display('initialisation des parametres Commande MCC1');

ACM1.ntypcomcc=1;       % type de commande (1 : flux maximum)

% resistances a 20 deg
ACM1.Rind=0.057;     %  induit en Ohm
ACM1.Rexcit=5.54;    %  inducteur en Ohm

% Temps de r�ponse de mont�e des courants 
ACM1.trind=0.1;      % induit en s
ACM1.trex=0.5;       % inducteur en s

% Inductance mutuelle en mH
%    version VERT
ACM1.Iind_sat= [-250 250];       % courant induit en A
ACM1.Iexcit_sat=[0 0.5 1 1.66 2 2.5 2.9 3.33 3.6 3.85  4.08 4.4 4.83 5 5.2 ...
            5.41 5.8 6.16 6.8 7.3 7.9 8.33 9 9.75 10.5 11.41]; % Iex en A
ACM1.Mutuel_sat=[1.928 1.9300 1.9355 1.9181 1.9061 1.8799 1.8588 1.8235 1.8038 ...
            1.7807 1.7590 1.7253 1.6707 1.6458 1.6159 1.5867 1.5335 1.4860 ...
            1.4083 1.3470 1.2814 1.2311 1.1649 1.1029 1.0426 0.9789;
            1.928 1.9300 1.9355 1.9181 1.9061 1.8799 1.8588 1.8235 1.8038 ...
            1.7807 1.7590 1.7253 1.6707 1.6458 1.6159 1.5867 1.5335 1.4860 ...
            1.4083 1.3470 1.2814 1.2311 1.1649 1.1029 1.0426 0.9789];
                        
ACM1.Mutuel_sat=ACM1.Mutuel_sat/1000;      % conversion en Henry            
ACM1.consflux=59.2056;

%  limitation de courant en fonction de la vitesse mpteur
ACM1.Vit_Imax=[0 5750 7000];       % vitesse rot mot en tr/mn
ACM1.Iind_max=[200 200 107];

%  limitation de la vitesse maxi par l'interrmediaire du couple
ACM1.Vit_Cmax=[0  6990 7010];       % vitesse rot mot en tr/mn
ACM1.Cpl_max=[200 200  0];

%  limitation courant maxi par l'interrmediaire du couple
ACM1.Iind_Cmax=[0  200 210];       % courant mot en A
ACM1.Cind_max=[200 200  0];

ACM1.Vit_Imin=[0 50 300 1165 2800 5200 7000];
ACM1.Iind_min=[0 0 -72 -72 -72 -123 -123];

ACM1.Iex_max=10.5;            % valeur max pour l'excit en A
ACM1.sec_iex=0.2;             % marge de securit� pour assurer le defluxage

ACM1.omeg_ex=[ 0         0    0.3787    0.7821    1.1714    1.5739    1.9771...  
          2.3804    2.7833    3.1502    3.4687    3.7516    4.0070    4.2400... 
          4.4546    4.6533    4.8384    5.0115    5.1739    5.3268    5.4710... 
          5.6073    5.7363    5.8562    5.9640    6.0612    6.1490    6.2282...
          6.3000    6.3649    6.4238    6.4772    6.5256    6.5696    6.6096...
          6.6459    6.6789    6.7090    6.7363    6.7611    6.7837    6.8043...
          6.8231    6.8401    6.8556    6.8698]*1000; 
          
            
ACM1.iex_omeg=[0   10.8331   10.7019   10.6221   10.5735   10.5436    7.0314...
          4.5515    3.5282    3.0387    2.7216    2.4949    2.3206    2.1816...
          2.0676    1.9728    1.8931    1.8242    1.7639    1.7107    1.6633...
          1.6212    1.5832    1.5613    1.5449    1.5307    1.5182    1.5072...
          1.4974    1.4888    1.4811    1.4742    1.4680    1.4625    1.4575...
          1.4529    1.4488    1.4451    1.4418    1.4387    1.4359    1.4334...
          1.4311    1.4290    1.4270    1.4252];


display('initialisation termin�e'); 