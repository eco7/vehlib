% Function: gen_carto_msb
function []=gen_carto_msb(fichier,fichier_mct,fichier_com,ubat,Wmail_min,Wmail_max,pas_omeg,Cmail_min,Cmail_max,pas_couple,Ismax,Iex_max)
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%programme de creation de carto de id iq if optimum 
%pour un moteur synchrone bobine
%minimisation des pertes par l'algorithme du Simplexe. 

%variables globales.
global imag_sat;
global Cem Rs Rf p N omega Ld Lq Mf count Vs Ptot Id Iq Vd Vq;
global nbpolmsy Ld_sat Lq_sat Mf_sat flg_exmax Ismax Iex;
global nbp_couple saill k1 k2 ldd ;


%nombre d'iterations maxi du Simplexe:800. 
%options(3)=1e-4;
options(14)=800; 
%initialisation du compteur d'iterations du Simplexe. 
count=0;
tic;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% choix et chargement du fichier moteur
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%execution du fichier
eval(fichier(1:length(fichier)-2));


Rs=ACM1.Rstat;
Rf=ACM1.Rexcit;
Td=ACM1.Td;
Tq=ACM1.Tq;
Tex=ACM1.Tex;
imag_sat=ACM1.Imagn_sat;
Mf_sat=ACM1.Mf_sat;
k1=ACM1.k1;
k2=ACM1.k2;
ldd=ACM1.ldd;
saill=ACM1.saillance;
nbpolmsy=ACM1.nbpolmsy;
cqfpmgvit=ACM1.cqfpmgvit; 
cqfpmgvit2=ACM1.cqfpmgvit2;
cqfpmgvit3=ACM1.cqfpmgvit3;
cqfpfercons=ACM1.cqfpfercons;
cqfpfervit=ACM1.cqfpfervit;
cqfpfervit2=ACM1.cqfpfervit2;
cqfpsupcons=ACM1.cqfpsupcons;
cqfsupreg=ACM1.cqfsupreg;
regtestsup=ACM1.regtestsup;

Wmail_min=Wmail_min*pi/30;
Wmail_max=Wmail_max*pi/30;
pas_omeg=pas_omeg*pi/30;

%Iex_max=10;
Iex_min=Iex_max/100;
%Ismax=120;
Vsmax=(ubat*sqrt(2)/pi)*ACM1.TR_ET;
flag_exmax=0;

%puissance maxi majoree
Pmax=(3*(Vsmax/ACM1.TR_ET)*Ismax)*1.2;

i=1;
j=1;
cpl(i)=Cmail_min;
w(j)=Wmail_min;

%------boucle d'incrementation du couple
while cpl(i)<Cmail_max+pas_couple
   
%++++++boucle d'incrementation du regime (en rad/s)
while w(j)<Wmail_max+pas_omeg
%pas_omeg=0.13333*w(j)+20.944

Cmeca=cpl(i)
omega=w(j)
N=omega*30/pi;
Vs=50;


Puiss_cour=Cmeca*omega;

if abs(Puiss_cour)<Pmax

Pfer=(cqfpfercons+cqfpfervit*N+cqfpfervit2*N^2)*Vs^2;
Pfer1=0;
Pmeca=cqfpmgvit*N+cqfpmgvit2*N^2+cqfpmgvit3*N^3;

%************** boucle d'optimisation bidimensionnelle

while abs((Pfer1-Pfer)/Pfer)>0.01
Pfer1=Pfer;
if omega~=0
Cem=Cmeca+(Pfer1+Pmeca)/omega;
else
Cem=Cmeca;
end

v1=abs(Cem/0.8); 
v2=abs(Cem/20); 
v=[v1 v2]'
%recherche du mini des pertes 
'entree opt bidim'
sol1=fmins('calptot_msb1',v,options);
'sortie opt bidim'
Id=sol1(1);
Iex=sol1(2);
Iq
%stop
%Iq=Cem/(p*((Ld-Lq)*Id+Mf*Iex))

Vs=sqrt((Vd^2+Vq^2)/3)

Pfer=(cqfpfercons+cqfpfervit*N+cqfpfervit2*N^2)*Vs^2;
Pjoule=Rs*(Id^2+Iq^2)+Rf*Iex^2;
Pconv=0;
Ptot_acm=Pfer+Pjoule+Pmeca+Pconv;

%%------------fin de la boucle sur les pertes fer
%% *************optim bidim
end


%%=============  Dans le cas de depassement de Iex

if Iex>=Iex_max
Iex=Iex_max;
flag_exmax=1;

Pfer=(cqfpfercons+cqfpfervit*N+cqfpfervit2*N^2)*Vs^2;
Pfer1=0;
Pmeca=cqfpmgvit*N+cqfpmgvit2*N^2+cqfpmgvit3*N^3;

% boucle d'optim unidimensionnelle
while abs((Pfer1-Pfer)/Pfer)>0.01
Pfer1=Pfer;
if omega~=0
Cem=Cmeca+(Pfer1+Pmeca)/omega;
else
Cem=Cmeca;
end

idmin=-abs(Cem/0.3);  
idmax=abs(Cem/0.3);
'entree opt uni Iex max'
sol2=fmin('calptot_msb2',idmin,idmax,options);
'sortie opt uni Iex max'
Id=sol2;
Iex;
Iq;
%Iq=Cem/(p*((Ld-Lq)*Id+Mf*Iex));
Vs=sqrt((Vd^2+Vq^2)/3);

Pfer=(cqfpfercons+cqfpfervit*N+cqfpfervit2*N^2)*Vs^2;
Pjoule=Rs*(Id^2+Iq^2)+Rf*Iex^2;
Pconv=0;
Ptot_acm=Pfer+Pjoule+Pmeca+Pconv;

%%------------fin de la boucle sur les pertes fer
%************ optim unidim (cas Iexmax)
end
end


%%=============  Dans le cas de depassement de Vsmax

if Vs>=Vsmax
%Idmaxtemp=abs(Cem/0.3);
%Idmintemp=-abs(Cem/0.3);
Idmaxtemp=Ismax*sqrt(3);
Idmintemp=-Ismax*sqrt(3);
%Id=0;
Id_test=0;
flag_deflux=1;
'entree deflux'
Cem
while abs(Vs-Vsmax)/Vsmax>0.01&abs(Id_test-Id)/abs(Id)>0.01
   Id_test=Id;
if Vs>Vsmax;
Idmaxtemp=Id;
Id=(Idmaxtemp+Idmintemp)/2;
end
if Vs<Vsmax;
Idmintemp=Id;
Id=(Idmaxtemp+Idmintemp)/2;
end

Pfer=(cqfpfercons+cqfpfervit*N+cqfpfervit2*N^2)*Vs^2;
Pfer1=0;
Pmeca=cqfpmgvit*N+cqfpmgvit2*N^2+cqfpmgvit3*N^3;
compt1=0;

% boucle d'optim unidimensionnelle
while (abs((Pfer1-Pfer)/Pfer)>0.01)&(compt1<100)
compt1=compt1+1;
Pfer1=Pfer;
if omega~=0
Cem=Cmeca+(Pfer1+Pmeca)/omega;
else
Cem=Cmeca;
end
%options(1)=1e-2; 

%iexmin=0.55*Ld_sat(1,1)*abs(Idmaxtemp)/Mf_sat(1,11);
iexmax=Iex_max
iexmin=Iex_min;
'entree opt unidim deflux'
sol3=fmin('calptot_msb3',iexmin,iexmax,options)
'sortie opt unidim deflux'
Iex=sol3;
%Iex2=Iex
Iq;
%Iq=Cem/(p*((Ld-Lq)*Id+Mf*Iex));
Vs=sqrt((Vd^2+Vq^2)/3);

Pfer=(cqfpfercons+cqfpfervit*N+cqfpfervit2*N^2)*Vs^2;
Pfer1;
Pjoule=Rs*(Id^2+Iq^2)+Rf*Iex^2;
Pconv=0;
Ptot_acm=Pfer+Pjoule+Pmeca+Pconv;


%%------------fin de la boucle sur les pertes fer
%************ optim unidim (cas Vsmax)
end
end
end

else
   Id=999;
   Iq=999;
   Is=999;
   Vs=999;
   Pfer=0;
   Pjoule=0;
   Pmeca=0;
   Pconv=Pt_acm(i,j-1);          
end   

id_opt(i,j)=Id;
iq_opt(i,j)=Iq;
iex_opt(i,j)=Iex;

Is=sqrt((Id^2+Iq^2)./3);
Istat(i,j)=Is;
Vstat(i,j)=Vs;
Vsmax;
Cmeca;

Cplmax(i,j)=Cmeca;

deltav=Vsmax*2/100;
deltai=Ismax*2/100;
if (Vstat(i,j)>(Vsmax+deltav))|((Istat(i,j)+deltai)>Ismax)
      Cplmax(i,j)=0;
end

Mfsor(i,j)=Mf;
Cemest(i,j)=p*((Ld-Lq)*Id+Mf*Iex)*Iq;

Pt_acm(i,j)=Pfer+Pjoule+Pmeca+Pconv;


%si le point ne peut pas �tre fait avec la tension dispo
%if Vmin>Vsmax
%   if Cem>0&k1>1
%      Cplmax(k1,k2)=Cem;
%   end
   
%end



j=j+1;
w(j)=w(j-1)+pas_omeg;
end
j=1

i=i+1;

'ici calcul du couple suivant'
cpl(i)=cpl(i-1)+pas_couple;

end

%verification du couple electromag
%Cem=p*((Ld-Lq)*id_opt+Mf*iex_opt)*iq_opt;

%grandeurs interessantes 
Is=sqrt((Id^2+Iq^2)./3);
%Iex
%Ld
%Mf
Vs; 
Vsmax;
%Pfer
%Ptot 
toc;


if ~isempty(fichier_mct)

fid=fopen(fichier_mct,'w');
fprintf(fid,'%s','% fichier acm1 genere pour VEHLIB a partir d un moteur')
fprintf(fid,'\n','')
fprintf(fid,'%s','% synchrone bobine  NRJ135')
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.ntypmg=10;')
fprintf(fid,'\n','');
fprintf(fid,'\n','');


%     mise en forme des donnees a la sortie 
%     ecriture de la carto des pertes   
fprintf(fid,'%s','ACM1.Regmot_pert=[');
for i=1:length(w)-1
fprintf(fid,'%8.1f',w(i));
end
fprintf(fid,'%s','];')
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmot_pert=[');
for i=1:length(cpl)-1
fprintf(fid,'%8.1f',cpl(i));
end
fprintf(fid,'%s','];')

%fprintf(fid,'\n','');
%fprintf(fid,'\n','');
%for i=1:length(w)-1
% fprintf(fid,'%8.2f',Vstat(:,i));
% fprintf(fid,'\n','');
%end
%fprintf(fid,'\n','');
%fprintf(fid,'\n','');
%for i=1:length(w)-1
% fprintf(fid,'%8.2f',Istat(:,i));
% fprintf(fid,'\n','');
%end

fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Pert_mot=[');
for i=1:length(w)-1
 fprintf(fid,'%8.2f',Pt_acm(:,i));
 fprintf(fid,'\n','');
end
fprintf(fid,'%s','];');

fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.3f %s','ACM1.J_mg=',ACM1.J_mg,';');
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.3f %s','ACM1.Masse_mg=',ACM1.Masse_mg,';');

fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.1f %8.1f %8.1f','ACM1.Tension_cont=[',ubat*0.8,ubat,ubat*1.2);
fprintf(fid,'%s','];')

fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Regmot_cmax=[');
for i=1:length(w)-1
fprintf(fid,'%8.1f',w(i));
end
fprintf(fid,'%s','];')

%fprintf(fid,'\n','');
%fprintf(fid,'\n','')
%for i=1:length(w)-1
% fprintf(fid,'%8.2f',Cplmax(:,i));
% fprintf(fid,'\n','');
%end
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmax_mot=[');
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid,'%8.1f',cplmax2(i)*0.8);
end
fprintf(fid,'\n','')
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid,' %8.1f',cplmax2(i));
end
fprintf(fid,'\n','')
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid,' %8.1f',cplmax2(i)*1.2);
end
fprintf(fid,'%s','];');
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmax_mot=ACM1.Cmax_mot'';');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmin_mot=-ACM1.Cmax_mot;');
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','%gradient de montee et descente du couple en Nm/s');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.1f %s','ACM1.grad_cmg_mont=',max(cplmax2)/(Tex+Tq),';');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.1f %s','ACM1.grad_cmg_desc=',-max(cplmax2)/(Tex+Tq),';');
fprintf(fid,'\n','');

fprintf(fid,'\n','');
fprintf(fid,'%s','% Courbe de performances maxi sur l arbre du moteur de traction pour le calculateur');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.reg_trac_max=ACM1.Regmot_cmax;');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.cpl_trac_max=ACM1.Cmax_mot(:,length(ACM1.Tension_cont))'';');
fprintf(fid,'\n','');
fprintf(fid,'\n','');


statut=fclose(fid);

end

if ~isempty(fichier_com)
   
fid2=fopen(fichier_com,'w');
%     mise en forme des donnees a la sortie 
%     ecriture des valeur trouvees de idopt et iqopt   

fprintf(fid2,'%s','% fichier commande optim genere � partir d un moteur')
fprintf(fid2,'\n','')
fprintf(fid2,'%s','% synchrone bobine NRJ135')
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.ntypcom=1;')
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');

fprintf(fid2,'%s','ACM1.omeg_opt=[')
for i=1:length(w)-1
fprintf(fid2,'%8.1f',w(i));
end
fprintf(fid2,'%s','];');
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.cpl_opt=[');
for i=1:length(cpl)-1
fprintf(fid2,'%8.1f',cpl(i));
end
fprintf(fid2,'%s','];');

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.id_opt=[')
for i=1:length(w)-1
 fprintf(fid2,'%8.2f',id_opt(:,i));
 fprintf(fid2,'\n','');
end
fprintf(fid2,'%s','];')
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.iq_opt=[')
for i=1:length(w)-1
 fprintf(fid2,'%8.2f',iq_opt(:,i));
 fprintf(fid2,'\n','');
end
fprintf(fid2,'%s','];')

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.iex_opt=[')
for i=1:length(w)-1
 fprintf(fid2,'%8.2f',iex_opt(:,i));
 fprintf(fid2,'\n','');
end
fprintf(fid2,'%s','];')

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s %8.1f %8.1f %8.1f','ACM1.Tension_cont=[',ubat*0.8,ubat,ubat*1.2);
fprintf(fid2,'%s','];')

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.Regmot_cmax=[');
for i=1:length(w)-1
fprintf(fid2,'%8.1f',w(i));
end
fprintf(fid2,'%s','];')

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.Cmax_mot=[');
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid2,'%8.1f',cplmax2(i)*0.8);
end
fprintf(fid2,'\n','')
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid2,' %8.1f',cplmax2(i));
end
fprintf(fid2,'\n','')
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid2,' %8.1f',cplmax2(i)*1.2);
end
fprintf(fid2,'%s','];');
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.Cmax_mot=ACM1.Cmax_mot'';');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.Cmin_mot=-ACM1.Cmax_mot;');
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','%limite du couple du au rapport cyclique');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.rapp_cycl=[0 0.97  0.99 1.01];');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.cplmax_rcyc=[150 150 150  0];');

statut=fclose(fid2);

end
