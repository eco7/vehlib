% File: rend2pert2

%
%  © Copyright IFSTTAR LTE 1999-2011 
%
clear all;

fid=fopen('BRUSH_DC_UQM_CT2.m','w');

BRUSH_DC_UQM_RD

ACM1.Cmot_pert1=Cmot_pert;
ACM1.Cmot_pert2=-Cmot_pert;

for k=1:length(ACM1.Cmot_pert2)
   ACM1.Cmot_pert3(k)=ACM1.Cmot_pert2(length(ACM1.Cmot_pert2)-k+1);
end

ACM1.Cmot_pert2

ACM1.Cmot_pert=[ACM1.Cmot_pert3 ACM1.Cmot_pert1];

Rend_mot1=Rend_mot;

for i=1:length(Regmot_pert)
   
   for j=1:length(Cmot_pert)
      
     Rend_mot2(i,j)=Rend_mot1(i,length(Cmot_pert)-j+1);
     
  end
  
end

Rend_mot=[Rend_mot2 Rend_mot];

for i=1:length(Regmot_pert)
   
   for j=1:length(ACM1.Cmot_pert)
      Pmeca=Regmot_pert(i)*ACM1.Cmot_pert(j);
      if Pmeca<0
         
        ACM1.Pert_mot1(i,j)=Pmeca*(Rend_mot(i,j)/100-1);   
      else
        ACM1.Pert_mot1(i,j)=Pmeca*100/Rend_mot(i,j)-Pmeca;
      end

     
  end
  
end




ACM1.Pert_mot=ACM1.Pert_mot1;



%     mise en forme des donnees a la sortie 
%     ecriture de la carto des pertes   
fprintf(fid,'%s','ACM1.Regmot_pert=[');
for i=1:length(Regmot_pert)
fprintf(fid,'%8.1f',Regmot_pert(i));
end
fprintf(fid,'%s','];')
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmot_pert=[');
for i=1:length(ACM1.Cmot_pert)
fprintf(fid,'%8.1f',ACM1.Cmot_pert(i));
end
fprintf(fid,'%s','];')
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Pert_mot=[');
for i=1:length(Regmot_pert)
 fprintf(fid,'%10.2f',ACM1.Pert_mot(i,:));
 fprintf(fid,'\n','');
end

statut=fclose(fid);
