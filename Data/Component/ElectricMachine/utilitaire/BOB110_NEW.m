% File: BOB110_NEW
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%         fichier donn�es d'un moteur synchrone bobin�
%            Moteur INRETS  Radio Energie BOB110
%            pour modele nouvelle version (avec fuites)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
display('initialisation des parametres Moteur MSY BOB110');

global Mf_sat imag_sat Ld_sat;
global Rstat Rexcit Nd Nf;



% resistances a 20 deg	R(T)=R(20)*(1+alpha*(T-20))
Rstat=0.0965;     %  resit stator � 20� en Ohm
alpha_stat=4.0e-4;% coeff de temper stat
Rexcit=96;    %  resit rotor � 20� en Ohm
alpha_excit=4.0e-4;% coeff de temper rot


Nd=3;
Nf=253;

imag_sat=[-100 0 0.186 0.25 0.504 0.7 0.904 1.101 1.349 1.499 1.702 1.899 2 2.102 2.2 2.3 2.4 2.434 10 100];
Mf_sat=[227.52 227.52 227.52 227.52 228.37 223.99 211.07 193.27 172.27 161.23 148.24 137.34 132.21 127.40 123.06 118.97 115.19 113.89 111 90]/1000;

ldd=0.001;
Ld_sat=(Nd/Nf).*Mf_sat+ldd;
Lq_sat=Ld_sat;

Lf=17;

%nombre de paires de poles
nbpolmsy=3;

% pour toutes les pertes omega est en tr/mn     
% pertes mecaniques
cqfpmgvit=-0.0123;                %coefficient en omega
cqfpmgvit2=2e-5;		   %coefficient en omega carr�
cqfpmgvit3=0;              %coefficient en omega cube

%inertie
Jmsy=0.15;                % moment d'inertie du mcc en kgm2

% pertes fer
cqfpfercons=0.0303;
cqfpfervit=-8e-6;           %coefficient en omeg
cqfpfervit2=6e-10;           %coefficient en omeg carr�
