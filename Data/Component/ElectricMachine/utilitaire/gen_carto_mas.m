% Function: gen_carto_mas
function []=gen_carto_mas(fichier,fichier_mct,fichier_com,ubat,Wmail_min,Wmail_max,pas_omeg,Cmail_min,Cmail_max,pas_couple,Ismax)
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%programme de creation de carto de id iq et pertes 
%pour un moteur asynchrone 
%minimisation des pertes par l'algorithme du Simplexe. 

%clear all;
%variables globales.
global Cem Rs p N omega Ls_sat Imag_sat count Vs Ptot Id Iq Vd Vq;
global nbpolmas Ismax nbp_couple alpha sigma Tr Vsmin;

%nombre d'iterations maxi du Simplexe:800. 
%options(3)=1e-2;
options(14)=800; 
%initialisation du compteur d'iterations du Simplexe. 
count=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% choix et chargement du fichier moteur
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%execution du fichier
eval(fichier(1:length(fichier)-2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%affectation des variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Imag_sat=ACM1.Imagn_sat;
Ls_sat=ACM1.Ls_sat;
sigma=ACM1.sigma;
Rs=ACM1.Rstat;
Tr=ACM1.Taur20;
nbpolmas=ACM1.nbpolmas;
cqfpmgvit=ACM1.cqfpmgvit;                %coefficient en omega
cqfpmgvit2=ACM1.cqfpmgvit2;		   %coefficient en omega carre
cqfpmgvit3=ACM1.cqfpmgvit3;              %coefficient en omega cube
cqfpfercons=ACM1.cqfpfercons;
cqfpfervit=ACM1.cqfpfervit;           %coefficient en omeg
Td=ACM1.Td;
Tq=ACM1.Tq;


Wmail_min=Wmail_min*pi/30;
Wmail_max=Wmail_max*pi/30;
pas_omeg=pas_omeg*pi/30;

Vsmax=(ubat*sqrt(2)/pi)*ACM1.TR_ET;

%puissance maxi majoree
Pmax=(3*(Vsmax/ACM1.TR_ET)*Ismax)*1.2;

i=1;
j=1;
cpl(i)=Cmail_min;
w(j)=Wmail_min;

%------boucle d'incrementation du couple
while cpl(i)<Cmail_max+pas_couple
   
%++++++boucle d'incrementation du regime (en rad/s)
   while w(j)<Wmail_max+pas_omeg
%pas_omeg=0.13333*w(j)+20.944

     Cmeca=cpl(i)
     omega=w(j)
     N=omega*30/pi;
     Id=10;

     Puiss_cour=Cmeca*omega;

        if abs(Puiss_cour)<Pmax

            Pfer=(cqfpfercons+cqfpfervit*N)*Id*N;
            Pfer1=0;
            Pmeca=cqfpmgvit*N+cqfpmgvit2*N^2+cqfpmgvit3*N^3;

% boucle d'optim unidimensionnelle
            while abs((Pfer1-Pfer)/(Pfer+0.1))>0.01
               Pfer1=Pfer;
                     if omega~=0
                        Cem=Cmeca+(Pfer1+Pmeca)/omega;
                     else
                        Cem=Cmeca;
                     end

               idmin=0;  
               idmax=abs(Cem/0.3);

               sol2=fmin('calptot_mas',idmin,idmax,options);
               Id=sol2;

                Vs=sqrt((Vd^2+Vq^2)/3);

                Pfer=(cqfpfercons+cqfpfervit*N)*Id*N;
                Pjoule=Rs*(Id^2+Iq^2);
                Pconv=0;
                Ptot_acm=Pfer+Pjoule+Pmeca+Pconv;
                
            end

%%------------fin de la boucle sur les pertes fer
%************ optim unidim 

%%=============  Dans le cas de depassement de Vsmax

            if Vs>=Vsmax
   
                 idmin=abs(Cem)/20;  
                 idmax=abs(Cem/0.3);

                 sol3=fmin('calptot_mas3',idmin,idmax,options);
                 Id=sol3;
                 Vsmin;
                     if Vsmin<Vsmax       % si Vmin est sup � Vmax le point ne peut pas etre fait
   					                     	%  et on passe pas dans cette boucle
   
                           Idmaxtemp=Ismax*2;
                           Idmintemp=abs(Cem)/6;
                           %Id=-Ismax*2;
                           Id_test=0;
                           flag_deflux=1;
                           'entree deflux'
                           Cem
                           while abs(Vs-Vsmax)/Vsmax>0.01&abs(Id_test-Id)/abs(Id)>0.01
                                Id_test=Id;
                                if Vs>Vsmax;
                                      Idmaxtemp=Id;
                                      Id=(Idmaxtemp+Idmintemp)/2;
                                end
                                
                                if Vs<Vsmax;
                                      Idmintemp=Id;
                                      Id=(Idmaxtemp+Idmintemp)/2;
                                end

                                Pfer=(cqfpfercons+cqfpfervit*N)*Id*N;
                                Pfer1=0;
                                Pmeca=cqfpmgvit*N+cqfpmgvit2*N^2+cqfpmgvit3*N^3;

% boucle d homogeneisation pertes fer
                                while abs((Pfer1-Pfer)/Pfer)>0.01
                                      Pfer1=Pfer;
                                      if omega~=0
                                            Cem=Cmeca+(Pfer1+Pmeca)/omega;
                                      else
                                            Cem=Cmeca;
                                      end

                                      calptot_mas2

                                      Id;
                                      Vs=sqrt((Vd^2+Vq^2)/3);
                                      Is=sqrt((Id^2+Iq^2)./3);
                                      Pfer=(cqfpfercons+cqfpfervit*N)*Id*N;
                                      Pjoule=Rs*(Id^2+Iq^2);
                                      Pconv=0;
                                      Ptot_acm=Pfer+Pjoule+Pmeca+Pconv;
                                      
                                end

%%------------fin de la boucle sur les pertes fer
%************  (cas Vsmax) sans optimisation

                           end
                     else
                           Vs=Vsmin;
                           Id=999;
                           Iq=999;
                           Is=9999;
                     end
                end
                
        else
           Id=999;
           Iq=999;
           Is=999;
           Vs=999;
           Pfer=0;
           Pjoule=0;
           Pmeca=0;
           Pconv=Pt_acm(i,j-1);          
        end   
        
    id_opt(i,j)=Id;
    iq_opt(i,j)=Iq;

    Is=sqrt((Id^2+Iq^2)./3);
    Istat(i,j)=Is;
    Vstat(i,j)=Vs;
    Vsmax;
    Cmeca;

    if Cmeca>0
       Cplmax(i,j)=Cmeca;
    else
       Cplmax(i,j)=0;
    end

    if Cmeca>0
       deltav=Vsmax*2/100;
       deltai=Ismax*2/100;
       if (Vstat(i,j)>(Vsmax+deltav))|((Istat(i,j)-deltai)>Ismax)
           Cplmax(i,j)=0;
       end
    end
    %Cemest(i,j)=nbpolmsy*((Ld-Lq)*Id+phif)*Iq;
    Pfer
    Pjoule
    Pmeca
    Pconv
    Pt_acm(i,j)=Pfer+Pjoule+Pmeca+Pconv;


    j=j+1;
    w(j)=w(j-1)+pas_omeg;
    end
    
j=1

i=i+1;

'ici calcul du couple suivant'
cpl(i)=cpl(i-1)+pas_couple

end

%verification du couple electromag
%Cem=p*((Ld-Lq)*id_opt+Mf*iex_opt)*iq_opt;

%grandeurs interessantes 
Is=sqrt((Id^2+Iq^2)./3);
Vs; 
Vsmax;


if ~isempty(fichier_mct)

fid=fopen(fichier_mct,'w');

fprintf(fid,'%s','% fichier acm1 genere pour VEHLIB a partir du fichier moteur asynchrone')
fprintf(fid,'\n','')
fprintf(fid,'%s%s','% de nom ',fichier)
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.ntypmg=10;')
fprintf(fid,'\n','');
fprintf(fid,'\n','');

%     mise en forme des donnees a la sortie 
%     ecriture de la carto des pertes   
fprintf(fid,'%s','ACM1.Regmot_pert=[');
for i=1:length(w)-1
fprintf(fid,'%8.1f',w(i));
end
fprintf(fid,'%s','];')
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmot_pert=[');
for i=1:length(cpl)-1
fprintf(fid,'%8.1f',cpl(i));
end
fprintf(fid,'%s','];')

%fprintf(fid,'\n','');
%fprintf(fid,'\n','');
%for i=1:length(w)-1
% fprintf(fid,'%8.2f',Vstat(:,i));
% fprintf(fid,'\n','');
%end
%fprintf(fid,'\n','');
%fprintf(fid,'\n','');
%for i=1:length(w)-1
% fprintf(fid,'%8.2f',Istat(:,i));
% fprintf(fid,'\n','');
%end

fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s\n','ACM1.Pert_mot=[');
for i=1:length(w)-1
 fprintf(fid,'%10.2f',Pt_acm(:,i));
 fprintf(fid,'\n','');
end
fprintf(fid,'%s','];');

fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.3f %s','ACM1.J_mg=',ACM1.J_mg,';');
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.3f %s','ACM1.Masse_mg=',ACM1.Masse_mg,';');

fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.1f %8.1f %8.1f','ACM1.Tension_cont=[',ubat*0.8,ubat,ubat*1.2);
fprintf(fid,'%s','];')

fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Regmot_cmax=[');
for i=1:length(w)-1
fprintf(fid,'%8.1f',w(i));
end
fprintf(fid,'%s','];')

%fprintf(fid,'\n','');
%fprintf(fid,'\n','')
%for i=1:length(w)-1
% fprintf(fid,'%8.2f',Cplmax(:,i));
% fprintf(fid,'\n','');
%end
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmax_mot=[');
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid,'%8.1f',cplmax2(i)*0.8);
end
fprintf(fid,'\n','')
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid,' %8.1f',cplmax2(i));
end
fprintf(fid,'\n','')
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid,' %8.1f',cplmax2(i)*1.2);
end
fprintf(fid,'%s','];');
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmax_mot=ACM1.Cmax_mot'';');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmin_mot=-ACM1.Cmax_mot;');
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','%gradient de montee et descente du couple en Nm/s');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.1f %s','ACM1.grad_cmg_mont=',max(cplmax2)/(Td+Tq),';');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.1f %s','ACM1.grad_cmg_desc=',-max(cplmax2)/(Td+Tq),';');
fprintf(fid,'\n','');

fprintf(fid,'\n','');
fprintf(fid,'%s','% Courbe de performances maxi sur l arbre du moteur de traction pour le calculateur');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.reg_trac_max=ACM1.Regmot_cmax;');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.cpl_trac_max=ACM1.Cmax_mot(:,length(ACM1.Tension_cont))'';');
fprintf(fid,'\n','');
fprintf(fid,'\n','');


statut=fclose(fid);

end


%     mise en forme des donnees a la sortie 
%     ecriture des valeur trouvees de idopt et iqopt   

if ~isempty(fichier_com)
   
fid2=fopen(fichier_com,'w');

fprintf(fid2,'%s','% fichier commande optim genere � partir d un moteur')
fprintf(fid2,'\n','')
fprintf(fid2,'%s','% asynchrone Ls18kW')
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.ntypcom=1;')
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');



fprintf(fid2,'%s','ACM1.omeg_opt=[')
for i=1:length(w)-1
fprintf(fid2,'%8.1f',w(i));
end
fprintf(fid2,'%s','];');
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.cpl_opt=[');

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
for i=1:length(cpl)-1
fprintf(fid2,'%8.1f',cpl(i));
end
fprintf(fid2,'%s','];');

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.id_opt=[')
for i=1:length(w)-1
 fprintf(fid2,'%8.2f',id_opt(:,i));
 fprintf(fid2,'\n','');
end
fprintf(fid2,'%s','];')
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.iq_opt=[')
for i=1:length(w)-1
 fprintf(fid2,'%8.2f',iq_opt(:,i));
 fprintf(fid2,'\n','');
end
fprintf(fid2,'%s','];')

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s %8.1f %8.1f %8.1f','ACM1.Tension_cont=[',ubat*0.8,ubat,ubat*1.2);
fprintf(fid2,'%s','];')

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.Regmot_cmax=[');
for i=1:length(w)-1
fprintf(fid2,'%8.1f',w(i));
end
fprintf(fid2,'%s','];')

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.Cmax_mot=[');
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid2,'%8.1f',cplmax2(i)*0.8);
end
fprintf(fid2,'\n','')
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid2,' %8.1f',cplmax2(i));
end
fprintf(fid2,'\n','')
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid2,' %8.1f',cplmax2(i)*1.2);
end
fprintf(fid2,'%s','];');
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.Cmax_mot=ACM1.Cmax_mot'';');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.Cmin_mot=-ACM1.Cmax_mot;');
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','%limite du couple du au rapport cyclique');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.rapp_cycl=[0 0.97  0.99 1.01];');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.cplmax_rcyc=[150 150 150  0];');

statut=fclose(fid2);

end

