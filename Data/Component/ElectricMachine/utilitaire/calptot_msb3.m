% Function: calptot_msb3
%*********************************************************************
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%   Calcul des pertes pour un moteur synchrone bobine
%     Ce calcul utilise le fichier de donne du moteur
%*********************************************************************

function Ptot=calptot_msb3(v);

global imag_sat;
global Iex Iex_max flag_exmax;
global Mf_sat Ld_sat Lq_sat k1 k2;
global nbpolmsy saill ldd;
global Cem Rs Rf p N omega Ld Lq Mf count Vs Ptot Id Iq Vd Vq;


Id;
Iex=v;

p=nbpolmsy;

Istat1=0;
Is=100;
Iq=0;
compt=0;

while (abs((Istat1-Is)/Is)>0.05)&compt<100
Istat1=Is;
compt=compt+1;
imagn=abs(Iex+k1*Id+k2*abs(Iq));
Mf_sat;
imag_sat;
Mf=interplin1(imag_sat,Mf_sat,imagn);

Ld=k1*Mf+ldd;
Lq=Ld/saill;

Iq=Cem/(p*((Ld-Lq)*Id+Mf*Iex));

%Tension sur l'axe direct Vd 
Vd=Rs*Id-p*omega*Lq*Iq; 
%Tension sur l'axe en quadrature Vq
Vq=Rs*Iq+p*omega*(Ld*Id+Mf*Iex);
%pertes totales
Ptot=Vd*Id+Vq*Iq+Rf*Iex^2;

Is=sqrt((Id^2+Iq^2)/3);

end

