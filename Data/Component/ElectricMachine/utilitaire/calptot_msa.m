% Function: calptot_msa
%*********************************************************************
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%   Calcul des pertes pour un moteur synchrone bobin�
%     Ce calcul utilise le fichier de donn� du moteur
%*********************************************************************

function Ptot=calptot_msa(v);

global Ld Lq phif;
global Rstat nbpolmsy;
global Cem Rs  p N omega count Vs Ptot Iq Vd Vq;


Id=v;

Rs=Rstat;
p=nbpolmsy;

Iq=Cem/(p*((Ld-Lq)*Id+phif));

%Tension sur l'axe direct Vd 
Vd=Rs*Id-p*omega*Lq*Iq;
%Tension sur l'axe en quadrature Vq
Vq=Rs*Iq+p*omega*(Ld*Id+phif);
%pertes totales
Ptot=Vd*Id+Vq*Iq;


