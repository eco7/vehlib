% Function: util_motelec
function util_motelec(action)
%
%  © Copyright IFSTTAR LTE 1999-2011 
%


global param fichier type_mot fichier_mct
global ACM1

if nargin<1
   % Choix d un fichier par defaut
   action = 'initialise';
end

if strcmp(action,'initialise')
   clear all
   evalin('base','clear all');
   action = 'initialise';
   fichier='';
   param.titre(1)={'Entrer une tension batterie '};
   param.val(1)={250};
   param.titre(2)={'Entrer regime mini en tr/mn'};
   param.val(2)={10};
   param.titre(3)={'Entrer regime maxi en tr/mn'};
   param.val(3)={6020};
   param.titre(4)={'Entrer le pas de regime en tr/mn'};
   param.val(4)={1000};
   param.titre(5)={'Entrer couple mini en Nm'};
   param.val(5)={-160};
   param.titre(6)={'Entrer couple maxi en Nm '};
   param.val(6)={160};
   param.titre(7)={'Entrer le pas de couple en Nm'};
   param.val(7)={20};
   param.titre(8)={'Entrer le courant bobinage maxi en A'};
   param.val(8)={90};
   param.titre(9)={'Entrer le courant excitation maxi en A'};
   param.val(9)={10};

   FigParam = figure('Color',[0.20 0.750 0.5], ...
	'MenuBar','none', ...
	'Name','generation de cartographie', ...
   'NumberTitle','off', ...
   'Position',[100 150 600 650], ...
   'Visible','on',...
   'Resize','off', ...
   'Tag','TagFig');

   h1 = uicontrol('Parent',FigParam, ...
	'Units','points', ...
	'ListboxTop',0, ...
	'Position',[50 400 280 25], ...
   'Style','checkbox', ...
   'String','Generation d une cartographie de pertes',...
   	'FontSize',14, ...
   'Tag','Checkbox_ct');

   h1 = uicontrol('Parent',FigParam, ...
	'Units','points', ...
	'ListboxTop',0, ...
	'Position',[50 450 320 25], ...
   'Style','checkbox', ... 
   'String','Generation d un fichier commande cartographie',...
	'FontSize',14, ...
   'Tag','Checkbox_com');

   h10 = uicontrol('Parent',FigParam, ...
	'Units','points', ...
	'BackgroundColor',[0.5 0.5 0.9], ...
	'Position',[20 60 450 330], ...
   'Style','frame');

   yt=350;
   ye=350;
   pas=35;
   j=1;
for i=1:9
   tag=['statictext' num2str(j)];
	h1 = uicontrol('Parent',FigParam, ...
	'Units','points', ...
	'ListboxTop',0, ...
	'Position',[50 yt 300 25], ...
	'Style','text', ...
   'string',param.titre(i),...
   'FontSize',14, ...
   'Tag',tag);
 
   tag=['edittext' num2str(j)];
	h1 = uicontrol('Parent',FigParam, ...
	'Units','points', ...
	'BackgroundColor',[1 1 1], ...
	'ListboxTop',0, ...
	'Position',[350 ye 80 25], ...
	'Style','edit', ...
   'String',param.val(i),...
	'FontSize',14, ...
   'Tag',tag);
	yt=yt-pas;
   ye=ye-pas;
   j=j+1;
end

tag=['statictext' num2str(9)];
set(findobj('tag',tag),'visible','off');
tag=['edittext' num2str(9)];
set(findobj('tag',tag),'visible','off');

% bouton de choix moteur
   h1 = uicontrol('Parent',FigParam, ...
	'Units','points', ...
	'Position', [50 500 200 25], ...
	'String','Choix du fichier moteur', ...
   	'FontSize',14, ...
   'Tag','TagPushSelect', ...
   'Callback','util_motelec(''select'')');
   h1 = uicontrol('Parent',FigParam, ...
	'Units','points', ...
	'Position', [250 500 200 25], ...
	'Style','text', ...
   	'FontSize',14, ...
   'Tag','TagStrSelect', ...
   'Callback','util_motelec(''select'')');

% bouton de validation
   h1 = uicontrol('Parent',FigParam, ...
	'Units','points', ...
	'Position', [50 20 100 25], ...
	'String','Calculer', ...
   'Tag','TagPushValid', ...
   'Callback','util_motelec(''calcul'')');

% bouton de quitter
   h1 = uicontrol('Parent',FigParam, ...
	'Units','points', ...
	'Position', [200 20 100 25], ...
	'String','Quitter', ...
   'Tag','TagPushAnnul', ...
	'Callback','close(gcbf)');

% bouton de visualiser
   h1 = uicontrol('Parent',FigParam, ...
	'Units','points', ...
	'Position', [350 20 100 25], ...
	'String','Visualiser (mal)', ...
   'Tag','TagPushAnnul', ...
	'Callback','util_motelec(''Visu'')');
end

if strcmp(action,'select')
   pp=pwd;
   rep=which('initpath');
   for i=length(rep):-1:1
      if isunix
        if rep(i)=='/'
           sep='/';
           rep=rep(1:i);
           break;
        end
      else
        if rep(i)=='\'
          sep='\';
          rep=rep(1:i);
          break;
       end
      end
   end
   rep1=rep;
   rep=strcat(rep1,'motelec');
   cd(rep);
   % selecteur de fichier
   [fichier, repertoire]=uigetfile('*.m','Selection d''un fichier moteur asynchrone');
   if fichier==0
      cd(pp);
      return
   end
   set(findobj('Tag','TagStrSelect'),'String',fichier); 
   eval(fichier(1:length(fichier)-2));
   switch ACM1.ntypmg
   case 3
      tag=['statictext' num2str(9)];
      set(findobj('tag',tag),'visible','on');
      tag=['edittext' num2str(9)];
      set(findobj('tag',tag),'visible','on');
   otherwise
      tag=['statictext' num2str(9)];
      set(findobj('tag',tag),'visible','off');
      tag=['edittext' num2str(9)];
      set(findobj('tag',tag),'visible','off');
   end
   cd(pp);
end


if strcmp(action,'calcul')
   eval(fichier(1:length(fichier)-2));
   switch ACM1.ntypmg
   case 3
      npar=9
   otherwise
      npar=8
   end
   
   for j=1:npar
      tag=['edittext' num2str(j)];
      e=get(findobj('Tag',tag),'String');
      param.val{j}=str2num(char(e));
   end
   
   rep=which('initpath');
   for i=length(rep):-1:1
      if isunix
        if rep(i)=='/'
           sep='/';
           rep=rep(1:i);
           break;
        end
      else
        if rep(i)=='\'
          sep='\';
          rep=rep(1:i);
          break;
       end
      end
   end
   if get(findobj('Tag','Checkbox_ct'),'Value')
      fichier_mct=strcat(rep,'motelec',sep,'mct',sep,'MCT_',fichier)
   else
      fichier_mct='';
   end
   if get(findobj('Tag','Checkbox_com'),'Value')
      switch ACM1.ntypmg
      case 1
         repp='mcc';
      case 2
         repp='mas';
      case 3
         repp='msb';
      case 4
         repp='msa';
      end
      fichier_com=strcat(rep,'motelec',sep,repp,sep,'COM_',fichier)
   else
      fichier_com='';
   end
   
   switch ACM1.ntypmg
   case 1
      'mcc'
      gen_carto_mcc(fichier,fichier_mct,fichier_com,param.val{1},param.val{2},param.val{3},param.val{4},param.val{5},param.val{6},param.val{7},param.val{8})
   case 2
      'mas'
      gen_carto_mas(fichier,fichier_mct,fichier_com,param.val{1},param.val{2},param.val{3},param.val{4},param.val{5},param.val{6},param.val{7},param.val{8})
   case 3
      'msb'
      gen_carto_msb(fichier,fichier_mct,fichier_com,param.val{1},param.val{2},param.val{3},param.val{4},param.val{5},param.val{6},param.val{7},param.val{8},param.val{9})
   case 4
      'msa'
      gen_carto_msa(fichier,fichier_mct,fichier_com,param.val{1},param.val{2},param.val{3},param.val{4},param.val{5},param.val{6},param.val{7},param.val{8})
      
   end
end


if strcmp(action,'Visu')
      for i=length(fichier_mct):-1:1
      if isunix
        if fichier_mct(i)=='/'
           rep=fichier_mct(1:i)
           ff=fichier_mct(i+1:length(fichier_mct)-2)
           break;
        end
      else
        if fichier_mct(i)=='\'
           rep=fichier_mct(1:i)
           ff=fichier_mct(i+1:length(fichier_mct)-2)
          break;
       end
      end
   end
   
   p=pwd
   cd(rep);
   eval(ff)
   cd(p);
   eval(strcat('ACM1.nom=''',ff,''''))
   if isempty(findobj('tag','TagFigTrace'))
      figure('Tag','TagFigTrace');
   else
      set(0,'CurrentFigure',findobj('Tag','TagFigTrace'));
   end
   evalin('base','wmg1=[0];');
   evalin('base','cmg1=[0];');
   gr_melec
end




