% Function: interplin1
%*********************************************************************
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%   Interpolation lin�aire � 1 dimension
%     RT 11/99
%*********************************************************************

function [y0]=interplin1(x,y,x0)

i=0;
xroul=-9999;
while xroul<=x0&i<length(x)
i=i+1;
xroul=x(i);
end
i=i-1;
if(i<=0)
   'indice neg'
   i
   xroul
   x0
end
alpha=(y(i+1)-y(i))/(x(i+1)-x(i));
y0=y(i)+alpha*(x0-x(i));

