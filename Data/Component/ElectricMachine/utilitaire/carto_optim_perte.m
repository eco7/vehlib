% File: carto_optim_perte
% fichier acm1 genere pour VEHLIB a partir d un moteur
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% synchrone a aiment a base de bob135

ACM1.ntypmg=10;

ACM1.Regmot_pert=[   104.7   209.4];

ACM1.Cmot_pert=[    50.0    60.0];

ACM1.Pert_mot=[  235.42  247.24
  486.56  500.10
];

ACM1.J_mg=    0.150 ;

ACM1.Masse_mg=    0.000 ;

ACM1.Tension_cont=[    240.0    300.0    360.0];

ACM1.Regmot_cmax=[   104.7   209.4];

ACM1.Cmax_mot=[    48.0    48.0
     60.0     60.0
     72.0     72.0];

ACM1.Cmax_mot=ACM1.Cmax_mot';
ACM1.Cmin_mot=-ACM1.Cmax_mot;

%gradient de montee et descente du couple en Nm/s
ACM1.grad_cmg_mont=   1200.0 ;
ACM1.grad_cmg_desc=  -1200.0 ;

% Courbe de performances maxi sur l arbre du moteur de traction pour le calculateur
ACM1.reg_trac_max=ACM1.Regmot_cmax;
ACM1.cpl_trac_max=ACM1.Cmax_mot(:,length(ACM1.Tension_cont))';

