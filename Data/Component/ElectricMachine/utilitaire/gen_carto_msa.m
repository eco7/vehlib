% Function: gen_carto_msa
%programme de creation de carto de id iq et pertes 
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%pour un moteur synchrone a aiment
%minimisation des pertes par l'algorithme du Simplexe. 

function []=gen_carto_msa(fichier,fichier_mct,fichier_com,ubat,Wmail_min,Wmail_max,pas_omeg,Cmail_min,Cmail_max,pas_couple,Ismax)

%variables globales.
global Cem Rs p N omega Ld Lq count Vs Ptot Id Iq Vd Vq;
global nbpolmsy Ismax nbp_couple alpha phif Rstat;

%nombre d'iterations maxi du Simplexe:800. 
%options(3)=1e-2;
options(14)=800; 
%initialisation du compteur d'iterations du Simplexe. 
count=0;
tic;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% choix et chargement du fichier moteur
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%execution du fichier
eval(fichier(1:length(fichier)-2));

%affectation des variables
Ld=ACM1.Ld;
Lq=ACM1.Lq;
Rstat=ACM1.Rstat;
nbpolmsy=ACM1.nbpolmsy;
phif=ACM1.phif
cqfpmgvit=ACM1.cqfpmgvit;                %coefficient en omega
cqfpmgvit2=ACM1.cqfpmgvit2;		   %coefficient en omega carre
cqfpmgvit3=ACM1.cqfpmgvit3;              %coefficient en omega cube
cqfpfercons=ACM1.cqfpfercons;
cqfpfervit=ACM1.cqfpfervit;           %coefficient en omeg
cqfpfervit2=ACM1.cqfpfervit2;           %coefficient en omeg carre


Wmail_min=Wmail_min*pi/30;
Wmail_max=Wmail_max*pi/30;
pas_omeg=pas_omeg*pi/30;

%Ismax=120;
Vsmax=ubat*sqrt(2)/pi

i=1;
j=1;
cpl(i)=Cmail_min;
w(j)=Wmail_min;

%------boucle d'incrementation du couple
while cpl(i)<Cmail_max+pas_couple
   
%++++++boucle d'incrementation du regime (en rad/s)
while w(j)<Wmail_max+pas_omeg
%pas_omeg=0.13333*w(j)+20.944

Cmeca=cpl(i)
omega=w(j)
N=omega*30/pi;
Vs=50;

Pfer=(cqfpfercons+cqfpfervit*N+cqfpfervit2*N^2)*Vs^2;
Pfer1=0;
Pmeca=cqfpmgvit*N+cqfpmgvit2*N^2+cqfpmgvit3*N^3;

% boucle d'optim unidimensionnelle
while abs((Pfer1-Pfer)/Pfer)>0.01
Pfer1=Pfer;
if omega~=0
Cem=Cmeca+(Pfer1+Pmeca)/omega;
else
Cem=Cmeca;
end

idmin=-abs(Cem/0.3);  
idmax=abs(Cem/0.3);

sol2=fmin('calptot_msa',idmin,idmax,options);
Id=sol2;

Iq=Cem/(nbpolmsy*((Ld-Lq)*Id+phif));
%Tension sur l'axe direct Vd 
Vd=Rstat*Id-nbpolmsy*omega*Lq*Iq;
%Tension sur l'axe en quadrature Vq
Vq=Rstat*Iq+nbpolmsy*omega*(Ld*Id+phif);
%pertes totales
Ptot=Vd*Id+Vq*Iq;

Vs=sqrt((Vd^2+Vq^2)/3);

Pfer=(cqfpfercons+cqfpfervit*N+cqfpfervit2*N^2)*Vs^2;
Pjoule=Rstat*(Id^2+Iq^2);
Pconv=0;
Ptot_acm=Pfer+Pjoule+Pmeca+Pconv;

%%------------fin de la boucle sur les pertes fer
%************ optim unidim 
end

%%=============  Dans le cas de depassement de Vsmax

if Vs>=Vsmax
Idmaxtemp=Ismax*3;
Idmintemp=-Ismax*3;
%Id=-Ismax*2;
Id_test=0;
flag_deflux=1;
'entree deflux'
Cem
while abs(Vs-Vsmax)/Vsmax>0.01&abs(Id_test-Id)/abs(Id)>0.01
   Id_test=Id;
if Vs>Vsmax;
Idmaxtemp=Id;
Id=(Idmaxtemp+Idmintemp)/2;
end
if Vs<Vsmax;
Idmintemp=Id;
Id=(Idmaxtemp+Idmintemp)/2;
end

Pfer=(cqfpfercons+cqfpfervit*N+cqfpfervit2*N^2)*Vs^2;
Pfer1=0;
Pmeca=cqfpmgvit*N+cqfpmgvit2*N^2+cqfpmgvit3*N^3;

% boucle d homogeneisation pertes fer
while abs((Pfer1-Pfer)/Pfer)>0.01
Pfer1=Pfer;
if omega~=0
Cem=Cmeca+(Pfer1+Pmeca)/omega;
else
Cem=Cmeca;
end

%Tension sur l'axe direct Vd 
Vd=Rstat*Id-nbpolmsy*omega*Lq*Iq; 
%Tension sur l'axe en quadrature Vq
Vq=Rstat*Iq+nbpolmsy*omega*(Ld*Id+phif);

Iq=Cem/(nbpolmsy*((Ld-Lq)*Id+phif));
Vs=sqrt((Vd^2+Vq^2)/3);

Pfer=(cqfpfercons+cqfpfervit*N+cqfpfervit2*N^2)*Vs^2;
Pjoule=Rstat*(Id^2+Iq^2);
Pconv=0;
Ptot_acm=Pfer+Pjoule+Pmeca+Pconv;


%%------------fin de la boucle sur les pertes fer
%************  (cas Vsmax) sans optimisation
end
end
end

id_opt(i,j)=Id;
iq_opt(i,j)=Iq;

Is=sqrt((Id^2+Iq^2)./3);
Istat(i,j)=Is;
Vstat(i,j)=Vs;
Vsmax
Cmeca;

if Cmeca>0
   Cplmax(i,j)=Cmeca;
else
   Cplmax(i,j)=0;
end

if Cmeca>0
deltav=Vsmax*2/100;
deltai=Ismax*2/100;
if (Vstat(i,j)>(Vsmax+deltav))|((Istat(i,j)-deltai)>Ismax)
      Cplmax(i,j)=0;
end
end
Cemest(i,j)=nbpolmsy*((Ld-Lq)*Id+phif)*Iq;

Pt_acm(i,j)=Pfer+Pjoule+Pmeca+Pconv;


j=j+1;
w(j)=w(j-1)+pas_omeg;
end
j=1

i=i+1;

'ici calcul du couple suivant'
cpl(i)=cpl(i-1)+pas_couple

end

%verification du couple electromag
%Cem=p*((Ld-Lq)*id_opt+Mf*iex_opt)*iq_opt;

%grandeurs interessantes 
Is=sqrt((Id^2+Iq^2)./3)
Vs 
Vsmax
toc;

%     mise en forme des donnees a la sortie 
%     ecriture de la carto des pertes   
if ~isempty(fichier_mct)

fid=fopen(fichier_mct,'w');

fprintf(fid,'%s','% fichier acm1 genere pour VEHLIB a partir d un moteur')
fprintf(fid,'\n','')
fprintf(fid,'%s','% synchrone a aiment a base de bob135')
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.ntypmg=10;')
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Regmot_pert=[');
for i=1:length(w)-1
fprintf(fid,'%8.1f',w(i));
end
fprintf(fid,'%s','];')
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmot_pert=[');
for i=1:length(cpl)-1
fprintf(fid,'%8.1f',cpl(i));
end
fprintf(fid,'%s','];')

%fprintf(fid,'\n','');
%fprintf(fid,'\n','');
%for i=1:length(w)-1
% fprintf(fid,'%8.2f',Vstat(:,i));
% fprintf(fid,'\n','');
%end
%fprintf(fid,'\n','');
%fprintf(fid,'\n','');
%for i=1:length(w)-1
% fprintf(fid,'%8.2f',Istat(:,i));
% fprintf(fid,'\n','');
%end

fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Pert_mot=[');
for i=1:length(w)-1
 fprintf(fid,'%8.2f',Pt_acm(:,i));
 fprintf(fid,'\n','');
end
fprintf(fid,'%s','];');

fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.3f %s','ACM1.J_mg=',ACM1.J_mg,';');
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.3f %s','ACM1.Masse_mg=',ACM1.Masse_mg,';');

fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.1f %8.1f %8.1f','ACM1.Tension_cont=[',ubat*0.8,ubat,ubat*1.2);
fprintf(fid,'%s','];')

fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Regmot_cmax=[');
for i=1:length(w)-1
fprintf(fid,'%8.1f',w(i));
end
fprintf(fid,'%s','];')

%fprintf(fid,'\n','');
%fprintf(fid,'\n','')
%for i=1:length(w)-1
% fprintf(fid,'%8.2f',Cplmax(:,i));
% fprintf(fid,'\n','');
%end
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmax_mot=[');
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid,'%8.1f',cplmax2(i)*0.8);
end
fprintf(fid,'\n','')
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid,' %8.1f',cplmax2(i));
end
fprintf(fid,'\n','')
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid,' %8.1f',cplmax2(i)*1.2);
end
fprintf(fid,'%s','];');
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmax_mot=ACM1.Cmax_mot'';');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.Cmin_mot=-ACM1.Cmax_mot;');
fprintf(fid,'\n','');
fprintf(fid,'\n','');
fprintf(fid,'%s','%gradient de montee et descente du couple en Nm/s');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.1f %s','ACM1.grad_cmg_mont=',max(cplmax2)/ACM1.Tq,';');
fprintf(fid,'\n','');
fprintf(fid,'%s %8.1f %s','ACM1.grad_cmg_desc=',-max(cplmax2)/ACM1.Tq,';');
fprintf(fid,'\n','');

fprintf(fid,'\n','');
fprintf(fid,'%s','% Courbe de performances maxi sur l arbre du moteur de traction pour le calculateur');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.reg_trac_max=ACM1.Regmot_cmax;');
fprintf(fid,'\n','');
fprintf(fid,'%s','ACM1.cpl_trac_max=ACM1.Cmax_mot(:,length(ACM1.Tension_cont))'';');
fprintf(fid,'\n','');
fprintf(fid,'\n','');


statut=fclose(fid);

end

if ~isempty(fichier_com)
   
fid2=fopen(fichier_com,'w');

fprintf(fid2,'%s','% fichier commande optim genere � partir d un moteur')
fprintf(fid2,'\n','')
fprintf(fid2,'%s','% synchrone a aiment a base de bob135')
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.ntypcom=1;')
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');

%     mise en forme des donnees a la sortie 
%     ecriture des valeur trouvees de idopt et iqopt   


fprintf(fid2,'%s','ACM1.omeg_opt=[')
for i=1:length(w)-1
fprintf(fid2,'%8.1f',w(i));
end
fprintf(fid2,'%s','];');
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.cpl_opt=[');

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
for i=1:length(cpl)-1
fprintf(fid2,'%8.1f',cpl(i));
end
fprintf(fid2,'%s','];');

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.id_opt=[')
for i=1:length(w)-1
 fprintf(fid2,'%8.2f',id_opt(:,i));
 fprintf(fid2,'\n','');
end
fprintf(fid2,'%s','];')
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.iq_opt=[')
for i=1:length(w)-1
 fprintf(fid2,'%8.2f',iq_opt(:,i));
 fprintf(fid2,'\n','');
end
fprintf(fid2,'%s','];')

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s %8.1f %8.1f %8.1f','ACM1.Tension_cont=[',ubat*0.8,ubat,ubat*1.2);
fprintf(fid2,'%s','];')

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.Regmot_cmax=[');
for i=1:length(w)-1
fprintf(fid2,'%8.1f',w(i));
end
fprintf(fid2,'%s','];')

fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.Cmax_mot=[');
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid2,'%8.1f',cplmax2(i)*0.8);
end
fprintf(fid2,'\n','')
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid2,' %8.1f',cplmax2(i));
end
fprintf(fid2,'\n','')
for i=1:length(w)-1
cplmax2(i)=max(Cplmax(:,i));
fprintf(fid2,' %8.1f',cplmax2(i)*1.2);
end
fprintf(fid2,'%s','];');
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.Cmax_mot=ACM1.Cmax_mot'';');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.Cmin_mot=-ACM1.Cmax_mot;');
fprintf(fid2,'\n','');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','%limite du couple du au rapport cyclique');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.rapp_cycl=[0 0.97  0.99 1.01];');
fprintf(fid2,'\n','');
fprintf(fid2,'%s','ACM1.cplmax_rcyc=[150 150 150  0];');

statut=fclose(fid2);

end

