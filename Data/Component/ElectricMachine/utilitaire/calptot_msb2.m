% Function: calptot_msb2
%*********************************************************************
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%   Calcul des pertes pour un moteur synchrone bobin�
%     Ce calcul utilise le fichier de donn� du moteur
%*********************************************************************

function Ptot=calptot_msb2(v);

global imag_sat;
global Iex Iex_max flag_exmax;
global Mf_sat Ld_sat Lq_sat k1 k2;
global nbpolmsy saill ldd;
global Cem Rs Rf p N omega Ld Lq Mf count Vs Ptot Iq Vd Vq;


Id=v;
Iex;

p=nbpolmsy;

Istat1=0;
Is=100;
Iq=0;

while abs((Istat1-Is)/Is)>0.05
Istat1=Is;

imagn=abs(Iex+k1*Id+k2*abs(Iq));

Mf=interplin1(imag_sat,Mf_sat,imagn);
Ld=k1*Mf+ldd;
Lq=Ld/saill;

Iq=Cem/(p*((Ld-Lq)*Id+Mf*Iex));

%Tension sur l'axe direct Vd 
Vd=Rs*Id-p*omega*Lq*Iq; 
%Tension sur l'axe en quadrature Vq
Vq=Rs*Iq+p*omega*(Ld*Id+Mf*Iex);
%pertes totales
Ptot=Vd*Id+Vq*Iq+Rf*Iex^2;

Is=sqrt((Id^2+Iq^2)/3);

end


