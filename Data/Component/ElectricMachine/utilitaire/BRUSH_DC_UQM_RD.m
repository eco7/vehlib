% File: BRUSH_DC_UQM_RD
% fichier rendement a partir des donnees constructeur
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% du Moteur Unique mobility Brushless DC UQM
% tension d alimentation continue 180 V
% source : plaquette constructeur

Regmot_pert=[     0.0  52.35  105.8 157.07  210.5 261.8  315.2   419.9   524.6   629.4   734.1  837.75];

Cmot_pert=[  0.0   10.0  20.0    40.0    60.0    80.0   100.0   120.0   140.0   160.0];

Rend_mot=[   5      5     5       5       5       5       5       5       5       5
                  5     62     63     63      65      65      66      65      62      60
                  5     65     79     81      80      78      77      76      75      70
                  5     65     80     85      85.5    85      83      82      81      80
                  5     65     81     87.5    87.5    87.5    86      85.5    85.2    83
                  5     65     85     90.5    90.5    90.5    89.5    87      86      83
                  5     68     87     92      92      91      89      87      85.5    81.5
                  5     68     90     93      89.5    87.5    86.5    84      82      77
                  5     68     83     88      87      85      82.5    80      76      70
                  5     68     82.5   86      84      82      78      75      70      60
                  5     65     80     83.5    81.5    76.5    73      67      60      50
                  5     63     75     80      75      70      62      58      50      40];
                  
                  
                    

