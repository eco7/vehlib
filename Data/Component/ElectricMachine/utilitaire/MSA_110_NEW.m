% File: MSA_110_NEW
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%         fichier donn�es d'un moteur synchrone � aiment
%            Moteur INRETS  Radio Energie BOB110
%            avec rotor modifi� (aiment au lieu de bobinage)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
display('initialisation des parametres Moteur MSA 110');

global Rstat Ld Lq phif

% resistances a 20 deg	R(T)=R(20)*(1+alpha*(T-20))
Rstat=0.0965;     %  resit stator � 20� en Ohm
alpha_stat=4.0e-4;% coeff de temper stat

Ld=0.003;
Lq=0.003;

phif=0.4


Td=0.05;
Tq=0.05;
%nombre de paires de poles
nbpolmsy=3;

% pour toutes les pertes omega est en tr/mn     
% pertes mecaniques
cqfpmgvit=-0.0123;                %coefficient en omega
cqfpmgvit2=2e-5;		   %coefficient en omega carr�
cqfpmgvit3=0;              %coefficient en omega cube

%inertie
Jmsy=0.15;                % moment d'inertie du mcc en kgm2

% pertes fer
cqfpfercons=0.0303;
cqfpfervit=-8e-6;           %coefficient en omeg
cqfpfervit2=6e-10;           %coefficient en omeg carr�
