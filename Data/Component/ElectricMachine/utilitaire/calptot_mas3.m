% Function: calptot_mas3
%*********************************************************************
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%   Calcul des pertes pour un moteur asynchrone
%     Ce calcul utilise le fichier de donn� du moteur
%*********************************************************************

function Vsmin=calptot_mas3(v);

global Imag_sat Ls_sat sigma;
global Iex Iex_max flag_exmax;
global nbpolmas Tr;
global Cem Rs Rf p N omega Ls Lm count Ptot Iq Vd Vq Vsmin;


Id=v;

p=nbpolmas;

Istat1=0;
Is=100;
Iq=0;

while abs((Istat1-Is)/Is)>0.05
Istat1=Is;

imagn=sqrt((Id^2+sigma^2*Iq^2)/3);

Ls=interplin1(Imag_sat,Ls_sat,imagn);
Lm=sqrt(1-sigma)*Ls;

Iq=Cem/(p*(1-sigma)*Ls*Id);

omegar=(1/Tr)*(Iq/Id);
omegas=p*omega+omegar;

%Tension sur l'axe direct Vd 
Vd=Rs*Id-omegas*sigma*Ls*Iq; 
%Tension sur l'axe en quadrature Vq
Vq=Rs*Iq+omegas*Ls*Id;

Is=sqrt((Id^2+Iq^2)/3);

end

Vsmin=sqrt((Vd^2+Vq^2)/3);



