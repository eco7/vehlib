% File: carto_optim_idq
% fichier commande optim genere � partir d un moteur
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% synchrone a aiment a base de bob135

ACM1.ntypcom=1;

ACM1.omeg_opt=[   104.7   209.4];

ACM1.cpl_opt=[

    50.0    60.0];

ACM1.id_opt=[    3.21    4.54
    3.23    4.57
];

ACM1.iq_opt=[   51.82   61.69
   52.00   61.88
];

ACM1.Tension_cont=[    240.0    300.0    360.0];

ACM1.Regmot_cmax=[   104.7   209.4];

ACM1.Cmax_mot=[    48.0    48.0
     60.0     60.0
     72.0     72.0];

ACM1.Cmax_mot=ACM1.Cmax_mot';
ACM1.Cmin_mot=-ACM1.Cmax_mot;

%limite du couple du au rapport cyclique
ACM1.rapp_cycl=[0 0.97  0.99 1.01];
ACM1.cplmax_rcyc=[150 150 150  0];