% =====================================================
% fichier convertisseur entre reseau (primaire)
% et batterie (secondaire)
% ====================================================

% tension de travail cote secondaire
DCDC_1.Umin=0;
DCDC_1.Umax=Inf;

% rendement
DCDC_1.rend=0.97;

% Puissances maxi cote secondaire
DCDC_1.Pmax=240000;
DCDC_1.Pmin=-240000;

% Temps de reponse de la tension (Obsolete)
DCDC_1.tauS=0.02;

display('Fin d''initialisation des parametres du convertisseur DCDC 1');
