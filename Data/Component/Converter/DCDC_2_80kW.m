% =====================================================
% fichier convertisseur entre reseau (primaire)
% et super capacite (secondaire)
% ====================================================

% tension de travail cote secondaire
DCDC_2.Umin=0;
DCDC_2.Umax=inf;

% rendement
DCDC_2.rend=0.97;

% Puissances maxi cote secondaire
DCDC_2.Pmax=80000;
DCDC_2.Pmin=-80000;

% Temps de reponse de la tension
DCDC_2.tauS=0.02;


display('Fin d''initialisation des parametres du convertisseur DCDC 2');
