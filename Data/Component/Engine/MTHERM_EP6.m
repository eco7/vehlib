% File: MTHERM_EP6
% =============================================================================
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier moteur thermique
%                      Pour librairie VEHLIB
%                           INRETS - LTE
%  Objet: Ce script initialise la structure MOTH
%          contenant les champs necessaires pour renseigner
%          le composant MOTEUR THERMIQUE de la librairie VEHLIB.
%
% Moteur thermique 1598 cm3, 16 soupapes, VTi, 120ch
% equipant la 308
% =============================================================================

%reperage du type de moteur thermique: 10 = moteur thermique cartographie
MOTH.ntypmoth=10;

% Nom du moteur
MOTH.nom='MTHERM_EP6';

% masse du moteur thermique
MOTH.Masse_mth    = 140;

% inertie du moteur thermique Valeur non trouvee 
MOTH.J_mt        = 0.2 ; % inconnue;		

%................ESTIMATION DE LA CONSOMMATION
MOTH.ral_max    = 720*pi/30;			% regime maxi pour un ralenti (en rad/s)
MOTH.ral_min    = 620*pi/30;			% regime mini pour un ralenti (en rad/s)
MOTH.ral        = 700*pi/30;			% regime ralenti (en rad/s)
MOTH.ral_dem	 = 125;			    	% regime de fin d'action du demarreur (regulation)
MOTH.wmt_maxi=6000*pi/30;				% regime maxi regulation (en rad/s)

% Regulation de ralenti
MOTH.P_ral=0.1;
MOTH.I_ral=0;
MOTH.D_ral=0;

% Carburant essence
MOTH.dens_carb  = 740;			% densite du carburant
MOTH.CO2carb		= 1.85;					% rapport masses CO2 carburant pour une combustion parfaite 
%(mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85

% Reponse couple alpha du moteur:
MOTH.alpha_cons=[0 1];
MOTH.alpha2cpl=[0 1];

% Temps de reponse en couple du moteur
MOTH.tr=0.4; 

% Caracteristiques culasse
% Alesage (m)
MOTH.Alesage=0.077;
% Course (m)
MOTH.Course=0.0855;
% Volume util chambre
MOTH.Vd=MOTH.Course*pi*(MOTH.Alesage)^2/4;
% Nombre de cylindre
MOTH.Ncyl=4;
% Nombre de revolution par cycle (2 ou 4 temps)
MOTH.Rev=2;

% courbe enveloppe constructeur
MOTH.wmt_max_th =[1500 2000 2500 3000 3500 4000 4125 4250 4375 4500 5000 5500 5750 5875 6000 6125 6200 6250]*pi/30;
MOTH.cmt_max_th =[129 136 142 150 152 153.5 156 157 155.5 154 151.5 147.5 141 139 138.5 134 131 0];
% courbe enveloppe (estimation pleine charge en couple(Nm) en fct regime MT(rad/s)
MOTH.wmt_max=[ 0.0   500.0   850.0  1000.0  1250.0  1500.0  2000.0  2500.0  3000.0  3500.0  4000.0  4200.0  4250.0  4300.0  4500.0  5000.0  5500.0  5800.0  5900.0  5950.0  6000]*pi/30;
MOTH.cmt_max=[ 0.0     0.0   107.0   119.7   123.6   129.0   136.0   142.0   150.0   152.0   153.5   156.0   156.5   156.0   154.0   152.0   147.5   141.0   139.0   138.5   0];
MOTH.cmt_min=[ 0.0   -17.6   -18.1   -18.3   -18.7   -19.0   -19.3   -20.7   -21.7   -23.9   -26.4   -27.4   -27.6   -27.8   -28.6   -30.7   -32.6   -33.2   -33.5   -33.8  -34.3];

% Courbe de charge optimum regime couple du moteur thermique;
MOTH.wmt_opti=[   600.0   720.0   840.0  1200.0  1560.0  1800.0  2040.0  2279.0  2280.0  2520.0  2759.0  2760.0  2880.0  3120.0  3360.0  3720.0  3840.0  3960.0  4200.0  4440.0  4680.0  4920.0  5160.0  5400.0  5640.0]*pi/30;
MOTH.cmt_opti=[     0.0    45.3    77.7    81.6    83.7    90.7    96.0   100.2   114.5   116.6   118.3   130.1   136.0   136.0   136.0   131.6   136.0   140.1   139.9   139.7   139.5   139.3   139.2   139.0   138.9];
MOTH.pmt_opti = MOTH.cmt_opti.*MOTH.wmt_opti;

% Estimations de la consommation (g/s) du moteur thermique
MOTH.Reg_2dconso=pi/30*[     0.0   850.0  1000.0  1250.0  1500.0  1750.0  2000.0  2250.0  2500.0  2750.0  3000.0  3250.0  3500.0  3750.0  4000.0  4250.0  4500.0  4750.0  5000.0  5250.0  5500.0  5750.0  6000.0];
MOTH.Cpl_2dconso=[     0.0    10.0    20.0    30.0    40.0    50.0    60.0    70.0    80.0    90.0   100.0   110.0   120.0   130.0   140.0   150.0   160.0];
MOTH.Conso_2d=[ 
    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00
    0.12    0.17    0.22    0.28    0.33    0.38    0.42    0.47    0.54    0.60    0.66    0.84    1.06    1.27    1.49    1.71    1.94
    0.15    0.20    0.25    0.30    0.37    0.43    0.49    0.55    0.61    0.69    0.76    0.92    1.07    1.23    1.39    1.54    1.70
    0.19    0.25    0.30    0.37    0.44    0.51    0.59    0.66    0.74    0.83    0.93    1.09    1.27    1.44    1.61    1.78    1.95
    0.24    0.30    0.36    0.45    0.52    0.61    0.70    0.79    0.87    0.98    1.10    1.21    1.34    1.66    1.98    2.29    2.62
    0.24    0.33    0.42    0.52    0.60    0.70    0.80    0.91    1.02    1.13    1.25    1.39    1.54    1.84    2.13    2.43    2.73
    0.27    0.36    0.47    0.58    0.69    0.80    0.92    1.03    1.15    1.27    1.40    1.54    1.69    2.01    2.34    2.66    2.98
    0.28    0.40    0.52    0.64    0.75    0.89    1.03    1.16    1.28    1.42    1.56    1.72    1.87    2.19    2.50    2.82    3.14
    0.39    0.48    0.58    0.72    0.86    1.00    1.15    1.31    1.45    1.60    1.75    1.92    2.08    2.40    2.74    3.26    3.80
    0.40    0.52    0.64    0.79    0.94    1.11    1.27    1.44    1.61    1.77    1.94    2.11    2.29    2.46    2.63    3.67    4.71
    0.42    0.56    0.70    0.87    1.04    1.22    1.40    1.59    1.76    1.92    2.10    2.30    2.50    2.69    2.88    3.67    4.46
    0.45    0.62    0.79    0.97    1.15    1.34    1.52    1.71    1.90    2.09    2.29    2.50    2.70    2.90    3.11    3.81    4.53
    0.48    0.68    0.86    1.05    1.23    1.44    1.65    1.87    2.08    2.29    2.48    2.69    2.90    3.12    3.36    4.16    4.96
    0.54    0.73    0.93    1.13    1.35    1.57    1.79    2.00    2.21    2.44    2.66    2.88    3.12    3.34    3.62    4.43    5.25
    0.57    0.79    1.00    1.21    1.42    1.66    1.90    2.13    2.38    2.62    2.86    3.10    3.34    3.59    3.85    4.77    5.69
    0.65    0.87    1.10    1.32    1.54    1.80    2.06    2.30    2.55    2.81    3.06    3.34    3.62    3.88    4.16    4.92    5.67
    0.68    0.92    1.15    1.40    1.65    1.94    2.24    2.50    2.75    3.02    3.29    3.56    3.84    4.14    4.44    5.35    6.26
    0.77    1.01    1.26    1.52    1.79    2.08    2.37    2.63    2.88    3.18    3.47    3.77    4.06    4.41    4.75    5.75    6.74
    0.80    1.06    1.34    1.62    1.90    2.20    2.50    2.78    3.06    3.38    3.69    4.02    4.34    4.75    5.17    6.21    7.23
    0.90    1.16    1.42    1.71    2.01    2.32    2.63    2.93    3.23    3.57    3.92    4.29    4.68    5.21    5.74    6.71    7.68
    0.96    1.24    1.52    1.83    2.13    2.46    2.79    3.11    3.43    3.80    4.18    4.63    5.09    5.69    6.29    7.57    8.86
    1.10    1.37    1.63    1.96    2.28    2.62    2.96    3.30    3.66    4.05    4.45    4.93    5.46    6.11    6.82    8.32    9.82
    1.14    1.44    1.75    2.09    2.42    2.79    3.15    3.51    3.89    4.37    4.84    5.36    5.92    6.58    7.25    7.92    8.59
];

% Estimations des emissions de CO2 (g/s) du moteur thermique non disponible
MOTH.Reg_2dCO2=[ 1000.0  4500.0 ]*pi/30;
MOTH.Cpl_2dCO2=[  15.0   120.0 ];
MOTH.CO2_2d=[
   0 0
   0 0
];

% Estimations des emissions de CO (g/s) du moteur thermique
MOTH.Reg_2dCO=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dCO=[  15.0 120.0 ];
MOTH.CO_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de HC (g/s) du moteur thermique
MOTH.Reg_2dHC=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dHC=[  15.0  120.0 ];
MOTH.HC_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de NOx (g/s) du moteur thermique
MOTH.Reg_2dNOx=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dNOx=[   15.0  120.0 ];
MOTH.NOx_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de Particules (g/s) du moteur thermique
MOTH.Reg_2dPart=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dPart=[   15.0  120.0 ];
MOTH.Part_2d=[
   0 0
   0 0
]./1000;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Thermique moteur (valeur reprise des autres mod???les MTHERM)

% Pour inhiber le calcul de thermique moteur (=0)
MOTH.modele_therm=0;
% Temp initiale moteur
MOTH.Tinit=25;
% Part du carburant transforme en chaleur
MOTH.pci_to_chaleur = 0.617;
% Capacite calorifique equivalente du bloc (J/K)
MOTH.thetaC =41860;
% coefficient de convection
MOTH.k_ext_vit0 =12.53;
MOTH.k_ext_vit =4.85;
% Coefficient de convection radiateur  d eau
MOTH.k_ext_vit0_calor =6.45;
MOTH.k_ext_vit_calor =2.0;
% Temp d ouverture du calorstat
MOTH.Touv_calorstat =100;
% Regulation calorstat ouvert (!)
MOTH.Tcons=95;
MOTH.Kp_calor=1;
MOTH.pci=43000;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Emissions typiques  pour un moteur a allumage command?? stoechiometrique
% Typical exhaust gas concentration of a port-injected SI engine (G&O p 93)
MOTH.emissionModel = 1; % Essence stoechio TWC
MOTH.lambda=[0.8 0.9 1.0 1.065 1.1 1.2 1.3];
MOTH.HCppm=[4000 2500 1500 1000 900 850 1300];
MOTH.COppm=[75000 40000 10000 9000 8500 8500 8500];
MOTH.NOxppm=[1000 1500 2500 4000 3000 1000 500];
MOTH.AStoechio=14.7;

% Carto de richesse (arbitraire)
MOTH.wmtRichesse=[600 2000 3000 4000 5000 6000]*pi/30;
MOTH.chargeRichesse=[0 .85 .9 1];
MOTH.Richesse=[1 1 1 1 1 1; 1 1 1 1 1 1; 1.1 1.1 1.1 1.1 1.1 1.1; 1.2 1.2 1.2 1.2 1.2 1.2];

% Exhaust gas Temperature (N en rd/s, Texh en K)
% BJ;21-Jan-2016
% genere par fitTechapData.m
% Pour les couple negatifs, il s'agit de valeurs quand il y a une
% combustion. Sans combustion, il semble que les echauffements internes
% (pompage ...) entraine une temperature de l'ordre de 420K
MOTH.Reg_2dTexh=pi/30*[0  1500.0  2000.0  2500.0  3000.0  3500.0  4000.0  4500.0  5000.0  5500.0  6000.0];

MOTH.Cpl_2dTexh=[   -40.0   -20.0     0.0    20.0    40.0    60.0    80.0   100.0   120.0   140.0   160.0];

MOTH.Texh_2d=[
%-40    -20     0
 600.00 600.00  630.00  728.57  809.52  867.78  903.33  903.33  903.33  903.33  903.33
 600.00 600.00  630.00  728.57  809.52  867.78  903.33  903.33  903.33  903.33  903.33
 600.00 600.00  635.91  766.15  827.69  889.23  950.00  950.00  950.00  950.00  950.00
 600.00 600.00  690.00  912.00  927.14  955.71  955.71  955.71  955.71  955.71  955.71
 600.00 600.00  738.31  991.07  998.21  998.21  998.21  998.21  998.21  998.21  998.21
 600.00 998.21  998.21 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43
 600.00 600.00 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43
 600.00 600.00 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43
 600.00 600.00 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43
 600.00 600.00 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43
 600.00 600.00 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43 1016.43
]';

% Temperature sans combustion, mais moteur tournant
MOTH.Texh_min=420;

% Average molecular weight of exhaust gas
MOTH.Mexh=30; % g/mol

display('Fin d''initialisation des parametres moteur thermique'); 
