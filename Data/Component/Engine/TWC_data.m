% File: TWC_DATA
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier Three Ways Catalyst
%                      Pour librairie VEHLIB
%                          IFSTTAR - LTE 
%  Objet: Ce script initialise la structure TWC
%          contenant les champs necessaires pour renseigner 
%          le composant Catalyseur 3 voies de la librairie VEHLIB.
%
%
% 06/06/2016 : VP - BJ 
% -------------
% =============================================================================

TWC.ntypcata=2; %type = 1 modele 0D 1 zone type = 2 modele 0D multi-zone


% Parametres pour la thermique du catalyseur (Byron T Shaw A simplified
% coldstart catalyst ...)
TWC.MCata=0.5; % Masse kg
TWC.CpCata=500; % Capacité calorifique J/kgK

%Parameters identified from cool down trial
TWC.HsA=0.0911; 
TWC.HinA_Natural=0.193; % Internal heat transfer coefficient * Surface (J/K)

%TWC.Kreac=64;
TWC.T_LightOff=450; % in K

TWC.Length=0.15;
TWC.D=0.05;
TWC.GasDensity=1.29*(300/500);
TWC.MGaz=pi*TWC.D*TWC.D/4*TWC.Length*TWC.GasDensity;
TWC.CpGaz=1100;
TWC.debitGaz=0.0067;%1.6*1500/60/2*1.2e-3*ones(size(time))
TWC.Acs=pi*(TWC.D/2)^2;
TWC.Vcs=2e-4;
TWC.RhoS=7850;%density of monolith

TWC.e=0.9;
TWC.Ageo=5000;

%Parameters identified from warmup RICH cycle for more 9 zones
% TWC.HinA=400;
% TWC.Kreac=602;
% TWC.lambda=-400;
%Parameters identified from warmup RICH cycle for more 1 zones
TWC.HinA=4500;
TWC.Kreac=5500;
TWC.lambda=-400;

% Parametres pour la thermique du collecteur
TWC.MColl=2; 
TWC.CpColl=561;
TWC.HsAColl=0.38;
TWC.HsinAColl=23.49;

% Relative Oxygen Model (Guzella & Onder p 117)
TWC.OxygenToAirRatio=0.21; % Oxygen to air ratio (%)
TWC.OSC=400e-6; % Maximum Oxygen Storage Capacity (kg)

% Static efficiency Curve
% Inspire de Guzella & onder p 6 pour l'efficacite de conversion fontction de T
% Extrait de Byron T Shaw: A simplified coldstart catalyst thermal model to
% reduce hydrocarbon emissions pour la focntion de wiebe 2D (exces d'air, T)
% Wiebe fonction:
% y=1-exp(-a*((u-u0)/du)^m)
% u0 : ordinate at y=10%
% du : difference in u from y=90% et y=10%
% a et m : fitting parameter


% La fonction est modifiee; difficile de faire qque chose en 2D sinon.
%y=exp(-a*(du./(u-u0)).^m);

% Pour la temperature
TWC.aT=.8;
TWC.dT=200;
TWC.T0=350;
TWC.mT=14;

% Pour les CO
TWC.aCO=0.1233; %1.7807;
TWC.dCO=0.6190; %0.4856;
TWC.CO0=1.5694; %5.2867;
TWC.mCO=-10; %-66;

% Pour les HC
TWC.aHC=0.8238; %1.3228;
TWC.dHC=0.4263; %0.3310;
TWC.HC0=0.2888; %0.3463;    
TWC.mHC=6; %4;   

% Pour les NOx
TWC.aNOx=2.079; %0.7519;
TWC.dNOx=0.0931; %0.0996;
TWC.NOx0=1.1103; %1.1114;
TWC.mNOx= 30; %14;

% Enthalpie standard de formation (1bar, 298K)
TWC.dHf0CO2=-395.509*1e3; %J/mol
TWC.dHf0CO=-110.525*1e3;
TWC.dHf0O2=0;
TWC.dHf0CH4=-74.87*1e3;
TWC.dHf0H20=-241.818*1e3;
TWC.dHf0C3H6=20.410*1e3;

% Oxydation CO : CO+0.5O2 -> CO2
TWC.dH0CO=TWC.dHf0CO2-(TWC.dHf0CO+0.5*TWC.dHf0O2);
% Oxydation CH4 : CH4+2O2 -> CO2+2H2O
TWC.dH0CH4=TWC.dHf0CO2+2*TWC.dHf0H20-(TWC.dHf0CH4+2*TWC.dHf0O2);
% Oxydation C3H6 : C3H6+4.5O2 -> 3CO2+3H2O
TWC.dH0C3H6=3*TWC.dHf0CO2+3*TWC.dHf0H20-(TWC.dHf0C3H6+4.5*TWC.dHf0O2);

% Norme d'emission EURO IV reglementaires en g/km
TWC.CO_EURO4=1;
TWC.THC_EURO4=0.1;
TWC.NOx_EURO4=0.08;

% Norme d'emission EURO V reglementaires en g/km
TWC.CO_EURO5=1000e-3;
TWC.THC_EURO5=100e-3;
TWC.NMHC_EURO5=68e-3;
TWC.NOx_EURO5=60e-3;
TWC.PMWeight_EURO5=5e-3;
