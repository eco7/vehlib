# Engine types in vehlib. 
Created December 2021

Pour le moteur, une combinaison de **ntypmoth** qui décrit le modèle énergétique, et **emissionModel** qui décrit le modèle d'émission en sortie moteur. Tout deux font parties de la structure **MOTH**.

Pour le catalyseur, **ntypcata** décrit le modèle : modèle de thermique et d'efficacité.
___

## MOTH.ntypmoth = 10

- Fuel consumption maps as a function of speed and troque
- Maximum and minimum efficiency curves as a function of engine speed

#### example : EP6 engine (Peugeot gasoline IDI 1.6Liter) in MTHERM_EP6.m
#### Backward model in function : calc_mt.m
#### Forward model in library : VEHLIB.slx 
#### test Cases : a lot

## MOTH.ntypmoth == 2 

 - Modèle de rendement indiqué constant type G&0

#### example : MTHERM_EP6_mt_niv0.m / Peugeot_308SW_1L6VTI16V_mt_niv0.m
#### function : calc_mt_niv0


## MOTH.ntypmoth == 3

    - ISAT model. See Alice Guille des Buttes PhD, first part.

#### example : 
#### function : 

## MOTH.ntypmoth == 5

    - Model that take into account engine control parameters (air to fuel ratio and spark advance). See Alice Guille des Buttes PhD, second part

#### example : MTHERM_CONTROL_1L6.m
#### function : calc_mt_3U. et calc_mt_3U_reverse.m

___

## MOTH.emissionModel = 1

- Exhaust emissions : concentrations curves function of air to fuel ratio
- air to fuel ratio : map function of engine speed and engine charge
- Exhaust temperature : map function of speed and torque

#### example : MTHERM_EP6

## MOTH.emissionModel = 2
- emission model : See the first part of Alice Guille des Buttes Phd
- linear concentrations models function of engine PME

#### example : MTHERM_ISAT_DRIVE_1L6.m

## MOTH.emissionModel = 3
- emission model : See the second part of Alice Guille des Buttes Phd
- concentration models function of engine parameters : air to fuel ratio, spark advance

#### example : MTHERM_CONTROL_1L6.m

___

## TWC.ntypcata = 1

- modeles thermiques collecteur et modele 0D TWC 1 zone. A approfondir. Article V Pandey
- efficacité fonction de la température et de l'excès d'air

## TWC.ntypcata = 2

- modele thermiques collecteur et modele 0D TWC multi-zone. A approfondir. Article V Pandey
- efficacité fonction de la température et de l'excès d'air

## TWC.ntypcata = 3

- modele thermiques catalyseur "under roof". Modèle de la thèse d'Alice Guille des Buttes
- efficacité fonction de la température et de l'excès d'air

#### example : TWC_data

___

## Emissions Calculation :

### MOTH.ntypmoth == 10 and MOTH.emissionModel = 1 and TWC.ntypcata = 2

#### Backward function : calc_VTH_BV.m

#### Forward model : VEHLIB.mdl; VTH_BV_TWC.mdl


#### test Cases : test_backward_VTH/testVTH_BV_EP6_TWC_WLTC

### MOTH.ntypmoth == 3 and MOTH.emissionModel = 2 and TWC.ntypcata = 3

### MOTH.ntypmoth == 5 and MOTH.emissionModel = 3 and TWC.ntypcata = 3

#### Backward function : optimisation : calc_VTH_BV_2U_TWC.m et calc_HP_BV_2EMB_3U_TWC.m

#### test Cases : test_backward_VTH/test_backward_VTH_BV_2U_TWC et test_backward_VTH/test_backward_HP_BV_2EMB_TWC
