% File: TWC_DATA
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier Three Ways Catalyst
%                      Pour librairie VEHLIB
%                          IFSTTAR - LTE 
%  Objet: Ce script initialise la structure TWC
%          contenant les champs necessaires pour renseigner 
%          le composant Catalyseur 3 voies de la librairie VEHLIB.
%
%
% 13/09/18 : AGdB BJ
% -------------
% =============================================================================

TWC.ntypcata=3; %type = 1 modele 0D 1 zone type = 2 modele 0D multi-zone type = 3 0D eff(lambda) issue des mesures banc

% Parametres pour la thermique du catalyseur 
% (modele : Byron T Shaw A simplified coldstart catalyst ...)


%Parameters identified from cool down trial
TWC.HsA_mCp = 8.080226898193362e-04; 
%HinA_mCp = TWC.Hin_coeff(1)*dgaz + TWC.Hin_coeff(2);
TWC.Hin_coeff(1) = 0.003284741648433;
TWC.Hin_coeff(2) = -0.011069865742979;
TWC.K_mCp = 0.001718676757813;

% Static efficiency Curve
% Inspire de Guzella & onder p 6 pour l'efficacite de conversion fontction de T
% Pour la richesse donnes du banc fittes avec Wiebe :
% y = 1 - kPoll*exp(-a*((u-u0)/du)^m)
% u0 : ordinate at y=10%
% du : difference in u from y=90% et y=10%
% a et m : fitting parameter
% Wiebe modifie avec kPoll pour CO et HC

% Pour la temperature
TWC.aT=.8;
TWC.dT=200;
TWC.mT=14;
TWC.T0=350;

% Pour les CO
TWC.a_CO = 0.142894184039207;
TWC.m_CO = 9.136083142932408;
TWC.dlambda_CO = 0.129156403764628;
TWC.lambda0_CO = 0.815070399605557;

% Pour les HC
TWC.a_HC = 0.001120370504582;
TWC.m_HC = 5.771178922737123;
TWC.dlambda_HC = 0.127764833423593;
TWC.lambda0_HC = 0.502422765667229;

% Pour les NOx
TWC.a_NOx = 0.141588340025842;
TWC.m_NOx = 3.580292025759304;
TWC.dlambda_NOx = 0.016757819020979;
TWC.lambda0_NOx = 0.985587428791618;

% Enthalpie standard de formation (1bar, 298K)
TWC.dHf0CO2=-395.509*1e3; %J/mol
TWC.dHf0CO=-110.525*1e3;
TWC.dHf0O2=0;
TWC.dHf0CH4=-74.87*1e3;
TWC.dHf0H20=-241.818*1e3;
TWC.dHf0C3H6=20.410*1e3;

% Oxydation CO : CO+0.5O2 -> CO2
TWC.dH0CO=TWC.dHf0CO2-(TWC.dHf0CO+0.5*TWC.dHf0O2);
% Oxydation CH4 : CH4+2O2 -> CO2+2H2O
TWC.dH0CH4=TWC.dHf0CO2+2*TWC.dHf0H20-(TWC.dHf0CH4+2*TWC.dHf0O2);
% Oxydation C3H6 : C3H6+4.5O2 -> 3CO2+3H2O
TWC.dH0C3H6=3*TWC.dHf0CO2+3*TWC.dHf0H20-(TWC.dHf0C3H6+4.5*TWC.dHf0O2);

% Norme d'emission EURO IV reglementaires en g/km
TWC.CO_EURO4=1;
TWC.THC_EURO4=0.1;
TWC.NOx_EURO4=0.08;

% Norme d'emission EURO V reglementaires en g/km
TWC.CO_EURO5=1000e-3;
TWC.THC_EURO5=100e-3;
TWC.NMHC_EURO5=68e-3;
TWC.NOx_EURO5=60e-3;
TWC.PMWeight_EURO5=5e-3;

% Norme d'emission EURO 6d reglementaires en g/km
TWC.CO2_Cafe=95;
TWC.Conso_Cafe=4.041;
TWC.CO_EURO6=1000e-3;
TWC.THC_EURO6=100e-3;
TWC.NMHC_EURO6=68e-3;
TWC.NOx_EURO6=60e-3;
TWC.PMWeight_EURO6=5e-3;
TWC.PMnumber_EURO6=6e11;
