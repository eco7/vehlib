% File: MTHERM_XUD9A
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier moteur thermique
%                      Pour librairie VEHLIB
%                           INRETS - LTE
%  Objet: Ce script initialise la structure MOTH
%          contenant les champs necessaires pour renseigner 
%          le composant MOTEUR THERMIQUE de la librairie VEHLIB.
% =============================================================================

% Reperage du type de moteur thermique: 10 = moteur thermique cartographie
MOTH.ntypmoth=10;

% Nom du moteur
MOTH.nom='MTHERM_XUD9A';

% masse du moteur thermique
MOTH.Masse_mth    = 119;

% Inertie du moteur thermique
MOTH.J_mt        = 0.21;

% Regimes de fonctionnement
MOTH.ral_max    = 900*pi/30;			% regime maxi pour un ralenti (en rad/s)
MOTH.ral_min    = 800*pi/30;			% regime mini pour un ralenti (en rad/s)
MOTH.ral        = 850*pi/30;			% regime ralenti (en rad/s)
MOTH.ral_dem	= 900*pi/30;			% regime de fin d'action du demarreur (regulation)
MOTH.wmt_maxi=4500*pi/30;				% regime maxi regulation (en rad/s)

% Regulation ralenti
MOTH.P_ral=0.1;
MOTH.I_ral=0;
MOTH.D_ral=0;

% Carburant Diesel
MOTH.dens_carb  = 830;					% densite du carburant a 20 C (g/l)
MOTH.CO2carb	= 2.0;					% rapport masses CO2 carburant pour une combustion parfaite 
%(mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85

% Reponse couple alpha du moteur:
MOTH.alpha_cons=[0 1];
MOTH.alpha2cpl=[0 1];

% Temps de reponse en couple du moteur
MOTH.tr=0.6;

% Courbe enveloppe (estimation pleine charge et coupure d'injection(Nm) en fct regime MT(rad/s)
MOTH.wmt_max = [0  50 500 1000 1500 2000 2500 3000 3500 4000 4500 4700 5000]*pi/30;
MOTH.cmt_max = [0   0   0  102  112  120  121  118  116  112  103    0    0];
MOTH.cmt_min = [0 -15 -15  -17  -17  -20  -20  -28  -35  -40  -50  -50  -50]; 
MOTH.pmt_max = MOTH.cmt_max.*MOTH.wmt_max;   % pour le calculateur

% Courbe optimum utilisee par le calculateur
MOTH.wmt_opti = [0 500 1000 1500 2000 2500 3000 3500 4000 4500]*pi/30;
MOTH.cmt_opti = [0   0  102  112  120  121  118  116  112  103].*0.8;
MOTH.pmt_opti = MOTH.cmt_opti.*MOTH.wmt_opti;

% Estimations de la consommation (g/s) du moteur thermique XUD9A
% Valeurs de regimes en rd/s
MOTH.Reg_2dconso=[ 0 750  1000.0  1500.0  2000.0  2500.0  3000.0  3500.0  4000.0  4500.0 ]*pi/30;
% Valeurs de couples en Nm
MOTH.Cpl_2dconso=[   0.0   15.0   30.0   60.0   90.0  110.0  120.0 ];
% Valeurs de debit de carburant en g/s
MOTH.Conso_2d=[
   %cpl0.0   15.0         30.0         60.0        90.0         110.0        120.0
   0.00      0.00         0.00         0.00        0.00         0.00         0.00
   0.10      0.15         0.20         0.30        0.45         0.60         0.70
	0.113		0.204272		0.285035		0.472765		0.675428		0.8018			0.8650	
	0.170		0.316000		0.435556		0.694815		0.973370		1.212500		1.3321	
	0.266		0.458983		0.620000		0.950000		1.312857		1.598571		1.7410	
	0.353		0.632120		0.884837		1.276122		1.666571		2.025427		2.2097	
	0.532		0.812247		1.065186		1.583061		2.081379		2.529655		2.7538	
	0.750		1.063081		1.337509		1.970550		2.586314		3.043652		3.2723	
	1.000		1.371627		1.728333		2.476350		3.189764		3.737061		4.0107	
	1.400		1.813046		2.222480		3.027905		3.874797		4.5021			4.8158	
];

% Estimations des emissions de CO2 (g/s) du moteur thermique XUD9A
MOTH.Reg_2dCO2=[ 1000.0  1500.0  2000.0  2500.0  3000.0  3500.0  4000.0  4500.0 ]*pi/30;
MOTH.Cpl_2dCO2=[  15.0   30.0   60.0   90.0  110.0  120.0 ];
MOTH.CO2_2d=[
	0.617379		0.895745		1.559280		2.302416		2.7559			2.9827	
	0.914000		1.303889		2.202037		3.138514		4.000833		4.4320	
	1.359153		1.910000		2.940000		4.199834		5.183223		5.6749	
	1.880924		2.606467		3.936735		5.336000		6.631263		7.2967	
	2.401966		3.187797		4.967891		6.798759		8.495310		9.3436	
	3.299593		4.214505		6.395704		8.580819		10.034744		10.762	
	4.196084		5.239167		7.924234		10.505068		12.491554		13.485	
	5.541060		6.919680		9.684150		12.863579		15.254700		16.450	
];

% Estimations des emissions de CO (g/s) du moteur thermique XUD9A
MOTH.Reg_2dCO=[ 1000.0  1500.0  2000.0  2500.0  3000.0  3500.0  4000.0  4500.0 ]*pi/30;
MOTH.Cpl_2dCO=[  15.0   30.0   60.0   90.0  110.0  120.0 ];
MOTH.CO_2d=[
		1.695311		1.442589		1.104697		0.643520		0.1104			0.1			
		4.375000		3.778389		1.034870		0.350293		0.087250		0.05	
		8.478932		6.373000		0.860000		0.172256		0.097837		0.05
		14.368940		17.992582		1.823929		0.312371		0.173853		0.1424	
		14.006101		12.632529		2.980381		0.562276		0.551931		0.5468	
		7.802023		2.081208		1.632639		0.718198		0.519563		0.4202	
		3.879301		4.520917		1.748095		0.729899		0.850169		0.9103	
		4.261430		2.392032		0.790091		0.906351		1.5137			1.8174	
]./1000;

% Estimations des emissions de HC (g/s) du moteur thermique XUD9A
MOTH.Reg_2dHC=[ 1000.0  1500.0  2000.0  2500.0  3000.0  3500.0  4000.0  4500.0 ]*pi/30;
MOTH.Cpl_2dHC=[  15.0   30.0   60.0   90.0  110.0  120.0 ];
MOTH.HC_2d=[
		0.188010		0.156585		0.112705		0.091245		0.0942			0.0957	
		0.503600		0.469444		0.193519		0.132543		0.119500		0.1130	
		1.713678		1.373000		0.440000		0.213668		0.180445		0.1640	
		3.682723		7.815060		1.854010		0.559571		0.403007		0.3579	
		2.061326		3.432207		1.003272		0.432648		0.313338		0.2680	
		0.968948		0.671686		0.421883		0.332072		0.284290		0.2604	
		0.842717		1.049833		0.758120		0.489243		0.240595		0.1163	
		0.898490		0.770832		0.514585		0.230886		0.1				0.1	
]./1000;

% Estimations des emissions de NOx (g/s) du moteur thermique XUD9A
MOTH.Reg_2dNOx=[ 1000.0  1500.0  2000.0  2500.0  3000.0  3500.0  4000.0  4500.0 ]*pi/30;
MOTH.Cpl_2dNOx=[   15.0   30.0   60.0   90.0  110.0  120.0 ];
MOTH.NOx_2d=[
		3.615534		4.084149		3.808561		6.482416		10.9359		13.1627	
		3.834000		4.656111		5.424630		15.170036		14.075833		13.5287	
		4.553898		5.520000		15.590000		23.892027		23.094684		22.7000	
		5.823859		6.207011		17.553265		28.362000		28.817440		28.711638	
		8.186742		14.897966		23.800136		36.001034		36.587241		36.88	
		11.884593		16.543959		26.244880		37.193754		39.357577		40.4395	
		14.290602		24.120833		37.261496		51.034189		56.196351		58.7774	
		22.147748		30.477840		48.195573		62.518487		67.013			69.26	
]./1000;

% Estimations des emissions de Particules (g/s) du moteur thermique XUD9A
MOTH.Reg_2dPart=[ 1000.0  1500.0  2000.0  2500.0  3000.0  3500.0  4000.0  4500.0 ]*pi/30;
MOTH.Cpl_2dPart=[   15.0   30.0   60.0   90.0  110.0  120.0 ];
MOTH.Part_2d=[
		0.519320			2.708156			5.084280			2.711115			2.3					1.65	
		0.348000			0.896667			1.507778			2.775942			9.326667			12.6	
      1.451356			0.570000			2.440000			1.014684			2.483123			3.2		
      0.989891			2.669239			1.562449			1.163429			2.537611			3.3	
		1.912472			5.830102			2.908027			2.766759			6.863310			8.4	
		1.344477			1.889590			1.452749			2.672765			10.829761			14.9	
		1.912711			2.328333			1.553504			4.655676			11.520541			14.9	
		3.755828			3.523120			4.621423			10.306605			17.7				21.4	
]./1000;

% Thermique moteur

% Pour inhiber le calcul de thermique moteur
MOTH.modele_therm=0; 
% Temp initiale moteur
MOTH.Tinit=25;
% Part du carburant transforme en chaleur
MOTH.pci_to_chaleur = 0.617;
% Capcite calorifique equivallente du bloc (J/K)
MOTH.thetaC =41860;
% coefficient de convection
MOTH.k_ext_vit0 =12.53;
MOTH.k_ext_vit =4.85;
% Coefficient de convection radiateur  d eau
MOTH.k_ext_vit0_calor =6.45;
MOTH.k_ext_vit_calor =2.0;
% Temp d ouverture du calorstat
MOTH.Touv_calorstat =85;
% Regulation calorstat ouvert (!)
MOTH.Tcons=80;
MOTH.Kp_calor=1;
MOTH.pci=43000;

display('Fin d''initialisation des parametres moteur thermique'); 

%------------------------------------MISES A JOUR-----------------------------------%
% 18-12-06 (EV/RT) : rajout PID ralenti en parametres
% 09-05-07 (BJ) : modif gradient de montee en regime 140 --> 200 N.m/s 
%                (pb convergence calculs debut simulation pas de temps 1e-15)
