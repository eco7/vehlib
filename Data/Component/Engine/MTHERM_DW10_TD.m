% File: MTHERM_DW10_TD
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier moteur thermique
%                      Pour librairie VEHLIB
%                             INRETS - LTE 
%  Objet: Ce script initialise la structure MOTH
%          contenant les champs necessaires pour renseigner 
%          le composant MOTEUR THERMIQUE de la librairie VEHLIB.
%          
%  Donnees a partir de la cartographie de B. Malaquin Aout 2002.
% =============================================================================

%reperage du type de moteur thermique: 10 = moteur thermique cartographie
MOTH.ntypmoth=10;

%nom du moteur
MOTH.nom='MTHERM_DW10_TD';

% masse du moteur thermique
MOTH.Masse_mth    = 155;	% Estimation par linearisation de Mveh=f(Pmotherm);

% Inertie du moteur thermique
MOTH.J_mt        = 0.15;

% Regimes de fonctionnement
MOTH.ral_max    = 900*pi/30;                     % regime maxi pour un ralenti (en rad/s)
MOTH.ral_min    = 800*pi/30;                     % regime mini pour un ralenti (en rad/s)
MOTH.ral        = 820*pi/30;                     % regime ralenti (en rad/s)
MOTH.ral_dem    = 900*pi/30;                     % regime de fin d'action du demarreur (regulation)
MOTH.wmt_maxi   =5000*pi/30;                     % regime maxi regulation (en rad/s)

% Carburant Diesel
MOTH.dens_carb  = 830;                                   % densite du carburant a 20 C (g/l)
MOTH.CO2carb    = 2.0;                                  % rapport masses CO2 carburant pour une combustion parfaite
%(mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85

% Reponse couple alpha du moteur:
MOTH.alpha_cons=[0 1];
MOTH.alpha2cpl=[0 1];

%gradient de couple maximum
MOTH.grd_cmt_mont=140;
MOTH.grd_cmt_desc=-7000;

% regulation ralenti
MOTH.P_ral=0.1;
MOTH.I_ral=0;
MOTH.D_ral=0;


% courbe enveloppe (estimation pleine charge et coupure d'injection (Nm) en fct regime MT(rad/s)
%MOTH.pmt_max = MOTH.cmt_max.*MOTH.wmt_max;   % pour le calculateur

MOTH.wmt_max=[0  50  500   820   1000   1500.1   2000.0   2499.9   3000.1   3500.0   4000.1   4500.0   5000.0   5100.0]*pi/30;
MOTH.cmt_max=[0  0  0     50    100    187.3    210.4    207.7    189.6    170.3    148.3    118.3     15.2      0.0];

MOTH.cmt_min=[0 -10 -10.0  -15.0  -20.0   -28.0    -32.0     -38.0    -44.0    -53.0    -62.0    -72.0    -87.0    -90.0];

MOTH.pmt_max=MOTH.wmt_max.*MOTH.cmt_max;

% courbe optimum utilisee par le calculateur
MOTH.wmt_opti = MOTH.wmt_max(3:length(MOTH.wmt_max)-4);
MOTH.cmt_opti = MOTH.cmt_max(3:length(MOTH.cmt_max)-4)*0.8;
MOTH.pmt_opti = MOTH.cmt_opti.*MOTH.wmt_opti;



%valeurs donnees par la carto generique.
MOTH.Reg_2dconso=[ 0.0  500.0   750.0  1000.0  1250.0  1500.0  1750.0  2000.0  2250.0  2500.0  2750.0  3000.0  3250.0  3500.0  3750.0  4000.0  4250.0  4500.0  4750.0  5000.0  5100]*pi/30;
MOTH.Cpl_2dconso=[     0.0    20.0    40.0    60.0    80.0   100.0   120.0   140.0   160.0   180.0   200.0   220.0];
MOTH.Conso_2d=1/3.6*[
%cpl 0.0    20.0    40.0    60.0    80.0    100.0   120.0   140.0   160.0   180.0   200.0   220.0
    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00     0.00
    0.28    0.87    1.58    2.14    2.79    3.55    4.05    4.68    5.40    6.18    8.88    9.66
    0.28    0.87    1.58    2.14    2.79    3.55    4.05    4.68    5.40    6.18    8.88    9.66
    0.42    0.96    1.59    2.14    2.79    3.55    4.05    4.68    5.40    6.18    8.88    9.66
    0.55    1.05    1.60    2.20    2.81    3.50    4.05    4.68    5.40    6.18    8.88    9.66
    0.68    1.16    1.70    2.26    2.83    3.45    4.05    4.68    5.40    6.18    8.88    9.66
    0.91    1.43    2.05    2.71    3.36    4.08    4.76    5.49    6.29    7.13    8.88    9.66
    1.13    1.70    2.39    3.14    3.90    4.70    5.48    6.32    7.21    8.10    8.88    9.66
    1.52    2.06    2.87    3.69    4.55    5.44    6.33    7.33    8.28    9.31   10.41   11.51
    1.93    2.42    3.34    4.25    5.19    6.16    7.22    8.31    9.37   10.48   11.75   13.02
    2.33    2.91    3.92    4.95    6.03    7.11    8.26    9.47   10.70   11.93   13.09   14.25
    2.74    3.42    4.50    5.66    6.85    8.03    9.34   10.61   11.96   13.33   13.09   14.25
    3.15    4.05    5.26    6.53    7.90    9.27   10.54   11.79   13.07   14.73   13.09   14.25
    3.55    4.68    6.02    7.41    8.92   10.46   11.71   12.98   14.21   14.73   13.09   14.25
    3.98    5.43    6.96    8.49   10.19   11.57   12.87   14.17   15.35   14.73   13.09   14.25
    4.61    6.19    7.89    9.59   11.43   12.63   14.07   15.36   16.45   14.73   13.09   14.25
    5.33    7.07    8.91   10.72   12.34   13.49   15.26   16.55   16.45   14.73   13.09   14.25
    6.05    7.96    9.94   11.82   13.21   14.36   16.45   16.55   16.45   14.73   13.09   14.25
    6.77    9.14   11.01   12.31   14.08   15.23   16.45   16.55   16.45   14.73   13.09   14.25
    7.49   10.32   12.08   12.80   14.08   15.23   16.45   16.55   16.45   14.73   13.09   14.25
    8.00   11.00   12.50   13.50   14.50   16.00   16.45   16.55   16.45   14.73   13.09   14.25
];

% Estimations des emissions de CO2 (g/s) du moteur thermique non disponible
MOTH.Reg_2dCO2=[ 1000.0  4500.0 ]*pi/30;
MOTH.Cpl_2dCO2=[  15.0   120.0 ];
MOTH.CO2_2d=[
   0 0
   0 0
];

% Estimations des emissions de CO (g/s) du moteur thermique
MOTH.Reg_2dCO=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dCO=[  15.0 120.0 ];
MOTH.CO_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de HC (g/s) du moteur thermique
MOTH.Reg_2dHC=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dHC=[  15.0  120.0 ];
MOTH.HC_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de NOx (g/s) du moteur thermique
MOTH.Reg_2dNOx=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dNOx=[   15.0  120.0 ];
MOTH.NOx_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de Particules (g/s) du moteur thermique
MOTH.Reg_2dPart=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dPart=[   15.0  120.0 ];
MOTH.Part_2d=[
   0 0
   0 0
]./1000;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Thermique moteur

% Pour inhiber le calcul de thermique moteur
MOTH.modele_therm=0; 
% Temp initiale moteur
MOTH.Tinit=25;
% Part du carburant transforme en chaleur
MOTH.pci_to_chaleur = 0.617;
% Capcite calorifique equivallente du bloc (J/K)
MOTH.thetaC =41860;
% coefficient de convection
MOTH.k_ext_vit0 =12.53;
MOTH.k_ext_vit =4.85;
% Coefficient de convection radiateur  d eau
MOTH.k_ext_vit0_calor =6.45;
MOTH.k_ext_vit_calor =2.0;
% Temp d ouverture du calorstat
MOTH.Touv_calorstat =85;
% Regulation calorstat ouvert (!)
MOTH.Tcons=80;
MOTH.Kp_calor=1;
MOTH.pci=43000;

display('Fin d''initialisation des parametres moteur thermique');


%---------------------------------------MISES A JOUR----------------------------%
% 18-04-2003. RT. Ajout de la consommation au ralenti + interpollation a bas regime
% 28-04-2003. MDR. Modification des couples negatifs. Couple non nul a 500tr/min.
% 05-05-2003. MDR. Modification des couples negatifs pour faciliter le demarrage.
% 16-06-2002. MDR.RT. Ajout de masse du moteur thermique (estimation).
% 11/02/2004. MDR. Completion matrice conso pour regime 5100 tr/min.
% 24-03-2004. RT. remise des consos nulles a regime nul
% 18-12-06 (EV/RT) : rajout PID ralenti en parametres
