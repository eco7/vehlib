% File: TWC_DATA
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier Three Ways Catalyst
%                      Pour librairie VEHLIB
%                          IFSTTAR - LTE 
%  Objet: Ce script initialise la structure TWC
%          contenant les champs necessaires pour renseigner 
%          le composant Catalyseur 3 voies de la librairie VEHLIB.
%
%
% =============================================================================

TWC.ntypcata=1; %type = 1 modele 0D 1 zone type = 2 modele 0D multi-zone

% Parametres pour la thermique du catalyseur (Byron T Shaw A simplified
% coldstart catalyst ...)
TWC.MCata=5; % Masse kg
TWC.CpCata=800; % Capacité calorifique J/kgK

TWC.HsA=2.55; % Coefficient de convection surrounding * Surface (J/K)
TWC.HinA=46.7; % Internal heat transfer coefficient * Surface (J/K)

% Relative Oxygen Model (Guzella & Onder p 117)
TWC.OxygenToAirRatio=0.21; % Oxygen to air ratio (%)
TWC.OSC=400e-6; % Maximum Oxygen Storage Capacity (kg)

% Static efficiency Curve
% Inspire de Guzella & onder p 6 pour l'efficacite de conversion fontction de T
% Extrait de Byron T Shaw: A simplified coldstart catalyst thermal model to
% reduce hydrocarbon emissions pour la focntion de wiebe 2D (exces d'air, T)
% Wiebe fonction:
% y=1-exp(-a*((u-u0)/du)^m)
% u0 : ordinate at y=10%
% du : difference in u from y=90% et y=10%
% a et m : fitting parameter

% La fonction est modifiee; difficile de faire qque chose en 2D sinon.
%y=exp(-a*(du./(u-u0)).^m);

% Pour la temperature
TWC.aT=.8;
TWC.dT=200;
TWC.T0=350;
TWC.mT=14;

% Pour les CO
TWC.aCO=0.1233; %1.7807;
TWC.dCO=0.6190; %0.4856;
TWC.CO0=1.5694; %5.2867;
TWC.mCO=-10; %-66;

% Pour les HC
TWC.aHC=0.8238; %1.3228;
TWC.dHC=0.4263; %0.3310;
TWC.HC0=0.2888; %0.3463;    
TWC.mHC=6; %4;   

% Pour les NOx
TWC.aNOx=2.079; %0.7519;
TWC.dNOx=0.0931; %0.0996;
TWC.NOx0=1.1103; %1.1114;
TWC.mNOx= 30; %14;

% Enthalpie standard de formation (1bar, 298K)
TWC.dHf0CO2=-395.509*1e3; %J/mol
TWC.dHf0CO=-110.525*1e3;
TWC.dHf0O2=0;
TWC.dHf0CH4=-74.87*1e3;
TWC.dHf0H20=-241.818*1e3;
TWC.dHf0C3H6=20.410*1e3;

% Oxydation CO : CO+0.5O2 -> CO2
TWC.dH0CO=TWC.dHf0CO2-(TWC.dHf0CO+0.5*TWC.dHf0O2);
% Oxydation CH4 : CH4+2O2 -> CO2+2H2O
TWC.dH0CH4=TWC.dHf0CO2+2*TWC.dHf0H20-(TWC.dHf0CH4+2*TWC.dHf0O2);
% Oxydation C3H6 : C3H6+4.5O2 -> 3CO2+3H2O
TWC.dH0C3H6=3*TWC.dHf0CO2+3*TWC.dHf0H20-(TWC.dHf0C3H6+4.5*TWC.dHf0O2);

% Norme d'emission EURO IV reglementaires en g/km
TWC.CO_EURO4=1;
TWC.THC_EURO4=0.1;
TWC.NOx_EURO4=0.08;

% Norme d'emission EURO V reglementaires en g/km
TWC.CO_EURO5=1000e-3;
TWC.THC_EURO5=100e-3;
TWC.NMHC_EURO5=68e-3;
TWC.NOx_EURO5=60e-3;
TWC.PMWeight_EURO5=5e-3;
