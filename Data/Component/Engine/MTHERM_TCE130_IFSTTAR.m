% File: MTHERM_TCE130_IFSTTAR
% =========================================================================
%
%  (C) Copyright IFSTTAR LTE 1999-2013 
%
%
%                Initialisation du fichier moteur thermique
%                      Pour librairie VEHLIB
%                           IFSTTAR - LTE
%
%  Objet: Ce script initialise la structure MOTH
%         contenant les champs necessaires pour renseigner
%         le composant MOTEUR THERMIQUE de la librairie VEHLIB.
%
%  Commentaires: A partir de mesures realisees au banc moteur IFSTTAR
%
% Cree le 11-Sep-2018 par la fonction ecrit_carto_mth.
% Attention aux valeur d'emissions, de temperature et de conso a 700, 4500 et 4550 rpm
% il s'agit d'extrapolation corrigees a la main
% =========================================================================

% Reperage du type de moteur thermique: 10 = moteur thermique cartographie
MOTH.ntypmoth = 10;

% Nom du moteur
MOTH.nom = 'MTHERM_TCE130_IFSTTAR';

% Masse du moteur thermique
MOTH.Masse_mth = 140.0;

% Inertie du moteur thermique
MOTH.J_mt = 0.2;

% Regimes de fonctionnement
MOTH.ral_max = 81;		% regime maxi pour un ralenti (en rad/s)
MOTH.ral_min = 65;		% regime mini pour un ralenti (en rad/s)
MOTH.ral = 77;		% regime ralenti (en rad/s)
MOTH.ral_dem = 125;		% regime de fin d'action du demarreur (en rad/s)
MOTH.wmt_maxi = 654;		% regime maxi regulation (en rad/s)

% Regulation de ralenti
MOTH.P_ral = 0.1;
MOTH.I_ral = 0.0;
MOTH.D_ral = 0.0;

% Carburant
MOTH.dens_carb = 750.00;		% densite du carburant a 20 C (g/L)
MOTH.CO2carb = 1.84;		% rapport masses CO2 carburant pour une combustion parfaite
% (mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0, essence=1.85

% Reponse couple alpha du moteur:
MOTH.alpha_cons = [	0.00	1.00];
MOTH.alpha2cpl = [	0.00	1.00];

% Temps de reponse en couple du moteur
MOTH.tr = 0.5;

% courbe enveloppe constructeur
MOTH.wmt_max_th =[1000	1250	1500	1750	2000	2250	2500	2750	3000	3250	3500	3750	4000	4250	4500	4750	5000	5250	5500	5750	6000	6250]*pi/30;
MOTH.cmt_max_th =[112.987643	139.98469	184.97977	199.978129	204.977582	204.977582	204.977582	204.977582	204.977582	201.646697	198.315811	194.984925	191.65404	188.323154	184.992268	181.661382	178.330497	174.999611	168.381585	161.082383	154.383116	123.386506];

% Courbe enveloppe (estimation pleine charge en couple (Nm) fct regime MT)
MOTH.wmt_max = [	0	52	84	105	157	209	262	314	367	419	471	473];
MOTH.cmt_max = [	0.0	0.0	73.5	79.4	110.9	179.7	199.1	195.4	187.5	185.6	184.6	0.0];
MOTH.cmt_min = [	0.0	-10.0	-12.3	-12.4	-13.4	-14.0	-14.6	-15.4	-16.6	-17.9	-20.2	-20.3];
MOTH.pmt_max = MOTH.cmt_max.*MOTH.wmt_max;		% pour le calculateur

% Courbe de charge optimum regime couple du moteur thermique
% (determination de cette zone de points de fonctionnement d'apres les essais)
MOTH.wmt_opti = [	NaN	NaN];
MOTH.cmt_opti = [	NaN	NaN];
MOTH.pmt_opti = MOTH.cmt_opti.*MOTH.wmt_opti;		% pour le calculateur

% Consommation de carburant en g/s
% Attention : premiere ligne (73 rad/s), derniere colonne (200 Nm)
% et "coins" sont extrapoles
MOTH.Reg_2dconso = [	73	105	157	209	262	314	367	419	471	476];
MOTH.Cpl_2dconso = [	0.0	25.0	50.0	75.0	85.0	100.0	120.0	125.0	162.0	200.0];
MOTH.Conso_2d = [
	0.108	0.108	0.200	0.315	0.428	0.537	0.680	0.716	0.981	1.254
	0.108	0.265	0.439	0.628	0.700	0.808	0.955	0.991	1.265	1.551
	0.167	0.390	0.640	0.899	0.998	1.144	1.458	1.838	2.468	3.120
	0.233	0.520	0.857	1.192	1.326	1.527	1.854	1.944	2.935	3.952
	0.309	0.667	1.080	1.508	1.679	1.935	2.336	2.438	3.464	4.463
	0.392	0.829	1.337	1.883	2.087	2.393	2.837	2.948	3.981	4.962
	0.492	1.022	1.625	2.345	2.574	2.917	3.421	3.548	4.486	5.449
	0.633	1.218	1.965	2.630	2.907	3.322	3.875	4.013	5.035	6.083
	0.804	1.552	2.301	3.047	3.324	3.739	4.293	4.431	5.456	6.508
	0.820	1.577	2.334	3.088	3.365	3.781	4.334	4.473	5.497	6.550
];

% Estimation de la Richesse (-) du melange
MOTH.AStoechio = 14.7;
MOTH.Reg_2dRich = [	73	105	157	209	262	314	367	419	471	476];
MOTH.Cpl_2dRich = [	0.0	25.0	50.0	75.0	85.0	100.0	120.0	125.0	162.0	200.0];
MOTH.Rich_2d = [
	0.73	0.73	0.73	0.81	1.06	1.11	1.19	1.21	1.39	1.64
	0.76	0.80	0.88	0.98	1.02	1.06	1.13	1.15	1.31	1.54
	0.82	0.86	0.94	1.00	1.01	1.01	1.09	1.08	1.19	1.33
	0.86	0.86	0.95	1.00	1.01	1.02	1.06	1.08	1.15	1.24
	0.90	0.91	0.98	1.02	1.03	1.05	1.08	1.09	1.17	1.24
	0.94	0.95	1.01	1.05	1.06	1.07	1.09	1.10	1.17	1.22
	1.02	1.01	1.03	1.09	1.10	1.10	1.12	1.13	1.16	1.20
	1.10	1.02	1.08	1.07	1.09	1.11	1.13	1.14	1.19	1.26
	1.19	1.15	1.11	1.08	1.09	1.11	1.14	1.15	1.20	1.27
	1.19	1.15	1.11	1.08	1.09	1.11	1.14	1.15	1.20	1.27
];

% Estimation des emissions de CO2 (g/s) du moteur thermique
MOTH.Reg_2dCO2 = [	105	471];
MOTH.Cpl_2dCO2 = [	15.0	120.0];
MOTH.CO2_2d = [
	0.000	0.000
	0.000	0.000
];

% Estimation des emissions de CO (%vol) du moteur thermique
MOTH.Reg_2dCO = [	73	105	157	209	262	314	367	419	471	476];
MOTH.Cpl_2dCO = [	0.0	25.0	50.0	75.0	85.0	100.0	120.0	125.0	162.0	200.0];
MOTH.CO_2d = [
	1.324e-01	-3.145e+00	-1.796e+00	-1.759e-01	4.720e-01	7.908e-01	1.197e+00	1.298e+00	2.049e+00	2.821e+00
	1.198e-01	5.554e-02	1.784e-01	6.155e-01	8.184e-01	1.124e+00	1.532e+00	1.635e+00	2.396e+00	3.185e+00
	1.051e-01	-4.736e-03	1.073e-01	6.338e-01	6.205e-01	5.157e-01	2.298e+00	2.328e+00	3.642e+00	4.997e+00
	1.920e-01	-1.568e-02	1.713e-01	5.139e-01	6.371e-01	7.989e-01	1.599e+00	1.875e+00	3.747e+00	5.667e+00
	2.487e-01	1.529e-01	4.678e-01	8.444e-01	1.093e+00	1.515e+00	2.297e+00	2.493e+00	4.025e+00	5.531e+00
	3.746e-01	3.837e-01	7.700e-01	1.637e+00	1.789e+00	2.017e+00	2.475e+00	2.592e+00	4.030e+00	5.123e+00
	1.271e+00	6.372e-01	9.267e-01	2.521e+00	2.582e+00	2.673e+00	3.014e+00	3.103e+00	3.763e+00	4.442e+00
	2.774e+00	8.047e-01	1.430e+00	2.254e+00	2.586e+00	3.083e+00	3.743e+00	3.907e+00	5.121e+00	6.359e+00
	-1.440e+01	-1.664e+02	-2.092e+01	2.794e+00	3.127e+00	3.626e+00	4.292e+00	4.458e+00	5.690e+00	6.955e+00
	-1.667e+01	-1.837e+02	-2.333e+01	2.848e+00	3.181e+00	3.680e+00	4.346e+00	4.512e+00	5.744e+00	7.009e+00
];

MOTH.CO_2d(MOTH.CO_2d<0) = 0; %extrapolations aberrantes

% Estimation des emissions de HC (ppmC) du moteur thermique
MOTH.Reg_2dHC = [	73	105	157	209	262	314	367	419	471	476];
MOTH.Cpl_2dHC = [	0.0	25.0	50.0	75.0	85.0	100.0	120.0	125.0	162.0	200.0];
MOTH.HC_2d = [
	7.630e+03	-7.826e+04	-4.841e+04	-1.189e+04	2.716e+03	2.120e+03	6.686e+02	3.057e+02	-2.379e+03	-5.137e+03
	7.158e+03	4.214e+03	3.300e+03	3.215e+03	2.604e+03	1.523e+03	8.652e+01	-2.721e+02	-2.919e+03	-5.624e+03
	6.318e+03	3.749e+03	2.952e+03	2.669e+03	2.477e+03	2.158e+03	2.147e+03	1.971e+03	4.673e+02	-1.063e+03
	4.731e+03	2.742e+03	2.297e+03	2.177e+03	2.097e+03	1.928e+03	1.848e+03	1.844e+03	1.489e+03	1.123e+03
	2.911e+03	2.700e+03	2.232e+03	2.025e+03	1.951e+03	1.842e+03	1.740e+03	1.714e+03	1.483e+03	1.155e+03
	1.834e+03	2.384e+03	2.115e+03	1.936e+03	1.892e+03	1.826e+03	1.758e+03	1.741e+03	1.502e+03	1.211e+03
	2.911e+03	2.300e+03	2.074e+03	1.989e+03	1.974e+03	1.952e+03	1.825e+03	1.792e+03	1.545e+03	1.292e+03
	4.268e+03	2.332e+03	2.036e+03	1.974e+03	1.956e+03	1.928e+03	1.891e+03	1.882e+03	1.810e+03	1.734e+03
	-6.289e+03	-1.034e+05	-1.949e+04	2.020e+03	2.002e+03	1.975e+03	1.939e+03	1.930e+03	1.864e+03	1.796e+03
	-7.695e+03	-1.143e+05	-2.175e+04	2.024e+03	2.007e+03	1.980e+03	1.944e+03	1.935e+03	1.869e+03	1.801e+03
];

MOTH.HC_2d(MOTH.HC_2d<0) = 0; %extrapolations aberrantes

% Estimation des emissions de NOx (ppm) du moteur thermique
MOTH.Reg_2dNOx = [	73	105	157	209	262	314	367	419	471	476];
MOTH.Cpl_2dNOx = [	0.0	25.0	50.0	75.0	85.0	100.0	120.0	125.0	162.0	200.0];
MOTH.NOx_2d = [
	-2.507e+02	5.153e+04	3.397e+04	1.241e+04	3.794e+03	3.269e+03	2.930e+03	2.846e+03	2.220e+03	1.577e+03
	-1.685e+02	1.754e+03	2.474e+03	2.745e+03	2.613e+03	2.362e+03	2.027e+03	1.943e+03	1.321e+03	6.802e+02
	-4.460e+01	1.973e+03	2.409e+03	2.449e+03	2.492e+03	2.568e+03	1.190e+03	1.643e+03	8.442e+02	2.203e+01
	4.951e+01	1.843e+03	2.319e+03	2.526e+03	2.537e+03	2.461e+03	1.903e+03	1.709e+03	7.323e+02	-2.682e+02
	1.241e+02	2.092e+03	2.721e+03	2.585e+03	2.444e+03	2.193e+03	1.715e+03	1.595e+03	6.876e+02	-7.617e+01
	2.186e+02	1.957e+03	2.496e+03	2.144e+03	2.039e+03	1.881e+03	1.594e+03	1.521e+03	7.985e+02	2.715e+02
	1.751e+02	2.008e+03	2.426e+03	1.568e+03	1.552e+03	1.529e+03	1.385e+03	1.347e+03	1.065e+03	7.750e+02
	2.852e+02	2.126e+03	2.088e+03	1.837e+03	1.731e+03	1.573e+03	1.362e+03	1.309e+03	9.201e+02	5.224e+02
	1.395e+04	1.235e+05	2.299e+04	1.847e+03	1.741e+03	1.582e+03	1.370e+03	1.317e+03	9.249e+02	5.220e+02
	1.572e+04	1.361e+05	2.520e+04	1.848e+03	1.742e+03	1.583e+03	1.371e+03	1.318e+03	9.259e+02	5.230e+02
];

MOTH.NOx_2d(MOTH.NOx_2d<0) = 0; %extrapolations aberrantes

% Estimation des emissions de Particules (g/s) du moteur thermique
MOTH.Reg_2dPart = [	105	471];
MOTH.Cpl_2dPart = [	15.0	120.0];
MOTH.Part_2d = [
	0.000e+00	0.000e+00
	0.000e+00	0.000e+00
];

% Estimation de la Temperature echappement (deg C) /!\ passage en K dessous
MOTH.Reg_2dTechAm = [	73	105	157	209	262	314	367	419	471	476];
MOTH.Cpl_2dTechAm = [	0.0	25.0	50.0	75.0	85.0	100.0	120.0	125.0	162.0	200.0];
MOTH.TechAm_2d = [
	209	209 209	353	489	535	594	609	718	830
	239	306	385	463	493	537	596	611	719	830
	285	373	476	525	544	572	591	596	672	749
	344	441	542	581	596	618	644	650	692	736
	404	492	572	621	638	662	685	690	727	767
	463	551	615	653	675	707	725	729	758	795
	470	576	630	667	693	731	751	755	788	821
	486	587	642	721	751	796	855	870	979	1090
	506	608	711	812	842	887	947	962	1074	1189
	508	613	717	821	851	896	957	972	1083	1198
];

% Passage en Kelvin
MOTH.TechAm_2d = MOTH.TechAm_2d +273;

%---------------------------Thermique moteur------------------------------%

% Pour inhiber le calcul de thermique moteur (=0)
MOTH.modele_therm = 0;
% Temp initiale moteur
MOTH.Tinit = 25;
% Part du carburant transforme en chaleur
MOTH.pci_to_chaleur = 0.617;
% Capacite calorifique equivalente du bloc (J/K)
MOTH.thetaC = 41860;
% coefficient de convection
MOTH.k_ext_vit0 = 12.53;
MOTH.k_ext_vit = 4.85;
% Coefficient de convection radiateur  d eau
MOTH.k_ext_vit0_calor = 6.45;
MOTH.k_ext_vit_calor = 2.00;
% Temp d ouverture du calorstat
MOTH.Touv_calorstat = 100;
% Regulation calorstat ouvert (!)
MOTH.Tcons = 95;
MOTH.Kp_calor = 1;
MOTH.pci = 43000;

display('Fin d''initialisation des parametres moteur thermique');

%----------------------------MISES A JOUR---------------------------------%
