% =============================================================================
% 
%                Initialisation du fichier moteur thermique
%                     GRUAU A partir des donn�es de simulation 
%                    
%  Objet: Ce script initialise la structure MOTH
%          
%          le composant MOTEUR THERMIQUE 
%
% =============================================================================


%reperage du type de moteur thermique: 10 = moteur thermique cartographie
MOTH.ntypmoth=10;
MOTH.tr=0.1;

% masse du moteur thermique
MOTH.Masse_mth    = 65;

% Inertie du moteur thermique
MOTH.J_mt        = 0.15;


% Regulation ralenti
MOTH.P_ral=0.1;
MOTH.I_ral=0;
MOTH.D_ral=0;


% Regimes de fonctionnement
MOTH.ral_max    = 850*pi/30;			% regime maxi pour un ralenti (en rad/s)
MOTH.ral_min    = 750*pi/30;			% regime mini pour un ralenti (en rad/s)
MOTH.alpha_ral  = 0.15;					% condition sur alpha pour ralenti
MOTH.ral        = 800*pi/30;			% regime ralenti (en rad/s)
MOTH.alpha_accel= 0.05;				    % condition sur alpha pour une accel
MOTH.ral_dem	 	= 850*pi/30;		% regime de fin d'action du demarreur (regulation)
MOTH.dens_carb  = 830;					% densite du carburant a 20 C (g/l)
MOTH.CO2carb		= 2.0;				% rapport masses CO2 carburant pour une combustion parfaite 


%(mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85
MOTH.wmt_maxi=3500*pi/30;				% regime maxi regulation (en rad/s)
MOTH.cmt_maxi=100;
% Reponse couple alpha du moteur:
MOTH.alpha_cons=[0 1];
MOTH.alpha2cpl=[0 1];


MOTH.Reg_2dconso=[0 1100.00 1200.00 1300.00 1400.00 1500.00 1600.00 1700.00 1800.00 1900.00 2000.00 2100.00 2200.00 2300.00 2400.00 2500.00 2600.00 2700.00 2800.00 2900.00 3000.00 3100.00 3200.00 3300.00 3400.00 3500.00]*pi/30;

MOTH.Cpl_2dconso=[    0.00    5.00   10.00   15.00   20.00   25.00   30.00   35.00   40.00   45.00   50.00   55.00   60.00   65.00   70.00   75.00   80.00   85.00   90.00   95.00  100.00];

MOTH.Conso_2d=[ 
0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  0.0000  
0.0771	0.1040	0.1326	0.1629	0.1950	0.2288	0.2643	0.3015	0.3405	0.3812	0.4236	0.4677	0.5136	0.5612	0.6105	0.6615	0.7143	0.7688	0.8250	0.8829	0.9426	
0.0945	0.1200	0.1480	0.1784	0.2113	0.2465	0.2843	0.3244	0.3671	0.4121	0.4596	0.5095	0.5618	0.6166	0.6738	0.7335	0.7956	0.8602	0.9271	0.9965	1.0683	
0.1119	0.1360	0.1634	0.1939	0.2275	0.2643	0.3042	0.3473	0.3936	0.4430	0.4955	0.5512	0.6100	0.6720	0.7371	0.8054	0.8769	0.9515	1.0292	1.1101	1.1941	
0.1256	0.1530	0.1833	0.2165	0.2524	0.2912	0.3327	0.3772	0.4244	0.4745	0.5274	0.5831	0.6416	0.7029	0.7671	0.8340	0.9039	0.9765	1.0519	1.1303	1.2113	
0.1393	0.1701	0.2033	0.2391	0.2773	0.3181	0.3613	0.4071	0.4553	0.5060	0.5593	0.6150	0.6732	0.7339	0.7971	0.8627	0.9309	1.0016	1.0747	1.1504	1.2286	
0.1442	0.1800	0.2177	0.2576	0.2994	0.3433	0.3893	0.4373	0.4873	0.5394	0.5936	0.6498	0.7080	0.7682	0.8305	0.8948	0.9612	1.0297	1.1001	1.1726	1.2472	
0.1491	0.1898	0.2321	0.2760	0.3215	0.3686	0.4172	0.4675	0.5193	0.5728	0.6278	0.6845	0.7427	0.8025	0.8639	0.9269	0.9915	1.0577	1.1254	1.1948	1.2657	
0.1603	0.2021	0.2457	0.2912	0.3384	0.3875	0.4383	0.4909	0.5453	0.6015	0.6595	0.7193	0.7809	0.8443	0.9094	0.9765	1.0452	1.1158	1.1881	1.2623	1.3383	
0.1714	0.2143	0.2593	0.3063	0.3553	0.4063	0.4593	0.5142	0.5712	0.6302	0.6912	0.7541	0.8191	0.8861	0.9550	1.0260	1.0989	1.1739	1.2508	1.3298	1.4108	
0.1849	0.2283	0.2741	0.3222	0.3725	0.4251	0.4799	0.5370	0.5964	0.6581	0.7220	0.7882	0.8566	0.9274	1.0003	1.0756	1.1531	1.2330	1.3150	1.3994	1.4860	
0.1984	0.2424	0.2890	0.3381	0.3897	0.4439	0.5006	0.5599	0.6217	0.6860	0.7529	0.8223	0.8942	0.9687	1.0457	1.1253	1.2074	1.2921	1.3793	1.4690	1.5612	
0.2022	0.2497	0.2998	0.3523	0.4073	0.4648	0.5248	0.5873	0.6522	0.7196	0.7896	0.8619	0.9367	1.0142	1.0940	1.1763	1.2611	1.3484	1.4382	1.5304	1.6252	
0.2059	0.2570	0.3106	0.3665	0.4249	0.4857	0.5489	0.6146	0.6827	0.7532	0.8262	0.9015	0.9793	1.0596	1.1422	1.2273	1.3148	1.4047	1.4971	1.5918	1.6891	
0.2091	0.2656	0.3241	0.3849	0.4478	0.5128	0.5799	0.6491	0.7205	0.7941	0.8697	0.9475	1.0274	1.1095	1.1936	1.2799	1.3684	1.4589	1.5516	1.6464	1.7434	
0.2122	0.2741	0.3377	0.4033	0.4706	0.5398	0.6108	0.6836	0.7583	0.8349	0.9132	0.9934	1.0754	1.1593	1.2450	1.3325	1.4219	1.5131	1.6061	1.7009	1.7976	
0.2472	0.3070	0.3694	0.4344	0.5019	0.5719	0.6446	0.7198	0.7976	0.8780	0.9608	1.0463	1.1344	1.2250	1.3182	1.4139	1.5123	1.6131	1.7165	1.8225	1.9310	
0.2821	0.3399	0.4010	0.4654	0.5331	0.6041	0.6784	0.7560	0.8369	0.9211	1.0085	1.0993	1.1934	1.2907	1.3914	1.4953	1.6026	1.7131	1.8269	1.9441	2.0645	
0.2991	0.3589	0.4224	0.4894	0.5599	0.6341	0.7118	0.7930	0.8778	0.9662	1.0581	1.1536	1.2527	1.3553	1.4614	1.5712	1.6845	1.8013	1.9217	2.0457	2.1732	
0.3161	0.3780	0.4438	0.5134	0.5868	0.6641	0.7452	0.8301	0.9188	1.0114	1.1078	1.2080	1.3120	1.4199	1.5315	1.6471	1.7664	1.8896	2.0166	2.1474	2.2820	
0.3378	0.3997	0.4662	0.5374	0.6133	0.6939	0.7791	0.8690	0.9636	1.0629	1.1669	1.2756	1.3889	1.5069	1.6296	1.7570	1.8890	2.0258	2.1673	2.3134	2.4642	
0.3596	0.4213	0.4886	0.5614	0.6397	0.7236	0.8130	0.9079	1.0084	1.1144	1.2260	1.3431	1.4657	1.5939	1.7276	1.8669	2.0117	2.1620	2.3179	2.4793	2.6463	
0.3813	0.4430	0.5110	0.5854	0.6662	0.7534	0.8469	0.9468	1.0532	1.1659	1.2851	1.4107	1.5426	1.6809	1.8257	1.9768	2.1344	2.2982	2.4686	2.6452	2.8285	
0.4031	0.4646	0.5334	0.6094	0.6926	0.7831	0.8808	0.9857	1.0980	1.2174	1.3442	1.4782	1.6194	1.7679	1.9237	2.0867	2.2570	2.4344	2.6192	2.8112	3.0106	
0.4248	0.4862	0.5558	0.6334	0.7190	0.8128	0.9147	1.0246	1.1428	1.2689	1.4033	1.5457	1.6962	1.8549	2.0217	2.1966	2.3796	2.5706	2.7698	2.9771	3.1927	
0.4466	0.5079	0.5782	0.6574	0.7455	0.8426	0.9486	1.0635	1.1876	1.3204	1.4624	1.6133	1.7731	1.9419	2.1198	2.3065	2.5023	2.7068	2.9205	3.1431	3.3749	
];

MOTH.wmt_max=[0 50 500 900.00 1100.00 1300.00 1500.00 1700.00 1900.00 2100.00 2300.00 2500.00 2700.00 2900.00 3100.00 3300.00 3500.00 4000.00]*pi/30;

MOTH.cmt_max=[0 0 0 53.00   63.87   75.60   85.00   93.00  100.00  100.00  100.00  100.00  100.00   98.20   93.80   88.80   85.00 0.00];
%[0 500 750  1000 1500 2000 2500 3000 3500]
%[0 -10 -15 -20 -27 -32 -37 -50 -55]
MOTH.cmt_min = [0 -16 -17 -18 -21 -25  -27 -28  -31 -32 -33 -36 -48 -50 -51  -52  -55 -56];

MOTH.pmt_max = MOTH.cmt_max.*MOTH.wmt_max;

% courbe optimum utilisee par le calculateur

MOTH.wmt_opti =MOTH.wmt_max ;
MOTH.cmt_opti =MOTH.cmt_max *0.8;
MOTH.pmt_opti = MOTH.cmt_opti.*MOTH.wmt_opti;


%gradient de couple maximum
MOTH.grd_cmt_mont=140;
MOTH.grd_cmt_desc=-7000;


%%  Carto CO ( Non connues mais renseign�s pour les besoins de simulation

MOTH.Reg_2dCO=[ 1000.0  1500.0  2000.0  2500.0  3000.0  3500.0 ]*pi/30;
MOTH.Cpl_2dCO=[ 5  15.0   30.0   60.0   90.0  100.0  ];
MOTH.CO_2d=[
		0 0 0 0 0 0
        0 0 0 0 0 0
        0 0 0 0 0 0
        0 0 0 0 0 0
        0 0 0 0 0 0
        0 0 0 0 0 0
]./1000;
% Estimations des emissions de HC(g/s) du moteur thermique 
MOTH.Reg_2dHC=MOTH.Reg_2dCO;
MOTH.Cpl_2dHC=MOTH.Cpl_2dCO;
MOTH.HC_2d=MOTH.CO_2d;

% Estimations des emissions de NOx (g/s) du moteur thermique
MOTH.Reg_2dNOx=MOTH.Reg_2dHC;
MOTH.Cpl_2dNOx=MOTH.Cpl_2dHC;
MOTH.NOx_2d=MOTH.HC_2d;
% Estimations des emissions de Particules (g/s) du moteur thermique 
MOTH.Reg_2dPart=MOTH.Reg_2dNOx;
MOTH.Cpl_2dPart=MOTH.Cpl_2dNOx;
MOTH.Part_2d=MOTH.HC_2d;
%%%%%%%%%%%% Carto CO2 renseign�e
% Estimations des emissions de CO2 (g/s) du moteur thermique XUD9A
MOTH.Reg_2dCO2=MOTH.Reg_2dPart;
MOTH.Cpl_2dCO2=MOTH.Cpl_2dPart;
MOTH.CO2_2d=MOTH.Part_2d;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Thermique moteur

% Pour inhiber le calcul de thermique moteur
MOTH.modele_therm=0; 
% Temp initiale moteur
MOTH.Tinit=25;
% Part du carburant transforme en chaleur
MOTH.pci_to_chaleur = 0.617;
% Capcite calorifique equivallente du bloc (J/K)
MOTH.thetaC =41860;
% coefficient de convection
MOTH.k_ext_vit0 =12.53;
MOTH.k_ext_vit =4.85;
% Coefficient de convection radiateur  d eau
MOTH.k_ext_vit0_calor =6.45;
MOTH.k_ext_vit_calor =2.0;
% Temp d ouverture du calorstat
MOTH.Touv_calorstat =85;
% Regulation calorstat ouvert (!)
MOTH.Tcons=80;
MOTH.Kp_calor=1;
MOTH.pci=43000;

display('Fin d''initialisation des parametres moteur thermique'); 