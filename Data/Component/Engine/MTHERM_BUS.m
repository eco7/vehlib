% File: MTHERM_BUS
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier moteur thermique
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure MOTH
%          contenant les champs necessaires pour renseigner 
%          le composant MOTEUR THERMIQUE de la librairie VEHLIB.
%
%          Moteur DIESEL BUS V2G ref: 60226Y41 (EURO 3)
% =============================================================================


%reperage du type de moteur thermique: 10 = moteur thermique cartographie
MOTH.ntypmoth=10;

% Nom du moteur
MOTH.nom='MTHERM_BUS';

% masse du moteur thermique
MOTH.Masse_mth    = 1280;

% Inertie du moteur thermique
MOTH.J_mt        = 1.08;

% Regimes de fonctionnement
MOTH.ral_max    = 700*pi/30;			% regime maxi pour un ralenti (en rad/s)
MOTH.ral_min    = 600*pi/30;			% regime mini pour un ralenti (en rad/s)
MOTH.ral        = 650*pi/30;			% regime ralenti (en rad/s)
MOTH.ral_dem	= 650*pi/30;			% regime de fin d'action du demarreur (regulation)
MOTH.wmt_maxi   =2500*pi/30;			% regime maxi regulation (en rad/s)

% Carburant Diesel
MOTH.dens_carb  = 830;				% densite du carburant a 20 C (g/l)
MOTH.CO2carb	= 2.0;      		% rapport masses CO2 carburant pour une combustion parfaite 
%(mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85

%  regulation ralenti
MOTH.P_ral=0.1;
MOTH.I_ral=0;
MOTH.D_ral=0;

%gradient de couple maximum
MOTH.grd_cmt_mont=700;
MOTH.grd_cmt_desc=-7000;

% Reponse couple alpha du moteur:
MOTH.alpha_cons=[0 1];
MOTH.alpha2cpl=[0 1];

% Temps de reponse en couple du moteur
MOTH.tr=0.1;

% courbe enveloppe (estimation pleine charge et coupure d'injection (Nm) en fct regime MT(rad/s)
MOTH.wmt_max = [-1 0 1  10 500 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2100 2200 2300 2500 2550 2600]*pi/30;
MOTH.cmt_max = [0  0 0   0   0 590 650 700  750  800  870  865  880  875  875  875  860  840  820  790  780  735  700    0    0];
MOTH.cmt_min = [0  0 0 -40 -40 -62 -65 -70  -72  -75  -80  -85  -90  -93  -95 -100 -105 -110 -115 -120 -125 -130 -143 -143 -143]; 
MOTH.pmt_max = MOTH.cmt_max.*MOTH.wmt_max;   % pour le calculateur


% courbe optimum utilisee par le calculateur
MOTH.wmt_opti = [0 500 700 871 1645 2000 2250 2300 2500]*pi/30;
MOTH.cmt_opti = [0   0 160 400  675  660  600    0    0];
% les deux lignes suivantes pour les demarrage en cote 13 et 20 % avec patinage a 1200 tr/mn
%MOTH.wmt_opti = [-1 0 1  10 500 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2100 2200 2300 2500 2550 2600]*pi/30;
%MOTH.cmt_opti = [0  0 0   0   0 590 650 700  750  800  870  865  880  875  875  875  860  840  820  790  780  735  700    0    0];
MOTH.pmt_opti = MOTH.cmt_opti.*MOTH.wmt_opti;

% Estimations de la consommation (g/s) du moteur thermique 60226Y41
MOTH.Reg_2dconso=[0 500.0 700.0 800.0 900.0 1000.0 1100.0 1200.0 1300.0 1400.0 1500.0 1600.0 1700.0 1800.0 1900.0 2000.0 2100.0 2200.0 2300.0 2400.0 2500.0]*pi/30;
MOTH.Cpl_2dconso=[
  0.0     50.0    100.0   150.0   200.0   250.0   300.0   350.0   400.0   450.0   500.0   550.0   600.0   650.0   700.0   750.0   800.0   850.0   900.0];
MOTH.Conso_2d=[   
%cpl0.0     50.0    100.0   150.0   200.0   250.0   300.0   350.0   400.0   450.0   500.0   550.0   600.0   650.0   700.0   750.0   800.0   850.0   900.0];
   0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00
   0.20    0.30    0.50    0.60    0.70    0.90    1.10    1.30    1.50    1.80    2.00    2.30    2.60    3.00    3.20    3.50    3.80    4.20    4.50   
   0.23    0.41    0.60    0.78    0.97    1.18    1.40    1.62    1.85    2.09    2.34    2.61    2.93    3.21    3.50    3.81    4.11    4.40    4.76  
   0.25    0.43    0.62    0.86    1.10    1.33    1.56    1.80    2.05    2.30    2.56    2.86    3.16    3.45    3.77    4.07    4.40    4.68    5.05  
   0.31    0.51    0.71    0.95    1.21    1.47    1.73    1.98    2.23    2.49    2.75    3.05    3.36    3.71    4.07    4.42    4.76    5.09    5.50  
   0.35    0.58    0.80    1.05    1.34    1.62    1.89    2.15    2.42    2.68    2.98    3.28    3.58    3.92    4.27    4.69    5.09    5.46    5.90  
   0.37    0.66    0.94    1.14    1.45    1.77    2.05    2.34    2.62    2.92    3.22    3.52    3.84    4.20    4.55    4.95    5.35    5.74    6.17  
   0.39    0.68    0.97    1.24    1.59    1.93    2.23    2.53    2.83    3.15    3.46    3.79    4.13    4.48    4.83    5.25    5.67    6.10    6.48
   0.45    0.75    1.05    1.34    1.72    2.08    2.41    2.73    3.06    3.39    3.73    4.07    4.44    4.82    5.19    5.61    6.02    6.50    6.96
   0.51    0.83    1.15    1.45    1.85    2.24    2.59    2.93    3.28    3.63    4.00    4.37    4.74    5.14    5.54    5.97    6.42    6.90    7.37
   0.56    0.90    1.24    1.56    1.99    2.41    2.78    3.15    3.52    3.89    4.28    4.67    5.07    5.50    5.93    6.37    6.84    7.34    7.82
   0.63    1.03    1.41    1.67    2.12    2.57    2.97    3.36    3.76    4.17    4.58    5.00    5.42    5.87    6.32    6.78    7.28    7.81    8.41
   0.69    1.12    1.53    1.78    2.27    2.74    3.18    3.60    4.02    4.44    4.88    5.33    5.79    6.26    6.73    7.24    7.75    8.32    9.00
   0.82    1.24    1.65    1.89    2.41    2.92    3.40    3.83    4.27    4.73    5.20    5.67    6.15    6.66    7.17    7.70    8.27    8.88    9.53
   0.90    1.33    1.76    2.06    2.56    3.10    3.61    4.09    4.57    5.05    5.53    6.03    6.55    7.08    7.65    8.21    8.83    9.45    10.10  
   1.04    1.51    1.96    2.25    2.71    3.27    3.84    4.34    4.85    5.37    5.90    6.43    6.97    7.52    8.13    8.77    9.42    10.10   10.72  
   1.08    1.59    2.09    2.43    2.88    3.47    4.07    4.63    5.16    5.71    6.26    6.85    7.44    8.07    8.70    9.36    10.08   10.80   11.52  
   1.16    1.78    2.37    2.67    3.05    3.68    4.30    4.93    5.50    6.08    6.68    7.29    7.95    8.61    9.30    10.04   10.78   11.52   12.26  
   1.33    1.93    2.51    2.86    3.21    3.88    4.54    5.20    5.84    6.47    7.13    7.80    8.51    9.22    10.01   10.80   11.59   12.38   13.17  
   1.46    2.14    2.72    3.07    3.54    4.11    4.81    5.46    5.96    6.60    7.28    8.03    8.76    9.51    10.31   11.11   11.91   12.71   13.51  
   1.58    2.27    2.93    3.40    3.86    4.35    5.06    5.63    6.14    6.77    7.52    8.27    9.02    9.79    10.60   11.41   12.22   13.03   13.84  
];

% Estimations des emissions de CO2 (g/s) du moteur thermique 60226Y41
MOTH.Reg_2dCO2=[ 0 10000 ]*pi/30;
MOTH.Cpl_2dCO2=[ 0 1000 ];
MOTH.CO2_2d=[
  0 0
  0 0];

% Estimations des emissions de CO (g/s) du moteur thermique 60226Y41
MOTH.Reg_2dCO=[ 0 10000 ]*pi/30;
MOTH.Cpl_2dCO=[ 0 1000 ];
MOTH.CO_2d=[
   0 0
   0 0]./1000;

% Estimations des emissions de HC (g/s) du moteur thermique 60226Y41
MOTH.Reg_2dHC=[ 0 10000 ]*pi/30;
MOTH.Cpl_2dHC=[ 0 1000 ];
MOTH.HC_2d=[
   0 0
   0 0]./1000;

% Estimations des emissions de NOx (g/s) du moteur thermique 60226Y41
MOTH.Reg_2dNOx=[ 0 10000 ]*pi/30;
MOTH.Cpl_2dNOx=[ 0 1000 ];
MOTH.NOx_2d=[
   0 0
   0 0]./1000;

% Estimations des emissions de Particules (g/s) du moteur thermique 60226Y41
MOTH.Reg_2dPart=[ 0 1000 ]*pi/30;
MOTH.Cpl_2dPart=[ 0 1000   ];
MOTH.Part_2d=[
  0 0
  0 0]./1000;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Thermique moteur

% Pour inhiber le calcul de thermique moteur
MOTH.modele_therm=0; 
% Temp initiale moteur
MOTH.Tinit=25;
% Part du carburant transforme en chaleur
MOTH.pci_to_chaleur = 0.617;
% Capcite calorifique equivallente du bloc (J/K)
MOTH.thetaC =41860;
% coefficient de convection
MOTH.k_ext_vit0 =12.53;
MOTH.k_ext_vit =4.85;
% Coefficient de convection radiateur  d eau
MOTH.k_ext_vit0_calor =6.45;
MOTH.k_ext_vit_calor =2.0;
% Temp d ouverture du calorstat
MOTH.Touv_calorstat =85;
% Regulation calorstat ouvert (!)
MOTH.Tcons=80;
MOTH.Kp_calor=1;
MOTH.pci=43000;

display('Fin d''initialisation des parametres moteur thermique'); 

%------------------------------------MISES A JOUR-----------------------------------%
% 18-12-06 (EV/RT) : rajout PID ralenti en parametres
