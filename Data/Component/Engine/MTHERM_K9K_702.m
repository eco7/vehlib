% File: MTHERM_K9K_702
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier moteur thermique
%                      Pour librairie VEHLIB
%                           INRETS - LTE
%  Objet: Ce script initialise la structure MOTH
%          contenant les champs necessaires pour renseigner
%          le composant MOTEUR THERMIQUE de la librairie VEHLIB.
%
%  Cartographie r�alis�e d'apr�s des mesures banc moteur LTE
%  Balance de carburant AVL
% =============================================================================

% Reperage du type de moteur thermique: 10 = moteur thermique cartographie
MOTH.ntypmoth=10;

% Nom du moteur
MOTH.nom='MTHERM_K9K_702';

% Masse du moteur thermique
MOTH.Masse_mth    = 140;

% Inertie du moteur thermique
MOTH.J_mt        = 0.15;

% Regimes de fonctionnement
MOTH.ral_max    = 900*pi/30;                     % regime maxi pour un ralenti (en rad/s)
MOTH.ral_min    = 800*pi/30;                     % regime mini pour un ralenti (en rad/s)
MOTH.ral        = 850*pi/30;                     % regime ralenti (en rad/s)
MOTH.ral_dem    = 900*pi/30;                     % regime de fin d'action du demarreur (regulation)
MOTH.wmt_maxi   =4500*pi/30;                     % regime maxi regulation (en rad/s)

% Regulation de ralenti
MOTH.P_ral=0.1;
MOTH.I_ral=0;
MOTH.D_ral=0;

% Carburant Diesel
MOTH.dens_carb  = 830;                                   % densite du carburant a 20 C (g/l)
MOTH.CO2carb    = 2.0;                                   % rapport masses CO2 carburant pour une combustion parfaite
%(mCO2/Mcarb=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85


% Reponse couple alpha du moteur:
MOTH.alpha_cons=[0 0.08 0.7 1];
MOTH.alpha2cpl= [0 0    1   1];

% Temps de reponse en couple du moteur
MOTH.tr=0.6;

% Courbes enveloppe (estimation pleine charge et coupure d'injection(Nm) en fct regime MT(rad/s)
MOTH.wmt_max=[0 50  100 500 750 1000.1  1250.0  1500.0  1750.0  2000.0  2250.0  2500.0  2750.0  3000.0  3250.0  3500.0  3750.0  4000.1  4250.0   4500   4750]*pi/30;
MOTH.cmt_max=[0  0  0 0 60 103.0   131.0   161.0   177.0   175.0   174.0   172.0   167.0   162.5   154.0   145.0   137.0   130.0   119.0   50   0];
MOTH.cmt_min=[0  -15 -15 -15  -18   -18.0   -18.5   -20.0   -20.5   -21.4   -22.4   -23.4   -25.2   -27.4   -31.8   -34.5   -37.0   -38.2   -41.0   -45  -50];

% courbe optimum utilisee par le calculateur (veh. hybrides)
MOTH.wmt_opti = [ 80 125 MOTH.wmt_max(8:length(MOTH.wmt_max))];
MOTH.cmt_opti = [ 0 0 MOTH.cmt_max(8:length(MOTH.cmt_max))*0.8];
MOTH.pmt_opti = MOTH.cmt_opti.*MOTH.wmt_opti;

% Valeurs donnees par la carto generique.
% Valeurs de regimes en rd/s
MOTH.Reg_2dconso=[ 0.0  500.0   750.0  1000.0  1250.0  1500.0  1750.0  2000.0  2250.0  2500.0  2750.0  3000.0  3250.0  3500.0  3750.0  4000.0  4250.0   4500   4750]*pi/30;
% Valeurs de couples en Nm
MOTH.Cpl_2dconso=[     0.0    10.0    20.0    30.0    40.0    50.0    60.0    70.0    80.0    90.0   100.0   110.0   120.0   130.0   140.0   150.0   160.0   170.0   180.0];
% Valeurs de debit de carburant en g/s
MOTH.Conso_2d=1/3.6*[ 
    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00
    0.25    0.37    0.49    0.62    0.76    0.88    1.09    1.30    1.52    1.84    2.21    2.77    3.06    3.44    3.99    4.28    4.68    5.70    5.94
    0.25    0.37    0.49    0.62    0.76    0.88    1.09    1.30    1.52    1.84    2.21    2.77    3.06    3.44    3.99    4.28    4.68    5.70    5.94
    0.32    0.49    0.67    0.85    1.04    1.23    1.47    1.71    1.95    2.23    2.56    2.77    3.06    3.44    3.99    4.28    4.68    5.70    5.94
    0.39    0.61    0.85    1.08    1.32    1.58    1.85    2.12    2.38    2.62    2.91    3.22    3.53    3.89    3.99    4.28    4.68    5.70    5.94
    0.47    0.74    1.01    1.28    1.56    1.85    2.15    2.44    2.75    3.06    3.37    3.67    4.00    4.34    4.69    5.04    5.46    5.70    5.94
    0.61    0.91    1.21    1.52    1.85    2.19    2.53    2.88    3.23    3.58    3.93    4.27    4.62    4.99    5.39    5.80    6.24    6.72    7.20
    0.72    1.06    1.41    1.77    2.17    2.56    2.96    3.35    3.73    4.11    4.50    4.89    5.32    5.74    6.17    6.64    7.12    7.74    8.36
    0.86    1.24    1.63    2.02    2.46    2.92    3.37    3.82    4.28    4.73    5.19    5.63    6.04    6.45    6.98    7.44    7.95    8.64    9.33
    0.96    1.40    1.85    2.32    2.81    3.30    3.79    4.29    4.80    5.30    5.81    6.33    6.86    7.06    7.55    8.33    8.95    9.78   10.61
    1.20    1.66    2.13    2.64    3.16    3.73    4.30    4.86    5.42    5.99    6.52    7.08    7.71    8.23    8.73    9.39   10.70   11.55   11.70
    1.33    1.84    2.36    2.91    3.52    4.11    4.65    5.22    5.86    6.46    7.07    7.70    8.35    9.04    9.78   10.59   12.00   12.00   12.00
    1.53    2.40    3.00    3.59    4.21    4.79    5.32    5.94    6.58    7.23    7.89    8.59    9.36   10.12   11.02   12.00   13.50   13.50   13.50
    2.19    2.80    3.43    4.07    4.71    5.29    5.94    6.59    7.29    8.02    8.78    9.64   10.43   11.29   12.31   13.75   13.75   13.75   13.75
    2.56    3.24    3.93    4.61    5.31    5.98    6.62    7.28    8.02    8.83    9.63   10.48   11.48   12.61   14.00   14.00   14.00   14.00   14.00
    2.86    3.61    4.32    5.07    5.82    6.49    7.12    7.86    8.68    9.56   10.47   11.43   12.54   14.30   14.30   14.30   14.30   14.30   14.30
    3.19    3.95    4.71    5.44    6.21    6.96    7.75    8.57    9.52   10.47   11.41   12.60   13.60   16.00   16.00   16.00   16.00   16.00   16.00
    3.50    4.50    5.00    6.00    7.00    7.50    8.00    9.00    10.0   11.00   12.00   13.00   14.00   16.00   16.00   16.00   16.00   16.00   16.00
    4.00    5.00    5.50    6.50    7.50    8.00    8.50    9.50    10.5   11.50   12.50   13.50   14.50   16.00   16.00   16.00   16.00   16.00   16.00
];


% Estimations des emissions de CO2 (g/s) du moteur thermique non disponible
MOTH.Reg_2dCO2=[ 1000.0  4500.0 ]*pi/30;
MOTH.Cpl_2dCO2=[  15.0   120.0 ];
MOTH.CO2_2d=[
   0 0
   0 0
];

% Estimations des emissions de CO (g/s) du moteur thermique
MOTH.Reg_2dCO=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dCO=[  15.0 120.0 ];
MOTH.CO_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de HC (g/s) du moteur thermique
MOTH.Reg_2dHC=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dHC=[  15.0  120.0 ];
MOTH.HC_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de NOx (g/s) du moteur thermique
MOTH.Reg_2dNOx=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dNOx=[   15.0  120.0 ];
MOTH.NOx_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de Particules (g/s) du moteur thermique
MOTH.Reg_2dPart=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dPart=[   15.0  120.0 ];
MOTH.Part_2d=[
   0 0
   0 0
]./1000;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Thermique moteur

% Pour inhiber le calcul de thermique moteur
MOTH.modele_therm=0; 
% Temp initiale moteur
MOTH.Tinit=25;
% Part du carburant transforme en chaleur
MOTH.pci_to_chaleur = 0.617;
% Capcite calorifique equivallente du bloc (J/K)
MOTH.thetaC =41860;
% coefficient de convection
MOTH.k_ext_vit0 =12.53;
MOTH.k_ext_vit =4.85;
% Coefficient de convection radiateur  d eau
MOTH.k_ext_vit0_calor =6.45;
MOTH.k_ext_vit_calor =2.0;
% Temp d ouverture du calorstat
MOTH.Touv_calorstat =85;
% Regulation calorstat ouvert (!)
MOTH.Tcons=80;
MOTH.Kp_calor=1;
MOTH.pci=43000;

display('Fin d''initialisation des parametres moteur thermique');

%------------------------------------MISES A JOUR-----------------------------------%
% 11/02/2004 MDR. Fermeture de la cartographie moteur par cples max et adaptation
%                 de la matrice des consos pour ces points ajout?s (entre 4250 et 4750 tr/min).
% 24-03-2004. RT. remise des consos nulles a regime nul
% 18-12-06 (EV/RT) : rajout PID ralenti en parametres
