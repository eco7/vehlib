% File: MTHERM_EP6_mt_niv0
% =============================================================================
%
%  (C) Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier moteur thermique
%                      Pour librairie VEHLIB
%                           INRETS - LTE
%  Objet: Ce script initialise la structure MOTH
%          contenant les champs necessaires pour renseigner
%          le composant MOTEUR THERMIQUE de la librairie VEHLIB.
%
% Moteur thermique 1598 cm3, 16 soupapes, VTi, 120ch
% equipant la 308, identification banc moteur IFSTTAR
% ECU Bosch (rendement indique et frottement+pompage) et FH electronics 
% =============================================================================

%reperage du type de moteur thermique: 2 = moteur thermique essence champ moyen
MOTH.ntypmoth=2;

% Nom du moteur
MOTH.nom='MTHERM_EP6_mt_niv0';

% masse du moteur thermique
MOTH.Masse_mth    = 140;

% inertie du moteur thermique Valeur non trouvee 
MOTH.J_mt        = 0.2 ; % inconnue;		

MOTH.ral_max    = 720*pi/30;			% regime maxi pour un ralenti (en rad/s)
MOTH.ral_min    = 620*pi/30;			% regime mini pour un ralenti (en rad/s)
MOTH.ral        = 700*pi/30;			% regime ralenti (en rad/s)
MOTH.ral_dem	 = 125;			    	% regime de fin d'action du demarreur (regulation)
MOTH.wmt_maxi=6000*pi/30;				% regime maxi regulation (en rad/s)

% Carburant essence
MOTH.dens_carb  = 740;			% densite du carburant
MOTH.CO2carb		= 1.85;					% rapport masses CO2 carburant pour une combustion parfaite 
%(mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85

% Pouvoir calorifique inf??rieur du carburant en J/kg
MOTH.pci=43e6;

% Caracteristiques culasse
% Alesage (m)
MOTH.Alesage=0.077;
% Course (m)
MOTH.Course=0.0855;
% Volume util chambre
MOTH.Vd=MOTH.Course*pi*(MOTH.Alesage)^2/4;
% Nombre de cylindre
MOTH.Ncyl=4;
% Nombre de revolution par cycle (2 ou 4 temps)
MOTH.Rev=2;

% courbe enveloppe (estimation pleine charge en couple(Nm) en fct regime MT(rad/s)
MOTH.wmt_max=[ 0.0   500.0   850.0  1000.0  1250.0  1500.0  2000.0  2500.0  3000.0  3500.0  4000.0  4200.0  4250.0  4300.0  4500.0  5000.0  5500.0  5800.0  5900.0  5950.0  6000]*pi/30;
MOTH.cmt_max=[ 0.0     0.0   107.0   119.7   123.6   129.0   136.0   142.0   150.0   152.0   153.5   156.0   156.5   156.0   154.0   152.0   147.5   141.0   139.0   138.5   0];
MOTH.cmt_min=[ 0.0   -17.6   -18.1   -18.3   -18.7   -19.0   -19.3   -20.7   -21.7   -23.9   -26.4   -27.4   -27.6   -27.8   -28.6   -30.7   -32.6   -33.2   -33.5   -33.8  -34.3];

% Rendement thermodynamique constant
MOTH.RendTh=4.1285e-7;

% Rendement lie a la charge du moteur
MOTH.C1=0.9;
MOTH.RendG1=-2.0158;
MOTH.RendG2=2.8142;

% Rendement lie au regime du moteur
MOTH.RendA=501;
MOTH.RendB=1.0649e6;


