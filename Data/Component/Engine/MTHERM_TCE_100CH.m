% file: MOTH_TCE_100CH.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Cartographie du moteur RENAULT 1L2 TURBO TCE 100CH
% Mesures banc rouleau du LTE
% Campagne Juillet 2011
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% reperage du type de moteur thermique: 1 = moteur thermique cartographie
MOTH.ntypmoth=10;

% Nom du moteur
MOTH.nom='MTHERM_TCE_100CH';

% masse du moteur thermique (en attente)
MOTH.Masse_mth    = 120;

% inertie du moteur thermique Valeur non trouvee 
MOTH.J_mt        = 0.2 ; % inconnue;		
 
%................ESTIMATION DE LA CONSOMMATION (en attente)
MOTH.ral_max    = 670*pi/30;			% regime maxi pour un ralenti (en rad/s)
MOTH.ral_min    = 600*pi/30;			% regime mini pour un ralenti (en rad/s)
MOTH.ral        = 650*pi/30;			% regime ralenti (en rad/s)
MOTH.ral_dem	 = 125;			    	% regime de fin d'action du demarreur (regulation)
MOTH.wmt_maxi=6000*pi/30;				% regime maxi regulation (en rad/s)

% Regulation de ralenti
MOTH.P_ral=0.4;
MOTH.I_ral=0;
MOTH.D_ral=0;

% Carburant essence
MOTH.dens_carb  = 740;			% densite du carburant
MOTH.CO2carb		= 1.85;		% rapport masses CO2 carburant pour une combustion parfaite 
%(mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85

% Reponse couple alpha du moteur:
MOTH.alpha_cons=[0 1];
MOTH.alpha2cpl=[0 1];

% Temps de reponse en couple du moteur
MOTH.tr=0.4; 

% Cartographie de debit de carburant en g/s
MOTH.Reg_2dconso=pi/30*[ 0  1000.0  1500.0  2000.0  2500.0  3000.0  3500.0  4000.0  4500.0  5000.0  5500.0];

MOTH.Cpl_2dconso=[     0.0    10.0    20.0    30.0    40.0    50.0    60.0    70.0    80.0    90.0   100.0   110.0   120.0   130.0   140.0   150.0];

MOTH.Conso_2d=[ 
    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    
    0.13    0.18    0.25    0.31    0.37    0.43    0.50    0.59    0.77    0.94    1.12    1.30    1.47    1.65    1.83    2.00
    0.18    0.26    0.36    0.46    0.56    0.66    0.74    0.84    0.97    1.10    1.24    1.42    1.63    1.83    2.04    2.25
    0.23    0.35    0.48    0.61    0.74    0.87    0.99    1.12    1.27    1.42    1.56    1.74    2.14    2.61    3.08    3.55
    0.27    0.44    0.60    0.77    0.94    1.10    1.26    1.42    1.60    1.78    1.97    2.20    2.46    2.81    3.19    3.56
    0.35    0.55    0.74    0.93    1.13    1.33    1.52    1.72    1.92    2.12    2.33    2.57    2.83    3.15    3.57    3.99
    0.42    0.64    0.87    1.11    1.34    1.57    1.81    2.02    2.27    2.51    2.76    3.05    3.45    3.92    4.44    4.97
    0.45    0.70    0.98    1.26    1.54    1.81    2.08    2.34    2.63    2.92    3.21    3.63    4.16    4.69    5.22    5.75
    0.53    0.83    1.14    1.46    1.77    2.07    2.37    2.67    3.02    3.39    3.76    4.20    4.81    5.41    6.02    6.63
    0.63    0.96    1.32    1.68    2.04    2.37    2.70    3.03    3.44    3.90    4.36    4.92    5.66    6.40    7.14    7.88
    0.75    1.11    1.50    1.89    2.28    2.70    3.11    3.56    4.09    4.62    5.48    6.50    7.52    8.54    9.56   10.58
];

% Couple maximum et couple de frottement du moteur
MOTH.wmt_max=pi/30*[0 50 500  1000.0  1500.0  1750.0  2000.0  2250.0  2500.0  2750.0  3000.0  3250.0  3500.0  3750.0  4000.0  4250.0  4500.0  4750.0  5000.0  5250.0  5500.0  5750.0  6000.0 6100.0];

% courbe enveloppe constructeur
MOTH.wmt_max_th = MOTH.wmt_max;
MOTH.cmt_max_th =[0 0 0  90.0   125.0   135.0   140.0   141.3   142.5   143.8   145.0   145.0   145.0   145.0   145.0   142.5   140.0   138.0   136.0   133.0   130.0   125.0   120.0 0];

% couple max mesure, extrapolation pour les valeurs audela de 5500 tr/min
MOTH.cmt_max=[0 0 0   82.1   117.7   120.15  122.6   129.75  136.9   139.95  143.0   140.2   137.4   138.4   139.4   140.4   141.4   139.85  138.3   130.45  122.6   114.75  106.9 0];
% les 2 premi�res valeurs sont interpolees, les valeurs mesurees sont sans coupure d'injection pour les deux premieres valeurs
MOTH.cmt_min=[0 -16.4 -16.4  -16.5   -16.7   -16.9   -17.0   -17.1   -17.2   -17.7   -18.2   -18.4   -18.6   -18.9   -19.1   -19.1   -19.1   -19.8   -20.4   -21.0   -21.7   -22.3   -23.0 -23.0];

% Courbe de charge optimum regime couple du moteur thermique;
MOTH.wmt_opti = MOTH.wmt_max;
MOTH.cmt_opti = MOTH.cmt_max.*0.75;
MOTH.pmt_opti = MOTH.cmt_opti.*MOTH.wmt_opti;
% Courbe de charge optimum regime couple du moteur thermique calculee;
% MOTH.wmt_opti=[ 53  115.1917  115.1917  138.2301  172.7876  195.8259  207.3451  207.3451  241.9026  287.9793  311.0177  311.0177  322.5368  357.0944  380.1327  414.6902  437.7286  472.2861  495.3244  518.3628  472.2861  472.2861  483.8053  506.8436  541.4011];
% MOTH.cmt_opti=[  0        26.1818   52.3636   65.4545   69.8182   77.0053   87.2727  101.8182   99.7403   94.2545   96.9697  106.6667  112.2078  109.7947  111.0744  109.0909  110.2392  108.5588  109.5983  110.5455  127.7162  134.1020  137.1429  136.8595  133.6944];
% MOTH.pmt_opti = MOTH.cmt_opti.*MOTH.wmt_opti;


% Estimations des emissions de CO2 (g/s) du moteur thermique non disponible
MOTH.Reg_2dCO2=[ 1000.0  4500.0 ]*pi/30;
MOTH.Cpl_2dCO2=[  15.0   120.0 ];
MOTH.CO2_2d=[
   0 0
   0 0
];

% Estimations des emissions de CO (g/s) du moteur thermique
MOTH.Reg_2dCO=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dCO=[  15.0 120.0 ];
MOTH.CO_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de HC (g/s) du moteur thermique
MOTH.Reg_2dHC=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dHC=[  15.0  120.0 ];
MOTH.HC_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de NOx (g/s) du moteur thermique
MOTH.Reg_2dNOx=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dNOx=[   15.0  120.0 ];
MOTH.NOx_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de Particules (g/s) du moteur thermique
MOTH.Reg_2dPart=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dPart=[   15.0  120.0 ];
MOTH.Part_2d=[
   0 0
   0 0
]./1000;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Thermique moteur (valeur reprise des autres mod�les MTHERM)

% Pour inhiber le calcul de thermique moteur (=0)
MOTH.modele_therm=0;
% Temp initiale moteur
MOTH.Tinit=25;
% Part du carburant transforme en chaleur
MOTH.pci_to_chaleur = 0.617;
% Capacite calorifique equivalente du bloc (J/K)
MOTH.thetaC =41860;
% coefficient de convection
MOTH.k_ext_vit0 =12.53;
MOTH.k_ext_vit =4.85;
% Coefficient de convection radiateur  d eau
MOTH.k_ext_vit0_calor =6.45;
MOTH.k_ext_vit_calor =2.0;
% Temp d ouverture du calorstat
MOTH.Touv_calorstat =100;
% Regulation calorstat ouvert (!)
MOTH.Tcons=95;
MOTH.Kp_calor=1;
MOTH.pci=43000;

display('Fin d''initialisation des parametres moteur thermique');