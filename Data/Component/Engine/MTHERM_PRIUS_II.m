% File: MTHERM_PRIUS_II
% =============================================================================
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier moteur thermique
%                      Pour librairie VEHLIB
%                           INRETS - LTE
%  Objet: Ce script initialise la structure MOTH
%          contenant les champs necessaires pour renseigner
%          le composant MOTEUR THERMIQUE de la librairie VEHLIB.
%
% Mesure LTE banc rouleau sur veh instrumente avec capteur PMI
% Rapport LTE 0626 (Nov. 2006)
% =============================================================================

% Reperage du type de moteur thermique: 10 = moteur thermique cartographie
MOTH.ntypmoth=10;

%nom du moteur
MOTH.nom='MTHERM_PRIUS_II';

% Masse du moteur thermique
MOTH.Masse_mth    = 100;

% Inertie du moteur thermique
MOTH.J_mt        = 0.18;		

% Regimes de fonctionnement
MOTH.ral_max    = 90;			% regime maxi pour un ralenti (en rad/s)
MOTH.ral_min    = 75;			% regime mini pour un ralenti (en rad/s)
MOTH.ral        = 83;			% regime ralenti (en rad/s)
MOTH.ral_dem	 = 90;			% regime de fin d'action du demarreur (regulation)
%MOTH.wmt_maxi=4980*pi/30;		% regime maxi regulation (en rad/s)
MOTH.wmt_maxi=4960*pi/30;		% regime maxi regulation (en rad/s) sinon pb vehlib et accel max !!!

% Carburant Essence
MOTH.dens_carb  = 755;			% densite du carburant
MOTH.CO2carb		= 1.85;		% rapport masses CO2 carburant pour une combustion parfaite 
%(mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85

% Reponse couple alpha du moteur:
MOTH.alpha_cons=[0 1];
MOTH.alpha2cpl=[0 1];

% Temps de reponse en couple du moteur
MOTH.tr=0.6;

%  Regulation ralenti
MOTH.P_ral=0.1;
MOTH.I_ral=0;
MOTH.D_ral=0;

% Courbe enveloppe (estimation pleine charge en couple(Nm) en fct regime MT
%MOTH.wmt_max = [    0  50   400   700   900    1028   1289    1725    3669   4325   4979   5040  5050 5500  ]*2*acos(-1)/60;
MOTH.wmt_max = [    0  50   400   700   900    1028   1289    1725    3669   4325   4969   4970  4971 5500  ]*2*acos(-1)/60;
%MOTH.cmt_max = [    0   0     0    20   50    77.7   86.7    94.1   109.7  110.1  106.7   106.7    0    0  ];
MOTH.cmt_max = [    0   0     0    20   50    77.7   86.7      94.1   109    109    106.7   106.7    0    0  ];
MOTH.cmt_min = [    0 -10   -10   -10   -15   -25     -25    -25     -25     -25    -25    -25   -25   -25 ]; 


MOTH.pmt_max = MOTH.cmt_max.*MOTH.wmt_max;   % pour le calculateur

% Courbe de charge optimum regime couple du moteur thermique;
% (determination de cette zone de points de fonctionnement d'apres les essais)
MOTH.wmt_opti   = [ 0 900 1100   1250  1447  2430  3110  4010  4970]*pi/30;
MOTH.cmt_opti   = [ 0   0 4.8   61.13  74.9  88.7    97  102  102  ];
MOTH.pmt_opti = MOTH.cmt_opti.*MOTH.wmt_opti;

% Consommation de carburant en g/s
MOTH.Reg_2dconso=[0  970     1470      1970        2470     2970      3470      3970       4470     4970]*pi/30;
MOTH.Cpl_2dconso=[ -1     9    19    29    39    49    59    69    79    89    99  109];
MOTH.Conso_2d=[ 
     0.0       0.1931    0.1983    0.2582    0.3252    0.4082    0.4718    0.5249    0.5789    0.6602
     0.0       0.2352    0.2451    0.3433    0.4712    0.5549    0.6492    0.7229    0.8319    0.9391
     0.0       0.2773    0.3004    0.4385    0.5715    0.7015    0.8265    0.9416    1.1003    1.2179
     0.0       0.3193    0.3759    0.5333    0.6881    0.8481    1.0039    1.1619    1.3677    1.4970
     0.0       0.3615    0.4514    0.6280    0.8170    0.9947    1.1812    1.3841    1.6352    1.7760
     0.0       0.4064    0.5241    0.7227    0.9458    1.1413    1.3586    1.6063    1.8953    2.0669
     0.0       0.4513    0.5999    0.8175    1.0747    1.2879    1.5359    1.8251    2.1519    2.3591
     0.0       0.4961    0.6871    0.9122    1.2036    1.4264    1.7133    2.0367    2.4221    2.6513
     0.0       0.5410    0.7602    1.0183    1.3290    1.5863    1.8906    2.2482    2.6923    2.9435
     0.0       0.5859    0.8555    1.1208    1.4505    1.7531    2.0538    2.4597    2.9626    3.2357
     0.0       0.6309    0.9546    1.2459    1.5647    1.9198    2.2862    2.6966    3.2328    3.5832
     0.0       0.6836    1.0439    1.3777    1.7160    2.0926    2.5415    2.9927    3.5031    3.6023  ];

MOTH.Conso_2d=MOTH.Conso_2d';


% Estimations des emissions de CO2 (g/s) du moteur thermique
MOTH.Reg_2dCO2=[0  970     1470      1970        2470     2970      3470      3970       4470     4970]*pi/30;
MOTH.Cpl_2dCO2=[ -1     9    19    29    39    49    59    69    79    89    99  109];
MOTH.CO2_2d=[
    0.0       0.6188    0.6255    0.8205    1.0324    1.3004    1.4926    1.6646    1.8356    2.0927
    0.0       0.7501    0.7773    1.0931    1.4976    1.7676    2.0635    2.2995    2.6436    2.9768
    0.0       0.8814    0.9552    1.3955    1.8179    2.2348    2.6344    2.9949    3.5000    3.8609
    0.0       1.0127    1.1997    1.6964    2.1874    2.7020    3.2053    3.6967    4.3522    4.7454
    0.0       1.1445    1.4443    1.9973    2.5950    3.1692    3.7762    4.4012    5.2049    5.6299
    0.0       1.2870    1.6748    2.2981    3.0025    3.6363    4.3470    5.1056    6.0317    6.5528
    0.0       1.4296    1.9018    2.5990    3.4100    4.1035    4.9179    5.8020    6.8470    7.4801
    0.0       1.5721    2.1178    2.8999    3.8175    4.5380    5.4888    6.4811    7.7104    8.4073
    0.0       1.7147    2.3518    3.2423    4.2139    5.0402    6.0597    7.1601    8.5738    9.3345
    0.0       1.8572    2.7113    3.5350    4.6030    5.5664    6.5778    7.8391    9.4372   10.2618
    0.0       2.0001    3.0297    3.9532    4.9544    6.0925    7.3109    8.5988   10.3006   11.3636
    0.0       2.1673    3.3097    4.3677    5.4400    6.6343    8.1238    9.5503   11.1641   12.4000]./1000;

MOTH.CO2_2d=MOTH.CO2_2d';

% Estimations des emissions de CO (g/s) du moteur thermique
MOTH.Reg_2dCO=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dCO=[  15.0 120.0 ];
MOTH.CO_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de HC (g/s) du moteur thermique
MOTH.Reg_2dHC=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dHC=[  15.0  120.0 ];
MOTH.HC_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de NOx (g/s) du moteur thermique
MOTH.Reg_2dNOx=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dNOx=[   15.0  120.0 ];
MOTH.NOx_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de Particules (g/s) du moteur thermique
MOTH.Reg_2dPart=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dPart=[   15.0  120.0 ];
MOTH.Part_2d=[
   0 0
   0 0
]./1000;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Thermique moteur

% Pour inhiber le calcul de thermique moteur
MOTH.modele_therm=0; 
% Temp initiale moteur
MOTH.Tinit=25;
% Part du carburant transforme en chaleur
MOTH.pci_to_chaleur = 0.617;
% Capcite calorifique equivallente du bloc (J/K)
MOTH.thetaC =41860;
% coefficient de convection
MOTH.k_ext_vit0 =12.53;
MOTH.k_ext_vit =4.85;
% Coefficient de convection radiateur  d eau
MOTH.k_ext_vit0_calor =6.45;
MOTH.k_ext_vit_calor =2.0;
% Temp d ouverture du calorstat
MOTH.Touv_calorstat =85;
% Regulation calorstat ouvert (!)
MOTH.Tcons=80;
MOTH.Kp_calor=1;
MOTH.pci=43000;

display('Fin d''initialisation des parametres moteur thermique'); 

%---------------------------------------MISES A JOUR----------------------------%
% 27-10-05 (EV): creation fichier MTHERM_PRIUS_II ??? partir MTHERM_PRIUS97_IFP
%                Modif courbe couple max: donn???es livret_r???paration PRIUS II (CD20)
% 20-02-06 (EV) : Carto du moteur thermique issue des mesures sur le banc
% 18-12-06 (EV/RT) : rajout PID ralenti en parametres
% 28-11-12 (EV) : Modif courbe MOTH.cmt_max et MOTH.wmt_max et
% MOTH.wmt_maxi pour passer les fonction d'optimisation de groupe
% electrogene et les accel max et que cmt_max soit < au valeur max de
% couple de la carto.
