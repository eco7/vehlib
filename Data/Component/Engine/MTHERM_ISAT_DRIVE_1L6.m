% File: MTHERM_EP6
% =============================================================================
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier moteur thermique
%                      Pour librairie VEHLIB
%                           INRETS - LTE
%  Objet: Ce script initialise la structure MOTH
%          contenant les champs necessaires pour renseigner
%          le composant MOTEUR THERMIQUE de la librairie VEHLIB.
%
% =============================================================================

%reperage du type de moteur thermique: 3 = modele moteur ISAT
MOTH.ntypmoth=3;

% Nom du moteur
MOTH.nom='MTHERM_ISAT_DRIVE_1L6';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Caracteristiques moteur

% Cylindree du moteur (cm3)
MOTH.Vd=1.598;

% Couple max du moteur (Nm)
MOTH.Tmax=87.9*MOTH.Vd; %valeur par defaut, rentrer directement si connue

% Nombre de cylindre
MOTH.Ncyl=4;

% Nombre de revolution par cycle (2 ou 4 temps)
MOTH.Rev=2;

% masse du moteur thermique
MOTH.Masse_mth=140;

% inertie du moteur thermique Valeur non trouvee
MOTH.J_mt=0.2 ; %inconnue

% Carburant essence
MOTH.dens_carb  = 740;			% densite du carburant
MOTH.CO2carb		= 1.85;					% rapport masses CO2 carburant pour une combustion parfaite 
%(mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Limites de regime et couple

% Regimes limites
MOTH.ral        = 750*pi/30;			% regime ralenti (en rad/s)
MOTH.wmt_maxi   = 6000*pi/30;			% regime maxi regulation (en rad/s)

% couple max : parametres de la courbe (parabole en regime)
% /!\ attention, l'equation est donnee avec N en rpm
% Cmax = (MOTH.Tmax_N2*N.^2 + MOTH.Tmax_N*N + MOTH.Tmax_cst)*MOTH.Tmax/MOTH.Tmax_Tref;
MOTH.Tmax_N2 = -4.2e-6; %coefficient du terme en N^2
MOTH.Tmax_N = 0.032; %coefficient du terme en N
MOTH.Tmax_cst = 105; %coefficient constant
MOTH.Tmax_Tref = 165; %rapport d'homothetie par rapport a un Cmax de reference

% couple min : parametres de la courbe
% la courbe est initalement en pression moyenne de frottement
% PMF = MOTH.PMF_cst + MOTH.PMF_N*N;
% le couple de frottement se deduit grace a la cylindree
% Cmin = -PMF*Vd/(Rev*2*pi)
% Cmin = -(MOTH.PMF_cst + MOTH.PMF_N*N)*MOTH.Vd/(MOTH.Rev*2*pi)
MOTH.PMF_cst = 100; %constante de pression moyenne de frottement (en kPa)
MOTH.PMF_N = 20/1000; %pression moyenne de frottement proportionnelle (en kPa/rpm)
MOTH.PMF_N2 = 0; %pression moyenne de frottement quadratique (en kPa/rpm^2)
% attention unites : [kPa*dm3] = [Pa*m3] = [Nm]
% il faut que la cylindree soit donee en litre et les pressions moyennes en kPa

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimations de la consommation (g/s) du moteur thermique

% dcarb = (wmt*Cmt + wmt*(MOTH.PMF_cst + MOTH.PMF_N*N)*MOTH.Vd/(MOTH.Rev*2*pi))
%          /(MOTH.n_fi*(MOTH.n_c0-MOTH.n_cA/(MOTH.n_cB+N))*MOTH.pci)
MOTH.n_fi = 0.4; %rendement indique du carburant (fuel indicated efficiency)
MOTH.n_c0 = 0.98; %constante de rendement de combustion
MOTH.n_cA = 300; %constante de proportionalite : hyperbole rendement de combustion
MOTH.n_cB = 2000; %constante denominateur : hyperbole rendement de combustion
MOTH.pci = 44e3; %pouvoir calorifique de l'essence en J/g

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Emissions et temperatures pour un moteur a allumage commande
% Extrapolation mesures ISAT

MOTH.emissionModel = 2;

MOTH.lambda = 1; %on s'interesse uniquement a la thermique du catalyseur
MOTH.AStoechio = 14.7; %rapport air/fuel

% Exhaust gas Temperature (N en rpm, Texh en K, PME en Bars)
% /!\ attention aux unites !
% Texh = MOTH.Texh_cst + MOTH.Texh_Pad*N*PME;
MOTH.Texh_cst = 6.275785039019664e+02; %temperature a l'origine en K
MOTH.Texh_Pad = 0.012949786280493; %terme proportionnel a la puissance adimensionnee (PME au lieu du couple)

% Emissions (N en rpm, PME en Bars, CO en %vol, HC et NOx en ppm)
% /!\ attention aux unites !
% [X] = MOTH.X_cst + MOTH.X_Pad*N*PME;
MOTH.CO_cst = 0.847821252790832; % %vol de CO a l'orgine
MOTH.CO_Pad = -9.511982354145730e-06; %terme proportionnel a la puissance adimensionnee (PME au lieu du couple)
MOTH.mean_CO = 0.687111111111111; %moyenne, fit d'ordre 0

MOTH.HC_cst = 4.087938006501966e+02; % ppm de HC a l'orgine
MOTH.HC_Pad = -0.006278079104018; %terme proportionnel a la puissance adimensionnee (PME au lieu du couple)
MOTH.mean_HC = 3.027222222222222e+02; %moyenne, fit d'ordre 0

MOTH.NOx_cst = 3.137703918324894e+03; % ppm de NOx a l'orgine
MOTH.NOx_Pad = 0.012509310382318; %terme proportionnel a la puissance adimensionnee (PME au lieu du couple)
MOTH.mean_NOx = 3.349055555555556e+03; %moyenne, fit d'ordre 0

% Average molecular weight of exhaust gas
MOTH.Mexh=30; % g/mol

if exist('param','var') && isfield(param,'verbose') && param.verbose
    wmt = linspace(MOTH.ral,MOTH.wmt_max,20);
    N = wmt*30/pi;
    Cmt = linspace(-25,MOTH.Tmax,15);
    matCmt = repmat(Cmt', size(wmt))';
    matWmt = repmat(wmt', size(Cmt));
    matN = matWmt*30/pi;
    matPME = MOTH.Rev*2*pi*1e-2*Cmt/MOTH.Vd; %Bar
    
    Cmax = (MOTH.Tmax_N2*N.^2 + MOTH.Tmax_N*N + MOTH.Tmax_cst)*MOTH.Tmax/MOTH.Tmax_Tref;
    Cmin = -(MOTH.PMF_cst + MOTH.PMF_N*N)*MOTH.Vd/(MOTH.Rev*2*pi);
    
    dcarb = (matWmt.*matCmt + matWmt.*(MOTH.PMF_cst + MOTH.PMF_N*matN)*MOTH.Vd/(MOTH.Rev*2*pi))...
        ./(MOTH.n_fi*(MOTH.n_c0-MOTH.n_cA./(MOTH.n_cB+matN))*MOTH.pci);
    dcarb_min = (wmt.*Cmin + wmt.*(MOTH.PMF_cst + MOTH.PMF_N*N)*MOTH.Vd/(MOTH.Rev*2*pi))...
        ./(MOTH.n_fi*(MOTH.n_c0-MOTH.n_cA./(MOTH.n_cB+N))*MOTH.pci);
    dcarb_max = (wmt.*Cmax + wmt.*(MOTH.PMF_cst + MOTH.PMF_N*N)*MOTH.Vd/(MOTH.Rev*2*pi))...
        ./(MOTH.n_fi*(MOTH.n_c0-MOTH.n_cA./(MOTH.n_cB+N))*MOTH.pci);
    CSE = 3.6e6*dcarb./(matCmt.*matWmt);
    
    Texh = MOTH.Texh_cst + MOTH.Texh_Pad*matN.*matPME;
    CO = MOTH.CO_cst + MOTH.CO_Pad*matN.*matPME;
    HC = MOTH.HC_cst + MOTH.HC_Pad*matN.*matPME;
    NOx = MOTH.NOx_cst + MOTH.NOx_Pad*matN.*matPME;
    
    figure()
    [C,h] = contour(N,Cmt,CSE',-600:10:600);
    clabel(C,h,[270 360 450 540]);
    hold on
    plot(N,Cmin,N,Cmax)
    
    figure()
    surf(N,Cmt,dcarb')
    hold on
    scatter3(N,Cmin,dcarb_min,'o')
    scatter3(N,Cmax,dcarb_max,'*')
    
    figure()
    contour(N,Cmt,Texh','ShowText','on')
    
    figure()
    subplot(3,1,1)
    contour(N,Cmt,CO','ShowText','on')
    subplot(3,1,2)
    contour(N,Cmt,HC','ShowText','on')
    subplot(3,1,3)
    contour(N,Cmt,NOx','ShowText','on')
end

display('Fin d''initialisation des parametres moteur thermique');
