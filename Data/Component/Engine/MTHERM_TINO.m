% File: MTHERM_TINO
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier moteur thermique
%                      Pour librairie VEHLIB
%                           INRETS - LTE
%  Objet: Ce script initialise la structure MOTH
%          contenant les champs necessaires pour renseigner
%          le composant MOTEUR THERMIQUE de la librairie VEHLIB.
% =============================================================================

%reperage du type de moteur thermique: 10 = moteur thermique cartographie
MOTH.ntypmoth=10;

%nom du moteur
MOTH.nom='MTHERM_TINO';

% masse du moteur thermique
MOTH.Masse_mth    = 175;

% inertie du moteur thermique
MOTH.J_mt        = 0.18;		

% Regimes de fonctionnement
MOTH.ral_max    = 90;			% regime maxi pour un ralenti (en rad/s)
MOTH.ral_min    = 75;			% regime mini pour un ralenti (en rad/s)
MOTH.ral        = 83;			% r?gime ralenti (en rad/s)
MOTH.ral_dem	 = 125;				% r?gime de fin d'action du d?marreur (r?gulation)
MOTH.wmt_maxi=5000*pi/30;				% regime maxi regulation (en rad/s)

% Carburant Essence
MOTH.dens_carb  = 740;			% densite du carburant
MOTH.CO2carb	= 1.85;					% rapport masses CO2 carburant pour une combustion parfaite 
%(mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85

% Reponse couple alpha du moteur:
MOTH.alpha_cons=[0 1];
MOTH.alpha2cpl=[0 1];

%gradient de couple maximum
MOTH.grd_cmt_mont=70;
MOTH.grd_cmt_desc=-7000;

%  regulation ralenti
MOTH.P_ral=0.1;
MOTH.I_ral=0;
MOTH.D_ral=0;

% courbe enveloppe (estimation pleine charge en couple(Nm) en fct regime MT(rad/s)
MOTH.wmt_max = [0 500 700 1000 2000 2500 3000 4000 5000 5500 6000 6100 6200]*pi/30;
MOTH.cmt_max = [0  0   50  100  130 150   155  160 135   110  50    0    0 ];
MOTH.cmt_min = [0 -10 -10 -10  -15  -15  -15  -15  -15   -15 -15   -15 -15 ]; 
MOTH.pmt_max = MOTH.cmt_max.*MOTH.wmt_max;   % pour le calculateur

% Courbe de charge optimum regime couple du moteur thermique;
MOTH.wmt_opti   = [700 1000  2000 2500 3000 4000 5000]*pi/30;
MOTH.cmt_opti   = [ 0   80   110   130  135  140  115];
MOTH.pmt_opti = MOTH.cmt_opti.*MOTH.wmt_opti;

% Estimations de la consommation (g/s) du moteur thermique
MOTH.Reg_2dconso=[0 700 900 1000 1500 2000 2500 3000 3500 4000 4200]*pi/30*6.5/4;
MOTH.Cpl_2dconso=[5.96 11.93 23.87 35.8 47.73 59.67 71.6 77.56 83.53 86.52 98.45]*2;
MOTH.Conso_2d=[
%reg0  700    900    1000    1500  2000   2500   3000   3500   4000   4200
0.000  0.130  0.156  0.173  0.260  0.347  0.433  0.520  0.607  0.693  0.728
0.000  0.150  0.172  0.174  0.260  0.347  0.434  0.521  0.607  0.694  0.802
0.000  0.220  0.269  0.285  0.417  0.514  0.642  0.771  0.899  1.083  1.167
0.000  0.300  0.337  0.364  0.531  0.625  0.781  0.937  1.093  1.250  1.443
0.000  0.350  0.412  0.458  0.666  0.805  0.972  1.166  1.361  1.555  1.691
0.000  0.450  0.484  0.538  0.768  0.937  1.172  1.406  1.640  1.875  2.041
0.000  0.490  0.553  0.614  0.875  1.125  1.354  1.625  1.895  2.166  2.362
0.000  0.520  0.558  0.620  0.914  1.173  1.410  1.692  1.974  2.256  2.464
0.000  0.550  0.590  0.656  0.966  1.215  1.458  1.749  2.084  2.430  2.653
0.000  0.560  0.612  0.680  0.982  1.208  1.447  1.737  2.070  2.517  2.748
0.000  0.600  0.722  0.802  1.117  1.317  1.718  2.062  2.506  2.864  3.127]*2*6.5/4;
MOTH.Conso_2d=MOTH.Conso_2d';

% Estimations des emissions de CO2 (g/s) du moteur thermique
MOTH.Reg_2dCO2=[ 1500 2000 2500 2750 3000 3250 3500 3750 4000 4500 5000 ]*pi/30*6.5/4;
MOTH.Cpl_2dCO2=[ 11.93   23.87  35.80  47.73  59.67  71.60  83.53  95.47  107.40  122.91 ]*2;
MOTH.CO2_2d=[0.848  1.048  1.605  0.744  1.874  2.019  2.211  2.499  2.526  3.061  3.314
1.135  1.528  2.031  0.955  2.416  2.685  2.989  3.281  3.472  3.968  4.340
1.468  1.895  2.525  1.546  3.030  3.351  3.718  4.022  4.207  4.452  5.467
1.770  2.471  3.055  3.322  3.665  4.061  4.422  4.738  5.054  5.436  6.456
2.265  2.812  3.558  3.914  4.270  4.739  5.103  5.012  5.554  6.405  7.377
2.374  3.291  4.113  4.525  4.936  5.415  5.759  5.936  6.415  7.217  8.227
2.879  3.839  4.677  5.145  5.613  5.686  6.123  6.560  6.901  7.873  8.747
3.374  4.332  5.346  5.804  5.665  6.318  6.707  7.186  7.665  8.498  9.442
3.187  4.249  4.921  5.498  6.092  6.600  6.889  7.498  8.123  9.138  10.153
3.486  4.648  5.631  6.194  6.972  7.320  7.884  8.447  9.010  10.136  11.262]*2*6.5/4;
MOTH.CO2_2d=MOTH.CO2_2d';

% Estimations des emissions de CO (g/s) du moteur thermique
MOTH.Reg_2dCO=[ 1500 2000 2500 2750 3000 3250 3500 3750 4000 4500 5000 ]*pi/30*6.5/4;
MOTH.Cpl_2dCO=[ 11.93   23.87  35.80  47.73  59.67  71.60  83.53  95.47  107.40  122.91  ]*2;
MOTH.CO_2d=[0.020  0.027  0.040  0.019  0.052  0.054  0.072  0.082  0.123  0.148  0.453
0.027  0.043  0.054  0.024  0.064  0.077  0.092  0.104  0.179  0.214  0.671
0.031  0.044  0.051  0.030  0.061  0.079  0.106  0.119  0.216  0.861  0.674
0.032  0.059  0.070  0.087  0.093  0.091  0.130  0.156  0.265  0.829  0.761
0.043  0.067  0.092  0.112  0.124  0.142  0.183  0.586  0.689  0.874  0.949
0.049  0.082  0.113  0.133  0.147  0.162  0.802  0.723  0.753  1.086  1.239
0.068  0.106  0.133  0.151  0.184  0.829  0.902  1.011  1.172  1.580  1.458
0.086  0.122  0.165  0.189  0.907  0.902  1.088  1.215  1.406  2.073  1.666
0.656  0.843  1.562  1.718  1.342  1.515  1.831  1.936  1.949  2.537  1.874
0.804  1.073  1.788  2.458  1.515  2.324  2.503  2.681  2.860  3.218  3.575]*2*6.5/4;
MOTH.CO_2d=MOTH.CO_2d';

% Estimations des emissions de HC (g/s) du moteur thermique
MOTH.Reg_2dHC=[ 1500 2000 2500 2750 3000 3250 3500 3750 4000 4500 5000]*pi/30*6.5/4;
MOTH.Cpl_2dHC=[  11.93   23.87  35.80  47.73  59.67  71.60  83.53  95.47  107.40  122.91  ]*2;
MOTH.HC_2d=[0.0060  0.0068  0.0083  0.0087  0.0075  0.0073  0.0080  0.0080  0.0092  0.0099  0.0174
0.0074  0.0093  0.0101  0.0108  0.0110  0.0046  0.0145  0.0150  0.0167  0.0168  0.0244
0.0088  0.0102  0.0116  0.0133  0.0135  0.0135  0.0197  0.0200  0.0214  0.0288  0.0272
0.0094  0.0121  0.0134  0.0150  0.0158  0.0167  0.0226  0.0225  0.0238  0.0306  0.0306
0.0113  0.0130  0.0154  0.0166  0.0175  0.0180  0.0248  0.0286  0.0315  0.0334  0.0356
0.0111  0.0157  0.0163  0.0167  0.0176  0.0182  0.0328  0.0337  0.0344  0.0376  0.0404
0.0142  0.0170  0.0166  0.0171  0.0200  0.0355  0.0372  0.0385  0.0402  0.0443  0.0486
0.0184  0.0198  0.0191  0.0190  0.0404  0.0412  0.0412  0.0419  0.0443  0.0514  0.0555
0.0328  0.0354  0.0433  0.0558  0.0514  0.0545  0.0524  0.0519  0.0517  0.0586  0.0625
0.0509  0.0572  0.0760  0.0983  0.0629  0.0720  0.0676  0.0670  0.0644  0.0676  0.0715]*2*6.5/4;
MOTH.HC_2d=MOTH.HC_2d';

% Estimations des emissions de NOx (g/s) du moteur thermique XUD9A
MOTH.Reg_2dNOx=[ 1500 2000 2500 2750 3000 3250 3500 3750 4000 4500 5000 ]*pi/30*6.5/4;
MOTH.Cpl_2dNOx=[  11.93   23.87  35.80  47.73  59.67  71.60  83.53  95.47  107.40  122.91 ]*2;
MOTH.NOx_2d=[
0.0051  0.0097  0.0151  0.0157  0.0132  0.0175  0.0149  0.0097  0.0185  0.0306  0.0375
0.0115  0.0167  0.0238  0.0191  0.0224  0.0334  0.0227  0.0214  0.0208  0.0447  0.0632
0.0147  0.0223  0.0280  0.0229  0.0256  0.0521  0.0419  0.0443  0.0470  0.0696  0.0928
0.0194  0.0303  0.0368  0.0360  0.0384  0.0686  0.0661  0.0740  0.0710  0.0987  0.1229
0.0234  0.0382  0.0464  0.0475  0.0566  0.0753  0.0832  0.0931  0.1110  0.1315  0.1440
0.0275  0.0525  0.0656  0.0706  0.0815  0.0995  0.0889  0.1090  0.1246  0.1252  0.1463
0.0667  0.0680  0.0957  0.1018  0.1122  0.0908  0.0952  0.1035  0.1035  0.1126  0.1336
0.0766  0.1044  0.1364  0.1441  0.0801  0.0988  0.0996  0.1052  0.1181  0.1011  0.1389
0.0281  0.0375  0.0351  0.0430  0.0616  0.0670  0.0711  0.0758  0.0952  0.0986  0.1093
0.0161  0.0215  0.0268  0.0295  0.0631  0.0697  0.0751  0.0804  0.0858  0.0965  0.1073]*2*6.5/4;
MOTH.NOx_2d=MOTH.NOx_2d';

% Estimations des emissions de Particules (g/s) du moteur thermique
MOTH.Reg_2dPart=[  ]*pi/30;
MOTH.Cpl_2dPart=[   ];
MOTH.Part_2d=[];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Thermique moteur

% Pour inhiber le calcul de thermique moteur
MOTH.modele_therm=0; 
% Temp initiale moteur
MOTH.Tinit=25;
% Part du carburant transforme en chaleur
MOTH.pci_to_chaleur = 0.617;
% Capcite calorifique equivallente du bloc (J/K)
MOTH.thetaC =41860;
% coefficient de convection
MOTH.k_ext_vit0 =12.53;
MOTH.k_ext_vit =4.85;
% Coefficient de convection radiateur  d eau
MOTH.k_ext_vit0_calor =6.45;
MOTH.k_ext_vit_calor =2.0;
% Temp d ouverture du calorstat
MOTH.Touv_calorstat =85;
% Regulation calorstat ouvert (!)
MOTH.Tcons=80;
MOTH.Kp_calor=1;
MOTH.pci=43000;

display('Fin d''initialisation des parametres moteur thermique'); 


%---------------------------------------MISES A JOUR----------------------------%
% 18-12-06 (EV/RT) : rajout PID ralenti en parametres
