% File: MTHERM_CONTROL_TCE130
% =============================================================================
% 
%                Engine data file for VEHLIB library
%                Univ Gustave Eiffel - LICIT-Eco7 Lab.
%
%  Object: This script initialize The structure MOTH
%          defining the engine parameters
%
% =============================================================================

% Type of engine model 
% Model with exhaust emissions and engne control parameters 
% (intake pressure, Air to fuel ratio and spark advance)
MOTH.ntypmoth = 5;

% Engine name
MOTH.nom = 'MTHERM_CONTROL_TCE130';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Engine characteristics

% Engine displacement (dm3)
MOTH.Vd = 1.2;

% Number of cylinder
MOTH.Ncyl = 4;

% Number of revolution per cycle
MOTH.Rev = 2;

% Mass
MOTH.Masse_mth = 140;

% Inertia
MOTH.J_mt = 0.2 ; %inconnue

% Gazoline engine
MOTH.dens_carb  = 720; % Density
MOTH.CO2carb = 1.85; % ratio  CO2 mass/ fuel mass with a perfect combustion
%(mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85

% Speed limits
MOTH.ral = 750*pi/30; % regime ralenti (en rad/s)

MOTH.ral_min = 700*pi/30; % pour les calages moteur
MOTH.ral_max = 800*pi/30; % pour les calages moteur

MOTH.N_maxi = 6000; % regime maxi regulation (en rpm)
MOTH.wmt_maxi = MOTH.N_maxi*pi/30; % regime maxi regulation (en rad/s)

% Reponse couple alpha du moteur:
MOTH.alpha_cons = [	0.00	1.00];
MOTH.alpha2cpl = [	0.00	1.00];


% Torque limit
% couple max : consequence du modele
% couple min : parametres de la courbe de frottement
% la courbe est initalement en pression moyenne de frottement
% PMF = MOTH.PMF_cst + MOTH.PMF_N*N + MOTH.PMF_N2*N.^2;
% le couple de frottement se deduit grace a la cylindree
% Cmin = -PMF*Vd/(Rev*2*pi)
% Cmin = -(MOTH.PMF_cst + MOTH.PMF_N*N + MOTH.PMF_N2*N.^2)*MOTH.Vd/(MOTH.Rev*2*pi)
MOTH.PMF_cst = 166; %constante de pression moyenne de frottement (en kPa)
MOTH.PMF_N = -0.02495963; %pression moyenne de frottement proportionnelle (en kPa/rpm)
MOTH.PMF_N2 = 0.00000803; %pression moyenne de frottement quadratique (en kPa/rpm^2)
% attention unites : [kPa*dm3] = [Pa*m3] = [Nm]
% il faut que la cylindree soit en litre et les pressions moyennes en kPa

% Estimations de la consommation (g/s) du moteur thermique
% r_phi = repmat(MOTH.remp_cst,szD);
% r_phi(Phi>MOTH.remp_Phi_cst) = interp1([MOTH.remp_Phi_cst MOTH.remp_Phi_max],[MOTH.remp_cst MOTH.remp_max],Phi(Phi>MOTH.remp_Phi_cst));
MOTH.remp_cst = 0.9; %facteur de remplissage constant pour Phi<remp_Phi_cst
MOTH.remp_Phi_cst = 1; %richesse a partir de laquelle le remplissage change
MOTH.remp_max = 1; %facteur de remplissage maximal pour Phi=remp_Phi_max
MOTH.remp_Phi_max = 1.2; %richesse pour le rendement max
% remplissage = r_phi - r_N*(N_max-N).^2;
MOTH.remp_N_max = 2250; %regime pour le remplissage maximal (rpm)
MOTH.remp_r_N = 0.2/((MOTH.N_maxi-MOTH.remp_N_max)^2); %facteur quadratique de remplissage f(regime) (rpm-2)
% T_adm = Tamb + Tadm_N*N + Tadm_cst;
MOTH.Tadm_N = (3.9-15.5)/(4500-700); %facteur proportionnel de chute de temperature f(regime) (K/rpm)
MOTH.Tadm_cst = 3.9 - MOTH.Tadm_N*4500; %constante de chute de temperature (K)
% Intake Pressure (supposed to be at admission temperature)
MOTH.p_intake_wmt=[78.54  104.72  130.90  157.08  183.26  209.44  235.62  261.80  287.98  314.16  340.34  366.52  392.70  418.88  445.06  471.24  497.42  523.60  549.78  575.96  602.14  628.32];
MOTH.p_intake=[960.00  1200.00  1440.00  1820.00  1940.00  1940.00  1920.00  1920.00  1920.00  1940.00  1940.00  1940.00  1940.00  1940.00  1940.00  1960.00  1960.00  1980.00  2000.00  2000.00  2000.00  2000.00];

% dgaz = remplissage.*Padm*10^2*Vd.*N/(2*60)*M_gf./(R_gaz*T_adm); %kg/s
% dcarb = dgaz*1000./(1+AStoechio./Phi); %g/s
MOTH.M_gf = 0.030247933884298; %masse molaire des gaz frais (kg/mol)
MOTH.R_gaz =  8.314; %constante gaz parfaits J K-1 mol-1


% estimation du couple (Nm)
% n_comb = VD.MOTH.n_c0 - VD.MOTH.n_cA./(VD.MOTH.n_cB + N);
MOTH.n_c0 = 0.98; %constante de rendement de combustion
MOTH.n_cA = 300; %constante de proportionalite : hyperbole rendement de combustion
MOTH.n_cB = 2000; %constante denominateur : hyperbole rendement de combustion

% Optimal Spark Advance
% AA_0 = interp2(AA_Padm,AA_N,AA_0_N_Padm',Padm,N);
% n_AA = 1 - k_AA*(AA - AA_0).^2;
% table d'interpolation de l'avance optimale AA0 (�BTDC)
% Ajout d'une ligne pour Padm = 0 mbar (pas dans la table de calibration)
% HYPOTHESE : saturation TODO: verifier avec Alan
MOTH.AA0_N_Padm = [29	30	33	36	38      40      40      40      44.1	40      40	40	40	40
               29	30	33	36	38      40      40      40      44.1	40      40	40	40	40
               30	30	30	33	36      40.1	40.1	38.1	42.1	40.1	40	40	40	40
               28	28	30	32	33.3	35.1	35.1	34.1	34.1	34.1	34	34	34	34
               22	25	28	30	31.1	33.1	31.1	29.1	32.1	31.1	31	31	31	31
               20	23	25	27	28.1	29.1	28.1	27.1	29.1	30.1	29	29	29	29
               19	20	20	21	23.1	27.1	28.1	26.1	26.1	26.1	26	26	26	26
               21	20	20	24	21.1	26.1	28.1	24.1	25.1	24.1	25	25	25	25
               20	22	20	22	21.1	24.1	24.1	24.1	24.1	24.1	24	24	24	24
               20	20	20	17	19.1	21.1	21.1	20.1	23.1	24.1	21	21	21	29
               20	20	20	17	19.1	21.1	21.1	20.1	23.1	24.1	21	21	21	29];
MOTH.AA_Padm = [0 200:100:1000 3000]; % last line copied twice for interpolation purposes
MOTH.AA_N = [500 750 1000:500:3500 3750 4000:500:6000];
MOTH.k_AA = 0.04e-2; %facteur quadratique de chute d'efficacite en f(regime^2)

% efficacite en fonction de lambda
% inspire de guzzela et onder + decroissance richesse < 1
% n_rich(lambda<lambda1) = g1*lambda(lambda<lambda1) - g0;
% n_rich(lambda>=lambda1&lambda<lambda2) = e1 + (1-e1)*sin((lambda(lambda>=lambda1&lambda<lambda2)-lambda1)/(1-lambda1));
% n_rich(lambda>=lambda2)=1;
% n_rich(lambda>=lambda3&lambda<lambda4) = b_lambda + a_lambda*lambda(lambda>=lambda3&lambda<lambda4);
% n_rich(lambda>=lambda4)=0;
MOTH.lambda1 = 0.95;
MOTH.lambda2 = MOTH.lambda1 + pi/2*(1-MOTH.lambda1);
MOTH.g1 = 1.373;
MOTH.g0 = 0.373;
MOTH.e1 = MOTH.g1*MOTH.lambda1 - MOTH.g0;
% modification melange pauvre
MOTH.lambda3 = 1.3; %1/0.7;
MOTH.lambda4 = 1/0.5; %1/0.6;
MOTH.a_lambda = -1/(MOTH.lambda4 - MOTH.lambda3);
MOTH.b_lambda = 1 - MOTH.a_lambda*MOTH.lambda3;

MOTH.n_ind = 0.4076; %rendement indique du carburant (fuel indicated efficiency)

MOTH.pci = 44e3; %pouvoir calorifique de l'essence en J/g

% Emissions et temperatures pour un moteur a allumage commande
% Extrapolation mesures ISAT
MOTH.emissionModel = 3;

MOTH.AStoechio = 14.7; %rapport air/fuel

% Exhaust gas Temperature (N en rpm, Texh en K, Padm en mBars)
% elevation de temperature depuis Tadm grace a l'energie de combustion
MOTH.cp_gaz = 1.2e3; % capacite calorifique des gaz
% la fraction d'energie recuperee thermiquement depend de Phi, AA, N, Padm
% facteurs determines avec un fit du modele aux donnees (fminsearch)
MOTH.aPhi = -0.771169579806391; %facteur proportionnel rendement f(Phi)
MOTH.bPhi = 1.503004166402986; %facteur constant rendement f(Phi)
MOTH.aAA = 5.506973975857715e-05; %facteur quadratique rendement f(AA)
MOTH.bAA = 0.311394868889090; %facteur constant rendement f(AA)
MOTH.aN = 1.467759178826217e-04; %facteur proportionnel rendement f(N)
MOTH.bN = 0.250991230988239; %facteur constant rendement f(N)
MOTH.aP = 0.001080007806274; %facteur proportionnel rendement f(Padm)
MOTH.bP = 0.805190699672296; %facteur constant rendement f(N)
% n_Phi = VD.MOTH.bPhi + VD.MOTH.aPhi*Phi;
% n_AA = VD.MOTH.bAA + VD.MOTH.aAA*(AA-AA_0-AA_opt).^2;
% n_N = VD.MOTH.bN + VD.MOTH.aN*N;
% n_Padm = VD.MOTH.bP + VD.MOTH.aP*Padm;
% Texh = T_adm + VD.MOTH.pci*1e3./(VD.MOTH.cp_gaz*(1+lambdaFG*VD.MOTH.AStoechio)).*...
%     n_Phi.*n_AA.*n_N.*n_Padm;

% Emissions (N en rpm, Padm en mBar, CO en %vol, HC et NOx en ppm)

% [CO] ne depend que de la richesse
% COppm = MOTH.x_CO_1*(Phi - 1 + sqrt((Phi-1).^2 + MOTH.x_CO_2));
MOTH.x_CO_1 = 164073.3690366675;
MOTH.x_CO_2 = 0.0021716321;


% [HC] depend de richesse regime AA Padm
% HCppm = max(MOTH.x_HC_min, MOTH.x_HC_1*N + MOTH.x_HC_2*(Phi-0.9).^2 + MOTH.x_HC_3*AA + MOTH.x_HC_4*Padm + MOTH.x_HC_5);
MOTH.x_HC_1 = -0.0865999622;
MOTH.x_HC_2 = 4976.4940366879;
MOTH.x_HC_3 = 5.8638083665;
MOTH.x_HC_4 = -0.1995215471;
MOTH.x_HC_5 = 480.3901987722;
MOTH.x_HC_min = 0.0; % saturation a zero

% [NO] depend de richesse Padm AA
% NOppm = max(MOTH.x_NO_min, MOTH.x_NO_1*Padm + MOTH.x_NO_2*(Phi-0.9).^2 + MOTH.x_NO_3*AA + MOTH.x_NO_4);
MOTH.x_NO_1 = 4.6655090119;
MOTH.x_NO_2 = -73686.7811520695;
MOTH.x_NO_3 = 105.6839557511;
MOTH.x_NO_4 = -2669.2997417599;
MOTH.x_NO_min = 300.0; % concentration minimale NO

% Average molecular weight of exhaust gas
MOTH.Mexh=30; % g/mol

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Emissions et temperatures simplifiees
% linearisation (et extrapolation) mesures ISAT / modele constant

MOTH.lambda = 1; %on s'interesse uniquement a la thermique du catalyseur

% Exhaust gas Temperature (N en rpm, Texh en K, PME en Bars)
% /!\ attention aux unites !
% Texh = MOTH.Texh_cst + MOTH.Texh_Pad*N*PME;
MOTH.Texh_cst = 6.275785039019664e+02; %temperature a l'origine en K
MOTH.Texh_Pad = 0.012949786280493; %terme proportionnel a la puissance adimensionnee (PME au lieu du couple)

% Emissions (N en rpm, PME en Bars, CO en %vol, HC et NOx en ppm)
% /!\ attention aux unites !
% [X] = MOTH.X_cst + MOTH.X_Pad*N*PME;
MOTH.CO_cst = 0.847821252790832; % %vol de CO a l'orgine
MOTH.CO_Pad = -9.511982354145730e-06; %terme proportionnel a la puissance adimensionnee (PME au lieu du couple)
MOTH.mean_CO = 0.687111111111111; %moyenne, fit d'ordre 0

MOTH.HC_cst = 4.087938006501966e+02; % ppm de HC a l'orgine
MOTH.HC_Pad = -0.006278079104018; %terme proportionnel a la puissance adimensionnee (PME au lieu du couple)
MOTH.mean_HC = 3.027222222222222e+02; %moyenne, fit d'ordre 0

MOTH.NOx_cst = 3.137703918324894e+03; % ppm de NOx a l'orgine
MOTH.NOx_Pad = 0.012509310382318; %terme proportionnel a la puissance adimensionnee (PME au lieu du couple)
MOTH.mean_NOx = 3.349055555555556e+03; %moyenne, fit d'ordre 0

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% construction d'une surface d'interpolation pour comparer le moteur :
% prog_dyn soc VS prog_dyn 3U

% N = MOTH.ral*30/pi:10:MOTH.N_maxi;
% Padm = 0:50:1000;
% Phi = 1;
% AA = mean(mean(MOTH.AA0_N_Padm));
% 
% VD.MOTH = MOTH;
% VD.INIT = INIT;
% VD.BATT = BATT;
% VD.ACM1 = ACM1;
% VD.ADCPL = ADCPL;
% 
% VD.BATT.E = [];
% VD.BATT.R = [];
% VD.BATT.Q0 = [];
% param.Ures = [];
% 
% dcarb = zeros(length(Padm),length(N));
% Cmt = zeros(length(Padm),length(N));
% 
% for i_N = 1:length(N)
%     for i_P = 1:length(Padm)
%         [dcarb(i_P,i_N), Cmt(i_P,i_N)] = calc_me_mt_3U(Phi,AA,Padm(i_P),size(Phi),[],[],N(i_N)*pi/30,[],[],VD,param,[],1);
%     end
% end
% 
% MOTH.Cmt_max = max(Cmt);
% MOTH.Cmt_min = min(Cmt);
% MOTH.N_Cmt_max = N;
% 
% sz_P = size(Padm');
% N = repmat(N,sz_P);
% 
% MOTH.interp_dcarb = scatteredInterpolant(N(:), Cmt(:), dcarb(:));

%clearvars N Padm Phi AA dcarb Cmt VD param sz_P

display('Fin d''initialisation des parametres moteur thermique');
