% File: MTHERM_DW10_ATED
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier moteur thermique
%                      Pour librairie VEHLIB
%                             INRETS - LTE 
%  Objet: Ce script initialise la structure MOTH
%          contenant les champs necessaires pour renseigner 
%          le composant MOTEUR THERMIQUE de la librairie VEHLIB.
% =============================================================================

% Reperage du type de moteur thermique: 10 = moteur thermique cartographie
MOTH.ntypmoth=10;

% Nom du moteur
MOTH.nom='MTHERM_DW10_ATED';

% Masse du moteur thermique
MOTH.Masse_mth    = 189;

% Inertie du moteur thermique
MOTH.J_mt        = 0.15;

% Regimes de fonctionnement
MOTH.ral_max    = 900*pi/30;			% regime maxi pour un ralenti (en rad/s)
MOTH.ral_min    = 800*pi/30;			% regime mini pour un ralenti (en rad/s)
MOTH.ral        = 850*pi/30;			% regime ralenti (en rad/s)
MOTH.ral_dem	= 900*pi/30;			% regime de fin d'action du demarreur (regulation)
MOTH.wmt_maxi=3800*pi/30;				% regime maxi regulation (en rad/s)

% Regulation ralenti
MOTH.P_ral=0.1;
MOTH.I_ral=0;
MOTH.D_ral=0;

% Carburant Diesel
MOTH.dens_carb  = 830;					% densite du carburant a 20 C (g/l)
MOTH.CO2carb    = 2.0;					% rapport masses CO2 carburant pour une combustion parfaite 
%(mCO2/Mcab=44/(12+MOTH.CO2carb)) diesel=2.0; essence=1.85

% Reponse couple alpha du moteur:
MOTH.alpha_cons=[0 1];
MOTH.alpha2cpl=[0 1];

%gradient de couple maximum
MOTH.grd_cmt_mont=140;
MOTH.grd_cmt_desc=-7000;

% courbe enveloppe (estimation pleine charge et coupure d injection en(Nm) en fct regime MT(rad/s)
MOTH.wmt_max = [0   500  1000  1250  1500  1750  2000  2250  2500  2750 3000  3250  3500  3750  4000  4250  4500 4750 4900 5000]*pi/30;
MOTH.cmt_max = [0     0   140   177   254   256   259   259   247   251  238   232   221   206   185   171   153  100    0    0];
MOTH.cmt_min = [-0   -10    -10   -10.  -10 -14.7 -15.6 -16.8 -18.8 -22.7 -26.4 -30.2 -34. -37.6 -41.6 -51.2  -66. -66. -66. -66.];
MOTH.pmt_max = MOTH.cmt_max.*MOTH.wmt_max;   % pour le calculateur

% courbe optimum utilisee par le calculateur
MOTH.wmt_opti = MOTH.wmt_max(2:13);
MOTH.cmt_opti = MOTH.cmt_max(2:13)*0.8;
MOTH.pmt_opti = MOTH.cmt_opti.*MOTH.wmt_opti;

% Estimations de la consommation (g/s) du moteur thermique HDI_DW10
% Valeurs de regimes en rd/s
MOTH.Reg_2dconso=[     0.0   850.0  1000.0  1250.0  1500.0  1750.0  2000.0  2250.0  2500.0  2750.0  3000.0  3250.0  3500.0  3750.0  4000.0  4250.0  4500.0  ]*pi/30;
% Valeurs de couples en Nm
MOTH.Cpl_2dconso=[     0.0    10.0    20.0    30.0    40.0    50.0    60.0    70.0    80.0    90.0   100.0   110.0   120.0   130.0   140.0   150.0   160.0   170.0   180.0   190.0   200.0   210.0   220.0   230.0   240.0   250.0  ];
% Valeurs de debit de carburant en g/s
MOTH.Conso_2d=[
%Cpl0.0    10.0    20.0    30.0    40.0    50.0    60.0    70.0    80.0    90.0   100.0   110.0   120.0   130.0   140.0   150.0   160.0   170.0   180.0   190.0   200.0   210.0   220.0   230.0   240.0   250.0
  0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00
  0.130   0.150   0.180   0.230   0.300   0.350   0.400   0.450   0.500   0.550   0.620   0.680   0.760   0.800   0.900   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000   1.000       
  0.100   0.164   0.214   0.265   0.320   0.377   0.435   0.492   0.549   0.619   0.685   0.758   0.866   0.930   1.000   1.100   1.100   1.100   1.100   1.100   1.100   1.100   1.100   1.100   1.100   1.100  
  0.135   0.191   0.255   0.325   0.397   0.462   0.530   0.599   0.673   0.752   0.831   0.904   0.979   1.070   1.170   1.273   1.394   1.539   1.706   1.780   1.999   1.999   1.999   1.999   1.999   1.999  
  0.180   0.242   0.316   0.390   0.471   0.552   0.633   0.717   0.803   0.891   0.984   1.076   1.164   1.245   1.317   1.404   1.501   1.594   1.698   1.819   1.929   2.028   2.142   2.240   2.354   2.491  
  0.197   0.284   0.375   0.459   0.554   0.639   0.737   0.844   0.942   1.059   1.147   1.241   1.336   1.457   1.543   1.592   1.717   1.821   1.931   2.014   2.135   2.235   2.354   2.476   2.605   2.761  
  0.276   0.344   0.441   0.550   0.660   0.766   0.871   0.989   1.107   1.224   1.345   1.467   1.588   1.672   1.766   1.862   1.961   2.083   2.202   2.319   2.434   2.567   2.702   2.830   3.008   3.182  
  0.355   0.401   0.513   0.624   0.745   0.867   0.981   1.118   1.234   1.367   1.501   1.649   1.757   1.878   2.010   2.105   2.215   2.375   2.511   2.638   2.778   2.901   3.044   3.224   3.432   3.687  
  0.435   0.490   0.614   0.736   0.864   0.998   1.134   1.268   1.400   1.531   1.683   1.807   1.952   2.104   2.252   2.374   2.519   2.681   2.841   2.949   3.100   3.267   3.445   3.643   3.863   4.099  
  0.514   0.632   0.779   0.906   1.003   1.134   1.272   1.421   1.587   1.712   1.895   2.038   2.184   2.348   2.483   2.662   2.793   2.979   3.157   3.298   3.478   3.644   3.845   4.046   4.284   4.516  
  0.613   0.774   0.878   1.011   1.144   1.288   1.444   1.600   1.770   1.939   2.107   2.274   2.433   2.616   2.790   2.950   3.138   3.326   3.516   3.681   3.845   4.030   4.245   4.517   4.798   4.999  
  0.739   0.838   0.985   1.133   1.297   1.471   1.637   1.821   2.010   2.205   2.398   2.591   2.786   2.972   3.159   3.358   3.519   3.699   3.883   4.079   4.280   4.502   4.746   5.058   5.499   5.499  
  0.879   0.952   1.122   1.290   1.467   1.659   1.859   2.046   2.275   2.489   2.710   2.926   3.123   3.370   3.549   3.744   3.909   4.091   4.287   4.486   4.732   5.023   5.342   5.499   5.499   5.499  
  0.930   1.092   1.291   1.483   1.669   1.863   2.077   2.320   2.565   2.802   3.036   3.269   3.502   3.695   3.888   4.105   4.296   4.524   4.755   5.032   5.311   5.499   5.499   5.499   5.499   5.499  
  1.030   1.235   1.450   1.663   1.882   2.133   2.383   2.627   2.896   3.167   3.437   3.683   3.866   4.054   4.291   4.477   4.708   4.985   5.292   5.499   5.499   5.499   5.499   5.499   5.499   5.499  
  1.280   1.513   1.740   1.978   2.209   2.462   2.716   3.015   3.280   3.537   3.760   3.970   4.170   4.362   4.603   4.856   5.193   5.554   5.999   5.999   5.999   5.999   5.999   5.999   5.999   5.999  
  1.590   1.866   2.110   2.368   2.600   2.890   3.162   3.438   3.677   3.866   4.093   4.289   4.515   4.770   5.056   5.392   5.999   5.999   5.999   5.999   5.999   5.999   5.999   5.999   5.999   5.999  
 ];
 
 % Estimations des emissions de CO2 (g/s) du moteur thermique non disponible
 MOTH.Reg_2dCO2=[ 1000.0  4500.0 ]*pi/30;
MOTH.Cpl_2dCO2=[  15.0   120.0 ];
MOTH.CO2_2d=[
   0 0
   0 0
];

% Estimations des emissions de CO (g/s) du moteur thermique 
MOTH.Reg_2dCO=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dCO=[  15.0 120.0 ];
MOTH.CO_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de HC (g/s) du moteur thermique 
MOTH.Reg_2dHC=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dHC=[  15.0  120.0 ];
MOTH.HC_2d=[
   0 0
   0 0
]./1000;

% Estimations des emissions de NOx (g/s) du moteur thermique 
MOTH.Reg_2dNOx=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dNOx=[   15.0  120.0 ];
MOTH.NOx_2d=[
   0 0
   0 0 
]./1000;

% Estimations des emissions de Particules (g/s) du moteur thermique 
MOTH.Reg_2dPart=[ 1000.0 4500.0 ]*pi/30;
MOTH.Cpl_2dPart=[   15.0  120.0 ];
MOTH.Part_2d=[
   0 0
   0 0
]./1000;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Thermique moteur

% Pour inhiber le calcul de thermique moteur
MOTH.modele_therm=0; 
% Temp initiale moteur
MOTH.Tinit=25;
% Part du carburant transforme en chaleur
MOTH.pci_to_chaleur = 0.617;
% Capcite calorifique equivallente du bloc (J/K)
MOTH.thetaC =41860;
% coefficient de convection
MOTH.k_ext_vit0 =12.53;
MOTH.k_ext_vit =4.85;
% Coefficient de convection radiateur  d eau
MOTH.k_ext_vit0_calor =6.45;
MOTH.k_ext_vit_calor =2.0;
% Temp d ouverture du calorstat
MOTH.Touv_calorstat =85;
% Regulation calorstat ouvert (!)
MOTH.Tcons=80;
MOTH.Kp_calor=1;
MOTH.pci=43000;

display('Fin d''initialisation des parametres moteur thermique'); 

%------------------------------------MISES A JOUR-----------------------------------%
% 18-12-06 (EV/RT) : rajout PID ralenti en parametres

