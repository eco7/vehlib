% File: NiMh_panas_6Ah_NREL
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier monobloc batterie
%                      Pour librairie VEHLIB
%                            INRETS - LTE
%  Objet: Ce script initialise la structure BATT
%          contenant les champs necessaires pour renseigner 
%          le composant batterie de la librairie VEHLIB.
%
% Creation le 30-04-03 par MDR.
% d'apres donnees NREL - ADVISOR.
% =============================================================================

BATT.ntypbat=1;

BATT.Masse_bloc 		= 1.75;	% Masse d un monobloc

BATT.Nele_bloc     =6;   		% nb element par monobloc
BATT.Cahbat_nom =   6.5;    % capacite nominale en Ah

% temperature pour renseigner les resistances internes et tension a vide.
BATT.tbat_ocv = [0 20];
                
% profondeur de d?charge en % 
BATT.dod_ocv = [-10.0 0.00 10.00 20.00 30.00 40.00 50.00 60.00 70.00 80.00 90.00 100.00];

% tension a vide en V
BATT.ocv =...
   [8.3645  8.3645  7.9143  7.8078  7.7666  7.7294  7.6909  7.6459  7.5873  7.5106  7.4047  7.2370
    8.3645  8.3645  7.9143  7.8078  7.7666  7.7294  7.6909  7.6459  7.5873  7.5106  7.4047  7.2370];

BATT.tbat = [0 20];

% profondeur de d?charge en % 
BATT.dod = [-10.0 0.00 10.00 20.00 30.00 40.00 50.00 60.00 70.00 80.00 90.00 100.00];

% BATT.Rd = r?sistance interne en decharge en Ohm
BATT.Rd=...
   [0.0312  0.0312  0.0298  0.0283  0.0273  0.0269  0.0268  0.0275  0.0280  0.0300  0.0338  0.0377
    0.0312  0.0312  0.0298  0.0283  0.0273  0.0269  0.0268  0.0275  0.0280  0.0300  0.0338  0.0377];

% BATT.Rc = r?sistance interne en charge en Ohm
BATT.Rc=...
   [0.0204  0.0204  0.0204  0.0203  0.0197  0.0198  0.0196  0.0198  0.0198  0.0205  0.0220  0.0235
    0.0204  0.0204  0.0204  0.0203  0.0197  0.0198  0.0196  0.0198  0.0198  0.0205  0.0220  0.0235];
                 
BATT.R1=zeros(size(BATT.Rc));
BATT.C1=zeros(size(BATT.Rc));
BATT.R2=zeros(size(BATT.Rc));
BATT.C2=zeros(size(BATT.Rc));

% Gestion des courants de batterie
BATT.Ibat_max=[13*6.5 13*6.5];	% Imax (decharge) = 13C
BATT.DoD_Ibat_max=[0 100];
BATT.Ibat_min=-0.7*BATT.Ibat_max;  % Imin (charge) = 0.7*Imax
BATT.DoD_Ibat_min=[0 100];

% Tension de fin de decharge
BATT.Ubat_min=0.9*6/2;
% Tension de surcharge pour arret du calcul;
BATT.Ubat_max=1.6*6*2;
% Temperature maxi pour arret du calcul;
BATT.Tbat_max=60;


% Rendement faradique de la batterie
BATT.RdFarad=0.905;

% Gestion du courant de recuperation admissible par la batterie
% parametre pour la regul de la tension maxi
BATT.tmax_reset=0.5;
BATT.P_lim=160;
BATT.I_lim=0;
BATT.D_lim=0;

display('Fin d''initialisation des parametres batterie');

%----------------------------------MISES A JOUR------------------------------------%
% 12/11/2003. MDR. Suppression de l'effet Peukert

