% File: NiMh_panas_6Ah
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier monobloc batterie
%                      Pour librairie VEHLIB
%                            INRETS - LTE
%  Objet: Ce script initialise la structure BATT
%          contenant les champs necessaires pour renseigner 
%          le composant batterie de la librairie VEHLIB.
%
% Creation le 12-03-03 par MDR.
% =============================================================================

BATT.ntypbat=1;

BATT.Masse_bloc 		= 1.75;	% Masse d un monobloc

BATT.Nele_bloc     =6;   		% nb element par monobloc
BATT.Cahbat_nom =   6.5;    % capacite nominale en Ah

% temperature pour renseigner les resistances internes et tension a vide.
BATT.tbat_ocv = [0 20];
                
% profondeur de d?charge en % (=capacit? restante / capacit? nominale en Ah)
BATT.dod_ocv = [-10.0	0.00 3.00 6.00 10.00 20.00 30.00 40.00 50.00 60.00 70.00 80.00 90.00 93.00 96.00 100.00];

% BATT.ocv = tension a vide en V
BATT.ocv = [1.3990 1.3985 1.3833 1.3627 1.3434 1.3193 1.3118 1.3056 1.2967 1.2855 1.2713 1.2529 1.2292 1.2155 1.1928 1.0792
                       1.3985 1.3985 1.3833 1.3627 1.3434 1.3193 1.3118 1.3056 1.2967 1.2855 1.2713 1.2529 1.2292 1.2155 1.1928 1.0792
              ]*BATT.Nele_bloc;    % tension a vide en V

BATT.tbat = [0 20];

% profondeur de d?charge en % (=capacit? restante / capacit? nominale en Ah)
BATT.dod = [-10.0	0.00 3.00 6.00 10.00 20.00 30.00 40.00 50.00 60.00 70.00 80.00 90.00 93.00 96.00 100.00];

% r?sistance interne en decharge en Ohm
BATT.Rd= [1.3 1.3 5.7 5.9 5.8 5.6 5.7 5.8 5.9 5.9 6.0 5.9 6.0 6.0 5.7 2.4
                     1.3 1.3 5.7 5.9 5.8 5.6 5.7 5.8 5.9 5.9 6.0 5.9 6.0 6.0 5.7 2.4
                 ]*BATT.Nele_bloc/1000;

% r?sistance interne en charge en Ohm
BATT.Rc= [20.8 20.8 24.6 29.2 29.4 25.0 21.7 20.9 20.9 21.8 22.5 24.5 26.1 27.0 28.0 43.8
                   20.8 20.8 24.6 29.2 29.4 25.0 21.7 20.9 20.9 21.8 22.5 24.5 26.1 27.0 28.0 43.8
               ]*BATT.Nele_bloc/1000;

BATT.R1=zeros(size(BATT.Rc));
BATT.C1=zeros(size(BATT.Rc));
BATT.R2=zeros(size(BATT.Rc));
BATT.C2=zeros(size(BATT.Rc));

% Gestion des courants de batterie
BATT.Ibat_max=[ 13*6.5 ;13*6.5]	;		% Imax (decharge) = 13C
BATT.DoD_Ibat_max=[0 100];
BATT.Ibat_min=-0.7*BATT.Ibat_max;  % Imin (charge) = 0.7*Imax
BATT.DoD_Ibat_min=BATT.DoD_Ibat_max;

% Tension de fin de decharge
BATT.Ubat_min=0.75*BATT.Nele_bloc;
% Tension de surcharge pour arret du calcul;
BATT.Ubat_max=1.6*BATT.Nele_bloc*2; % facteur 2 arbitraire !
% Temperature maxi pour arret du calcul;
BATT.Tbat_max=60;


% Rendement faradique de la batterie
BATT.RdFarad=1;

% Gestion du courant de recuperation admissible par la abtterie
% parametre pour la regul de la tension maxi
BATT.tmax_reset=0.5;
BATT.P_lim=50;
BATT.I_lim=10;
BATT.D_lim=0;

display('Fin d''initialisation des parametres element batterie');

%----------------------------------MISES A JOUR------------------------------------%
