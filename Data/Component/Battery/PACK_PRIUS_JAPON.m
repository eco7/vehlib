% File: PACK_PRIUS_JAPON
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier Pack batterie
%                      Pour librairie VEHLIB
%                            INRETS - LTE
%  Objet: Ce script initialise la structure BATT
%          contenant les champs necessaires pour renseigner 
%          le composant batterie de la librairie VEHLIB.
%
%          Pack du vehicule prius japon
% =============================================================================

BATT.Nblocser     =   40;    	% nombre de monobloc en serie
BATT.Nbranchepar     =   1;		% nombre de branches en parallele

BATT.Nom_bloc='NiMh_panas_6Ah';

% actif (1) / inactif (0)
BATT.Therm_on=1;
% Capacite calorifique en W/(kgK)
BATT.Cp=700;
% Coefficient d'echange en W/K
BATT.hc=1;


% initialisation des caracteristiques de l element 
eval(BATT.Nom_bloc);

display('Fin d''initialisation des parametres pack batterie');

%----------------------------------MISES A JOUR------------------------------------%
% V2.0 dissociation de l element et du pack batterie
