function BATT = defaultBATTtype3(dod,tbat,ocv,Rc,Rd,Q3,a)
% function BATT = defaultBATTtype3
% Set de default values of a Battery type 3 (R+CPE)

if ~exist('dod','var')
    dod = [0 100];
end
if ~exist('tbat','var')
    tbat = [0 60];
end
if ~exist('ocv','var')
    ocv = [4 2];
end
if ~exist('Rc','var')
    Rc = [0.002 0.002];
end
if ~exist('Rd','var')
    Rd = [0.002 0.002];
end
if ~exist('Q3','var')
    Q3 = [10000 10000];
end
if ~exist('a','var')
    a = [0.5 0.5];
end

%initialisation de la structure
BATT = struct;
BATT.commentaire = 'modele de BATTERIE type 3';

%type de batterie (VEHLIB)
BATT.ntypbat = 3;
%masse de l'element (DATASHEET) en kg
BATT.Masse_bloc = 1;
%nombre d'elements (1)
BATT.Nele_bloc = 1;
%capacite mesuree de la Batterie en Ah
BATT.Cahbat_nom = 100;

%freqs for CPE:
%     'f1'
BATT.f1 = 10^-4.141;
%     'ff'
BATT.ff = 1.389;

BATT.f1 =     1/180000  ;    % Frequence min
BATT.ff =     0.1  ;    % Frequence max

%OCV
%     'dod_ocv'
BATT.dod_ocv = dod;
%     'tbat_ocv'
BATT.tbat_ocv = tbat;
%     'ocv'
BATT.ocv = repmat(ocv,length(BATT.tbat_ocv),1);
%     'ocv_low'
BATT.ocv_low = BATT.ocv;
%     'ocv_high'
BATT.ocv_high = BATT.ocv;
%     'pente_hyst'
BATT.pente_hyst = zeros(length(BATT.tbat_ocv),length(BATT.dod_ocv));
%     'Qh_ini'
BATT.Qh_ini = 0;

%Impedance
%     'dod' vector for impedance interpolation
BATT.dod = BATT.dod_ocv;
%temperature de la batterie
BATT.tbat = BATT.tbat_ocv;
%     'Rc' in ohms
BATT.Rc = repmat(Rc,length(BATT.tbat),1);
%     'Rd'
BATT.Rd = repmat(Rd,length(BATT.tbat),1);
%'tau2'(SIMCAL = 0)
BATT.tau2 = zeros(size(BATT.Rc));
%     'Q3'
BATT.Q3 = repmat(Q3,length(BATT.tbat),1);
%     'pente'
BATT.pente = repmat(a,length(BATT.tbat),1);
%     'R2a'(SIMCAL = 1)
BATT.R2a = ones(size(BATT.Rc));
%     'R2b'(SIMCAL = 0)
BATT.R2b = zeros(size(BATT.Rc));
%     'R2c'(SIMCAL = 0)
BATT.R2c = zeros(size(BATT.Rc));
%     'R2d'(SIMCAL = 1)
BATT.R2d = ones(size(BATT.Rc));
%     'R2e'(SIMCAL = 0)
BATT.R2e = zeros(size(BATT.Rc));
%     'R2f'(SIMCAL = 0)
BATT.R2f = zeros(size(BATT.Rc));


% Gestion des courants de batterie
BATT.DoD_Ibat_max=[0 100];
BATT.DoD_Ibat_min=[0 100];
BATT.Ibat_max= 10*BATT.Cahbat_nom*[1 1] ;% Imax (discharge, ex.: 10C) (cf. DATASHEET)
BATT.Ibat_min= -3*BATT.Cahbat_nom*[1 1] ;% Imin (charge, ex.: -3C) (cf. DATASHEET)

%Limits:
BATT.Ubat_min = 1.5;
BATT.Ubat_max = 4.5;
BATT.Tbat_max = 60;

%Rendements faradiques
% dod_rdfarad de la batterie
BATT.dod_rdfarad = [-100.00 200.00]; 
BATT.tbat_rdfarad = BATT.tbat;
% rdfarad de la batterie 
BATT.rdfarad = ones(length(BATT.tbat_rdfarad),length(BATT.dod_rdfarad)); 
% Rendement faradique du BMS
BATT.dod_RdFarad = [-100.00 200.00]; 
BATT.RdFarad =  ones(size(BATT.dod_RdFarad));

% BMS
BATT.tmax_reset=0.5;
BATT.ubat_lim_cons=  4.0 ;%DATASHEET
BATT.P_lim=4;
BATT.I_lim=0;
BATT.D_lim=0;

% Thermique
BATT.Tbat0=25;
BATT.Therm_on=2;
BATT.Cp=700;
BATT.hc=1;
BATT.Nblocser=1;
BATT.Nbranchepar=1;

%gestion du soc max/min
BATT.SoCmax = 100;
BATT.SoCmin = 0;
end
