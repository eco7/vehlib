% File: Pack_AXELEC
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier Pack batterie
%                      Pour librairie VEHLIB
%                            INRETS - LTE
%  Objet: Ce script initialise la structure BATT
%          contenant les champs necessaires pour renseigner 
%          le composant batterie de la librairie VEHLIB.
%
%          Pack du vehicule ax electrique
% =============================================================================

BATT.Nblocser     =   20;    % nombre de monobloc en serie
BATT.Nbranchepar     =   1;    % nombre de branches en parallele

BATT.Nom_bloc='NiCd_STM5_100_MRE';

% Thermique pack
% actif (1) / inactif (0)
BATT.Therm_on=1;
% Capacite calorifique en W/(kgK)
BATT.Cp=700;
% Coefficient d'echange en W/K
BATT.hc=1;


% initialisation des caracteristiques de l element 
eval(BATT.Nom_bloc);

display('Fin d''initialisation des parametres pack batterie');

%----------------------------------MISES A JOUR------------------------------------%
%          mise a jour avril 2003
