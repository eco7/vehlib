% Pack_kokam_12Ah_30kW
% =============================================================================
% 
%                Initialisation du fichier Pack batterie
%                      Pour librairie VEHLIB
%                            IFSTTAR - LTE
%  Objet:  Ce script initialise la structure BATT
%          contenant les champs necessaires pour renseigner 
%          le composant batterie de la librairie VEHLIB.
%
% =============================================================================

BATT.Nom_bloc='Lipol_kokam_12Ah_2008';
% initialisation des caracteristiques de l element 
eval(BATT.Nom_bloc);
BATT.Nom_bloc='Lipol_kokam_12Ah_2008';

BATT.Nblocser     =   35;    % nombre de monobloc en serie
BATT.Nbranchepar   =   1;    % nombre de branches en parallele


% Thermique pack
% NON CONNU : pris sur le pack de la C0
% actif (1) / inactif (0)
BATT.Therm_on=1;
% Capacite calorifique en W/(kgK)
BATT.Cp=700;
% Coefficient d'echange en W/K
BATT.hc=1;



display('Fin d''initialisation des parametres pack batterie');

%----------------------------------MISES A JOUR------------------------------------%

