% File: chargeur_type
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier Pack batterie
%                      Pour librairie VEHLIB
%                            INRETS - LTE
%  Objet: Ce script initialise la structure CHRG
%          contenant les champs necessaires pour renseigner 
%          le composant chargeur de la librairie VEHLIB.
%
%
% =============================================================================

% Parametres chargeur
% Phase 1: charge a courant constant
CHRG.Cahbat_ch=[0 60 120]*BATT.Cahbat_nom/100;        % Charge batterie par monobloc en Ah pour tabul courant
CHRG.Ichar=-[1 1 1]*BATT.Cahbat_nom;                  % Courant de recharge en nombre de Cnom en fonction des Ah

% Phase 2: Charge a tension constante
CHRG.ubat_ch_cons=6.5;                 % Tension monobloc seuil constante pour la phase 2 de recharge
CHRG.delta=0.08;                                                         % marge pour la detection de changement de phase
% Parametres du regulateur de charge (phase U constant)
CHRG.P_charg=10;                                     % parametre proportionnel
CHRG.I_charg=0;                                     % parametre integral
CHRG.D_charg=0;                                     % parametre derivee

% Phase 3: charge d egalisation
CHRG.ibat_egal_cons=-BATT.Cahbat_nom/5;               % Courant constant pour charge d'egalisation
CHRG.cond_charg_egal=BATT.Cahbat_nom/100;                                 % Condition de declenchement de la phase de charge d egalisation (Ah)

% Fin de charge
CHRG.type_cond_fin_charge=1; % 1 charge d egalisation
CHRG.Ifin_charge=0; % obsolete pour fin de charge de type 1
CHRG.cond_fin_recharge=0;     % Condition de fin de charge (Ah)



display('Fin d''initialisation des parametres chargeur');

%----------------------------------MISES A JOUR------------------------------------%
