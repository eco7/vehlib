% File: NiCd_STM5_100_MRE
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier monobloc batterie
%                      Pour librairie VEHLIB
%                            INRETS - LTE
%  Objet: Ce script initialise la structure BATT
%          contenant les champs necessaires pour renseigner 
%          le composant batterie de la librairie VEHLIB.
% =============================================================================

BATT.ntypbat=1;

BATT.Masse_bloc 		= 13.;	% Masse d un monobloc

BATT.Nele_bloc     =5;   		% nb element par monobloc
BATT.Cahbat_nom =   108.38;    % capacite nominale en Ah
BATT.U_nom_ele   =   1.2;    % Tension nominale en V du couple electrochimique pour un element.

% temperature pour renseigner les resistances internes et tension a vide.
BATT.tbat_ocv = [-20 0 20 40];
                
% profondeur de d?charge en % (=capacit? restante / capacit? nominale en Ah)
BATT.dod_ocv = [
 -20.0   -10.0     0.0    10.0    20.0    30.0    40.0    50.0    60.0    70.0    80.0    90.0   100.0  ];

% tension a vide en decharge en V
BATT.ocv= [
1.5200  1.4400  1.3880  1.3040  1.2600  1.2400  1.2300  1.2200  1.2100  1.1920  1.1640  1.0900  0.8000  
1.5200  1.4400  1.3880  1.3080  1.2660  1.2460  1.2380  1.2280  1.2180  1.2040  1.1800  1.1240  0.8100  
1.5200  1.4400  1.3920  1.3180  1.2760  1.2580  1.2480  1.2400  1.2280  1.2120  1.1940  1.1580  0.8300  
1.5200  1.4400  1.3820  1.3140  1.2760  1.2600  1.2500  1.2400  1.2280  1.2140  1.1960  1.1680  0.8400  ]*BATT.Nele_bloc;

BATT.tbat = [-20 0 20 40];

% profondeur de d?charge en % (=capacit? restante / capacit? nominale en Ah)
BATT.dod = [
 -20.0   -10.0     0.0    10.0    20.0    30.0    40.0    50.0    60.0    70.0    80.0    90.0   100.0  ];

% BATT.Rd = r?sistance interne en decharge en Ohm
BATT.Rd= [
0.5400  0.5400  0.5400  0.5400  0.5200  0.5400  0.5400  0.5400  0.5800  0.6000  0.6800  1.0200  1.2000  
0.5200  0.5200  0.5200  0.5200  0.5200  0.5400  0.5400  0.5400  0.5600  0.6000  0.6600  0.9000  0.9000  
0.5200  0.5200  0.5200  0.5200  0.5200  0.5400  0.5200  0.5400  0.5600  0.5600  0.6000  0.7200  0.8000  
0.5200  0.5200  0.5200  0.5000  0.5000  0.5200  0.5200  0.5200  0.5400  0.5600  0.5800  0.6400  0.8000  ]*1/1000*BATT.Nele_bloc;

% BATT.Rc = r?sistance interne en charge en Ohm
BATT.Rc= [
0.5000  0.5000  0.5000  0.6000  0.6000  0.6200  0.6200  0.6200  0.6400  0.6800  0.7400  1.0200  1.2000  
0.5000  0.5000  0.5000  0.6000  0.5800  0.5800  0.6000  0.6000  0.6200  0.6200  0.6800  0.8600  1.0000  
0.5600  0.5600  0.5600  0.5800  0.5600  0.5600  0.5600  0.5600  0.5800  0.5800  0.6200  0.7200  0.8000  
0.5800  0.5800  0.5800  0.5600  0.5400  0.5600  0.5400  0.5400  0.5400  0.5400  0.5600  0.6200  0.8000  ]*1/1000*BATT.Nele_bloc;

BATT.R1=zeros(size(BATT.Rc));
BATT.C1=zeros(size(BATT.Rc));
BATT.R2=zeros(size(BATT.Rc));
BATT.C2=zeros(size(BATT.Rc));

% Gestion des courants de batterie
BATT.Ibat_max= [250 250];			% Imax (decharge)
BATT.DoD_Ibat_max=[0 100];
BATT.Ibat_min=[-200 -200];  % Imin (charge)
BATT.DoD_Ibat_min=BATT.DoD_Ibat_max;

% Tension de fin de decharge pour arret du calcul
BATT.Ubat_min=0.8*BATT.Nele_bloc;
% Tension de surcharge pour arret du calcul;
BATT.Ubat_max=1.6*BATT.Nele_bloc*2; % facteur 2 arbitraire !
% Temperature maxi pour arret du calcul;
BATT.Tbat_max=60;


% Rendement faradique de la batterie
BATT.RdFarad=0.98;

% Gestion du courant de recuperation admissible par la batterie
% parametre pour la regul de la tension maxi
BATT.tmax_reset=0.5;
BATT.P_lim=80;
BATT.I_lim=0;
BATT.D_lim=0;

display('Fin d''initialisation des parametres element batterie');

%----------------------------------MISES A JOUR------------------------------------%
% Mise a jour avril 2003

