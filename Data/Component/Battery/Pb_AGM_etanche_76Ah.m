% ============================================================================
% .........Element batterie
% courbe de tension a vide et resistance interne
% D'apres donnees CEA dans le projet velecta
% modèle E(soc)-R(soc)
% On conservera les résistances à 30s du rapport.
% l'évolution de cette résistance étant (chaotique) on garde la moyenne des
% valeurs.
% Creation 12_07_2010 (EV)
% ============================================================================

BATT.ntypbat=1;

BATT.Masse_bloc 	= 28;	% Masse d un monobloc

BATT.Nele_bloc     =6;   		% nb element par monobloc
%BATT.Cahbat_nom =   76;    % capacite nominale en Ah
BATT.Cahbat_nom =   76*0.54;  % Coefficient issue mesure CEA sur energie reelle fournie
                               % lors d'essai d'autonomie (5kWh).

% BATT.tbat = temperature pour renseigner les resistances internes et tension a vide.
BATT.tbat_ocv = [0 20];
                
% BATT.dod_ocv : profondeur de d�charge en % (=capacit� restante / capacit� nominale en Ah)
BATT.dod_ocv = 0:10:100;

% BATT.ocv = tension a vide en V
BATT.ocv =...
   [13.52 13.13 12.96 12.79 12.60 12.43 12.24 12.06 11.87 11.63 11.48
    13.52 13.13 12.96 12.79 12.60 12.43 12.24 12.06 11.87 11.63 11.48];

BATT.tbat = [0 20];

% BATT.dod : profondeur de d�charge en % (=capacit� restante / capacit� nominale en Ah)
BATT.dod = [0. 100.];

% BATT.Rd = r�sistance interne en decharge en Ohm
BATT.Rd=...
   [0.0134 0.0134
    0.0134 0.0134];

% BATT.Rc = r�sistance interne en charge en Ohm
BATT.Rc=...
   [0.0134 0.0134
    0.0134 0.0134];

BATT.R1=zeros(size(BATT.Rc));
BATT.C1=zeros(size(BATT.Rc));
BATT.R2=zeros(size(BATT.Rc));
BATT.C2=zeros(size(BATT.Rc));

% Gestion des courants de batterie
BATT.DoD_Ibat_max=[0 100];
BATT.DoD_Ibat_min=[0 100];
BATT.Ibat_max=7.5*40*[1 1];			% Imax (decharge) = 7.5C voir doc Exide
BATT.Ibat_min=-0.7*BATT.Ibat_max/2.*[1 1];  % Imin (charge) = 0.7*Imax

% Tension mini de decharge
BATT.Ubat_min=9;
% Tension maxi de recharge;
BATT.Ubat_max=16;
% Temperature maxi pour arret du calcul;
BATT.Tbat_max=60;

% Rendement faradique de la batterie
BATT.RdFarad=0.95;

% Gestion du courant de recuperation admissible par la abtterie
% parametre pour la regul de la tension maxi
BATT.tmax_reset=0.5;
BATT.P_lim=100;
BATT.I_lim=10;
BATT.D_lim=0;

display('Fin d''initialisation des parametres batterie');

%----------------------------------MISES A JOUR------------------------------------%
% division par deux de la recup (cf banc)
% 20/09/05: rendement faradique 0.95 remplace 0.9

