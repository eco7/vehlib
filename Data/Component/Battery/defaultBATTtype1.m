function BATT = defaultBATTtype1
% function BATT = defaultBATTtype1
% Set de default values of a Battery type 1 (R+RC)

%initialisation de la structure
BATT = struct;
BATT.commentaire = 'modele de BATTERIE type 1';

% Carac general:
%ntypbat: numeric, 1x1
BATT.ntypbat = 1;%n.u.
%Masse_bloc: numeric, 1x1
BATT.Masse_bloc = 1;%kg
%Nele_bloc: numeric, 1x1
BATT.Nele_bloc = 1;%n.u.
%Cahbat_nom: numeric, 1x1
BATT.Cahbat_nom = 100;%Ah


%OCV: 
%tbat_ocv: numeric, 1x2
BATT.tbat_ocv = [10 40];%degC
%dod_ocv: numeric, 1x19
BATT.dod_ocv = (1:100);% % of Cahbat_nom
%ocv: numeric, 2x19
BATT.ocv = 4-0.02*BATT.dod_ocv; %tension linéaire entre 2 et 4V
BATT.ocv = repmat(BATT.ocv,2,1);

%Impedance
% Z = Rc (Rd) + R1//C1 + R2//C2
% tbat: numeric, 1x2
BATT.tbat = [10 40];%degC
% dod: numeric, 1x18
BATT.dod = (1:100);% % of Cahbat_nom
% Rc: numeric, 2x18
BATT.Rc = 0.002*ones(length(BATT.tbat),length(BATT.dod));% Ohms
% Rd: numeric, 2x18
BATT.Rd = 0.002*ones(length(BATT.tbat),length(BATT.dod));% Ohms
% R1: numeric, 2x18
BATT.R1 = 0.0010*ones(length(BATT.tbat),length(BATT.dod));% Ohms
% C1: numeric, 2x18
BATT.C1 = 1000*ones(length(BATT.tbat),length(BATT.dod));% F
% R2: numeric, 2x18
BATT.R2 = 0.0010*ones(length(BATT.tbat),length(BATT.dod));% Ohms
% C2: numeric, 2x18
BATT.C2 = 10000*ones(length(BATT.tbat),length(BATT.dod));% F


% Gestion des courants de batterie
BATT.DoD_Ibat_max=[0 100];
BATT.DoD_Ibat_min=[0 100];
BATT.Ibat_max= 10*BATT.Cahbat_nom*[1 1] ;% Imax (discharge, ex.: 10C) (cf. DATASHEET)
BATT.Ibat_min= -3*BATT.Cahbat_nom*[1 1] ;% Imin (charge, ex.: -3C) (cf. DATASHEET)

%Limits:
BATT.Ubat_min = 1.5;
BATT.Ubat_max = 4.5;
BATT.Tbat_max = 60;

%Rendements faradiques
BATT.RdFarad = 1;

% BMS
% Gestion du courant de recuperation admissible par la batterie
% parametre pour la regul de la tension maxi
BATT.tmax_reset=0.5;
BATT.P_lim=100;
BATT.I_lim=10;
BATT.D_lim=0;

% Thermique
BATT.Tbat0=25;
BATT.Therm_on=2;
BATT.Cp=700;
BATT.hc=1;
BATT.Nblocser=1;
BATT.Nbranchepar=1;

%gestion du soc max/min
BATT.SoCmax = 100;
BATT.SoCmin = 0;
end
