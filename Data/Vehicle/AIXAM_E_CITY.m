%% =============================================================================
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VELEC';
vehlib.DRIV='DRIVER_TYPE1_Val4';
vehlib.ECU='ECU_AIXAM_E_CITY';
vehlib.BATT='Pack_AIXAM_E_CITY';
vehlib.ACM1='ABM_40d';
vehlib.RED='RED_AIXAM_E_CITY';
vehlib.VEHI='VEH_AIXAM_E_CITY';
vehlib.ACC='AUX_AIXAM_E_CITY';
vehlib.CYCL='simulation_conduite_e_city_grise';
