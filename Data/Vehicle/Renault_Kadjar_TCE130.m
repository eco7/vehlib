% File: Peugeot_308SW_1L6VTI16V
% file: P308_SW_1L6VTI16V
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% =============================================================================
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    IFSTTAR - LTE 04/01/11
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VTH_BV';
vehlib.DRIV='DRIVER_TYPE1_Val1';
vehlib.ECU='ECU_VTH';
vehlib.MOTH='MTHERM_TCE130_IFSTTAR';
vehlib.EMBR1='EMB1_TYPE';
vehlib.BV='BV6_Kadjar_TL4';
vehlib.RED='RED_Kadjar';
vehlib.VEHI='VEH_Kadjar';
vehlib.BELT1='BELT1_TYPE';
vehlib.ACC='AUX_Kadjar';
vehlib.CYCL='CIN_NEDC_BV6';
