% File: Peugeot_308SW_1L6VTI16V
% file: P308_SW_1L6VTI16V
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% =============================================================================
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    IFSTTAR - LTE 04/01/11
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='HP_BV_1EMB_Lagrange';
vehlib.DRIV='DRIVER_TYPE1_Val1';
vehlib.ECU='ECU_HP_BV_1EMB_Lagrange';
vehlib.MOTH='MTHERM_EP6';
vehlib.TWC='TWC_data';
vehlib.EMBR1='EMB1_TYPE';
vehlib.BV='BV5_308_20DP42';
vehlib.RED='RED_308';
vehlib.VEHI='VEH_308SW_1L6VTI16V';
vehlib.BELT1='BELT1_TYPE';
vehlib.ACC='AUX_CLIO_HP';
vehlib.CYCL='CIN_NEDC_BV';
vehlib.ACM1='MCT_MSA_N1_PRIUS_II_15kW';
vehlib.BATT='PACK_A123_20Ah_20kW';
vehlib.ADCPL='ADCPL_RENDCST';

