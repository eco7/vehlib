% File: BUS_CPLHYD
%% =============================================================================
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VTH_CPLHYD';
vehlib.DRIV='DRIVER_TYPE1_Val6';
vehlib.ECU='ECU_BUS_CPLHYD';
vehlib.MOTH='MTHERM_BUS';
vehlib.CONV='CONVERT';
vehlib.BV='BV4_BUS';
vehlib.RED='RED_BUS';
vehlib.VEHI='VEH_BUS';
vehlib.BELT1='BELT1_CLIO_1L5DCI';
vehlib.ACC='AUX_BUS';
vehlib.CYCL='SORT2';
