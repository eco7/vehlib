% File: Renault_Kadjar_TCE130_HP_BV_2EMB_CONTROL.m
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% =============================================================================
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    IFSTTAR - LTE 04/01/11
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='HP_BV_2EMB_CONTROL_NN';
vehlib.DRIV='DRIVER_TYPE1_Val2';
vehlib.ECU='ECU_HP_BV_2EMB_CONTROL_TCE130_NN';
vehlib.EMBR1='EMB1_GROS';
vehlib.BV='BV6_Kadjar_TL4';
vehlib.RED='RED_Kadjar';
vehlib.VEHI='VEH_Kadjar';
vehlib.BELT1='BELT1_TYPE';
vehlib.ACC='AUX_Kadjar_HP';
vehlib.CYCL='CIN_NEDC_BV6';
vehlib.ACM1='MCT_MSA_N1_PRIUS_II';
vehlib.BATT='Pack_kokam_12Ah_30kW';
vehlib.EMBR2='EMB2_TYPE';			% cote moteur thermique
vehlib.ADCPL='ADCPL_RENDCST';
vehlib.MOTH='MTHERM_CONTROL_TCE130';
vehlib.TWC='TWC_TCE130';

