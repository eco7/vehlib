% File: CLIO_1L5DCI_HP_BV_1EMB_UQM
%% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='HP_BV_1EMB';
vehlib.DRIV='DRIVER_TYPE1_Val1';
vehlib.ECU='ECU_HP_BV_1EMB';
vehlib.BATT='Pack_insight_NREL';	    	
vehlib.ACM1='MCT_UQM_8kW';
vehlib.ADCPL='ADDCPL_RENDCST_BAVL';
vehlib.MOTH='MTHERM_K9K_702';
vehlib.EMBR1='EMB1_TYPE';
vehlib.BV='BV5_CLIO_1L5DCI';
vehlib.RED='RED_CLIO_1L5DCI';
vehlib.VEHI='VEH_CLIO_1L5DCI';
vehlib.ACC='AUX_CLIO_HP';
vehlib.CYCL='CIN_NEDC_BV';
