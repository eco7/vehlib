% File: XANTIA
%% =============================================================================
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VTH_BV2';
vehlib.DRIV='DRIVER_TYPE1_Val1';
vehlib.ECU='ECU_VTH';
vehlib.MOTH='MTHERM_XU7JP4';
vehlib.EMBR1='EMB1_TYPE';
vehlib.BV='BV5_XANTIA';
vehlib.RED='RED_XANTIA';
vehlib.VEHI='VEH_XANTIA';
vehlib.BELT1='BELT1_TYPE';
vehlib.ACC='AUX_XANTIA';
vehlib.CYCL='CIN_NEDC_BV';
