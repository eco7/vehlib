% Renault Zoe 135ps electrical vehicle
% Datas come from : https://zeperfs.com/en/fiche8148-renault-zoe-r135.htm#Specs
% This data file is used to create a vehlib's compatible structure (VD) with a reduced number of parameters
nbacm1 =1;
Cme_max=245; % Nm
Pme_max=99*1000; % W
Wme_max = 11000*pi/30;

% Pneumatiques 195/55 R 16
largeur = 0.195;                                % m
hauteur = 0.55;                                 % en pourcentage de la largeur
d_jante = 16*25.4e-3;                           % diametre de jante en pouce => m

Sveh=70*61*25.4*25.4/10^6;
Cx=0.75/Sveh; % /
Veh_Mass=1652; % kg
Batt_nrj=52*1000; % kWh
Vmax= 140/3.6;
Nmax = 10900*pi/30;
kred=Nmax/(Vmax*2*pi/2); % 136 km/h pour 10900 rpm

Pacc_elec = 300;
