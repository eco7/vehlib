%% =============================================================================
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VELEC_BAT_SC_DCDCSC';
vehlib.DRIV='DRIVER_TYPE1_Val4';
vehlib.ECU='ECU_AXELEC_BAT_SC';
vehlib.BATT='Pack_AXELEC';
vehlib.ACM1='MCT_MCC_SA13';
vehlib.RED='RED_AXELEC';
vehlib.VEHI='VEH_AXELEC';
vehlib.ACC='AUX_AXELEC';
vehlib.CYCL='CIN_ECE15';
vehlib.SC='Pack_SC_MW_144F_48V';
vehlib.DCDC_2='DCDC_2_80kW';
vehlib.DCDC_1='DCDC_1_240kW';