%% =============================================================================
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='HP_BV_2EMB';
vehlib.DRIV='DRIVER_TYPE1_Val2';
vehlib.ECU='ECU_HP_BV_2EMB_NES_NHP';	% Calculateur adapte pour simuler les perfos max du vehicule
%vehlib.ACM1='MCT_NES_22kW';
vehlib.ACM1='MCT_MSA_N1_PRIUS_II';
vehlib.EMBR1='EMB1_TYPE';			% cote BV
vehlib.EMBR2='EMB2_TYPE';			% cote moteur thermique
vehlib.BV='BV5_CLIO_1L5DCI';
vehlib.ADCPL='ADDCPL_RENDCST_BAVL';
vehlib.MOTH='MTHERM_K9K_702';
vehlib.TWC='TWC_data';
%vehlib.BATT='Pack_NHP_42V';
vehlib.BATT='PACK_PRIUS_II';
vehlib.RED='RED_CLIO_1L5DCI';
vehlib.VEHI='VEH_CLIO_1L5DCI';
vehlib.ACC='AUX_CLIO_HP';
vehlib.CYCL='CIN_NEDC_BV';
