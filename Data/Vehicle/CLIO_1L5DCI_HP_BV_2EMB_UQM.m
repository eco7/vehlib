% File: CLIO_1L5DCI_HP_BV_2EMB_UQM
%% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='HP_BV_2EMB';
vehlib.DRIV='DRIVER_TYPE1_Val2';
vehlib.ECU='ECU_HP_BV_2EMB';	
vehlib.ACM1='MCT_UQM_30kW';
vehlib.EMBR1='EMB1_TYPE';			% cote BV
vehlib.EMBR2='EMB2_TYPE';			% cote moteur thermique
vehlib.BV='BV5_CLIO_1L5DCI';
vehlib.ADCPL='ADDCPL_RENDCST_BAVL';
vehlib.MOTH='MTHERM_K9K_702';
vehlib.BATT='PACK_PRIUS_JAPON';
vehlib.RED='RED_CLIO_1L5DCI';
vehlib.VEHI='VEH_CLIO_1L5DCI';
vehlib.ACC='AUX_CLIO_HP';
vehlib.CYCL='CIN_NEDC_BV';
