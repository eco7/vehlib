%% =============================================================================
%% 
%%           	Initialisation des noms des fichiers composants
%%                          Pour librairie VEHLIB
%%                          IFSTTAR - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VELEC';
vehlib.DRIV='DRIVER_TYPE1_Val4';
vehlib.ECU='ECU_Citroen_C0';
vehlib.BATT='Pack_kokam_12Ah_30kW';
vehlib.ACM1='MCT_MSA_N1_PRIUS_II';
vehlib.RED='RED_Citroen_C0';
vehlib.VEHI='VEH_308SW_1L6VTI16V';
vehlib.ACC='AUX_CLIO_HP';
vehlib.CYCL='CIN_NEDC';
