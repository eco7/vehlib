% File: PRIUS_II
%% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='HPDP_EPI_SURV';
vehlib.DRIV='DRIVER_TYPE1_Val3';
vehlib.ECU='ECU_PRIUS_HSDP_EPI';
vehlib.ACM1='MCT_MSA_N1_PRIUS_II';
%vehlib.ACM2='GCT_MSA_N1_PRIUS_II';
vehlib.ACM2='GCT_MSA_N1_PRIUS_II_moteur';
vehlib.EPI='TRAIN_PRIUS_HSDP_EPI';
vehlib.RED='RED_PRIUS_HSDP_EPI';
vehlib.MOTH='MTHERM_PRIUS_II';
vehlib.BATT='PACK_PRIUS_II';
vehlib.SURV='SURV_PRIUS_II';
vehlib.VEHI='VEH_PRIUS_II';
vehlib.ACC='AUX_PRIUS_II';
vehlib.CYCL='CIN_NEDC';
vehlib.documents='Rapport LTE0626';
