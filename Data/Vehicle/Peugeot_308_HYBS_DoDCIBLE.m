% File: D308_HYBS_PVAR
%% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='HSER_GE';
vehlib.DRIV='DRIVER_TYPE1_Val4';
vehlib.ECU='ECU_308_HSER_DoDCIBLE';
vehlib.MOTH='MTH_GRUAU_DEF';
vehlib.VEHI='VEH_308SW_1L6VTI16V';
vehlib.ACM1='MCT_MSA_N1_308';
vehlib.ACM2='GCT_MSA_N1_PRIUS_II';
vehlib.RED='RED_308';
vehlib.BATT='PACK_308_HYBS';
vehlib.ACC='AUX_ELEC_TYPE';
vehlib.CYCL='CIN_NEDC';
