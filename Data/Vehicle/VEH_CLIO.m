% File: VEH_CLIO
%% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VEH';
vehlib.DRIV='DRIVER_TYPE1_Val5';
vehlib.VEHI='VEH_CLIO_1L5DCI';
vehlib.CYCL='CIN_NEDC_BV';
