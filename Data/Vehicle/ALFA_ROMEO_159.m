% File: ALFA_ROMEO_159
%% =============================================================================
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 29/07/2009
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VTH_BV';
vehlib.DRIV='DRIVER_TYPE1_Val1';
vehlib.ECU='ECU_VTH';	
vehlib.EMBR1='EMB1_GROS';	
vehlib.BV='BV6_ALFA_ROMEO_159';
vehlib.RED='RED_ALFA_ROMEO_159';
vehlib.MOTH='MTHERM_JTD150';
vehlib.VEHI='VEH_ALFA_ROMEO_159';
vehlib.BELT1='BELT1_TYPE';
vehlib.ACC='AUX_ALFA_ROMEO_159';
vehlib.CYCL='CIN_NEDC_BV';
