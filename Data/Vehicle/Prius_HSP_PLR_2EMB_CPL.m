%% =============================================================================
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='HSP_BV_2EMB';
vehlib.DRIV='DRIVER_TYPE1_Val1';
vehlib.ECU='ECU_HSP_BV_2EMB_PRIUS';
vehlib.ACM1='MCT_MSA_N1_PRIUS_II';
%vehlib.ACM2='GCT_MSA_N1_PRIUS_II';
vehlib.ACM2='GCT_MSA_N1_PRIUS_II_moteur';
vehlib.RED='RED_PRIUS_II_BV';
%vehlib.RED='RED_PRIUS_II';
vehlib.RED2='RED2_HSP_PLR';
vehlib.RED3='RED3_HSP_PLR';
vehlib.MOTH='MTHERM_PRIUS_II';
vehlib.BATT='PACK_PRIUS_II';
%vehlib.BV='BV_PRIUS_2EMB_11';
vehlib.BV='BV_PRIUS_2EMB';
vehlib.VEHI='VEH_PRIUS_II';
vehlib.ACC='AUX_PRIUS_II';
vehlib.EMBR1='EMB1_TYPE';			
vehlib.EMBR2='EMB2_TYPE';
vehlib.ADCPL1='ADCPL1_RENDCST';
vehlib.ADCPL2='ADCPL2_RENDCST';
%vehlib.ADCPL1_R='ADCPL1_RV';
%vehlib.ADCPL2_R='ADCPL2_RV';
vehlib.CYCL='CIN_NEDC';

