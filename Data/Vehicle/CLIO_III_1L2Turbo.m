% file: CLIO_III_1L2Turbo
%% =============================================================================
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    IFSTTAR - LTE 04/01/11
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VTH_BV';
vehlib.DRIV='DRIVER_TYPE1_Val1';
vehlib.ECU='ECU_VTH';
vehlib.MOTH='MTHERM_TCE_100CH';
vehlib.EMBR1='EMB1_TYPE';
vehlib.BV='BV5_CLIO_III_1L2Turbo';
vehlib.RED='RED_CLIO_III_1L2Turbo';
vehlib.VEHI='VEH_CLIO_III_1L2Turbo';
vehlib.BELT1='BELT1_TYPE';
vehlib.ACC='AUX_CLIO_III_1L2Turbo';
vehlib.CYCL='CIN_NEDC_BV';
