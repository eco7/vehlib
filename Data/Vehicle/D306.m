% File: D306
%% =============================================================================
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VTH_BV';
vehlib.DRIV='DRIVER_TYPE1_Val1';
vehlib.ECU='ECU_VTH';
vehlib.MOTH='MTHERM_XUD9A';
vehlib.EMBR1='EMB1_TYPE';
vehlib.BV='BV5_D306';
vehlib.RED='RED_D306';
vehlib.VEHI='VEH_D306';
vehlib.BELT1='BELT1_TYPE';
vehlib.ACC='AUX_D306';
vehlib.CYCL='CIN_NEDC_BV';
