% Tesla model 3 electrical vehicle
% Datas come from : https://zeperfs.com/en/fiche9245-tesla-model-3-standard-plus.htm
% This data file is used to create a vehlib's compatible structure (VD) with a reduced number of parameters
% 

nbacm1 =1;
Cme_max=420; % Nm
Pme_max=210*1000; % W
Wme_max = 16000*pi/30;
% Pneumatiques 235/45 R 18
largeur = 0.235;                                % m
hauteur = 0.45;                                 % en pourcentage de la largeur
d_jante = 18*25.4e-3;                           % diametre de jante en pouce => m

Cx=0.23; % /
Sveh = 76*58*25.4*25.4/10^6;
Veh_Mass=1750; % kg
Batt_nrj=55*1000; % kWh
Wmax = (16000*pi/30);
Vmax = (222/3.6);
kred=Wmax/(Vmax*2*pi/2); % 222 km/h pour 16000rpm

Pacc_elec = 400;
