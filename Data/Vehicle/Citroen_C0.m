%% =============================================================================
%% 
%%           	Initialisation des noms des fichiers composants
%%                          Pour librairie VEHLIB
%%                          IFSTTAR - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VELEC';
vehlib.DRIV='DRIVER_TYPE1_Val4';
vehlib.ECU='ECU_Citroen_C0';
vehlib.BATT='Pack_Citroen_C0';
vehlib.ACM1='MCT_MSA_C0_47kW';
vehlib.RED='RED_Citroen_C0';
vehlib.VEHI='VEH_Citroen_C0';
vehlib.ACC='AUX_Citroen_C0';
vehlib.CYCL='CIN_NEDC';
