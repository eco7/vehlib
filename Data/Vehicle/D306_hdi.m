% File: D306_hdi
%% =============================================================================
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VTH_BV';
vehlib.DRIV='DRIVER_TYPE1_Val1';
vehlib.ECU='ECU_VTH';
vehlib.MOTH='MTHERM_DW10_TD';
vehlib.EMBR1='EMB1_TYPE';
vehlib.BV='BV5_D306_hdi';
vehlib.RED='RED_D306_hdi';
vehlib.VEHI='VEH_D306_hdi';
vehlib.BELT1='BELT1_TYPE';
vehlib.ACC='AUX_D306_hdi';
vehlib.CYCL='CIN_NEDC_BV';
