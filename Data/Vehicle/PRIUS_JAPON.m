% File: PRIUS_JAPON
%% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='HPDP_EPI';
vehlib.DRIV='DRIVER_TYPE1_Val3';
vehlib.ECU='ECU_PRIUS_JAPON';
vehlib.ACM1='MCT_MSA_PRIUS_JAPON';
vehlib.ACM2='GCT_MSA_PRIUS_JAPON';
vehlib.EPI='TRAIN_PRIUS_JAPON';
vehlib.RED='RED_PRIUS_JAPON';
vehlib.MOTH='MTHERM_PRIUS_JAPON';
vehlib.BATT='PACK_PRIUS_JAPON';
vehlib.VEHI='VEH_PRIUS_JAPON';
vehlib.ACC='AUX_PRIUS_JAPON';
vehlib.CYCL='CIN_NEDC';
vehlib.documents='Rapport LTE9925';
