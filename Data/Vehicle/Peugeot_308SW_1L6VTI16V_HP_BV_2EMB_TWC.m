% File: Peugeot_308SW_1L6VTI16V
% file: P308_SW_1L6VTI16V
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% =============================================================================
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    IFSTTAR - LTE 04/01/11
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='HP_BV_2EMB';
vehlib.DRIV='DRIVER_TYPE1_Val2';
vehlib.ECU='ECU_VTH';
vehlib.MOTH='MTHERM_EP6';
vehlib.TWC='TWC_data_N0';
vehlib.EMBR1='EMB1_TYPE';
vehlib.BV='BV5_308_20DP42';
vehlib.RED='RED_308';
vehlib.VEHI='VEH_308SW_1L6VTI16V';
vehlib.BELT1='BELT1_TYPE';
vehlib.ACC='AUX_CLIO_HP';
vehlib.CYCL='CIN_NEDC_BV';
vehlib.ACM1='MCT_MSA_N1_PRIUS_II';
vehlib.BATT='PACK_PRIUS_II_3P';
vehlib.EMBR2='EMB2_TYPE';			% cote moteur thermique
vehlib.ADCPL='ADCPL_RENDCST';

