% File: PRIUS_II
%% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='HSER_GE';
vehlib.DRIV='DRIVER_TYPE1_Val4';
vehlib.ECU='ECU_PRIUS_HS_GE';
vehlib.ACM1='MCT_MSA_N1_PRIUS_II';
%vehlib.ACM2='GCT_MSA_N1_PRIUS_II';
vehlib.ACM2='GCT_MSA_N1_PRIUS_II_moteur';
vehlib.RED='RED_PRIUS_II';
vehlib.MOTH='MTHERM_PRIUS_II';
vehlib.BATT='PACK_PRIUS_II';
vehlib.VEHI='VEH_PRIUS_II';
vehlib.ACC='AUX_PRIUS_II';
vehlib.CYCL='CIN_NEDC';
vehlib.documents='Rapport LTE0626';
