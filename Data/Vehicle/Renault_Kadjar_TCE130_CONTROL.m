% File: Renault_Kadjar_TCE130_CONTROL.m
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% =============================================================================
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    IFSTTAR - LTE 04/01/11
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VTH_BV_CONTROL';
vehlib.DRIV='DRIVER_TYPE1_Val2';
vehlib.ECU='ECU_VTH_CONTROL_TCE130';
vehlib.EMBR1='EMB1_TYPE';
vehlib.BV='BV6_Kadjar_TL4';
vehlib.RED='RED_Kadjar';
vehlib.VEHI='VEH_Kadjar';
vehlib.BELT1='BELT1_TYPE';
vehlib.ACC='AUX_Kadjar';
vehlib.CYCL='CIN_NEDC_BV6';
vehlib.MOTH='MTHERM_CONTROL_TCE130';
vehlib.TWC='TWC_TCE130';

