% File: CLIO_1L5DCI_perfos
%% =============================================================================
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VTH_BV';
vehlib.DRIV='DRIVER_TYPE1_Val1';
vehlib.ECU='ECU_VTH_perfos';
vehlib.MOTH='MTHERM_K9K_702';
vehlib.EMBR1='EMB1_TYPE';
vehlib.BV='BV5_CLIO_1L5DCI';
vehlib.RED='RED_CLIO_1L5DCI';
vehlib.VEHI='VEH_CLIO_1L5DCI';
vehlib.BELT1='BELT1_CLIO_1L5DCI';
vehlib.ACC='AUX_CLIO_1L5DCI';
vehlib.CYCL='CIN_NEDC_BV';
