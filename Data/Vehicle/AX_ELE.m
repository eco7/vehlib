% File: AX_ELE
%% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    INRETS - LTE 03/07/01
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VELEC';
vehlib.DRIV='DRIVER_TYPE1_Val4';
vehlib.ECU='ECU_AX';
vehlib.BATT='Pack_AXELEC';
vehlib.ACM1='MCT_MCC_SA13';
vehlib.RED='RED_AXELEC';
vehlib.VEHI='VEH_AXELEC';
vehlib.ACC='AUX_AXELEC';
vehlib.CYCL='CIN_ECE15';
