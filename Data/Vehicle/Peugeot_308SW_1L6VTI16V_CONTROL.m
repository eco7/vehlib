% File: Peugeot_308SW_1L6VTI16V_HP_BV_2EMB_kokam_DRIVE
% file: P308_SW_1L6VTI16V
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% =============================================================================
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    IFSTTAR - LTE 04/01/11
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.DRIV='DRIVER_TYPE1_Val2';
vehlib.ECU='ECU_VTH';
vehlib.EMBR1='EMB1_TYPE';
vehlib.BV='BV5_308_20DP42';
vehlib.RED='RED_308';
vehlib.VEHI='VEH_308SW_1L6VTI16V';
vehlib.ACC='AUX_CLIO_HP';
vehlib.CYCL='CIN_NEDC_BV';
vehlib.MOTH='MTHERM_CONTROL_1L6';
vehlib.TWC='TWC_TCE130';

