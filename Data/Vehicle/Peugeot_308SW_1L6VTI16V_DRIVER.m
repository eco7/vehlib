% File: Peugeot_308SW_1L6VTI16V
% file: P308_SW_1L6VTI16V
%
%  ?? Copyright IFSTTAR LTE 1999-2011 
%
%% =============================================================================
%% 
%%                Initialisation des noms des fichiers composants
%%                      Pour librairie VEHLIB
%%                    IFSTTAR - LTE 04/01/11
%%
%%  Objet: Ce script initialise la structure vehlib
%%         contenant les champs necessaires a un calcul.
%%          
%%
%% =============================================================================

vehlib.INIT='initdata';
vehlib.simulink='VTH_BV_DRIVER';
vehlib.DRIV='DRIVER_TYPE2_DYN';
vehlib.ECU='ECU_VTH';
vehlib.MOTH='MTHERM_EP6';
vehlib.EMBR1='EMB1_TYPE';
vehlib.BV='BV5_308_20DP42';
vehlib.RED='RED_308';
vehlib.VEHI='VEH_308SW_1L6VTI16V';
vehlib.BELT1='BELT1_TYPE';
vehlib.ACC='AUX_308SW_1L6VTI16V';
vehlib.CYCL='TRAJET_ARTROUT_Vmax';

