% File: SORT3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%            nom         : SORT3.m
%
%          Language      : MATLAB v4 et 5 
%
%            auteur      : INRETS/LTE
% 
%       Date de creation : Juin 2004
%
%            Sujet       : Fichier cinematique vitesse-distance
%                          pour les modeles de simulation
%                          de la bibliotheque VEHLIB
%		 Cycle SORT UITP pour autobus
%		 SORT3: SubUrbain
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Type de cinematique 2: vitesse - distance
CYCL.ntypcin=2;

% Recharge reseau obsolete aujourd'hui
CYCL.recharge=0;

%djerk en m/s3
CYCL.djerk=10;

% acceleration maximum autorisee em m/s2
CYCL.acclig=0.77;

% deceleration constante en m/s2
CYCL.dcclig=-0.8;

% Donnees de definition de la ligne 
% type balise==1 distance (km)      vitesse (km/h)  ELE/HYB (1:autorisation/0:interdiction)
% type balise==2 Temps arret (s)  charge passagers (kg)  ELE/HYB (1:autorisation/0:interdiction)
% type balise==3 Temps de charge reseau EDF(s)   charge passager (kg)   ELE/HYB (1:autorisation/0:interdiction)
CYCL.def_ligne=[	1   0.200          30.0     1
 			2   20.0000        0       1
 			1   0.600          50.0     1
 			2   10.0000        0       1
 			1   0.650          60.0     1
 			2   10.0000        0       1];

CYCL.ntyppente=1;
            % Donnees de pente
% distance (km)   pente (%)
CYCL.pente_ligne=[   0.00          0.
                     6.00          0];


% Donnees de consommation des accessoires
% distance (km)   Puissance accessoires (W) Obsolete aujourd'hui
CYCL.acces_ligne=[     0.00          0
                     6.00          0];

