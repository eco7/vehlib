ECE15=[
0 0 0
11 0 1
4 15 1
8 15 1
2 10 1
3 0 0
21 0 1
5 15 1
1 15 2
1 15 2
5 32 2
24 32 2
8 10 2
3 0 0
21 0 1
5 15 1
1 15 2
1 15 2
9 35 2
1 35 3
1 35 3
8 50 3
12 50 3
8 35 3
13 35 3
1 35 2
1 35 2
7 10 2
3 0 0
6 0 0];

EUDC=[
0 0 0
20 0 1
5 15 1
1 15 2
1 15 2
9 35 2
1 35 3
1 35 3
8 50 3
1 50 4
1 50 4
13 70 4
1 70 5
49 70 5
4 60 5
4 50 4
69 50 4
13 70 4
1 70 5
49 70 5
35 100 5
30 100 5
20 120 5
10 120 5
16 80 5
8 50 5
10 0 0
20 0 0];

t_ece15=cumsum(ECE15(:,1));
t_eudc=cumsum(EUDC(:,1));

t_nedc=[t_ece15;t_ece15+max(t_ece15)+1;t_ece15+2*max(t_ece15)+2;t_ece15+3*max(t_ece15)+3;t_eudc+4*max(t_ece15)+4];

v_ece15=ECE15(:,2);
v_eudc=EUDC(:,2);
v_nedc=[v_ece15;v_ece15;v_ece15;v_ece15;v_eudc];

r_ece15=ECE15(:,3);
r_eudc=EUDC(:,3);
r_nedc=[r_ece15;r_ece15;r_ece15;r_ece15;r_eudc];

t_ech=1;
t=0:t_ech:max(t_nedc);

v=interp1(t_nedc,v_nedc,t);
r=floor(interp1(t_nedc,r_nedc,t)+eps); 
% en 2016b: 
% a = interp1(t_nedc,r_nedc,t);
% floor(a(end-22))
%
% ans =
%
%     0
%>> a(end-22)
% 
% ans =
% 
%    1.000000000000000
%
% en 2010b
% a = interp1(t_nedc,r_nedc,t);
% floor(a(end-22))
%
%ans =
%
%     1

CYCL.ntypcin=3;	% cinematique vitesse-temps-rapport de boite
CYCL.recharge=0;	% Recharge non dispo aujourd'hui
CYCL.pente=0;		% Pente en %
CYCL.temps=t;
CYCL.temps=CYCL.temps;
CYCL.vitesse=v./3.6;
CYCL.vitesse=CYCL.vitesse;
CYCL.rappvit=r';

clearvars ECE15 EUDC r r_ece15 r_eudc r_nedc t t_ece15 t_ech t_eudc t_nedc v v_ece15 v_eudc v_nedc

display('Fin d''initialisation des parametres cinematiques'); 

% plot(t,v,t,r)
% grid on