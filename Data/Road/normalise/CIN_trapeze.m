%%    fichier cin�matique


CYCL.ntypcin=1;	% cinematique vitesse-temps

CYCL.recharge=0;	% Recharge non dispo aujourd'hui
CYCL.pente=0;		% Pente en %


accel=1;
t=0;
v0=0;
fq=1;
a=0.5;
d=-1;
vmax=50/3.6;

acc=a;
ind=1;
v=v0;
CYCL.temps(ind)=t;
CYCL.vitesse(ind)=v;

while (accel==1)
    ind=ind+1;
    t=t+fq;
    v=v+acc*fq;
    if v>vmax
        acc=d;
    end
    CYCL.temps(ind)=t;
    CYCL.vitesse(ind)=v;
    if acc==d & v<=0
        break
    end
end
