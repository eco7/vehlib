% File: CIN_NEDC_BV
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier cinematique
%                      Pour librairie VEHLIB
%                          INRETS - LTE
%  Objet: Ce script initialise la structure CYCL
%          contenant les champs necessaires pour renseigner 
%          le composant cinematique de la librairie VEHLIB.
%
%   Cycle normalise europeen 
%
% =============================================================================


CYCL.ntypcin=3;	% cinematique vitesse-temps-rapport de boite

CYCL.recharge=0;	% Recharge non dispo aujourd'hui
CYCL.pente=0;		% Pente en %

% Temps, vitesse, rapport de boite
A=[  1  0.00    0
    2  0.00    0
    3  0.00    0
    4  0.00    0
    5  0.00    0
    6  0.00    0
    7  0.00    0
    8  0.00    0
    9  0.00    0
   10  0.00    1
   11  0.00    1
   12  3.80    1
   13  7.50    1
   14 11.20    1
   15 15.00    1
   16 15.00    1
   17 15.00    1
   18 15.00    1
   19 15.00    1
   20 15.00    1
   21 15.00    1
   22 15.00    1
   23 15.00    1
   24 12.50    1
   25 10.00    1
   26  6.70    0
   27  3.30    0
   28  0.00    0
   29  0.00    0
   30  0.00    0
   31  0.00    0
   32  0.00    0
   33  0.00    0
   34  0.00    0
   35  0.00    0
   36  0.00    0
   37  0.00    0
   38  0.00    0
   39  0.00    0
   40  0.00    0
   41  0.00    0
   42  0.00    0
   43  0.00    0
   44  0.00    0
   45  0.00    0
   46  0.00    0
   47  0.00    0
   48  0.00    1
   49  0.00    1
   50  3.00    1
   51  6.00    1
   52  9.00    1
   53 12.00    1
   54 15.00    2
   55 15.00    2
   56 15.00    2
   57 18.40    2
   58 21.80    2
   59 25.20    2
   60 28.60    2
   61 32.00    2
   62 32.00    2
   63 32.00    2
   64 32.00    2
   65 32.00    2
   66 32.00    2
   67 32.00    2
   68 32.00    2
   69 32.00    2
   70 32.00    2
   71 32.00    2
   72 32.00    2
   73 32.00    2
   74 32.00    2
   75 32.00    2
   76 32.00    2
   77 32.00    2
   78 32.00    2
   79 32.00    2
   80 32.00    2
   81 32.00    2
   82 32.00    2
   83 32.00    2
   84 32.00    2
   85 32.00    2
   86 29.20    2
   87 26.50    2
   88 23.80    2
   89 21.00    2
   90 18.20    2
   91 15.50    2
   92 12.80    2
   93 10.00    0
   94  6.70    0
   95  3.30    0
   96  0.00    0
   97  0.00    0
   98  0.00    0
   99  0.00    0
  100  0.00    0
  101  0.00    0
  102  0.00    0
  103  0.00    0
  104  0.00    0
  105  0.00    0
  106  0.00    0
  107  0.00    0
  108  0.00    0
  109  0.00    0
  110  0.00    0
  111  0.00    0
  112  0.00    0
  113  0.00    0
  114  0.00    0
  115  0.00    0
  116  0.00    1
  117  0.00    1
  118  3.00    1
  119  6.00    1
  120  9.00    1
  121 12.00    1
  122 15.00    2
  123 15.00    2
  124 15.00    2
  125 17.20    2
  126 19.40    2
  127 21.70    2
  128 23.90    2
  129 26.10    2
  130 28.30    2
  131 30.60    2
  132 32.80    2
  133 35.00    3
  134 35.00    3
  135 35.00    3
  136 36.90    3
  137 38.80    3
  138 40.60    3
  139 42.50    3
  140 44.40    3
  141 46.20    3
  142 48.10    3
  143 50.00    3
  144 50.00    3
  145 50.00    3
  146 50.00    3
  147 50.00    3
  148 50.00    3
  149 50.00    3
  150 50.00    3
  151 50.00    3
  152 50.00    3
  153 50.00    3
  154 50.00    3
  155 50.00    3
  156 48.10    3
  157 46.20    3
  158 44.40    3
  159 42.50    3
  160 40.60    3
  161 38.80    3
  162 36.90    3
  163 35.00    3
  164 35.00    3
  165 35.00    3
  166 35.00    3
  167 35.00    3
  168 35.00    3
  169 35.00    3
  170 35.00    3
  171 35.00    3
  172 35.00    3
  173 35.00    3
  174 35.00    3
  175 35.00    3
  176 35.00    3
  177 33.50    3
  178 32.00    3
  179 28.90    2
  180 25.70    2
  181 22.60    2
  182 19.40    2
  183 16.30    2
  184 13.10    2
  185 10.00    0
  186  6.70    0
  187  3.30    0
  188  0.00    0
  189  0.00    0
  190  0.00    0
  191  0.00    0
  192  0.00    0
  193  0.00    0
  194  0.00    0
  195  0.00    0
  196  0.00    0
  197  0.00    0
  198  0.00    0
  199  0.00    0
  200  0.00    0
  201  0.00    0
  202  0.00    0
  203  0.00    0
  204  0.00    0
  205  0.00    1
  206  0.00    1
  207  3.80    1
  208  7.50    1
  209 11.20    1
  210 15.00    1
  211 15.00    1
  212 15.00    1
  213 15.00    1
  214 15.00    1
  215 15.00    1
  216 15.00    1
  217 15.00    1
  218 15.00    1
  219 12.50    1
  220 10.00    1
  221  6.70    0
  222  3.30    0
  223  0.00    0
  224  0.00    0
  225  0.00    0
  226  0.00    0
  227  0.00    0
  228  0.00    0
  229  0.00    0
  230  0.00    0
  231  0.00    0
  232  0.00    0
  233  0.00    0
  234  0.00    0
  235  0.00    0
  236  0.00    0
  237  0.00    0
  238  0.00    0
  239  0.00    0
  240  0.00    0
  241  0.00    0
  242  0.00    0
  243  0.00    1
  244  0.00    1
  245  3.00    1
  246  6.00    1
  247  9.00    1
  248 12.00    1
  249 15.00    2
  250 15.00    2
  251 15.00    2
  252 18.40    2
  253 21.80    2
  254 25.20    2
  255 28.60    2
  256 32.00    2
  257 32.00    2
  258 32.00    2
  259 32.00    2
  260 32.00    2
  261 32.00    2
  262 32.00    2
  263 32.00    2
  264 32.00    2
  265 32.00    2
  266 32.00    2
  267 32.00    2
  268 32.00    2
  269 32.00    2
  270 32.00    2
  271 32.00    2
  272 32.00    2
  273 32.00    2
  274 32.00    2
  275 32.00    2
  276 32.00    2
  277 32.00    2
  278 32.00    2
  279 32.00    2
  280 32.00    2
  281 29.20    2
  282 26.50    2
  283 23.80    2
  284 21.00    2
  285 18.20    2
  286 15.50    2
  287 12.80    2
  288 10.00    0
  289  6.70    0
  290  3.30    0
  291  0.00    0
  292  0.00    0
  293  0.00    0
  294  0.00    0
  295  0.00    0
  296  0.00    0
  297  0.00    0
  298  0.00    0
  299  0.00    0
  300  0.00    0
  301  0.00    0
  302  0.00    0
  303  0.00    0
  304  0.00    0
  305  0.00    0
  306  0.00    0
  307  0.00    0
  308  0.00    0
  309  0.00    0
  310  0.00    0
  311  0.00    1
  312  0.00    1
  313  3.00    1
  314  6.00    1
  315  9.00    1
  316 12.00    1
  317 15.00    2
  318 15.00    2
  319 15.00    2
  320 17.20    2
  321 19.40    2
  322 21.70    2
  323 23.90    2
  324 26.10    2
  325 28.30    2
  326 30.60    2
  327 32.80    2
  328 35.00    3
  329 35.00    3
  330 35.00    3
  331 36.90    3
  332 38.80    3
  333 40.60    3
  334 42.50    3
  335 44.40    3
  336 46.20    3
  337 48.10    3
  338 50.00    3
  339 50.00    3
  340 50.00    3
  341 50.00    3
  342 50.00    3
  343 50.00    3
  344 50.00    3
  345 50.00    3
  346 50.00    3
  347 50.00    3
  348 50.00    3
  349 50.00    3
  350 50.00    3
  351 48.10    3
  352 46.20    3
  353 44.40    3
  354 42.50    3
  355 40.60    3
  356 38.80    3
  357 36.90    3
  358 35.00    3
  359 35.00    3
  360 35.00    3
  361 35.00    3
  362 35.00    3
  363 35.00    3
  364 35.00    3
  365 35.00    3
  366 35.00    3
  367 35.00    3
  368 35.00    3
  369 35.00    3
  370 35.00    3
  371 35.00    3
  372 33.50    3
  373 32.00    3
  374 28.90    3
  375 25.70    2
  376 22.60    2
  377 19.40    2
  378 16.30    2
  379 13.10    0
  380 10.00    0
  381  6.70    0
  382  3.30    0
  383  0.00    0
  384  0.00    0
  385  0.00    0
  386  0.00    0
  387  0.00    0
  388  0.00    0
  389  0.00    0
  390  0.00    0
  391  0.00    0
  392  0.00    0
  393  0.00    0
  394  0.00    0
  395  0.00    0
  396  0.00    0
  397  0.00    0
  398  0.00    0
  399  0.00    0
  400  0.00    1
  401  0.00    1
  402  3.80    1
  403  7.50    1
  404 11.20    1
  405 15.00    1
  406 15.00    1
  407 15.00    1
  408 15.00    1
  409 15.00    1
  410 15.00    1
  411 15.00    1
  412 15.00    1
  413 15.00    1
  414 12.50    1
  415 10.00    1
  416  6.70    0
  417  3.30    0
  418  0.00    0
  419  0.00    0
  420  0.00    0
  421  0.00    0
  422  0.00    0
  423  0.00    0
  424  0.00    0
  425  0.00    0
  426  0.00    0
  427  0.00    0
  428  0.00    0
  429  0.00    0
  430  0.00    0
  431  0.00    0
  432  0.00    0
  433  0.00    0
  434  0.00    0
  435  0.00    0
  436  0.00    0
  437  0.00    0
  438  0.00    1
  439  0.00    1
  440  3.00    1
  441  6.00    1
  442  9.00    1
  443 12.00    1
  444 15.00    2
  445 15.00    2
  446 15.00    2
  447 18.40    2
  448 21.80    2
  449 25.20    2
  450 28.60    2
  451 32.00    2
  452 32.00    2
  453 32.00    2
  454 32.00    2
  455 32.00    2
  456 32.00    2
  457 32.00    2
  458 32.00    2
  459 32.00    2
  460 32.00    2
  461 32.00    2
  462 32.00    2
  463 32.00    2
  464 32.00    2
  465 32.00    2
  466 32.00    2
  467 32.00    2
  468 32.00    2
  469 32.00    2
  470 32.00    2
  471 32.00    2
  472 32.00    2
  473 32.00    2
  474 32.00    2
  475 32.00    2
  476 29.20    2
  477 26.50    2
  478 23.80    2
  479 21.00    2
  480 18.20    2
  481 15.50    2
  482 12.80    0
  483 10.00    0
  484  6.70    0
  485  3.30    0
  486  0.00    0
  487  0.00    0
  488  0.00    0
  489  0.00    0
  490  0.00    0
  491  0.00    0
  492  0.00    0
  493  0.00    0
  494  0.00    0
  495  0.00    0
  496  0.00    0
  497  0.00    0
  498  0.00    0
  499  0.00    0
  500  0.00    0
  501  0.00    0
  502  0.00    0
  503  0.00    0
  504  0.00    0
  505  0.00    0
  506  0.00    1
  507  0.00    1
  508  3.00    1
  509  6.00    1
  510  9.00    1
  511 12.00    1
  512 15.00    2
  513 15.00    2
  514 15.00    2
  515 17.20    2
  516 19.40    2
  517 21.70    2
  518 23.90    2
  519 26.10    2
  520 28.30    2
  521 30.60    2
  522 32.80    2
  523 35.00    3
  524 35.00    3
  525 35.00    3
  526 36.90    3
  527 38.80    3
  528 40.60    3
  529 42.50    3
  530 44.40    3
  531 46.20    3
  532 48.10    3
  533 50.00    3
  534 50.00    3
  535 50.00    3
  536 50.00    3
  537 50.00    3
  538 50.00    3
  539 50.00    3
  540 50.00    3
  541 50.00    3
  542 50.00    3
  543 50.00    3
  544 50.00    3
  545 50.00    3
  546 48.10    3
  547 46.20    3
  548 44.40    3
  549 42.50    3
  550 40.60    3
  551 38.80    3
  552 36.90    3
  553 35.00    3
  554 35.00    3
  555 35.00    3
  556 35.00    3
  557 35.00    3
  558 35.00    3
  559 35.00    3
  560 35.00    3
  561 35.00    3
  562 35.00    3
  563 35.00    3
  564 35.00    3
  565 35.00    3
  566 35.00    3
  567 33.50    3
  568 32.00    3
  569 28.90    3
  570 25.70    2
  571 22.60    2
  572 19.40    2
  573 16.30    2
  574 13.10    0
  575 10.00    0
  576  6.70    0
  577  3.30    0
  578  0.00    0
  579  0.00    0
  580  0.00    0
  581  0.00    0
  582  0.00    0
  583  0.00    0
  584  0.00    0
  585  0.00    0
  586  0.00    0
  587  0.00    0
  588  0.00    0
  589  0.00    0
  590  0.00    0
  591  0.00    0
  592  0.00    0
  593  0.00    0
  594  0.00    0
  595  0.00    1
  596  0.00    1
  597  3.80    1
  598  7.50    1
  599 11.20    1
  600 15.00    1
  601 15.00    1
  602 15.00    1
  603 15.00    1
  604 15.00    1
  605 15.00    1
  606 15.00    1
  607 15.00    1
  608 15.00    1
  609 12.50    1
  610 10.00    1
  611  6.70    0
  612  3.30    0
  613  0.00    0
  614  0.00    0
  615  0.00    0
  616  0.00    0
  617  0.00    0
  618  0.00    0
  619  0.00    0
  620  0.00    0
  621  0.00    0
  622  0.00    0
  623  0.00    0
  624  0.00    0
  625  0.00    0
  626  0.00    0
  627  0.00    0
  628  0.00    0
  629  0.00    0
  630  0.00    0
  631  0.00    0
  632  0.00    0
  633  0.00    1
  634  0.00    1
  635  3.00    1
  636  6.00    1
  637  9.00    1
  638 12.00    1
  639 15.00    2
  640 15.00    2
  641 15.00    2
  642 18.40    2
  643 21.80    2
  644 25.20    2
  645 28.60    2
  646 32.00    2
  647 32.00    2
  648 32.00    2
  649 32.00    2
  650 32.00    2
  651 32.00    2
  652 32.00    2
  653 32.00    2
  654 32.00    2
  655 32.00    2
  656 32.00    2
  657 32.00    2
  658 32.00    2
  659 32.00    2
  660 32.00    2
  661 32.00    2
  662 32.00    2
  663 32.00    2
  664 32.00    2
  665 32.00    2
  666 32.00    2
  667 32.00    2
  668 32.00    2
  669 32.00    2
  670 32.00    2
  671 29.20    2
  672 26.50    2
  673 23.80    2
  674 21.00    2
  675 18.20    2
  676 15.50    2
  677 12.80    0
  678 10.00    0
  679  6.70    0
  680  3.30    0
  681  0.00    0
  682  0.00    0
  683  0.00    0
  684  0.00    0
  685  0.00    0
  686  0.00    0
  687  0.00    0
  688  0.00    0
  689  0.00    0
  690  0.00    0
  691  0.00    0
  692  0.00    0
  693  0.00    0
  694  0.00    0
  695  0.00    0
  696  0.00    0
  697  0.00    0
  698  0.00    0
  699  0.00    0
  700  0.00    0
  701  0.00    1
  702  0.00    1
  703  3.00    1
  704  6.00    1
  705  9.00    1
  706 12.00    1
  707 15.00    2
  708 15.00    2
  709 15.00    2
  710 17.20    2
  711 19.40    2
  712 21.70    2
  713 23.90    2
  714 26.10    2
  715 28.30    2
  716 30.60    2
  717 32.80    2
  718 35.00    3
  719 35.00    3
  720 35.00    3
  721 36.90    3
  722 38.80    3
  723 40.60    3
  724 42.50    3
  725 44.40    3
  726 46.20    3
  727 48.10    3
  728 50.00    3
  729 50.00    3
  730 50.00    3
  731 50.00    3
  732 50.00    3
  733 50.00    3
  734 50.00    3
  735 50.00    3
  736 50.00    3
  737 50.00    3
  738 50.00    3
  739 50.00    3
  740 50.00    3
  741 48.10    3
  742 46.20    3
  743 44.40    3
  744 42.50    3
  745 40.60    3
  746 38.80    3
  747 36.90    3
  748 35.00    3
  749 35.00    3
  750 35.00    3
  751 35.00    3
  752 35.00    3
  753 35.00    3
  754 35.00    3
  755 35.00    3
  756 35.00    3
  757 35.00    3
  758 35.00    3
  759 35.00    3
  760 35.00    3
  761 35.00    3
  762 33.50    3
  763 32.00    3
  764 28.90    3
  765 25.70    2
  766 22.60    2
  767 19.40    2
  768 16.30    2
  769 13.10    0
  770 10.00    0
  771  6.90    0
  772  3.80    0
  773  0.60    0
  774  0.00    0
  775  0.00    0
  776  0.00    0
  777  0.00    0
  778  0.00    0
  779  0.00    0
  780  0.00    0
  781  0.00    0
  782  0.00    0
  783  0.00    0
  784  0.00    0
  785  0.00    0
  786  0.00    0
  787  0.00    0
  788  0.00    0
  789  0.00    0
  790  0.00    0
  791  0.00    0
  792  0.00    0
  793  0.00    0
  794  0.00    0
  795  0.00    0
  796  0.00    0
  797  0.00    0
  798  0.00    0
  799  0.00    1
  800  0.00    1
  801  3.00    1
  802  6.00    1
  803  9.00    1
  804 12.00    1
  805 15.00    2
  806 15.00    2
  807 15.00    2
  808 17.20    2
  809 19.40    2
  810 21.70    2
  811 23.90    2
  812 26.10    2
  813 28.30    2
  814 30.60    2
  815 32.80    2
  816 35.00    3
  817 35.00    3
  818 35.00    3
  819 36.90    3
  820 38.80    3
  821 40.60    3
  822 42.50    3
  823 44.40    3
  824 46.20    3
  825 48.10    3
  826 50.00    4
  827 50.00    4
  828 50.00    4
  829 51.50    4
  830 53.10    4
  831 54.60    4
  832 56.20    4
  833 57.70    4
  834 59.20    4
  835 60.80    4
  836 62.30    4
  837 63.80    4
  838 65.40    4
  839 66.90    4
  840 68.50    4
  841 70.00    5
  842 70.00    5
  843 70.00    5
  844 70.00    5
  845 70.00    5
  846 70.00    5
  847 70.00    5
  848 70.00    5
  849 70.00    5
  850 70.00    5
  851 70.00    5
  852 70.00    5
  853 70.00    5
  854 70.00    5
  855 70.00    5
  856 70.00    5
  857 70.00    5
  858 70.00    5
  859 70.00    5
  860 70.00    5
  861 70.00    5
  862 70.00    5
  863 70.00    5
  864 70.00    5
  865 70.00    5
  866 70.00    5
  867 70.00    5
  868 70.00    5
  869 70.00    5
  870 70.00    5
  871 70.00    5
  872 70.00    5
  873 70.00    5
  874 70.00    5
  875 70.00    5
  876 70.00    5
  877 70.00    5
  878 70.00    5
  879 70.00    5
  880 70.00    5
  881 70.00    5
  882 70.00    5
  883 70.00    5
  884 70.00    5
  885 70.00    5
  886 70.00    5
  887 70.00    5
  888 70.00    5
  889 70.00    5
  890 70.00    5
  891 70.00    5
  892 67.50    5
  893 65.00    5
  894 62.50    5
  895 60.00    5
  896 57.50    4
  897 55.00    4
  898 52.50    4
  899 50.00    4
  900 50.00    4
  901 50.00    4
  902 50.00    4
  903 50.00    4
  904 50.00    4
  905 50.00    4
  906 50.00    4
  907 50.00    4
  908 50.00    4
  909 50.00    4
  910 50.00    4
  911 50.00    4
  912 50.00    4
  913 50.00    4
  914 50.00    4
  915 50.00    4
  916 50.00    4
  917 50.00    4
  918 50.00    4
  919 50.00    4
  920 50.00    4
  921 50.00    4
  922 50.00    4
  923 50.00    4
  924 50.00    4
  925 50.00    4
  926 50.00    4
  927 50.00    4
  928 50.00    4
  929 50.00    4
  930 50.00    4
  931 50.00    4
  932 50.00    4
  933 50.00    4
  934 50.00    4
  935 50.00    4
  936 50.00    4
  937 50.00    4
  938 50.00    4
  939 50.00    4
  940 50.00    4
  941 50.00    4
  942 50.00    4
  943 50.00    4
  944 50.00    4
  945 50.00    4
  946 50.00    4
  947 50.00    4
  948 50.00    4
  949 50.00    4
  950 50.00    4
  951 50.00    4
  952 50.00    4
  953 50.00    4
  954 50.00    4
  955 50.00    4
  956 50.00    4
  957 50.00    4
  958 50.00    4
  959 50.00    4
  960 50.00    4
  961 50.00    4
  962 50.00    4
  963 50.00    4
  964 50.00    4
  965 50.00    4
  966 50.00    4
  967 50.00    4
  968 50.00    4
  969 51.50    4
  970 53.10    4
  971 54.60    4
  972 56.20    4
  973 57.70    4
  974 59.20    4
  975 60.80    4
  976 62.30    4
  977 63.80    4
  978 65.40    4
  979 66.90    4
  980 68.50    4
  981 70.00    5
  982 70.00    5
  983 70.00    5
  984 70.00    5
  985 70.00    5
  986 70.00    5
  987 70.00    5
  988 70.00    5
  989 70.00    5
  990 70.00    5
  991 70.00    5
  992 70.00    5
  993 70.00    5
  994 70.00    5
  995 70.00    5
  996 70.00    5
  997 70.00    5
  998 70.00    5
  999 70.00    5
 1000 70.00    5
 1001 70.00    5
 1002 70.00    5
 1003 70.00    5
 1004 70.00    5
 1005 70.00    5
 1006 70.00    5
 1007 70.00    5
 1008 70.00    5
 1009 70.00    5
 1010 70.00    5
 1011 70.00    5
 1012 70.00    5
 1013 70.00    5
 1014 70.00    5
 1015 70.00    5
 1016 70.00    5
 1017 70.00    5
 1018 70.00    5
 1019 70.00    5
 1020 70.00    5
 1021 70.00    5
 1022 70.00    5
 1023 70.00    5
 1024 70.00    5
 1025 70.00    5
 1026 70.00    5
 1027 70.00    5
 1028 70.00    5
 1029 70.00    5
 1030 70.00    5
 1031 70.00    5
 1032 70.90    5
 1033 71.70    5
 1034 72.60    5
 1035 73.40    5
 1036 74.30    5
 1037 75.10    5
 1038 76.00    5
 1039 76.90    5
 1040 77.70    5
 1041 78.60    5
 1042 79.40    5
 1043 80.30    5
 1044 81.10    5
 1045 82.00    5
 1046 82.90    5
 1047 83.70    5
 1048 84.60    5
 1049 85.40    5
 1050 86.30    5
 1051 87.10    5
 1052 88.00    5
 1053 88.90    5
 1054 89.70    5
 1055 90.60    5
 1056 91.40    5
 1057 92.30    5
 1058 93.10    5
 1059 94.00    5
 1060 94.90    5
 1061 95.70    5
 1062 96.60    5
 1063 97.40    5
 1064 98.30    5
 1065 99.10    5
 1066 100.00    5
 1067 100.00    5
 1068 100.00    5
 1069 100.00    5
 1070 100.00    5
 1071 100.00    5
 1072 100.00    5
 1073 100.00    5
 1074 100.00    5
 1075 100.00    5
 1076 100.00    5
 1077 100.00    5
 1078 100.00    5
 1079 100.00    5
 1080 100.00    5
 1081 100.00    5
 1082 100.00    5
 1083 100.00    5
 1084 100.00    5
 1085 100.00    5
 1086 100.00    5
 1087 100.00    5
 1088 100.00    5
 1089 100.00    5
 1090 100.00    5
 1091 100.00    5
 1092 100.00    5
 1093 100.00    5
 1094 100.00    5
 1095 100.00    5
 1096 100.00    5
 1097 101.00    5
 1098 102.00    5
 1099 103.00    5
 1100 104.00    5
 1101 105.00    5
 1102 106.00    5
 1103 107.00    5
 1104 108.00    5
 1105 109.00    5
 1106 110.00    5
 1107 111.00    5
 1108 112.00    5
 1109 113.00    5
 1110 114.00    5
 1111 115.00    5
 1112 116.00    5
 1113 117.00    5
 1114 118.00    5
 1115 119.00    5
 1116 120.00    5
 1117 120.00    5
 1118 120.00    5
 1119 120.00    5
 1120 120.00    5
 1121 120.00    5
 1122 120.00    5
 1123 120.00    5
 1124 120.00    5
 1125 120.00    5
 1126 120.00    5
 1127 117.50    5
 1128 115.00    5
 1129 112.50    5
 1130 110.00    5
 1131 107.50    5
 1132 105.00    5
 1133 102.50    5
 1134 100.00    5
 1135 97.50    5
 1136 95.00    5
 1137 92.50    5
 1138 90.00    5
 1139 87.50    5
 1140 85.00    5
 1141 82.50    5
 1142 80.00    5
 1143 76.20    5
 1144 72.50    5
 1145 68.80    5
 1146 65.00    5
 1147 61.20    5
 1148 57.50    5
 1149 53.80    5
 1150 50.00    4
 1151 45.00    4
 1152 40.00    4
 1153 35.00    3
 1154 30.00    3
 1155 25.00    3
 1156 20.00    2
 1157 15.00    2
 1158 10.00    0
 1159  5.00    0
 1160  0.00    0
 1161  0.00    0
 1162  0.00    0
 1163  0.00    0
 1164  0.00    0
 1165  0.00    0
 1166  0.00    0
 1167  0.00    0
 1168  0.00    0
 1169  0.00    0
 1170  0.00    0
 1171  0.00    0
 1172  0.00    0
 1173  0.00    0
 1174  0.00    0
 1175  0.00    0
 1176  0.00    0
 1177  0.00    0
 1178  0.00    0
 1179  0.00    0
 1180  0.00    0];
  
CYCL.temps=A(:,1);
CYCL.temps=CYCL.temps';
CYCL.vitesse=A(:,2)./3.6;
CYCL.vitesse=CYCL.vitesse';
CYCL.rappvit=A(:,3);

clear A;

display('Fin d''initialisation des parametres cinematiques'); 

%------------------------------------MISES A JOUR-----------------------------------%

%   modifi� le 15/4/2002 pour VEHLIB
%   Modif pour VEHIL: Anticipation des chgts de rapport de 1 sec.
%   valide le 5 mai 04

