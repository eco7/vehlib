% File: CIN_ECE15_BV
% CYCLE ece avec paliers de changement de rapport de boite
%
%  © Copyright IFSTTAR LTE 1999-2011
%
% et rapports de boite imposes
% Le 27/6/00 compatible avec VEHLIB

% Reperage du type de cycle de conduite

CYCL.ntypcin=3;	% cinematique vitesse-temps

CYCL.recharge=0;	% Recharge non dispo aujourd'hui
CYCL.pente=0;		% Pente en %



A=[  0  0.00  0
    1  0.00  0
    2  0.00  0
    3  0.00  0
    4  0.00  0
    5  0.00  0
    6  0.00  0
    7  0.00  0
    8  0.00  0
    9  0.00  0
    10  0.00 0
    11  0.00 1
    12  1.04 1
    13  2.08 1
    14  3.13 1
    15  4.17 1
    16  4.17 1
    17  4.17 1
    18  4.17 1
    19  4.17  1
    20  4.17  1
    21  4.17  1
    22  4.17  1
    23  4.17 1
    24  3.47 1
    25  2.78 1
    26  1.85 1
    27  0.93 0
    28  0.00  0
    29  0.00  0
    30  0.00  0
    31  0.00  0
    32  0.00  0
    33  0.00  0
    34  0.00  0
    35  0.00  0
    36  0.00  0
    37  0.00  0
    38  0.00  0
    39  0.00  0
    40  0.00  0
    41  0.00  0
    42  0.00  0
    43  0.00  0
    44  0.00  0
    45  0.00  0
    46  0.00  0
    47  0.00  0
    48  0.00 0
    49  0.00 1
    50  0.83 1
    51  1.67 1
    52  2.50 1
    53  3.33  1
    54  4.17  2
    55  4.17  2
    56  4.17 2
    57  5.11  2
    58  6.06  2
    59  7.00 2
    60  7.94 2
    61  8.89 2
    62  8.89 2
    63  8.89 2
    64  8.89 2
    65  8.89 2
    66  8.89 2
    67  8.89 2
    68  8.89  2
    69  8.89  2
    70  8.89 2
    71  8.89 2
    72  8.89 2
    73  8.89  2
    74  8.89  2
    75  8.89 2
    76  8.89  2
    77  8.89 2
    78  8.89 2
    79  8.89  2
    80  8.89  2
    81  8.89 2
    82  8.89 2
    83  8.89 2
    84  8.89 2
    85  8.89 2
    86  8.13 2
    87  7.36 2
    88  6.60 2
    89  5.83 2
    90  5.07 2
    91  4.31 2
    92  3.54 2
    93  2.78 0
    94  1.85 0
    95  0.93 0
    96  0.00 0
    97  0.00 0
    98  0.00  0
    99  0.00  0
    100  0.00  0
    101  0.00  0
    102  0.00  0
    103  0.00  0
    104  0.00  0
    105  0.00  0
    106  0.00  0
    107  0.00  0
    108  0.00  0
    109  0.00  0
    110  0.00  0
    111  0.00  0
    112  0.00  0
    113  0.00  0
    114  0.00  0
    115  0.00  0
    116  0.00 0
    117  0.00 1
    118  0.83 1
    119  1.67 1
    120  2.50  1
    121  3.33 1
    122  4.17  2
    123  4.17 2
    124  4.17 2
    125  4.78 2
    126  5.40  2
    127  6.02  2
    128  6.64 2
    129  7.25  2
    130  7.87 2
    131  8.49  2
    132  9.10  2
    133  9.72 3
    134  9.72  3
    135  9.72 3
    136 10.24 3
    137 10.76 3
    138 11.28  3
    139 11.81  3
    140 12.33  3
    141 12.85  3
    142 13.37  3
    143 13.89  3
    144 13.89  3
    145 13.89 3
    146 13.89  3
    147 13.89 3
    148 13.89  3
    149 13.89  3
    150 13.89  3
    151 13.89  3
    152 13.89  3
    153 13.89  3
    154 13.89  3
    155 13.89 3
    156 13.37 3
    157 12.85 3
    158 12.33 3
    159 11.81 3
    160 11.28 3
    161 10.76 3
    162 10.24 3
    163  9.72 3
    164  9.72 3
    165  9.72 3
    166  9.72 3
    167  9.72  3
    168  9.72  3
    169  9.72 3
    170  9.72 3
    171  9.72 3
    172  9.72 3
    173  9.72 3
    174  9.72 3
    175  9.72 3
    176  9.72 3
    177  9.31 3
    178  8.89 3
    179  8.02 2
    180  7.14 2
    181  6.27 2
    182  5.40 2
    183  4.52 2
    184  3.65 2
    185  2.78 2
    186  1.85 0
    187  0.93 0
    188  0.00 0
    189  0.00 0
    190  0.00 0
    191  0.00 0
    192  0.00 0
    193  0.00 0
    194  0.00 0
    195  0.00 0];


CYCL.temps=A(:,1);
CYCL.temps=CYCL.temps';
CYCL.vitesse=A(:,2);
CYCL.vitesse=CYCL.vitesse';
CYCL.rappvit=A(:,3);
