% File: CIN_STAB50_perturb_3eme
%%    fichier cin�matique
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%%   cycle R1 INRETS 
%%   modifi� le 31/12/98 pour VEHLIB

CYCL.ntypcin=3;	% cinematique vitesse-temps

CYCL.recharge=0;	% Recharge non dispo aujourd'hui
CYCL.pente=0;		% Pente en %


A=  [0.00     0.00 0
      1      0  1
     10      25 1
     20      40 2
     30      50 3
     50      50 3
     50.1    55 3
     55      55 3
     55.1    45 3
     60      45 3
     61      50 3
     200     50 3
     600     50 3];

CYCL.temps=A(:,1);
CYCL.temps=CYCL.temps';
CYCL.vitesse=A(:,2)./3.6;
CYCL.vitesse=CYCL.vitesse';
CYCL.rappvit=A(:,3);
