% File: CIN_STAB30
%%    fichier cin�matique
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%%   cycle R1 INRETS 
%%   modifi� le 31/12/98 pour VEHLIB


CYCL.ntypcin=1;	% cinematique vitesse-temps

CYCL.recharge=0;	% Recharge non dispo aujourd'hui
CYCL.pente=0;		% Pente en %


A=  [0.00     0.00
     1       30
     100     30];

CYCL.temps=A(:,1);
CYCL.temps=CYCL.temps';
CYCL.vitesse=A(:,2)./3.6;
CYCL.vitesse=CYCL.vitesse';
