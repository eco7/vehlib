% File: CIN_ACCEL_MAX
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier cinematique
%                      Pour librairie VEHLIB
%                          INRETS - LTE 
%  Objet: Ce script initialise la structure CYCL
%          contenant les champs necessaires pour renseigner 
%          la cinematique du vehicule pour la librairie VEHLIB.
%
%          Cycle pour performances maximums des vehicules
% =============================================================================


%reperage du type de cinematique
CYCL.ntypcin=1;	% cinematique vitesse-temps

CYCL.recharge=0;	% Recharge non dispo aujourd'hui
CYCL.pente=0;		% Pente en %


A=  [0.00     0.00 
     1       200 
     100     200 ];

CYCL.temps=A(:,1);
CYCL.temps=CYCL.temps';
CYCL.vitesse=A(:,2)./3.6;
CYCL.vitesse=CYCL.vitesse';

display('Fin d''initialisation des parametres cinematique'); 

%------------------------------------MISES A JOUR-----------------------------------%
