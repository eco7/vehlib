% File: CIN_STAB50_perturb_5eme
%%    fichier cin�matique
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%%   cycle R1 INRETS 
%%   modifi� le 31/12/98 pour VEHLIB

CYCL.ntypcin=3;	% cinematique vitesse-temps

CYCL.recharge=0;	% Recharge non dispo aujourd'hui
CYCL.pente=0;		% Pente en %


A=  [0.00     0.00 0
      1      0  1
      9      25 1
     10      25 2
     19      40 2
     20      40 3
     30      50 5
     50      50 5
     50.1    55 5
     55      55 5
     55.1    45 5
     60      45 5
     61      50 5
     200     50 5
     600     50 5];

CYCL.temps=A(:,1);
CYCL.temps=CYCL.temps';
CYCL.vitesse=A(:,2)./3.6;
CYCL.vitesse=CYCL.vitesse';
CYCL.rappvit=A(:,3);
