% File: CIN_STAB70
%%    fichier cin�matique
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%%   cycle R1 INRETS 
%%   modifi� le 31/12/98 pour VEHLIB


CYCL.ntypcin=3;	% cinematique vitesse-temps

CYCL.recharge=0;	% Recharge non dispo aujourd'hui
CYCL.pente=0;		% Pente en %


A=  [0.00     0.00 0
     1       70 1
     5       70 2
     10       70 3
     15       70 4
     20       70 5
     200     70 5
     600     70 5];

CYCL.temps=A(:,1);
CYCL.temps=CYCL.temps';
CYCL.vitesse=A(:,2)./3.6;
CYCL.vitesse=CYCL.vitesse';
CYCL.rappvit=A(:,3);
