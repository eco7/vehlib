%%    fichier cin�matique
%%   Partie urbaine du cycle normalise europeen 
%%   sans rapport de boite de vitesse 
%%   modifi� le 15/4/2002 pour VEHLIB


CYCL.ntypcin=3;	% cinematique vitesse-temps

CYCL.recharge=0;	% Recharge non dispo aujourd'hui
CYCL.pente=0;		% Pente en %


A=[  0.0      0.00   1
     1.0      0.00   1
     2.0      0.00   1
     3.0      3.75   1
     4.0      7.50   1
     5.0     11.25   1
     6.0     15.00   1
     7.0     15.00   1
     8.0     15.00   1
     9.0     12.00   1
     10.0     9.00   1
     11.0     6.00   1
     12.0     3.00   1
     13.0     0.00   1
     14.00    0.00   1   ];
  
CYCL.temps=[A(:,1)];
CYCL.temps=CYCL.temps';
CYCL.vitesse=A(:,2)./3.6;
CYCL.vitesse=CYCL.vitesse';
CYCL.rappvit=A(:,3);

