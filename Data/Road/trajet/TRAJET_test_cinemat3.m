% File: TRAJET_test_cinemat3
% =============================================================================
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
% 
%                Initialisation du fichier cycle
%                      Pour librairie VEHLIB
%                    INRETS - LTE Fevrier 2005
%
%  Objet: Ce script initialise la structure CYCL
%          contenant les champs necessaires a un calcul
%          pour la bibliotheque vehlib. 
%   Ici, trajet regroupant une liste de fichiers type 3 (vitesse-temps-rapport de boite).
%
% =============================================================================

% Identification du type de trajet
CYCL.ntyptrajet=3;

% Charge de la batterie en fin de cycle (1:valide; 0:non effectuee)
CYCL.recharge=0;

% liste des fichiers avec leur chemin complet
CYCL.ligne_trajet={ ... 
'CIN_ECE15_BV';
'CIN_ECE15_BV';
'CIN_ECE15_BV';
      };

 % Sens du profil de pente pour parcourir le trajet :
CYCL.sens_profils_pente={ ...
%       1;
%       1;
%       1;
%       1;
%       1;
%       1;
%       1;
%       1;
%       1;
      1;
      1;
      1;
     };

% 1. Les sens du profil de vitesse et celui de la pente sont dissocies.
%
% 2. Le profil de la reference de vitesse tient compte du sens defini
%   ci-dessus par la var "CYCL.sens_profils_vitesse" (1 - pour l'aller, 
%   cella veut dire que les stations sont parcourues dans l'ordre definie 
%   par "CYCL.ligne_trajet" et donc dans le sens des Pk croissantes - 0
%   pour le retour).
%
% 3. Le sens de pourcour du profil de la pente sera impos� par la PREMIERE 
%   valeur de la variable "CYCL.sens_profils_pente", soit
%   CYCL.sens_profils_pente{1}, pour "ntyppente=2" (soit la pente en
%   fonction des PK). Les valeurs de "CYCL.AllerRetour" de chaque station
%   seront ignor�es.
%
% 4. Le sens de pourcour du profil de la pente sera impos� par la variable
%   "CYCL.sens_profils_pente",  pour "ntyppente=1" (soit la pente en
%   fonction de la distance, la variable ).
%
% 5. La valeur de la pente pour le trajet entier est fix� par la PREMIERE
%   valeur de la variable "CYCL.sens_profils_pente", soit
%   CYCL.sens_profils_pente{1}, pour "ntyppente=0" (pente constante).