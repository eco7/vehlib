%%    fichier cinematique :CIN_URBAIN_PRIUS
%%   cycle sur route realise avec la Prius II 
% Essais 78 du 27-06-07 a 14h07 pour la vitesse
% Pente generee ?? partir des mesures de pentes gps bas cout des essais '53','55','57','59','61','76','78','80'

CYCL.ntypcin=1;	% cinematique vitesse-temps
CYCL.recharge=0;	% Recharge non dispo aujourd'hui

CYCL.ntyppente=3;	% cinematique pente-distance

% Temps         vitesse  
A = [
       0.0         0.0    
     1.000         0.0    
     2.000         0.0    
     3.000         0.0    
     4.000         0.0    
     5.000         0.0    
     6.000         0.0    
     7.000         0.0    
     8.000         0.0    
     9.000         0.0    
    10.000       0.698    
    11.000       2.960    
    12.000       5.222    
    13.000       7.483    
    14.000       9.745    
    15.000      12.023    
    16.000      12.037    
    17.000      12.033    
    18.000      12.245    
    19.000      13.127    
    20.000      15.212    
    21.000      17.752    
    22.000      19.817    
    23.000      21.735    
    24.000      22.762    
    25.000      22.512    
    26.000      22.955    
    27.000      24.193    
    28.000      22.595    
    29.000      18.018    
    30.000      16.120    
    31.000      14.221    
    32.000      15.100    
    33.000      17.613    
    34.000      24.707    
    35.000      31.195    
    36.000      36.785    
    37.000      40.675    
    38.000      43.032    
    39.000      44.405    
    40.000      44.683    
    41.000      44.577    
    42.000      45.515    
    43.000      45.228    
    44.000      42.808    
    45.000      41.205    
    46.000      39.657    
    47.000      37.575    
    48.000      34.660    
    49.000      30.470    
    50.000      24.335    
    51.000      21.284    
    52.000      20.320    
    53.000      21.194    
    54.000      22.067    
    55.000      26.000    
    56.000      28.808    
    57.000      29.008    
    58.000      28.567    
    59.000      27.150    
    60.000      25.998    
    61.000      24.846    
    62.000      25.150    
    63.000      28.827    
    64.000      31.637    
    65.000      33.300    
    66.000      36.665    
    67.000      40.237    
    68.000      42.920    
    69.000      44.362    
    70.000      46.012    
    71.000      47.397    
    72.000      48.390    
    73.000      49.122    
    74.000      50.820    
    75.000      50.363    
    76.000      49.107    
    77.000      48.214    
    78.000      47.406    
    79.000      45.841    
    80.000      43.198    
    81.000      39.212    
    82.000      35.640    
    83.000      33.107    
    84.000      30.570    
    85.000      29.691    
    86.000      28.812    
    87.000      29.680    
    88.000      31.525    
    89.000      31.627    
    90.000      30.847    
    91.000      31.558    
    92.000      32.147    
    93.000      32.228    
    94.000      32.013    
    95.000      30.873    
    96.000      27.540    
    97.000      26.850    
    98.000      28.418    
    99.000      29.987    
   100.000      33.115    
   101.000      34.643    
   102.000      37.081    
   103.000      39.423    
   104.000      40.508    
   105.000      41.382    
   106.000      42.523    
   107.000      43.333    
   108.000      43.968    
   109.000      44.102    
   110.000      43.338    
   111.000      43.283    
   112.000      42.987    
   113.000      41.670    
   114.000      40.273    
   115.000      38.847    
   116.000      38.072    
   117.000      39.545    
   118.000      40.333    
   119.000      41.113    
   120.000      41.852    
   121.000      42.827    
   122.000      43.145    
   123.000      43.275    
   124.000      43.603    
   125.000      43.197    
   126.000      43.516    
   127.000      42.880    
   128.000      40.233    
   129.000      36.693    
   130.000      33.590    
   131.000      31.410    
   132.000      27.073    
   133.000      22.923    
   134.000      22.198    
   135.000      21.473    
   136.000      22.758    
   137.000      26.312    
   138.000      30.165    
   139.000      32.997    
   140.000      36.060    
   141.000      39.325    
   142.000      41.506    
   143.000      42.417    
   144.000      44.180    
   145.000      45.212    
   146.000      45.738    
   147.000      45.947    
   148.000      45.563    
   149.000      44.888    
   150.000      44.602    
   151.000      44.048    
   152.000      42.318    
   153.000      37.757    
   154.000      31.717    
   155.000      24.955    
   156.000      18.237    
   157.000      12.485    
   158.000       8.125    
   159.000       3.687    
   160.000         0.0    
   161.000         0.0    
   162.000         0.0    
   163.000         0.0    
   164.000         0.0    
   165.000         0.0    
   166.000         0.0    
   167.000         0.0    
   168.000         0.0    
   169.000         0.0    
   170.000         0.0    
   171.000         0.0    
   172.000         0.0    
   173.000         0.0    
   174.000         0.0    
   175.000         0.0    
   176.000         0.0    
   177.000         0.0    
   178.000         0.0    
   179.000         0.0    
   180.000         0.0    
   181.000         0.0    
   182.000         0.0    
   183.000         0.0    
   184.000         0.0    
   185.000         0.0    
   186.000         0.0    
   187.000         0.0    
   188.000         0.0    
   189.000         0.0    
   190.000       0.203    
   191.000       5.103    
   192.000      11.368    
   193.000      16.113    
   194.000      20.358    
   195.000      23.437    
   196.000      27.150    
   197.000      31.140    
   198.000      34.362    
   199.000      37.146    
   200.000      39.827    
   201.000      41.155    
   202.000      41.790    
   203.000      42.192    
   204.000      43.108    
   205.000      43.980    
   206.000      45.560    
   207.000      46.417    
   208.000      45.875    
   209.000      44.487    
   210.000      43.333    
   211.000      42.515    
   212.000      42.378    
   213.000      40.598    
   214.000      39.297    
   215.000      37.387    
   216.000      32.927    
   217.000      27.820    
   218.000      21.248    
   219.000      13.720    
   220.000       6.508    
   221.000       1.198    
   222.000         0.0    
   223.000         0.0    
   224.000         0.0    
   225.000         0.0    
   226.000         0.0    
   227.000         0.0    
   228.000         0.0    
   229.000         0.0    
   230.000         0.0    
   231.000         0.0    
   232.000         0.0    
   233.000         0.0    
   234.000         0.0    
   235.000         0.0    
   236.000         0.0    
   237.000         0.0    
   238.000         0.0    
   239.000         0.0    
   240.000         0.0    
   241.000         0.0    
   242.000         0.0    
   243.000         0.0    
   244.000         0.0    
   245.000         0.0    
   246.000         0.0    
   247.000         0.0    
   248.000         0.0    
   249.000         0.0    
   250.000       0.226    
   251.000       6.053    
   252.000      13.703    
   253.000      18.312    
   254.000      21.552    
   255.000      24.230    
   256.000      24.598    
   257.000      22.975    
   258.000      22.448    
   259.000      20.943    
   260.000      18.107    
   261.000      14.910    
   262.000      13.445    
   263.000      12.200    
   264.000      11.428    
   265.000      10.153    
   266.000       8.879    
   267.000       6.183    
   268.000       4.915    
   269.000       4.285    
   270.000       3.888    
   271.000       3.445    
   272.000       2.725    
   273.000       1.668    
   274.000       3.295    
   275.000       3.470    
   276.000       3.715    
   277.000       3.384    
   278.000       1.572    
   279.000         0.0    
   280.000         0.0    
   281.000         0.0    
   282.000         0.0    
   283.000         0.0    
   284.000         0.0    
   285.000         0.0    
   286.000         0.0    
   287.000         0.0    
   288.000         0.0    
   289.000         0.0    
   290.000         0.0    
   291.000         0.0    
   292.000         0.0    
   293.000         0.0    
   294.000         0.0    
   295.000         0.0    
   296.000         0.0    
   297.000         0.0    
   298.000         0.0    
   299.000         0.0    
   300.000         0.0    
   301.000         0.0    
   302.000         0.0    
   303.000         0.0    
   304.000         0.0    
   305.000         0.0    
   306.000         0.0    
   307.000         0.0    
   308.000         0.0    
   309.000         0.0    
   310.000         0.0    
   311.000         0.0    
   312.000         0.0    
   313.000         0.0    
   314.000         0.0    
   315.000         0.0    
   316.000         0.0    
   317.000         0.0    
   318.000         0.0    
   319.000         0.0    
   320.000         0.0    
   321.000         0.0    
   322.000         0.0    
   323.000         0.0    
   324.000         0.0    
   325.000         0.0    
   326.000         0.0    
   327.000         0.0    
   328.000         0.0    
   329.000         0.0    
   330.000         0.0    
   331.000       0.503    
   332.000       3.958    
   333.000       8.992    
   334.000      13.687    
   335.000      17.533    
   336.000      21.295    
   337.000      22.665    
   338.000      22.588    
   339.000      21.588    
   340.000      21.462    
   341.000      20.725    
   342.000      19.513    
   343.000      16.940    
   344.000      14.000    
   345.000      11.060    
   346.000       6.943    
   347.000       3.528    
   348.000       1.313    
   349.000         0.0    
   350.000         0.0    
   351.000         0.0    
   352.000         0.0    
   353.000         0.0    
   354.000         0.0    
   355.000         0.0    
   356.000         0.0    
   357.000         0.0    
   358.000         0.0    
   359.000         0.0    
   360.000         0.0    
   361.000         0.0    
   362.000         0.0    
   363.000         0.0    
   364.000         0.0    
   365.000         0.0    
   366.000         0.0    
   367.000         0.0    
   368.000         0.0    
   369.000         0.0    
   370.000         0.0    
   371.000         0.0    
   372.000         0.0    
   373.000         0.0    
   374.000         0.0    
   375.000         0.0    
   376.000         0.0    
   377.000         0.0    
   378.000         0.0    
   379.000         0.0    
   380.000         0.0    
   381.000         0.0    
   382.000         0.0    
   383.000         0.0    
   384.000         0.0    
   385.000         0.0    
   386.000         0.0    
   387.000         0.0    
   388.000         0.0    
   389.000         0.0    
   390.000         0.0    
   391.000         0.0    
   392.000         0.0    
   393.000         0.0    
   394.000         0.0    
   395.000         0.0    
   396.000         0.0    
   397.000         0.0    
   398.000         0.0    
   399.000         0.0    
   400.000         0.0    
   401.000         0.0    
   402.000         0.0    
   403.000         0.0    
   404.000         0.0    
   405.000         0.0    
   406.000         0.0    
   407.000         0.0    
   408.000         0.0    
   409.000         0.0    
   410.000       6.318    
   411.000      13.215    
   412.000      19.383    
   413.000      22.838    
   414.000      24.717    
   415.000      26.142    
   416.000      28.845    
   417.000      31.452    
   418.000      32.530    
   419.000      31.842    
   420.000      31.982    
   421.000      31.228    
   422.000      30.937    
   423.000      29.477    
   424.000      28.273    
   425.000      26.886    
   426.000      25.493    
   427.000      22.695    
   428.000      22.085    
   429.000      21.474    
   430.000      22.322    
   431.000      25.463    
   432.000      28.740    
   433.000      32.522    
   434.000      36.412    
   435.000      38.648    
   436.000      39.415    
   438.000      39.455    
   439.000      38.463    
   440.000      36.895    
   441.000      37.014    
   442.000      39.065    
   443.000      40.893    
   444.000      40.542    
   446.000      38.277    
   447.000      37.140    
   448.000      35.722    
   449.000      35.289    
   450.000      34.218    
   451.000      30.283    
   452.000      22.290    
   454.000       4.273    
   455.000       1.602    
   456.000         0.0    
   457.000         0.0    
   458.000         0.0    
   459.000         0.0    
   460.000         0.0    
   461.000         0.0    
   462.000         0.0    
   463.000         0.0    
   464.000         0.0    
   465.000         0.0    
   466.000         0.0    
   467.000         0.0    
   468.000         0.0    
   469.000         0.0    
   470.000         0.0    
   471.000         0.0    
   472.000         0.0    
   473.000         0.0    
   474.000         0.0    
   475.000         0.0    
   476.000         0.0    
   477.000         0.0    
   478.000         0.0    
   479.000         0.0    
   480.000         0.0    
   481.000         0.0    
   482.000         0.0    
   483.000         0.0    
   484.000         0.0    
   485.000         0.0    
   486.000         0.0    
   487.000       0.830    
   488.000       4.290    
   489.000      12.177    
   490.000      17.448    
   491.000      22.207    
   492.000      23.297    
   493.000      24.387    
   494.000      23.602    
   495.000      22.625    
   496.000      22.053    
   497.000      18.672    
   498.000      15.728    
   499.000      14.360    
   500.000      15.914    
   501.000      17.468    
   502.000      21.433    
   503.000      24.712    
   504.000      28.953    
   505.000      32.348    
   506.000      35.438    
   507.000      36.525    
   508.000      35.582    
   509.000      35.783    
   511.000      35.157    
   512.000      34.003    
   513.000      33.693    
   514.000      33.635    
   515.000      33.907    
   516.000      34.047    
   517.000      35.070    
   518.000      35.647    
   519.000      37.405    
   520.000      39.487    
   521.000      40.377    
   522.000      41.418    
   523.000      41.173    
   524.000      41.453    
   525.000      42.288    
   526.000      42.882    
   527.000      43.693    
   528.000      44.053    
   529.000      44.657    
   530.000      44.883    
   532.000      43.058    
   533.000      43.502    
   534.000      43.697    
   535.000      42.880    
   536.000      41.075    
   537.000      37.695    
   538.000      34.032    
   539.000      28.903    
   540.000      23.332    
   541.000      22.137    
   542.000      20.942    
   543.000      21.438    
   544.000      25.982    
   545.000      30.398    
   546.000      33.905    
   547.000      35.072    
   548.000      36.955    
   549.000      38.552    
   550.000      39.280    
   551.000      38.830    
   552.000      37.360    
   553.000      36.837    
   554.000      36.025    
   555.000      34.817    
   556.000      32.470    
   557.000      29.943    
   558.000      27.700    
   559.000      26.208    
   560.000      27.944    
   561.000      29.473    
   562.000      28.065    
   563.000      23.707    
   564.000      17.873    
   565.000      12.480    
   566.000      11.990    
   567.000      11.501    
   568.000      13.168    
   569.000      18.790    
   570.000      24.247    
   571.000      28.511    
   572.000      31.050    
   573.000      31.228    
   574.000      31.185    
   575.000      30.147    
   576.000      28.740    
   577.000      25.978    
   578.000      22.798    
   579.000      21.150    
   580.000      19.501    
   581.000      21.110    
   582.000      23.288    
   583.000      25.760    
   584.000      28.423    
   585.000      28.812    
   586.000      27.602    
   587.000      26.222    
   588.000      25.057    
   589.000      26.052    
   590.000      27.047    
   591.000      29.945    
   592.000      32.735    
   593.000      33.677    
   594.000      31.953    
   595.000      30.113    
   596.000      26.010    
   597.000      19.793    
   598.000      18.187    
   599.000      16.580    
   600.000      16.885    
   601.000      19.118    
   602.000      22.582    
   603.000      26.288    
   604.000      29.263    
   605.000      30.888    
   606.000      30.825    
   607.000      29.648    
   608.000      23.895    
   609.000      18.237    
   610.000      17.018    
   611.000      18.533    
   612.000      20.048    
   613.000      24.940    
   614.000      29.613    
   615.000      30.174    
   616.000      30.735    
   617.000      29.870    
   618.000      28.304    
   619.000      25.743    
   620.000      22.032    
   621.000      16.018    
   622.000       9.574    
   623.000       3.889    
   624.000       0.293    
   625.000         0.0    
   626.000         0.0    
   627.000         0.0    
   628.000       0.453    
   629.000       3.487    
   630.000       4.063    
   631.000       5.141    
   632.000       6.218    
   633.000       6.869    
   634.000       7.520    
   635.000      12.087    
   636.000      16.685    
   637.000      18.595    
   638.000      17.733    
   639.000      14.020    
   640.000       7.715    
   641.000       6.070    
   642.000       0.245    
   643.000       0.245    
   644.000         0.0    
   645.000         0.0    
   646.000         0.0    
   647.000         0.0    
   648.000         0.0    
   649.000         0.0    
   650.000         0.0    
   651.000         0.0    
   652.000         0.0    
   653.000         0.0    
   654.000         0.0    
   655.000         0.0    
   656.000         0.0    
   657.000         0.0    
   658.000         0.0    
   659.000         0.0    
   660.000         0.0    
   661.000         0.0    
   662.000         0.0    
   663.000         0.0    
   664.000         0.0    
   665.000         0.0    
   666.000         0.0    
   667.000         0.0    
   668.000         0.0    ];
CYCL.temps=A(:,1);
CYCL.temps=CYCL.temps';
CYCL.vitesse=A(:,2)./3.6;
CYCL.vitesse=CYCL.vitesse';

% distance                 pente  
B = [
     1.000         0.0    
     7.110      -0.151    
    13.220      -0.303    
    19.329       0.240    
    25.439       0.486    
    31.549       0.779    
    37.659       0.877    
    43.768       0.810    
    49.878       0.393    
    55.988      -0.276    
    62.098      -1.105    
    68.208      -2.021    
    74.317      -3.170    
    80.427      -3.786    
    86.537      -3.638    
    92.647      -3.270    
    98.756      -3.117    
   104.866      -3.068    
   110.976      -2.814    
   117.086      -2.995    
   123.195      -3.245    
   129.305      -3.260    
   135.415      -3.348    
   141.525      -3.838    
   147.635      -4.312    
   153.744      -4.444    
   159.854      -4.448    
   165.964      -4.448    
   172.074      -4.445    
   178.183      -4.489    
   184.293      -4.504    
   190.403      -4.401    
   196.513      -4.323    
   202.623      -4.487    
   208.732      -4.722    
   214.842      -4.754    
   220.952      -4.640    
   227.062      -4.581    
   233.171      -4.683    
   239.281      -4.816    
   245.391      -4.881    
   251.501      -5.694    
   257.610      -6.506    
   263.720      -5.924    
   269.830      -5.585    
   275.940      -5.472    
   282.050      -5.162    
   288.159      -4.679    
   294.269      -4.483    
   300.379      -4.315    
   306.489      -4.275    
   312.598      -4.675    
   318.708      -4.879    
   324.818      -4.382    
   330.928      -3.927    
   337.038      -3.783    
   343.147      -3.425    
   349.257      -2.737    
   355.367      -1.916    
   361.477      -1.391    
   367.586      -1.232    
   373.696      -1.291    
   379.806      -1.520    
   385.916      -1.792    
   392.025      -2.021    
   398.135      -2.201    
   404.245      -2.322    
   410.355      -2.322    
   416.465      -2.203    
   422.574      -2.009    
   428.684      -1.824    
   434.794      -1.679    
   440.904      -1.591    
   447.013      -1.514    
   453.123      -1.406    
   459.233      -1.377    
   465.343      -1.381    
   471.453      -1.399    
   477.562      -1.482    
   483.672      -1.586    
   489.782      -1.584    
   495.892      -1.511    
   502.001      -1.496    
   508.111      -1.439    
   514.221      -1.331    
   520.331      -1.229    
   526.440      -1.128    
   532.550      -1.052    
   538.660      -0.964    
   544.770      -0.861    
   550.880      -0.755    
   556.989      -0.724    
   563.099      -0.824    
   569.209      -0.938    
   575.319      -1.030    
   581.428      -1.105    
   587.538      -0.973    
   593.648      -0.705    
   599.758      -0.448    
   605.868      -0.540    
   611.977      -0.295    
   618.087      -0.050    
   624.197      -1.004    
   630.307      -1.958    
   636.416      -2.883    
   642.526      -3.809    
   648.636      -3.781    
   654.746      -3.668    
   660.855      -3.440    
   666.965      -3.292    
   673.075      -3.207    
   679.185      -3.240    
   685.295      -3.944    
   691.404      -4.648    
   697.514      -4.336    
   703.624      -3.821    
   709.734      -3.678    
   715.843      -3.462    
   721.953      -3.334    
   728.063      -3.823    
   734.173      -4.013    
   740.283      -3.607    
   746.392      -3.440    
   752.502      -3.183    
   758.612      -2.781    
   764.722      -2.319    
   770.831      -1.811    
   776.941      -1.603    
   783.051      -1.361    
   789.161      -0.999    
   795.270      -0.790    
   801.380      -0.621    
   807.490      -0.543    
   813.600      -0.508    
   819.710      -0.565    
   825.819      -0.654    
   831.929      -0.682    
   838.039      -0.679    
   844.149      -0.757    
   850.258      -0.992    
   856.368      -1.266    
   862.478      -1.354    
   868.588      -1.415    
   874.698      -1.399    
   880.807      -1.314    
   886.917      -1.370    
   893.027      -1.443    
   899.137      -1.456    
   905.246      -1.316    
   911.356      -1.234    
   917.466      -1.584    
   923.576      -2.012    
   929.685      -2.218    
   935.795      -2.283    
   941.905      -2.443    
   948.015      -3.078    
   954.125      -2.406    
   960.234      -1.734    
   966.344      -1.180    
   972.454      -0.921    
   978.564      -1.120    
   984.673      -1.384    
   990.783      -1.364    
   996.893      -1.701    
   1003.00      -2.159    
   1009.11      -1.703    
   1015.22      -1.248    
   1021.33      -0.891    
   1027.44      -0.860    
   1033.55      -0.822    
   1039.66      -0.774    
   1045.77      -0.719    
   1051.88      -0.700    
   1057.99      -0.689    
   1064.10      -0.761    
   1070.21      -0.892    
   1076.32      -0.877    
   1082.43      -0.747    
   1088.54      -0.645    
   1094.65      -0.545    
   1100.76      -0.460    
   1106.87      -0.121    
   1112.98       0.261    
   1119.09       0.370    
   1125.20       0.521    
   1131.31       0.624    
   1137.42       0.557    
   1143.53       0.445    
   1149.64       0.325    
   1155.75       0.335    
   1161.86       0.409    
   1167.97       0.571    
   1174.08       1.065    
   1180.19       1.450    
   1186.30       1.529    
   1192.41       1.531    
   1198.52       1.446    
   1204.63       1.063    
   1210.74       0.685    
   1216.84       0.511    
   1222.95       0.365    
   1229.06      -0.145    
   1235.17      -0.645    
   1241.28      -0.604    
   1247.39      -0.548    
   1253.50      -0.799    
   1259.61      -0.923    
   1265.72      -0.671    
   1271.83      -0.572    
   1277.94      -0.619    
   1284.05      -0.755    
   1290.16      -0.829    
   1296.27      -0.901    
   1302.38      -1.082    
   1308.49      -1.247    
   1314.60      -1.396    
   1320.71      -1.477    
   1326.82      -1.638    
   1332.93      -1.718    
   1339.04      -1.110    
   1345.15      -0.759    
   1351.26      -1.399    
   1357.37      -1.360    
   1363.48      -0.690    
   1369.59      -1.601    
   1375.70      -1.499    
   1381.81      -0.209    
   1387.92      -0.169    
   1394.03      -0.362    
   1400.14      -0.701    
   1406.25      -0.860    
   1412.36      -0.826    
   1418.47      -0.873    
   1424.58      -1.036    
   1430.69      -1.180    
   1436.80      -1.179    
   1442.91      -1.041    
   1449.02      -0.937    
   1455.13      -0.918    
   1461.24      -0.422    
   1467.35      -0.903    
   1473.46      -1.682    
   1479.57      -1.527    
   1485.67      -1.559    
   1491.78      -1.630    
   1497.89      -1.705    
   1504.00      -1.747    
   1510.11      -1.717    
   1516.22      -1.641    
   1522.33      -1.565    
   1528.44      -1.476    
   1534.55      -1.312    
   1540.66      -1.124    
   1546.77      -1.013    
   1552.88      -0.881    
   1558.99      -0.609    
   1565.10      -0.326    
   1571.21      -0.225    
   1577.32      -0.172    
   1583.43      -0.107    
   1589.54      -0.179    
   1595.65      -0.424    
   1601.76      -0.910    
   1607.87      -1.395    
   1613.98      -0.824    
   1620.09      -0.908    
   1626.20      -1.006    
   1632.31      -1.134    
   1638.42      -1.269    
   1644.53      -0.619    
   1650.64      -0.687    
   1656.75      -1.472    
   1662.86      -1.335    
   1668.97      -1.138    
   1675.08      -0.791    
   1681.19      -0.229    
   1687.30      -0.202    
   1693.41      -0.197    
   1699.52       0.924    
   1705.63       1.245    
   1711.74       1.565    
   1717.85       1.080    
   1723.96       0.909    
   1730.07       0.686    
   1736.18       0.723    
   1742.29       1.039    
   1748.40       0.358    
   1754.50    -8.6E-03    
   1760.61      -0.275    
   1766.72      -1.253    
   1772.83      -1.475    
   1778.94      -1.603    
   1785.05      -1.128    
   1791.16      -0.262    
   1797.27       0.604    
   1803.38       1.035    
   1809.49       0.698    
   1815.60      -0.294    
   1821.71      -1.285    
   1827.82      -1.135    
   1833.93      -1.026    
   1840.04      -0.980    
   1846.15      -0.823    
   1852.26      -0.630    
   1858.37      -0.599    
   1864.48      -0.581    
   1870.59      -0.464    
   1876.70      -1.005    
   1882.81      -1.084    
   1888.92      -0.569    
   1895.03      -0.744    
   1901.14      -0.858    
   1907.25      -0.840    
   1913.36      -0.844    
   1919.47      -0.760    
   1925.58      -0.873    
   1931.69      -1.092    
   1937.80      -1.018    
   1943.91      -0.552    
   1950.02      -0.080    
   1956.13       0.022    
   1962.24      -0.043    
   1968.35      -0.135    
   1974.46      -0.210    
   1980.57      -0.264    
   1986.68      -0.225    
   1992.79      -0.113    
   1998.90      -0.068    
   2005.01      -0.082    
   2011.12      -0.099    
   2017.23      -0.146    
   2023.33      -0.271    
   2029.44      -0.576    
   2035.55      -0.826    
   2041.66      -0.909    
   2047.77      -0.987    
   2053.88      -0.985    
   2059.99      -0.884    
   2066.10      -0.820    
   2072.21      -0.720    
   2078.32      -0.300    
   2084.43       0.233    
   2090.54      -0.411    
   2096.65      -1.056    
   2102.76      -1.523    
   2108.87      -1.990    
   2114.98      -1.419    
   2121.09      -0.829    
   2127.20      -0.851    
   2133.31      -1.331    
   2139.42      -1.167    
   2145.53      -0.626    
   2151.64      -0.085    
   2157.75      -0.133    
   2163.86      -0.180    
   2169.97      -1.384    
   2176.08      -2.100    
   2182.19      -1.711    
   2188.30      -0.778    
   2194.41       0.042    
   2200.52      -0.171    
   2206.63      -0.394    
   2212.74       0.140    
   2218.85       0.163    
   2224.96      -0.038    
   2231.07      -0.203    
   2237.18      -0.158    
   2243.29       0.247    
   2249.40       0.607    
   2255.51       0.820    
   2261.62       0.946    
   2267.73       0.942    
   2273.84       0.948    
   2279.95       0.967    
   2286.06       0.872    
   2292.16       0.601    
   2298.27       0.291    
   2304.38       0.260    
   2310.49       0.371    
   2316.60       0.251    
   2322.71       0.120    
   2328.82       0.384    
   2334.93       0.772    
   2341.04       1.996    
   2347.15       2.294    
   2353.26       2.592    
   2359.37       2.075    
   2365.48       1.019    
   2371.59       0.284    
   2377.70       0.423    
   2383.81     6.2E-03    
   2389.92      -0.938    
   2396.03      -1.230    
   2402.14      -1.055    
   2408.25      -0.811    
   2414.36      -0.655    
   2420.47      -0.563    
   2426.58      -0.438    
   2432.69      -0.348    
   2438.80      -0.252    
   2444.91      -0.105    
   2451.02    -9.4E-03    
   2457.13       0.100    
   2463.24       0.180    
   2469.35       0.239    
   2475.46       0.230    
   2481.57       0.186    
   2487.68       0.212    
   2493.79       0.247    
   2499.90       0.421    
   2506.01       0.650    
   2512.12       0.764    
   2518.23       0.792    
   2524.34       0.772    
   2530.45       0.767    
   2536.56       0.785    
   2542.67       0.792    
   2548.78       0.882    
   2554.89       0.955    
   2560.99       0.999    
   2567.10       1.155    
   2573.21       1.467    
   2579.32       2.024    
   2585.43       2.240    
   2591.54       1.905    
   2597.65       1.646    
   2603.76       1.021    
   2609.87       0.122    
   2615.98      -0.145    
   2622.09      -0.020    
   2628.20       0.620    
   2634.31       0.579    
   2640.42       0.265    
   2646.53       0.451    
   2652.64      -0.151    
   2658.75      -1.192    
   2664.86      -1.590    
   2670.97      -1.008    
   2677.08      -1.158    
   2683.19      -1.275    
   2689.30      -0.804    
   2695.41      -0.530    
   2701.52      -0.339    
   2707.63      -0.469    
   2713.74      -0.608    
   2719.85      -0.679    
   2725.96      -0.405    
   2732.07      -0.050    
   2738.18       0.114    
   2744.29       0.329    
   2750.40       0.387    
   2756.51       0.211    
   2762.62      -0.112    
   2768.73      -0.014    
   2774.84       0.423    
   2780.95       0.524    
   2787.06       0.289    
   2793.17      -0.438    
   2799.28      -1.285    
   2805.39      -1.264    
   2811.50      -0.727    
   2817.61      -0.426    
   2823.72      -0.240    
   2829.82      -0.177    
   2835.93       0.232    
   2842.04       0.838    
   2848.15       1.103    
   2854.26       0.763    
   2860.37       0.047    
   2866.48      -0.474    
   2872.59      -0.784    
   2878.70      -0.018    
   2884.81       0.747    
   2890.92       1.401    
   2897.03       1.009    
   2903.14      -0.090    
   2909.25      -0.840    
   2915.36      -0.296    
   2921.47       0.328    
   2927.58       0.290    
   2933.69       0.284    
   2939.80       0.114    
   2945.91      -0.095    
   2952.02       0.087    
   2958.13       0.160    
   2964.24      -0.259    
   2970.35      -0.735    
   2976.46      -1.174    
   2982.57      -1.564    
   2988.68      -1.734    
   2994.79      -1.673    
   3000.90      -1.590    
   3007.01      -1.438    
   3013.12      -1.162    
   3019.23      -1.674    
   3025.34      -1.877    
   3031.45      -1.254    
   3037.56      -0.788    
   3043.67      -0.263    
   3049.78       0.170    
   3055.89       0.412    
   3062.00       0.161    
   3068.11      -0.431    
   3074.22      -0.709    
   3080.33      -0.851    
   3086.44      -0.836    
   3092.55      -0.285    
   3098.65      -0.192    
   3104.76      -0.699    
   3110.87      -1.085    
   3116.98      -1.228    
   3123.09      -1.200    
   3129.20      -1.174    
   3135.31      -0.598    
   3141.42       0.044    
   3147.53       0.091    
   3153.64       0.083    
   3159.75       0.042    
   3165.86      -0.038    
   3171.97      -0.098    
   3178.08       0.099    
   3184.19       0.759    
   3190.30       1.026    
   3196.41       0.425    
   3202.52         0    
   3208.63         0    
   3214.74         0    
   3220.85         0    
   3226.96         0    
   3233.07         0    
   3239.18         0    
   3245.29         0    
   3251.40         0    
   3257.51         0    
   3263.62         0    ];

CYCL.PKpente=B(:,1);
CYCL.PKpente=CYCL.PKpente';
CYCL.penteFpk=B(:,2);
CYCL.penteFpk=CYCL.penteFpk';
