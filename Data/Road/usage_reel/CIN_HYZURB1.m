% File: CIN_HYZURB1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%%   fichier cin�matique
%%   cycle usage reel HYZEM urbain1 
%%   compatible avec bibliotheque VEHLIB
%%   INRETS LTE 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


CYCL.ntypcin=1;	% cinematique vitesse-temps

CYCL.recharge=0;	% Recharge non dispo aujourd'hui
CYCL.pente=0;		% Pente en %


%CYCLE HYZEM URBAIN 1.	
A=[1    0
2    0
3    0
4    0
5    0
6    0
7    0
8    0
9    0
10    0
11    0
12    0
13    0
14    0
15    0
16    0
17    0
18    0
19    0
20    0
21    0
22    0
23    0
24    0
25    0
26    0
27    0
28    0
29    0.4
30    0.9
31    1.7
32    2.6
33    1.7
34    2.2
35    0.4
36    0.4
37    0.4
38    2.2
39    2.6
40    3.9
41    2.2
42    1.3
43    0.4
44    2.6
45    6.6
46    11.4
47    16.6
48    21.4
49    26.2
50    27.5
51    28.4
52    31
53    33.2
54    33.6
55    32.3
56    31.9
57    30.6
58    29.3
59    28
60    21.4
61    10.9
62    3.9
63    2.2
64    1.7
65    1.3
66    0.9
67    0
68    0
69    0
70    0
71    0
72    0
73    0
74    0.9
75    3.5
76    10
77    14
78    19.7
79    24
80    24.9
81    28
82    31
83    33.2
84    35.4
85    35
86    34.1
87    31.9
88    29.3
89    27.1
90    24.9
91    22.7
92    20.1
93    15.7
94    8.7
95    3.1
96    0
97    0
98    0
99    0
100    0
101    0
102    0
103    0
104    0
105    0
106    0
107    0
108    0
109    0
110    0
111    0
112    0
113    0.4
114    0.4
115    0.4
116    0.4
117    0.9
118    0.9
119    0.4
120    0
121    0.9
122    3.9
123    7.9
124    5.2
125    0.4
126    0.4
127    0.4
128    0.4
129    0.4
130    0.4
131    0.4
132    1.3
133    0.9
134    1.3
135    1.3
136    1.3
137    1.7
138    1.7
139    1.3
140    1.3
141    0.9
142    0.4
143    0
144    0
145    0.9
146    2.2
147    2.6
148    2.2
149    1.3
150    0.4
151    0.4
152    0
153    0
154    0.9
155    2.6
156    8.3
157    14.9
158    21
159    25.3
160    27.1
161    28.8
162    31.9
163    35
164    36.7
165    36.7
166    35.4
167    35.8
168    36.7
169    37.1
170    36.3
171    35.8
172    35.4
173    36.7
174    37.6
175    38
176    38.4
177    38.4
178    38
179    37.1
180    32.8
181    24
182    17.9
183    15.3
184    13.5
185    10.9
186    4.8
187    2.6
188    1.3
189    3.1
190    7
191    13.5
192    17.9
193    20.1
194    19.7
195    20.1
196    21
197    24.5
198    27.5
199    30.6
200    31.9
201    32.3
202    34.1
203    36.7
204    38.4
205    39.3
206    38.9
207    37.6
208    35.8
209    35.4
210    36.7
211    38.9
212    40.6
213    42.8
214    44.6
215    45.9
216    46.3
217    45
218    44.6
219    43.7
220    43.3
221    43.7
222    43.7
223    43.3
224    42.8
225    42.4
226    37.6
227    25.8
228    10
229    0.9
230    0
231    0
232    0
233    0
234    0
235    0
236    0
237    0
238    0
239    0
240    0
241    0
242    0
243    0
244    0
245    0
246    0
247    0
248    0
249    0
250    0
251    0
252    0
253    0
254    0
255    0
256    0
257    0
258    0
259    0
260    0
261    0
262    0
263    0
264    0
265    0
266    0
267    0
268    2.6
269    10.9
270    17
271    18.8
272    18.8
273    21.4
274    24
275    25.8
276    28
277    31.9
278    35.8
279    35.4
280    34.5
281    33.6
282    32.8
283    31.5
284    31
285    31
286    30.6
287    29.3
288    24.9
289    16.6
290    10
291    4.4
292    2.2
293    1.3
294    0
295    0
296    0
297    0
298    0
299    0
300    0
301    0
302    0
303    0
304    0
305    0
306    0
307    0
308    0
309    0
310    0
311    0
312    0
313    0
314    0
315    0
316    0
317    0
318    0
319    0
320    0
321    0
322    0
323    0
324    0
325    0
326    0
327    0
328    0
329    0
330    1.7
331    9.6
332    17.5
333    24.9
334    26.2
335    29.7
336    35.8
337    41.5
338    47.6
339    48.5
340    50.2
341    53.3
342    54.6
343    53.3
344    52
345    49.8
346    46.7
347    40.2
348    33.2
349    24.9
350    17.9
351    11.4
352    6.1
353    2.6
354    0.4
355    0
356    0
357    0
358    0
359    0
360    0
361    0
362    0
363    0
364    0
365    0
366    0
367    0
368    0
369    0
370    0
371    0
372    0
373    0
374    0
375    0.9
376    6.1
377    14.9
378    20.5
379    24.5
380    24.9
381    30.1
382    35.4
383    39.8
384    43.3
385    45
386    45.9
387    46.3
388    45
389    45.9
390    47.2
391    48.5
392    48.9
393    48.9
394    49.8
395    50.2
396    48.9
397    47.6
398    45
399    45
400    46.3
401    48.1
402    49.4
403    50.7
404    51.6
405    51.1
406    50.7
407    50.7
408    49.4
409    48.1
410    46.3
411    43.3
412    38.9
413    35.8
414    31
415    26.2
416    21.4
417    16.2
418    11.8
419    9.2
420    7.9
421    6.1
422    3.5
423    0.9
424    0
425    0
426    0
427    0
428    0
429    0
430    0
431    0
432    0
433    0
434    0
435    0
436    0
437    0
438    2.2
439    8.3
440    16.2
441    18.8
442    21.4
443    25.8
444    29.3
445    31.5
446    32.8
447    32.8
448    34.1
449    35.8
450    36.7
451    36.3
452    38.4
453    41.5
454    45
455    47.6
456    50.2
457    52.4
458    53.7
459    52.9
460    50.7
461    45.9
462    40.6
463    37.1
464    34.5
465    32.8
466    30.6
467    27.5
468    23.2
469    18.8
470    16.2
471    12.2
472    9.2
473    7.4
474    7
475    6.1
476    5.2
477    3.5
478    5.2
479    11.8
480    15.3
481    14.9
482    16.6
483    21
484    24
485    24.5
486    28.4
487    34.5
488    40.2
489    46.7
490    50.2
491    50.2
492    52.4
493    55.5
494    56.8
495    58.5
496    59
497    58.5
498    57.2
499    54.6
500    53.3
501    51.1
502    50.2
503    50.2
504    50.2
505    49.4
506    47.6
507    46.3
508    45.4
509    44.6
510    43.7
511    43.7
512    45.9
513    48.5
514    50.7
515    52
516    51.6
517    51.1
518    52
519    50.7
520    46.3
521    41.1
522    36.7
523    33.6
524    28.8
525    22.7
526    16.6
527    11.4
528    5.7
529    2.2
530    0.9
531    0
532    0
533    0
534    0
535    0
536    3.9
537    11.4
538    17
539    17.5
540    22.7
541    27.1
542    30.6
543    31
544    33.2
545    36.3
546    39.8
547    43.3
548    45.9
549    48.9
550    49.8
551    49.4
552    48.9
553    49.4
554    51.1
555    52.4
556    53.3
557    54.6
558    55.5
559    55.9
560    55
561    54.6
562    53.7
563    52.9
564    53.3
565    54.2
566    54.6
567    54.6
568    55
569    55.5
570    55.9
571    55.9
572    55.5
573    55.5
574    56.4
575    56.8
576    56.4
577    55.5
578    53.7
579    53.3
580    53.3
581    54.6
582    55
583    55.9
584    55.9
585    55.9
586    56.4
587    56.8
588    56.4
589    55.9
590    56.4
591    56.4
592    55.5
593    54.2
594    52.9
595    50.2
596    46.3
597    42.8
598    38.4
599    35.8
600    32.3
601    28.4
602    24.9
603    23.2
604    21
605    17
606    10.9
607    5.7
608    2.2
609    0
610    0
611    0
612    0
613    0
614    0
615    0
616    0
617    0
618    0
619    0
620    0.9
621    6.6
622    14
623    18.3
624    21
625    21.4
626    24.9
627    28.8
628    32.8
629    35.8
630    37.6
631    36.7
632    35.8
633    34.1
634    31.9
635    30.1
636    27.5
637    26.2
638    28.8
639    31.9
640    35
641    37.6
642    40.6
643    43.7
644    45.4
645    48.5
646    49.4
647    50.2
648    49.8
649    48.9
650    48.5
651    48.5
652    48.5
653    47.2
654    45.4
655    41.9
656    37.6
657    32.3
658    28
659    22.3
660    19.2
661    17.5
662    18.8
663    23.2
664    28
665    31.5
666    34.1
667    33.6
668    35
669    37.1
670    37.1
671    36.3
672    32.8
673    27.1
674    19.2
675    11.8
676    4.8
677    0
678    0
679    0
680    0.4
681    1.3
682    2.6
683    4.4
684    5.2
685    7
686    9.2
687    12.2
688    15.3
689    17.9
690    21.8
691    22.3
692    23.6
693    27.1
694    27.5
695    27.1
696    24.5
697    23.2
698    21.4
699    19.7
700    17.9
701    17.9
702    17.9
703    19.2
704    20.1
705    18.8
706    17.5
707    14.9
708    9.2
709    2.6
710    0.4
711    0
712    0
713    0
714    0
715    0
716    0
717    0
718    0
719    0
720    0];

CYCL.temps=A(:,1);
CYCL.temps=CYCL.temps';
CYCL.vitesse=A(:,2)./3.6;
CYCL.vitesse=CYCL.vitesse';


