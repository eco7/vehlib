% File: CIN_R1
%%    fichier cin�matique
%
%  © Copyright IFSTTAR LTE 1999-2011 
%
%%   cycle R1 INRETS 
%%   modifi� le 31/12/98 pour VEHLIB


CYCL.ntypcin=1;	% cinematique vitesse-temps

CYCL.recharge=0;	% Recharge non dispo aujourd'hui
CYCL.pente=0;		% Pente en %


A=  [0.00     0.00
     1.00     0.00
     2.00     0.00
     3.00     0.00
     4.00     0.00
     5.00     0.00
     6.00     0.00
     7.00     3.20
     8.00     9.70
     9.00    15.60
    10.00    18.00
    11.00    22.10
    12.00    25.10
    13.00    28.10
    14.00    29.80
    15.00    30.40
    16.00    34.00
    17.00    39.90
    18.00    45.80
    19.00    48.80
    20.00    52.30
    21.00    54.70
    22.00    55.90
    23.00    55.90
    24.00    55.90
    25.00    55.90
    26.00    57.60
    27.00    60.60
    28.00    63.00
    29.00    64.20
    30.00    64.80
    31.00    65.30
    32.00    65.30
    33.00    64.80
    34.00    65.30
    35.00    65.30
    36.00    65.30
    37.00    64.20
    38.00    63.00
    39.00    61.20
    40.00    59.40
    41.00    58.80
    42.00    57.10
    43.00    55.90
    44.00    55.30
    45.00    54.10
    46.00    51.70
    47.00    48.20
    48.00    43.40
    49.00    38.10
    50.00    32.20
    51.00    26.90
    52.00    23.90
    53.00    20.40
    54.00    15.60
    55.00    10.90
    56.00    12.10
    57.00    18.00
    58.00    23.30
    59.00    28.60
    60.00    31.60
    61.00    36.90
    62.00    41.70
    63.00    45.20
    64.00    48.80
    65.00    50.00
    66.00    48.80
    67.00    47.00
    68.00    45.20
    69.00    43.40
    70.00    41.70
    71.00    39.30
    72.00    38.10
    73.00    36.30
    74.00    35.20
    75.00    34.00
    76.00    32.20
    77.00    31.00
    78.00    29.80
    79.00    28.60
    80.00    27.50
    81.00    26.30
    82.00    25.70
    83.00    25.10
    84.00    22.70
    85.00    19.80
    86.00    16.80
    87.00    13.30
    88.00     7.90
    89.00     2.00
    90.00     0.00
    91.00     0.00
    92.00     2.10
    93.00     9.90
    94.00    18.20
    95.00    23.50
    96.00    27.00
    97.00    34.20
    98.00    41.30
    99.00    43.60
   100.00    46.00
   101.00    50.20
   102.00    54.90
   103.00    59.00
   104.00    62.00
   105.00    60.80
   106.00    60.20
   107.00    61.40
   108.00    61.40
   109.00    62.00
   110.00    60.20
   111.00    59.60
   112.00    60.80
   113.00    62.60
   114.00    65.60
   115.00    67.30
   116.00    67.90
   117.00    66.20
   118.00    62.60
   119.00    59.60
   120.00    53.10
   121.00    44.20
   122.00    37.10
   123.00    35.90
   124.00    38.90
   125.00    42.50
   126.00    47.20
   127.00    50.70
   128.00    49.60
   129.00    44.80
   130.00    40.70
   131.00    41.30
   132.00    43.60
   133.00    40.10
   134.00    35.30
   135.00    34.20
   136.00    34.20
   137.00    30.00
   138.00    21.10
   139.00    12.80
   140.00    14.00
   141.00    20.50
   142.00    24.70
   143.00    28.20
   144.00    35.30
   145.00    42.50
   146.00    44.20
   147.00    46.60
   148.00    51.30
   149.00    56.10
   150.00    59.00
   151.00    57.90
   152.00    55.50
   153.00    50.20
   154.00    41.30
   155.00    32.40
   156.00    25.90
   157.00    24.70
   158.00    27.00
   159.00    30.60
   160.00    35.90
   161.00    36.50
   162.00    33.00
   163.00    29.40
   164.00    28.20
   165.00    25.90
   166.00    23.50
   167.00    21.10
   168.00    16.40
   169.00    11.00
   170.00     6.90
   171.00     3.30
   172.00     2.80
   173.00     2.30
   174.00     1.10
   175.00     0.50
   176.00     1.10
   177.00     2.30
   178.00     4.10
   179.00     7.70
   180.00    11.20
   181.00    17.10
   182.00    22.50
   183.00    24.80
   184.00    24.80
   185.00    18.90
   186.00    11.80
   187.00    11.20
   188.00    10.00
   189.00     4.70
   190.00     8.80
   191.00    15.40
   192.00    21.30
   193.00    27.20
   194.00    34.90
   195.00    40.30
   196.00    47.40
   197.00    49.10
   198.00    54.50
   199.00    55.70
   200.00    62.20
   201.00    64.00
   202.00    61.60
   203.00    62.80
   204.00    63.40
   205.00    62.20
   206.00    62.20
   207.00    61.60
   208.00    59.80
   209.00    58.60
   210.00    55.70
   211.00    58.00
   212.00    53.90
   213.00    56.30
   214.00    53.90
   215.00    51.50
   216.00    45.60
   217.00    44.40
   218.00    43.80
   219.00    42.00
   220.00    44.40
   221.00    47.40
   222.00    48.60
   223.00    52.10
   224.00    55.70
   225.00    57.40
   226.00    53.90
   227.00    54.50
   228.00    50.30
   229.00    45.00
   230.00    37.90
   231.00    33.10
   232.00    32.60
   233.00    30.20
   234.00    24.80
   235.00    17.70
   236.00    13.60
   237.00     5.30
   238.00     0.00
   239.00     0.00
   240.00     0.00
   241.00     0.00
   242.00     0.00
   243.00     0.00
   244.00     0.00
   245.00     0.00
   246.00     0.00
   247.00     0.00
   248.00     0.00
   249.00     0.00
   250.00     0.00
   251.00     0.00
   252.00     0.00
   253.00     0.00
   254.00     0.00
   255.00     0.00
   256.00     0.00
   257.00     0.00
   258.00     0.00
   259.00     2.90
   260.00     7.10
   261.00    17.10
   262.00    25.40
   263.00    30.20
   264.00    36.10
   265.00    41.40
   266.00    43.20
   267.00    48.00
   268.00    52.10
   269.00    54.50
   270.00    58.00
   271.00    58.00
   272.00    63.40
   273.00    63.40
   274.00    65.10
   275.00    65.10
   276.00    64.60
   277.00    61.00
   278.00    62.80
   279.00    59.20
   280.00    56.80
   281.00    54.50
   282.00    54.50
   283.00    40.30
   284.00    36.70
   285.00    30.20
   286.00    26.00
   287.00    27.80
   288.00    32.00
   289.00    41.40
   290.00    48.60
   291.00    53.30
   292.00    54.50
   293.00    52.10
   294.00    51.50
   295.00    49.10
   296.00    44.40
   297.00    42.00
   298.00    39.10
   299.00    34.90
   300.00    28.40
   301.00    20.10
   302.00    16.00
   303.00    17.70
   304.00    23.70
   305.00    30.20
   306.00    34.30
   307.00    34.90
   308.00    36.70
   309.00    43.20
   310.00    43.20
   311.00    43.80
   312.00    42.00
   313.00    40.30
   314.00    40.30
   315.00    41.40
   316.00    45.60
   317.00    47.40
   318.00    45.00
   319.00    46.80
   320.00    43.80
   321.00    43.80
   322.00    39.10
   323.00    30.80
   324.00    20.10
   325.00    10.60
   326.00     5.90
   327.00     2.90
   328.00     0.00
   329.00     0.00
   330.00     0.00
   331.00     0.00
   332.00     0.00
   333.00     0.00
   334.00     0.00
   335.00     0.00
   336.00     0.00
   337.00     0.00
   338.00     0.00
   339.00     0.00
   340.00     0.00
   341.00     0.00
   342.00     0.00
   343.00     0.00
   344.00     0.00
   345.00     0.00
   346.00     0.00
   347.00     0.00
   348.00     0.00
   349.00     0.00
   350.00     0.00
   351.00     0.00
   352.00     0.00
   353.00     4.20
   354.00    11.40
   355.00    16.80
   356.00    22.80
   357.00    25.80
   358.00    28.20
   359.00    28.20
   360.00    28.20
   361.00    27.00
   362.00    25.80
   363.00    25.20
   364.00    24.60
   365.00    26.40
   366.00    30.60
   367.00    35.30
   368.00    40.70
   369.00    46.10
   370.00    52.10
   371.00    55.70
   372.00    58.10
   373.00    61.60
   374.00    64.60
   375.00    68.20
   376.00    70.00
   377.00    68.80
   378.00    66.40
   379.00    63.40
   380.00    60.40
   381.00    58.70
   382.00    58.10
   383.00    58.10
   384.00    57.50
   385.00    56.30
   386.00    54.50
   387.00    52.70
   388.00    51.50
   389.00    49.70
   390.00    48.50
   391.00    47.30
   392.00    46.10
   393.00    44.90
   394.00    43.70
   395.00    42.50
   396.00    41.30
   397.00    40.10
   398.00    38.90
   399.00    37.70
   400.00    37.10
   401.00    35.90
   402.00    33.50
   403.00    29.40
   404.00    22.80
   405.00    15.60
   406.00    10.20
   407.00     5.40
   408.00     2.40
   409.00     1.90
   410.00     0.70
   411.00     0.00
   412.00     0.00
   413.00     0.00
   414.00     0.00
   415.00     0.00
   416.00     0.00
   417.00     0.00
   418.00     0.00
   419.00     0.00
   420.00     0.00
   421.00     0.00
   422.00     0.00
   423.00     0.00
   424.00     0.00
   425.00     0.00
   426.00     0.00
   427.00     0.00
   428.00     0.00
   429.00     0.00
   430.00     0.00
   431.00     0.00
   432.00     0.00
   433.00     0.00
   434.00     0.70
   435.00     4.80
   436.00    13.20
   437.00    23.40
   438.00    30.60
   439.00    38.30
   440.00    46.10
   441.00    50.30
   442.00    54.50
   443.00    57.50
   444.00    58.10
   445.00    59.30
   446.00    61.00
   447.00    62.20
   448.00    64.00
   449.00    65.80
   450.00    67.60
   451.00    68.20
   452.00    68.80
   453.00    68.80
   454.00    68.80
   455.00    68.20
   456.00    66.40
   457.00    64.60
   458.00    63.40
   459.00    58.70
   460.00    63.40
   461.00    64.60
   462.00    65.80
   463.00    65.80
   464.00    65.20
   465.00    64.60
   466.00    63.40
   467.00    62.80
   468.00    64.00
   469.00    64.60
   470.00    64.60
   471.00    62.80
   472.00    60.40
   473.00    56.30
   474.00    50.90
   475.00    45.50
   476.00    39.50
   477.00    35.90
   478.00    34.70
   479.00    34.70
   480.00    34.70
   481.00    36.50
   482.00    38.30
   483.00    38.30
   484.00    37.70
   485.00    36.50
   486.00    34.70
   487.00    33.50
   488.00    32.90
   489.00    33.50
   490.00    34.10
   491.00    32.90
   492.00    30.00
   493.00    25.20
   494.00    22.20
   495.00    19.80
   496.00    18.60
   497.00    18.00
   498.00    18.00
   499.00    16.80
   500.00    15.60
   501.00    15.00
   502.00    14.40
   503.00    11.40
   504.00     9.00
   505.00     8.40
   506.00     8.40
   507.00     9.60
   508.00    10.20
   509.00    12.60
   510.00    10.80
   511.00     9.60
   512.00    12.00
   513.00    12.60
   514.00    13.20
   515.00    13.20
   516.00    13.20
   517.00    15.60
   518.00    15.60
   519.00    15.00
   520.00    13.80
   521.00    12.00
   522.00    10.80
   523.00     9.60
   524.00     7.20
   525.00     3.60
   526.00     1.30
   527.00     0.10
   528.00     0.00
   529.00     0.00
   530.00     0.00
   531.00     0.00
   532.00     0.00
   533.00     0.00
   534.00     0.00
   535.00     0.00
   536.00     0.00
   537.00     0.00
   538.00     0.00
   539.00     0.00
   540.00     0.00
   541.00     0.00
   542.00     0.00
   543.00     0.00
   544.00     0.00
   545.00     0.00
   546.00     0.00
   547.00     0.00
   548.00     0.00
   549.00     0.00
   550.00     0.00
   551.00     0.00
   552.00     0.00
   553.00     0.00
   554.00     0.00
   555.00     0.00
   556.00     0.00
   557.00     0.00
   558.00     0.00
   559.00     3.40
   560.00    10.10
   561.00    18.00
   562.00    26.40
   563.00    30.90
   564.00    35.40
   565.00    40.40
   566.00    46.00
   567.00    50.50
   568.00    54.40
   569.00    55.50
   570.00    57.20
   571.00    58.90
   572.00    58.30
   573.00    55.50
   574.00    52.70
   575.00    50.50
   576.00    46.60
   577.00    39.30
   578.00    32.00
   579.00    25.30
   580.00    19.70
   581.00    14.10
   582.00     9.00
   583.00     8.50
   584.00    14.60
   585.00    23.00
   586.00    30.90
   587.00    36.50
   588.00    41.50
   589.00    46.60
   590.00    51.60
   591.00    56.60
   592.00    61.10
   593.00    64.50
   594.00    66.20
   595.00    67.30
   596.00    68.40
   597.00    69.50
   598.00    70.10
   599.00    70.10
   600.00    67.30
   601.00    63.90
   602.00    61.10
   603.00    58.30
   604.00    55.00
   605.00    51.00
   606.00    46.00
   607.00    42.10
   608.00    38.20
   609.00    34.20
   610.00    29.80
   611.00    25.30
   612.00    20.80
   613.00    15.20
   614.00    10.10
   615.00     4.50
   616.00     1.20
   617.00     0.80
   618.00     0.00
   619.00     0.00
   620.00     0.00
   621.00     0.00
   622.00     0.80
   623.00     5.40
   624.00    13.70
   625.00    22.00
   626.00    26.60
   627.00    28.40
   628.00    31.20
   629.00    37.60
   630.00    43.10
   631.00    45.90
   632.00    45.90
   633.00    44.10
   634.00    43.10
   635.00    43.10
   636.00    43.10
   637.00    44.10
   638.00    45.00
   639.00    45.90
   640.00    46.80
   641.00    46.80
   642.00    46.80
   643.00    46.80
   644.00    45.90
   645.00    45.00
   646.00    43.10
   647.00    41.30
   648.00    38.50
   649.00    34.90
   650.00    32.10
   651.00    29.30
   652.00    27.50
   653.00    25.70
   654.00    24.70
   655.00    23.80
   656.00    22.90
   657.00    20.10
   658.00    18.30
   659.00    16.50
   660.00    13.70
   661.00    11.90
   662.00    13.70
   663.00    18.30
   664.00    23.80
   665.00    26.60
   666.00    30.30
   667.00    33.00
   668.00    36.70
   669.00    41.30
   670.00    47.70
   671.00    53.30
   672.00    56.90
   673.00    57.90
   674.00    56.90
   675.00    56.00
   676.00    57.90
   677.00    60.60
   678.00    61.50
   679.00    62.50
   680.00    60.60
   681.00    58.80
   682.00    56.00
   683.00    52.30
   684.00    47.70
   685.00    41.30
   686.00    39.50
   687.00    41.30
   688.00    44.10
   689.00    45.90
   690.00    46.80
   691.00    47.70
   692.00    46.80
   693.00    45.00
   694.00    41.30
   695.00    38.50
   696.00    34.90
   697.00    29.30
   698.00    24.70
   699.00    19.20
   700.00    11.90
   701.00     4.50
   702.00     0.80
   703.00     0.60
   704.00     0.00
   705.00     0.00
   706.00     0.00
   707.00     0.00
   708.00     0.00
   709.00     0.00
   710.00     0.00
   711.00     0.00
   712.00     0.00
   713.00     0.00
   714.00     0.00
   715.00     0.00
   716.00     0.00
   717.00     2.50
   718.00     8.90
   719.00    19.00
   720.00    25.50
   721.00    27.30
   722.00    27.30
   723.00    25.50
   724.00    23.60
   725.00    26.40
   726.00    32.80
   727.00    41.10
   728.00    44.80
   729.00    43.00
   730.00    39.30
   731.00    34.70
   732.00    31.00
   733.00    31.00
   734.00    31.90
   735.00    32.80
   736.00    34.70
   737.00    35.60
   738.00    38.40
   739.00    39.30
   740.00    41.10
   741.00    43.00
   742.00    43.90
   743.00    45.70
   744.00    47.60
   745.00    47.60
   746.00    45.70
   747.00    46.60
   748.00    47.60
   749.00    49.40
   750.00    51.20
   751.00    52.20
   752.00    51.20
   753.00    47.60
   754.00    45.70
   755.00    46.60
   756.00    47.60
   757.00    48.50
   758.00    51.20
   759.00    54.00
   760.00    56.80
   761.00    57.70
   762.00    55.80
   763.00    54.00
   764.00    53.10
   765.00    51.20
   766.00    45.70
   767.00    43.00
   768.00    39.30
   769.00    37.40
   770.00    35.60
   771.00    34.70
   772.00    31.00
   773.00    28.20
   774.00    26.40
   775.00    22.70
   776.00    18.10
   777.00    14.40
   778.00    13.50
   779.00    13.50
   780.00    10.80
   781.00     8.00
   782.00     5.20
   783.00     5.20
   784.00     3.40
   785.00     0.60
   786.00     0.00
   787.00     0.00
   788.00     0.00
   789.00     0.00
   790.00     0.00
   791.00     0.00
   792.00     0.00
   793.00     0.00
   794.00     0.00
   795.00     0.00
   796.00     0.00
   797.00     0.00
   798.00     0.00
   799.00     0.00
   800.00     0.00
   801.00     0.00
   802.00     0.00
   803.00     0.00
   804.00     0.00
   805.00     0.00
   806.00     0.00
   807.00     0.00
   808.00     0.00
   809.00     0.00
   810.00     7.70
   811.00    17.90
   812.00    28.00
   813.00    33.50
   814.00    35.30
   815.00    34.40
   816.00    39.90
   817.00    47.30
   818.00    52.80
   819.00    54.70
   820.00    51.90
   821.00    49.10
   822.00    42.70
   823.00    35.30
   824.00    31.70
   825.00    28.00
   826.00    26.10
   827.00    27.10
   828.00    26.10
   829.00    27.10
   830.00    30.30
   831.00    33.50
   832.00    37.20
   833.00    38.10
   834.00    39.00
   835.00    37.20
   836.00    35.30
   837.00    36.30
   838.00    39.90
   839.00    41.80
   840.00    39.90
   841.00    38.10
   842.00    35.30
   843.00    32.60
   844.00    32.60
   845.00    32.60
   846.00    35.30
   847.00    39.00
   848.00    42.70
   849.00    48.20
   850.00    52.80
   851.00    58.30
   852.00    62.90
   853.00    65.70
   854.00    65.70
   855.00    60.20
   856.00    54.70
   857.00    48.20
   858.00    43.60
   859.00    43.60
   860.00    45.50
   861.00    47.30
   862.00    49.10
   863.00    51.90
   864.00    55.60
   865.00    57.40
   866.00    59.30
   867.00    61.10
   868.00    61.10
   869.00    59.30
   870.00    56.50
   871.00    54.70
   872.00    52.80
   873.00    50.10
   874.00    50.10
   875.00    46.40
   876.00    44.50
   877.00    42.70
   878.00    39.90
   879.00    36.30
   880.00    27.10
   881.00    14.20
   882.00     4.10
   883.00     0.40
   884.00     0.00
   885.00     0.00
   886.00     0.00
   887.00     0.00];

CYCL.temps=A(:,1);
CYCL.temps=CYCL.temps';
CYCL.vitesse=A(:,2)./3.6;
CYCL.vitesse=CYCL.vitesse';
