%% Driver file 
%%Initialise Driver parameters
% for VEHLIB 
% 15/02/2017

DRIV.type=1;  % PID controller

%% Average driver parameters 

DRIV.P=250;  % Proportional
DRIV.I=10;   % Integrator
DRIV.D=0.0;   % Derivator
