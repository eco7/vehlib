%%% =============================================================================
%% 
%%                Initialisation du fichier DRIVER
%%                      Pour librairie VEHLIB
%%                    IFSTTAR - LTE
%%
%%  Objet: Ce script initialise la structure DRIV
%%         contenant les champs necessaires au modele de conducteur de type 2.
%%          
%% 17/03/2017
%% dynamic driver independant from cycle (adaptive param)
%% R Trigui, S Javanmardi
%% =============================================================================

DRIV.type=2;   % driver model for driver simulation.
               % adaptive param with use (vmax)

%% dynamic driver parameters 

DRIV.vmax=[30 50 70 90 110 130]/3.6;

DRIV.ag=[350 350 320 220 200 180]/2;  % acceleration params 
DRIV.R=[0.2 0.2 0.15 0.1 0.08 0.05]*0;   % objective speed deviation from max
DRIV.dex=2.8;   % integrator coef : measure driver ability to follow a long term speed
DRIV.Ta=[3 3 4 7 8 8]*2;    % anticipation time (ant_dist=Veh_speed *Ta)
DRIV.agdecel=[300 300]/2; % decelration factor function of speed
DRIV.v_agdecel=[20 100]./3.6;
DRIV.alpha=0; % slope to be removed (put PentFpk instead)

display('Fin d''initialisation des parametres conducteur'); 

%------------------------------------MISES A JOUR-----------------------------------%
% 

