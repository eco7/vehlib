%% Driver file 
%%Initialise Driver parameters
% for VEHLIB 
% 17/03/2017
%eco driver independant from cycle (adaptive param)

DRIV.type=2;   % driver model for driver simulation.
               % adaptive param with use (vmax)

%% eco driver parameters 

DRIV.vmax=[30 50 70 90 110 130]/3.6;

DRIV.ag=[200 200 150 120 100 100];  % acceleration params 
DRIV.R=[-0.1 -0.1 -0.1 -0.1 -0.11 -0.12];   % objective speed deviation from max
DRIV.dex=2.8;   % integrator coef : measure driver ability to follow a long term speed
DRIV.Ta=[6 6 7 9 11 13];    % anticipation time (ant_dist=Veh_speed *Ta)
DRIV.agdecel=[300 200]; % decelration factor function of speed
DRIV.v_agdecel=[20 100]./3.6;
DRIV.alpha=0; % slope to be removed (put PentFpk instead)

display('Fin d''initialisation des parametres conducteur'); 

%------------------------------------MISES A JOUR-----------------------------------%
%
