%% Driver file 
%%Initialise Driver parameters
% for VEHLIB 
% 15/02/2017

DRIV.type=2;   % driver model for driver simulation

%% Average driver parameters 

DRIV.ag=250;  % acceleration params 
DRIV.R=0.0;   % objective speed deviation from max
DRIV.dex=2;   % integrator coef : measure driver ability to follow a long term speed
DRIV.Ta=5;    % anticipation time (ant_dist=Veh_speed *Ta)
DRIV.agdecel=[500 399.46 114.28 190.75]/2; % decelration factor function of speed
DRIV.v_agdecel=[15 30 80 120]./3.6;
DRIV.alpha=0; % slope to be removed (put PentFpk instead)