% File: DRIVER_TYPE
% =============================================================================
%
%  C  Copyright IFSTTAR LTE 1999-2011 
%
%
%                Initialisation du fichier driver
%                      Pour librairie VEHLIB
%                          IFSTTAR - LTE 
%  Objet: Ce script initialise la structure DRIV
%          contenant les champs necessaires pour renseigner 
%          le composant driver de la librairie VEHLIB.
%
%%             Fichier driver      DRIVER_TYPE.m
%            donnees pour modele du regulateur du conducteur
%                        
% =============================================================================

% PID controller
DRIV.type=1;

% Gain proportionnel
DRIV.P = 2000;

% Gain integral
DRIV.I = 0;

% Gain derviee
DRIV.D = 0;

disp('Fin d''initialisation des parametres vehicule');

%------------------------------------MISES A JOUR-----------------------------------%
