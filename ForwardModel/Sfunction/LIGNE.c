/*  File    : LIGNE.c
 *      Modelisation des lignes de bus.  
 *
 *  © Copyright IFSTTAR LTE 1999-2011 
 *
 * -------------------------------------------------------------------------
 *
 * Objet:
 *
 *      Modelisation des lignes de bus.  
 *      INRETS Juillet 1999 
 *      
 * 
 *  Copyright (c) 1990-1998 by The MathWorks, Inc. All Rights Reserved.
 *  $Revision: 1.3 $
 *  
 * Parametres de sortie:
  *                                     y[0] : vitesse objectif 
  * 					y[1] : vitesse balise 
  * 					y[2] : flag de fin de parcours du cycle 
  * 					y[3] : autorisation de fonct mode hybride 
  * 					y[4] : Flag de charge batterie reseau EDF
  *					y[5] : Charge en passager 
  *					y[6] : Pente longitudinale de la chaussee en %
  *					y[7] : Accessoires variables en W
  *
  * Les etats derives:
  *                             x[0] : distance parcourue 
  * 				x[1] : integrale du djerk == acceleration demandee 
  * 				x[2] : vitesse limitee par le djerk (==y[0])
  */


#define S_FUNCTION_NAME LIGNE
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <math.h>

#define U(element) (*uPtrs[element])  		/* Pointer to Input Port0 */

/* Les arguments de la S fonction */

#define I_TYPE_BAL	0			/* index du premier argument: type de la balise */
#define P_TYPE_BAL(S)	( ssGetSFcnParam(S,I_TYPE_BAL) )

#define I_DIST_BAL  1			/* index du deuxieme argument: distance balise */
#define P_DIST_BAL(S)	( ssGetSFcnParam(S,I_DIST_BAL) )

#define I_VIT_BAL  2			/* index du troisieme argument: vitesse balise */
#define P_VIT_BAL(S)	( ssGetSFcnParam(S,I_VIT_BAL) )

#define I_AUTOH_BAL 3			/* index du quatrieme argument: mode de fonctt impose */
#define P_AUTOH_BAL(S)	( ssGetSFcnParam(S,I_AUTOH_BAL) )

#define I_ARRET_BAL 4			/* index du cinquieme argument: duree d arret */
#define P_ARRET_BAL(S)	( ssGetSFcnParam(S,I_ARRET_BAL) )

#define I_CHARGE_BAL 5			/* index du sixieme argument: charge passager variable */
#define P_CHARGE_BAL(S)	( ssGetSFcnParam(S,I_CHARGE_BAL) )

#define I_DIST_PTE   6			/* index du septieme argument: distance balise pour pente */
#define P_DIST_PTE(S)	( ssGetSFcnParam(S,I_DIST_PTE) )

#define I_PENTE   7			/* index du huitieme argument: pente */
#define P_PENTE(S)	( ssGetSFcnParam(S,I_PENTE) )

#define I_DIST_ACCES   8	/* index du huitieme argument: distance balise pour puiss. accessoires */
#define P_DIST_ACCES(S)	( ssGetSFcnParam(S,I_DIST_ACCES) )

#define I_ACCES   9			/* index du neuvieme argument: puissance accessoires */
#define P_ACCES(S)	( ssGetSFcnParam(S,I_ACCES) )

#define I_DJERK		10		/* index du dixieme argument: djerk */
#define P_DJERK(S)	( ssGetSFcnParam(S,I_DJERK) )

#define I_DCC		11		/* index du onzieme argument: deceleration imposee */
#define P_DCC(S)	( ssGetSFcnParam(S,I_DCC) )

#define I_ACC		12			/* index du douzieme argument: acceleration maximum */
#define P_ACC(S)	( ssGetSFcnParam(S,I_ACC) )

#define NPARAMS		13		/* nombre total de parametres de la Sfonction */

/* Indice de la balise courante */
static int ind_bal;
static int ind_accel;
/* Indice de la balise de pente */
static int ind_pte;
/* Indice de la balise de puissance accessoire */
static int ind_acces;

/*====================*
 * S-function methods *
 *====================*/

/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{
    
      
    ssSetNumSFcnParams(S, NPARAMS);  /* Number of expected parameters */
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        return; /* Parameter mismatch will be reported by Simulink */
    }

     
    ssSetNumContStates(S, 3);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, 1);
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, 8);

    
    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 1);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    /* Take care when specifying exception free code - see sfuntmpl.doc */
    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);

}

/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    Specifiy that we have a continuous sample time.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
}



#define MDL_INITIALIZE_CONDITIONS
/* Function: mdlInitializeConditions ========================================
 * Abstract:
 *    Initialize both continuous states to zero.
 */
static void mdlInitializeConditions(SimStruct *S)
{
real_T *x0 = ssGetContStates(S);
real_T *dccLigne   = mxGetPr(P_DCC(S));


  /* initialisation de l indice au debut */
  ind_bal=0;
  ind_pte=0;
  ind_acces=0;
  ind_accel=1;
  *x0=0.0;
  ssSetRWorkValue(S,0,0.0);  /* le temps */
}

/* Function: mdlOutputs =======================================================
 * Abstract:
 *      y = Cx + Du 
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
   real_T            *y    = ssGetOutputPortRealSignal(S,0);
   real_T            *x    = ssGetContStates(S);
   InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
   real_T temps=ssGetT(S);
   static char mess_err[256];
   real_T vit, dist, cumt_dec, dist_dec; 
   static double v0, t0;
   real_T *typeBal    = mxGetPr(P_TYPE_BAL(S));
   real_T *vitBal     = mxGetPr(P_VIT_BAL(S));
   real_T *distBal    = mxGetPr(P_DIST_BAL(S));
   real_T *arrBal     = mxGetPr(P_ARRET_BAL(S));
   real_T *authBal    = mxGetPr(P_AUTOH_BAL(S));
   real_T *chargeBal  = mxGetPr(P_CHARGE_BAL(S));
   real_T *distpente  = mxGetPr(P_DIST_PTE(S));
   real_T *pente	   = mxGetPr(P_PENTE(S));
   real_T *distacces  = mxGetPr(P_DIST_ACCES(S));
   real_T *acces	   = mxGetPr(P_ACCES(S));
   real_T *djerkLigne = mxGetPr(P_DJERK(S));
   real_T *dccLigne   = mxGetPr(P_DCC(S));
   int_T indBalMax = mxGetNumberOfElements(P_TYPE_BAL(S));
   int_T indPteMax = mxGetNumberOfElements(P_PENTE(S));
   int_T indAccesMax = mxGetNumberOfElements(P_ACCES(S));
   int_T j;
   real_T binf;
   real_T vmoy, d, tmoy;

   /* vit==vitesse realisee */ 
   vit=*uPtrs[0];

   /* dist= distance parcourue */
   dist=x[0];
   
   /*-----------------------------------------------------------*
    *                  Gestion pente                            *
    *-----------------------------------------------------------*/
   if(ind_pte<(indPteMax-1) && dist>distpente[ind_pte]) {
      ind_pte+=1;
   }
   y[6]=pente[ind_pte];
   /*-----------------------------------------------------------*/

   /*-----------------------------------------------------------*
    *                  Gestion accessoires                      *
    *-----------------------------------------------------------*/
   if(ind_acces<(indAccesMax-1) && dist>distacces[ind_acces]) {
      ind_acces+=1;
   }
   y[7]=acces[ind_acces];
   /*-----------------------------------------------------------*/
  
   /*-----------------------------------------------------------*
    *          Gestion de l arret on a parcouru le cycle        *
    *-----------------------------------------------------------*/
   /* Condition necessaire sur y[4] pour provoquer la charge en fin de parcour de ligne */
    if(ind_bal==indBalMax) {
      y[2]=1;
      y[4]=1;
      return;
    }
   /*-----------------------------------------------------------*/


   /*-----------------------------------------------------------*
    *     Gestion de la vitesse balise y[1] issue du fichier    *
    *   sans djerk sans acceleration ni deceleration limitee   * 
    *-----------------------------------------------------------*/
    binf=0;
    for (j=0;j<indBalMax;j++) {
      if(typeBal[ind_bal]==1 && dist<distBal[j] && dist>binf) {
         y[1]=vitBal[j];
         break;
      }
      else if( (typeBal[ind_bal]==2||typeBal[ind_bal]==3)) {
         y[1]=0;
         break;
      }
      binf=distBal[j];
    }
   /*-----------------------------------------------------------*/

   /* Flag de recharge initialise a 0 */
    y[4]=0;

   /*-----------------------------------------------------------*
    *                  Gestion d une balise d arret             * 
    *-----------------------------------------------------------*/
    if(typeBal[ind_bal]==2||typeBal[ind_bal]==3) { 					
     y[0]=0;
     y[2]=0;
     if (typeBal[ind_bal]==3) { 
       y[4]=1; /* On provoque une charge */
     }
   
     /* arret pendant arrBal[ind_bal] sec. */
     if((temps-ssGetRWorkValue(S,0))>=arrBal[ind_bal]) {
       ssSetRWorkValue(S,0,temps);
       ind_bal+=1;
       dccLigne[0]=dccLigne[0];
       /* Gestion de l arret on a parcouru le cycle */
       if(ind_bal==indBalMax) {
          y[2]=1;
          y[4]=1;
          return;
       }
     }
   }
   else {
       ssSetRWorkValue(S,0,temps);
   }
   /*-----------------------------------------------------------*
    *                 Gestion d une balise de vitesse           * 
    *-----------------------------------------------------------*/
   if (typeBal[ind_bal]==1) {								
     if(ind_bal<(indBalMax-1)&&vitBal[ind_bal+1]<vitBal[ind_bal]) {
       /* il faut integrer une deceleration*/
       /*  cumt_dec=(vit-vitBal[ind_bal+1])/(-1*dccLigne[0]);
       dist_dec=dccLigne[0]/2.*cumt_dec*cumt_dec+(vit)*cumt_dec; */
       cumt_dec=(vit-vitBal[ind_bal+1])/(-1*dccLigne[0]);
       dist_dec=dccLigne[0]/2.*cumt_dec*cumt_dec+(vit)*cumt_dec; 
     }
     else {
       cumt_dec=0;
       dist_dec=0;
     }
      
     if((dist+dist_dec)<=distBal[ind_bal]) {
       /* on continue d accelerer avec djerk */
       ind_accel=1;
       y[0]=x[2];
       y[2]=0;
       /*  v0=vitBal[ind_bal]; */
       v0=*uPtrs[0];
       t0=temps;
     }
     else {
       /* on decelere */
       ind_accel=-1;
       y[0]=x[2];
       y[2]=0;
       if((ind_bal<(indBalMax-1)&&y[0]<=vitBal[ind_bal+1])||
          (ind_bal==(indBalMax-1)&&y[0]<=0.)) {
          /* on change de balise */
          ind_bal+=1;
          if(ind_bal==indBalMax) {
            /* Gestion de l arret on a parcouru le cycle */
            y[2]=1;
            y[4]=1;
            return;
          }
       }
     }
   }
   /*-----------------------------------------------------------*/

   /* autorisation de fonctionner en mode hybride */
   y[3]=authBal[ind_bal];
   y[5]=chargeBal[ind_bal];
  /* printf("Sortie mdl_output y0=%f y1=%f indbal=%d\n",y[0],y[1],ind_bal); */

}

#define MDL_DERIVATIVES
/* Function: mdlDerivatives =================================================
 * Abstract:
 *      xdot = Ax + Bu
 */
static void mdlDerivatives(SimStruct *S)
{
    real_T            *dx   = ssGetdX(S);
    real_T            *x    = ssGetContStates(S);
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
    real_T *vitBal     = mxGetPr(P_VIT_BAL(S));
    real_T *djerkLigne = mxGetPr(P_DJERK(S));
    real_T *dccLigne   = mxGetPr(P_DCC(S));
    real_T *accLigne   = mxGetPr(P_ACC(S));
    real_T *typeBal    = mxGetPr(P_TYPE_BAL(S));
    int_T indBalMax    = mxGetNumberOfElements(P_TYPE_BAL(S));

    /* Premier etat derivee: la vitesse reelle */
    dx[0]=(*uPtrs[0]);
    if (dx[0] < 0.) {
        /* La vitesse vehicule est négative car il y a une pente >0
         * Si la vitesse ne repasse pas >0 sur un pas de calcul, x[2] reste nul,
         * le vehicule reste a l'arret. Il faut le "forcer a decoller" */
        dx[0]=0.01;
    }

    if(ind_accel==1) { 			/* phase d'acceleration */
      dx[1]=djerkLigne[0];
      /* On limite l acceleration maximum */
      if(x[1]>accLigne[0]) { 	
         x[1]=accLigne[0];
      } 
      dx[2]=x[1];
      if (x[2]>=vitBal[ind_bal]) {
          dx[1]=0;
          x[1]=0;
          dx[2]=0;
          x[2]=vitBal[ind_bal];
      }
    }
    else {				/* phase de freinage */
      dx[1]=-10;
      /* On limite la deceleration maximum */
      if(x[1]<dccLigne[0]) { 	
         x[1]=dccLigne[0];
      } 
      dx[2]=x[1];
      if (x[2]<=0) {
          dx[1]=0;
          x[1]=0;
          dx[2]=0;
          x[2]=0;
      }
    }
    

   /* Gestion de l arret on a parcouru le cycle */
   if(ind_bal==indBalMax) {
      y[2]=1;
      y[4]=1;
      return;
   }
   
}

/* Function: mdlTerminate =====================================================
 * Abstract:
 *    No termination needed, but we are required to have this routine.
 */
static void mdlTerminate(SimStruct *S)
{
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif

