CycleName_test = 'ARTAUTO_BV_Classe33in_NoNeut_maxcrouerecup_dtot'; %name of the test set
n_inputs = '3';
CycleName_train = 'ARTURB_BV_Classe3_-20__-10__0__10__20_ARTROUT_BV_Classe3_-20__-10__0__10__20_ARTAUTO_BV_Classe3_-20__-10__0__10__20_3in_NoNeut_maxcrouerecup_dtot'; %name of the training set for importing statistics data for input normalization
Model_name = 'nrj_mngt_neural_network_Model12_test'; %name of the neural network imported
tf_prediction =  csvread(strcat(Model_name,'_',CycleName_test,'_predictions.csv')); % i create a matrix of the outputs of the neural network imported from python for further comparisons
filename = strcat(Model_name,'.h5'); %this is the name of the h5 file with my neural network weights
classNames = {'0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21'};
%nrj_mngt_neuralnet = importKerasNetwork(filename, 'Classes', classNames, 'OutputLayerType','classification'); % i import the neural network
%nrj_mngt_neuralnet = evalin('base',"nrj_mngt_neuralnet");
load(strcat(CycleName_train,'_settings.mat')); %i load the stats about my training set in order to normalize my inputs in matlab before prediction
load(strcat(CycleName_test,'_X.mat')); %I load the input data from the test set WLTC
Stats=evalin('base',"Settings.Stats");
mean_croue = Stats(1,1);
var_croue = Stats(1,2);
mean_wroue = Stats(2,1);
var_wroue = Stats(2,2);
mean_SOC = Stats(3,1);
var_SOC = Stats(3,2);
y_classes = [-25000,-22500,-20000,-17500,-15000,-12500,-10000,-7500,-5000,-2500,0,2500,5000,7500,10000,12500,15000,17500,20000,22500,25000,27500];
%X=evalin('base',"X");
%matlab_prediction = double(classify(nrj_mngt_neuralnet,[(X(1,:)-mean_croue)/sqrt(var_croue);(X(2,:)-mean_wroue)/sqrt(var_wroue);((X(3,:))-mean_SOC)/(sqrt(var_SOC))]'));
time = linspace(0,10,51);
croue = [time' , (X(1,51:152)'-mean_croue)/sqrt(var_croue)];
wroue = [time' , (X(2,51:152)'-mean_wroue)/sqrt(var_wroue)];
DSOC = [time' , (X(3,51:152)'-mean_SOC)/sqrt(var_SOC)];
sim('test_model12');
idx = tf_prediction(51:152)+1;
pacm1_tf = y_classes(idx)';
index = find((double(Pacm1.Data) == pacm1_tf));
errors = length(index);
% for i=1:length(X(1,:))
%     croue = [0 (X(1,i)-mean_croue)/sqrt(var_croue)
%             10 (X(1,i)-mean_croue)/sqrt(var_croue)];
%     wroue = [0 (X(2,i)-mean_wroue)/sqrt(var_wroue)
%              10 (X(2,i)-mean_wroue)/sqrt(var_wroue)];
%     DSOC = [0 ((X(3,i))-mean_SOC)/(sqrt(var_SOC))
%             10 ((X(3,i))-mean_SOC)/(sqrt(var_SOC))];
%     sim('test_model12');
%     if Pacm1.Data(end) ~= y_classes(tf_prediction(i)+1)
%        Pacm1(end);
%        y_classes(tf_prediction(i)+1);
%        error = error + 1;
%     end
% end

%I classify my inputs thanks to the matlab neural network and convert it to
%a double matrix
%index = find((tf_prediction+1) ~= matlab_prediction);
%import_differences = length(index); %here i have all the differences from the 2 networks (python and matlab)