/*  File    : sf_simulation_conduite.c
 *
 *      Modelisation d'un mode simulation de conduite dans VEHLIB.  
 *      IFSTTAR-LTE Fevrier 2011
 *      
 * 
 *  Copyright (c) 1990-1998 by The MathWorks, Inc. All Rights Reserved.
 *  $Revision: 1.1 $
 *  
 */

 /* Les sorties :			y[0] : vitesse objectif 
  * 					y[1] : vitesse balise 
  * 					y[2] : flag de fin de parcours du cycle 
  *					y[3] : Pente longitudinale de la chaussee en %
  *
  * Les etats derives:		x[0] : distance parcourue 
  * 				x[1] : integrale du djerk == acceleration demandee 
  * 				x[2] : vitesse limitee par le djerk (==y[0])
  */


#define S_FUNCTION_NAME generation_cycle
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <math.h>

#define U(element) (*uPtrs[element])  		/* Pointer to Input Port0 */

/* Les arguments de la S fonction */
/* CYCL.balise(:,1),CYCL.balise(:,2),CYCL.balise(:,3),CYCL.distpente_bal,CYCL.pente_bal,CYCL.Vmaxi,CYCL.aAccel,CYCL.bAccel,CYCL.aDecel,CYCL.bDecel,CYCL.djerk */

#define I_TYPE_BAL	0			/* type de la balise */
#define P_TYPE_BAL(S)	( ssGetSFcnParam(S,I_TYPE_BAL) )

#define I_DIST_BAL  1			/* distance balise de vitesse */
#define P_DIST_BAL(S)	( ssGetSFcnParam(S,I_DIST_BAL) )

#define I_VIT_BAL  2			/* vitessse de la balise */
#define P_VIT_BAL(S)	( ssGetSFcnParam(S,I_VIT_BAL) )

#define I_ARRET_BAL  3			/* duree d'arret de la balise */
#define P_ARRET_BAL(S)	( ssGetSFcnParam(S,I_ARRET_BAL) )

#define I_DIST_PTE   4			/* distance pour balise de pente */
#define P_DIST_PTE(S)	( ssGetSFcnParam(S,I_DIST_PTE) )

#define I_PENTE   5			/* pente */
#define P_PENTE(S)	( ssGetSFcnParam(S,I_PENTE) )

#define I_VMAXI		6		/* index du sixieme argument: vitesse maxi vehicule */
#define P_VMAXI(S)	( ssGetSFcnParam(S,I_VMAXI) )

#define I_AACCEL	7			/* index du septieme argument: aAccel */
#define P_AACCEL(S)	( ssGetSFcnParam(S,I_AACCEL) )

#define I_BACCEL	8			/* index du huitieme argument: bAccel */
#define P_BACCEL(S)	( ssGetSFcnParam(S,I_BACCEL) )

#define I_ADECEL	9			/* index du neuvieme argument: aAccel */
#define P_ADECEL(S)	( ssGetSFcnParam(S,I_ADECEL) )

#define I_BDECEL	10			/* index du dixieme argument: bAccel */
#define P_BDECEL(S)	( ssGetSFcnParam(S,I_BDECEL) )

#define I_ACCELMINI	        11			/* index du onzieme argument: djerk */
#define P_ACCELMINI(S)	( ssGetSFcnParam(S,I_ACCELMINI) )

#define I_DJERK	        12			/* index du onzieme argument: djerk */
#define P_DJERK(S)	( ssGetSFcnParam(S,I_DJERK) )

#define NPARAMS		13		/* nombre total de parametres de la Sfonction */

#define CONTINUOUS_STATE_NUMBER 3

#define GET_SPARAM(S,i) ssGetSFcnParam(S,i)

/* Indice de la balise courante */
static int ind_bal;
static int ind_accel;
/* Indice de la balise de pente */
static int ind_pte;
/* Indice de la balise de puissance accessoire */
static int ind_acces;

/*====================*
 * S-function methods *
 *====================*/

#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
  /* Function: mdlCheckParameters =============================================
   * Abstract:
   *    Validate our parameters to verify they are okay.
   */
  static void mdlCheckParameters(SimStruct *S)
  {
      /* Check 1st parameter: vit */
      {
          if (!mxIsDouble(GET_SPARAM(S,0)) ||
              mxGetNumberOfElements(GET_SPARAM(S,0)) != 1) {
              ssSetErrorStatus(S,"1st parameter to S-function must be a "
                               "scalar \"vit\"");
              return;
          }
      }
 
      /* Check 2nd parameter: Cres */
     {
          if (!mxIsDouble(GET_SPARAM(S,1)) ||
              mxGetNumberOfElements(GET_SPARAM(S,1)) != 1) {
              ssSetErrorStatus(S,"2nd parameter to S-function must be a "
                               "scalar \"Fres\"");
              return;
          }
      }
 
      /* Check 3nd parameter: Masse */
      {
          if (!mxIsDouble(GET_SPARAM(S,2)) ||
              mxGetNumberOfElements(GET_SPARAM(S,2)) != 1) {
              ssSetErrorStatus(S,"3rd parameter to S-function must be a "
                               "scalar \"Masse_bil\"");
              return;
          }
      }
      /* Check 3nd parameter: Cmax_acm1(wacm1) */
      {
          if (!mxIsDouble(GET_SPARAM(S,3)) ||
              mxGetNumberOfElements(GET_SPARAM(S,3)) != 1) {
              ssSetErrorStatus(S,"4rd parameter to S-function must be a "
                               "scalar \"Fmax_acm1\"");
              return;
          }
      }
  }
#endif /* MDL_CHECK_PARAMETERS */
 

/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{
    
      
    ssSetNumSFcnParams(S, NPARAMS);  /* Number of expected parameters */
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        return; /* Parameter mismatch will be reported by Simulink */
    }

     
    ssSetNumContStates(S, CONTINUOUS_STATE_NUMBER);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 4)) return;

    /* La vitesse */
    ssSetInputPortWidth(S, 0, 1);
    /* La force de resistance a l'avancement */
    ssSetInputPortWidth(S, 1, 1);
    /* La masse vehicule*/
    ssSetInputPortWidth(S, 2, 1);
    /* La force motrice ACM1 */
    ssSetInputPortWidth(S, 3, 1);

    ssSetInputPortDirectFeedThrough(S, 0, 1);
    ssSetInputPortDirectFeedThrough(S, 1, 1);
    ssSetInputPortDirectFeedThrough(S, 2, 1);
    ssSetInputPortDirectFeedThrough(S, 3, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, 4);

    
    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 1);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    /* Take care when specifying exception free code - see sfuntmpl.doc */
    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);

}

/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    Specifiy that we have a continuous sample time.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
}



#define MDL_INITIALIZE_CONDITIONS
/* Function: mdlInitializeConditions ========================================
 * Abstract:
 *    Initialize both continuous states to zero.
 */
static void mdlInitializeConditions(SimStruct *S)
{
real_T *x0 = ssGetContStates(S);


  /* initialisation de l indice au debut */
  ind_bal=0;
  ind_pte=0;
  ind_acces=0;
  ind_accel=1;
  x0[0]=0.0;
  x0[1]=0.0;
  x0[2]=0.0;
  ssSetRWorkValue(S,0,0.0);  /* le temps */
}

/* Function: mdlOutputs =======================================================
 * Abstract:
 *      y = Cx + Du 
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
   real_T            *y    = ssGetOutputPortRealSignal(S,0);
   real_T            *x    = ssGetContStates(S);
   InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
   
   real_T temps=ssGetT(S);
   static char mess_err[256];
   real_T vit, dist, cumt_dec, dist_dec; 
   static double v0, t0;
   real_T *typeBal    = mxGetPr(P_TYPE_BAL(S));
   real_T *distBal     = mxGetPr(P_DIST_BAL(S));
   real_T *vitBal     = mxGetPr(P_VIT_BAL(S));
   real_T *arrBal     = mxGetPr(P_ARRET_BAL(S));
   real_T *distpente  = mxGetPr(P_DIST_PTE(S));
   real_T *pente	   = mxGetPr(P_PENTE(S));
   real_T *djerkLigne = mxGetPr(P_DJERK(S));
   int_T indBalMax = mxGetNumberOfElements(P_TYPE_BAL(S));
   int_T indPteMax = mxGetNumberOfElements(P_PENTE(S));
   int_T j;
   real_T binf;
   real_T vmoy, d, tmoy;
   real_T decelDesired;
    real_T *aDecel     = mxGetPr(P_ADECEL(S));
    real_T *bDecel     = mxGetPr(P_BDECEL(S));
    real_T *vitMaxi     = mxGetPr(P_VMAXI(S));

    
   decelDesired=(aDecel[0]*vitBal[ind_bal]+bDecel[0]);
   
   /* inputportsignals */
   vit=*uPtrs[0];

   /* dist= distance parcourue */
   dist=x[0];
   
   /*-----------------------------------------------------------*
    *                  Gestion pente                            *
    *-----------------------------------------------------------*/

   /* printf("Gestion pente ind_pte=%d distpente=%f dis=%f \n",ind_pte,distpente[ind_pte],dist); */

   if(ind_pte<(indPteMax-1) && dist>distpente[ind_pte]) {
      ind_pte+=1;
   }
   y[3]=pente[ind_pte];
   /*-----------------------------------------------------------*/

   /*-----------------------------------------------------------*
    *          Gestion de l arret on a parcouru le cycle        *
    *-----------------------------------------------------------*/
   /* Condition necessaire sur y[4] pour provoquer la charge en fin de parcour de ligne */
    if(ind_bal==indBalMax) {
      y[2]=1;
      return;
    }
   /*-----------------------------------------------------------*/


   /*-----------------------------------------------------------*
    *     Gestion de la vitesse balise y[1] issue du fichier    *
    *   sans djerk sans acceleration ni deceleratrion limitee   * 
    *-----------------------------------------------------------*/
    binf=0;
    for (j=0;j<indBalMax;j++) {
      if(typeBal[ind_bal]==1 && dist<distBal[j] && dist>binf) {
         y[1]=vitBal[j];
         break;
      }
      else if( (typeBal[ind_bal]==2||typeBal[ind_bal]==3)) {
         y[1]=0;
         break;
      }
      binf=distBal[j];
    }
   /*-----------------------------------------------------------*/

   /*-----------------------------------------------------------*
    *                  Gestion d une balise d arret             * 
    *-----------------------------------------------------------*/
    if(typeBal[ind_bal]==2) { 
     y[0]=0;
     y[2]=0;
   
     /* arret pendant arrBal[ind_bal] sec. */
     if((temps-ssGetRWorkValue(S,0))>=arrBal[ind_bal]) {
       ssSetRWorkValue(S,0,temps);
       ind_bal+=1;
       /* Gestion de l arret on a parcouru le cycle */
       if(ind_bal==indBalMax) {
          y[2]=1;
          return;
       }
     }
   }
   else {
       ssSetRWorkValue(S,0,temps);
   }
   /*-----------------------------------------------------------*
    *                 Gestion d une balise de vitesse           * 
    *-----------------------------------------------------------*/
   if (typeBal[ind_bal]==1) {
     if(ind_bal<(indBalMax-1)&&vitBal[ind_bal+1]<vitBal[ind_bal]) {
       /* il faut integrer une deceleration*/
       cumt_dec=(vit-vitBal[ind_bal+1])/(-1*decelDesired);
       dist_dec=decelDesired/2.*cumt_dec*cumt_dec+(vit)*cumt_dec; 
     }
     else {
       cumt_dec=0;
       dist_dec=0;
     }
      
     if((dist+dist_dec)<=distBal[ind_bal]) {
       /* on continue d accelerer avec djerk */
        /* printf("Acceleration y[0]=%f \n",x[2]); */

       ind_accel=1;
       y[0]=x[2];
       y[2]=0;
       /*  v0=vitBal[ind_bal]; */
       v0=*uPtrs[0];
       t0=temps;
     }
     else {
       /* on decelere */
       ind_accel=-1;
       y[0]=x[2];
       y[2]=0;
       if((ind_bal<(indBalMax-1)&&y[0]<=vitBal[ind_bal+1])||
          (ind_bal==(indBalMax-1)&&y[0]<=0.)) {
          /* on change de balise */
          ind_bal+=1;
          if(ind_bal==indBalMax) {
            /* Gestion de l arret on a parcouru le cycle */
            y[2]=1;
            return;
          }
       }
     }
   }
   /*-----------------------------------------------------------*/

  /* printf("Sortie mdl_output temps=%f y0=%f y1=%f indbal=%d\n",temps,y[0],y[1],ind_bal); */

}

#define MDL_DERIVATIVES
/* Function: mdlDerivatives =================================================
 * Abstract:
 *      xdot = Ax + Bu
 */
static void mdlDerivatives(SimStruct *S)
{
    real_T            *dx   = ssGetdX(S);
    real_T            *x    = ssGetContStates(S);
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
    real_T vit, fMaxAcm1, fAcm1, fRes, masse;
    real_T accelDesired, decelDesired;
    real_T *typeBal    = mxGetPr(P_TYPE_BAL(S));
    real_T *vitBal     = mxGetPr(P_VIT_BAL(S));
    real_T *vitMaxi     = mxGetPr(P_VMAXI(S));
    real_T *aAccel     = mxGetPr(P_AACCEL(S));
    real_T *bAccel     = mxGetPr(P_BACCEL(S));
    real_T *aDecel     = mxGetPr(P_ADECEL(S));
    real_T *bDecel     = mxGetPr(P_BDECEL(S));
    real_T *accelMini     = mxGetPr(P_ACCELMINI(S));

    real_T *djerkLigne = mxGetPr(P_DJERK(S));
    real_T dccLigne   = -1;
    real_T accLigne   = 1;
    int_T indBalMax    = mxGetNumberOfElements(P_TYPE_BAL(S));
   
    /* inputportsignals */
   vit=*uPtrs[0];
   fRes=*uPtrs[1];
   masse=*uPtrs[2];
   fMaxAcm1=*uPtrs[3];

   /* Calcul de fAcm1: force motrice */
   fAcm1=fMaxAcm1*(aAccel[0]*vitBal[ind_bal]+bAccel[0]);
   /* Desired Acceleration */
   accelDesired=(fAcm1-fRes)/masse;
   /* Desired Deceleration */
   decelDesired=(aDecel[0]*vitBal[ind_bal]+bDecel[0]);

    /* premier etat derivee: la vitesse reelle */
    dx[0]=(*uPtrs[0]);

    if(ind_accel==1) { 			/* phase d'acceleration */
      dx[1]=djerkLigne[0];
      /* On limite l acceleration maximum */
      if(x[1]>accelDesired) { 	
         x[1]=accelDesired;
      } 
      /* On limite l acceleration minimum */
      if(x[1]<accelMini[0]) { 	
         x[1]=accelMini[0];
      } 
      dx[2]=x[1];
      if (x[2]>=vitBal[ind_bal]) {
          dx[1]=0;
          x[1]=0;
          dx[2]=0;
          x[2]=vitBal[ind_bal];
      }
      if (x[2]>=vitMaxi[0]) {
          dx[1]=0;
          x[1]=0;
          dx[2]=0;
          x[2]=vitMaxi[0];
      }
      
      
    }
    else {				/* phase de freinage */
      dx[1]=(-1)*djerkLigne[0];
      /* On limite la deceleration maximum */
      if(x[1]<decelDesired) { 	
         x[1]=decelDesired;
      }
      if(x[1]>=0) {
          x[1]=0;
      }
      dx[2]=x[1];
      if (x[2]<=0) {
          dx[1]=0;
          x[1]=0;
          dx[2]=0;
          x[2]=0;
      }
    }
    

   /* Gestion de l arret on a parcouru le cycle */
   if(ind_bal==indBalMax) {
      y[2]=1;
      return;
   }
        
  /* printf("Sortie deriv ind_accel= %d dx[1]=%f x[1]=%f x[2]=%f vitBal[ind_bal]=%f \n",ind_accel,dx[1],x[1],x[2],vitBal[ind_bal]); */
    
}

/* Function: mdlTerminate =====================================================
 * Abstract:
 *    No termination needed, but we are required to have this routine.
 */
static void mdlTerminate(SimStruct *S)
{
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
