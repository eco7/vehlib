/* Function: pacejka_sfonction
 * Modele de Pacejka dans la bibliotheque de modele VEHLIB
 *
 *  © Copyright IFSTTAR LTE 1999-2011 
 *
 * -------------------------------------------------------------------------
 *
 * Objet:
 *
 * Modele de Pacejka dans la bibliotheque de modele VEHLIB
 */

#define S_FUNCTION_NAME pacejka_sfonction
#define S_FUNCTION_LEVEL 2

#include <math.h>
#include "simstruc.h"

/*====================*
 * S-function methods *
 *====================*/

#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS)
static void mdlCheckParameters(SimStruct *S)
{
    
    if (mxGetNumberOfElements(ssGetSFcnParam(S,0)) != 1) {
        ssSetErrorStatus(S, "Un scalaire, non de dju !");
        return;}
}

#endif

/*
definition des parametres
*/
#define pCx1 mxGetPr(ssGetSFcnParam(S,0))[0]
#define pDx1 mxGetPr(ssGetSFcnParam(S,0))[1]
#define pDx2 mxGetPr(ssGetSFcnParam(S,0))[2]
#define pEx1 mxGetPr(ssGetSFcnParam(S,0))[3]
#define pEx2 mxGetPr(ssGetSFcnParam(S,0))[4]
#define pEx3 mxGetPr(ssGetSFcnParam(S,0))[5]
#define pEx4 mxGetPr(ssGetSFcnParam(S,0))[6]
#define pKx1 mxGetPr(ssGetSFcnParam(S,0))[7]
#define pKx2 mxGetPr(ssGetSFcnParam(S,0))[8]
#define pKx3 mxGetPr(ssGetSFcnParam(S,0))[9]
#define pHx1 mxGetPr(ssGetSFcnParam(S,0))[10]
#define pHx2 mxGetPr(ssGetSFcnParam(S,0))[11]
#define pVx1 mxGetPr(ssGetSFcnParam(S,0))[12]
#define pVx2 mxGetPr(ssGetSFcnParam(S,0))[13]

#define rBx1 mxGetPr(ssGetSFcnParam(S,0))[14]
#define rBx2 mxGetPr(ssGetSFcnParam(S,0))[15]
#define rCx1 mxGetPr(ssGetSFcnParam(S,0))[16]
#define rHx1 mxGetPr(ssGetSFcnParam(S,0))[17]

#define Fz0 mxGetPr(ssGetSFcnParam(S,0))[18]
#define Fpz0 mxGetPr(ssGetSFcnParam(S,0))[19]

/* Function: mdlInitializeSizes */
static void mdlInitializeSizes(SimStruct *S)
{
    /* See sfuntmpl_doc.c for more details on the macros below */

    /* gestion des parametres (en lien avec les define ci-dessus)*/
    ssSetNumSFcnParams(S,1);  /* Number of expected parameters
     en laissant 1, on entrera soit une valeur, soit une liste de
     valeurs, par exemple [1  3.4  5] pour 3 valeurs*/
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        return;
    }

    /* nombre d'etat continu*/
    ssSetNumContStates(S, 0);
    /* aucune valeur d'etat */
    ssSetNumDiscStates(S, 0);
    

    if (!ssSetNumInputPorts(S, 3)) return; 
    /*ssSetInputPortWidth(S, 0, DYNAMICALLY_SIZED);
    ssSetInputPortDirectFeedThrough(S, 0, 1); */
    ssSetInputPortWidth(S, 0, 1);
    ssSetInputPortWidth(S, 1, 1);
    ssSetInputPortWidth(S, 2, 1);
    ssSetInputPortDirectFeedThrough(S, 0, 1);
    ssSetInputPortDirectFeedThrough(S, 1, 1);
    ssSetInputPortDirectFeedThrough(S, 1, 2);
    
    /* gestion des sorties*/
    if (!ssSetNumOutputPorts(S, 2)) return;
    ssSetOutputPortWidth(S, 0, 1);
    ssSetOutputPortWidth(S, 1, 1);
    
    /*ssSetOutputPortDirectFeedThrough(S, 0, 1);
    ssSetOutputPortDirectFeedThrough(S, 1, 1);*/
    
    /*ssSetOutputPortWidth(S, 0, DYNAMICALLY_SIZED);*/
    /*ssSetOutputPortWidth(S, 0, 1);*/

    /* gestion des pas de calcul*/
    ssSetNumSampleTimes(S, 1);
    ssSetOptions(S, 0);
}



/* Function: mdlInitializeSampleTimes */
static void mdlInitializeSampleTimes(SimStruct *S)
{
/* Initilize the sample times */
    ssSetSampleTime(S,0,CONTINUOUS_SAMPLE_TIME);   /* continu*/
    ssSetOffsetTime(S,0,0.0);                          /* continu*/

}



#undef MDL_INITIALIZE_CONDITIONS   /* Change to #undef to remove function */
#if defined(MDL_INITIALIZE_CONDITIONS)
  /* Function: mdlInitializeConditions */
  static void mdlInitializeConditions(SimStruct *S)
  {
    /*real_T *x0 = ssGetContStates(S);*/

    /* Initialize the states */
    /*/x0[0] = 0;*/
    /*x0[1] = 0;*/
    
  
  }
#endif /* MDL_INITIALIZE_CONDITIONS */



#undef MDL_START  /* Change to #undef to remove function */
#if defined(MDL_START) 
  /* Function: mdlStart */
  static void mdlStart(SimStruct *S)
  {
  }
#endif /*  MDL_START */

#define MDL_PROCESS_PARAMETERS
#if defined(MDL_PROCESS_PARAMETERS)  && defined(MATLAB_MEX_FILE)
/*
 * Process any changes to parameters
 */
static void mdlProcessParameters(SimStruct *S)
{
}
#endif



/* Function: mdlOutputs */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    InputRealPtrsType uPtrs0 = ssGetInputPortRealSignalPtrs(S,0);
    real_T dfz,SHx,Glx,Cx,Mux,Dx,Ex,Kx,Bx,SVx,Fx,Fx0,sign_glx;
    real_T Bxalpha,Cxalpha,SHxalpha,Gxalpha;
    real_T Gl,Fz,alpha;

    
    Gl  = *uPtrs0[0];
    Fz  = *uPtrs0[1];
    alpha=*uPtrs0[2];
                         /*variables */
  
/*calcul effort longi pur*/
                         
dfz = ( Fz - Fpz0 ) / Fz0 ;  /*incrément de charge*/
 
SHx = pHx1 + pHx2 * dfz ; 
Glx = Gl + SHx ; 
Cx = pCx1 ; 
Mux = ( pDx1 + pDx2 * dfz ) ;
Dx = Mux * Fz ;
if (Glx >= 0)
{
    sign_glx=1;
}
else
{
    sign_glx=-1;
}
    
Ex = ( pEx1 + pEx2 * dfz + pEx3 * dfz*dfz ) * ( 1 - pEx4 * sign_glx ) ;

if (Ex > 1)
{
     Ex = 1;
}
 
Kx = Fz * ( pKx1 + pKx2 * dfz ) * exp ( pKx3 * dfz ) ;
Bx = Kx / ( Cx * Dx ) ;
SVx = Fz * ( pVx1 + pVx2 * dfz ) ; 
Fx0 = Dx * sin ( Cx * atan ( Bx * Glx - Ex * ( Bx * Glx - atan ( Bx * Glx ) ) ) ) + SVx ;
   
/*calcul effort longi couple*/
Bxalpha = rBx1 * cos ( atan ( rBx2 * Gl ) ) ;
 
Cxalpha = rCx1 ;
 
SHxalpha = rHx1 ;
 
Gxalpha = cos ( Cxalpha * atan ( Bxalpha * ( alpha + SHxalpha ) ) ) / cos ( Cxalpha * atan ( Bxalpha * SHxalpha ) ) ;
 
Fx = Fx0 * Gxalpha ;

                                        
  y[0] = Fx0;
  y[1] = Fx;
}



#undef MDL_UPDATE  /* Change to #undef to remove function */
#if defined(MDL_UPDATE)
  /* Function: mdlUpdate */
  static void mdlUpdate(SimStruct *S, int_T tid)
  {
      /*
      fonction vide pour un block continu
      */
  }
#endif /* MDL_UPDATE */



#undef MDL_DERIVATIVES  /* Change to #undef to remove function */
#if defined(MDL_DERIVATIVES)
  /* Function: mdlDerivatives */
  static void mdlDerivatives(SimStruct *S)
  {

    real_T            *x    = ssGetContStates(S);
 /*   real_T            *dx   = ssGetdX(S);

    InputRealPtrsType uPtrs0 = ssGetInputPortRealSignalPtrs(S,0);*/
                            
  }
#endif /* MDL_DERIVATIVES */



/* Function: mdlTerminate */
static void mdlTerminate(SimStruct *S)
{
}


/*=============================*
 * Required S-function trailer *
 *=============================*/

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
