/*  File    : bin2decSfun.c
 *      Conversion d'un nombre binaire en decimal suivant le nombre de bit
 *      de codage passe en parametre.  
 *     Le bit de poids fort est a droite (!!)

 *  © Copyright IFSTTAR LTE 1999-2011 
 *
 * -------------------------------------------------------------------------
 *
 *
 */


#define S_FUNCTION_NAME bin2decSfun
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <math.h>

#define U(element) (*uPtrs[element])  		/* Pointer to Input Port0 */

#define I_NB_BIT		0			/* index du douzieme argument: acceleration maximum */
#define P_NB_BIT(S)	( ssGetSFcnParam(S,I_NB_BIT) )

#define NPARAMS		1		/* nombre total de parametres de la Sfonction */

/*====================*
 * S-function methods *
 *====================*/

/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */

static void mdlInitializeSizes(SimStruct *S)
{
          
    ssSetNumSFcnParams(S, NPARAMS);  /* Number of expected parameters */
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        return; /* Parameter mismatch will be reported by Simulink */
    }
  
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 1)) return;    
    ssSetInputPortDirectFeedThrough(S, 0, 1);
    ssSetInputPortWidth(S, 0, DYNAMICALLY_SIZED);
    
    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, DYNAMICALLY_SIZED);
    
    /* specify the sim state compliance to be same as a built-in block */
    ssSetSimStateCompliance(S, USE_DEFAULT_SIM_STATE);

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    /* Take care when specifying exception free code - see sfuntmpl.doc */
    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);

}


#if defined(MATLAB_MEX_FILE)
#define MDL_SET_INPUT_PORT_WIDTH
/* The "mdlSetOutputPortWidth" should be a mirror-image/reverse of your "mdlSetInputPortWidth" 
 * function. People often mistakenly assume that dimensions only propagate "downstream", 
 * but it is entirely possible that your block's output port dimension gets assigned first. 
 * In this case, you should assign that dimension to the second input port as well.
 *
 * "mdlSetDefaultPortDimensionInfo" gets called if any of the ports are
 * still dynamically typed, so it is incorrect to assume that it means that
 * all of the ports are still unspecified. Therefore, instead of just
 * assigning dimensions to all of the ports, you should check each one to
 * confirm that it is still dynamically typed, and only assign dimensions
 * in that case. */
static void mdlSetInputPortWidth(SimStruct *S, int_T port, int_T inputPortWidth)
{
    int_T dim;
    real_T *nb_bit   = mxGetPr(P_NB_BIT(S));
    dim=(int)(*nb_bit);
    ssSetInputPortWidth(S, port, dim);
    ssSetOutputPortWidth(S, 0, 1);
}
#define MDL_SET_OUTPUT_PORT_WIDTH
static void mdlSetOutputPortWidth(SimStruct *S, int_T port, int_T outputPortWidth)
{
    int_T dim;
    real_T *nb_bit   = mxGetPr(P_NB_BIT(S));
    dim=(int)(*nb_bit);
    ssSetOutputPortWidth(S, 0, 1);
    ssSetInputPortWidth(S, port, dim);
    
}
static void mdlSetDefaultPortDimensionInfo(SimStruct *S)
{
    ssSetInputPortWidth(S, 0, 4);
    ssSetOutputPortWidth(S, 0, 1);
}
#endif


/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    Specifiy that we have a continuous sample time.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_INITIALIZE_CONDITIONS
/* Function: mdlInitializeConditions ========================================
 * Abstract:
 *    Initialize both continuous states to zero.
 */
static void mdlInitializeConditions(SimStruct *S)
{
}

/* Function: mdlOutputs =======================================================
 * Abstract:
 *      y = Cx + Du 
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
    real_T *nb_bit   = mxGetPr(P_NB_BIT(S));
    int_T ind=0;
    int_T bit;
    real_T val;

    bit=(int)(*nb_bit);
    val=0;

    while ( ind<bit )
    {
        val=val+(int)(*uPtrs[ind])*pow(2,ind);
        ind=ind+1;
    }
    y[0]=(real_T)val;
}

#define MDL_DERIVATIVES
/* Function: mdlDerivatives =================================================
 * Abstract:
 *      xdot = Ax + Bu
 */
static void mdlDerivatives(SimStruct *S)
{
}

/* Function: mdlTerminate =====================================================
 * Abstract:
 *    No termination needed, but we are required to have this routine.
 */
static void mdlTerminate(SimStruct *S)
{
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif

