% This function calculate the altitude with RGEALTI V2 or BDALTI V2
% longitude and latitude are in degre.decimal values
% altitude is in meter


% fileName='GPS_p2_dyn.csv';
fileName='Anneau_CentreEssaiRenaultAubevoye_Geoportail.csv';

fid =fopen(fileName,'r');
line=fgetl(fid);
A = fscanf(fid, '%g %g', [2 inf]);
fclose(fid);
A = A';

% Coordonnees
longitude=A(:,1);
latitude=A(:,2);

% FileNameKml='test.kml';
% PathNameKml=pwd;
% ecritureKML(latitude,longitude,ones(size(longitude)),FileNameKml,PathNameKml);
if ispc 
    pathDalle='R:\LTE\Projets\VEH\BASES_DE_DONNEES\RGEALTI_V2';
else
    pathDalle='/mnt/ECO7/COMMUN/BASES_DE_DONNEES/RGEALTI_V2';
end

carteType='RGEALTI';
altitude = compute_altitude_IGN([longitude latitude], pathDalle, carteType);

if ispc 
    pathDalle='R:\LTE\Projets\VEH\BASES_DE_DONNEES\BDALTI_V2';
else
    pathDalle='/mnt/ECO7/COMMUN/BASES_DE_DONNEES/BDALTI_V2';
end
carteType='BDALTI';
altitude2 = compute_altitude_IGN([longitude latitude], pathDalle, carteType);

figure;
plot3(longitude, latitude, altitude, longitude, latitude, altitude2)
grid
title('Centre d essai Renault Aubevoye')
legend('RGEALTI\_V2','BDALTI\_V2');

% Ou es-tu ?
disp('Ou es-tu ?');
longitude=4.925405;
latitude=45.736102;
if ispc 
    pathDalle='R:\LTE\Projets\VEH\BASES_DE_DONNEES\RGEALTI_V2';
else
    pathDalle='/mnt/ECO7/COMMUN/BASES_DE_DONNEES/RGEALTI_V2';
end
carteType='RGEALTI';
altitude = compute_altitude_IGN([longitude latitude], pathDalle, carteType);
fprintf(1,'%s %.2f\n','Tu es a l IFSTTAR; l altitude au sol est de : ',altitude);

