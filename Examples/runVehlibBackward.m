function runVehlibBackward
% These are examples showing how to run backward model of vehlib
% with different configurations
close all;

% Run a conventional vehicle
runExample1;

%pause;
close all;

% Run an hybrid vehicle twice, with DP and PMP
runExample2;

end

function runExample1

vehlibFile='Peugeot_308SW_1L6VTI16V';
architecture='VTH_BV';
driveCycle='CIN_NEDC';
paramFile = 'param_backward';
%run the simulation
[ERR, vehlib, VD, ResXml]=VEHLIB_backward(driveCycle, vehlibFile, architecture, paramFile,1,1);

%Extract the temporal variable from xml structure in the current workspace (ie caller)
[err,res]=affectVehlibStruct2Workspace(ResXml,'caller');

% Now you can plot directly
figure(1);
yyaxis left
plot(tsim,vit*3.6,'linewidth',2);
ylabel('Vitesse km/h')
yyaxis right
plot(tsim,rappvit_bv,'linewidth',2);
ylabel('Rapport boite')
xlabel('Temps sec')
grid on;

figure(2);
plot(tsim,croue.*wroue/1000,tsim,cmt.*wmt/1000,'linewidth',2);
ylabel('Puissance kW')
xlabel('Temps sec')
legend('vehicule','moteur')
grid on

figure(3);
plot(tsim,dcarb);
ylabel('debit de carburant')
grid on

gr_mtherm('',VD.MOTH,wmt,cmt,'CSP','2D');

tileWindows;

end

function runExample2

% Initialise datas
driveCycle='CIN_NEDC_BV';
architecture='HP_BV_2EMB';
vehicleFile='Peugeot_308SW_1L6VTI16V_HP_BV_2EMB';
paramFile ='param_backward_prog_dyn_2EMB';
% Run the optimisation
[ERR, vehlib, VD, ResXml_DP]=VEHLIB_backward(driveCycle, vehicleFile, architecture, paramFile, 1, 1, 50);


% Initialise datas
driveCycle='CIN_NEDC_BV';
architecture='HP_BV_2EMB';
vehicleFile='Peugeot_308SW_1L6VTI16V_HP_BV_2EMB';
paramFile ='param_backward_lagrange_HP_BV_2EMB';
% Run the optimisation
[ERR, vehlib, VD, ResXml_PMP]=VEHLIB_backward(driveCycle, vehicleFile, architecture, paramFile, 1, 1, 50);

% Some figures.
figure;
yyaxis left
plot(ResXml_DP.table{1}.tsim.vector,ResXml_DP.table{1}.vit.vector*3.6,'linewidth',2);
ylabel('Vitesse km/h')
yyaxis right
plot(ResXml_DP.table{1}.tsim.vector,ResXml_DP.table{5}.rappvit_bv.vector,'linewidth',2);
ylabel('Rapport boite')
xlabel('Temps sec')
grid on;

gr_mtherm('',VD.MOTH,ResXml_DP.table{9}.wmt.vector,ResXml_DP.table{9}.cmt.vector,'CSP','2D');

gr_melec(VD.ACM1,ResXml_DP.table{7}.wacm1.vector,ResXml_DP.table{7}.cacm1.vector);

figure;
subplot(2,1,1)
plot(ResXml_DP.table{1}.tsim.vector,ResXml_DP.table{7}.wacm1.vector.*ResXml_DP.table{7}.cacm1.vector/1000, ...
    ResXml_DP.table{1}.tsim.vector,ResXml_DP.table{9}.wmt.vector.*ResXml_DP.table{9}.cmt.vector/1000,'linewidth',2);
legend('El motor','Engine');
ylabel('Power kW');
title('DP');
grid on
subplot(2,1,2)
plot(ResXml_PMP.table{1}.tsim.vector,ResXml_PMP.table{7}.wacm1.vector.*ResXml_PMP.table{7}.cacm1.vector/1000, ...
    ResXml_PMP.table{1}.tsim.vector,ResXml_PMP.table{9}.wmt.vector.*ResXml_PMP.table{9}.cmt.vector/1000,'linewidth',2);
legend('El motor','Engine');
ylabel('Power kW');
title('PMP');
grid on

figure;
plot(ResXml_DP.table{1}.tsim.vector,ResXml_DP.table{9}.dcarb.vector, ResXml_PMP.table{1}.tsim.vector,ResXml_PMP.table{9}.dcarb.vector,'linewidth',2);
ylabel([ResXml_DP.table{9}.dcarb.longname, ' : ',ResXml_DP.table{9}.dcarb.unit] )
legend('DP','PMP');
grid on

figure;
plot(ResXml_DP.table{1}.tsim.vector,ResXml_DP.table{10}.soc.vector, ResXml_PMP.table{1}.tsim.vector,ResXml_PMP.table{10}.soc.vector,'linewidth',2);
ylabel([ResXml_DP.table{10}.soc.longname, ' : ',ResXml_DP.table{10}.soc.unit] )
legend('DP','PMP');
grid on

tileWindows;

end

