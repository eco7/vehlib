%0.- fermer tout et ajouter le path des figures
clc;close all;
addpath('figures');

%1.- generer les donnees 
load figures/simcal;

%2.- faire les figures
figure;
simcalPlot(essai,'t','qai',subplot(231));
simcalPlot(essai,'t','soci',subplot(232));
simcalPlot(essai,'t','Ui',subplot(233));

simcalPlot(essai,'ql','qai',subplot(234));
simcalPlot(essai,'ql','soci',subplot(235));
simcalPlot(essai,'ql','Ui',subplot(236));

figure;
simcalPlot(essai,'t','ql',axes);

%2.1- mettre des legendes
printLegTag(gca,'eastoutside');

%3.- faire joli
ha = findobj('type','axes','tag','');
simcalPrettyPlot(ha);

%4.- sauvegarder (manuellement)

%5.- ouvrir et modifier

%5.0.- redimensionner
tileWindows;
fprintf('Fenetres agencees en mosaique horizontal\n');
pause;
tileWindows([],1);
fprintf('Fenetres agencees en mosaique vertical\n');
% hf = findobj('type','figure');
% hf = sort(hf);
% tileWindows([0 hf],1,3);
% maxiWindows(hf(1));
%5.1.- link axes

%5.1.1.-click on figure with subplots
% ha = [subplot(231),subplot(232),subplot(233)];
% linkaxes(ha,'x');
% linkaxes(ha(1:2),'xy');

%5.2.- prettyAxes2
%5.2.1.-click on figure without subplots
fprintf('Appuyer sur une touche du clavier pour changer la couleur des axes\n');
pause;
prettyAxesIfs(gca);

%6.- sauvegarder a nouveau
fprintf('Appuyer sur une touche pour tout fermer\n');pause;
close all;
%7.- export en PNG et en PDF
fprintf('Appuyer sur une touche pour convertir les figures en PDF(vectoriel) et en png(bitmap)\n');pause;
FIG = lsFiles('./figs','.fig');close all;
cellfun(@fig2png,FIG);close all;
cellfun(@fig2pdf,FIG);
%7.1.-ouvrir le PDF en Inkscape
fprintf('En faisant zoom les fichiers.png "pixelent", les fichiers PDF non\n');
fprintf('Les PDF peuvent etre modifiés avec Inkscape (ouvrir et exporter en svg)\n');
%8.- Conclusions:

%8.0.- manipulation de 'handlers' d'objets graphiques
%8.0.1.- ver<=R2014a: get / set
%8.0.2.- ver>=R2014b: handlers are now objects not doubles

%8.1.- Usage de la propriete 'tag'
%8.2.- Usage des expressions regulieres pour selectionner une ligne ou ensemble de lignes
%8.3.- Enregistrer les figures en .fig pour manipulation ulterieur
%8.3.1.- changer palette de couleurs
%8.3.2.- format slide ou format rapport (article)
%8.4.- Preferez l'export en vectoriel (pdf, svg) que bitmap (bmp, jpg, png)
%8.4.1.- exemple ever 2015
%8.5.- Compatibilité Windows / UNIX?
%8.5.1.- Generalement meilleur rendu dans Windows
%8.5.2.- Probleme accents Linux
%8.5.3.- Probleme polices

