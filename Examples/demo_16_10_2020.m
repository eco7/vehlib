% bdclose all;
% close all;
% clearvars;
% clc
% warning('off','all');

fprintf(1,'\n%s\n','Premiere demonstration modeles forward, vehicule C0, parcours CEVE avec modele de conducteur');

% Premiere demonstration sur vehicule CEVE avec modele de conducteur
%vehsim;  Selectionner VELEC et lancer la simulation

vehlibFile = 'Citroen_C0_DRIVER';
architecture = 'VELEC';
driveCycle = 'TRAJET_CEVE_Wstops';
dod0 = 0; % battery fully charged
[vehlib, VD] = vehsim_auto2(vehlibFile, architecture, driveCycle, dod0, 1, 1);
pause;


sim('VELEC_DRIVER');
pause;

figure(1);
plot(distance/1000,vmax*3.6,distance/1000,vit*3.6,'linewidth',2);
ylabel('vitesse km/h')
xlabel('Distance km')
legend('vit limites','vit vehicule');
grid on;

figure(2);
plot(tsim,croue.*wroue/1000,tsim,cacm1.*wacm1/1000,tsim,ubat.*ibat/1000,'linewidth',2);
ylabel('Puissance kW')
xlabel('Temps sec')
legend('vehicule','machine elect.','batterie')
grid on

figure(3);
ax(1)=subplot(3,1,1);
plot(tsim,ubat,'linewidth',2);
ylabel('tension batterie V')
grid on
ax(2)=subplot(3,1,2);
plot(tsim,ibat,'linewidth',2)
grid on
ylabel('Courant batterie A')
xlabel('Temps sec')
ax(3)=subplot(3,1,3);
plot(tsim,soc,'linewidth',2)
grid on
ylabel('Etat de charge %')
xlabel('Temps sec')
linkaxes(ax,'x');

figure(4);
gr_melec(VD.ACM1,wacm1,cacm1);

tileWindows;

pause;

bdclose all;
clearvars;
close all;

fprintf(1,'\n\n%s\n','Seconde demonstration modeles backward, vehicule 308 SW conventionnel, avec simulation amorcage catalyseur');
tic; [ERR, vehlib, VD, ResXml]=VEHLIB_backward('CIN_NEDC_BV_lisse','Peugeot_308SW_1L6VTI16V_TWC','VTH_BV','param_backward_test_VTH',1,1); t=toc;
fprintf(1,'%s\n',['[ERR, vehlib, VD, ResXml]=VEHLIB_backward(''CIN_NEDC_BV_lisse'',''Peugeot_308SW_1L6VTI16V_TWC'',''VTH_BV'',''param_backward_test_VTH'',1,1)']);
fprintf(1,'%s%.2f%s\n','Elapsed time : ',t,' sec');

affectVehlibStruct2Workspace(ResXml,'caller');

co2_gkm =44*trapz(tsim, dcarb)./(12+VD.MOTH.CO2carb)/(distance(end)/1000);

fprintf(1,'%s %.2f%s\n','Consommation du vehicule : ',conso100,' l/100 km');
fprintf(1,'%s %.2f%s\n','Emission CO2 : ',co2_gkm,' g/km');
fprintf(1,'%s %.2f%s\n','Emission CO : ',co_gkm,' g/km');
fprintf(1,'%s %.2f%s\n','Emission HC : ',hc_gkm,' g/km');
fprintf(1,'%s %.2f%s\n','Emission NOx : ',nox_gkm,' g/km');

pause;

figure(1);
yyaxis left
plot(tsim,vit*3.6,'linewidth',2);
ylabel('Vitesse km/h')
yyaxis right
plot(tsim,rappvit_bv,'linewidth',2);
ylabel('Rapport boite')
xlabel('Temps sec')
grid on;

figure(2);
plot(tsim,croue.*wroue/1000,tsim,cmt.*wmt/1000,'linewidth',2);
ylabel('Puissance kW')
xlabel('Temps sec')
legend('vehicule','moteur')
grid on

figure(3);
ax(1)=subplot(3,1,1);
plot(tsim,dcarb);
ylabel('debit de carburant')
grid on
ax(2)=subplot(3,1,2);
plot(tsim,cocum,tsim,hccum,tsim,noxcum,'linewidth',2)
grid on
ylabel('Emission g')
legend('CO','HC','NOx');
ax(3)=subplot(3,1,3);
plot(tsim,co_eff,tsim,hc_eff,tsim,nox_eff,'linewidth',2)
legend('CO','HC','NOx');
grid on
ylabel('Efficacite %')
xlabel('Temps sec')
linkaxes(ax,'x');

gr_mtherm('',VD.MOTH,wmt,cmt,'CSP','2D');


tileWindows;

