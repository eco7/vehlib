function runVehlibForward
% These are examples showing how to run forward model of vehlib
% with different configurations
close all;

% Run a model
runExample1;

pause;
close all;

% Run a model with zero state of charge variation
runExample2;

pause;
close all;

% Read datas, and change a component without changing the other, run the
% simulation
runExample3;

end

function runExample1
% The simplest simulation in batch mode

vehlib_f='CLIO_1L5DCI';
architecture='VTH_BV';
cinem='CIN_ECE15';

% Initialize datas in VD
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem);

% In a function, you have to assign The vehicle data VD
% to the base workspace
assignin('base','VD',VD);
% Open the model and simulate the model
sim(vehlib.simulink);
% close it;
bdclose(vehlib.simulink);
end

function runExample2
% This example will run several times in order to satisfy a zero state of charge for the battery
vehlib_f='PRIUS_II';
architecture='HPDP_EPI';
cinem='CIN_NEDC';
% Dod interval
int_dod=[40 50];
lect_mod=2;
method=2;
precision=0.05;
verbose=0;
warning off;
[erreur,consoZero,conso100,deltaDod,dodIni]=charge_sustain(method,vehlib_f,architecture,cinem,int_dod,lect_mod,precision,verbose);
end

function runExample3
% Change a component data without altering others datas

vehlib_f='CLIO_1L5DCI';
architecture='VTH_BV';
cinem='CIN_ECE15';
Dod0=20;
% Initialize datas in VD
[vehlib, VD]=vehsim_auto2(vehlib_f,architecture,cinem,Dod0);

% Change a component (the cycle in this case) without changing the others
vehlib.CYCL='CIN_ECE15_BV';
[VD, err]=lectureData(vehlib, 2, {'CYCL'}, VD);
[err, vehlib, VD]=ini_calcul(vehlib, VD);

% In a function, you have to assign The vehicle data VD
% to the base workspace
assignin('base','VD',VD);
% Open the model and simulate the model
sim(vehlib.simulink);
% close it;
bdclose(vehlib.simulink);
end
