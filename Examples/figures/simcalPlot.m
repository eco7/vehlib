function ha = simcalPlot(essai,varX,varY,ha,T,SOC,el)
% essai = simcalPlot(essai,T,SOC,el)
% Compile les resultats du traitement des RPT d'une cellule SIMCAL.
% Donnees d'entree:
% - essai [nx1 strust]: structure issue de la compilation faite dans
% simcalRPT
% - T [1x1 double]: temperature de vieillissement (30, 45, 60).
% - SOC [1x1 double]: SOC cible de vieillissement (30, 65, 100).
% - el [1x1 double]: numero d'element (1, 2, 3)
% Donnees de sortie:
% - essai [1x1 struct]: structure avec les champs suivants:
%       - T, SOC, el (conditions d'essai donnees en entree)
%       - tIni, tFin: temps initial et final de chaque essai (sec. a partir de 1/1/2000)
%       - t: temps depuis le premier essai en jours.
%       - Q, DoDi, DoDf, Qsd, Qai, Qaf: capacites
%       - Ui, Uf: tensions
%       - q, qai, qaf, qsd: capacites en p.u. de Q(1).
%       - soci, socf, dodi, dodf: etats de charge
%
% See also simcalRPT

if ~isfield(essai,varX)
    fprintf('ERREUR: le champ %s n''existe pas\n',varX);
    return;
end
if ~isfield(essai,varY)
    fprintf('ERREUR: le champ %s n''existe pas\n',varY);
    return;
end
if ~exist('ha','var')
    hf = figure;
    ha = axes;
end
if ~exist('el','var')
    el = [];
end
if ~exist('SOC','var')
    SOC = [];
end
if ~exist('T','var')
    T = [];
end
%valeurs par defaut: TOUS
if isempty(el)
    el = 1:3;
end
if isempty(SOC)
    SOC = [30 65 100];
end
if isempty(T)
    T = [30 45 60];
end

if length(T)>1
    ha = arrayfun(@(Ti) simcalPlot(essai,varX,varY,ha,Ti,SOC,el),T);
    ha = ha(1);
    return
end
if length(SOC)>1
    ha = arrayfun(@(SOCi) simcalPlot(essai,varX,varY,ha,T,SOCi,el),SOC);
    ha = ha(1);
    return
end
if length(el)>1
    ha = arrayfun(@(eli) simcalPlot(essai,varX,varY,ha,T,SOC,eli),el);
    ha = ha(1);
    return
end

It = [essai.T]==T;
Is = [essai.SOC]==SOC;
Ie = [essai.el]==el;
If = It & Is & Ie;

cetEssai = essai(If);
plot(cetEssai.(varX),cetEssai.(varY),'-o','tag',cetEssai.nom),hold on
xlabel(varX),ylabel(varY);
end