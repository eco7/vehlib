function simcalPrettyPlot(ha)

colorLinesSIMCAL(ha);
prettyAxes(ha);
changeAxesFontSize(ha,12,15);
changeLine(ha,2,10);

simcalPrettyLabels(ha);
end