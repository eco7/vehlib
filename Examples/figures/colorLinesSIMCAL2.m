function colorLinesSIMCAL2(ha)

c = hsv(20);

%%T60
%couleurs pour les cellules calendaires T60 (rouge fonce, normal et orange)
rougef = [0.5 0 0];
rouge = [1 0 0];
oran = [1 0.25 0];
hl = findobj(ha,'-regexp','tag','T60SOC100');
set(hl,'color',c(20,:));
% set(hl,'color',rougef);
hl = findobj(ha,'-regexp','tag','T60SOC.*65');
% set(hl,'color',c(18,:));
set(hl,'color',rouge);
hl = findobj(ha,'-regexp','tag','T60SOC.*30');
set(hl,'color',c(16,:));
% set(hl,'color',oran);

%%T45
%couleurs pour les cellules calendaires T45 (vert fonce, normal et clair)
vertf = [0 .25 0];
vertc = [0 1 0];
vert = [0 .75 .25];
hl = findobj(ha,'-regexp','tag','T45SOC100');
set(hl,'color',c(14,:));
% set(hl,'color',vertf);
hl = findobj(ha,'-regexp','tag','T45SOC.*65');
set(hl,'color',c(12,:));
% set(hl,'color',vert);
hl = findobj(ha,'-regexp','tag','T45SOC.*30');
set(hl,'color',c(10,:));
% set(hl,'color',vertc);
%couleurs pour les cellules calendaire T30 (bleu fonce, normal et clair)
bleuf = [0 0 .5];
bleu = [0 0 1];
bleuc = [0 1 1];
hl = findobj(ha,'-regexp','tag','T30SOC100');
set(hl,'color',c(7,:));
% set(hl,'color',bleuf);
hl = findobj(ha,'-regexp','tag','T30SOC.*65');
set(hl,'color',c(5,:));
% set(hl,'color',bleu);
hl = findobj(ha,'-regexp','tag','T30SOC.*30');
set(hl,'color',c(3,:));
% set(hl,'color',bleuc);

%marqueurs pour les cellules calendaires
hl = findobj(ha,'-regexp','tag','1$');
set(hl,'marker','o');
hl = findobj(ha,'-regexp','tag','2$');
set(hl,'marker','s');
hl = findobj(ha,'-regexp','tag','3$');
set(hl,'marker','d');
hl = findobj(ha,'-regexp','tag','4$');
set(hl,'marker','p');
hl = findobj(ha,'-regexp','tag','5$');
set(hl,'marker','h');
hl = findobj(ha,'-regexp','tag','6$');
set(hl,'marker','^');
end
