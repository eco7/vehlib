function simcalPrettyLabels(ha)
% mettre des unites aux axes x
xl = get(ha,'xlabel');
if iscell(xl)
    xl = [xl{:}];
end
xlt = get(xl,'string');
xlt2 = regexprep(xlt,'^t$','t (j)');
xlt2 = regexprep(xlt2,'(^q[a-z]*$)','$1 (p.u.)');
if length(xl)>1
    for ind = 1:length(xl)
        set(xl(ind),'string',xlt2{ind});
    end
elseif length(xl)==1
    set(xl,'string',xlt2);
end

% mettre des unites aux axes y
yl = get(ha,'ylabel');
if iscell(yl)
    yl = [yl{:}];
end
ylt = get(yl,'string');
ylt2 = regexprep(ylt,'^t$','t (j)');
ylt2 = regexprep(ylt2,'(^q[a-z]*$)','$1 (p.u.)');
ylt2 = regexprep(ylt2,'(^soc[a-z]*$)','$1 (p.u.)');
ylt2 = regexprep(ylt2,'(^U[a-z]*$)','$1 (V)');

if length(yl)>1
    for ind = 1:length(yl)
        set(yl(ind),'string',ylt2{ind});
    end
elseif length(yl)==1
    set(yl,'string',ylt2);
end
end